package scaleform.clik.managers 
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import flash.ui.*;
    import scaleform.clik.constants.*;
    import scaleform.clik.controls.*;
    import scaleform.clik.core.*;
    import scaleform.clik.events.*;
    import scaleform.clik.ui.*;
    import scaleform.clik.utils.*;
    import scaleform.gfx.*;
    
    public class FocusHandler extends Object
    {
        public function FocusHandler()
        {
            super();
            this.currentFocusLookup = new scaleform.clik.utils.WeakRefDictionary();
            this.actualFocusLookup = new scaleform.clik.utils.WeakRefDictionary();
            return;
        }

        public function set stage(arg1:flash.display.Stage):void
        {
            if (this._stage == null) 
            {
                this._stage = arg1;
            }
            this._stage.stageFocusRect = false;
            if (scaleform.gfx.Extensions.enabled) 
            {
                this._stage.addEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.trackMouseDown, false, 0, true);
                this._stage.addEventListener(flash.events.MouseEvent.MOUSE_UP, this.trackMouseDown, false, 0, true);
            }
            this._stage.addEventListener(flash.events.FocusEvent.FOCUS_IN, this.updateActualFocus, false, 0, true);
            this._stage.addEventListener(flash.events.FocusEvent.FOCUS_OUT, this.updateActualFocus, false, 0, true);
            this._stage.addEventListener(flash.events.FocusEvent.KEY_FOCUS_CHANGE, this.handleMouseFocusChange, false, 0, true);
            this._stage.addEventListener(flash.events.FocusEvent.MOUSE_FOCUS_CHANGE, this.handleMouseFocusChange, false, 0, true);
            var loc1:*=scaleform.clik.managers.InputDelegate.getInstance();
            loc1.initialize(this._stage);
            loc1.addEventListener(scaleform.clik.events.InputEvent.INPUT, this.handleInput, false, 0, true);
            return;
        }

        public function getFocus(arg1:uint):flash.display.DisplayObject
        {
            return this.currentFocusLookup.getValue(arg1) as flash.display.DisplayObject;
        }

        public function setFocus(arg1:flash.display.DisplayObject, arg2:uint=0, arg3:Boolean=false):void
        {
            var loc2:*=null;
            var loc8:*=null;
            var loc9:*=NaN;
            var loc10:*=0;
            var loc11:*=0;
            var loc12:*=false;
            var loc1:*=arg1;
            if (arg1 != null) 
            {
                for (;;) 
                {
                    if ((loc2 = arg1 as scaleform.clik.core.UIComponent) != null) 
                    {
                    };
                    if (loc2.focusTarget != null) 
                    {
                        arg1 = loc2.focusTarget;
                        continue;
                    }
                }
            }
            if (loc2 != null) 
            {
                if (loc2.focusable == false) 
                {
                    arg1 = null;
                }
            }
            var loc3:*;
            if ((loc3 = arg1 as flash.display.Sprite) && arg3 && loc3.tabEnabled == false) 
            {
                arg1 = null;
            }
            if (scaleform.clik.core.CLIK.disableNullFocusMoves && (arg1 == null || arg1 == this._stage)) 
            {
                return;
            }
            var loc4:*=this.actualFocusLookup.getValue(arg2) as flash.display.DisplayObject;
            var loc5:*;
            if ((loc5 = this.currentFocusLookup.getValue(arg2) as flash.display.DisplayObject) != arg1) 
            {
                if ((loc2 = loc5 as scaleform.clik.core.UIComponent) != null) 
                {
                    loc2.focused = loc2.focused & ~(1 << arg2);
                }
                if (loc5 != null) 
                {
                    loc5.dispatchEvent(new scaleform.clik.events.FocusHandlerEvent(scaleform.clik.events.FocusHandlerEvent.FOCUS_OUT, true, false, arg2));
                }
                loc5 = arg1;
                this.currentFocusLookup.setValue(arg2, arg1);
                if ((loc2 = loc5 as scaleform.clik.core.UIComponent) != null) 
                {
                    loc2.focused = loc2.focused | 1 << arg2;
                }
                if (loc5 != null) 
                {
                    loc5.dispatchEvent(new scaleform.clik.events.FocusHandlerEvent(scaleform.clik.events.FocusHandlerEvent.FOCUS_IN, true, false, arg2));
                }
            }
            var loc6:*=loc4 is flash.text.TextField;
            var loc7:*=loc5 is scaleform.clik.core.UIComponent;
            if (!(loc4 == loc5) && (!loc6 || loc6 && !loc7)) 
            {
                if (loc1 is flash.text.TextField && !(loc1 == arg1) && arg1 == null) 
                {
                    arg1 = loc1;
                }
                this.preventStageFocusChanges = true;
                if (loc8 = arg1 as flash.display.InteractiveObject) 
                {
                    loc8.focusRect = null;
                }
                if (scaleform.gfx.Extensions.isScaleform) 
                {
                    loc9 = scaleform.gfx.FocusManager.getControllerMaskByFocusGroup(arg2);
                    loc10 = scaleform.gfx.Extensions.numControllers;
                    loc11 = 0;
                    while (loc11 < loc10) 
                    {
                        if (loc12 = !((loc9 >> loc11 & 1) == 0)) 
                        {
                            this.setSystemFocus(loc8, loc11);
                        }
                        ++loc11;
                    }
                }
                else 
                {
                    this.setSystemFocus(loc8);
                }
                this._stage.addEventListener(flash.events.Event.ENTER_FRAME, this.clearFocusPrevention, false, 0, true);
            }
            return;
        }

        protected function setSystemFocus(arg1:flash.display.InteractiveObject, arg2:uint=0):void
        {
            if (scaleform.gfx.Extensions.isScaleform) 
            {
                scaleform.gfx.FocusManager.setFocus(arg1, arg2);
            }
            else 
            {
                this._stage.focus = arg1;
            }
            return;
        }

        protected function getSystemFocus(arg1:uint=0):flash.display.InteractiveObject
        {
            if (scaleform.gfx.Extensions.isScaleform) 
            {
                return scaleform.gfx.FocusManager.getFocus(arg1);
            }
            return this._stage.focus;
        }

        protected function clearFocusPrevention(arg1:flash.events.Event):void
        {
            this.preventStageFocusChanges = false;
            this._stage.removeEventListener(flash.events.Event.ENTER_FRAME, this.clearFocusPrevention, false);
            return;
        }

        public function input(arg1:scaleform.clik.ui.InputDetails):void
        {
            var loc1:*=new scaleform.clik.events.InputEvent(scaleform.clik.events.InputEvent.INPUT, arg1);
            this.handleInput(loc1);
            return;
        }

        public function trackMouseDown(arg1:flash.events.MouseEvent):void
        {
            this.mouseDown = arg1.buttonDown;
            return;
        }

        protected function handleInput(arg1:scaleform.clik.events.InputEvent):void
        {
            var loc16:*=null;
            var loc1:*=arg1.details.controllerIndex;
            var loc2:*=scaleform.gfx.FocusManager.getControllerFocusGroup(loc1);
            var loc3:*;
            if ((loc3 = this.currentFocusLookup.getValue(loc2) as flash.display.InteractiveObject) == null) 
            {
                loc3 = this._stage;
            }
            var loc4:*=arg1.clone() as scaleform.clik.events.InputEvent;
            var loc5:*;
            if (!(loc5 = loc3.dispatchEvent(loc4)) || loc4.handled) 
            {
                return;
            }
            if (arg1.details.value == scaleform.clik.constants.InputValue.KEY_UP) 
            {
                return;
            }
            var loc6:*=arg1.details.navEquivalent;
            var loc7:*=arg1.details.code;
            var loc8:*=this.currentFocusLookup.getValue(loc2) as flash.display.InteractiveObject;
            var loc9:*=this.actualFocusLookup.getValue(loc2) as flash.display.InteractiveObject;
            var loc10:*=this.getSystemFocus(loc2);
            if (loc9 is flash.text.TextField && loc9 == loc8 && this.handleTextFieldInput(loc7, loc6, loc1)) 
            {
                return;
            }
            if (loc9 is flash.text.TextField && this.handleTextFieldInput(loc7, loc6, loc1)) 
            {
                return;
            }
            if (loc6 == null) 
            {
                return;
            }
            var loc11:*=loc6 == scaleform.clik.constants.NavigationCode.LEFT || loc6 == scaleform.clik.constants.NavigationCode.RIGHT;
            var loc12:*=loc6 == scaleform.clik.constants.NavigationCode.UP || scaleform.clik.constants.NavigationCode.DOWN;
            if (loc8 == null) 
            {
                if (loc10 && loc10 is scaleform.clik.core.UIComponent) 
                {
                    loc8 = loc10 as scaleform.clik.core.UIComponent;
                }
            }
            if (loc8 == null) 
            {
                if (loc9 && loc9 is scaleform.clik.core.UIComponent) 
                {
                    loc8 = loc9 as scaleform.clik.core.UIComponent;
                }
            }
            if (loc8 == null) 
            {
                return;
            }
            var loc13:*=loc8.parent;
            var loc14:*=scaleform.clik.constants.FocusMode.DEFAULT;
            if (loc11 || loc12) 
            {
                loc16 = loc11 ? scaleform.clik.constants.FocusMode.HORIZONTAL : scaleform.clik.constants.FocusMode.VERTICAL;
                while (loc13 != null) 
                {
                    if (loc16 in loc13) 
                    {
                        if (!((loc14 = loc13[loc16]) == null) && !(loc14 == scaleform.clik.constants.FocusMode.DEFAULT)) 
                        {
                            break;
                        }
                        loc13 = loc13.parent;
                        continue;
                    }
                    break;
                }
            }
            else 
            {
                loc13 = null;
            }
            if (loc9 is flash.text.TextField && loc9.parent == loc8) 
            {
                loc8 = this.getSystemFocus(loc1);
            }
            var loc15:*;
            if ((loc15 = scaleform.gfx.FocusManager.findFocus(loc6, null, loc14 == scaleform.clik.constants.FocusMode.LOOP, loc8, false, loc1)) != null) 
            {
                this.setFocus(loc15, loc2);
            }
            return;
        }

        protected function handleMouseFocusChange(arg1:flash.events.FocusEvent):void
        {
            this.handleFocusChange(arg1.relatedObject as flash.display.InteractiveObject, arg1.target as flash.display.InteractiveObject, arg1);
            return;
        }

        protected function handleFocusChange(arg1:flash.display.InteractiveObject, arg2:flash.display.InteractiveObject, arg3:flash.events.FocusEvent):void
        {
            var loc4:*=null;
            if (this.mouseDown && arg2 is flash.text.TextField) 
            {
                arg3.preventDefault();
                return;
            }
            if (scaleform.clik.core.CLIK.disableDynamicTextFieldFocus && arg2 is flash.text.TextField) 
            {
                if ((loc4 = arg2 as flash.text.TextField).type == "dynamic") 
                {
                    arg3.stopImmediatePropagation();
                    arg3.stopPropagation();
                    arg3.preventDefault();
                    return;
                }
            }
            if (arg2 is scaleform.clik.core.UIComponent) 
            {
                arg3.preventDefault();
            }
            if (arg1 is flash.text.TextField && arg2 == null && scaleform.clik.core.CLIK.disableTextFieldToNullFocusMoves) 
            {
                arg3.preventDefault();
                return;
            }
            var loc1:*;
            var loc2:*=(loc1 = arg3 as scaleform.gfx.FocusEventEx) != null ? loc1.controllerIdx : 0;
            var loc3:*=scaleform.gfx.FocusManager.getControllerFocusGroup(loc2);
            this.actualFocusLookup.setValue(loc3, arg2);
            this.setFocus(arg2, loc3, arg3.type == flash.events.FocusEvent.MOUSE_FOCUS_CHANGE);
            return;
        }

        protected function updateActualFocus(arg1:flash.events.FocusEvent):void
        {
            var loc1:*=null;
            var loc2:*=null;
            if (arg1.type != flash.events.FocusEvent.FOCUS_IN) 
            {
                loc1 = arg1.target as flash.display.InteractiveObject;
                loc2 = arg1.relatedObject as flash.display.InteractiveObject;
            }
            else 
            {
                loc1 = arg1.relatedObject as flash.display.InteractiveObject;
                loc2 = arg1.target as flash.display.InteractiveObject;
            }
            if (arg1.type == flash.events.FocusEvent.FOCUS_OUT) 
            {
                if (this.preventStageFocusChanges) 
                {
                    arg1.stopImmediatePropagation();
                    arg1.stopPropagation();
                }
            }
            var loc3:*;
            var loc4:*=(loc3 = arg1 as scaleform.gfx.FocusEventEx) != null ? loc3.controllerIdx : 0;
            var loc5:*=scaleform.gfx.FocusManager.getControllerFocusGroup(loc4);
            this.actualFocusLookup.setValue(loc5, loc2);
            var loc6:*=this.currentFocusLookup.getValue(loc5) as flash.display.InteractiveObject;
            if (!(loc2 == null) && loc2 is flash.text.TextField && !(loc2.parent == null) && loc6 == loc2.parent && loc6 == loc1) 
            {
                return;
            }
            var loc7:*=loc2 is flash.text.TextField;
            var loc8:*=loc6 is scaleform.clik.core.UIComponent;
            if (loc2 != loc6) 
            {
                if (!(loc7 && loc8) || loc2 == null) 
                {
                    if (!this.preventStageFocusChanges || loc7) 
                    {
                        this.setFocus(loc2, loc5);
                    }
                }
            }
            return;
        }

        protected function handleTextFieldInput(arg1:Number, arg2:String, arg3:uint):Boolean
        {
            var loc1:*=null;
            var loc4:*=null;
            if ((loc1 = this.actualFocusLookup.getValue(arg3) as flash.text.TextField) == null) 
            {
                return false;
            }
            var loc2:*=loc1.caretIndex;
            var loc3:*=0;
            var loc5:*=arg2;
            switch (loc5) 
            {
                case scaleform.clik.constants.NavigationCode.UP:
                {
                    if (!loc1.multiline) 
                    {
                        return false;
                    }
                }
                case scaleform.clik.constants.NavigationCode.LEFT:
                {
                    return true;
                }
                case scaleform.clik.constants.NavigationCode.DOWN:
                {
                    if (!loc1.multiline) 
                    {
                        return false;
                    }
                }
                case scaleform.clik.constants.NavigationCode.RIGHT:
                {
                    return true;
                }
            }
            loc5 = arg1;
            switch (loc5) 
            {
                case flash.ui.Keyboard.ENTER:
                {
                    if (loc4 = loc1.parent as scaleform.clik.controls.TextInput) 
                    {
                        loc4.playSound(scaleform.clik.constants.SoundEvent.ENTER);
                        loc4.dispatchEvent(new flash.events.Event(scaleform.clik.events.TextInputEvent.ENTER));
                    }
                    return !(loc4 == null);
                }
            }
            return false;
        }

        public static function getInstance():scaleform.clik.managers.FocusHandler
        {
            if (instance == null) 
            {
                instance = new FocusHandler();
            }
            return instance;
        }

        public static function init(arg1:flash.display.Stage, arg2:scaleform.clik.core.UIComponent):void
        {
            if (initialized) 
            {
                return;
            }
            var loc1:*=scaleform.clik.managers.FocusHandler.getInstance();
            loc1.stage = arg1;
            scaleform.gfx.FocusManager.alwaysEnableArrowKeys = true;
            scaleform.gfx.FocusManager.disableFocusKeys = false;
            initialized = true;
            return;
        }

        
        {
            initialized = false;
        }

        protected var _stage:flash.display.Stage;

        protected var currentFocusLookup:scaleform.clik.utils.WeakRefDictionary;

        protected var actualFocusLookup:scaleform.clik.utils.WeakRefDictionary;

        protected var preventStageFocusChanges:Boolean=false;

        protected var mouseDown:Boolean=false;

        protected static var initialized:Boolean=false;

        public static var instance:scaleform.clik.managers.FocusHandler;
    }
}
