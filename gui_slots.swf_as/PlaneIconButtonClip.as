﻿package 
{
    import lesta.dialogs.battle_window.markers.container.views.planes.*;
    import monstrofil.global.GuiSlotsConfig;
    import lesta.constants.PlayerRelation;
    import flash.geom.ColorTransform;
    import monstrofil.global.GlobalData;
    import lesta.cpp.Translator;
    
    public dynamic class PlaneIconButtonClip extends lesta.dialogs.battle_window.markers.container.views.planes.PlaneMarker
    {
        public function PlaneIconButtonClip()
        {
            super();
            addFrameScript(0, this.frame1, 9, this.frame10, 19, this.frame20, 29, this.frame30, 39, this.frame40, 40, this.frame41, 49, this.frame50, 59, this.frame60, 69, this.frame70, 79, this.frame80);
            return;
        }

        internal function frame1():*
        {
            return;
        }

        internal function frame10():*
        {
            stop();
            return;
        }

        internal function frame20():*
        {
            stop();
            return;
        }

        internal function frame30():*
        {
            stop();
            return;
        }

        internal function frame40():*
        {
            stop();
            return;
        }

        internal function frame41():*
        {
            return;
        }

        internal function frame50():*
        {
            stop();
            return;
        }

        internal function frame60():*
        {
            stop();
            return;
        }

        internal function frame70():*
        {
            stop();
            return;
        }

        internal function frame80():*
        {
            stop();
            return;
        }
    }
}
