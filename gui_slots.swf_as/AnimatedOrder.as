package 
{
    import lesta.controls.*;
    
    public dynamic class AnimatedOrder extends lesta.controls.AnimatedStateClip
    {
        public function AnimatedOrder()
        {
            super();
            addFrameScript(0, this.frame1, 9, this.frame10);
            return;
        }

        internal function frame1():*
        {
            stop();
            return;
        }

        internal function frame10():*
        {
            stop();
            return;
        }
    }
}
