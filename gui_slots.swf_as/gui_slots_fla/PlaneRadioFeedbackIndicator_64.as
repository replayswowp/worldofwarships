package gui_slots_fla 
{
    import flash.display.*;
    
    public dynamic class PlaneRadioFeedbackIndicator_64 extends flash.display.MovieClip
    {
        public function PlaneRadioFeedbackIndicator_64()
        {
            super();
            addFrameScript(0, this.frame1, 15, this.frame16);
            return;
        }

        function frame1():*
        {
            stop();
            return;
        }

        function frame16():*
        {
            gotoAndPlay("cycle");
            return;
        }
    }
}
