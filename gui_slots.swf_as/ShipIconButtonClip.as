﻿package 
{
    import lesta.dialogs.battle_window.markers.container.views.ships.*;
    import lesta.controls.Image;
    import monstrofil.global.GuiSlotsConfig;
    import flash.events.Event;
    import flash.geom.ColorTransform;
    import lesta.constants.PlayerRelation;
    import monstrofil.global.GlobalData;
    
    public dynamic class ShipIconButtonClip extends lesta.dialogs.battle_window.markers.container.views.ships.ShipMarker
    {
		var torpedo_icon:Image = new Image();
		var artilery_icon:Image = new Image();
        public function ShipIconButtonClip()
        {
            super();
			torpedo_icon.init();
			artilery_icon.init();
			
			if(!GuiSlotsConfig.instance.inProgress){
				GuiSlotsConfig.instance.loadConfig();
				GuiSlotsConfig.instance.inProgress = true;
			}
			
			this.icon.addChild(torpedo_icon);
			this.icon.addChild(artilery_icon);
			
			torpedo_icon.src = "../markers_icons/torpedo.png";
			artilery_icon.src = "../markers_icons/rifle.png";
			
            addFrameScript(0, this.frame1, 9, this.frame10, 19, this.frame20, 29, this.frame30, 39, this.frame40, 40, this.frame41, 49, this.frame50, 59, this.frame60, 69, this.frame70, 79, this.frame80);
            return;
        }
		
		protected override function localUpdate():void{
			super.localUpdate();
			 
			torpedo_icon.visible = this._player.shipParams.listTorpedoes != null && this._player.shipParams.torpedoes_max_dist >= this._player.distanceToShip;
			artilery_icon.visible = this._player.shipParams.artillery_max_dist >= this._player.distanceToShip;
			
			torpedo_icon.x = torpedo_icon.visible ? -16: 0;
			artilery_icon.x = torpedo_icon.visible ? 0: -8;
			
			torpedo_icon.width = torpedo_icon.height = artilery_icon.width = artilery_icon.height = 16;
			
			torpedo_icon.y = artilery_icon.y = -40;
			
			
			if(GuiSlotsConfig.instance.ConfigLoaded){
				if(this._player.relation == PlayerRelation.ENEMY && GuiSlotsConfig.instance.xml.alternative_colors == "true"){
					var color:ColorTransform = GlobalData.instance.activeColorTransformDefault;
					var c:uint = GuiSlotsConfig.instance.xml.alternative_enemy;
					color.color = c;
					
					this.icon.itemContainer.transform.colorTransform = color;
				}
			}
		}
		

        internal function frame1():*
        {
            return;
        }

        internal function frame10():*
        {
            stop();
            return;
        }

        internal function frame20():*
        {
            stop();
            return;
        }

        internal function frame30():*
        {
            stop();
            return;
        }

        internal function frame40():*
        {
            stop();
            return;
        }

        internal function frame41():*
        {
            return;
        }

        internal function frame50():*
        {
            stop();
            return;
        }

        internal function frame60():*
        {
            stop();
            return;
        }

        internal function frame70():*
        {
            stop();
            return;
        }

        internal function frame80():*
        {
            stop();
            return;
        }
    }
}
