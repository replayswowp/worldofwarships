﻿package monstrofil.global
{
	import flash.filters.DropShadowFilter;
	import flash.text.TextFormat;
	import flash.geom.ColorTransform;
	import flash.text.Font;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;

	public class MinimapConfig
	{
        public function MinimapConfig()
        {
            if (_instance != null)
            {
                throw new Error("GlobalData can only be accessed through GlobalData.instance");
            }
        }

        public static function get instance():MinimapConfig
        {
            return _instance;
        }
		
		public function loadConfig():void{
			var configLoader:URLLoader = new URLLoader();
			configLoader.addEventListener(Event.COMPLETE, onLoaded);
			configLoader.load(new URLRequest(this.configFile));
		}
		
		internal function onLoaded(e:Event):void {
			this.xml = new XML(e.target.data);
			this.configLoaded = true;
			trace("[XVM]: minimap config loaded.")
		}
		
		public var configFile:String = "minimap.xml";
		public var xml:XML = null;
		public var configLoaded:Boolean = false;
		
		public var _shipNameDist:uint = 7;
		public var _shipNameEnabled:Boolean = true;
		public var _healthWidth:uint = 1;
		public var _healthEnabled:Boolean = true;
		
		private static var _instance:MinimapConfig = new MinimapConfig();
	}
}