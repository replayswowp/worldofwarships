﻿/**
 * ...
 * @author Monstrofil
 */
package monstrofil.global
{
	import lesta.structs.Player;
	import lesta.cpp.Translator;
	import lesta.structs.PlaneInfo;
	import flash.utils.Dictionary;
	import flash.text.TextFormat;
	import flash.text.TextField;
	import flashx.textLayout.formats.TextAlign;
	
	public final class EarsConfig 
	{
		public static var romValues = ["X","IX","VIII","VII","VI","V","IV","III","II","I"];
	
		public static var earStage:int = 1;
	
		public static var PATTERN_LEFT:String = "[{level}] {ship}";
	
		public static var PATTERN_RIGHT:String = "{ship} [{level}]";
	
		public static var SCALE_LEFT = 0.7;
	
		public static var SCALE_RIGHT = -0.7;
		
		public static var xmlFileName:String = "battle_stats.xml";
		
		public static var needReload:Boolean = false;
		
		public static var health_alpha:Number = 0.9;
		
		public static var health_index:Number = 1;
		
		/* SPOTTED LAMPS */
		public static var lamp_active:Boolean = true;
		
		public static var lamp_x:Number = -220;
		
		public static var lamp_y:Number = 0;
		
		public static var visibleIcon:String = "../spotted_icons/visible.png"
		
		public static var unvisibleIcon:String = "../spotted_icons/unvisible.png"
		
		
		/* LEFT EAR */
	
		public static var left_txt_ship_visible = true;
	
		public static var left_txt_ship_width = 79;
	
		public static var left_txt_ship_x = 100;
	
		public static var left_txt_user_name_visible = true;
	
		public static var left_txt_user_name_width = 86;
	
		public static var left_txt_user_name_x = 14;
	
		public static var left_txt_frags_x = 179;
	
		public static var left_txt_frags_width = 24;
	
		public static var left_txt_frags_visible = true;
	
		public static var left_mvc_ship_position_x = 203;
		
		public static var left_mvc_ship_position_y = 4;
		
		public static var left_health_x = 14;
		
		public static var left_health_y = 18;
		
		public static var left_health_width = 200;
		
		public static var left_health_height = 2;
		
		
		/* RIGHT EAR */
		
		public static var right_txt_ship_visible = true;
	
		public static var right_txt_ship_width = 79;
	
		public static var right_txt_ship_x = -179;
	
		public static var right_txt_user_name_visible = true;
	
		public static var right_txt_user_name_width = 86;
	
		public static var right_txt_user_name_x = -101;
	
		public static var right_txt_frags_x = -199;
	
		public static var right_txt_frags_width = 24;
	
		public static var right_txt_frags_visible = true;
	
		public static var right_mvc_ship_position_x = -199;
		
		public static var right_mvc_ship_position_y = 4;
		
		public static var right_health_x = -179;
		
		public static var right_health_y = 18;
		
		public static var right_health_width = 200;
		
		public static var right_health_height = 2;
		
		public static var right_priorityMarkers_x = -192;
		
		public static var deadIcon:String = "../ship_dead_icons/{IDS}.png"
		
		public static var aliveIcon:String = "../ship_icons/{IDS}.png"
		
		public static var ownIcon:String = "../ship_own_icons/{IDS}.png"
		
		public static var xml:XML = new XML();
		
		public static function doMacros(player:lesta.structs.Player, st:String):String{
			var shipName:String = Translator.translate(player.shipIDS);
			if (shipName.length > 9) 
			{
				shipName = shipName.substr(0, 7) + "...";
			}
			st = st.replace("{IDS}",player.shipIDS.slice(4, player.shipIDS.length));
			st = st.replace("{ship}", shipName);
			st = st.replace("{level}", romValues[10 - player.shipLevel]);
			st = st.replace("{type}", player.shipTypeIndex);
			return st;
		}
		
		public static function createField(item:Object, info:Object, player:Player): void{
			var default_format:TextFormat = item.item.txt_user_name.getTextFormat();
			default_format.align = info.align;
			default_format.bold = info.bold.toString() == "true";
			var tmpField:TextField = new TextField();
			tmpField.setTextFormat(default_format);
			tmpField.filters = item.item.txt_user_name.filters;
			tmpField.x = info.x;
			tmpField.y = info.y;
			tmpField.width = info.width;
			tmpField.height = info.height;
			tmpField.visible = true;
			tmpField.name = info.name;
			
			item.addChild(tmpField);
			
			item.textFields[info.name.toString()] = tmpField;
		}
		
		public static function updateField(item:Object, info:Object, player:Player): void{
			var default_format:TextFormat = item.item.txt_user_name.getTextFormat();
			var field:TextField = item.textFields[info.name.toString()];
			field.text = item.doMacros(player, info.text);
			field.setTextFormat(default_format);
		}
	}
}