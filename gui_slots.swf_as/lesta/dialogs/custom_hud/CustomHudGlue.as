package lesta.dialogs.custom_hud 
{
    public class CustomHudGlue extends Object
    {
        public function CustomHudGlue(arg1:int=2, arg2:String="none", arg3:Number=0, arg4:int=0, arg5:Number=0, arg6:Boolean=false)
        {
            super();
            this.type = arg1;
            this.glueElementID = arg2;
            this.glueElementCoord = arg3;
            this.elementConstraintType = arg4;
            this.distanceFromGlue = arg5;
            this.active = arg6;
            return;
        }

        public function isChanged(arg1:lesta.dialogs.custom_hud.CustomHudGlue):Boolean
        {
            return !(arg1.type == this.type) || !(arg1.glueElementID == this.glueElementID) || !(arg1.elementConstraintType == this.elementConstraintType) || !(arg1.distanceFromGlue == this.distanceFromGlue);
        }

        public function toString():String
        {
            return "glueType: " + (this.type != LINE ? this.type != ELEMENT ? "border" : "element" : "line") + " elementId: " + this.glueElementID + " constrIndex: " + this.elementConstraintType + " distance " + this.distanceFromGlue;
        }

        public function toXML(arg1:String):XML
        {
            var loc1:*=new XML(arg1);
            loc1.type = this.type;
            loc1.glueElementID = this.glueElementID;
            loc1.glueElementCoord = this.glueElementCoord;
            loc1.elementConstraintType = this.elementConstraintType;
            loc1.distanceFromGlue = this.distanceFromGlue;
            return loc1;
        }

        public static const LINE:int=0;

        public static const ELEMENT:int=1;

        public static const BORDER:int=2;

        public var type:int;

        public var glueElementID:String;

        public var glueElementCoord:Number;

        public var elementConstraintType:int;

        public var distanceFromGlue:Number;

        public var active:Boolean=false;

        public var isGluedToElement:Boolean=false;
    }
}
