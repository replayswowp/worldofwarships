package lesta.dialogs.custom_hud 
{
    import flash.display.*;
    
    public dynamic class HudElementContainer extends flash.display.Sprite
    {
        public function HudElementContainer()
        {
            this.states = new Array();
            super();
            this.states[BY_GAME] = true;
            this.states[BY_DEVGUI] = true;
            return;
        }

        public function setElement(arg1:flash.display.DisplayObject):void
        {
            this.element = arg1;
            var loc1:*=arg1 as Object;
            this.name = arg1.name;
            this.size = loc1.size;
            this.background = loc1.background;
            this.pivot = loc1.pivot;
            this.update();
            return;
        }

        public function getElement():flash.display.DisplayObject
        {
            return this.element;
        }

        public function setAddedToStage(arg1:Boolean, arg2:int):void
        {
            this.states[arg2] = arg1;
            this.update();
            return;
        }

        internal function update():void
        {
            var loc1:*=this.states[0] && this.states[1];
            if (loc1) 
            {
                if (this.element.parent == null) 
                {
                    addChild(this.element);
                }
            }
            else if (this.element.parent == this) 
            {
                removeChild(this.element);
            }
            return;
        }

        public static const BY_GAME:int=0;

        public static const BY_DEVGUI:int=1;

        internal var element:flash.display.DisplayObject;

        internal var states:Array;
    }
}
