package lesta.dialogs.battle_window 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import lesta.constants.*;
    import lesta.controls.*;
    import lesta.data.*;
    import lesta.dialogs.battle_window.markers.*;
    import lesta.dialogs.battle_window.markers.container.views.base.*;
    import lesta.dialogs.battle_window_new.*;
    import lesta.structs.*;
    import lesta.utils.*;
    
    public class PlaneNavpointsController extends lesta.dialogs.battle_window_new.HudElementController
    {
        public function PlaneNavpointsController()
        {
            super();
            this.squadronsCountWasSet = false;
            return;
        }

        protected override function init():void
        {
            lesta.data.GameDelegate.addCallBack("planeNavpoints.setCountSquadrons", this, this.setCountSquadrons);
            lesta.data.GameDelegate.addCallBack("planeNavpoints.setScaleMarkers", this, this.setScaleMarkers);
            lesta.data.GameDelegate.addCallBack("battle.setTacticalMapOn", this, this.onTacticalMapOnOff);
            lesta.data.GameDelegate.addCallBack("vehicleAutopilot.toggle", this, this.onToggleVehicleAutopilot);
            lesta.data.GameDelegate.addCallBack("tacticalMap.placeTexts", this, this.onPlaceTexts);
            lesta.data.GameDelegate.addCallBack("navpoints.stopDragging", this, this.onStopDragging);
            lesta.utils.GameInfoHolder.instance.addCallback(this, lesta.utils.GameInfoHolder.UPDATE_SQUADRON_ORDER_LISTS, this.onNavpointsUpdated);
            lesta.utils.GameInfoHolder.instance.addCallback(this, lesta.utils.GameInfoHolder.UPDATE_SQUADRON_ORDER_POSITIONS, this.onPositionsUpdated);
            lesta.utils.GameInfoHolder.instance.addCallback(this, lesta.utils.GameInfoHolder.SELECTED_PLANES, this.onSelectedPlaneChanged);
            lesta.utils.GameInfoHolder.instance.addCallback(this, lesta.utils.GameInfoHolder.UPDATE_AUTOPILOT_WAYPOINTS, this.onAutopilotWaypointsChanged);
            lesta.utils.GameInfoHolder.instance.addCallback(this, lesta.utils.GameInfoHolder.UPDATE_AUTOPILOT_WAYPOINTS_POSITIONS, this.updateAutopilotWaypointsPositions);
            lesta.utils.GameInfoHolder.instance.addCallback(this, lesta.utils.GameInfoHolder.SQUADRON_DUPLICATE_INFO, this.planeNavpoints.updateDuplicateNavpoints);
            lesta.utils.GameInfoHolder.instance.addCallback(this, lesta.utils.GameInfoHolder.PLAYER_SHIP_SELECTION, this.onCarrierSelectionChanged);
            (this.autopilot_indicator.item as lesta.controls.HotKeyContainer).toggle = true;
            return;
        }

        internal function onAutopilotWaypointsChanged():void
        {
            if (this.squadronsCountWasSet) 
            {
                this.planeNavpoints.onNavpointsUpdated(this.countSquadrons);
                this.planeNavpoints.onPositionsUpdated([this.countSquadrons]);
                this.planeNavpoints.setSubplaneVisibility(this.countSquadrons, true);
            }
            return;
        }

        internal function updateAutopilotWaypointsPositions():void
        {
            if (this.squadronsCountWasSet) 
            {
                this.planeNavpoints.updateSelected([this.countSquadrons]);
            }
            return;
        }

        internal function setCountSquadrons(arg1:int):void
        {
            var loc1:*=[];
            var loc2:*=0;
            while (loc2 <= arg1) 
            {
                loc1.push(lesta.utils.GameInfoHolder.instance.mapNavpointListsBySquadron[loc2]);
                ++loc2;
            }
            this.countSquadrons = arg1;
            this.planeNavpoints.setDataSources(loc1);
            this.squadronsCountWasSet = true;
            return;
        }

        internal function setScaleMarkers(arg1:Number):void
        {
            this.planeNavpoints.setScaleMarkers(arg1);
            return;
        }

        internal function onNavpointsUpdated(arg1:int):void
        {
            this.attachNavpointToMarker(arg1);
            if (gameInfoHolder.listSelectedPlanes.indexOf(arg1) != -1) 
            {
                this.planeNavpoints.onNavpointsUpdated(arg1);
            }
            return;
        }

        internal function onPositionsUpdated():void
        {
            var loc1:*=lesta.utils.GameInfoHolder.instance.listSelectedPlanes;
            var loc2:*=0;
            var loc3:*=loc1.length;
            while (loc2 < loc3) 
            {
                this.attachNavpointPositionToMarker(loc1[loc2]);
                ++loc2;
            }
            this.planeNavpoints.onPositionsUpdated(loc1);
            return;
        }

        internal function onSelectedPlaneChanged():void
        {
            var loc1:*=lesta.utils.GameInfoHolder.instance.listSelectedPlanes;
            this.planeNavpoints.updateSelected(loc1);
            var loc2:*=0;
            while (loc2 < this.countSquadrons) 
            {
                this.planeNavpoints.setSubplaneVisibility(loc2, loc1.indexOf(loc2) >= 0);
                ++loc2;
            }
            return;
        }

        internal function attachNavpointToMarker(arg1:int):void
        {
            var loc5:*=null;
            var loc6:*=null;
            var loc1:*=lesta.utils.GameInfoHolder.instance.mapNavpointListsBySquadron[arg1];
            if (loc1 == null) 
            {
                return;
            }
            var loc2:*=loc1.navpoints;
            if (loc2 == null || loc2.length == 0) 
            {
                return;
            }
            var loc3:*=0;
            var loc4:*=loc2.length;
            while (loc3 < loc4) 
            {
                loc5 = loc2[loc3];
                loc6 = this.markersManager.markers.getMarker(loc5.objectType, loc5.objectId) as lesta.dialogs.battle_window.markers.container.views.base.Marker;
                loc5.objectMarker = loc6;
                ++loc3;
            }
            return;
        }

        internal function attachNavpointPositionToMarker(arg1:int):void
        {
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=null;
            var loc8:*=false;
            var loc9:*=0;
            var loc1:*=lesta.utils.GameInfoHolder.instance.mapNavpointListsBySquadron[arg1];
            if (loc1 == null) 
            {
                return;
            }
            var loc2:*=loc1.navpoints;
            if (loc2 == null || loc2.length == 0) 
            {
                return;
            }
            var loc3:*=0;
            var loc4:*=loc2.length;
            while (loc3 < loc4) 
            {
                if ((loc6 = (loc5 = loc2[loc3]).objectMarker) != null) 
                {
                    loc5.behindCamera = loc6.info.isBehindCamera;
                    if (loc6.visible) 
                    {
                        loc9 = (loc8 = !((loc7 = loc6.info as lesta.structs.PlaneInfo) == null) && loc7.relation == lesta.constants.PlayerRelation.SELF) ? NAVPOINTS_OFFSET_OWN_SQUADRONS : NAVPOINTS_OFFSET;
                        loc5.screenPos.x = loc6.icon.x + loc6.x;
                        loc5.screenPos.y = loc6.icon.y + loc6.y - loc9;
                    }
                }
                ++loc3;
            }
            return;
        }

        internal function onTacticalMapOnOff(arg1:Boolean):void
        {
            (this.autopilot_indicator.item as lesta.controls.HotKeyContainer).toggled = arg1;
            this.markersManager.isTacticalMap = arg1;
            return;
        }

        internal function onToggleVehicleAutopilot(arg1:Boolean):void
        {
            if (arg1) 
            {
                (this.autopilot_indicator as lesta.controls.AnimatedStateClip).setState("active");
            }
            else 
            {
                (this.autopilot_indicator as lesta.controls.AnimatedStateClip).setState("def");
            }
            return;
        }

        internal function onPlaceTexts(arg1:Array):void
        {
            this.tacticalMap.onPlaceTexts(arg1);
            return;
        }

        internal function onStopDragging(arg1:int):void
        {
            this.planeNavpoints.stopDragging(arg1);
            return;
        }

        internal function onCarrierSelectionChanged():void
        {
            if (this.squadronsCountWasSet) 
            {
                this.planeNavpoints.setTransparency(this.countSquadrons, gameInfoHolder.playerShipSelected ? 0 : AUTOPILOT_LINES_TRANSPARENCY);
                this.planeNavpoints.onPositionsUpdated([this.countSquadrons]);
            }
            return;
        }

        public static const NAVPOINTS_OFFSET:Number=18;

        public static const NAVPOINTS_OFFSET_OWN_SQUADRONS:Number=45;

        public static const AUTOPILOT_LINES_TRANSPARENCY:Number=0.5;

        public var planeNavpoints:lesta.dialogs.battle_window.PlaneNavpoints;

        public var markersManager:lesta.dialogs.battle_window.markers.MarkersManager;

        public var autopilot_indicator:flash.display.MovieClip;

        public var tacticalMap:lesta.dialogs.battle_window.TacticalMap;

        internal var countSquadrons:int;

        internal var squadronsCountWasSet:Boolean;
    }
}
