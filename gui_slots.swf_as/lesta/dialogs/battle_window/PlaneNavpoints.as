package lesta.dialogs.battle_window 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import flash.utils.*;
    import lesta.constants.*;
    import lesta.data.*;
    import lesta.dialogs.battle_window.constants.*;
    import lesta.managers.*;
    import lesta.structs.*;
    import lesta.utils.*;
    import scaleform.gfx.*;
    
    public class PlaneNavpoints extends flash.display.MovieClip
    {
        public function PlaneNavpoints()
        {
            this.spriteNavpoints = new flash.display.Sprite();
            this.mapNavpointsById = new flash.utils.Dictionary();
            this.mapNavpointsMarkers = new Vector.<Array>();
            this.mapLinesSprites = new Vector.<lesta.dialogs.battle_window.CacheDottedLinesSprite>();
            this.transparencyMods = [];
            this.stageMinMinPos = new IntPoint();
            this.stageMaxMaxPos = new IntPoint();
            this.stageMinMaxPos = new IntPoint();
            this.stageMaxMinPos = new IntPoint();
            this.stageLines = [{"s":this.stageMinMinPos, "e":this.stageMinMaxPos}, {"s":this.stageMinMaxPos, "e":this.stageMaxMaxPos}, {"s":this.stageMaxMaxPos, "e":this.stageMaxMinPos}, {"s":this.stageMaxMinPos, "e":this.stageMinMinPos}];
            super();
            this.NavpointAnimation = flash.utils.getDefinitionByName("NavpointAnimation") as Class;
            this.settings = lesta.utils.GameInfoHolder.instance.navpointsSettings;
            addChild(this.spriteNavpoints);
            return;
        }

        public function setDataSources(arg1:Array):void
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                loc2 = new lesta.dialogs.battle_window.CacheDottedLinesSprite();
                scaleform.gfx.InteractiveObjectEx.setHitTestDisable(loc2, true);
                loc2.navpointList = arg1[loc1];
                this.mapLinesSprites.push(loc2);
                loc2.visible = false;
                addChildAt(loc2, 0);
                this.mapNavpointsMarkers.push(new Array());
                ++loc1;
            }
            return;
        }

        internal function updateNavpoints(arg1:int):void
        {
            var loc6:*=null;
            var loc7:*=null;
            var loc8:*=null;
            var loc1:*=lesta.utils.GameInfoHolder.instance.mapNavpointListsBySquadron[arg1];
            if (loc1 == null) 
            {
                trace("[Flash]PlaneNavpoints.updateNavpoints : invalid id squadron ", arg1);
                return;
            }
            var loc2:*=this.mapLinesSprites[arg1];
            loc2.visible = loc1.isLinesDraw;
            var loc3:*=this.mapNavpointsMarkers[arg1].slice();
            var loc4:*=0;
            while (loc4 < loc3.length) 
            {
                (loc6 = loc3[loc4]).toDelete = true;
                ++loc4;
            }
            var loc5:*;
            (loc5 = this.mapNavpointsMarkers[arg1]).length = 0;
            loc4 = 0;
            while (loc4 < loc1.navpoints.length) 
            {
                loc7 = loc1.navpoints[loc4];
                if ((loc6 = this.mapNavpointsById[loc7.id]) == null) 
                {
                    loc8 = new this.NavpointAnimation();
                    (loc6 = new lesta.dialogs.battle_window.Navpoint(loc8)).callbackOver = this.onNavpointOverStateChanged;
                    loc6.setNavpointInfo(arg1, loc7);
                    this.mapNavpointsById[loc7.id] = loc6;
                    this.spriteNavpoints.addChild(loc6.clip);
                }
                loc5.push(loc6);
                loc6.toDelete = false;
                loc6.update();
                ++loc4;
            }
            loc4 = 0;
            while (loc4 < loc3.length) 
            {
                if ((loc6 = loc3[loc4]).toDelete) 
                {
                    delete this.mapNavpointsById[loc6.navpointInfo.id];
                    this.spriteNavpoints.removeChild(loc6.clip);
                    loc6.fini();
                }
                ++loc4;
            }
            return;
        }

        internal function calcCoordsForLine(arg1:flash.geom.Point, arg2:flash.geom.Point, arg3:flash.display.Sprite, arg4:Object):Boolean
        {
            var loc7:*=null;
            var loc8:*=null;
            var loc1:*=arg1.x >= this.stageMinMinPos.x && arg1.x <= this.stageMaxMaxPos.x && arg1.y >= this.stageMinMinPos.y && arg1.y <= this.stageMaxMaxPos.y;
            var loc2:*=arg2.x >= this.stageMinMinPos.x && arg2.x <= this.stageMaxMaxPos.x && arg2.y >= this.stageMinMinPos.y && arg2.y <= this.stageMaxMaxPos.y;
            var loc3:*=arg1.y < this.stageMinMinPos.y && arg2.y < this.stageMinMinPos.y || arg1.y > this.stageMaxMaxPos.y && arg2.y > this.stageMaxMaxPos.y;
            var loc4:*=arg1.x < this.stageMinMinPos.x && arg2.x < this.stageMinMinPos.x || arg1.x > this.stageMaxMaxPos.x && arg2.x > this.stageMaxMaxPos.x;
            arg4[0] = arg1.x;
            arg4[1] = arg1.y;
            arg4[2] = arg2.x;
            arg4[3] = arg2.y;
            if (loc1 && loc2) 
            {
                return true;
            }
            if (loc3 || loc4) 
            {
                return false;
            }
            var loc5:*=new flash.geom.Point();
            var loc6:*=0;
            while (loc6 < this.stageLines.length) 
            {
                loc7 = new flash.geom.Point(this.stageLines[loc6].s.x, this.stageLines[loc6].s.y);
                loc8 = new flash.geom.Point(this.stageLines[loc6].e.x, this.stageLines[loc6].e.y);
                if (lesta.utils.Geometry.getLinesIntersection(arg1, arg2, loc7, loc8, loc5)) 
                {
                    if (loc1) 
                    {
                        if (!loc2) 
                        {
                            arg4[2] = loc5.x;
                            arg4[3] = loc5.y;
                            loc2 = true;
                        }
                    }
                    else 
                    {
                        arg4[0] = loc5.x;
                        arg4[1] = loc5.y;
                        loc1 = true;
                    }
                }
                ++loc6;
            }
            return loc1 && loc2;
        }

        internal function redrawLines(arg1:int):void
        {
            var loc9:*=null;
            var loc10:*=null;
            var loc11:*=null;
            var loc12:*=null;
            var loc13:*=false;
            var loc14:*=NaN;
            var loc15:*=NaN;
            var loc1:*=this.mapLinesSprites[arg1];
            var loc2:*=this.mapNavpointsMarkers[arg1];
            loc1.setLinesCount(Math.max(0, (loc2.length - 1)));
            if (loc2.length == 0) 
            {
                return;
            }
            var loc3:*=[];
            var loc4:*=loc2[0];
            var loc5:*=loc1.navpointList.isLinesDraw;
            var loc6:*=this.transparencyMods[arg1] ? this.transparencyMods[arg1] : 1;
            var loc7:*=0;
            var loc8:*=loc2.length;
            while (loc7 < loc8) 
            {
                loc10 = (loc9 = loc2[loc7]).clip;
                loc11 = loc9.navpointInfo;
                loc12 = loc4.navpointInfo;
                if (loc7 > 0 && loc5) 
                {
                    if (loc13 = (!(loc11.type == lesta.dialogs.battle_window.constants.NavpointTypes.ATTACK) || loc11.planeType == lesta.constants.PlaneTypes.INT_FIGHTER) && !(loc11.type == lesta.dialogs.battle_window.constants.NavpointTypes.ATTACK_GROUND) && !(loc12.behindCamera && loc11.behindCamera) && this.calcCoordsForLine(loc12.screenPos, loc11.screenPos, loc1, loc3)) 
                    {
                        loc1.drawDottedLine((loc7 - 1), loc3[0], loc3[1], loc3[2], loc3[3], loc11.color, loc11.alpha, this.settings.lineThickness, this.settings.dotLength, this.settings.spaceLength);
                    }
                    else 
                    {
                        loc1.setLineInvisible((loc7 - 1));
                    }
                    loc1.setLineParams((loc7 - 1), loc11.color, loc6 * loc11.alpha);
                }
                loc10.x = loc11.screenPos.x + loc11.xOffset + (loc11.objectMarker && loc11.objectMarker.priority ? (NAVPOINT_WIDTH + NAVPOINT_GAP) / 2 : 0);
                loc10.y = loc11.screenPos.y;
                var loc16:*;
                loc10.scaleY = loc16 = this.scaleMarkers;
                loc10.scaleX = loc16;
                loc10.visible = !loc11.behindCamera && !loc11.isDuplicated;
                if (loc12.type == lesta.dialogs.battle_window.constants.NavpointTypes.ATTACK_RADIUS && !loc11.isDragging) 
                {
                    loc14 = loc11.goalScreenPos.x;
                    loc15 = loc11.goalScreenPos.y;
                    loc4.clipAsMovieClip.navpoints.dirAttack.rotation = Math.atan2(loc4.clip.y - loc15, loc4.clip.x - loc14) / Math.PI * 180;
                }
                loc4 = loc9;
                ++loc7;
            }
            return;
        }

        public function onNavpointOverStateChanged(arg1:lesta.dialogs.battle_window.Navpoint, arg2:Boolean=false):void
        {
            if (arg1.isOver && !arg1.hidden) 
            {
                if (arg1.navpointInfo.isDraggable) 
                {
                    this.currentNavpointOver = arg1.navpointInfo.id;
                }
            }
            else if (this.currentNavpointOver == arg1.navpointInfo.id) 
            {
                this.currentNavpointOver = -1;
            }
            if (arg2 && this.currentNavpointOver == -1) 
            {
                lesta.data.GameDelegate.call("battle.checkCursorOnGui", []);
            }
            lesta.managers.CursorManager.setCursorFlag(lesta.managers.CursorFlags.CURSOR_OVER_NAVPOINT, !(this.currentNavpointOver == -1));
            return;
        }

        public function setTransparency(arg1:int, arg2:Number):void
        {
            this.transparencyMods[arg1] = arg2;
            return;
        }

        public function stopDragging(arg1:int):void
        {
            var loc1:*=this.mapNavpointsById[arg1];
            if (loc1) 
            {
                loc1.stopDragging();
            }
            else 
            {
                trace(new Error("ERROR: [WWSD-27061] - Attempt to stop dragging a deleted navpoint (id = " + arg1 + ")").getStackTrace());
            }
            return;
        }

        public function updateDuplicateNavpoints():void
        {
            var loc3:*=0;
            var loc4:*=null;
            var loc5:*=0;
            var loc6:*=null;
            var loc7:*=null;
            var loc8:*=null;
            var loc9:*=0;
            var loc10:*=0;
            var loc11:*=NaN;
            var loc1:*=lesta.utils.GameInfoHolder.instance.listSelectedPlanes;
            var loc2:*=0;
            while (loc2 < loc1.length) 
            {
                loc3 = loc1[loc2];
                loc4 = this.mapNavpointsMarkers[loc3];
                loc5 = 0;
                while (loc5 < loc4.length) 
                {
                    loc7 = (loc6 = loc4[loc5]).clip;
                    loc9 = (loc8 = loc6.navpointInfo).countDuplicated;
                    loc10 = loc8.indexOnObject;
                    if (loc8.objectMarker) 
                    {
                        loc8.objectMarker.navpointsAmount = loc9;
                    }
                    if (loc8.indexOnObject >= 0) 
                    {
                        loc11 = NAVPOINT_WIDTH * loc9 + NAVPOINT_GAP * (loc9 - 1);
                        loc8.xOffset = NAVPOINT_WIDTH * 0.5 + (NAVPOINT_WIDTH + NAVPOINT_GAP) * loc10 - loc11 * 0.5;
                    }
                    else 
                    {
                        loc8.xOffset = 0;
                    }
                    ++loc5;
                }
                ++loc2;
            }
            return;
        }

        public function updateSelected(arg1:Array):void
        {
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                this.redrawLines(arg1[loc1]);
                this.updateNavpoints(arg1[loc1]);
                ++loc1;
            }
            return;
        }

        public function setSubplaneVisibility(arg1:int, arg2:Boolean):void
        {
            var index:int;
            var visibility:Boolean;
            var markersToShow:Array;
            var delay:Number;
            var t:flash.utils.Timer;
            var showFunction:Function;
            var hideFunction:Function;

            var loc1:*;
            markersToShow = null;
            delay = NaN;
            t = null;
            showFunction = null;
            hideFunction = null;
            index = arg1;
            visibility = arg2;
            this.mapLinesSprites[index].visible = visibility;
            markersToShow = this.mapNavpointsMarkers[index].slice();
            if (markersToShow.length > 0) 
            {
                delay = visibility ? this.appearDelay : this.disappearDelay;
                t = new flash.utils.Timer(delay, markersToShow.length);
                showFunction = function (arg1:flash.events.TimerEvent):void
                {
                    var loc1:*=markersToShow.shift();
                    if (loc1.clip) 
                    {
                        loc1.showViaSelection();
                    }
                    return;
                }
                hideFunction = function (arg1:flash.events.TimerEvent):void
                {
                    var loc1:*=markersToShow.shift();
                    if (loc1.clip) 
                    {
                        loc1.hide();
                    }
                    return;
                }
                t.addEventListener(flash.events.TimerEvent.TIMER, visibility ? showFunction : hideFunction);
                t.start();
            }
            return;
        }

        public function onPositionsUpdated(arg1:Array):void
        {
            var loc1:*=0;
            var loc2:*=arg1.length;
            while (loc1 < loc2) 
            {
                this.redrawLines(arg1[loc1]);
                ++loc1;
            }
            return;
        }

        public function onNavpointsUpdated(arg1:int):void
        {
            var loc1:*=null;
            this.updateNavpoints(arg1);
            if (this.currentNavpointOver >= 0) 
            {
                loc1 = this.mapNavpointsById[this.currentNavpointOver];
                if (!loc1.navpointInfo.isDraggable) 
                {
                    lesta.managers.CursorManager.setCursorFlag(lesta.managers.CursorFlags.CURSOR_OVER_NAVPOINT, false);
                    this.currentNavpointOver = -1;
                }
            }
            return;
        }

        public function setStageSize(arg1:Number, arg2:Number):void
        {
            var loc1:*=0;
            var loc2:*=1;
            this.stageMinMinPos.x = arg1 * loc1;
            this.stageMinMinPos.y = arg2 * loc1;
            this.stageMaxMaxPos.x = arg1 * loc2;
            this.stageMaxMaxPos.y = arg2 * loc2;
            this.stageMinMaxPos.x = arg1 * loc1;
            this.stageMinMaxPos.y = arg2 * loc2;
            this.stageMaxMinPos.x = arg1 * loc2;
            this.stageMaxMinPos.y = arg2 * loc1;
            return;
        }

        public function setScaleMarkers(arg1:Number):void
        {
            this.scaleMarkers = arg1;
            return;
        }

        public static const NAVPOINT_WIDTH:Number=26;

        public static const NAVPOINT_GAP:Number=4;

        public var appearDelay:Number=30;

        internal var stageMaxMinPos:IntPoint;

        internal var stageMinMaxPos:IntPoint;

        internal var stageMaxMaxPos:IntPoint;

        internal var stageMinMinPos:IntPoint;

        internal var settings:lesta.structs.NavpointsSettings;

        internal var transparencyMods:Array;

        internal var mapLinesSprites:__AS3__.vec.Vector.<lesta.dialogs.battle_window.CacheDottedLinesSprite>;

        internal var mapNavpointsMarkers:__AS3__.vec.Vector.<Array>;

        internal var mapNavpointsById:flash.utils.Dictionary;

        internal var spriteNavpoints:flash.display.Sprite;

        internal var scaleMarkers:Number=1;

        internal var NavpointAnimation:Class;

        public var disappearDelay:Number=0;

        internal var stageLines:Array;

        internal var currentNavpointOver:int=-1;
    }
}


class IntPoint extends Object
{
    public function IntPoint(arg1:int=0, arg2:int=0)
    {
        super();
        this.x = arg1;
        this.y = arg2;
        return;
    }

    public var x:int=0;

    public var y:int=0;
}