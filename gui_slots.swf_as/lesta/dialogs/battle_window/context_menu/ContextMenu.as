package lesta.dialogs.battle_window.context_menu 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import lesta.managers.*;
    
    public class ContextMenu extends flash.display.MovieClip
    {
        public function ContextMenu()
        {
            super();
            return;
        }

        public function fini():void
        {
            var loc1:*=0;
            while (loc1 < this.items.length) 
            {
                this.items[loc1].fini();
                ++loc1;
            }
            return;
        }

        public function init(arg1:lesta.managers.ContextMenuManager):void
        {
            var loc2:*=null;
            var loc3:*=null;
            this.manager = arg1;
            this.items = new Vector.<lesta.dialogs.battle_window.context_menu.ContextMenuItem>();
            var loc1:*=0;
            while (loc1 < numChildren) 
            {
                loc2 = getChildAt(loc1);
                if (loc3 = loc2 as lesta.dialogs.battle_window.context_menu.ContextMenuItem) 
                {
                    loc3.init(arg1);
                    this.items.push(loc3);
                }
                ++loc1;
            }
            return;
        }

        public function open():void
        {
            var loc1:*=0;
            while (loc1 < this.items.length) 
            {
                this.items[loc1].open();
                ++loc1;
            }
            return;
        }

        internal var manager:lesta.managers.ContextMenuManager;

        internal var items:__AS3__.vec.Vector.<lesta.dialogs.battle_window.context_menu.ContextMenuItem>;
    }
}
