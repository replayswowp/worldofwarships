package lesta.dialogs.battle_window.markers.utils 
{
    public class MarkerTypes extends Object
    {
        public function MarkerTypes()
        {
            super();
            return;
        }

        public static const SHIP:int=0;

        public static const PLANE:int=1;

        public static const TORPEDO:int=2;

        public static const CAPTURE_POINTS:int=3;
    }
}
