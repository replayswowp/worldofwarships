package lesta.dialogs.battle_window.markers.config.elements 
{
    import __AS3__.vec.*;
    
    public class MarkerElementsConfig extends Object
    {
        public function MarkerElementsConfig()
        {
            super();
            this.init();
            return;
        }

        internal function init():void
        {
            this._elements = new Vector.<lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig>();
            this._elementsObject = {};
            return;
        }

        public function loadFromXML(arg1:XML):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=0;
            var loc5:*=arg1.*;
            for each (loc1 in loc5) 
            {
                loc2 = loc1.name();
                if ((loc3 = this._elementsObject[loc2]) == null) 
                {
                    loc3 = new lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig();
                    this._elementsObject[loc2] = loc3;
                    this._elements.push(loc3);
                }
                loc3.loadFromXML(loc1);
            }
            this._length = this._elements.length;
            this.updatePinnedRefs();
            return;
        }

        public function updatePinnedRefs():void
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < this._length) 
            {
                loc2 = this._elements[loc1];
                loc2.pinnedToConfig = this._elementsObject[loc2.pinnedTo];
                ++loc1;
            }
            return;
        }

        public function setElementPinnedToElement(arg1:String, arg2:String):void
        {
            var loc1:*=this._elementsObject[arg1];
            loc1.pinnedTo = arg2;
            loc1.pinnedToConfig = this._elementsObject[loc1.pinnedTo];
            return;
        }

        public function get length():int
        {
            return this._length;
        }

        public function getElementByIndex(arg1:int):lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig
        {
            return this._elements[arg1];
        }

        public function getElementByName(arg1:String):lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig
        {
            var loc1:*=this._elementsObject[arg1];
            if (loc1 == null) 
            {
                loc1 = new lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig();
                loc1.name = arg1;
                this._elements.push(loc1);
                this._elementsObject[arg1] = loc1;
                this._length = this._elements.length;
            }
            return loc1;
        }

        public function generate(arg1:lesta.dialogs.battle_window.markers.config.elements.MarkerElementsConfig, arg2:lesta.dialogs.battle_window.markers.config.elements.MarkerElementsConfig, arg3:Number):void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc1:*=arg1.length;
            var loc2:*=0;
            while (loc2 < loc1) 
            {
                loc3 = arg1.getElementByIndex(loc2);
                if (loc4 = arg2.getElementByName(loc3.name)) 
                {
                    (loc5 = new lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig()).generate(loc3, loc4, arg3);
                    this._elements.push(loc5);
                    this._elementsObject[loc5.name] = loc5;
                    var loc6:*;
                    var loc7:*=((loc6 = this)._length + 1);
                    loc6._length = loc7;
                }
                ++loc2;
            }
            return;
        }

        internal var _elements:__AS3__.vec.Vector.<lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig>;

        internal var _elementsObject:Object;

        internal var _length:int;
    }
}
