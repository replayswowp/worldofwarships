package lesta.dialogs.battle_window.markers.config.elements 
{
    public class MarkerElementConfig extends Object
    {
        public function MarkerElementConfig()
        {
            super();
            return;
        }

        public function copy(arg1:lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig):void
        {
            this.scaleX = arg1.scaleX;
            this.scaleY = arg1.scaleY;
            this.horizontalOffset = arg1.horizontalOffset;
            this.verticalOffset = arg1.verticalOffset;
            this.visible = arg1.visible;
            this.pinnedTo = arg1.pinnedTo;
            this.fontColor = arg1.fontColor;
            return;
        }

        public function loadFromXML(arg1:XML):void
        {
            this.name = arg1.name();
            this.scaleX = arg1.@scaleX == undefined ? 1 : arg1.@scaleX;
            this.scaleY = arg1.@scaleY == undefined ? 1 : arg1.@scaleY;
            this.horizontalOffset = arg1.@horizontalOffset == undefined ? 0 : arg1.@horizontalOffset;
            this.verticalOffset = arg1.@verticalOffset == undefined ? 0 : arg1.@verticalOffset;
            this.visible = arg1.@visible == true;
            this.pinnedTo = !(arg1.@pinnedTo == undefined) && !(arg1.@pinnedTo == "") ? arg1.@pinnedTo : null;
            this.fontColor = !(arg1.@fontColor == undefined) && !(arg1.@fontColor == "") ? uint("0x" + arg1.@fontColor) : null;
            return;
        }

        public function generate(arg1:lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig, arg2:lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig, arg3:Number):void
        {
            this.name = arg1.name;
            this.scaleX = arg1.scaleX + (arg2.scaleX - arg1.scaleX) * arg3;
            this.scaleY = arg1.scaleY + (arg2.scaleY - arg1.scaleY) * arg3;
            this.horizontalOffset = arg1.horizontalOffset + (arg2.horizontalOffset - arg1.horizontalOffset) * arg3;
            this.verticalOffset = arg1.verticalOffset + (arg2.verticalOffset - arg1.verticalOffset) * arg3;
            this.visible = arg1.visible;
            this.pinnedTo = arg1.pinnedTo;
            this.fontColor = arg1.fontColor;
            return;
        }

        public function getXML():XML
        {
            var loc1:*=new XML("<name scaleX=\"" + this.scaleX + "\" scaleY=\"" + this.scaleY + "\" horizontalOffset=\"" + this.horizontalOffset + "\" verticalOffset=\"" + this.verticalOffset + "\" visible=\"" + this.visible + "\" pinnedTo=\"" + (this.pinnedTo == null ? "" : this.pinnedTo) + "\" fontColor=\"" + this.fontColor.toString(16) + "\"/>");
            loc1.setName(this.name);
            return loc1;
        }

        public var name:String;

        public var scaleX:Number=1;

        public var scaleY:Number=1;

        public var horizontalOffset:Number=0;

        public var verticalOffset:Number=0;

        public var visible:Boolean=false;

        public var pinnedTo:String=null;

        public var fontColor:uint=0;

        public var pinnedToConfig:lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig=null;
    }
}
