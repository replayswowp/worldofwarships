package lesta.dialogs.battle_window.markers.container.views.base 
{
    import lesta.dialogs.battle_window.markers.config.*;
    import lesta.dialogs.battle_window.markers.settings.*;
    import lesta.structs.*;
    
    public class SimpleMarker extends lesta.dialogs.battle_window.markers.container.views.base.TargetAnimationMarker
    {
        public function SimpleMarker()
        {
            super();
            return;
        }

        public function init():void
        {
            return;
        }

        public override function fini():void
        {
            this._settings = null;
            this._configs = null;
            this._info = null;
            super.fini();
            return;
        }

        public function get type():Number
        {
            return this._type;
        }

        public function get targetType():int
        {
            return this._targetType;
        }

        public function get id():int
        {
            return this._info.id;
        }

        public function set configs(arg1:lesta.dialogs.battle_window.markers.config.MarkersConfig):void
        {
            this._configs = arg1;
            return;
        }

        public function get hidden():Boolean
        {
            return this._hidden;
        }

        public function updatePositions(arg1:Array, arg2:int, arg3:int, arg4:Number):void
        {
            var loc1:*=NaN;
            this._hidden = !this._settings.forceVisible && !this.getVisible();
            visible = this._info.isOnScreen && !this._hidden;
            if (!visible) 
            {
                return;
            }
            this.distanceToCamera = this._info.distanceToCamera;
            x = int(this._info.screenPos.x);
            y = int(this._info.screenPos.y);
            if (this.isClickAria && this.info.isControllable) 
            {
                if ((loc1 = (x - arg2) * (x - arg2) + (y - arg3) * (y - arg3)) <= arg4) 
                {
                    arg1.push({"type":this.type, "id":this.id, "dist":loc1});
                }
            }
            return;
        }

        public function stateUpdate():void
        {
            if (!visible) 
            {
                return;
            }
            this.localUpdate();
            this.needInitialUpdate = false;
            return;
        }

        public function set info(arg1:lesta.structs.EntityInfo):void
        {
            this._info = arg1;
            return;
        }

        public function get info():lesta.structs.EntityInfo
        {
            return this._info;
        }

        public function set settings(arg1:lesta.dialogs.battle_window.markers.settings.MarkersSettings):void
        {
            this._settings = arg1;
            return;
        }

        protected function getVisible():Boolean
        {
            return true;
        }

        protected function localUpdate():void
        {
            return;
        }

        protected var _settings:lesta.dialogs.battle_window.markers.settings.MarkersSettings;

        protected var _configs:lesta.dialogs.battle_window.markers.config.MarkersConfig;

        protected var _info:lesta.structs.EntityInfo;

        protected var _type:int;

        protected var _targetType:int=100;

        protected var _hidden:Boolean=true;

        public var distanceToCamera:Number=0;

        public var forDelete:Boolean=false;

        public var isClickAria:Boolean=false;

        public var needInitialUpdate:Boolean=true;
    }
}
