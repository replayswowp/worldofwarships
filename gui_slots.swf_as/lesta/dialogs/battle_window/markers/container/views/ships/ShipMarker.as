package lesta.dialogs.battle_window.markers.container.views.ships 
{
    import flash.display.*;
    import lesta.constants.*;
    import lesta.dialogs.battle_window.markers.container.views.*;
    import lesta.dialogs.battle_window.markers.container.views.base.*;
    import lesta.dialogs.battle_window.markers.utils.*;
    import lesta.structs.*;
    
    public class ShipMarker extends lesta.dialogs.battle_window.markers.container.views.base.Marker
    {
        public function ShipMarker()
        {
            super();
            isClickAria = true;
            _type = lesta.dialogs.battle_window.markers.utils.MarkerTypes.SHIP;
            _targetType = lesta.structs.TargetTypes.SHIP;
            this.updateAimIcon();
            this.initTargetDelegator();
            return;
        }

        internal function updateShipIcon():void
        {
            var loc1:*=this._player.relation == lesta.constants.PlayerRelation.SELF && _settings.isTacticalMap;
            var loc2:*=loc1 ? TACTICAL_VIEW_FRAME : this._player.shipTypeIndex + 1;
            if (loc2 == this.shipIcon.currentFrame) 
            {
                this.shipIcon.stop();
            }
            else 
            {
                this.shipIcon.gotoAndStop(loc2);
            }
            this.shipIconType = this.shipIcon.getChildAt(0) as flash.display.MovieClip;
            this.updateShipIconType();
            return;
        }

        protected override function getVisible():Boolean
        {
            return this._player.isAlive && this._player.vehicleVisible && this._player.isVisible && (!(this._player.relation == lesta.constants.PlayerRelation.SELF) || this._player.shipTypeIndex == lesta.constants.ShipTypes.CARRIER || _settings.isWeaponAircraft || _settings.isTacticalMap);
        }

        public override function fini():void
        {
            this.healthBar.fini();
            this.healthBar = null;
            super.fini();
            return;
        }

        public override function set info(arg1:lesta.structs.EntityInfo):void
        {
            super.info = arg1;
            this._player = arg1 as lesta.structs.Player;
            this.updateTKStatus();
            var loc1:*=this._player.relation + 1;
            if (this.healthBar.currentFrame == loc1) 
            {
                this.healthBar.stop();
            }
            else 
            {
                this.healthBar.gotoAndStop(loc1);
            }
            this.vehicleName.label.text = this._player.shipIDS;
            this.vehicleTypeText.label.text = this._player.shipTypeIdent;
            this.vehicleLevel.label.text = this._player.shipTypeIndex.toString();
            return;
        }

        protected override function localUpdate():void
        {
            var loc1:*=NaN;
            super.localUpdate();
            if (_isSelected != this._player.isSelected) 
            {
                _isSelected = this._player.isSelected;
                this.updateShipIconType();
            }
            if (this._distanceToShipValue != this._player.distanceToShip) 
            {
                this._distanceToShipValue = this._player.distanceToShip;
                this.distance.label.text = this._distanceToShipValue.toFixed(1) + kmPostfix;
            }
            if (!(this._healthValue == this._player.health) || !(this._maxHealthValue == this._player.maxHealth)) 
            {
                this._healthValue = this._player.health;
                this._maxHealthValue = this._player.maxHealth;
                this.healthBar.maximum = this._maxHealthValue;
                this.healthBar.setValue(this._player.health, this._player.damage + this._player.mirrorDamage, this._player.selfDamage);
                this.healthPercent.label.text = Math.ceil(this._healthValue * 100 / this._maxHealthValue) + "%";
                this.healthAbsolute.label.text = Math.ceil(this._healthValue) + "/" + this._maxHealthValue;
            }
            if (_settings.isTacticalMap) 
            {
                loc1 = this._player.worldYaw * DEGREES_IN_RADIANS;
                if (this._player.relation != lesta.constants.PlayerRelation.ENEMY) 
                {
                    icon.rotation = loc1 - 90;
                }
                else 
                {
                    icon.rotation = loc1 + 90;
                }
            }
            else 
            {
                icon.rotation = 0;
            }
            return;
        }

        public function updateTKStatus():void
        {
            this.updateItemContainer();
            if (this._player.tkStatus && !(this._player.relation == lesta.constants.PlayerRelation.ENEMY)) 
            {
                this.playerName.label.htmlText = "<FONT color=\"#" + lesta.constants.Common.TEAMKILLER_COLOR.toString(16) + "\">" + this._player.name + "</FONT>";
            }
            else 
            {
                this.playerName.label.text = this._player.name;
            }
            return;
        }

        public function updateTacticalState():void
        {
            this.updateShipIcon();
            return;
        }

        internal function updateAimIcon():void
        {
            if (this._isTarget) 
            {
                this.aimIcon.gotoAndPlay("in");
            }
            else 
            {
                this.aimIcon.gotoAndPlay("out");
            }
            return;
        }

        internal function updateBuoyancy():void
        {
            if (this._buoyancyValue == this._player.buoyancy && this._maxBuoyancyValue == this._player.maxBuoyancy) 
            {
                return;
            }
            this._buoyancyValue = this._player.buoyancy;
            this._maxBuoyancyValue = this._player.maxBuoyancy;
            this.buoyancyBar.maximum = this._maxBuoyancyValue;
            this.buoyancyBar.setValue(this._player.buoyancy, this._player.buoyancyDamage, this._player.selfBuoyancyDamage);
            this.buoyancyPercent.label.text = Math.ceil(this._buoyancyValue * 100 / this._maxBuoyancyValue) + "%";
            this.buoyancyAbsolute.label.text = Math.ceil(this._buoyancyValue) + "/" + this._maxBuoyancyValue;
            return;
        }

        public function set isTarget(arg1:Boolean):void
        {
            if (this._isTarget == arg1) 
            {
                return;
            }
            this._isTarget = arg1;
            this.updateAimIcon();
            return;
        }

        public function playHitAnimation():void
        {
            icon.gotoAndPlay(START_ANIMATION_FRAME);
            return;
        }

        protected function initTargetDelegator():void
        {
            addAnimationController(lesta.dialogs.battle_window.markers.container.views.TargetAnimations.MAIN_ANIMATION, this.alertIndicatorContainer, "QuickCommand");
            return;
        }

        internal function updateItemContainer():void
        {
            icon.itemContainer.gotoAndStop(this._player.getFrameLabel());
            this.shipIcon = icon.itemContainer.getChildByName("shipIcon");
            this.updateShipIcon();
            return;
        }

        internal function updateShipIconType():void
        {
            if (this.shipIconType) 
            {
                this.shipIconType.gotoAndStop(int(_isSelected) + 1);
            }
            return;
        }

        internal static const TACTICAL_VIEW_FRAME:int=5;

        internal static const START_ANIMATION_FRAME:int=1;

        internal static const DEGREES_IN_RADIANS:Number=180 / Math.PI;

        internal var _teamkillerStatusValue:Boolean;

        internal var _healthValue:Number;

        internal var _maxHealthValue:Number;

        public var healthAbsolute:flash.display.MovieClip;

        internal var _maxBuoyancyValue:Number;

        internal var _isTarget:Boolean;

        public var playerName:flash.display.MovieClip;

        public var healthBar:lesta.dialogs.battle_window.markers.container.views.ships.ShipMarkerPercentBar;

        internal var _buoyancyValue:Number;

        public var buoyancyBar:lesta.dialogs.battle_window.markers.container.views.ships.ShipMarkerPercentBar;

        public var healthPercent:flash.display.MovieClip;

        public var buoyancyAbsolute:flash.display.MovieClip;

        public var buoyancyPercent:flash.display.MovieClip;

        public var vehicleName:flash.display.MovieClip;

        public var vehicleTypeText:flash.display.MovieClip;

        public var vehicleLevel:flash.display.MovieClip;

        public var distance:flash.display.MovieClip;

        public var alertIndicatorContainer:flash.display.MovieClip;

        internal var _player:lesta.structs.Player;

        public var aimIcon:flash.display.MovieClip;

        public static var kmPostfix:String;

        public var shipIcon:flash.display.MovieClip;

        public var shipIconType:flash.display.MovieClip;

        internal var _distanceToShipValue:Number;
    }
}
