﻿package lesta.dialogs.battle_window.markers.container.views.torpedoes 
{
    import flash.display.*;
    import lesta.constants.*;
    import lesta.dialogs.battle_window.markers.container.views.base.*;
    import lesta.dialogs.battle_window.markers.utils.*;
    import lesta.structs.*;
    import lesta.utils.*;
    
    public class TorpedoMarker extends lesta.dialogs.battle_window.markers.container.views.base.SimpleMarker
    {
        public function TorpedoMarker()
        {
            super();
            return;
        }

        public override function init():void
        {
            super.init();
            _type = lesta.dialogs.battle_window.markers.utils.MarkerTypes.TORPEDO;
            _targetType = lesta.structs.TargetTypes.SHIP;
            this.isAircraftCarrier = lesta.utils.GameInfoHolder.instance.selfPlayer.shipTypeIdent == lesta.constants.ShipTypes.AircraftCarrier;
            play();
            return;
        }

        public override function set info(arg1:lesta.structs.EntityInfo):void
        {
            super.info = arg1;
            var loc1:*=_info.relation + 1;
            if (this.relation.currentFrame == loc1) 
            {
                this.relation.stop();
            }
            else 
            {
                this.relation.gotoAndStop(loc1);
            }
            return;
        }

        protected override function getVisible():Boolean
        {
            return _info.distanceToShip / lesta.constants.Common.BW_TO_BALLISTIC <= lesta.constants.Common.TORPEDO_VISIBILITY_DIST || this.isAircraftCarrier;
        }

        public var relation:flash.display.MovieClip;

        internal var isAircraftCarrier:Boolean;
    }
}
