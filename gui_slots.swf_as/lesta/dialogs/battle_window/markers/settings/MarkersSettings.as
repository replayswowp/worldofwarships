package lesta.dialogs.battle_window.markers.settings 
{
    public class MarkersSettings extends Object
    {
        public function MarkersSettings()
        {
            this.intermediateDistances = [];
            super();
            return;
        }

        public var far:Number=0;

        public var near:Number=0;

        public var isAlternativeView:Boolean=false;

        public var isWeaponAircraft:Boolean=false;

        public var isTacticalMap:Boolean=false;

        public var intermediateDistances:Array;

        public var intermediateDistancesLength:int=0;

        public var intermediateDistancesLast:Number=0;

        public var forceVisible:Boolean=false;
    }
}
