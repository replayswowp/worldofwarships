package lesta.constants 
{
    public class ShipTypes extends Object
    {
        public function ShipTypes()
        {
            super();
            return;
        }

        public static function getIdByIdent(arg1:String):int
        {
            return FRAMES_ORDER.indexOf(arg1);
        }

        public static function getOrderSortIndex(arg1:String):int
        {
            return SORT_ORDER.indexOf(arg1);
        }

        public static function getIndexByString(arg1:String):int
        {
            return SHIPSLIST.indexOf(arg1);
        }

        
        {
            SHIPSLIST = [Destroyer, AircraftCarrier, Cruiser, BattleShip, Auxiliary];
            FRAMES_ORDER = [Destroyer, AircraftCarrier, Cruiser, BattleShip, Auxiliary];
            SORT_ORDER = [AircraftCarrier, BattleShip, Cruiser, Destroyer, Auxiliary];
        }

        public static const Destroyer:String="Destroyer";

        public static const AircraftCarrier:String="AirCarrier";

        public static const Cruiser:String="Cruiser";

        public static const BattleShip:String="Battleship";

        public static const Auxiliary:String="Auxiliary";

        public static const CARRIER:int=1;

        public static const BATTLESHIP:int=0;

        public static const CRUISER:int=2;

        public static const DESTROYER:int=3;

        public static const AUXILIARYSHIP:int=4;

        public static var SHIPSLIST:Array;

        public static var FRAMES_ORDER:Array;

        public static var SORT_ORDER:Array;
    }
}
