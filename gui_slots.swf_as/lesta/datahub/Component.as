package lesta.datahub 
{
    public class Component extends Object
    {
        public function Component()
        {
            super();
            return;
        }

        public function fini():void
        {
            this.entity = null;
            return;
        }

        public function get classID():uint
        {
            return 0;
        }

        public function get className():String
        {
            return "NoName";
        }

        public static const ADD:uint=0;

        public static const REMOVE:uint=65536;

        public var entity:lesta.datahub.Entity;
    }
}
