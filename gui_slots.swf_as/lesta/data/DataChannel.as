package lesta.data 
{
    public class DataChannel extends Object
    {
        public function DataChannel(arg1:String, arg2:String=null, arg3:Function=null)
        {
            super();
            this.channelName = arg1;
            this.invokeName = arg2;
            this.postAction = arg3;
            return;
        }

        public var channelName:String;

        public var invokeName:String;

        public var postAction:Function;
    }
}
