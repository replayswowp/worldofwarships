package lesta.utils 
{
    import flash.utils.*;
    
    public class Reflection extends Object
    {
        public function Reflection()
        {
            super();
            return;
        }

        public static function toPythonObject(arg1:Object):Object
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=null;
            var loc1:*=new Object();
            var loc2:*=flash.utils.describeType(arg1);
            var loc3:*=[loc2, loc2];
            var loc8:*=0;
            var loc9:*=loc3;
            for each (loc4 in loc9) 
            {
                var loc10:*=0;
                var loc11:*=loc4;
                for each (loc5 in loc11) 
                {
                    loc6 = loc5.attribute("name").toString();
                    loc7 = loc5.attribute("type").toString();
                    loc1[loc6] = PYTHON_TYPES.indexOf(loc7) > -1 || !arg1[loc6] ? arg1[loc6] : toPythonObject(arg1[loc6]);
                }
            }
            return loc1;
        }

        public static function toObject(arg1:XML, arg2:Object):Object
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=0;
            var loc5:*=arg1.children();
            for each (loc1 in loc5) 
            {
                if (!arg2.hasOwnProperty(loc1.name())) 
                {
                    continue;
                }
                if (loc1.hasComplexContent()) 
                {
                    loc2 = Class(flash.utils.getDefinitionByName(flash.utils.getQualifiedClassName(arg2[loc1.name()])));
                    loc3 = arg2[loc1.name()] ? arg2[loc1.name()] : new loc2();
                    arg2[loc1.name()] = toObject(loc1, loc3);
                    continue;
                }
                arg2[loc1.name()] = loc1;
            }
            return arg2;
        }

        
        {
            PYTHON_TYPES = ["int", "Boolean", "Number", "uint", "String", "Array", "Object"];
        }

        public static var PYTHON_TYPES:Array;
    }
}
