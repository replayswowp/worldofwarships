package lesta.utils 
{
    public class XMLUtils extends Object
    {
        public function XMLUtils()
        {
            super();
            return;
        }

        public static function castXML(arg1:String):*
        {
            var loc1:*=arg1;
            if (arg1.match(new RegExp("\\[.*]", "g")).length > 0) 
            {
                loc1 = arg1.substring(1, (arg1.length - 1)).split(",");
            }
            if (!isNaN(Number(arg1))) 
            {
                loc1 = Number(arg1);
            }
            if (arg1.toLowerCase() == "true" || arg1.toLowerCase() == "false") 
            {
                loc1 = arg1.toLowerCase() == "true";
            }
            return loc1;
        }
    }
}
