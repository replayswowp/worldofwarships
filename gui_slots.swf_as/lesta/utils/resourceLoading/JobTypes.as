package lesta.utils.resourceLoading 
{
    public class JobTypes extends Object
    {
        public function JobTypes()
        {
            super();
            return;
        }

        public static function getJobClass(arg1:String, arg2:String):Class
        {
            var loc2:*=null;
            var loc1:*=null;
            if (arg1 != LOAD_RESOURCE) 
            {
                loc1 = CUSTOM_RESOURCES_MAP[arg1];
            }
            else 
            {
                loc2 = arg2.split(".");
                loc1 = FILE_TYPES[loc2[(loc2.length - 1)]];
            }
            return loc1;
        }

        public static const LOAD_RESOURCE:String="job.loadResource";

        public static const PRELOAD_BATTLE_WINDOW_DATA:String="job.preloadBattleWindowData";

        internal static const FILE_TYPES:Object={"swf":lesta.utils.resourceLoading.DisplayResource, "jpg":lesta.utils.resourceLoading.BitmapDataResource, "png":lesta.utils.resourceLoading.BitmapDataResource, "gif":lesta.utils.resourceLoading.BitmapDataResource, "usm":lesta.utils.resourceLoading.VideoResource, "flv":lesta.utils.resourceLoading.VideoResource};

        internal static const CUSTOM_RESOURCES_MAP:Object={"job.preloadBattleWindowData":lesta.utils.resourceLoading.PreloadBattleWindowJob};
    }
}
