package lesta.utils.resourceLoading 
{
    import flash.events.*;
    
    public interface IJob extends flash.events.IEventDispatcher
    {
        function get id():uint;

        function get result():*;

        function get isDone():Boolean;

        function doJob():void;

        function release():void;
    }
}
