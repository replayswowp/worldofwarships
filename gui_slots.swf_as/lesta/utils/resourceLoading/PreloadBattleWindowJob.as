package lesta.utils.resourceLoading 
{
    import flash.events.*;
    import flash.net.*;
    import lesta.data.*;
    import lesta.dialogs.battle_window.markers.config.*;
    import lesta.dialogs.battle_window_new.*;
    
    public class PreloadBattleWindowJob extends lesta.utils.resourceLoading.JobBaseClass implements lesta.utils.resourceLoading.IJob
    {
        public function PreloadBattleWindowJob(arg1:int, arg2:String)
        {
            super(arg1);
            this._url = arg2;
            return;
        }

        function fini():void
        {
            this._configLoader = null;
            this._markersConfig = null;
            return;
        }

        public function doJob():void
        {
            this.loadMarkers();
            this.loadConfig();
            return;
        }

        public function get result():*
        {
            return {"markers":this._markersConfig, "config":this._configLoader};
        }

        public function get isDone():Boolean
        {
            return this.markersLoaded && this.configLoaded;
        }

        public function get markersConfig():lesta.dialogs.battle_window.markers.config.MarkersConfig
        {
            return this._markersConfig;
        }

        public function get configLoader():lesta.dialogs.battle_window_new.ConfigLoader
        {
            return this._configLoader;
        }

        internal function loadMarkers():void
        {
            var loc1:*=new flash.net.URLLoader();
            loc1.addEventListener(flash.events.Event.COMPLETE, this.onUrlLoaded);
            loc1.load(new flash.net.URLRequest(ROOT_URL + this._url));
            return;
        }

        internal function loadConfig():void
        {
            this._configLoader = new lesta.dialogs.battle_window_new.ConfigLoader();
            this._configLoader.addEventListener(flash.events.Event.COMPLETE, this.onConfigLoaded);
            return;
        }

        internal function onUrlLoaded(arg1:flash.events.Event):void
        {
            var loc1:*=arg1.target as flash.net.URLLoader;
            loc1.removeEventListener(flash.events.Event.COMPLETE, this.onUrlLoaded);
            this.parseXml(loc1.data);
            this.markersLoaded = true;
            this.checkComplete();
            return;
        }

        internal function parseXml(arg1:*):void
        {
            this._markersConfig = new lesta.dialogs.battle_window.markers.config.MarkersConfig();
            this._markersConfig.loadFromXML(XML(arg1));
            return;
        }

        internal function onConfigLoaded(arg1:flash.events.Event):void
        {
            this.configLoaded = true;
            this.checkComplete();
            return;
        }

        internal function checkComplete():void
        {
            if (!this.isDone) 
            {
                return;
            }
            lesta.data.GameDelegate.call("resourceLoadingDelegator.jobDone", [id]);
            dispatchEvent(new flash.events.Event(flash.events.Event.COMPLETE));
            return;
        }

        internal static const ROOT_URL:String="../../";

        internal var _markersConfig:lesta.dialogs.battle_window.markers.config.MarkersConfig;

        internal var _configLoader:lesta.dialogs.battle_window_new.ConfigLoader;

        internal var _url:String;

        internal var markersLoaded:Boolean=false;

        internal var configLoaded:Boolean=false;
    }
}
