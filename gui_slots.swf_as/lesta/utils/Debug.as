package lesta.utils 
{
    import flash.display.*;
    
    public class Debug extends Object
    {
        public function Debug()
        {
            super();
            return;
        }

        public static function isUssLoadingDebugMode():Boolean
        {
            var loc1:*=false;
            return loc1;
        }

        public static function setDebugSettings(arg1:Boolean, arg2:Object, arg3:Function):void
        {
            lesta.utils.Debug.debugMode = arg1;
            lesta.utils.Debug.debugSettings = arg2;
            lesta.utils.Debug.out = arg3;
            return;
        }

        public static function getDebugEnabled(arg1:int=-1):Boolean
        {
            if (!debugMode || !(arg1 == -1) && !debugSettings[arg1]) 
            {
                return false;
            }
            return true;
        }

        public static function formatString(arg1:String, arg2:Array):String
        {
            var loc1:*=0;
            while (loc1 < arg2.length) 
            {
                arg1 = arg1.replace("%", arg2[loc1]);
                ++loc1;
            }
            return arg1;
        }

        public static function checkChildren(arg1:flash.display.DisplayObjectContainer, arg2:Array):void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc1:*="[ERROR] %type   %obj.%childName";
            var loc2:*=0;
            while (loc2 < arg2.length) 
            {
                loc3 = arg2[loc2];
                loc4 = arg1.getChildByName(loc3) as flash.display.DisplayObject;
                loc5 = null;
                try 
                {
                    loc5 = (arg1 as Object)[loc3] as flash.display.DisplayObject;
                }
                catch (e:Error)
                {
                };
                if (loc4 == null || loc5 == null) 
                {
                    loc6 = (loc6 = (loc6 = (loc6 = loc1).replace("%type", arg1)).replace("%obj", arg1.name)).replace("%childName", loc3);
                    lesta.utils.Debug.out(loc6);
                }
                ++loc2;
            }
            return;
        }

        
        {
            debugMode = false;
            debugSettings = new Object();
        }

        internal static var debugMode:Boolean=false;

        internal static var debugSettings:Object;

        internal static var out:Function;
    }
}
