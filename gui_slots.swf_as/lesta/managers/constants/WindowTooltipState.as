package lesta.managers.constants 
{
    public class WindowTooltipState extends Object
    {
        public function WindowTooltipState()
        {
            super();
            return;
        }

        public static const pinned:int=0;

        public static const dragging:int=1;

        public static const free:int=2;
    }
}
