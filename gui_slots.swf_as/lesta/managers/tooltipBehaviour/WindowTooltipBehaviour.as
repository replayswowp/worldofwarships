package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import lesta.managers.*;
    import lesta.managers.constants.*;
    import lesta.managers.events.*;
    import lesta.unbound.layout.*;
    import scaleform.gfx.*;
    
    public class WindowTooltipBehaviour extends lesta.managers.tooltipBehaviour.InteractiveTooltipBehaviour
    {
        public function WindowTooltipBehaviour()
        {
            super();
            return;
        }

        public override function getName():String
        {
            return "window";
        }

        public override function onReasonMouseOver(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onReasonMouseOut(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onReasonRollOver(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onReasonRollOut(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onReasonMouseMove(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onReasonMouseDown(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onReasonClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            var loc1:*;
            var loc2:*;
            if (loc2 = (loc1 = arg3 as scaleform.gfx.MouseEventEx).buttonIdx == scaleform.gfx.MouseEventEx.LEFT_BUTTON) 
            {
                if (arg2.visible) 
                {
                    hideTooltip(arg2);
                }
                else 
                {
                    this.showTooltip(arg1, arg2, arg3);
                }
                return arg2.visible;
            }
            return loc2;
        }

        public override function onTooltipRollOver(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onTooltipRollOut(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onTooltipMouseMove(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onTooltipMouseDown(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            this.showTooltip(arg1, arg2, arg3);
            return false;
        }

        public override function onTooltipClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onOutsideMouseDown(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onOutsideMouseClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onTooltipClose(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            hideTooltip(arg2);
            return false;
        }

        public override function onApplicationLooseFocus(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            return false;
        }

        public override function onRegisterTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            super.onRegisterTooltip(arg1, arg2);
            this.state = arg2.isTooltipPinned() ? lesta.managers.constants.WindowTooltipState.free : lesta.managers.constants.WindowTooltipState.pinned;
            if (this.state == lesta.managers.constants.WindowTooltipState.free) 
            {
                arg2.hideAllPins();
            }
            arg2.stateChangedCallback(this.state);
            var loc1:*=arg2.tipInstance as lesta.unbound.layout.UbBlock;
            var loc2:*=loc1.styleFragments.width;
            var loc3:*=loc1.styleFragments.height;
            if (isNaN(loc2)) 
            {
                loc2 = loc1.width;
            }
            if (isNaN(loc3)) 
            {
                loc3 = loc1.height;
            }
            this.correctSizeAndPosition(arg1, arg2, loc2, loc3);
            return;
        }

        protected override function placeTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):void
        {
            this.correctSizeAndPosition(arg1, arg2, arg2.tipInstance.width, arg2.tipInstance.height);
            return;
        }

        protected function getPinnedPosition(arg1:lesta.managers.tooltipBehaviour.TooltipInfo, arg2:Number=-1):flash.geom.Point
        {
            if (!isValidTooltip(arg1)) 
            {
                return new flash.geom.Point(0, 0);
            }
            var loc1:*=arg1.getStyleProperty("tinyOffset", tinyOffset);
            var loc2:*=arg1.getStyleProperty("rightOffset", rightOffset);
            var loc3:*=arg1.getStyleProperty("bottomOffset", bottomOffset);
            var loc4:*;
            var loc5:*=(loc4 = getTargetRect(arg1)).left + loc2;
            var loc6:*=loc3 + loc4.top - (arg2 != -1 ? arg2 : arg1.tipInstance.height);
            return new flash.geom.Point(loc5, loc6);
        }

        public override function onTooltipStartDragging(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            this.state = lesta.managers.constants.WindowTooltipState.dragging;
            arg2.hideAllPins();
            this.showTooltip(arg1, arg2, null);
            arg2.stateChangedCallback(this.state);
            return false;
        }

        public override function onTooltipStopDragging(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            var loc1:*=this.getPinnedPosition(arg2);
            var loc2:*=Math.abs(loc1.x - arg2.tipInstance.x);
            var loc3:*=Math.abs(loc1.y - arg2.tipInstance.y);
            var loc4:*=loc2 < 50 && loc3 < 50;
            this.state = loc4 ? lesta.managers.constants.WindowTooltipState.pinned : lesta.managers.constants.WindowTooltipState.free;
            if (this.state == lesta.managers.constants.WindowTooltipState.pinned) 
            {
                arg2.tipInstance.dispatchEvent(new lesta.managers.events.DragEvent(lesta.managers.events.DragEvent.CLEAR_DRAGGING));
                this.correctSizeAndPosition(arg1, arg2, arg2.tipInstance.width, arg2.tipInstance.height);
            }
            if (this.state == lesta.managers.constants.WindowTooltipState.free) 
            {
                arg2.hideAllPins();
            }
            arg2.stateChangedCallback(this.state);
            return false;
        }

        public override function showTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent, arg4:Boolean=false):void
        {
            if (arg2.visible) 
            {
                this.correctSizeAndPosition(arg1, arg2, arg2.tipInstance.width, arg2.tipInstance.height);
                arg2.tipInstance.parent.setChildIndex(arg2.tipInstance, (arg2.tipInstance.parent.numChildren - 1));
            }
            else 
            {
                super.showTooltip(arg1, arg2, arg3, arg4);
            }
            return;
        }

        public override function onApplicationSizeChanged(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            this.correctSizeAndPosition(arg1, arg2, arg2.tipInstance.width, arg2.tipInstance.height);
            return;
        }

        internal function correctSizeAndPosition(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:Number, arg4:Number):void
        {
            var loc9:*=NaN;
            var loc10:*=NaN;
            var loc11:*=null;
            var loc1:*=arg2.getStyleProperty("minResizeHeight", 0);
            var loc2:*=arg2.getStyleProperty("minResizeWidth", 0);
            arg4 = Math.max(loc1, arg4);
            arg3 = Math.max(loc2, arg3);
            var loc3:*=lesta.managers.DragAndDropManager.sceenBorderOffset;
            var loc4:*=arg1.stage;
            var loc5:*=arg4;
            var loc6:*=arg3;
            var loc7:*=Math.max(arg2.tipInstance.x, loc3);
            var loc8:*=Math.max(arg2.tipInstance.y, loc3);
            if (this.state != lesta.managers.constants.WindowTooltipState.pinned) 
            {
                loc9 = loc4.stageHeight - loc3 * 2;
                loc10 = loc4.stageWidth - loc3 * 2;
                loc5 = Math.min(loc9, arg4);
                loc5 = Math.max(loc1, loc5);
                loc7 = Math.min(loc4.stageWidth - loc6 - loc3, loc7);
                loc8 = Math.min(loc4.stageHeight - loc5 - loc3, loc8);
            }
            else 
            {
                loc9 = (loc11 = this.getPinnedPosition(arg2, arg4)).y + arg4 - loc3;
                loc10 = loc4.stageWidth - loc11.x - loc3;
                loc5 = Math.min(loc9, arg4);
                loc5 = Math.max(loc1, loc5);
                loc7 = (loc11 = this.getPinnedPosition(arg2, loc5)).x;
                loc8 = loc11.y;
            }
            loc6 = Math.min(loc10, arg3);
            loc6 = Math.max(loc2, loc6);
            if (loc5 != arg2.tipInstance.width) 
            {
                arg2.tipInstance.width = loc6;
            }
            if (loc5 != arg2.tipInstance.height) 
            {
                arg2.tipInstance.height = loc5;
            }
            arg2.tipInstance.x = loc7;
            arg2.tipInstance.y = loc8;
            placeTooltipPin(arg1, arg2, null, DIRECTION_VERTICAL, true);
            return;
        }

        internal var state:int=0;
    }
}
