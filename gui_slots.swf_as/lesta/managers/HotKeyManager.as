package lesta.managers 
{
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.controls.*;
    import lesta.data.*;
    import scaleform.clik.constants.*;
    import scaleform.clik.controls.*;
    
    public class HotKeyManager extends flash.events.EventDispatcher
    {
        public function HotKeyManager()
        {
            this.mHotKeysInfo = new flash.utils.Dictionary();
            this.mMouseEvents = new flash.utils.Dictionary();
            this.mapCommandsStr = new flash.utils.Dictionary();
            this.mapIndicators = new flash.utils.Dictionary();
            this.listIndicators = new Array();
            this.substituteKeys = new Array();
            super();
            return;
        }

        internal function onCommandUp(arg1:int):void
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            this.setHotkeyState(arg1, false);
            var loc1:*=this.getHotKeyInfo(arg1);
            if (loc1 == null) 
            {
                return;
            }
            loc1.isActive = false;
            var loc2:*=true;
            var loc3:*=0;
            while (loc3 < loc1.controls.length) 
            {
                loc5 = (loc4 = loc1.controls[loc3]) as flash.display.InteractiveObject;
                if (!((loc6 = loc4 as scaleform.clik.controls.Button) == null) && loc6.mouseEnabled && loc1.enabled) 
                {
                    loc6.playSound(scaleform.clik.constants.SoundEvent.RELEASE_HOTKEY);
                }
                if (loc4) 
                {
                    loc4.dispatchEvent(new flash.events.MouseEvent(flash.events.MouseEvent.ROLL_OUT));
                }
                if (loc5 && !loc5.mouseEnabled) 
                {
                    loc2 = false;
                }
                ++loc3;
            }
            if (loc2) 
            {
                if (loc1.callbackUp != null) 
                {
                    loc1.callbackUp();
                }
                if (loc1.callback != null) 
                {
                    loc1.callback(false);
                }
            }
            return;
        }

        internal function setModifiersMask(arg1:int):void
        {
            this.mCurrentModifiersMask = arg1;
            return;
        }

        internal function setCommandActive(arg1:int, arg2:Boolean):void
        {
            var loc1:*=this.getHotKeyInfo(arg1);
            if (loc1 == null) 
            {
                return;
            }
            if (loc1.isActive && !arg2) 
            {
                this.onCommandUp(arg1);
            }
            else if (!loc1.isActive && arg2) 
            {
                this.onCommandDown(arg1);
            }
            return;
        }

        public function isActive(arg1:int):Boolean
        {
            var loc1:*=this.getHotKeyInfo(arg1);
            if (loc1 == null) 
            {
                return false;
            }
            return loc1.isActive;
        }

        internal function getHotKeyInfo(arg1:uint):HotKeyInfo
        {
            if (this.mHotKeysInfo[arg1] == null) 
            {
                this.mHotKeysInfo[arg1] = new HotKeyInfo(arg1);
            }
            return this.mHotKeysInfo[arg1];
        }

        public function setHotkeyState(arg1:int, arg2:Boolean):void
        {
            var loc1:*=null;
            var loc2:*=null;
            if (this.mapIndicators[arg1] == null) 
            {
                return;
            }
            var loc3:*=0;
            var loc4:*=this.mapIndicators[arg1];
            for each (loc1 in loc4) 
            {
                if (loc1.toggle) 
                {
                    continue;
                }
                loc2 = arg2 ? lesta.controls.HotKeyContainer.DOWN : lesta.controls.HotKeyContainer.DEFAULT;
                loc1.setState(loc2, false, true);
            }
            return;
        }

        public static function get instance():lesta.managers.HotKeyManager
        {
            if (mInstance == null) 
            {
                mInstance = new HotKeyManager();
            }
            return mInstance;
        }

        
        {
            mInstance = null;
        }

        public function loadInfo(arg1:Array):void
        {
            var loc2:*=0;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=0;
            var loc7:*=0;
            var loc8:*=0;
            this.mMouseEvents = new flash.utils.Dictionary();
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                loc2 = arg1[loc1++];
                loc3 = arg1[loc1++];
                loc4 = arg1[loc1++];
                (loc5 = new MouseCommandInfo(loc2)).groupName = loc4;
                this.mMouseEvents[loc2] = loc5;
                loc6 = 0;
                while (loc6 < loc3.length) 
                {
                    loc7 = loc3[loc6++];
                    loc8 = loc3[loc6++];
                    loc5.buttons.push(loc7);
                    loc5.modefiers.push(loc8);
                }
            }
            return;
        }

        public function countDictionary(arg1:flash.utils.Dictionary):int
        {
            var loc1:*=0;
            var loc2:*=undefined;
            if (arg1 == null) 
            {
                return 0;
            }
            var loc3:*=0;
            var loc4:*=arg1;
            for each (loc2 in loc4) 
            {
                ++loc1;
            }
            return loc1;
        }

        public function loadCommandsStr(arg1:Array):void
        {
            var loc3:*=0;
            var loc4:*=null;
            var loc5:*=null;
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                loc3 = arg1[loc1][0];
                loc4 = arg1[loc1][1];
                this.mapCommandsStr[loc3] = loc4;
                ++loc1;
            }
            var loc2:*=0;
            while (loc2 < this.listIndicators.length) 
            {
                (loc5 = this.listIndicators[loc2]).updateCommandData();
                ++loc2;
            }
            dispatchEvent(new flash.events.Event(EVENT_HOTKEY_STR_UPDATED));
            return;
        }

        public function loadSubstituteKeys(arg1:Array):void
        {
            this.substituteKeys = arg1;
            return;
        }

        public function init():void
        {
            lesta.data.GameDelegate.addCallBack("CommandMapping.loadInfo", this, this.loadInfo);
            lesta.data.GameDelegate.addCallBack("CommandMapping.loadCommandsStr", this, this.loadCommandsStr);
            lesta.data.GameDelegate.addCallBack("CommandMapping.loadSubstitutes", this, this.loadSubstituteKeys);
            lesta.data.GameDelegate.addCallBack("CommandMapping.commandDown", this, this.onCommandDown);
            lesta.data.GameDelegate.addCallBack("CommandMapping.commandUp", this, this.onCommandUp);
            lesta.data.GameDelegate.addCallBack("CommandMapping.setCommandActive", this, this.setCommandActive);
            lesta.data.GameDelegate.addCallBack("CommandMapping.setModefiersMask", this, this.setModifiersMask);
            return;
        }

        public function fini():void
        {
            lesta.data.GameDelegate.removeCallBack(this);
            return;
        }

        public function getKeyStrByCommandId(arg1:int):String
        {
            if (this.mapCommandsStr[arg1] == null) 
            {
                return "-";
            }
            return this.mapCommandsStr[arg1];
        }

        public function setHotkeyIndicator(arg1:flash.display.MovieClip, arg2:int):void
        {
            if (!this.mapIndicators[arg2]) 
            {
                this.mapIndicators[arg2] = [];
            }
            (this.mapIndicators[arg2] as Array).push(arg1);
            this.listIndicators.push(arg1);
            return;
        }

        public function clearHotkeyIndicator(arg1:int):void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=0;
            var loc4:*=this.mapIndicators[arg1];
            for each (loc1 in loc4) 
            {
                loc2 = this.listIndicators.indexOf(loc1);
                this.listIndicators.splice(loc2, 1);
            }
            delete this.mapIndicators[arg1];
            return;
        }

        public function setControl(arg1:uint, arg2:flash.display.MovieClip):void
        {
            var loc1:*=this.getHotKeyInfo(arg1);
            loc1.controls.push(arg2);
            return;
        }

        public function setEnabled(arg1:int, arg2:Boolean):void
        {
            var loc1:*=this.getHotKeyInfo(arg1);
            loc1.enabled = arg2;
            return;
        }

        public function setCallback(arg1:uint, arg2:Function, arg3:String=null):void
        {
            var loc1:*;
            if ((loc1 = this.getHotKeyInfo(arg1)) == null) 
            {
                return;
            }
            if (arg3 != scaleform.clik.constants.InputValue.KEY_DOWN) 
            {
                if (arg3 != scaleform.clik.constants.InputValue.KEY_UP) 
                {
                    if (arg3 == null) 
                    {
                        loc1.callback = arg2;
                    }
                }
                else 
                {
                    loc1.callbackUp = arg2;
                }
            }
            else 
            {
                loc1.callbackDown = arg2;
            }
            return;
        }

        public function clearControl(arg1:uint, arg2:flash.display.DisplayObject=null):void
        {
            var loc3:*=null;
            var loc1:*=this.getHotKeyInfo(arg1);
            if (loc1 == null) 
            {
                return;
            }
            var loc2:*=(loc1.controls.length - 1);
            while (loc2 >= 0) 
            {
                if ((loc3 = loc1.controls[loc2]) == arg2 || arg2 == null) 
                {
                    loc3.dispatchEvent(new flash.events.MouseEvent(flash.events.MouseEvent.ROLL_OUT));
                    loc1.controls.splice(loc2, 1);
                }
                --loc2;
            }
            return;
        }

        public function clearCallback(arg1:uint, arg2:String=null):void
        {
            var loc1:*=this.getHotKeyInfo(arg1);
            if (loc1 == null) 
            {
                return;
            }
            if (arg2 != scaleform.clik.constants.InputValue.KEY_DOWN) 
            {
                if (arg2 != scaleform.clik.constants.InputValue.KEY_UP) 
                {
                    if (arg2 == null) 
                    {
                        loc1.callback = null;
                    }
                }
                else 
                {
                    loc1.callbackUp = null;
                }
            }
            else 
            {
                loc1.callbackDown = null;
            }
            return;
        }

        public function isMouseCommand(arg1:int, arg2:int):Boolean
        {
            var loc4:*=undefined;
            var loc5:*=null;
            var loc6:*=0;
            var loc7:*=0;
            if (this.mMouseEvents[arg2] == null) 
            {
                return false;
            }
            var loc1:*=this.mMouseEvents[arg2].groupName;
            var loc2:*=false;
            var loc3:*=false;
            var loc8:*=0;
            var loc9:*=this.mMouseEvents;
            for (loc4 in loc9) 
            {
                if ((loc5 = this.mMouseEvents[loc4]) == null || !(loc1 == loc5.groupName)) 
                {
                    continue;
                }
                loc6 = loc5.buttons.length;
                loc7 = 0;
                while (loc7 < loc6) 
                {
                    if (loc5.buttons[loc7] == arg1) 
                    {
                        if (loc5.modefiers[loc7] == this.mCurrentModifiersMask) 
                        {
                            return arg2 == loc4;
                        }
                        if (arg2 == loc4 && loc5.modefiers[loc7] == 0) 
                        {
                            loc3 = true;
                        }
                    }
                    ++loc7;
                }
            }
            return loc3;
        }

        internal function onCommandDown(arg1:int):void
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            this.setHotkeyState(arg1, true);
            var loc1:*=this.getHotKeyInfo(arg1);
            if (loc1 == null || !loc1.enabled) 
            {
                return;
            }
            loc1.isActive = true;
            var loc2:*=true;
            var loc3:*=0;
            while (loc3 < loc1.controls.length) 
            {
                loc5 = (loc4 = loc1.controls[loc3]) as flash.display.InteractiveObject;
                loc6 = loc4 as scaleform.clik.controls.Button;
                if (loc4.stage != null) 
                {
                    if (loc5 && loc5.mouseEnabled || !loc5) 
                    {
                        loc4.dispatchEvent(new flash.events.MouseEvent(flash.events.MouseEvent.MOUSE_DOWN, true, false, Number.NaN, Number.NaN));
                        if (loc6 != null) 
                        {
                            loc6.playSound(scaleform.clik.constants.SoundEvent.PRESS_HOTKEY);
                        }
                        if (!(loc6 == null) && loc6.toggle) 
                        {
                            loc6.selected = !loc6.selected;
                        }
                    }
                    else 
                    {
                        loc2 = false;
                    }
                }
                ++loc3;
            }
            if (loc2) 
            {
                if (loc1.callbackDown != null) 
                {
                    loc1.callbackDown();
                }
                if (loc1.callback != null) 
                {
                    loc1.callback(true);
                }
            }
            return;
        }

        public static const MODIFIER_CTRL:uint=1;

        public static const EVENT_HOTKEY_STR_UPDATED:String="hotkeyStrUpdated";

        public static const MODIFIER_NONE:uint=0;

        internal var mHotKeysInfo:flash.utils.Dictionary;

        internal var mMouseEvents:flash.utils.Dictionary;

        internal var mCurrentModifiersMask:int=0;

        internal var mPrevInput:String=null;

        internal var mapCommandsStr:flash.utils.Dictionary;

        internal var mapIndicators:flash.utils.Dictionary;

        internal var listIndicators:Array;

        public var substituteKeys:Array;

        internal static var mInstance:lesta.managers.HotKeyManager=null;
    }
}

import __AS3__.vec.*;
import flash.display.*;
import flash.utils.*;


class HotKeyInfo extends Object
{
    public function HotKeyInfo(arg1:int)
    {
        this.keys = new Array();
        this.modifierByKeyCode = new flash.utils.Dictionary();
        this.controls = new Vector.<flash.display.DisplayObject>();
        super();
        this.id = arg1;
        return;
    }

    public function clear():void
    {
        this.controls.length = 0;
        this.enabled = true;
        this.callbackDown = null;
        this.callbackUp = null;
        return;
    }

    public var keys:Array;

    public var modifierByKeyCode:flash.utils.Dictionary;

    public var id:int=0;

    public var controls:__AS3__.vec.Vector.<flash.display.DisplayObject>;

    public var enabled:Boolean=true;

    public var callbackDown:Function=null;

    public var callbackUp:Function=null;

    public var callback:Function=null;

    public var isActive:Boolean=false;
}

class MouseCommandInfo extends Object
{
    public function MouseCommandInfo(arg1:int)
    {
        this.buttons = new Array();
        this.modefiers = new Array();
        super();
        this.commandId = arg1;
        return;
    }

    public var commandId:int=0;

    public var buttons:Array;

    public var modefiers:Array;

    public var groupName:String;
}