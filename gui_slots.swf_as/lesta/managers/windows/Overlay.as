package lesta.managers.windows 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import lesta.data.*;
    import lesta.interfaces.*;
    import lesta.managers.*;
    import lesta.utils.*;
    import scaleform.clik.constants.*;
    import scaleform.clik.utils.*;
    import scaleform.gfx.*;
    
    public class Overlay extends flash.display.MovieClip implements lesta.interfaces.IOverlay
    {
        public function Overlay(arg1:flash.display.DisplayObject=null, arg2:Boolean=false, arg3:Number=0.3, arg4:int=-1)
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=0;
            var loc7:*=null;
            this.mStartSize = new flash.geom.Point(0, 0);
            this.mStageSize = new flash.geom.Point(0, 0);
            this.focusManager = new lesta.managers.FocusWindowManager();
            super();
            if (arg4 == -1) 
            {
                arg4 = scaleform.clik.utils.Constraints.CENTER_H | scaleform.clik.utils.Constraints.CENTER_V;
            }
            if (arg1 != null) 
            {
                this.overlayCenterWidget = arg1 as flash.display.DisplayObjectContainer;
            }
            this.mIsRatioScaleBackground = arg2;
            if (this.overlayBackground == null && !(arg3 == -1)) 
            {
                (loc4 = new flash.display.Sprite()).graphics.beginFill(0, arg3);
                loc4.graphics.drawRect(0, 0, 100, 100);
                loc4.graphics.endFill();
                this.overlayBackground = loc4 as flash.display.DisplayObject;
                addChildAt(this.overlayBackground, 0);
            }
            var loc1:*=-1;
            var loc2:*=-1;
            if (this.overlaySize != null) 
            {
                this.mStartSize.x = this.overlaySize.width;
                this.mStartSize.y = this.overlaySize.height;
                loc1 = this.overlaySize.width;
                loc2 = this.overlaySize.height;
                this.overlaySize.mouseChildren = false;
                this.overlaySize.mouseEnabled = false;
                scaleform.gfx.InteractiveObjectEx.setHitTestDisable(this.overlaySize, true);
                this.overlaySize.visible = false;
            }
            this.mConstraints = new scaleform.clik.utils.Constraints(this, scaleform.clik.constants.ConstrainMode.REFLOW, loc1, loc2);
            this.mConstraints.addElement("background", this.overlayBackground, scaleform.clik.utils.Constraints.CENTER_H | scaleform.clik.utils.Constraints.CENTER_V);
            if (this.overlayCenterWidget != null) 
            {
                this.mConstraints.addElement("centerWidget", this.overlayCenterWidget, arg4);
            }
            var loc3:*;
            if ((loc3 = this.overlayCenterWidget) != null) 
            {
                loc5 = this as Object;
                loc6 = 0;
                while (loc6 < loc3.numChildren) 
                {
                    loc7 = loc3.getChildAt(loc6);
                    try 
                    {
                        loc5[loc7.name] = loc7;
                    }
                    catch (e:Error)
                    {
                    };
                    ++loc6;
                }
            }
            addEventListener(flash.events.Event.ADDED_TO_STAGE, this.onAddedToStage);
            return;
        }

        public function get isTop():Boolean
        {
            return this.mIsTop;
        }

        public function updateStageSize():void
        {
            return;
        }

        public function beforeOpen(arg1:Object=null):void
        {
            this.updateData(arg1);
            this.mIsTop = true;
            if (this.overlayCenterWidget != null) 
            {
                if (!contains(this.overlayCenterWidget)) 
                {
                    addChild(this.overlayCenterWidget);
                }
                this.overlayCenterWidget.visible = true;
            }
            this.updateStageSizeNow();
            var loc1:*=this.overlayCenterWidget as lesta.interfaces.IOverlay;
            if (loc1 != null) 
            {
                loc1.beforeOpen(arg1);
            }
            return;
        }

        public function startShow():void
        {
            this.focusManager.handleWindowOnStartShow();
            return;
        }

        public function startHide():void
        {
            this.focusManager.handleWindowOnStartHide();
            return;
        }

        public function setIsTop(arg1:Boolean, arg2:Object):void
        {
            this.mIsTop = arg1;
            if (arg1) 
            {
                this.onBecomeTop(arg2);
            }
            else 
            {
                this.onLostTop();
            }
            return;
        }

        public function beforeClose():void
        {
            this.mConstraints.removeAllElements();
            this.mConstraints = null;
            this.mHasOwnAppearance = true;
            lesta.data.GameDelegate.removeCallBack(this);
            lesta.utils.GameInfoHolder.instance.removeCallback(this);
            lesta.managers.GameInputHandler.instance.clearKeyHandlerByScope(this);
            lesta.data.GameDelegate.removeCallBack(this.overlayCenterWidget);
            lesta.utils.GameInfoHolder.instance.removeCallback(this.overlayCenterWidget);
            lesta.managers.GameInputHandler.instance.clearKeyHandlerByScope(this.overlayCenterWidget);
            lesta.managers.GameInputHandler.instance.setScopeInputEnabled(this, true);
            lesta.managers.GameInputHandler.instance.setScopeInputEnabled(this.overlayCenterWidget, true);
            lesta.managers.InputMapping.unregisterGuiButtonByScope(this);
            lesta.managers.InputMapping.unregisterGuiButtonByScope(this.overlayCenterWidget);
            var loc1:*=this.overlayCenterWidget as lesta.interfaces.IOverlay;
            if (loc1 != null) 
            {
                loc1.beforeClose();
            }
            return;
        }

        public function updateData(arg1:Object=null):void
        {
            this.data = this.data == null ? {} : arg1;
            this.data.windowId = this.mWindowId;
            this.data.windowName = this.mWindowName;
            return;
        }

        public final function updateStageSizeNow():void
        {
            this.updateBackgroundSize();
            this.updateStageSize();
            if (this.mConstraints) 
            {
                this.mConstraints.update(this.stageWidth, this.stageHeight);
            }
            return;
        }

        protected function onAddedToStage(arg1:flash.events.Event):void
        {
            removeEventListener(flash.events.Event.ADDED_TO_STAGE, this.onAddedToStage);
            addEventListener(flash.events.Event.REMOVED_FROM_STAGE, this.onRemovedFromStage);
            return;
        }

        protected function onRemovedFromStage(arg1:flash.events.Event):void
        {
            removeEventListener(flash.events.Event.REMOVED_FROM_STAGE, this.onRemovedFromStage);
            addEventListener(flash.events.Event.ADDED_TO_STAGE, this.onAddedToStage);
            return;
        }

        internal function onCenterWidgetClosed(arg1:flash.events.Event):void
        {
            this.overlayCenterWidget.removeEventListener(flash.events.Event.REMOVED_FROM_STAGE, this.onCenterWidgetClosed);
            var loc1:*=this.overlayCenterWidget as lesta.interfaces.IOverlay;
            var loc2:*=null;
            if (loc1 != null) 
            {
                loc2 = loc1.getResult();
            }
            this.sendCloseMe(loc2);
            return;
        }

        protected function updateBackgroundSize():void
        {
            if (this.overlayBackground == null) 
            {
                return;
            }
            var loc1:*=this.stageWidth / (this.overlayBackground.width / this.overlayBackground.scaleX);
            var loc2:*=this.stageHeight / (this.overlayBackground.height / this.overlayBackground.scaleY);
            var loc3:*=this.mIsRatioScaleBackground ? Math.max(loc1, loc2) : loc1;
            var loc4:*=this.mIsRatioScaleBackground ? Math.max(loc1, loc2) : loc2;
            this.overlayBackground.scaleX = loc3;
            this.overlayBackground.scaleY = loc4;
            return;
        }

        public function get windowId():int
        {
            return this.mWindowId;
        }

        public function initialize(arg1:int, arg2:String):void
        {
            this.mWindowId = arg1;
            this.mWindowName = arg2;
            return;
        }

        public function get needWaitReadyForDataState():Boolean
        {
            return false;
        }

        public function get needWaitReadyForStageState():Boolean
        {
            return false;
        }

        public function prepareForData():void
        {
            return;
        }

        public function prepareForStage():void
        {
            return;
        }

        protected function dispatchState(arg1:String):void
        {
            dispatchEvent(new lesta.managers.windows.WindowStateEvent(lesta.managers.windows.WindowStateEvent.STATE_CHANGED, arg1));
            return;
        }

        public function setRatioScaleBackground(arg1:Boolean):void
        {
            this.mIsRatioScaleBackground = arg1;
            return;
        }

        public function set openingProgress(arg1:Number):void
        {
            alpha = arg1;
            return;
        }

        public function sendCloseMe(arg1:Object=null):void
        {
            return;
        }

        public function setStageSize(arg1:Number, arg2:Number):void
        {
            this.mStageSize.x = arg1;
            this.mStageSize.y = arg2;
            this.updateStageSizeNow();
            return;
        }

        public function getResult():Object
        {
            return null;
        }

        public function onBecomeTop(arg1:Object):void
        {
            if (this.overlayCenterWidget != null) 
            {
                this.overlayCenterWidget.visible = true;
            }
            var loc1:*=this.overlayCenterWidget as lesta.interfaces.IOverlay;
            if (loc1 != null) 
            {
                loc1.onBecomeTop(arg1);
            }
            lesta.managers.GameInputHandler.instance.setScopeInputEnabled(this, true);
            lesta.managers.GameInputHandler.instance.setScopeInputEnabled(this.overlayCenterWidget, true);
            this.focusManager.handleWindowOnBecomeTop();
            return;
        }

        public function onLostTop():void
        {
            var loc1:*=this.overlayCenterWidget as lesta.interfaces.IOverlay;
            if (loc1 != null) 
            {
                loc1.onLostTop();
            }
            lesta.managers.GameInputHandler.instance.setScopeInputEnabled(this, false);
            lesta.managers.GameInputHandler.instance.setScopeInputEnabled(this.overlayCenterWidget, false);
            this.focusManager.handleWindowOnLostTop();
            return;
        }

        public function setBackgroundVisible(arg1:Boolean):void
        {
            if (this.overlayBackground != null) 
            {
                this.overlayBackground.visible = arg1;
            }
            return;
        }

        public function onNextWindowAddedToStage():void
        {
            return;
        }

        public function get constraints():scaleform.clik.utils.Constraints
        {
            return this.mConstraints;
        }

        public function get startWidth():Number
        {
            return this.mStartSize.x;
        }

        public function get startHeight():Number
        {
            return this.mStartSize.y;
        }

        public function get stageWidth():Number
        {
            return this.mStageSize.x;
        }

        public function get stageHeight():Number
        {
            return this.mStageSize.y;
        }

        public function set data(arg1:Object):void
        {
            this.mData = arg1;
            return;
        }

        public function get data():Object
        {
            return this.mData;
        }

        public var overlayBackground:flash.display.DisplayObject;

        public var overlayCenterWidget:flash.display.DisplayObjectContainer;

        public var overlaySize:flash.display.MovieClip;

        internal var mIsRatioScaleBackground:Boolean=false;

        internal var mStartSize:flash.geom.Point;

        internal var mWindowId:int=-1;

        protected var mHasOwnAppearance:Boolean=false;

        internal var mStageSize:flash.geom.Point;

        internal var mIsTop:Boolean=false;

        internal var mData:Object=null;

        internal var mConstraints:scaleform.clik.utils.Constraints;

        public var focusManager:lesta.managers.FocusWindowManager;

        internal var mWindowName:String=null;
    }
}
