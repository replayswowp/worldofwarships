package lesta.managers 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.data.*;
    import scaleform.clik.controls.*;
    import scaleform.clik.events.*;
    
    public class InputMapping extends Object
    {
        public function InputMapping()
        {
            super();
            return;
        }

        public static function registerGuiButtonsByScope(arg1:flash.display.DisplayObjectContainer, arg2:flash.display.InteractiveObject):void
        {
            var loc1:*=mapGuiButtonsByScope[arg1];
            if (loc1 == null) 
            {
                loc1 = new Vector.<flash.display.InteractiveObject>();
                mapGuiButtonsByScope[arg1] = loc1;
            }
            loc1.push(arg2);
            if (arg2 is scaleform.clik.controls.Button) 
            {
                arg2.addEventListener(scaleform.clik.events.ButtonEvent.CLICK, onButtonClick);
            }
            else 
            {
                arg2.addEventListener(flash.events.MouseEvent.CLICK, onButtonClick);
            }
            return;
        }

        public static function unregisterGuiButtonByScope(arg1:flash.display.DisplayObjectContainer):void
        {
            var loc1:*=mapGuiButtonsByScope[arg1];
            if (loc1 == null) 
            {
                return;
            }
            var loc2:*=0;
            while (loc2 < loc1.length) 
            {
                loc1[loc2].removeEventListener(flash.events.MouseEvent.CLICK, onButtonClick);
                ++loc2;
            }
            delete mapGuiButtonsByScope[arg1];
            return;
        }

        public static function dispatchButtonClick(arg1:String):void
        {
            lesta.data.GameDelegate.call("inputMapping.onButtonClick", [arg1]);
            return;
        }

        public static function dispatchRequestEvent(arg1:String, arg2:Object):void
        {
            lesta.data.GameDelegate.call("inputMapping.onRequest", [arg1, arg2]);
            return;
        }

        public static function dispatchActionEvent(arg1:String, arg2:Object):void
        {
            lesta.data.GameDelegate.call("inputMapping.onAction", [arg1, arg2]);
            return;
        }

        internal static function onButtonClick(arg1:flash.events.Event):void
        {
            var loc1:*=arg1.currentTarget as flash.display.InteractiveObject;
            lesta.data.GameDelegate.call("inputMapping.onButtonClick", [loc1.name]);
            return;
        }

        
        {
            mapGuiButtonsByScope = new flash.utils.Dictionary(true);
        }

        internal static var mapGuiButtonsByScope:flash.utils.Dictionary;
    }
}
