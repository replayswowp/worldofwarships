package lesta.managers.events 
{
    import flash.events.*;
    
    public class DragEvent extends flash.events.Event
    {
        public function DragEvent(arg1:String)
        {
            super(arg1);
            return;
        }

        public static const START_DRAGGING:String="startDragging";

        public static const STOP_DRAGGING:String="stopDragging";

        public static const CLEAR_DRAGGING:String="clearDragging";
    }
}
