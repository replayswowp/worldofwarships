package lesta.controls 
{
    import flash.display.*;
    import flash.events.*;
    import flash.net.*;
    import flash.utils.*;
    
    public class Image extends flash.display.MovieClip
    {
        public function Image()
        {
            super();
            return;
        }

        public function init(arg1:Boolean=false):void
        {
            this._originSize = arg1;
            this.bitmap = new flash.display.Bitmap();
            if (!this._originSize) 
            {
                if (numChildren > 0) 
                {
                    this._width = super.width;
                    this._height = super.height;
                }
                var loc1:*;
                scaleY = loc1 = 1;
                scaleX = loc1;
            }
            while (numChildren > 0) 
            {
                removeChildAt(0);
            }
            addChild(this.bitmap);
            this.createLoader();
            return;
        }

        public function free():void
        {
            this.destroyLoader();
            return;
        }

        public function get isLoaded():Boolean
        {
            return this._loaded;
        }

        public function get src():String
        {
            return this._src;
        }

        public function set src(arg1:String):void
        {
            if (this.src != arg1) 
            {
                this._src = arg1;
                this.destroyLoader();
                if (this._src in cache) 
                {
                    this.bitmap.bitmapData = cache[this._src];
                    this.setSize();
                    this._loaded = true;
                    dispatchEvent(new flash.events.Event(flash.events.Event.COMPLETE));
                }
                else 
                {
                    this.createLoader();
                    this._loaded = false;
                    this.loader.load(new flash.net.URLRequest(arg1));
                }
            }
            return;
        }

        public function set className(arg1:String):void
        {
            var loc1:*=null;
            if (this.src != arg1) 
            {
                this._src = arg1;
                if (this._src in cache) 
                {
                    this.bitmap.bitmapData = cache[this._src];
                    this.setSize();
                    this._loaded = true;
                    dispatchEvent(new flash.events.Event(flash.events.Event.COMPLETE));
                }
                else 
                {
                    loc1 = flash.utils.getDefinitionByName(this._src) as Class;
                    this.bitmap.bitmapData = new loc1();
                    cache[this._src] = this.bitmap.bitmapData;
                }
            }
            return;
        }

        public override function set width(arg1:Number):void
        {
            var loc1:*;
            this._width = loc1 = arg1;
            this.bitmap.width = loc1;
            return;
        }

        public override function set height(arg1:Number):void
        {
            var loc1:*;
            this._height = loc1 = arg1;
            this.bitmap.height = loc1;
            return;
        }

        internal function createLoader():void
        {
            this.loader = new flash.display.Loader();
            this.loader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, this.onLoadComplete);
            return;
        }

        internal function destroyLoader():void
        {
            if (this.loader != null) 
            {
                this.loader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, this.onLoadComplete);
            }
            return;
        }

        internal function onLoadComplete(arg1:flash.events.Event):void
        {
            var loc1:*;
            cache[this._src] = loc1 = flash.display.Bitmap(arg1.target.content).bitmapData;
            this.bitmap.bitmapData = loc1;
            this.setSize();
            this._loaded = true;
            dispatchEvent(new flash.events.Event(flash.events.Event.COMPLETE));
            return;
        }

        internal function setSize():void
        {
            if (!this._originSize) 
            {
                this.bitmap.width = this._width;
                this.bitmap.height = this._height;
            }
            return;
        }

        
        {
            cache = new flash.utils.Dictionary();
        }

        internal var _src:String="";

        internal var loader:flash.display.Loader;

        internal var bitmap:flash.display.Bitmap;

        internal var _originSize:Boolean=false;

        internal var _width:Number;

        internal var _height:Number;

        internal var _loaded:Boolean=false;

        internal static var cache:flash.utils.Dictionary;
    }
}
