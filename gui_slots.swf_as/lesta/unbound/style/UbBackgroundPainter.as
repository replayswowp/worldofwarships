package lesta.unbound.style 
{
    import flash.display.*;
    import flash.geom.*;
    import flash.utils.*;
    import lesta.unbound.layout.*;
    
    public class UbBackgroundPainter extends Object
    {
        public function UbBackgroundPainter(arg1:lesta.unbound.layout.UbBlock)
        {
            this.matrix = new flash.geom.Matrix();
            super();
            this.element = arg1;
            return;
        }

        public function paintBackground():void
        {
            var loc1:*=this.element.measuredWidth;
            var loc2:*=this.element.measuredHeight;
            var loc3:*=this.element.computedStyle;
            this.paintSolidColor(this.element.graphics, loc1, loc2, loc3.backgroundColor);
            this.paintImage(this.element, loc1, loc2, loc3);
            return;
        }

        public function fini():void
        {
            if (this.loaderWrapper) 
            {
                this.loaderWrapper.fini();
            }
            this.loaderWrapper = null;
            this.element = null;
            return;
        }

        internal function paintSolidColor(arg1:flash.display.Graphics, arg2:Number, arg3:Number, arg4:uint):void
        {
            arg1.clear();
            if (arg4 > 0) 
            {
                arg1.beginFill(arg4 & 16777215, (arg4 >>> 24) / 255);
                arg1.drawRect(0, 0, arg2, arg3);
                arg1.endFill();
            }
            return;
        }

        internal function onLoader(arg1:String, arg2:flash.display.BitmapData):void
        {
            if (!this.element.computedStyle.backgroundImageOriginatesFromLibrary) 
            {
                bitmapsCache[arg1] = arg2;
            }
            if (!this.element.computedStyle.backgroundImageOriginatesFromLibrary && this.element.computedStyle.backgroundImage == arg1) 
            {
                this.paintImage(this.element, this.element.measuredWidth, this.element.measuredHeight, this.element.computedStyle);
            }
            this.loaderWrapper = null;
            return;
        }

        internal function paintImage(arg1:lesta.unbound.layout.UbBlock, arg2:Number, arg3:Number, arg4:lesta.unbound.style.UbStyle):void
        {
            var loc3:*=null;
            var loc4:*=NaN;
            var loc5:*=NaN;
            var loc6:*=NaN;
            var loc7:*=NaN;
            var loc8:*=NaN;
            var loc9:*=NaN;
            var loc10:*=NaN;
            var loc1:*;
            if ((loc1 = arg4.backgroundImage).length == 0 || loc1 == "null" || loc1 == "undefined") 
            {
                return;
            }
            if (loc1 in bitmapsCache) 
            {
                loc3 = bitmapsCache[loc1];
            }
            else if (arg4.backgroundImageOriginatesFromLibrary) 
            {
                loc3 = new (flash.utils.getDefinitionByName(loc1) as Class)();
            }
            else 
            {
                this.loaderWrapper = new LoaderWrapper(arg4.backgroundImage, this.onLoader);
                return;
            }
            this.matrix.identity();
            if (arg4.backgroundSize != lesta.unbound.style.UbStyle.BACKGROUND_SIZE_NOT_SET) 
            {
                loc8 = arg2 / arg3;
                loc10 = (loc9 = loc3.width / loc3.height) > loc8 ? arg3 / loc3.height : arg2 / loc3.width;
                this.matrix.scale(loc10, loc10);
                this.matrix.translate(0.5 * (arg2 - loc10 * loc3.width), 0.5 * (arg3 - loc10 * loc3.height));
                loc6 = arg2;
                loc7 = arg3;
            }
            else 
            {
                loc4 = arg4.backgroundStretchX ? arg2 : loc3.width;
                loc5 = arg4.backgroundStretchY ? arg3 : loc3.height;
                this.matrix.scale(loc4 / loc3.width, loc5 / loc3.height);
                loc6 = arg4.backgroundRepeatX ? arg2 : loc4;
                loc7 = arg4.backgroundRepeatY ? arg3 : loc5;
            }
            var loc2:*;
            (loc2 = arg1.graphics).clear();
            loc2.beginBitmapFill(loc3, this.matrix, true, true);
            loc2.drawRect(0, 0, loc6, loc7);
            loc2.endFill();
            return;
        }

        public function paintFilters():void
        {
            paintBgFilters(this.element, this.element.computedStyle);
            return;
        }

        internal static function paintBgFilters(arg1:lesta.unbound.layout.UbBlock, arg2:lesta.unbound.style.UbStyle):void
        {
            var loc1:*=arg1.getFiltersTarget().filters;
            loc1.length = 0;
            if (arg2.dropShadowFilter != null) 
            {
                loc1.push(arg2.dropShadowFilter);
            }
            if (arg2.glowFilter != null) 
            {
                loc1.push(arg2.glowFilter);
            }
            if (arg2.blurFilter != null) 
            {
                loc1.push(arg2.blurFilter);
            }
            if (arg2.colorMatrixFilter != null) 
            {
                loc1.push(arg2.colorMatrixFilter);
            }
            arg1.getFiltersTarget().filters = loc1;
            return;
        }

        
        {
            bitmapsCache = {};
        }

        internal var matrix:flash.geom.Matrix;

        internal var element:lesta.unbound.layout.UbBlock;

        internal var loaderWrapper:LoaderWrapper;

        public static var bitmapsCache:Object;
    }
}

import flash.display.*;
import flash.events.*;
import flash.net.*;


class LoaderWrapper extends Object
{
    public function LoaderWrapper(arg1:String, arg2:Function)
    {
        super();
        this.callback = arg2;
        this.imageId = arg1;
        this.loader = new flash.display.Loader();
        this.loader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, this.onLoad);
        this.loader.load(new flash.net.URLRequest(arg1));
        return;
    }

    public function fini():void
    {
        this.loader.removeEventListener(flash.events.Event.COMPLETE, this.onLoad);
        this.loader.close();
        this.loader = null;
        this.callback = null;
        this.imageId = null;
        return;
    }

    internal function onLoad(arg1:flash.events.Event):void
    {
        this.callback(this.imageId, flash.display.Bitmap(this.loader.content).bitmapData);
        this.fini();
        return;
    }

    internal var callback:Function;

    internal var imageId:String;

    internal var loader:flash.display.Loader;
}