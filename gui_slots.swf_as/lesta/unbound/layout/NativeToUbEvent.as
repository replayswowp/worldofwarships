package lesta.unbound.layout 
{
    import flash.events.*;
    
    public class NativeToUbEvent extends flash.events.Event
    {
        public function NativeToUbEvent(arg1:String, arg2:Object, arg3:Boolean=true, arg4:Boolean=false)
        {
            this._eventData = arg2;
            super(arg1, arg3, arg4);
            return;
        }

        public function get eventData():Object
        {
            return this._eventData;
        }

        public static const TRANSFER:String="NativeToUbEvent.NativeToUbEvent";

        internal var _eventData:Object;
    }
}
