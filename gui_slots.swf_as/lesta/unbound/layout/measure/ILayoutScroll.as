package lesta.unbound.layout.measure 
{
    public interface ILayoutScroll
    {
        function updateScroller():void;

        function scrollTo(arg1:Number, arg2:Function=null):void;
    }
}
