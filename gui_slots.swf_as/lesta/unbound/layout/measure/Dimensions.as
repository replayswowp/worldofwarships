package lesta.unbound.layout.measure 
{
    import lesta.unbound.core.*;
    import lesta.unbound.layout.*;
    
    public class Dimensions extends Object
    {
        public function Dimensions(arg1:lesta.unbound.layout.UbStyleable)
        {
            super();
            this._block = arg1;
            return;
        }

        public function get x():Number
        {
            return this.intX + this.extX;
        }

        public function get y():Number
        {
            return this.intY + this.extY;
        }

        public function fini():void
        {
            this._block = null;
            return;
        }

        public function get marginTop():Number
        {
            return this.computeDimensionValue(this._block.computedStyle.marginTop, this._block.computedStyle.marginTopMode, this.height);
        }

        public function get marginBottom():Number
        {
            return isNaN(this._block.computedStyle.marginBottom) ? 0 : this.computeDimensionValue(this._block.computedStyle.marginBottom, this._block.computedStyle.marginBottomMode, this.height);
        }

        public function get marginLeft():Number
        {
            return this.computeDimensionValue(this._block.computedStyle.marginLeft, this._block.computedStyle.marginLeftMode, this.width);
        }

        public function get marginRight():Number
        {
            return isNaN(this._block.computedStyle.marginRight) ? 0 : this.computeDimensionValue(this._block.computedStyle.marginRight, this._block.computedStyle.marginRightMode, this.width);
        }

        public function get measuredWidth():int
        {
            return this.width * this._block.computedStyle.scaleX;
        }

        public function get measuredHeight():int
        {
            return this.height * this._block.computedStyle.scaleY;
        }

        public function get fullWidth():Number
        {
            return this.measuredWidth + this.marginLeft + this.marginRight;
        }

        public function get fullHeight():Number
        {
            return this.measuredHeight + this.marginTop + this.marginBottom;
        }

        public function updatePosition(arg1:Number, arg2:Number, arg3:Number, arg4:Number):void
        {
            var loc1:*=NaN;
            var loc2:*=NaN;
            var loc3:*=NaN;
            var loc4:*=NaN;
            if (isNaN(this._block.computedStyle.right)) 
            {
                if (isNaN(this._block.computedStyle.left)) 
                {
                    this.intX = 0;
                }
                else 
                {
                    loc2 = this.computeDimensionValue(this._block.computedStyle.left, this._block.computedStyle.leftMode, arg3);
                    this.intX = arg1 + loc2;
                }
            }
            else 
            {
                loc1 = this.computeDimensionValue(this._block.computedStyle.right, this._block.computedStyle.rightMode, arg3);
                this.intX = arg1 + arg3 - this.width - loc1;
            }
            if (isNaN(this._block.computedStyle.bottom)) 
            {
                if (isNaN(this._block.computedStyle.top)) 
                {
                    this.intY = 0;
                }
                else 
                {
                    loc4 = this.computeDimensionValue(this._block.computedStyle.top, this._block.computedStyle.topMode, arg4);
                    this.intY = arg2 + loc4;
                }
            }
            else 
            {
                loc3 = this.computeDimensionValue(this._block.computedStyle.bottom, this._block.computedStyle.bottomMode, arg4);
                this.intY = arg2 + arg4 - this.height - loc3;
            }
            this._block.nativeX = this.x;
            this._block.nativeY = this.y;
            return;
        }

        public function computeDimensionValue(arg1:Number, arg2:int, arg3:Number):Number
        {
            return lesta.unbound.core.UbHelpers.computeDimensionValue(arg1, arg2, arg3, this._block);
        }

        public var width:int=0;

        public var height:int=0;

        public var intX:int=0;

        public var intY:int=0;

        public var extX:int=0;

        public var extY:int=0;

        internal var _block:lesta.unbound.layout.UbStyleable;
    }
}
