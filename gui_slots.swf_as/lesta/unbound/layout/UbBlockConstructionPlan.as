package lesta.unbound.layout 
{
    import __AS3__.vec.*;
    import lesta.unbound.expression.*;
    import lesta.unbound.style.*;
    
    public class UbBlockConstructionPlan extends Object
    {
        public function UbBlockConstructionPlan()
        {
            this.bindings = new Vector.<lesta.unbound.layout.UbBindingData>();
            super();
            this.settingsDict = {};
            return;
        }

        public function createFromXml(arg1:XML, arg2:lesta.unbound.layout.UbBlockFactory):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=0;
            var loc8:*=null;
            var loc9:*=null;
            var loc10:*=null;
            var loc11:*=null;
            var loc12:*=null;
            var loc13:*=null;
            var loc14:*=null;
            var loc15:*=null;
            this.styleFragments = {};
            var loc16:*=0;
            var loc17:*=arg1.style;
            for each (loc1 in loc17) 
            {
                loc8 = styleParser.parseXml(loc1);
                var loc18:*=0;
                var loc19:*=loc8;
                for (loc9 in loc19) 
                {
                    this.styleFragments[loc9] = loc8[loc9];
                }
            }
            loc2 = [];
            loc16 = 0;
            loc17 = arg1.styleClass;
            for each (loc3 in loc17) 
            {
                loc2.push(String(loc3.attribute("value")));
            }
            loc16 = 0;
            loc17 = arg1.bind;
            for each (loc4 in loc17) 
            {
                loc10 = loc4.attribute("name");
                loc11 = loc4.attribute("value");
                loc12 = this.bindingsFromString(loc10, loc11, arg2, loc4.toXMLString());
                this.bindings.push(loc12);
            }
            this.createChildrenPlans(arg1, arg2);
            loc16 = 0;
            loc17 = arg1.params;
            for each (loc5 in loc17) 
            {
                loc18 = 0;
                loc19 = loc5.param;
                for each (loc13 in loc19) 
                {
                    if ((loc14 = loc13.attribute("value")) != "true") 
                    {
                        if (loc14 != "false") 
                        {
                            if (loc14.charAt(0) == "[" && loc14.charAt((loc14.length - 1)) == "]") 
                            {
                                loc15 = loc14.substring(1, (loc14.length - 1)).split(",");
                            }
                            else 
                            {
                                loc15 = loc14;
                            }
                        }
                        else 
                        {
                            loc15 = false;
                        }
                    }
                    else 
                    {
                        loc15 = true;
                    }
                    this.settingsDict[String(loc13.attribute("name"))] = loc15;
                }
            }
            this.styleClassesImplementations = {};
            loc7 = 0;
            while (loc7 < loc2.length) 
            {
                loc6 = loc2[loc7];
                this.styleClassesImplementations[loc6] = arg2.getStyleClassById(loc6);
                ++loc7;
            }
            return;
        }

        public function constructAsRoot(arg1:lesta.unbound.layout.UbBlockFactory):lesta.unbound.layout.UbRootBlock
        {
            var loc1:*=new lesta.unbound.layout.UbRootBlock();
            this.principalConstruct(loc1, arg1);
            return loc1;
        }

        public function construct(arg1:lesta.unbound.layout.UbBlockFactory):lesta.unbound.layout.UbBlock
        {
            var loc2:*=null;
            var loc1:*=new lesta.unbound.layout.UbBlock();
            this.principalConstruct(loc1, arg1);
            var loc3:*=0;
            var loc4:*=this.settingsDict;
            for (loc2 in loc4) 
            {
                loc1[loc2] = this.settingsDict[loc2];
            }
            return loc1;
        }

        protected function principalConstruct(arg1:lesta.unbound.layout.UbBlock, arg2:lesta.unbound.layout.UbBlockFactory):void
        {
            arg1.setFactory(arg2);
            arg1.setBaseStyle(this.styleClassesImplementations, this.styleFragments);
            arg1.bindingsSettings = this.bindings;
            this.constructChildren(arg1, arg2);
            return;
        }

        protected function constructChildren(arg1:lesta.unbound.layout.UbBlock, arg2:lesta.unbound.layout.UbBlockFactory):void
        {
            var loc3:*=null;
            var loc1:*=0;
            var loc2:*=this.childrenPlans.length;
            while (loc1 < loc2) 
            {
                loc3 = this.childrenPlans[loc1].construct(arg2);
                arg1.addChildProperly(loc3);
                ++loc1;
            }
            return;
        }

        protected function bindingsFromString(arg1:String, arg2:String, arg3:lesta.unbound.layout.UbBlockFactory, arg4:String):lesta.unbound.layout.UbBindingData
        {
            var loc3:*=0;
            var loc4:*=0;
            var loc1:*=[];
            var loc2:*=new Vector.<lesta.unbound.expression.IUbExpression>();
            if (arg2.length > 0) 
            {
                loc1 = arg2.split(";");
                loc3 = 0;
                loc4 = loc1.length;
                while (loc3 < loc4) 
                {
                    loc2.push(arg3.parseBindingExpression(loc1[loc3], arg4));
                    ++loc3;
                }
            }
            return new lesta.unbound.layout.UbBindingData(arg1, loc2, loc1);
        }

        protected function createChildrenPlans(arg1:XML, arg2:lesta.unbound.layout.UbBlockFactory):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            this.childrenPlans = new Vector.<lesta.unbound.layout.UbBlockConstructionPlan>();
            var loc4:*=0;
            var loc5:*=arg1.block;
            for each (loc1 in loc5) 
            {
                loc2 = loc1.attribute("type");
                var loc6:*=loc2;
                switch (loc6) 
                {
                    case "native":
                    {
                        loc3 = new lesta.unbound.layout.UbNativeBlockConstructionPlan();
                        break;
                    }
                    case "text":
                    {
                        loc3 = new lesta.unbound.layout.UbTextBlockConstructionPlan();
                        break;
                    }
                    default:
                    {
                        loc3 = new lesta.unbound.layout.UbBlockConstructionPlan();
                    }
                }
                loc3.createFromXml(loc1, arg2);
                this.childrenPlans.push(loc3);
            }
            return;
        }

        protected static const styleParser:lesta.unbound.style.UbStyleParser=new lesta.unbound.style.UbStyleParser();

        protected var childrenPlans:__AS3__.vec.Vector.<lesta.unbound.layout.UbBlockConstructionPlan>;

        protected var settingsDict:Object;

        protected var bindings:__AS3__.vec.Vector.<lesta.unbound.layout.UbBindingData>;

        protected var styleFragments:Object;

        protected var styleClassesImplementations:Object;
    }
}
