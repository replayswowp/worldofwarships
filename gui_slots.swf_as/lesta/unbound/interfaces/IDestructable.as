package lesta.unbound.interfaces 
{
    public interface IDestructable
    {
        function fini():void;
    }
}
