package lesta.unbound.expression 
{
    import __AS3__.vec.*;
    
    public class UbParser extends Object
    {
        public function UbParser()
        {
            super();
            return;
        }

        public function createAST(arg1:__AS3__.vec.Vector.<lesta.unbound.expression.UbToken>, arg2:String):lesta.unbound.expression.IUbASTNode
        {
            this.debugText = arg2;
            this.tokens = arg1;
            this.tokenCount = arg1.length;
            this.pointer = 0;
            var loc1:*=this.ternaryExpression();
            this.tokens = null;
            if (this.pointer < this.tokenCount) 
            {
                throw new Error("Binding expression\'s compilation error: " + arg1);
            }
            return loc1;
        }

        internal function ternaryExpression():lesta.unbound.expression.IUbASTNode
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=this.logicalExpression();
            if (loc1 == null) 
            {
                this.error("condition missing");
            }
            if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_TERNARY_COND)) 
            {
                loc2 = this.ternaryExpression();
                if (loc2 == null) 
                {
                    this.error("missing expression after ? operator");
                }
                if (!this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_TERNARY_ALT)) 
                {
                    this.error("missing : after first alternative");
                }
                loc3 = this.ternaryExpression();
                if (loc3 == null) 
                {
                    this.error("missing false alternative");
                }
                return new lesta.unbound.expression.UbASTNodeCondition(loc1, loc2, loc3);
            }
            return loc1;
        }

        internal function logicalExpression():lesta.unbound.expression.IUbASTNode
        {
            var loc6:*=null;
            var loc7:*=null;
            var loc1:*=[this.comparisonExpression()];
            var loc2:*=[];
            for (;;) 
            {
                if (loc6 = this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_AND, lesta.unbound.expression.UbToken.S_OR)) 
                {
                    if (!(loc7 = this.comparisonExpression())) 
                    {
                        this.error("missing second operand for logical expression");
                    }
                    loc1.push(loc7);
                    loc2.push(loc6.value);
                    continue;
                }
                break;
            }
            var loc3:*=loc1[0];
            var loc4:*=1;
            var loc5:*=loc1.length;
            while (loc4 < loc5) 
            {
                loc3 = new lesta.unbound.expression.UbASTNodeBinaryOp(loc3, loc2[(loc4 - 1)], loc1[loc4]);
                ++loc4;
            }
            return loc3;
        }

        internal function comparisonExpression():lesta.unbound.expression.IUbASTNode
        {
            var loc3:*=null;
            var loc1:*=this.bitwiseExpression();
            var loc2:*=this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_EQ, lesta.unbound.expression.UbToken.S_NEQ, lesta.unbound.expression.UbToken.S_MORE, lesta.unbound.expression.UbToken.S_LESS, lesta.unbound.expression.UbToken.S_EQ_MORE, lesta.unbound.expression.UbToken.S_EQ_LESS);
            if (loc2 == null) 
            {
                loc2 = this.testNextToken(lesta.unbound.expression.UbToken.T_IDENTIFIER, lesta.unbound.expression.UbToken.I_IN);
            }
            if (loc2) 
            {
                loc3 = this.bitwiseExpression();
                if (!loc3) 
                {
                    this.error("missing second operand for comparison expression");
                }
                return new lesta.unbound.expression.UbASTNodeBinaryOp(loc1, loc2.value, loc3);
            }
            return loc1;
        }

        internal function bitwiseExpression():lesta.unbound.expression.IUbASTNode
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=this.arithmeticExpressionAdd();
            for (;;) 
            {
                loc2 = this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_BITWISE_AND, lesta.unbound.expression.UbToken.S_BITWISE_OR, lesta.unbound.expression.UbToken.S_BITWISE_XOR, lesta.unbound.expression.UbToken.S_BITWISE_L_SHIFT, lesta.unbound.expression.UbToken.S_BITWISE_R_SHIFT);
                if (loc2) 
                {
                    loc3 = this.arithmeticExpressionAdd();
                    if (!loc3) 
                    {
                        this.error("missing second operand for bitwise expression");
                    }
                    loc1 = new lesta.unbound.expression.UbASTNodeBinaryOp(loc1, loc2.value, loc3);
                    continue;
                }
                break;
            }
            return loc1;
        }

        internal function arithmeticExpressionAdd():lesta.unbound.expression.IUbASTNode
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=this.arithmeticExpressionMul();
            for (;;) 
            {
                loc2 = this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_ADD, lesta.unbound.expression.UbToken.S_NEG);
                if (loc2) 
                {
                    loc3 = this.arithmeticExpressionMul();
                    if (!loc3) 
                    {
                        this.error("missing second operand for arithmetic expression");
                    }
                    loc1 = new lesta.unbound.expression.UbASTNodeBinaryOp(loc1, loc2.value, loc3);
                    continue;
                }
                break;
            }
            return loc1;
        }

        internal function arithmeticExpressionMul():lesta.unbound.expression.IUbASTNode
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=this.chainExpression();
            for (;;) 
            {
                loc2 = this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_MUL, lesta.unbound.expression.UbToken.S_DIV, lesta.unbound.expression.UbToken.S_MOD);
                if (loc2) 
                {
                    loc3 = this.chainExpression();
                    if (!loc3) 
                    {
                        this.error("missing second operand for arithmetic expression");
                    }
                    loc1 = new lesta.unbound.expression.UbASTNodeBinaryOp(loc1, loc2.value, loc3);
                    continue;
                }
                break;
            }
            return loc1;
        }

        internal function chainExpression():lesta.unbound.expression.IUbASTNode
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc1:*=this.logicalNotExpression();
            for (;;) 
            {
                if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_DOT)) 
                {
                    loc2 = this.testNextToken(lesta.unbound.expression.UbToken.T_IDENTIFIER);
                    if (!loc2) 
                    {
                        this.error("missing identifier after dot");
                    }
                    loc1 = new lesta.unbound.expression.UbASTNodeGetPropertyString(loc1, loc2.value);
                    continue;
                }
                if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_LEFT_BRACKET)) 
                {
                    loc3 = this.ternaryExpression();
                    if (loc3 == null) 
                    {
                        this.error("missing expression in brackets");
                    }
                    if (!this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_RIGHT_BRACKET)) 
                    {
                        this.error("missing closing right bracket");
                    }
                    loc1 = new lesta.unbound.expression.UbASTNodeGetProperty(loc1, loc3);
                    continue;
                }
                if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_LEFT_BRACE)) 
                {
                    if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_RIGHT_BRACE)) 
                    {
                        loc1 = new lesta.unbound.expression.UbASTNodeFunctionCall(loc1);
                    }
                    else 
                    {
                        (loc4 = new Vector.<lesta.unbound.expression.IUbASTNode>()).push(this.ternaryExpression());
                        for (;;) 
                        {
                            if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_COMMA)) 
                            {
                                loc4.push(this.ternaryExpression());
                                continue;
                            }
                            if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_RIGHT_BRACE)) 
                            {
                                break;
                            }
                            this.error("unexpected token in function call");
                        }
                        loc1 = new lesta.unbound.expression.UbASTNodeFunctionCall(loc1, loc4);
                    }
                    continue;
                }
                break;
            }
            return loc1;
        }

        internal function logicalNotExpression():lesta.unbound.expression.IUbASTNode
        {
            var loc1:*=this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_NOT);
            if (loc1) 
            {
                return new lesta.unbound.expression.UbASTNodeUnaryOp(loc1.value, this.logicalNotExpression());
            }
            return this.negateExpression();
        }

        internal function negateExpression():lesta.unbound.expression.IUbASTNode
        {
            var loc1:*=this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_NEG);
            if (loc1) 
            {
                return new lesta.unbound.expression.UbASTNodeUnaryOp(loc1.value, this.negateExpression());
            }
            return this.bitwiseNotExpression();
        }

        internal function bitwiseNotExpression():lesta.unbound.expression.IUbASTNode
        {
            var loc1:*=this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_BITWISE_NOT);
            if (loc1) 
            {
                return new lesta.unbound.expression.UbASTNodeUnaryOp(loc1.value, this.bitwiseNotExpression());
            }
            return this.basicExpression();
        }

        internal function basicExpression():lesta.unbound.expression.IUbASTNode
        {
            var loc1:*=null;
            if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_LEFT_BRACE)) 
            {
                loc1 = this.ternaryExpression();
                if (!this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_RIGHT_BRACE)) 
                {
                    this.error("missing closing brace");
                }
                return loc1;
            }
            return this.atomExpression();
        }

        internal function atomExpression():lesta.unbound.expression.IUbASTNode
        {
            var loc1:*=null;
            if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_LEFT_CURLY)) 
            {
                return this.hashExpression();
            }
            if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_LEFT_BRACKET)) 
            {
                return this.listExpression();
            }
            loc1 = this.testNextToken(lesta.unbound.expression.UbToken.T_NUMBER | lesta.unbound.expression.UbToken.T_IDENTIFIER | lesta.unbound.expression.UbToken.T_STRING);
            if (loc1 == null) 
            {
                this.error("missing identifier or number or string");
            }
            return new lesta.unbound.expression.UbASTNodeTerminal(loc1.type, loc1.value);
        }

        internal function listExpression():lesta.unbound.expression.UbASTNodeList
        {
            var loc1:*=null;
            if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_RIGHT_BRACKET)) 
            {
                return new lesta.unbound.expression.UbASTNodeList([]);
            }
            loc1 = [this.ternaryExpression()];
            for (;;) 
            {
                if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_COMMA)) 
                {
                    loc1.push(this.ternaryExpression());
                    continue;
                }
                if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_RIGHT_BRACKET)) 
                {
                    break;
                }
                this.error("unexpected token in list definition");
            }
            return new lesta.unbound.expression.UbASTNodeList(loc1);
        }

        internal function hashExpression():lesta.unbound.expression.UbASTNodeHash
        {
            var loc1:*=null;
            if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_RIGHT_CURLY)) 
            {
                return new lesta.unbound.expression.UbASTNodeHash(new Vector.<lesta.unbound.expression.UbASTHashPair>());
            }
            loc1 = new Vector.<lesta.unbound.expression.UbASTHashPair>();
            loc1.push(this.hashPair());
            for (;;) 
            {
                if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_COMMA)) 
                {
                    loc1.push(this.hashPair());
                    continue;
                }
                if (this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_RIGHT_CURLY)) 
                {
                    break;
                }
                this.error("unexpected token in hash definition");
            }
            return new lesta.unbound.expression.UbASTNodeHash(loc1);
        }

        internal function hashPair():lesta.unbound.expression.UbASTHashPair
        {
            var loc1:*=this.testNextToken(lesta.unbound.expression.UbToken.T_IDENTIFIER);
            if (!loc1) 
            {
                this.error("expected identifier in hash definition");
            }
            var loc2:*=loc1.value;
            if (!this.testNextToken(lesta.unbound.expression.UbToken.T_SYMBOL, lesta.unbound.expression.UbToken.S_TERNARY_ALT)) 
            {
                this.error("expected colon after hash key");
            }
            var loc3:*=this.ternaryExpression();
            return new lesta.unbound.expression.UbASTHashPair(loc2, loc3);
        }

        internal function testNextToken(arg1:int, ... rest):lesta.unbound.expression.UbToken
        {
            var loc2:*=0;
            var loc3:*=null;
            var loc4:*=0;
            var loc1:*=this.nextToken();
            if (loc1 != null) 
            {
                if (loc1.type & arg1) 
                {
                    if ((loc2 = rest.length) == 0) 
                    {
                        return loc1;
                    }
                    loc3 = loc1.value;
                    loc4 = 0;
                    while (loc4 < loc2) 
                    {
                        if (rest[loc4] == loc3) 
                        {
                            return loc1;
                        }
                        ++loc4;
                    }
                }
                this.returnToken();
                return null;
            }
            return null;
        }

        internal function nextToken():lesta.unbound.expression.UbToken
        {
            if (this.pointer < this.tokenCount) 
            {
                var loc1:*;
                var loc2:*=((loc1 = this).pointer + 1);
                loc1.pointer = loc2;
                return this.tokens[(loc1 = this).pointer];
            }
            return null;
        }

        internal function returnToken():void
        {
            var loc1:*;
            var loc2:*=((loc1 = this).pointer - 1);
            loc1.pointer = loc2;
            return;
        }

        internal function error(arg1:String):*
        {
            throw new Error("UbParser -- error: " + arg1 + "\n at: \n" + this.debugText);
        }

        internal var tokenCount:int;

        internal var tokens:__AS3__.vec.Vector.<lesta.unbound.expression.UbToken>;

        internal var pointer:int;

        internal var debugText:String;
    }
}
