package lesta.unbound.expression 
{
    import __AS3__.vec.*;
    
    public class UbExpressionCompiler extends Object
    {
        public function UbExpressionCompiler()
        {
            super();
            if (expressionClassesByASTType.length == 0) 
            {
                expressionClassesByASTType.push(lesta.unbound.expression.UbExpressionBinaryOperation);
                expressionClassesByASTType.push(lesta.unbound.expression.UbExpressionCondition);
                expressionClassesByASTType.push(lesta.unbound.expression.UbExpressionFunctionCall);
                expressionClassesByASTType.push(lesta.unbound.expression.UbExpressionGetProperty);
                expressionClassesByASTType.push(lesta.unbound.expression.UbExpressionGetPropertyString);
                expressionClassesByASTType.push(lesta.unbound.expression.UbExpressionHash);
                expressionClassesByASTType.push(lesta.unbound.expression.UbExpressionList);
                expressionClassesByASTType.push(lesta.unbound.expression.UBExpressionTerminal);
                expressionClassesByASTType.push(lesta.unbound.expression.UBExpressionUnaryOperation);
            }
            this.lexer = new lesta.unbound.expression.UbLexer();
            this.parser = new lesta.unbound.expression.UbParser();
            return;
        }

        public function fini():void
        {
            this.lexer = null;
            this.parser = null;
            return;
        }

        public function compile(arg1:String, arg2:String="[No debug text]"):lesta.unbound.expression.IUbExpression
        {
            var loc1:*=this.lexer.tokenize(arg1);
            var loc2:*=this.parser.createAST(loc1, arg2);
            return createExpression(loc2);
        }

        public function compileLValueSetter(arg1:String):Function
        {
            var loc1:*=this.lexer.tokenize(arg1);
            var loc2:*=this.parser.createAST(loc1, arg1);
            return this.generateLValueSetter(loc2);
        }

        internal function generateLValueSetter(arg1:lesta.unbound.expression.IUbASTNode):Function
        {
            var ast:lesta.unbound.expression.IUbASTNode;
            var currentNode:lesta.unbound.expression.IUbASTNode;
            var chain:Array;
            var base:String;
            var chainLength:int;
            var propNode:lesta.unbound.expression.UbASTNodeGetProperty;
            var propStringNode:lesta.unbound.expression.UbASTNodeGetPropertyString;

            var loc1:*;
            chain = null;
            base = null;
            chainLength = 0;
            propNode = null;
            propStringNode = null;
            ast = arg1;
            currentNode = ast;
            chain = [];
            for (;;) 
            {
                if (currentNode is lesta.unbound.expression.UbASTNodeTerminal) 
                {
                    base = lesta.unbound.expression.UbASTNodeTerminal(currentNode).value;
                    break;
                }
                if (currentNode is lesta.unbound.expression.UbASTNodeGetProperty) 
                {
                    propNode = lesta.unbound.expression.UbASTNodeGetProperty(currentNode);
                    chain.push(createExpression(propNode.property));
                    currentNode = propNode.source;
                    continue;
                }
                if (currentNode is lesta.unbound.expression.UbASTNodeGetPropertyString) 
                {
                    propStringNode = lesta.unbound.expression.UbASTNodeGetPropertyString(currentNode);
                    chain.push(propStringNode.property);
                    currentNode = propStringNode.source;
                    continue;
                }
                throw new Error("Failed to compile LValueSetter -- invalid node type");
            }
            chainLength = chain.length;
            chain.reverse();
            return function (arg1:*, arg2:*):void
            {
                var loc2:*=undefined;
                var loc3:*=undefined;
                var loc4:*=undefined;
                var loc1:*=arg1;
                if (chainLength != 0) 
                {
                    loc4 = 0;
                    while (loc4 < (chainLength - 1)) 
                    {
                        if ((loc2 = chain[loc4]) is String) 
                        {
                            loc1 = loc1[loc2];
                        }
                        else 
                        {
                            loc3 = loc2 as lesta.unbound.expression.IUbExpression;
                            loc1 = loc1[loc3.eval(arg1)];
                        }
                        ++loc4;
                    }
                    if ((loc2 = chain[(chainLength - 1)]) is String) 
                    {
                        loc1[loc2] = arg2;
                    }
                    else 
                    {
                        loc3 = loc2 as lesta.unbound.expression.IUbExpression;
                        loc1[loc3.eval(arg1)] = arg2;
                    }
                }
                else 
                {
                    loc1[base] = arg2;
                }
                return;
            }
        }

        public static function createExpression(arg1:lesta.unbound.expression.IUbASTNode):lesta.unbound.expression.IUbExpression
        {
            return new expressionClassesByASTType[arg1.astType](arg1);
        }

        public static function createSimpleGetterExpression(arg1:String):lesta.unbound.expression.UbExpressionGetPropertyString
        {
            return new lesta.unbound.expression.UbExpressionGetPropertyString(new lesta.unbound.expression.UbASTNodeGetPropertyString(new lesta.unbound.expression.UbASTNodeTerminal(lesta.unbound.expression.UbToken.T_IDENTIFIER, "$this"), arg1));
        }

        public static const expressionClassesByASTType:__AS3__.vec.Vector.<Class>=new Vector.<Class>();

        internal var lexer:lesta.unbound.expression.UbLexer;

        internal var parser:lesta.unbound.expression.UbParser;
    }
}
