package lesta.unbound.expression 
{
    public class UbExpressionGetPropertyString extends lesta.unbound.expression.UbExpressionBase implements lesta.unbound.expression.IUbExpression
    {
        public function UbExpressionGetPropertyString(arg1:lesta.unbound.expression.UbASTNodeGetPropertyString)
        {
            super(arg1);
            this.property = arg1.property;
            this.fromProc = lesta.unbound.expression.UbExpressionCompiler.createExpression(arg1.source);
            addRequestedProperty(this.property);
            addRequestedPropertiesOfExpression(this.fromProc);
            return;
        }

        public override function eval(arg1:Object):*
        {
            var loc1:*=this.fromProc.eval(arg1);
            return loc1 ? loc1[this.property] : null;
        }

        internal var property:String;

        internal var fromProc:lesta.unbound.expression.IUbExpression;
    }
}
