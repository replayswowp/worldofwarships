package lesta.unbound.expression 
{
    import __AS3__.vec.*;
    
    public class UbASTNodeFunctionCall extends Object implements lesta.unbound.expression.IUbASTNode
    {
        public function UbASTNodeFunctionCall(arg1:lesta.unbound.expression.IUbASTNode, arg2:__AS3__.vec.Vector.<lesta.unbound.expression.IUbASTNode>=null)
        {
            super();
            this.source = arg1;
            this.args = arg2;
            return;
        }

        public function get astType():int
        {
            return lesta.unbound.expression.UbASTNodeType.FUNCTION_CALL;
        }

        public var source:lesta.unbound.expression.IUbASTNode;

        public var args:__AS3__.vec.Vector.<lesta.unbound.expression.IUbASTNode>;
    }
}
