package lesta.unbound.core 
{
    import lesta.unbound.layout.*;
    
    public interface IGenerator
    {
        function getItemByIndex(arg1:int):lesta.unbound.layout.UbBlock;

        function getItemById(arg1:*):lesta.unbound.layout.UbBlock;

        function freeItem(arg1:lesta.unbound.layout.UbBlock):void;

        function get length():int;
    }
}
