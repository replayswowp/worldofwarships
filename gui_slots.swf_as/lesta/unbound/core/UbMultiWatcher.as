package lesta.unbound.core 
{
    import __AS3__.vec.*;
    import lesta.unbound.expression.*;
    
    public class UbMultiWatcher extends Object implements lesta.unbound.core.IWatcher
    {
        public function UbMultiWatcher(arg1:__AS3__.vec.Vector.<lesta.unbound.expression.IUbExpression>, arg2:lesta.unbound.core.UbScope, arg3:Function)
        {
            super();
            this.expressions = arg1;
            this.oldValues = [];
            this.newValues = [];
            var loc1:*=0;
            var loc2:*=arg1.length;
            while (loc1 < loc2) 
            {
                arg2.addWatcher(arg1[loc1], this);
                this.oldValues[loc1] = null;
                ++loc1;
            }
            this.scope = arg2;
            this.onChange = arg3;
            return;
        }

        public function onChanged(... rest):void
        {
            if (!this.dirty) 
            {
                this.scope.dirtyWatchers.push(this);
            }
            this.dirty = true;
            return;
        }

        public function update():void
        {
            if (!this.dirty) 
            {
                return;
            }
            this.dirty = false;
            var loc1:*=false;
            var loc2:*=0;
            var loc3:*=this.expressions.length;
            while (loc2 < loc3) 
            {
                this.newValues[loc2] = this.expressions[loc2].eval(this.scope);
                if (!loc1 && (!(this.newValues[loc2] == this.oldValues[loc2]) || typeof this.newValues[loc2] == "object")) 
                {
                    loc1 = true;
                }
                ++loc2;
            }
            if (loc1) 
            {
                this.onChange(this.oldValues, this.newValues);
            }
            this.oldValues = this.newValues;
            this.newValues = [];
            return;
        }

        public function currentValue():*
        {
            return this.oldValues;
        }

        public function free():void
        {
            var loc1:*=0;
            var loc2:*=this.expressions.length;
            while (loc1 < loc2) 
            {
                this.scope.removeWatcher(this, this.expressions[loc1]);
                ++loc1;
            }
            this.expressions = null;
            this.newValues = null;
            this.oldValues = null;
            this.scope = null;
            this.onChange = null;
            return;
        }

        public static function create(arg1:Array, arg2:lesta.unbound.core.UbScope, arg3:Function):lesta.unbound.core.UbMultiWatcher
        {
            var loc3:*=null;
            var loc1:*=new Vector.<lesta.unbound.expression.IUbExpression>();
            var loc2:*=0;
            while (loc2 < arg1.length) 
            {
                loc3 = lesta.unbound.expression.UbExpressionCompiler.createSimpleGetterExpression(arg1[loc2]);
                loc1.push(loc3);
                ++loc2;
            }
            return new UbMultiWatcher(loc1, arg2, arg3);
        }

        internal var expressions:__AS3__.vec.Vector.<lesta.unbound.expression.IUbExpression>;

        internal var scope:lesta.unbound.core.UbScope;

        internal var oldValues:Array;

        internal var newValues:Array;

        internal var onChange:Function;

        internal var dirty:Boolean=true;
    }
}
