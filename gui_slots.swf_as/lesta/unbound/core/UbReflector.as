package lesta.unbound.core 
{
    import flash.utils.*;
    
    public class UbReflector extends Object
    {
        public function UbReflector()
        {
            super();
            return;
        }

        public static function getPublicMethods(arg1:String, arg2:Object):Array
        {
            if (!(arg1 in reflectionCache)) 
            {
                reflectionCache[arg1] = introspect(arg1, arg2);
            }
            return reflectionCache[arg1].methods;
        }

        public static function getPublicVariables(arg1:String, arg2:Object):Array
        {
            if (!(arg1 in reflectionCache)) 
            {
                reflectionCache[arg1] = introspect(arg1, arg2);
            }
            return reflectionCache[arg1].variables;
        }

        public static function getAccessors(arg1:String, arg2:Object, arg3:Array):Array
        {
            if (!(arg1 in reflectionCache)) 
            {
                reflectionCache[arg1] = introspect(arg1, arg2);
            }
            return reflectionCache[arg1].accessors.filter(accessFilter(arg3));
        }

        internal static function introspect(arg1:String, arg2:Object):Object
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=null;
            var loc1:*=flash.utils.describeType(arg2);
            var loc2:*=[];
            var loc8:*=0;
            var loc9:*=loc1.variable;
            for each (loc3 in loc9) 
            {
                loc2.push({"name":loc3.attribute("name"), "type":loc3.attribute("type")});
            }
            loc4 = [];
            loc8 = 0;
            loc9 = loc1.method;
            for each (loc5 in loc9) 
            {
                loc4.push({"name":loc5.attribute("name"), "returnType":loc5.attribute("returnType"), "declaredBy":loc5.attribute("declaredBy")});
            }
            loc6 = [];
            loc8 = 0;
            loc9 = loc1.accessor;
            for each (loc7 in loc9) 
            {
                loc6.push({"name":loc7.attribute("name"), "type":loc7.attribute("type"), "access":loc7.attribute("access")});
            }
            return {"variables":loc2, "methods":loc4, "accessors":loc6};
        }

        internal static function accessFilter(arg1:Array):Function
        {
            var accessTypes:Array;

            var loc1:*;
            accessTypes = arg1;
            return function (arg1:Object, arg2:int, arg3:Array):Boolean
            {
                var loc2:*=undefined;
                var loc1:*=false;
                var loc3:*=0;
                var loc4:*=accessTypes;
                for each (loc2 in loc4) 
                {
                    loc1 = loc1 || arg1.access == loc2;
                }
                return loc1;
            }
        }

        
        {
            reflectionCache = new flash.utils.Dictionary();
        }

        public static const READ_WRITE:String="readwrite";

        public static const READ_ONLY:String="readonly";

        public static const WRITE_ONLY:String="writeonly";

        internal static var reflectionCache:flash.utils.Dictionary;
    }
}
