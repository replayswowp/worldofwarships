package lesta.unbound.core 
{
    import flash.events.*;
    
    public class UbDataRef extends flash.events.EventDispatcher
    {
        public function UbDataRef()
        {
            super();
            return;
        }

        public function setRef(arg1:Object):void
        {
            if (this.ref == arg1) 
            {
                return;
            }
            this.ref = arg1;
            dispatchEvent(new flash.events.Event(flash.events.Event.CHANGE));
            return;
        }

        public var ref:Object=null;
    }
}
