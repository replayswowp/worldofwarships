package lesta.structs 
{
    import flash.utils.*;
    
    public class DailyQuestInfo extends Object
    {
        public function DailyQuestInfo()
        {
            this.timer = new flash.utils.Timer(1000);
            super();
            return;
        }

        public function set iconId(arg1:int):void
        {
            this._iconId = arg1;
            this.backgroundURL = this.iconsPath + "quest_bg_" + String(arg1) + ".png";
            return;
        }

        public function get iconId():int
        {
            return this._iconId;
        }

        internal const iconsPath:String="../quests/";

        public var id:int=0;

        public var type:int=1;

        public var titleText:String="";

        public var descriptionText:String="";

        public var rewardType:int=0;

        public var rewardAddType:int=0;

        public var rewardNum:Number=0;

        public var progressMax:Number=0;

        public var progressCurrent:Number=0;

        public var backgroundURL:String="";

        public var isSeen:Boolean=false;

        public var isActive:Boolean=true;

        public var day:int=1;

        public var month:int=1;

        public var monthIDS:String="IDS_MONTH_1";

        public var timeLeftFormatted:String="";

        public var timeLeft:Number=0;

        public var timeToChange:Number=0;

        public var inOneBattle:Boolean=false;

        internal var _iconId:int=0;

        internal var timer:flash.utils.Timer;
    }
}
