package lesta.structs 
{
    public class CrewToHireSingleton extends Object
    {
        public function CrewToHireSingleton()
        {
            super();
            return;
        }

        public var baseUrl:String;

        public var overlayUrl:String;

        public var firstNameIDS:String;

        public var lastNameIDS:String;

        public var baseTrainingHirePrice:int=0;

        public var moneyTrainingHirePrice:int=0;

        public var goldTrainingHirePrice:int=0;

        public var baseTrainingLevel:int=1;

        public var moneyTrainingLevel:int=2;

        public var goldTrainingLevel:int=3;

        public var rank:int=0;
    }
}
