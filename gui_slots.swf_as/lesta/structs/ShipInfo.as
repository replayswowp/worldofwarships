package lesta.structs 
{
    import lesta.utils.*;
    
    public class ShipInfo extends Object
    {
        public function ShipInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return lesta.utils.Debug.formatString("[ShipInfo id: %, fullName : %, stypeIdent : %, country : %]", [this.id, this.fullName, this.stypeIdent, this.country]);
        }

        public var id:Number;

        public var isPremium:Boolean;

        public var fullName:String;

        public var level:int;

        public var rlevel:String;

        public var shortName:String;

        public var nameIDS:String;

        public var stypeIdent:String;

        public var stypeIDS:String;

        public var country:String;

        public var pathPreview:String;

        public var pathPreviewDS:String;

        public var pathBigPreview:String;

        public var pathTinyIcon:String;

        public var pathTinyDeadIcon:String;

        public var pathOwnTinyIcon:String;

        public var pathBigIcon:String;

        public var pathBigIconBg:String;

        public var isPaperShip:Boolean;

        public var shipInfoCollection:Object;
    }
}
