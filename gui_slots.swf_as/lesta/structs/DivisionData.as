package lesta.structs 
{
    public class DivisionData extends Object
    {
        public function DivisionData()
        {
            super();
            return;
        }

        public var divisionExist:Boolean;

        public var startBattleAvailable:Boolean;

        public var startDisableReason:int;

        public var isCommander:Boolean;

        public var isReady:Boolean;

        public var commanderId:int;

        public var maxPlayersInDivision:int;
    }
}
