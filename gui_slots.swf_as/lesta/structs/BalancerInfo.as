package lesta.structs 
{
    public class BalancerInfo extends Object
    {
        public function BalancerInfo()
        {
            super();
            return;
        }

        public var maxWaitMin:int;

        public var maxWaitSec:int;

        public var fullTeamOnly:Boolean;

        public var playersTotal:int;

        public var playersInRange:int;

        public var destroyerNum:int;

        public var carrierNum:int;

        public var cruiserNum:int;

        public var battleshipNum:int;
    }
}
