package lesta.structs 
{
    public class DivisionEntranceData extends Object
    {
        public function DivisionEntranceData()
        {
            super();
            return;
        }

        public var loaded:Boolean;

        public var seekerStatus:Boolean;

        public var seekersList:Array;
    }
}
