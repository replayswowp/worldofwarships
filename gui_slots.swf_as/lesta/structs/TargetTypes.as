package lesta.structs 
{
    public class TargetTypes extends Object
    {
        public function TargetTypes()
        {
            super();
            return;
        }

        public static const INVALID:int=-1;

        public static const SHIP:int=0;

        public static const PLANE:int=1;

        public static const TORPEDO:int=2;

        public static const CAPTURE_POINT:int=11;

        public static const PLAYER:int=12;

        public static const EMPTY:int=100;
    }
}
