package lesta.structs 
{
    import lesta.utils.*;
    
    public class UpgradableShipInfo extends Object
    {
        public function UpgradableShipInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return lesta.utils.Debug.formatString("[UpgradableShipInfo name: %s shipId: %s]", [this.name, this.shipId]);
        }

        public var id:Number;

        public var isValid:Boolean;

        public var err:String;

        public var name:String;

        public var prevName:String;

        public var isExplored:Boolean;

        public var dependencyResearched:Boolean;

        public var dependentModules:Array;

        public var isOwned:Boolean;

        public var shipId:Number;

        public var costXP:int;

        public var costFreeXP:int;

        public var totalCostXP:int;

        public var costGold:int;

        public var costCR:int;

        public var expDeficit:int;

        public var creditsDeficit:int;

        public var isElite:Boolean;

        public var isPremium:Boolean;

        public var isHardcodedShip:Boolean;
    }
}
