package lesta.structs 
{
    public class MapInfo extends Object
    {
        public function MapInfo()
        {
            super();
            return;
        }

        public static const ID:String="id";

        public var id:int;

        public var IDS:String;

        public var lobbyPrevPath:String;

        public var minimapPath:String;

        public var customRoomPath:String;

        public var mapBGPath:String;

        public var enabled:Boolean;
    }
}
