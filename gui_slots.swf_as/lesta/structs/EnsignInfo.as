package lesta.structs 
{
    public class EnsignInfo extends Object
    {
        public function EnsignInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "[EnsignInfo] " + "id: " + this.id + " ,installed: " + this.installed + " ,iconPath: " + this.iconPath;
        }

        public var id:String;

        public var iconPath:String;

        public var desaturatedIconPath:String;

        public var installed:Boolean;

        public var params:Array;
    }
}
