package lesta.structs 
{
    import flash.geom.*;
    
    public class CameraInfo extends Object
    {
        public function CameraInfo()
        {
            this.rectView = new flash.geom.Rectangle();
            super();
            return;
        }

        public var yaw:Number=0;

        public var x:Number=0;

        public var y:Number=0;

        public var frustumProjection:Array;

        public var frustumAngle:Number;

        public var heightPercent:Number=0;

        public var rectView:flash.geom.Rectangle;

        public var isRectVisible:Boolean=false;

        public var directionLineAngle:Number=0;

        public var type:int=-1;

        public var modeIndex:int=-1;

        public var zoom:Number=1;

        public var zoomUp:Boolean=true;

        public var visibilityRange:Number=0;

        public var shootRange:Number=0;
    }
}
