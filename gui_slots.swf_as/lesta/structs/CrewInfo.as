package lesta.structs 
{
    public class CrewInfo extends Object
    {
        public function CrewInfo()
        {
            super();
            return;
        }

        public var freeSkillPoints:int;

        public var level:int;

        public var learnedSkillIDs:Array;

        public var experience:int;

        public var nation:String;

        public var isInBarracks:Boolean;

        public var skillSet:Array;

        public var skillsEffectiveness:Number;

        public var id:int;

        public var name:String;

        public var adaptationExperiencePenalty:int;

        public var adaptationBlockedSkillIDs:Array;

        public var adaptationExperienceEarned:int;

        public var skillsResetPrice:int;

        public var specialization:String;

        public var specializationID:Number;

        public var experienceToRichNextLevel:int=0;

        public var shipName:String;

        public var shipID:Number;

        public var allSkillPoints:int;

        public var suitableShips:Array;

        public var creditsRetrainingPrice:int=0;

        public var goldRetrainingPrice:int=0;

        public var needRetraining:Boolean=false;

        public var penaltyForGoldAdaptation:int;

        public var penaltyForSilverAdaptation:int;

        public var penaltyForFreeAdaptation:int;

        public var penaltyExpForGoldAdaptation:int;

        public var penaltyExpForSilverAdaptation:int;

        public var penaltyExpForFreeAdaptation:int;

        public var isInAdaptation:Boolean;

        public var canResetSkills:Boolean;

        public var hasSpaceToAssignCrew:Object;

        public var suitableShipsAdaptationPenalties:Object;

        public var baseUrl:String;

        public var overlayUrl:String;

        public var rank:String;

        public var firstNameIDS:String;

        public var lastNameIDS:String;
    }
}
