package lesta.structs 
{
    import lesta.constants.*;
    import lesta.utils.*;
    
    public class Player extends lesta.structs.EntityInfo
    {
        public function Player()
        {
            this.shipParams = {};
            super();
            var loc1:*;
            entityId = LAST_PLAYER_ENTITY_ID++;
            return;
        }

        
        {
            LAST_PLAYER_ENTITY_ID = 0;
        }

        public function updateShipInfoData():void
        {
            var loc1:*=lesta.utils.GameInfoHolder.instance.getDataByID(lesta.utils.GameInfoHolder.SHIPS, this.shipId) as lesta.structs.ShipInfo;
            if (loc1 != null) 
            {
                this.shipTypeIdent = loc1.stypeIdent;
                this.shipTypeIndex = lesta.constants.ShipTypes.getIdByIdent(loc1.stypeIdent);
                this.shipTypeSortIndex = lesta.constants.ShipTypes.getOrderSortIndex(loc1.stypeIdent);
                this.shipName = loc1.fullName;
                this.pathShipTinyIcon = loc1.pathTinyIcon;
                this.pathOwnShipTinyIcon = loc1.pathOwnTinyIcon;
                this.pathDeadShipIcon = loc1.pathTinyDeadIcon;
                this.shipIDS = loc1.nameIDS;
                this.shipLevel = loc1.level;
                this.shipRlevel = lesta.constants.RDigits.rdigits[loc1.level];
            }
            return;
        }

        public function getUnionText():String
        {
            if (!(relation == lesta.constants.PlayerRelation.SELF) && this.isInSameDivision) 
            {
                return this.tkStatus ? "division_teamkiller" : "division";
            }
            if (this.tkStatus && !(relation == lesta.constants.PlayerRelation.ENEMY)) 
            {
                return relation != lesta.constants.PlayerRelation.SELF ? "teamkiller" : "self_teamkiller";
            }
            return UNIONS[relation];
        }

        public function getFrameLabel():String
        {
            return this.getUnionText() + "," + this.getLifeStateText();
        }

        public function getLifeStateText():String
        {
            return LIFE_STATES[int(isAlive)];
        }

        public override function toString():String
        {
            return lesta.utils.Debug.formatString("[PlayerInfo id: %, name: %]", [id, this.name]);
        }

        internal static const UNIONS:Array=["player", "ally", "enemy", "neutral", "teamkiller", "self_teamkiller"];

        public static const SHIP_TYPE_SORT_INDEX:String="shipTypeSortIndex";

        public static const SHIP_LEVEL:String="shipLevel";

        public static const SHIP_NAME:String="shipName";

        public static const NAME:String="name";

        internal static const LIFE_STATES:Array=["dead", "alive"];

        public var shipLevel:int=0;

        public var shipRlevel:String="";

        public var maxHealth:uint=100;

        public var maxBuoyancy:uint=100;

        public var health:Number=100;

        public var buoyancy:Number=100;

        public var fragsCount:int=0;

        public var damage:Number=0;

        public var buoyancyDamage:Number=0;

        public var selfDamage:Number=0;

        public var selfBuoyancyDamage:Number=0;

        public var mirrorDamage:Number=0;

        public var shipTypeIndex:int=0;

        public var regenerationBuoyancy:Number=100;

        public var vehicleVisible:Boolean=false;

        public var isInSameDivision:Boolean=false;

        public var isObserved:Boolean=false;

        public var tkStatus:Boolean=false;

        public var isMainGunFocusTarget:Boolean=false;

        public var isATBAPriorTarget:Boolean=false;

        public var currentAttackedTarget:int=-1;

        public var isReady:Boolean=false;

        public var isConnected:Boolean=false;

        public var shipParams:Object;

        public var name:String="nobody";

        public var division:int;

        public var shipId:Number=0;

        public var level:int=0;

        public var shipTypeIdent:String="";

        public var shipTypeSortIndex:int=0;

        public var regenerationHealth:Number=100;

        public static var LAST_PLAYER_ENTITY_ID:int=0;

        public var shipName:String="";

        public var pathShipTinyIcon:String="";

        public var pathOwnShipTinyIcon:String="";

        public var pathDeadShipIcon:String="";

        public var shipIDS:String="";
    }
}
