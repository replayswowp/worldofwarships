package lesta.structs 
{
    public class AccountResourceInfo extends Object
    {
        public function AccountResourceInfo()
        {
            super();
            return;
        }

        public var gold:Number=0;

        public var freeExp:Number=0;

        public var credits:Number=0;

        public var slotsNum:Number=0;

        public var isPremium:Boolean;

        public var premiumTimeStr:String="";

        public var canBuyPremium:Boolean=true;

        public var goldIsLocked:Boolean=false;
    }
}
