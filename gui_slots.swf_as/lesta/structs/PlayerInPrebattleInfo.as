package lesta.structs 
{
    public class PlayerInPrebattleInfo extends Object
    {
        public function PlayerInPrebattleInfo()
        {
            super();
            return;
        }

        public var id:int;

        public var name:String;

        public var shipId:Number;

        public var teamId:int;

        public var state:int;

        public var isInBattle:Boolean;

        public var isCommander:Boolean;

        public var isReady:Boolean;

        public var isConnected:Boolean;

        public var isCurrentPlayer:Boolean;

        public var tkStatus:Boolean;

        public var rankDenyReason:uint;
    }
}
