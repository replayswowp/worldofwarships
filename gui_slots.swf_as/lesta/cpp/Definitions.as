package lesta.cpp 
{
    public class Definitions extends Object
    {
        public function Definitions()
        {
            super();
            return;
        }

        public static const TWEEN_EXTENSION_CALL:String="cpp_extension_call_tween";

        public static const TWEEN_METHOD_TO:int=0;

        public static const TWEEN_METHOD_KILL:int=1;

        public static const TRANSLATOR_EXTENSION_CALL:String="cpp_extension_call_translator";

        public static const TRANSLATOR_METHOD_TRANSLATE:int=0;

        public static const TRANSLATOR_METHOD_TRANSLATE_PLURAL:int=1;
    }
}
