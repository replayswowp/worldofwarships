﻿package monstrofil.components {
	import flash.display.Sprite;
	import lesta.utils.Geometry;
	import flash.display.LineScaleMode;
	import monstrofil.utils.DrawingShapes;
	
	public class Arc extends Sprite {
		var radius:Number;
		var w:Number;
		public function Arc(x:Number, y:Number, radius:Number, w:Number) {
			this.x = x;
			this.y = y;
			this.radius = radius;
			this.w = w;

			
		}
		
		public function draw(angle_from, angle_to, color=0xccFFcc, precision=5) {
			this.graphics.clear();
			this.graphics.lineStyle(w, color, 1, false, LineScaleMode.NONE);
			DrawingShapes.drawArc(this.graphics, this.x, this.y, this.radius, angle_to, -90);
		}

	}
	
}
