﻿package 
{
    import lesta.dialogs.battle_window._minimap.components.*;
	import monstrofil.global.GlobalData;
	import flash.text.TextField;
	import lesta.cpp.Translator;
	import lesta.structs.Player;
	import flash.text.TextFormat;
	import lesta.constants.PlayerRelation;
	import flash.display.Sprite;
	import flash.display.LineScaleMode;
	import lesta.utils.Geometry;
	import flash.geom.Point;
	import flash.display.Graphics;
	import monstrofil.components.HealthBarCircle;
	import monstrofil.utils.utils;
	import monstrofil.global.MinimapConfig;
	import flash.geom.ColorTransform;
    
    public dynamic class MapShipItemClip extends lesta.dialogs.battle_window._minimap.components.MapShipItem
    {
		public static var selfPlayer:Object = null;
		
        public function MapShipItemClip()
        {
			super();
			txtShipName.filters = [GlobalData.instance.shadowDefault];
			this.typeIcon.addChild(txtShipName);
            return;
        }
		
		public override function setData(arg1:Object):void{
			super.setData(arg1);
			
			var shipFormat:TextFormat = GlobalData.instance.textFormatDefault;
			shipFormat.color = this.playerInfo.relation == PlayerRelation.ENEMY? 0xff3300: 0x66ff33;
			if(this.playerInfo.isInSameDivision)
				shipFormat.color = 0xFFCC66;
			
			this.txtShipName.defaultTextFormat = shipFormat;
			this.txtShipName.text = lesta.cpp.Translator.translate(arg1.shipIDS);
			if(this.playerInfo.isInSameDivision)
				this.txtShipName.text = this.playerInfo.name;
			
			this.txtShipName.visible = this.playerInfo.relation != PlayerRelation.SELF;
			
			if(!healthBarCircle){
				healthBarCircle = new HealthBarCircle(0, this.icon.width * 0.65, this.icon.width * 1.3, MinimapConfig.instance._healthWidth);
				healthBarCircle.alpha = 0.6;
				healthBarCircle.visible = this.playerInfo.relation != PlayerRelation.SELF;
				healthBarCircle.filters = [GlobalData.instance.shadowDefault];
				this.addChild(healthBarCircle);
			}
			
			if (this.playerInfo.relation == PlayerRelation.SELF){
				MapShipItemClip.selfPlayer = this.playerInfo;
			}
		}
		
		public override function refresh():void{
			super.refresh();
			if(this.playerInfo){
				this.txtShipName.rotation = -1 * this.playerInfo.worldYaw * lesta.utils.Geometry.DEG_TO_RAD_COEF; 				
				
				var position:Point = monstrofil.utils.utils.rotatePoint(new Point(MinimapConfig.instance._shipNameDist, MinimapConfig.instance._shipNameDist), new Point(0,0),-this.rotation / lesta.utils.Geometry.DEG_TO_RAD_COEF);  
				this.txtShipName.x = Math.round(position.x);
				this.txtShipName.y = Math.round(position.y);
				if (this.playerInfo.isAlive) 
				{
					if (this.playerInfo.isVisible) 
					{
						var trans:ColorTransform = GlobalData.instance.activeColorTransformDefault;
						if(MinimapConfig.instance.configLoaded)
							if(MinimapConfig.instance.xml.alternative_colors == "true"){
								if(this.playerInfo.relation == PlayerRelation.ENEMY){
									var c:uint = MinimapConfig.instance.xml.alternative_enemy;
									this.txtShipName.textColor = c;
									trans.color = c;
									this.healthBarCircle.transform.colorTransform = trans;
									this.icon.transform.colorTransform = trans;
								}
							}
						this.transform.colorTransform = GlobalData.instance.activeColorTransformDefault;	
								
						this.alpha = 1;
					}
					else 
					{
						this.transform.colorTransform = GlobalData.instance.inactiveColorTransformDefault;
						this.alpha = 0.6;
					}
				}
				if(healthBarCircle){
					healthBarCircle.draw_bar(this.playerInfo.health, this.playerInfo.maxHealth, this.playerInfo.relation == PlayerRelation.ENEMY? 0xff3300: 0x66ff33);
				}
			}
		}
		
		public override function update():void{
            super.update();
			if (this.playerInfo){
				if (this.playerInfo.isAlive) 
					this.visible = true;
				else
					this.visible = false;
			}
            return;
        }
		
		
		
		var txtShipName:TextField = new TextField();
		
		var healthBarCircle:HealthBarCircle = null;
    }
}
