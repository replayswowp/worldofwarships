package scaleform.clik.constants 
{
    public class SoundEvent extends Object
    {
        public function SoundEvent()
        {
            super();
            return;
        }

        public static const SHOW:String="show";

        public static const HIDE:String="hide";

        public static const OUT:String="out";

        public static const CLICK:String="click";

        public static const OVER:String="over";

        public static const PRESS:String="press";

        public static const SELECT:String="select";

        public static const UNSELECT:String="unselect";

        public static const PRESS_HOTKEY:String="press_hotkey";

        public static const RELEASE_HOTKEY:String="release_hotkey";

        public static const LIST_EXPEND:String="list_expend";

        public static const INDEX_CHANGE:String="index_change";

        public static const CHANGE:String="change";

        public static const FOCUSED:String="focused";

        public static const SCROLL:String="scroll";

        public static const ENTER:String="enter";
    }
}
