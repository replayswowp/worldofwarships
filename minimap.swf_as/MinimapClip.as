﻿package 
{
    import lesta.dialogs.battle_window._minimap.*;
    import monstrofil.global.MinimapConfig;
    
    public dynamic class MinimapClip extends lesta.dialogs.battle_window._minimap.Minimap
    {
        public function MinimapClip()
        {
            super();
			
			MinimapConfig.instance.loadConfig();
            return;
        }
    }
}
