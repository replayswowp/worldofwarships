package minimap_fla 
{
    import adobe.utils.*;
    import flash.accessibility.*;
    import flash.desktop.*;
    import flash.display.*;
    import flash.errors.*;
    import flash.events.*;
    import flash.external.*;
    import flash.filters.*;
    import flash.geom.*;
    import flash.globalization.*;
    import flash.media.*;
    import flash.net.*;
    import flash.net.drm.*;
    import flash.printing.*;
    import flash.profiler.*;
    import flash.sampler.*;
    import flash.sensors.*;
    import flash.system.*;
    import flash.text.*;
    import flash.text.engine.*;
    import flash.text.ime.*;
    import flash.ui.*;
    import flash.utils.*;
    import flash.xml.*;
    
    public dynamic class Hotkeys_group_119 extends flash.display.MovieClip
    {
        public function Hotkeys_group_119()
        {
            super();
            this.__setProp_eee_Hotkeys_group_Layer1_0();
            this.__setProp_www_Hotkeys_group_Layer1_0();
            return;
        }

        function __setProp_eee_Hotkeys_group_Layer1_0():*
        {
            try 
            {
                this.eee["componentInspectorSetting"] = true;
            }
            catch (e:Error)
            {
            };
            this.eee.clipClassName = "";
            this.eee.commandId = 70;
            try 
            {
                this.eee["componentInspectorSetting"] = false;
            }
            catch (e:Error)
            {
            };
            return;
        }

        function __setProp_www_Hotkeys_group_Layer1_0():*
        {
            try 
            {
                this.www["componentInspectorSetting"] = true;
            }
            catch (e:Error)
            {
            };
            this.www.clipClassName = "";
            this.www.commandId = 71;
            try 
            {
                this.www["componentInspectorSetting"] = false;
            }
            catch (e:Error)
            {
            };
            return;
        }

        public var www:HotkeyAnimation;

        public var size:flash.display.MovieClip;

        public var eee:HotkeyAnimation;
    }
}
