package lesta.structs 
{
    import lesta.dialogs.custom_hud.*;
    
    public class CustomHudElement extends Object
    {
        public function CustomHudElement()
        {
            this._vglue = new lesta.dialogs.custom_hud.CustomHudGlue();
            this._hglue = new lesta.dialogs.custom_hud.CustomHudGlue();
            super();
            return;
        }

        public function get vglue():lesta.dialogs.custom_hud.CustomHudGlue
        {
            return this._vglue;
        }

        public function set vglue(arg1:*):void
        {
            var loc1:*=undefined;
            if (arg1 is lesta.dialogs.custom_hud.CustomHudGlue) 
            {
                this._vglue = arg1;
            }
            else 
            {
                var loc2:*=0;
                var loc3:*=arg1;
                for (loc1 in loc3) 
                {
                    this._vglue[loc1] = arg1[loc1];
                }
            }
            return;
        }

        public function get hglue():lesta.dialogs.custom_hud.CustomHudGlue
        {
            return this._hglue;
        }

        public function set hglue(arg1:*):void
        {
            var loc1:*=undefined;
            if (arg1 is lesta.dialogs.custom_hud.CustomHudGlue) 
            {
                this._hglue = arg1;
            }
            else 
            {
                var loc2:*=0;
                var loc3:*=arg1;
                for (loc1 in loc3) 
                {
                    this._hglue[loc1] = arg1[loc1];
                }
            }
            return;
        }

        public function toString():String
        {
            return " id " + this.id + " glues " + this._hglue + this.vglue;
        }

        public var id:String;

        public var hasAnimationInside:Boolean;

        internal var _vglue:lesta.dialogs.custom_hud.CustomHudGlue;

        internal var _hglue:lesta.dialogs.custom_hud.CustomHudGlue;

        public var scaleX:Number;

        public var scaleY:Number;

        public var active:Boolean;

        public var tieDepth:int=0;
    }
}
