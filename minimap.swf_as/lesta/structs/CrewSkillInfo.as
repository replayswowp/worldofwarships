package lesta.structs 
{
    public class CrewSkillInfo extends Object
    {
        public function CrewSkillInfo()
        {
            super();
            return;
        }

        public var id:String="";

        public var learned:Boolean;

        public var skillType:int=0;

        public var tier:int=0;

        public var column:int=0;

        public var name:String;

        public var blocked:Boolean;

        public var canBeLearned:Boolean;

        public var hasLearnedSkillOnPreviousTier:Boolean;

        public var hasEnoughSkillPointsToLearnSkill:Boolean;

        public var attributes:Array;
    }
}
