package lesta.structs 
{
    public class AccountLevelInfo extends Object
    {
        public function AccountLevelInfo()
        {
            super();
            return;
        }

        public function get accountLevel():int
        {
            return this._accountLevel;
        }

        public function set accountLevel(arg1:int):void
        {
            this._accountLevel = arg1;
            this.setIcons(arg1, this, false);
            this.updateLevelsList();
            return;
        }

        public function get featureDictionary():Object
        {
            return this._featureDictionary;
        }

        public function set featureDictionary(arg1:Object):void
        {
            this._featureDictionary = arg1;
            this.updateLevelsList();
            return;
        }

        public function toString():String
        {
            return String("name " + this.playerName + " iconLevel " + this.iconLevel);
        }

        internal function updateLevelsList():void
        {
            var loc2:*=undefined;
            var loc3:*=0;
            var loc4:*=false;
            var loc5:*=null;
            this.levels = [];
            var loc1:*=[];
            var loc6:*=0;
            var loc7:*=this._featureDictionary;
            for (loc2 in loc7) 
            {
                loc1.push(loc2);
            }
            loc1.sort(Array.NUMERIC);
            if (loc1.length == 0) 
            {
                return;
            }
            loc6 = 0;
            loc7 = loc1;
            for each (loc3 in loc7) 
            {
                loc4 = !Boolean(this.featureDictionary[loc3]["comingSoon"]);
                loc5 = {"levelNumber":loc3, "isComingSoon":loc4};
                this.setIcons(loc3, loc5, loc4);
                this.levels.push(loc5);
            }
            return;
        }

        internal function setIcons(arg1:int, arg2:Object, arg3:Boolean):void
        {
            arg2.outerClipFrame = this.getOuterIconFrame(arg1, arg3);
            arg2.iconLevel = "../accountLevel/icons/icon_level_big_" + String(arg1) + ".png";
            arg2.outerIcon = "../accountLevel/icons/icon_account_" + arg2.outerClipFrame + ".png";
            arg2.smallIconLevel = "../accountLevel/icons/icon_level_small_" + String(arg1) + ".png";
            arg2.rewards = this.featureDictionary[arg1]["rewards"];
            return;
        }

        internal function getOuterIconFrame(arg1:int, arg2:Boolean):int
        {
            var loc1:*=0;
            if (0 < arg1 && arg1 <= 4) 
            {
                loc1 = 1;
            }
            else if (4 < arg1 && arg1 <= 22) 
            {
                loc1 = 2;
            }
            else 
            {
                loc1 = 3;
            }
            if (arg1 > this._accountLevel) 
            {
                loc1 = 4;
            }
            if (arg2) 
            {
                loc1 = 5;
            }
            return loc1;
        }

        public var playerName:String;

        public var nextLevelProgress:int;

        public var thisLevelProgress:int;

        public var deltaProgress:int;

        public var currentProgress:int;

        public var maxLevelEnabled:int;

        public var featureStateArray:Array;

        public var iconLevel:String;

        public var outerIcon:String;

        public var smallIconLevel:String;

        public var outerClipFrame:int;

        public var rewards:Array;

        public var tkStatus:Boolean;

        public var levels:Array;

        internal var _featureDictionary:Object;

        internal var _accountLevel:int=1;
    }
}
