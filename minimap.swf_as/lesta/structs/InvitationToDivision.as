package lesta.structs 
{
    public class InvitationToDivision extends Object
    {
        public function InvitationToDivision()
        {
            super();
            return;
        }

        public var playerName:String;

        public var senderLevel:int;

        public var expirationTime:Number;

        public var creationTime:Number;

        public var progress:Number;

        public var currentTime:Number;

        public var timeLeft:String;

        public var tkStatus:Boolean;

        public var id:*;
    }
}
