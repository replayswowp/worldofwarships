package lesta.structs 
{
    public class RewardInfo extends Object
    {
        public function RewardInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "AchievementInfo{name=" + this.name + ",id=" + String(this.id) + ",amount=" + String(this.amount) + ",reasons=" + this.reasons + "}";
        }

        internal static const IMAGE_PATH:String="/gui/achievements/icons/icon_achievement_";

        internal static const IMAGE_EXTENSION:String=".png";

        public var id:uint=0;

        public var amount:int=0;

        public var reasons:Array;

        public var name:String;

        public var type:String;

        public var subtype:String;
    }
}
