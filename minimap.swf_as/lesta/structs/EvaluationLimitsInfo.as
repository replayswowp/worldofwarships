package lesta.structs 
{
    public class EvaluationLimitsInfo extends Object
    {
        public function EvaluationLimitsInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "EvaluationLimitsInfo{complain=" + this.complain + ",praise=" + this.praise + "}";
        }

        public var id:uint=0;

        public var complain:int=0;

        public var praise:int=0;
    }
}
