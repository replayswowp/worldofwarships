package lesta.structs 
{
    public class PlayerInvitedToPrebattleInfo extends lesta.structs.PlayerInPrebattleInfo
    {
        public function PlayerInvitedToPrebattleInfo()
        {
            super();
            return;
        }

        public var expirationTime:Number=-1;

        public var creationTime:Number=-1;

        public var progress:Number=-1;

        public var timeLeft:String;

        public var currentTime:Number=-1;
    }
}
