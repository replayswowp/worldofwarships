package lesta.structs 
{
    import lesta.utils.*;
    
    public class ModernizationInfo extends Object
    {
        public function ModernizationInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return lesta.utils.Debug.formatString("[ModernizationInfo shipId: %, slotId : %, title : %, installed : %, description : %]", [this.shipId, this.slotId, this.title, this.installed, this.description]);
        }

        public static const MOUNT:int=1;

        public static const UNMOUNT:int=2;

        public static const BUY:int=4;

        public static const DESTROY:int=8;

        public var id:Number;

        public var shipId:Number;

        public var slotId:int;

        public var modId:String;

        public var modNumericId:Number;

        public var slotsNum:int;

        public var paramsList:Array;

        public var installed:Boolean;

        public var numInStorage:int;

        public var iconPath:String;

        public var title:String;

        public var description:String;
    }
}
