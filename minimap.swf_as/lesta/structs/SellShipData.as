package lesta.structs 
{
    public class SellShipData extends Object
    {
        public function SellShipData()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "SellShipData{shipCost=" + String(this.shipCost) + ",unmountCost=" + String(this.unmountCost) + ",unmountCurrency=" + String(this.unmountCurrency) + ",sellCostMod=" + String(this.sellCostMod) + ",sellCurrency=" + String(this.sellCurrency) + "}";
        }

        public var shipCost:int;

        public var unmountCost:int;

        public var unmountCurrency:int;

        public var sellCostMod:int;

        public var sellCurrency:int;
    }
}
