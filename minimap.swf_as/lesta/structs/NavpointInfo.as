package lesta.structs 
{
    import flash.geom.*;
    import lesta.dialogs.battle_window.constants.*;
    import lesta.dialogs.battle_window.markers.container.views.base.*;
    import lesta.utils.*;
    
    public class NavpointInfo extends Object
    {
        public function NavpointInfo()
        {
            super();
            return;
        }

        public function updateAlphaColor():void
        {
            var loc1:*=lesta.utils.GameInfoHolder.instance.navpointsSettings;
            this.alpha = this.index <= loc1.countActiveNavpoints && !(this.type == lesta.dialogs.battle_window.constants.NavpointTypes.FAKE) ? loc1.alphaActiveNavpoint : loc1.alphaUnactiveNavpoint;
            this.color = loc1.mapColors[this.type];
            return;
        }

        public function toString():String
        {
            var loc1:*="[NavpointInfo -- type: %, x: %, y: %, worldX : %, worldY : %]";
            return lesta.utils.Debug.formatString(loc1, [this.type, this.screenPos.x, this.screenPos.y, this.worldPos.x, this.worldPos.y]);
        }

        public var id:int;

        public var fullId:int;

        public var screenPos:flash.geom.Point;

        public var goalScreenPos:flash.geom.Point;

        public var worldPos:flash.geom.Point;

        public var behindCamera:Boolean;

        public var isDraggable:Boolean=false;

        public var isDragging:Boolean=false;

        public var index:int;

        public var type:int;

        public var planeType:int;

        public var indexOnObject:int;

        public var countDuplicated:int;

        public var isDuplicated:int;

        public var xOffset:Number=0;

        public var color:uint;

        public var alpha:Number;

        public var objectType:int=-1;

        public var objectId:int=-1;

        public var objectMarker:lesta.dialogs.battle_window.markers.container.views.base.Marker=null;
    }
}
