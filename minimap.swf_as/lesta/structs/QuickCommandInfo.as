package lesta.structs 
{
    public class QuickCommandInfo extends Object
    {
        public function QuickCommandInfo()
        {
            super();
            return;
        }

        public function get isShipTarget():Boolean
        {
            return this.targetType == SHIP;
        }

        public function get isCapturePointTarget():Boolean
        {
            return this.targetType == CAPTURE_POINT;
        }

        public function get sourceAnimationName():String
        {
            return "Sender_" + this.commandName;
        }

        public function get targetAnimationName():String
        {
            return "Target_CMD";
        }

        public static const EMPTY:int=100;

        public static const SHIP:int=0;

        public static const PLANE:int=1;

        public static const TORPEDO:int=2;

        public static const CAPTURE_POINT:int=11;

        public static const PLAYER:int=12;

        public var cmdId:int;

        public var targetType:int;

        public var targetId:int;

        public var sourcePlayerId:int;

        public var commandName:String;
    }
}
