package lesta.structs 
{
    public class CrewsSingleton extends Object
    {
        public function CrewsSingleton()
        {
            super();
            return;
        }

        public var barracksCapacity:int;

        public var barracksCapacityPrice:int;

        public var crewsInBarracks:int;

        public var buisyCrews:int;

        public var canIncreaseCapacity:Boolean;

        public var canDisassignCrew:Boolean=true;

        public var maxLevel:int;

        public var textCountIncreaseCapacity:String;
    }
}
