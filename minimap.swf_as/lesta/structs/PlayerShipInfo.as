package lesta.structs 
{
    import lesta.utils.*;
    
    public class PlayerShipInfo extends Object
    {
        public function PlayerShipInfo()
        {
            this.suitableVacantCrews = [];
            this.suitableBuisyCrews = [];
            super();
            return;
        }

        public function updateShipInfoReference():void
        {
            this.shipInfo = lesta.utils.GameInfoHolder.instance.getDataByID(lesta.utils.GameInfoHolder.SHIPS, this.id) as lesta.structs.ShipInfo;
            return;
        }

        public function set country(arg1:String):void
        {
            return;
        }

        public function get shipIDS():String
        {
            return this.shipInfo == null ? "" : this.shipInfo.nameIDS;
        }

        public function set shipIDS(arg1:String):void
        {
            return;
        }

        public function get country():String
        {
            return this.shipInfo ? this.shipInfo.country : "";
        }

        public function get level():int
        {
            return this.shipInfo ? this.shipInfo.level : 0;
        }

        public function set level(arg1:int):void
        {
            return;
        }

        public function get stypeIdent():String
        {
            return this.shipInfo ? this.shipInfo.stypeIdent : "";
        }

        public function set stypeIdent(arg1:String):void
        {
            return;
        }

        public function toString():String
        {
            return lesta.utils.Debug.formatString("[PlayerShipInfo id: %, shipId : %, exp : %]", [this.id, this.shipInfo ? this.shipInfo["shortName"] : "", this.exp]);
        }

        public var isInFormation:Boolean=false;

        public var isNeedRepair:Boolean=false;

        public var isNeedShell:Boolean=false;

        public var costRepair:Number=0;

        public var costShell:Number=0;

        public var costTotal:Number=0;

        public var creditsDeficit:Number=0;

        public var expMultiply:Number=1;

        public var suitableVacantCrews:Array;

        public var suitableBuisyCrews:Array;

        public var crewId:int=0;

        public var exteriors:Object;

        public var sigLimit:int;

        public var camLimit:int;

        public var sigInstalled:int=0;

        public var params:Object;

        public var abilInstalled:int=0;

        public var autorechargeFlags:Boolean=false;

        public var autorechargeCamouflage:Boolean=false;

        public var autorechargeAbilities:Boolean=false;

        public var autorechargeExteriorEstimatePrice:Object;

        public var autorechargeExteriorSummPrice:Object;

        public var autorechargeExteriorSummPriceConsumed:Array;

        public var autorechargeExteriorEstimatePriceConsumed:Array;

        public var autorechargeAbilityEstimatePrice:Object;

        public var autorechargeAbilitySummPrice:Object;

        public var autorechargeAbilitySummPriceConsumed:Array;

        public var autorechargeAbilityEstimatePriceConsumed:Array;

        public var type:int=0;

        public var canBuySomeCamouflage:Boolean=false;

        public var canBuySomeAbility:Boolean=false;

        public var index:int;

        public var id:Number;

        public var shipId:Number;

        public var camInstalled:int=0;

        public var exp:Number;

        public var upgradesInfo:lesta.structs.UpgradableShipInfo;

        internal var shipInfo:lesta.structs.ShipInfo;

        public var isReady:Boolean=true;

        public var isLocked:Boolean=false;

        public var canBuySomeSignal:Boolean=false;
    }
}
