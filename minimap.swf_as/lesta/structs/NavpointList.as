package lesta.structs 
{
    import __AS3__.vec.*;
    import lesta.utils.*;
    
    public class NavpointList extends Object
    {
        public function NavpointList()
        {
            this.navpoints = new Vector.<lesta.structs.NavpointInfo>();
            super();
            return;
        }

        public function toString():String
        {
            return "[NavpointList for " + this.id + " :" + String(this.navpoints) + "]";
        }

        public function updateNavpointReferences():void
        {
            var loc2:*=0;
            var loc3:*=null;
            this.navpoints.length = 0;
            var loc1:*=0;
            while (loc1 < this.count) 
            {
                loc2 = this.id * 1000 + loc1;
                loc3 = lesta.utils.GameInfoHolder.instance.mapNavpointsByFullId[loc2];
                if (loc3 == null) 
                {
                    loc3 = lesta.utils.GameInfoHolder.instance.mapWaypointsByFullId[loc2];
                }
                this.navpoints.push(loc3);
                ++loc1;
            }
            return;
        }

        public var id:int;

        public var count:int;

        public var isLinesDraw:Boolean=true;

        public var navpoints:__AS3__.vec.Vector.<lesta.structs.NavpointInfo>;
    }
}
