package lesta.structs 
{
    public class BuildInfo extends Object
    {
        public function BuildInfo()
        {
            this.licenseLogoUrls = [];
            super();
            return;
        }

        public var licenseLogoUrls:Array;
    }
}
