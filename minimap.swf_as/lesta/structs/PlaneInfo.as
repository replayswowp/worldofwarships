package lesta.structs 
{
    import flash.geom.*;
    import lesta.constants.*;
    import lesta.utils.*;
    
    public class PlaneInfo extends lesta.structs.EntityInfo
    {
        public function PlaneInfo(arg1:int=-1, arg2:int=-1, arg3:String="", arg4:int=-1)
        {
            super();
            this.id = arg1;
            this.ownPlaneId = arg2;
            this.relation = arg4;
            var loc1:*;
            entityId = LAST_PLANE_ENTITY_ID++;
            return;
        }

        public function get hpPercentage():Number
        {
            return this._countPlanes > 0 ? this.currentCountPlanes / this._countPlanes : 0;
        }

        public function get countPlanes():int
        {
            return this._countPlanes;
        }

        public function set countPlanes(arg1:int):void
        {
            if (this._countPlanes == -1) 
            {
                this._countPlanes = arg1;
            }
            return;
        }

        public function get playerName():String
        {
            var loc1:*=lesta.utils.GameInfoHolder.instance.mapPlayers[this.ownerId];
            return loc1 == null ? "NoName" : loc1.name;
        }

        public function get typeId():int
        {
            return lesta.constants.PlaneTypes.TYPES.indexOf(this.typeIdent);
        }

        public override function toString():String
        {
            var loc1:*="[PlaneInfo unique_id: @unique_id, planeId: @planeId, ownPlaneId: @ownPlaneId, isAlive: @isAlive, state: @state, ownerId: @ownerId, relation: @relation, order: @order]";
            loc1 = loc1.replace("@unique_id", this.unique_id);
            loc1 = loc1.replace("@planeId", id);
            loc1 = loc1.replace("@ownPlaneId", this.ownPlaneId);
            loc1 = loc1.replace("@isAlive", isAlive);
            loc1 = loc1.replace("@state", this.state);
            loc1 = loc1.replace("@ownerId", this.ownerId);
            loc1 = loc1.replace("@relation", relation);
            loc1 = loc1.replace("@order", this.order);
            return loc1;
        }

        
        {
            LAST_PLANE_ENTITY_ID = 0;
        }

        public var ownPlaneId:int=-1;

        public var _countPlanes:int=-1;

        public var typeIdent:String=null;

        public var numInQueue:int=0;

        public var ownerId:int=-1;

        public var modelName:String;

        public var state:int=1;

        public var currentCountPlanes:int=0;

        public var nameOnIcon:String=null;

        public var order:String="None";

        public var pointMovetoOrder:flash.geom.Point;

        public var hasBomb:Boolean=false;

        public var ammo:Number=0;

        public var maxAmmo:Number=0;

        public var inAttack:Boolean=false;

        public var commandId:int=0;

        public var tkStatus:Boolean=false;

        public var canLand:Boolean=false;

        public var canEscort:Boolean=false;

        public var canLandByHotkey:Boolean=false;

        public var canEscortByHotkey:Boolean=false;

        public var unique_id:String;

        public static var LAST_PLANE_ENTITY_ID:int=0;
    }
}
