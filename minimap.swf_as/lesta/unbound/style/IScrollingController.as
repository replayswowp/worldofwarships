package lesta.unbound.style 
{
    import lesta.unbound.layout.*;
    
    public interface IScrollingController
    {
        function init(arg1:lesta.unbound.layout.UbBlock, arg2:Function, arg3:Function=null, arg4:Function=null):void;

        function enableMask(arg1:Number, arg2:Number):void;

        function disableMask():void;

        function update(arg1:Number, arg2:Number):void;

        function free():void;

        function scrollToFixed(arg1:Number, arg2:Function=null):void;
    }
}
