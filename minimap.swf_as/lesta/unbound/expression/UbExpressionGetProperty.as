package lesta.unbound.expression 
{
    public class UbExpressionGetProperty extends lesta.unbound.expression.UbExpressionBase implements lesta.unbound.expression.IUbExpression
    {
        public function UbExpressionGetProperty(arg1:lesta.unbound.expression.UbASTNodeGetProperty)
        {
            super(arg1);
            this.fromProc = lesta.unbound.expression.UbExpressionCompiler.createExpression(arg1.source);
            this.propProc = lesta.unbound.expression.UbExpressionCompiler.createExpression(arg1.property);
            addRequestedPropertiesOfExpression(this.fromProc);
            addRequestedPropertiesOfExpression(this.propProc);
            return;
        }

        public override function eval(arg1:Object):*
        {
            var loc1:*=this.fromProc.eval(arg1);
            return loc1 ? loc1[this.propProc.eval(arg1)] : null;
        }

        internal var fromProc:lesta.unbound.expression.IUbExpression;

        internal var propProc:lesta.unbound.expression.IUbExpression;
    }
}
