package lesta.unbound.expression 
{
    public class UbASTNodeGetPropertyString extends Object implements lesta.unbound.expression.IUbASTNode
    {
        public function UbASTNodeGetPropertyString(arg1:lesta.unbound.expression.IUbASTNode, arg2:String)
        {
            super();
            this.source = arg1;
            this.property = arg2;
            return;
        }

        public function get astType():int
        {
            return lesta.unbound.expression.UbASTNodeType.GET_PROPERTY_STRING;
        }

        public var source:lesta.unbound.expression.IUbASTNode;

        public var property:String;
    }
}
