package lesta.unbound.expression 
{
    public class UbExpressionCondition extends lesta.unbound.expression.UbExpressionBase implements lesta.unbound.expression.IUbExpression
    {
        public function UbExpressionCondition(arg1:lesta.unbound.expression.UbASTNodeCondition)
        {
            super(arg1);
            this.conditionProc = lesta.unbound.expression.UbExpressionCompiler.createExpression(arg1.condition);
            this.trueChoice = lesta.unbound.expression.UbExpressionCompiler.createExpression(arg1.trueChoice);
            this.falseChoice = lesta.unbound.expression.UbExpressionCompiler.createExpression(arg1.falseChoice);
            addRequestedPropertiesOfExpression(this.conditionProc);
            addRequestedPropertiesOfExpression(this.trueChoice);
            addRequestedPropertiesOfExpression(this.falseChoice);
            return;
        }

        public override function eval(arg1:Object):*
        {
            return this.conditionProc.eval(arg1) ? this.trueChoice.eval(arg1) : this.falseChoice.eval(arg1);
        }

        internal var conditionProc:lesta.unbound.expression.IUbExpression;

        internal var trueChoice:lesta.unbound.expression.IUbExpression;

        internal var falseChoice:lesta.unbound.expression.IUbExpression;
    }
}
