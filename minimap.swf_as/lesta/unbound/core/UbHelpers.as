package lesta.unbound.core 
{
    import flash.display.*;
    import lesta.unbound.layout.*;
    
    public class UbHelpers extends Object
    {
        public function UbHelpers()
        {
            super();
            return;
        }

        public static function getRootBlock(arg1:flash.display.DisplayObject):lesta.unbound.layout.UbRootBlock
        {
            var loc1:*=arg1;
            while (loc1 != null) 
            {
                if (loc1 is lesta.unbound.layout.UbRootBlock) 
                {
                    return loc1 as lesta.unbound.layout.UbRootBlock;
                }
                loc1 = loc1.parent;
            }
            return null;
        }
    }
}
