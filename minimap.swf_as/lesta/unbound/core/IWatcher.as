package lesta.unbound.core 
{
    public interface IWatcher
    {
        function update():void;

        function onChanged(... rest):void;

        function free():void;

        function currentValue():*;
    }
}
