﻿package lesta.unbound.bindings 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import lesta.unbound.core.*;
    
    public class UbPropertyBinding extends lesta.unbound.bindings.UbBinding
    {
        public function UbPropertyBinding(arg1:String)
        {
            super();
            this.bindingName = arg1;
            return;
        }

        public override function init(arg1:__AS3__.vec.Vector, arg2:Array, arg3:flash.display.DisplayObject, arg4:lesta.unbound.core.UbScope, arg5:lesta.unbound.core.UbCentral):void
        {
            super.init(arg1, arg2, arg3, arg4, arg5);
            var loc1:*=(this.bindingName.length - 1);
            var loc2:*;
            if (loc2 = this.bindingName.length > 0 && this.bindingName.indexOf("!") == loc1) 
            {
                this.bindingName = this.bindingName.substr(0, loc1);
                this.watcher = new lesta.unbound.core.UbMultiWatcher(arg1, arg4, this.updateWithMethodCall);
            }
            else 
            {
                this.watcher = new lesta.unbound.core.UbWatcher(arg1[0], arg4, this.update);
            }
            this.watcher.update();
            return;
        }

        public override function free():void
        {
            if (this.watcher) 
            {
                this.watcher.free();
            }
            this.watcher = null;
            super.free();
            return;
        }

        internal function update(arg1:*, arg2:*):void
        {
            if (arg2 != undefined) 
            {
                obj[this.bindingName] = arg2;
            }
            return;
        }

        internal function updateWithMethodCall(arg1:Array, arg2:Array):void
        {
            var loc1:*=obj[this.bindingName] as Function;
            if (loc1 == null) 
            {
                if (this.bindingName in scope) 
                {
                    scope[this.bindingName].apply(scope, arg2);
                }
                else 
                {
                    trace("ERROR: Wrong function name in uss ", this.bindingName);
                }
            }
            else 
            {
                loc1.apply(obj, arg2);
            }
            return;
        }

        internal var watcher:lesta.unbound.core.IWatcher;

        internal var bindingName:String;
    }
}
