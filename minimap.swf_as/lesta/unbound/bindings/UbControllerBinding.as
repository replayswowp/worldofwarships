﻿package lesta.unbound.bindings 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.utils.*;
    import lesta.unbound.core.*;
    
    public class UbControllerBinding extends lesta.unbound.bindings.UbBinding
    {
        public function UbControllerBinding()
        {
            super();
            return;
        }

        public override function init(arg1:__AS3__.vec.Vector, arg2:Array, arg3:flash.display.DisplayObject, arg4:lesta.unbound.core.UbScope, arg5:lesta.unbound.core.UbCentral):void
        {
            arg1 = arg1.concat();
            super.init(arg1, arg2, arg3, arg4, arg5);
            this._controller = new (flash.utils.getDefinitionByName(arg2[0]) as Class)();
            this.injector.inject(this._controller);
            this._controller.initialize(arg5, arg4, arg3);
            arg1.shift();
            this._controller.init(arg1);
            if (arg1.length) 
            {
                this._watcher = new lesta.unbound.core.UbMultiWatcher(arg1, arg4, this._controller.onParamsChanged);
                this._watcher.update();
            }
            return;
        }

        public override function free():void
        {
            if (this._watcher) 
            {
                this._watcher.free();
                this._watcher = null;
            }
            this._controller.free();
            this._controller = null;
            return;
        }

        public var injector:lesta.unbound.core.UbInjector;

        protected var _controller:lesta.unbound.core.UbController;

        protected var _watcher:lesta.unbound.core.UbMultiWatcher;
    }
}
