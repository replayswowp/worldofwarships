﻿package lesta.unbound.bindings 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.events.*;
    import lesta.unbound.core.*;
    
    public class UbBinding extends flash.events.EventDispatcher
    {
        public function UbBinding()
        {
            super();
            return;
        }

        public function init(arg1:__AS3__.vec.Vector, arg2:Array, arg3:flash.display.DisplayObject, arg4:lesta.unbound.core.UbScope, arg5:lesta.unbound.core.UbCentral):void
        {
            this.obj = arg3;
            this.scope = arg4;
            this.central = arg5;
            this.expressions = arg1;
            return;
        }

        public function free():void
        {
            this.obj = null;
            this.scope = null;
            this.central = null;
            this.expressions = null;
            return;
        }

        protected var obj:flash.display.DisplayObject;

        protected var scope:lesta.unbound.core.UbScope;

        protected var central:lesta.unbound.core.UbCentral;

        protected var expressions:__AS3__.vec.Vector;
    }
}
