package lesta.unbound.layout 
{
    import lesta.unbound.core.*;
    import lesta.unbound.expression.*;
    import lesta.unbound.style.*;
    import lesta.utils.*;
    
    public class UbBlockFactory extends Object
    {
        public function UbBlockFactory(arg1:lesta.unbound.core.UbCentral)
        {
            this.styleParser = new lesta.unbound.style.UbStyleParser();
            this.bindingsParser = new lesta.unbound.expression.UbExpressionCompiler();
            super();
            this.central = arg1;
            return;
        }

        public function fini():void
        {
            this.styleParser.fini();
            this.styleParser = null;
            this.bindingsParser.fini();
            this.bindingsParser = null;
            this.central = null;
            return;
        }

        public function loadPlansFromXml(arg1:XML):void
        {
            var loc1:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc2:*=lesta.utils.Debug.isUssLoadingDebugMode();
            var loc6:*=0;
            var loc7:*=arg1.css;
            for each (loc3 in loc7) 
            {
                loc1 = String(loc3.attribute("name"));
                if (!(loc2 || !(loc1 in styleClasses))) 
                {
                    continue;
                }
                styleClasses[loc1] = this.styleParser.parseXml(loc3);
            }
            loc6 = 0;
            loc7 = arg1.block;
            for each (loc4 in loc7) 
            {
                loc1 = String(loc4.attribute("className"));
                if (!(loc2 || !(loc1 in elementPlans))) 
                {
                    continue;
                }
                (loc5 = new lesta.unbound.layout.UbBlockConstructionPlan()).createFromXml(loc4, this);
                elementPlans[loc1] = loc5;
            }
            return;
        }

        public function constructAsRoot(arg1:String, arg2:lesta.unbound.core.UbScope=null):lesta.unbound.layout.UbRootBlock
        {
            var loc1:*=null;
            if (arg1 in elementPlans) 
            {
                loc1 = elementPlans[arg1].constructAsRoot(this);
                this.central.bindBlock(loc1, arg2);
                return loc1;
            }
            throw new Error("missing construction plan for " + arg1 + " elementPlans contets for this plan: " + elementPlans[arg1]);
        }

        public function construct(arg1:String, arg2:lesta.unbound.core.UbScope=null):lesta.unbound.layout.UbBlock
        {
            var loc1:*=null;
            if (arg1 in elementPlans) 
            {
                loc1 = elementPlans[arg1].construct(this);
                this.central.bindBlock(loc1, arg2);
                return loc1;
            }
            throw new Error("missing construction plan for " + arg1 + " elementPlans contets for this plan: " + elementPlans[arg1]);
        }

        public function getStyleClassById(arg1:String):Object
        {
            return styleClasses[arg1];
        }

        public function parseBindingExpression(arg1:String, arg2:String):lesta.unbound.expression.IUbExpression
        {
            return this.bindingsParser.compile(arg1, arg2);
        }

        
        {
            elementPlans = {};
            styleClasses = {};
        }

        internal var styleParser:lesta.unbound.style.UbStyleParser;

        internal var bindingsParser:lesta.unbound.expression.UbExpressionCompiler;

        internal var central:lesta.unbound.core.UbCentral;

        internal static var elementPlans:Object;

        internal static var styleClasses:Object;
    }
}
