﻿package lesta.unbound.layout 
{
    import __AS3__.vec.*;
    
    public class UbBindingData extends Object
    {
        public function UbBindingData(arg1:String, arg2:__AS3__.vec.Vector, arg3:Array)
        {
            super();
            this.name = arg1;
            this.compiledExpressions = arg2;
            this.expressionsList = arg3;
            return;
        }

        public var name:String;

        public var compiledExpressions:__AS3__.vec.Vector;

        public var expressionsList:Array;

        public var elementName:String;
    }
}
