package lesta.unbound.layout 
{
    public class UbTextBlockConstructionPlan extends lesta.unbound.layout.UbBlockConstructionPlan
    {
        public function UbTextBlockConstructionPlan()
        {
            super();
            return;
        }

        public override function createFromXml(arg1:XML, arg2:lesta.unbound.layout.UbBlockFactory):void
        {
            var loc1:*=null;
            super.createFromXml(arg1, arg2);
            var loc2:*=0;
            var loc3:*=arg1.children();
            for each (loc1 in loc3) 
            {
                if (loc1.name() != "text") 
                {
                    continue;
                }
                this.defaultText = loc1.attribute("value");
            }
            return;
        }

        public override function construct(arg1:lesta.unbound.layout.UbBlockFactory):lesta.unbound.layout.UbBlock
        {
            var loc1:*=new lesta.unbound.layout.UbTextBlock();
            principalConstruct(loc1, arg1);
            loc1.text = this.defaultText;
            return loc1;
        }

        internal var defaultText:String="";
    }
}
