package lesta.constants 
{
    public class RDigits extends Object
    {
        public function RDigits()
        {
            super();
            return;
        }

        public static const rdigits:Array=["-", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII", "XIII"];
    }
}
