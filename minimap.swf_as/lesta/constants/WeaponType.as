package lesta.constants 
{
    public class WeaponType extends Object
    {
        public function WeaponType()
        {
            super();
            return;
        }

        public static const ARTILLERY:int=0;

        public static const ATBA:int=1;

        public static const TORPEDO:int=2;

        public static const AIRPLANES:int=3;

        public static const AIRDEFENSE:int=4;

        public static const CONSUME:int=10;
    }
}
