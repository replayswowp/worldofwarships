package lesta.constants 
{
    public class PlaneTypes extends Object
    {
        public function PlaneTypes()
        {
            super();
            return;
        }

        public static const SCOUNT:String="Scout";

        public static const DIVE_BOMBER:String="Dive";

        public static const TORPEDO_BOMBER:String="Bomber";

        public static const FIGHTER:String="Fighter";

        public static const TYPES:Array=["Scout", "Dive", "Bomber", "Fighter"];

        public static const INT_SCOUT:int=0;

        public static const INT_DIVE_BOMBER:int=1;

        public static const INT_TORPEDO_BOMBER:int=2;

        public static const INT_FIGHTER:int=3;
    }
}
