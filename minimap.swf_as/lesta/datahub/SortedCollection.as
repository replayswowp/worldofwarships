package lesta.datahub 
{
    import __AS3__.vec.*;
    
    public class SortedCollection extends lesta.datahub.Collection
    {
        public function SortedCollection()
        {
            this.sorted = new Vector.<Node>();
            super();
            return;
        }

        public override function fini():void
        {
            this.sorted = null;
            super.fini();
            return;
        }

        public override function refill(arg1:Array):void
        {
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                this.sorted[loc1] = arg1[loc1];
                ++loc1;
            }
            update();
            return;
        }

        public function createNode(arg1:lesta.datahub.Entity):Node
        {
            var loc1:*=new Node(arg1);
            this.sorted.push(loc1);
            return loc1;
        }

        public function removeNodeAt(arg1:int):void
        {
            this.sorted.splice(arg1, 1);
            var loc1:*=arg1;
            while (loc1 < this.sorted.length) 
            {
                var loc2:*;
                var loc3:*=((loc2 = this.sorted[loc1]).index - 1);
                loc2.index = loc3;
                ++loc1;
            }
            update();
            return;
        }

        public var sorted:__AS3__.vec.Vector.<Node>;
    }
}


class Node extends Object
{
    public function Node(arg1:lesta.datahub.Entity, arg2:int=-1)
    {
        super();
        this.entity = arg1;
        this.index = arg2;
        return;
    }

    public var entity:lesta.datahub.Entity;

    public var index:int;
}