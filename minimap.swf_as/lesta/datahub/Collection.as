package lesta.datahub 
{
    import flash.events.*;
    import flash.utils.*;
    
    public class Collection extends flash.events.EventDispatcher
    {
        public function Collection()
        {
            this.items = new Array();
            this.children = new flash.utils.Dictionary(true);
            super();
            return;
        }

        public function fini():void
        {
            this.children = null;
            this.items = null;
            return;
        }

        public function refill(arg1:Array):void
        {
            this.items.length = 0;
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                this.items.push(arg1[loc1]);
                ++loc1;
            }
            this.update();
            return;
        }

        public function add(arg1:lesta.datahub.Entity):void
        {
            this.items.push(arg1);
            this.update();
            return;
        }

        public function insertFront(arg1:lesta.datahub.Entity):void
        {
            this.items.unshift(arg1);
            this.update();
            return;
        }

        public function remove(arg1:lesta.datahub.Entity):void
        {
            var loc1:*=this.items.indexOf(arg1);
            if (loc1 >= 0) 
            {
                this.items.splice(loc1, 1);
                this.update();
            }
            return;
        }

        public function child(arg1:*):lesta.datahub.Collection
        {
            return this.children[arg1];
        }

        public function createChild(arg1:*):lesta.datahub.Collection
        {
            var loc1:*=new lesta.datahub.Collection();
            this.children[arg1] = loc1;
            return loc1;
        }

        public function createSorting(arg1:*):lesta.datahub.SortedCollection
        {
            var loc1:*=new lesta.datahub.SortedCollection();
            this.children[arg1] = loc1;
            return loc1;
        }

        public function removeChild(arg1:*):void
        {
            delete this.children[arg1];
            return;
        }

        protected function update():void
        {
            dispatchEvent(new flash.events.Event(flash.events.Event.CHANGE));
            return;
        }

        public var items:Array;

        internal var children:flash.utils.Dictionary;
    }
}
