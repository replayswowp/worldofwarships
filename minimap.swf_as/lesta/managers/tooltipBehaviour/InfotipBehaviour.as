package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import lesta.managers.*;
    
    public class InfotipBehaviour extends lesta.managers.tooltipBehaviour.SimpleTooltipBehaviour
    {
        public function InfotipBehaviour()
        {
            super();
            appearDelay = 0;
            appearDuration = 0.3;
            disappearDelay = 0;
            disappearDuration = 0.3;
            tinyOffset = 5;
            rightOffset = 0;
            bottomOffset = 0;
            return;
        }

        public override function getName():String
        {
            return "infotip";
        }

        protected override function appearTooltipAnimation(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):void
        {
            visualize(arg1, arg2, arg3);
            var loc1:*=appearDuration;
            var loc2:*=appearDelay;
            var loc3:*={"alpha":arg2.tipInstance.alpha, "x":arg2.tipInstance.x + 10};
            var loc4:*={"alpha":1, "x":arg2.tipInstance.x};
            arg2.tipInstance.alpha = 0;
            lesta.managers.TweenManager.addTweenExplicitly(arg2.tipInstance, loc1, loc3, loc4, null, 0, loc2);
            return;
        }

        protected override function disappearTooltipAnimation(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            var tooltip:lesta.managers.tooltipBehaviour.TooltipInfo;
            var finishAnimationCallback:Function;
            var duration:Number;
            var delay:Number;
            var startProps:Object;
            var finishProps:Object;

            var loc1:*;
            finishAnimationCallback = null;
            tooltip = arg1;
            finishAnimationCallback = function ():void
            {
                tooltip.tipInstance.visible = false;
                return;
            }
            duration = disappearDuration;
            delay = disappearDelay;
            startProps = {"alpha":tooltip.tipInstance.alpha, "x":tooltip.tipInstance.x};
            finishProps = {"alpha":0, "x":tooltip.tipInstance.x + 10};
            lesta.managers.TweenManager.addTweenExplicitly(tooltip.tipInstance, duration, startProps, finishProps, finishAnimationCallback, 0, delay);
            return;
        }

        protected override function placeTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):void
        {
            var loc1:*=NaN;
            var loc2:*=NaN;
            var loc3:*=NaN;
            var loc4:*=null;
            var loc5:*=false;
            var loc6:*=NaN;
            if (isValidTooltip(arg2)) 
            {
                loc1 = arg2.getStyleProperty("tinyOffset", tinyOffset);
                loc2 = arg2.getStyleProperty("rightOffset", rightOffset);
                loc3 = arg2.getStyleProperty("bottomOffset", bottomOffset);
                loc5 = (loc4 = getTargetRect(arg2)).right + arg2.tipInstance.width + loc1 > arg1.stage.stageWidth;
                arg2.tipInstance.y = loc4.top + (loc4.height - arg2.tipInstance.height) / 2;
                arg2.tipInstance.y = Math.max(loc1, Math.min(arg2.tipInstance.y + loc3, arg1.stage.stageHeight - loc1 - arg2.tipInstance.height));
                loc6 = arg2.getStyleProperty("screenTopOffset", screenTopOffset);
                if (arg2.tipInstance.y < loc6) 
                {
                    arg2.tipInstance.y = loc6;
                }
                arg2.tipInstance.x = loc5 ? loc4.left - arg2.tipInstance.width - loc2 : loc4.right + loc2;
                placeTooltipPin(arg1, arg2, arg3, DIRECTION_HORIZONTAL, loc5);
            }
            return;
        }

        public override function onReasonMouseMove(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onReasonRollOut(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public override function onReasonClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            showTooltip(arg1, arg2, arg3);
            return true;
        }

        public override function onOutsideMouseClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            hideTooltip(arg2);
            return true;
        }

        public override function onTooltipResize(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            return arg2.visible;
        }
    }
}
