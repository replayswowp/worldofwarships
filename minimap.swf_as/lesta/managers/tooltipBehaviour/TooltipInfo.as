package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import lesta.managers.*;
    import lesta.utils.*;
    
    public class TooltipInfo extends Object
    {
        public function TooltipInfo(arg1:int, arg2:Function, arg3:Function, arg4:Function, arg5:Function, arg6:Function, arg7:Function, arg8:Function, arg9:int, arg10:flash.display.DisplayObjectContainer, arg11:Function, arg12:lesta.managers.tooltipBehaviour.TooltipInfo=null)
        {
            this._pins = [];
            super();
            this.id = arg1;
            this.retrieveTooltip = arg2;
            this.closeCallback = arg3;
            this.beforeShowCallback = arg4;
            this.showCallback = arg5;
            this.stateChangedCallback = arg6;
            this.validateTooltipSize = arg7;
            this.behaviour = arg9;
            this.tooltipReason = arg10;
            this._parentTooltip = arg12;
            this.addTooltipToStage = arg8;
            this.isTooltipPinned = arg11;
            return;
        }

        protected function set activePin(arg1:flash.display.DisplayObject):void
        {
            if (this._activePin != null) 
            {
                this._activePin.visible = false;
            }
            this._activePin = arg1;
            if (this._activePin != null) 
            {
                this._activePin.visible = true;
            }
            return;
        }

        protected function findTooltipStyle():Object
        {
            var loc1:*="computedStyle";
            var loc2:*="userData";
            if (this.tipInstance[loc1] && this.tipInstance[loc1][loc2]) 
            {
                return this.tipInstance[loc1][loc2];
            }
            var loc3:*=this.tipInstance;
            while (loc3 && loc3.numChildren) 
            {
                loc3 = loc3.getChildAt(0) as flash.display.DisplayObjectContainer;
                if (!(loc3 && loc3[loc1] && loc3[loc1][loc2])) 
                {
                    continue;
                }
                return loc3[loc1][loc2];
            }
            return null;
        }

        internal function getAllPins():void
        {
            this._availPinTypes = PIN_NONE;
            var loc1:*=0;
            while (loc1 < PIN_TYPES.length) 
            {
                this._pins[loc1] = lesta.utils.DisplayObjectUtils.getDescendantByName(this.tipInstance, PIN_NAMES[loc1]);
                if (this._pins[loc1] != null) 
                {
                    this._pins[loc1].visible = false;
                    this._availPinTypes = this._availPinTypes + PIN_TYPES[loc1];
                }
                ++loc1;
            }
            return;
        }

        public function hideAllPins():void
        {
            var loc3:*=null;
            if (this._availPinTypes == -1) 
            {
                this.getAllPins();
            }
            var loc1:*=0;
            var loc2:*=this._pins.length;
            while (loc1 < loc2) 
            {
                loc3 = this._pins[loc1];
                if (loc3) 
                {
                    loc3.visible = false;
                }
                ++loc1;
            }
            return;
        }

        public function getPin(arg1:int):flash.display.DisplayObject
        {
            if (this._availPinTypes == -1) 
            {
                this.getAllPins();
            }
            var loc1:*=this._pins[PIN_TYPES.indexOf(arg1)];
            this.activePin = loc1;
            return loc1;
        }

        public function getStyleProperty(arg1:String, arg2:*):*
        {
            var loc1:*=this.getStyle();
            if (loc1 && !(loc1[arg1] == null)) 
            {
                return loc1[arg1];
            }
            return arg2;
        }

        public function getStyle():Object
        {
            this._style = this._style || this.findTooltipStyle();
            return this._style || {};
        }

        public function fini():void
        {
            this._parentTooltip = null;
            this.eventHandlerReason = null;
            this.eventHandlerTooltip = null;
            this.tooltipReason = null;
            this.retrieveTooltip = null;
            this.closeCallback = null;
            this.showCallback = null;
            this.isTooltipPinned = null;
            this.stateChangedCallback = null;
            this.validateTooltipSize = null;
            this._pins = null;
            this._style = null;
            this._activePin = null;
            lesta.managers.TweenManager.clearObject(this.tipInstance);
            this.tipInstance = null;
            return;
        }

        public function toString():String
        {
            return "TooltipInfo{id=" + String(this.id) + ",behaviour=" + String(this.behaviour) + ",visible=" + String(this.visible) + ",_availPinTypes=" + String(this._availPinTypes) + "}";
        }

        public function get parentTooltip():lesta.managers.tooltipBehaviour.TooltipInfo
        {
            return this._parentTooltip;
        }

        public function set parentTooltip(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            this._parentTooltip = arg1;
            return;
        }

        public static const PIN_TYPES:Array=[PIN_LEFT, PIN_TOP, PIN_RIGHT, PIN_BOTTOM];

        public static const PIN_NAMES:Array=["pinLeft", "pinTop", "pinRight", "pinBottom"];

        public static const PIN_LEFT:int=4096;

        public static const PIN_TOP:int=256;

        public static const PIN_RIGHT:int=16;

        public static const PIN_BOTTOM:int=1;

        public static const PIN_NONE:int=0;

        public var id:int=0;

        public var retrieveTooltip:Function;

        public var closeCallback:Function;

        public var beforeShowCallback:Function;

        public var showCallback:Function;

        public var stateChangedCallback:Function;

        public var validateTooltipSize:Function;

        public var addTooltipToStage:Function;

        public var measured:Boolean=false;

        public var tipInstance:flash.display.DisplayObjectContainer;

        public var eventHandlerTooltip:Function;

        public var eventHandlerReason:Function;

        public var tooltipReason:flash.display.DisplayObjectContainer;

        public var behaviour:int;

        protected var _parentTooltip:lesta.managers.tooltipBehaviour.TooltipInfo;

        protected var _style:Object;

        protected var _pins:Array;

        protected var _availPinTypes:int=-1;

        public var isTooltipPinned:Function;

        public var visible:Boolean=false;

        protected var _activePin:flash.display.DisplayObject;
    }
}
