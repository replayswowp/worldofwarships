package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    import scaleform.clik.controls.*;
    
    public class VerticalInfoWidgetBehaviour extends lesta.managers.tooltipBehaviour.VerticalInfotipBehaviour implements lesta.managers.tooltipBehaviour.ITooltipBehaviour
    {
        public function VerticalInfoWidgetBehaviour()
        {
            super();
            return;
        }

        public override function getName():String
        {
            return "verticalInfoWidget";
        }

        public override function onOutsideMouseClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            this.hideTooltip(arg2);
            return false;
        }

        public override function onTooltipClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            this.hideTooltip(arg2);
            return false;
        }

        public override function onReasonClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            if (arg2.visible) 
            {
                this.hideTooltip(arg2);
            }
            else 
            {
                this.showTooltip(arg1, arg2, arg3);
            }
            return arg2.visible;
        }

        public override function showTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent, arg4:Boolean=false):void
        {
            if (arg2.tooltipReason is scaleform.clik.controls.Button) 
            {
                (arg2.tooltipReason as scaleform.clik.controls.Button).selected = true;
            }
            super.showTooltip(arg1, arg2, arg3, arg4);
            return;
        }

        public override function hideTooltip(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            if (arg1.tooltipReason is scaleform.clik.controls.Button) 
            {
                (arg1.tooltipReason as scaleform.clik.controls.Button).selected = false;
            }
            super.hideTooltip(arg1);
            return;
        }
    }
}
