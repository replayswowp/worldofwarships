package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    
    public class HorizontalInteractiveTooltipBehaviour extends lesta.managers.tooltipBehaviour.InteractiveTooltipBehaviour
    {
        public function HorizontalInteractiveTooltipBehaviour()
        {
            super();
            return;
        }

        public override function getName():String
        {
            return "horizontalInteractive";
        }

        protected override function placeTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):void
        {
            var loc1:*=null;
            var loc2:*=false;
            var loc3:*=NaN;
            var loc4:*=NaN;
            var loc5:*=NaN;
            if (isValidTooltip(arg2)) 
            {
                loc2 = (loc1 = getTargetRect(arg2)).right + arg2.tipInstance.width + tinyOffset > arg1.stage.stageWidth;
                loc3 = arg2.getStyleProperty("tinyOffset", tinyOffset);
                loc4 = arg2.getStyleProperty("rightOffset", rightOffset);
                loc5 = arg2.getStyleProperty("bottomOffset", bottomOffset);
                arg2.tipInstance.y = loc5 + loc1.top + (loc1.height - arg2.tipInstance.height) / 2;
                arg2.tipInstance.y = Math.max(loc3, Math.min(arg2.tipInstance.y, arg1.stage.stageHeight - loc3 - arg2.tipInstance.height));
                arg2.tipInstance.x = loc2 ? loc1.left - arg2.tipInstance.width - loc4 : loc1.right + loc4;
                placeTooltipPin(arg1, arg2, arg3, DIRECTION_HORIZONTAL, loc2);
            }
            return;
        }
    }
}
