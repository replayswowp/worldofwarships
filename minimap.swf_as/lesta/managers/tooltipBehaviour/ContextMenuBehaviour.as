package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    import scaleform.gfx.*;
    
    public class ContextMenuBehaviour extends lesta.managers.tooltipBehaviour.SimpleTooltipBehaviour
    {
        public function ContextMenuBehaviour()
        {
            super();
            appearDelay = 0;
            tinyOffset = 0;
            rightOffset = 0;
            bottomOffset = 0;
            return;
        }

        public override function getName():String
        {
            return "contextMenu";
        }

        public override function onOutsideMouseDown(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            hideTooltip(arg2);
            return false;
        }

        public override function onReasonMouseMove(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return arg2.visible;
        }

        public override function onReasonRollOut(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return arg2.visible;
        }

        public override function onTooltipClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            hideTooltip(arg2);
            return false;
        }

        public override function onTooltipResize(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            return arg2.visible;
        }

        public override function onReasonClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            var loc1:*;
            if ((loc1 = arg3 as scaleform.gfx.MouseEventEx).buttonIdx == scaleform.gfx.MouseEventEx.RIGHT_BUTTON) 
            {
                if (!arg2.visible) 
                {
                    showTooltip(arg1, arg2, arg3, true);
                }
                placeTooltip(arg1, arg2, arg3);
                arg3.stopPropagation();
                return true;
            }
            hideTooltip(arg2);
            return false;
        }
    }
}
