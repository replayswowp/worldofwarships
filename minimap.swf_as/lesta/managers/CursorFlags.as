package lesta.managers 
{
    public class CursorFlags extends Object
    {
        public function CursorFlags()
        {
            super();
            return;
        }

        public static const HANGAR_DRAGGING:String="HANGAR_DRAGGING";

        public static const HANGAR_DRAGGING_VERTICAL:String="HANGAR_DRAGGING_VERTICAL";

        public static const GUI_CURSOR_HIGHLIGHT:String="GUI_CURSOR_HIGHLIGHT";

        public static const CURSOR_OVER_MINIMAP:String="CURSOR_OVER_MINIMAP";

        public static const CURSOR_OVER_NAVPOINT:String="CURSOR_OVER_NAVPOINT";
    }
}
