﻿package lesta.managers 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.managers.events.*;
    import lesta.managers.tooltipBehaviour.*;
    
    public class TooltipManager extends flash.events.EventDispatcher
    {
        public function TooltipManager(arg1:flash.display.DisplayObjectContainer, arg2:Array=null)
        {
            this.tooltipBehaviourMethodNames = {};
            this.allTooltips = new Vector.<lesta.managers.tooltipBehaviour.TooltipInfo>();
            this.mapTooltips = new flash.utils.Dictionary();
            this.visibleTooltips = new Vector.<lesta.managers.tooltipBehaviour.TooltipInfo>();
            this.outsiders = new Vector();
            this.notOutsiders = new Vector();
            this.notOutsideForNextClick = new Vector.<lesta.managers.tooltipBehaviour.TooltipInfo>();
            this.mapBehavioursIdsByName = {};
            super();
            this.initBehaviours(arg2);
            this.tooltipBehaviourMethodNames["reason" + flash.events.MouseEvent.MOUSE_OVER] = "onReasonMouseOver";
            this.tooltipBehaviourMethodNames["reason" + flash.events.MouseEvent.MOUSE_OUT] = "onReasonMouseOut";
            this.tooltipBehaviourMethodNames["reason" + flash.events.MouseEvent.ROLL_OVER] = "onReasonRollOver";
            this.tooltipBehaviourMethodNames["reason" + flash.events.MouseEvent.ROLL_OUT] = "onReasonRollOut";
            this.tooltipBehaviourMethodNames["reason" + flash.events.MouseEvent.MOUSE_MOVE] = "onReasonMouseMove";
            this.tooltipBehaviourMethodNames["reason" + flash.events.MouseEvent.MOUSE_DOWN] = "onReasonMouseDown";
            this.tooltipBehaviourMethodNames["reason" + flash.events.MouseEvent.CLICK] = "onReasonClick";
            this.tooltipBehaviourMethodNames["tooltip" + flash.events.MouseEvent.ROLL_OVER] = "onTooltipRollOver";
            this.tooltipBehaviourMethodNames["tooltip" + flash.events.MouseEvent.ROLL_OUT] = "onTooltipRollOut";
            this.tooltipBehaviourMethodNames["tooltip" + flash.events.MouseEvent.MOUSE_MOVE] = "onTooltipMouseMove";
            this.tooltipBehaviourMethodNames["tooltip" + flash.events.MouseEvent.MOUSE_DOWN] = "onTooltipMouseDown";
            this.tooltipBehaviourMethodNames["tooltip" + flash.events.MouseEvent.CLICK] = "onTooltipClick";
            this.tooltipBehaviourMethodNames["tooltip" + flash.events.Event.CLOSE] = "onTooltipClose";
            this.tooltipBehaviourMethodNames["tooltip" + lesta.managers.events.DragEvent.START_DRAGGING] = "onTooltipStartDragging";
            this.tooltipBehaviourMethodNames["tooltip" + lesta.managers.events.DragEvent.STOP_DRAGGING] = "onTooltipStopDragging";
            this.tooltipBehaviourMethodNames["tooltip" + flash.events.Event.RESIZE] = "onTooltipResize";
            this.scene = arg1;
            if (arg1.stage) 
            {
                arg1.stage.addEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.handleStageMouseDown, false, 0, true);
                arg1.stage.addEventListener(flash.events.MouseEvent.CLICK, this.handleStageMouseDown, false, 0, true);
            }
            else 
            {
                arg1.addEventListener(flash.events.Event.ADDED_TO_STAGE, this.onSceneAdded);
            }
            return;
        }

        protected function eventHandlerTemplate(arg1:flash.events.Event, arg2:String, arg3:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            var loc5:*=0;
            var loc1:*=this.getConcurrentTooltips(arg3);
            var loc2:*;
            if (!(loc2 = this.getPermissionToHandleTooltipEvent(arg3, loc1))) 
            {
                return;
            }
            this.fetchTooltipDisplayObject(arg3);
            var loc3:*=arg3.visible;
            var loc4:*;
            var loc6:*;
            (loc6 = loc4 = this._tooltipBehaviours[arg3.behaviour])[arg2](this.scene, arg3, arg1);
            if (loc3 != arg3.visible) 
            {
                if (arg3.visible) 
                {
                    this.visibleTooltips.push(arg3);
                    loc5 = 0;
                    while (loc5 < loc1.length) 
                    {
                        this.hideTooltip(loc1[loc5]);
                        ++loc5;
                    }
                }
                else 
                {
                    this.hideTooltip(arg3);
                }
            }
            return;
        }

        internal function handleStageMouseDown(arg1:flash.events.MouseEvent):void
        {
            var loc1:*=null;
            var loc3:*=null;
            var loc4:*=false;
            var loc5:*=null;
            var loc6:*=0;
            var loc7:*=false;
            var loc8:*=false;
            var loc9:*=null;
            var loc2:*=arg1.target as flash.display.DisplayObject;
            if (loc2.stage != null) 
            {
                this.outsiders.length = 0;
                this.notOutsiders.length = 0;
                loc3 = null;
                loc5 = (loc4 = arg1.type == flash.events.MouseEvent.CLICK) ? "onOutsideMouseClick" : "onOutsideMouseDown";
                loc6 = 0;
                while (loc6 < this.visibleTooltips.length) 
                {
                    loc1 = this.visibleTooltips[loc6];
                    loc7 = loc1.tipInstance.contains(loc2);
                    loc8 = loc1.tooltipReason.contains(loc2);
                    if (loc7 || loc8) 
                    {
                        if (loc7) 
                        {
                            loc3 = loc1;
                        }
                    }
                    else 
                    {
                        this.outsiders.push(arg1, loc5, loc1);
                    }
                    ++loc6;
                }
                loc5 = loc4 ? "onTooltipClick" : "onTooltipMouseDown";
                if (loc3 != null) 
                {
                    loc9 = loc3.parentTooltip;
                    while (loc9 != null) 
                    {
                        this.removeOutsider(arg1, loc5, loc9);
                        if (!loc4) 
                        {
                            this.notOutsideForNextClick.push(loc9);
                        }
                        loc9 = loc9.parentTooltip;
                    }
                }
                if (loc4) 
                {
                    loc6 = 0;
                    while (loc6 < this.notOutsideForNextClick.length) 
                    {
                        this.removeOutsider(arg1, loc5, this.notOutsideForNextClick[loc6]);
                        ++loc6;
                    }
                    this.notOutsideForNextClick.length = 0;
                }
                while (this.outsiders.length > 0) 
                {
                    this.eventHandlerTemplate(this.outsiders.shift(), this.outsiders.shift(), this.outsiders.shift());
                }
            }
            return;
        }

        internal function removeOutsider(arg1:flash.events.MouseEvent, arg2:String, arg3:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            var loc1:*;
            if ((loc1 = this.outsiders.indexOf(arg3)) > -1) 
            {
                this.outsiders.splice(loc1 - 2, 3);
            }
            this.notOutsiders.push(arg1, arg2, arg3);
            return;
        }

        internal function handleStageDeactivate(arg1:flash.events.Event):void
        {
            var loc1:*=0;
            while (loc1 < this.visibleTooltips.length) 
            {
                this.eventHandlerTemplate(arg1, "onApplicationLooseFocus", this.visibleTooltips[loc1]);
                ++loc1;
            }
            return;
        }

        internal function fetchTooltipDisplayObject(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):flash.display.DisplayObject
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=null;
            if (arg1.tipInstance == null) 
            {
                this.createTooltip(arg1);
                loc2 = 0;
                while (loc2 < this.visibleTooltips.length) 
                {
                    if ((loc3 = this.visibleTooltips[loc2]).tipInstance.contains(arg1.tooltipReason)) 
                    {
                        loc1 = loc3;
                        break;
                    }
                    ++loc2;
                }
                arg1.parentTooltip = loc1;
            }
            return arg1.tipInstance;
        }

        internal function createTooltip(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            var tipData:lesta.managers.tooltipBehaviour.TooltipInfo;
            var tooltipEventHandler:Function;
            var tip:flash.display.DisplayObjectContainer;
            var behaviour:lesta.managers.tooltipBehaviour.ITooltipBehaviour;

            var loc1:*;
            tooltipEventHandler = null;
            tip = null;
            behaviour = null;
            tipData = arg1;
            tooltipEventHandler = function (arg1:flash.events.Event):void
            {
                eventHandlerTemplate(arg1, tooltipBehaviourMethodNames["tooltip" + arg1.type], tipData);
                return;
            }
            if (tipData.tipInstance == null) 
            {
                tip = tipData.retrieveTooltip();
                tipData.tipInstance = tip;
                tip.addEventListener(flash.events.MouseEvent.ROLL_OVER, tooltipEventHandler, false, 0, true);
                tip.addEventListener(flash.events.MouseEvent.ROLL_OUT, tooltipEventHandler, false, 0, true);
                tip.addEventListener(flash.events.MouseEvent.MOUSE_MOVE, tooltipEventHandler, false, 0, true);
                tip.addEventListener(flash.events.MouseEvent.MOUSE_DOWN, tooltipEventHandler, false, 0, true);
                tip.addEventListener(flash.events.MouseEvent.CLICK, tooltipEventHandler, false, 0, true);
                tip.addEventListener(flash.events.Event.CLOSE, tooltipEventHandler, false, 0, true);
                tip.addEventListener(lesta.managers.events.DragEvent.START_DRAGGING, tooltipEventHandler, false, 0, true);
                tip.addEventListener(lesta.managers.events.DragEvent.STOP_DRAGGING, tooltipEventHandler, false, 0, true);
                tip.addEventListener(flash.events.Event.RESIZE, tooltipEventHandler, false, 0, true);
                tipData.eventHandlerTooltip = tooltipEventHandler;
                behaviour = this._tooltipBehaviours[tipData.behaviour];
                behaviour.onRegisterTooltip(this.scene, tipData);
            }
            return;
        }

        internal function execHideTooltipCallback(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            if (arg1.closeCallback != null) 
            {
                arg1.closeCallback();
            }
            return;
        }

        public function hideAllTooltips():void
        {
            while (this.visibleTooltips.length > 0) 
            {
                this.hideTooltip(this.visibleTooltips[0]);
            }
            return;
        }

        internal function hideTooltip(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            this._tooltipBehaviours[arg1.behaviour].hideTooltip(arg1);
            var loc1:*=this.visibleTooltips.indexOf(arg1);
            this.visibleTooltips.splice(loc1, 1);
            this.execHideTooltipCallback(arg1);
            return;
        }

        public function get tooltipBehaviours():__AS3__.vec.Vector.<lesta.managers.tooltipBehaviour.ITooltipBehaviour>
        {
            return this._tooltipBehaviours;
        }

        internal function onSceneAdded(arg1:flash.events.Event=null):void
        {
            this.scene.removeEventListener(flash.events.Event.ADDED_TO_STAGE, this.onSceneAdded);
            this.scene.stage.addEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.handleStageMouseDown, false, 0, true);
            this.scene.stage.addEventListener(flash.events.MouseEvent.CLICK, this.handleStageMouseDown, false, 0, true);
            return;
        }

        public function initBehaviours(arg1:Array):void
        {
            var loc3:*=null;
            if (this._tooltipBehaviours == null) 
            {
                this._tooltipBehaviours = new Vector.<lesta.managers.tooltipBehaviour.ITooltipBehaviour>();
                this._tooltipBehaviours.push(new lesta.managers.tooltipBehaviour.SimpleTooltipBehaviour());
                this._tooltipBehaviours.push(new lesta.managers.tooltipBehaviour.InteractiveTooltipBehaviour());
            }
            while (arg1 && arg1.length > 0) 
            {
                this._tooltipBehaviours.push(arg1.shift());
            }
            this._tooltipBehaviours.push(new lesta.managers.tooltipBehaviour.ContextMenuBehaviour());
            this._tooltipBehaviours.push(new lesta.managers.tooltipBehaviour.WindowTooltipBehaviour());
            var loc1:*=0;
            var loc2:*=this._tooltipBehaviours.length;
            while (loc1 < loc2) 
            {
                loc3 = this._tooltipBehaviours[loc1];
                this.mapBehavioursIdsByName[loc3.getName()] = loc1;
                ++loc1;
            }
            return;
        }

        public function free():void
        {
            this.scene.stage.removeEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.handleStageMouseDown);
            this.scene.stage.removeEventListener(flash.events.MouseEvent.CLICK, this.handleStageMouseDown);
            while (this.allTooltips.length) 
            {
                this.doUnregisterTooltip(this.allTooltips[0]);
            }
            this._tooltipBehaviours = null;
            return;
        }

        public function registerObject(arg1:flash.display.DisplayObjectContainer, arg2:Function, arg3:Function, arg4:Function, arg5:Function, arg6:Function, arg7:Function, arg8:Function, arg9:int, arg10:Function):int
        {
            var tooltipReason:flash.display.DisplayObjectContainer;
            var retrieveTooltip:Function;
            var closeCallback:Function;
            var beforeShowCallback:Function;
            var showCallback:Function;
            var stateChangedCallback:Function;
            var validateTooltipSize:Function;
            var addTooltipToStage:Function;
            var behaviour:int;
            var isTooltipPinned:Function;
            var ti:lesta.managers.tooltipBehaviour.TooltipInfo;
            var onReasonMouseHandler:Function;

            var loc1:*;
            ti = null;
            onReasonMouseHandler = null;
            tooltipReason = arg1;
            retrieveTooltip = arg2;
            closeCallback = arg3;
            beforeShowCallback = arg4;
            showCallback = arg5;
            stateChangedCallback = arg6;
            validateTooltipSize = arg7;
            addTooltipToStage = arg8;
            behaviour = arg9;
            isTooltipPinned = arg10;
            onReasonMouseHandler = function (arg1:flash.events.MouseEvent):void
            {
                eventHandlerTemplate(arg1, tooltipBehaviourMethodNames["reason" + arg1.type], ti);
                return;
            }
            var loc2:*;
            var loc3:*;
            ti = new lesta.managers.tooltipBehaviour.TooltipInfo(loc2.lastTooltipId = loc3 = ((loc2 = this).lastTooltipId + 1), retrieveTooltip, closeCallback, beforeShowCallback, showCallback, stateChangedCallback, validateTooltipSize, addTooltipToStage, behaviour, tooltipReason, isTooltipPinned);
            tooltipReason.addEventListener(flash.events.MouseEvent.MOUSE_OVER, onReasonMouseHandler, false, 0, true);
            tooltipReason.addEventListener(flash.events.MouseEvent.MOUSE_OUT, onReasonMouseHandler, false, 0, true);
            tooltipReason.addEventListener(flash.events.MouseEvent.ROLL_OVER, onReasonMouseHandler, false, 0, true);
            tooltipReason.addEventListener(flash.events.MouseEvent.ROLL_OUT, onReasonMouseHandler, false, 0, true);
            tooltipReason.addEventListener(flash.events.MouseEvent.MOUSE_MOVE, onReasonMouseHandler, false, 0, true);
            tooltipReason.addEventListener(flash.events.MouseEvent.MOUSE_DOWN, onReasonMouseHandler, false, 0, true);
            tooltipReason.addEventListener(flash.events.MouseEvent.CLICK, onReasonMouseHandler, false, 0, true);
            ti.eventHandlerReason = onReasonMouseHandler;
            this.allTooltips.push(ti);
            this.mapTooltips[ti.id] = ti;
            return ti.id;
        }

        public function unregisterTooltip(arg1:int):void
        {
            this.doUnregisterTooltip(this.mapTooltips[arg1]);
            return;
        }

        public function unregisterObject(arg1:flash.display.DisplayObject):void
        {
            var loc1:*=null;
            var loc2:*=0;
            while (loc2 < this.allTooltips.length) 
            {
                loc1 = this.allTooltips[loc2];
                if (loc1.tooltipReason == arg1) 
                {
                    this.doUnregisterTooltip(loc1);
                    continue;
                }
                ++loc2;
            }
            return;
        }

        public function updateStageSize():void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=0;
            while (loc1 < this.visibleTooltips.length) 
            {
                loc2 = this.visibleTooltips[loc1];
                loc3 = this._tooltipBehaviours[loc2.behaviour];
                loc3.onApplicationSizeChanged(this.scene, loc2);
                ++loc1;
            }
            return;
        }

        public function showTooltip(arg1:int):void
        {
            var loc1:*=this.mapTooltips[arg1];
            if (loc1 == null) 
            {
                trace("[WARNING] TooltipManager.showTooltip: invalid tooltip id - ", arg1);
                return;
            }
            this.fetchTooltipDisplayObject(loc1);
            var loc2:*=this._tooltipBehaviours[loc1.behaviour];
            loc2.showTooltip(this.scene, loc1, null, true);
            var loc3:*=this.visibleTooltips.indexOf(loc1);
            if (loc1.visible && loc3 == -1) 
            {
                this.visibleTooltips.push(loc1);
            }
            return;
        }

        internal function doUnregisterTooltip(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            arg1.tooltipReason.removeEventListener(flash.events.MouseEvent.MOUSE_OVER, arg1.eventHandlerReason);
            arg1.tooltipReason.removeEventListener(flash.events.MouseEvent.MOUSE_OUT, arg1.eventHandlerReason);
            arg1.tooltipReason.removeEventListener(flash.events.MouseEvent.ROLL_OVER, arg1.eventHandlerReason);
            arg1.tooltipReason.removeEventListener(flash.events.MouseEvent.ROLL_OUT, arg1.eventHandlerReason);
            arg1.tooltipReason.removeEventListener(flash.events.MouseEvent.MOUSE_MOVE, arg1.eventHandlerReason);
            arg1.tooltipReason.removeEventListener(flash.events.MouseEvent.MOUSE_DOWN, arg1.eventHandlerReason);
            arg1.tooltipReason.removeEventListener(flash.events.MouseEvent.CLICK, arg1.eventHandlerReason);
            if (arg1.tipInstance != null) 
            {
                arg1.tipInstance.removeEventListener(flash.events.MouseEvent.ROLL_OVER, arg1.eventHandlerTooltip);
                arg1.tipInstance.removeEventListener(flash.events.MouseEvent.ROLL_OUT, arg1.eventHandlerTooltip);
                arg1.tipInstance.removeEventListener(flash.events.MouseEvent.MOUSE_MOVE, arg1.eventHandlerTooltip);
                arg1.tipInstance.removeEventListener(flash.events.MouseEvent.MOUSE_DOWN, arg1.eventHandlerTooltip);
                arg1.tipInstance.removeEventListener(flash.events.MouseEvent.CLICK, arg1.eventHandlerTooltip);
                arg1.tipInstance.removeEventListener(flash.events.Event.CLOSE, arg1.eventHandlerTooltip);
                arg1.tipInstance.removeEventListener(lesta.managers.events.DragEvent.START_DRAGGING, arg1.eventHandlerTooltip);
                arg1.tipInstance.removeEventListener(lesta.managers.events.DragEvent.STOP_DRAGGING, arg1.eventHandlerTooltip);
                arg1.tipInstance.removeEventListener(flash.events.Event.RESIZE, arg1.eventHandlerTooltip);
            }
            var loc1:*=this._tooltipBehaviours[arg1.behaviour];
            loc1.onUnregisterTooltip(this.scene, arg1);
            var loc2:*=this.visibleTooltips.indexOf(arg1);
            if (loc2 > -1) 
            {
                this.visibleTooltips.splice(loc2, 1);
            }
            delete this.mapTooltips[arg1.id];
            loc2 = this.allTooltips.indexOf(arg1);
            this.allTooltips.splice(loc2, 1);
            arg1.fini();
            return;
        }

        protected function getPermissionToHandleTooltipEvent(arg1:lesta.managers.tooltipBehaviour.TooltipInfo, arg2:__AS3__.vec.Vector.<lesta.managers.tooltipBehaviour.TooltipInfo>):Boolean
        {
            var loc2:*=0;
            var loc3:*=0;
            if (arg1 == null || this.allTooltips.indexOf(arg1) == -1) 
            {
                return false;
            }
            var loc1:*=this.mapBehavioursIdsByName["window"];
            if (arg2.length > 0 && !(arg1.behaviour == loc1)) 
            {
                loc2 = 0;
                while (loc2 < arg2.length) 
                {
                    if ((loc3 = arg2[loc2].behaviour) > arg1.behaviour && !(loc3 == loc1)) 
                    {
                        return false;
                    }
                    ++loc2;
                }
            }
            return true;
        }

        internal function getConcurrentTooltips(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):__AS3__.vec.Vector.<lesta.managers.tooltipBehaviour.TooltipInfo>
        {
            var loc3:*=0;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc1:*=new Vector.<lesta.managers.tooltipBehaviour.TooltipInfo>();
            var loc2:*=this.mapBehavioursIdsByName["window"];
            if (arg1.behaviour != loc2) 
            {
                loc3 = 0;
                while (loc3 < this.visibleTooltips.length) 
                {
                    loc5 = (loc4 = this.visibleTooltips[loc3]).tooltipReason;
                    loc6 = arg1.tooltipReason;
                    if (!(arg1 == loc4) && (loc5 == loc6 || loc5.contains(loc6)) && !(loc4.behaviour == loc2)) 
                    {
                        loc1.push(loc4);
                    }
                    ++loc3;
                }
            }
            return loc1;
        }

        internal var _tooltipBehaviours:__AS3__.vec.Vector.<lesta.managers.tooltipBehaviour.ITooltipBehaviour>;

        internal var tooltipBehaviourMethodNames:Object;

        internal var scene:flash.display.DisplayObjectContainer;

        internal var allTooltips:__AS3__.vec.Vector.<lesta.managers.tooltipBehaviour.TooltipInfo>;

        internal var mapTooltips:flash.utils.Dictionary;

        internal var visibleTooltips:__AS3__.vec.Vector.<lesta.managers.tooltipBehaviour.TooltipInfo>;

        internal var lastTooltipId:int=0;

        internal var outsiders:*;

        internal var notOutsiders:*;

        internal var notOutsideForNextClick:*;

        public var mapBehavioursIdsByName:Object;
    }
}
