package lesta.managers 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import flash.utils.*;
    import lesta.data.*;
    import lesta.managers.events.*;
    
    public class DragAndDropManager extends Object
    {
        public function DragAndDropManager(arg1:flash.display.Stage, arg2:flash.display.DisplayObjectContainer)
        {
            super();
            this.dragLayer = arg2;
            this.stage = arg1;
            this.draggables = new flash.utils.Dictionary();
            this.droppables = new flash.utils.Dictionary();
            lesta.data.GameDelegate.addCallBack("inputHandler.stopDraggingFlashByUpOutSide", this, this.onStopDraggingFlashByUpOutSide);
            return;
        }

        internal function stopDragging():void
        {
            if (this.nowDragging != null) 
            {
                this.tryAcceptDraggable();
                this.freeNowDragging();
            }
            this.freePrepareForDragListeners();
            return;
        }

        internal function tryAcceptDraggable():void
        {
            if (this.nowInteractingDroppable != null) 
            {
                this.fadeOutDroppable(this.nowInteractingDroppable);
                this.acceptDraggable(this.nowInteractingDroppable);
                this.nowInteractingDroppable = null;
            }
            return;
        }

        internal function tryInteractWithDroppables():void
        {
            var loc1:*=this.findDroppableInteractingWith(this.nowDragging);
            if (this.nowInteractingDroppable != loc1) 
            {
                this.fadeOutDroppable(this.nowInteractingDroppable);
                this.fadeInDroppable(loc1);
                this.nowInteractingDroppable = loc1;
            }
            return;
        }

        internal function fadeOutDroppable(arg1:flash.display.DisplayObject):void
        {
            this.executeIfNotNull(arg1, 2);
            return;
        }

        internal function fadeInDroppable(arg1:flash.display.DisplayObject):void
        {
            this.executeIfNotNull(arg1, 1);
            return;
        }

        internal function acceptDraggable(arg1:flash.display.DisplayObject):void
        {
            this.executeIfNotNull(arg1, 0);
            return;
        }

        internal function executeIfNotNull(arg1:flash.display.DisplayObject, arg2:int):void
        {
            var loc1:*=null;
            if (arg1 != null) 
            {
                loc1 = this.droppables[arg1][this.nowDraggingClass][arg2];
                if (loc1 != null) 
                {
                    loc1();
                }
            }
            return;
        }

        internal function findDroppableInteractingWith(arg1:flash.display.DisplayObject):flash.display.DisplayObject
        {
            var loc4:*=undefined;
            var loc5:*=null;
            var loc6:*=NaN;
            var loc1:*=null;
            var loc2:*=1e-008;
            var loc3:*=arg1.getBounds(this.dragLayer);
            var loc7:*=0;
            var loc8:*=this.droppables;
            for (loc4 in loc8) 
            {
                loc5 = loc4.getBounds(this.dragLayer);
                if (!((loc6 = this.computeAABBIntersectionArea(loc3, loc5)) > loc2)) 
                {
                    continue;
                }
                loc1 = loc4;
                loc2 = loc6;
            }
            return loc1;
        }

        internal function computeAABBIntersectionArea(arg1:flash.geom.Rectangle, arg2:flash.geom.Rectangle):Number
        {
            var loc1:*=Math.max(0, Math.min(arg1.x + arg1.width, arg2.x + arg2.width) - Math.max(arg1.x, arg2.x));
            var loc2:*=Math.max(0, Math.min(arg1.y + arg1.height, arg2.y + arg2.height) - Math.max(arg1.y, arg2.y));
            return loc1 * loc2;
        }

        internal function freePrepareForDragListeners():void
        {
            this.preparingToDrag = null;
            this.stage.removeEventListener(flash.events.MouseEvent.MOUSE_MOVE, this.onMouseMove, true);
            this.stage.removeEventListener(flash.events.MouseEvent.MOUSE_UP, this.onMouseUp, true);
            this.stage.removeEventListener(flash.events.MouseEvent.CLICK, this.onMouseEvent, true);
            this.stage.removeEventListener(flash.events.MouseEvent.ROLL_OUT, this.onMouseEvent, true);
            this.stage.removeEventListener(flash.events.MouseEvent.ROLL_OVER, this.onMouseEvent, true);
            lesta.managers.CursorManager.setCursorFlag(lesta.managers.CursorFlags.HANGAR_DRAGGING, false);
            return;
        }

        internal function freeNowDragging():void
        {
            if (!this.dragParams.sameLayer) 
            {
                this.dragLayer.removeChild(this.nowDragging);
            }
            if (this.preparingToDrag[1] != null) 
            {
                var loc1:*;
                (loc1 = this.preparingToDrag)[1](this.nowDragging);
            }
            this.nowDragging.dispatchEvent(new lesta.managers.events.DragEvent(lesta.managers.events.DragEvent.STOP_DRAGGING));
            this.nowDragging = null;
            return;
        }

        internal function climbPosition(arg1:Number, arg2:Number, arg3:Number):Number
        {
            var loc1:*=arg2 - arg1 - sceenBorderOffset;
            var loc2:*=arg3;
            loc2 = Math.max(sceenBorderOffset, loc2);
            return loc2 = Math.min(loc1, loc2);
        }

        public function free():void
        {
            var loc1:*=null;
            lesta.data.GameDelegate.removeCallBack(this);
            var loc2:*=0;
            var loc3:*=this.draggables;
            for (loc1 in loc3) 
            {
                this.unregisterDraggable(flash.display.DisplayObject(loc1));
            }
            loc2 = 0;
            loc3 = this.droppables;
            for (loc1 in loc3) 
            {
                this.unregisterDroppable(flash.display.DisplayObject(loc1));
            }
            this.stage = null;
            return;
        }

        public function registerDraggable(arg1:flash.display.DisplayObject, arg2:Function, arg3:Function, arg4:Object):void
        {
            this.draggables[arg1] = [arg2, arg3, arg4];
            arg1.addEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.onDraggableMouseDown);
            return;
        }

        public function unregisterDraggable(arg1:flash.display.DisplayObject):void
        {
            if (this.preparingToDrag == this.draggables[arg1]) 
            {
                if (this.nowDragging != null) 
                {
                    this.freeNowDragging();
                }
                this.freePrepareForDragListeners();
            }
            delete this.draggables[arg1];
            arg1.removeEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.onDraggableMouseDown);
            return;
        }

        public function registerDroppable(arg1:flash.display.DisplayObject, arg2:flash.utils.Dictionary):void
        {
            this.droppables[arg1] = arg2;
            return;
        }

        public function unregisterDroppable(arg1:flash.display.DisplayObject):void
        {
            delete this.droppables[arg1];
            return;
        }

        public function setPosition(arg1:flash.display.DisplayObject, arg2:Number, arg3:Number):void
        {
            arg1.x = this.climbPosition(arg1.width, this.stage.stageWidth, arg2);
            arg1.y = this.climbPosition(arg1.height, this.stage.stageHeight, arg3);
            return;
        }

        internal function onDraggableMouseDown(arg1:flash.events.MouseEvent):void
        {
            var loc1:*=arg1.currentTarget as flash.display.DisplayObject;
            this.preparingToDrag = this.draggables[loc1];
            this.preparingToDragPosition = new flash.geom.Point(arg1.stageX, arg1.stageY);
            this.stage.addEventListener(flash.events.MouseEvent.MOUSE_MOVE, this.onMouseMove, true);
            this.stage.addEventListener(flash.events.MouseEvent.MOUSE_UP, this.onMouseUp, true);
            this.stage.addEventListener(flash.events.MouseEvent.CLICK, this.onMouseEvent, true);
            this.stage.addEventListener(flash.events.MouseEvent.ROLL_OUT, this.onMouseEvent, true);
            this.stage.addEventListener(flash.events.MouseEvent.ROLL_OVER, this.onMouseEvent, true);
            lesta.managers.CursorManager.setCursorFlag(lesta.managers.CursorFlags.HANGAR_DRAGGING, true);
            return;
        }

        internal function onMouseMove(arg1:flash.events.MouseEvent):void
        {
            var loc1:*=NaN;
            var loc2:*=NaN;
            var loc3:*=NaN;
            if (this.nowDragging == null) 
            {
                this.dragParams = this.preparingToDrag[2];
                loc1 = this.dragParams.distance || 1;
                loc2 = arg1.stageX - this.preparingToDragPosition.x;
                loc3 = arg1.stageY - this.preparingToDragPosition.y;
                if (loc2 * loc2 + loc3 * loc3 >= loc1 * loc1) 
                {
                    var loc4:*;
                    this.nowDragging = (loc4 = this.preparingToDrag)[0]();
                    this.nowDraggingClass = flash.utils.getDefinitionByName(flash.utils.getQualifiedClassName(this.nowDragging)) as Class;
                    this.nowDragging.dispatchEvent(new lesta.managers.events.DragEvent(lesta.managers.events.DragEvent.START_DRAGGING));
                    if (!this.dragParams.sameLayer) 
                    {
                        this.dragLayer.addChild(this.nowDragging);
                    }
                    this.nowDraggingClickPosition = new flash.geom.Point(arg1.stageX, arg1.stageY);
                    this.nowDraggingElementStartPosition = this.dragParams.stickToCursor ? new flash.geom.Point(arg1.stageX, arg1.stageY) : new flash.geom.Point(this.nowDragging.x, this.nowDragging.y);
                    this.placeDraggable(arg1.stageX, arg1.stageY);
                }
            }
            else 
            {
                arg1.stopImmediatePropagation();
                this.placeDraggable(arg1.stageX, arg1.stageY);
                this.tryInteractWithDroppables();
            }
            return;
        }

        internal function onMouseEvent(arg1:flash.events.MouseEvent):void
        {
            if (this.nowDragging != null) 
            {
                arg1.stopImmediatePropagation();
            }
            return;
        }

        internal function placeDraggable(arg1:Number, arg2:Number):void
        {
            var loc1:*=this.nowDraggingElementStartPosition.x + arg1 - this.nowDraggingClickPosition.x;
            var loc2:*=this.nowDraggingElementStartPosition.y + arg2 - this.nowDraggingClickPosition.y;
            this.nowDragging.x = this.climbPosition(this.nowDragging.width, this.stage.stageWidth, loc1);
            this.nowDragging.y = this.climbPosition(this.nowDragging.height, this.stage.stageHeight, loc2);
            return;
        }

        internal function onStopDraggingFlashByUpOutSide():void
        {
            this.stopDragging();
            return;
        }

        internal function onMouseUp(arg1:flash.events.MouseEvent=null):void
        {
            arg1.stopImmediatePropagation();
            this.stopDragging();
            return;
        }

        public static const sceenBorderOffset:Number=6;

        internal var stage:flash.display.Stage;

        internal var dragLayer:flash.display.DisplayObjectContainer;

        internal var draggables:flash.utils.Dictionary;

        internal var droppables:flash.utils.Dictionary;

        internal var nowDragging:flash.display.DisplayObject;

        internal var nowDraggingClass:Class;

        internal var dragParams:Object;

        internal var nowDraggingElementStartPosition:flash.geom.Point;

        internal var nowDraggingClickPosition:flash.geom.Point;

        internal var preparingToDrag:Object;

        internal var preparingToDragPosition:flash.geom.Point;

        internal var nowInteractingDroppable:flash.display.DisplayObject;
    }
}
