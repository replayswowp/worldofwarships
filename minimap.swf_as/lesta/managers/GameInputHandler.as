package lesta.managers 
{
    import flash.display.*;
    import flash.utils.*;
    import scaleform.clik.events.*;
    import scaleform.clik.managers.*;
    import scaleform.clik.ui.*;
    
    public class GameInputHandler extends Object
    {
        public function GameInputHandler()
        {
            this.mInputHandlers = new flash.utils.Dictionary();
            this.mListDisabledScope = new Array();
            super();
            this.mInputHandlers = new flash.utils.Dictionary();
            return;
        }

        public function initialize():void
        {
            this.mInited = true;
            this.mInputDelegate = scaleform.clik.managers.InputDelegate.getInstance();
            this.mInputDelegate.addEventListener(scaleform.clik.events.InputEvent.INPUT, this.handleInput);
            this.mFocusHandler = scaleform.clik.managers.FocusHandler.instance;
            return;
        }

        public function setKeyHandler(arg1:Number, arg2:String, arg3:Function, arg4:flash.display.DisplayObject=null, arg5:Boolean=false, arg6:Boolean=false, arg7:Boolean=false):void
        {
            var loc2:*=null;
            if (this.mInputHandlers[arg1] == undefined) 
            {
                this.mInputHandlers[arg1] = new flash.utils.Dictionary();
            }
            if (this.mInputHandlers[arg1][arg2] == undefined) 
            {
                this.mInputHandlers[arg1][arg2] = new Array();
            }
            var loc1:*=false;
            var loc3:*=0;
            var loc4:*=this.mInputHandlers[arg1][arg2];
            for each (loc2 in loc4) 
            {
                loc1 = loc1 || loc2.callback == arg3 && loc2.scope == arg4 && loc2.isShift == arg5 && loc2.isCtrl == arg6 && loc2.isAlt == arg7;
            }
            if (!loc1) 
            {
                this.mInputHandlers[arg1][arg2].push({"callback":arg3, "isShift":arg5, "isCtrl":arg6, "isAlt":arg7, "scope":arg4});
            }
            return;
        }

        public function clearKeyHandler(arg1:Number, arg2:String, arg3:Function):void
        {
            if (this.mInputHandlers[arg1] == undefined) 
            {
                return;
            }
            if (arg3 == null) 
            {
                this.mInputHandlers[arg1][arg2] = null;
                return;
            }
            var loc1:*;
            var loc2:*=((loc1 = this.mInputHandlers[arg1][arg2]).length - 1);
            while (loc2 >= 0) 
            {
                if (loc1[loc2].callback == arg3) 
                {
                    loc1.splice(loc2, 1);
                }
                --loc2;
            }
            return;
        }

        public function clearKeyHandlerByScope(arg1:flash.display.DisplayObject):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=0;
            var loc5:*=null;
            var loc6:*=0;
            var loc7:*=this.mInputHandlers;
            for (loc1 in loc7) 
            {
                var loc8:*=0;
                var loc9:*=this.mInputHandlers[loc1];
                for (loc2 in loc9) 
                {
                    loc4 = ((loc3 = this.mInputHandlers[loc1][loc2]).length - 1);
                    while (loc4 >= 0) 
                    {
                        if ((loc5 = loc3[loc4]).scope == arg1) 
                        {
                            loc3.splice(loc4, 1);
                        }
                        --loc4;
                    }
                }
            }
            return;
        }

        public function clearKeyHandlers():void
        {
            this.mInputHandlers = new flash.utils.Dictionary();
            return;
        }

        public function dumpCallbacks():void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=0;
            var loc7:*=null;
            var loc1:*=new flash.utils.Dictionary();
            var loc8:*=0;
            var loc9:*=this.mInputHandlers;
            for (loc2 in loc9) 
            {
                var loc10:*=0;
                var loc11:*=this.mInputHandlers[loc2];
                for (loc4 in loc11) 
                {
                    loc6 = ((loc5 = this.mInputHandlers[loc2][loc4]).length - 1);
                    while (loc6 >= 0) 
                    {
                        if (!((loc7 = loc5[loc6].scope) in loc1)) 
                        {
                            loc1[loc7] = [];
                        }
                        loc1[loc7].push([loc2, loc4]);
                        --loc6;
                    }
                }
            }
            trace("******************");
            loc8 = 0;
            loc9 = loc1;
            for (loc3 in loc9) 
            {
                trace(loc3, ": ", loc1[loc3]);
            }
            trace("******************");
            trace(this.mListDisabledScope);
            return;
        }

        public function handleInput(arg1:Object):void
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc1:*=arg1.details;
            if (!(this.mLastInputDetails == null) && loc1.code == this.mLastInputDetails.code && loc1.value == this.mLastInputDetails.value) 
            {
                return;
            }
            this.mLastInputDetails = loc1;
            if (this.mInputHandlers[loc1.code] == null) 
            {
                return;
            }
            if (this.mInputHandlers[loc1.code][loc1.value] == null) 
            {
                return;
            }
            var loc2:*=this.mInputHandlers[loc1.code][loc1.value];
            var loc3:*=[];
            var loc6:*=0;
            var loc7:*=loc2;
            for each (loc4 in loc7) 
            {
                if (!this.isScopeInputEnabled(loc4.scope)) 
                {
                    continue;
                }
                if (!(loc4.isShift == loc1.shiftKey) || !(loc4.isAlt == loc1.altKey) || !(loc4.isCtrl == loc1.ctrlKey)) 
                {
                    continue;
                }
                loc3.push(loc4);
                this.mSendedInputDetails = loc1;
            }
            loc6 = 0;
            loc7 = loc3;
            for each (loc5 in loc7) 
            {
                loc5.callback(arg1);
            }
            return;
        }

        public function setScopeInputEnabled(arg1:flash.display.DisplayObject, arg2:Boolean):void
        {
            var loc1:*=this.mListDisabledScope.indexOf(arg1);
            if (loc1 == -1 && !arg2) 
            {
                this.mListDisabledScope.push(arg1);
            }
            else if (!(loc1 == -1) && arg2) 
            {
                this.mListDisabledScope.splice(loc1, 1);
            }
            return;
        }

        public function isScopeInputEnabled(arg1:flash.display.DisplayObject):Boolean
        {
            return this.mListDisabledScope.indexOf(arg1) == -1;
        }

        public static function get instance():lesta.managers.GameInputHandler
        {
            if (_instance == null) 
            {
                _instance = new GameInputHandler();
            }
            return _instance;
        }

        
        {
            _instance = instance;
        }

        internal var mInputDelegate:scaleform.clik.managers.InputDelegate;

        internal var mFocusHandler:scaleform.clik.managers.FocusHandler;

        internal var mInited:Boolean=false;

        internal var mInputHandlers:flash.utils.Dictionary;

        internal var mLastInputDetails:scaleform.clik.ui.InputDetails;

        internal var mSendedInputDetails:scaleform.clik.ui.InputDetails;

        internal var mListDisabledScope:Array;

        internal static var _instance:lesta.managers.GameInputHandler;
    }
}
