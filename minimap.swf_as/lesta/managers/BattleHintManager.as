package lesta.managers 
{
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.constants.*;
    import lesta.cpp.*;
    import lesta.structs.*;
    import lesta.utils.*;
    
    public class BattleHintManager extends Object
    {
        public function BattleHintManager()
        {
            this.tooltipDataDict = new flash.utils.Dictionary(true);
            super();
            return;
        }

        public function register(arg1:flash.display.DisplayObject, arg2:Object):void
        {
            if (!arg2) 
            {
                return;
            }
            this.tooltipDataDict[arg1] = arg2;
            arg1.addEventListener(flash.events.MouseEvent.MOUSE_OVER, this.showTooltip);
            arg1.addEventListener(flash.events.MouseEvent.MOUSE_OUT, this.hideTooltip);
            this.dataToShow = arg2;
            this.setDataToHint();
            return;
        }

        public function unregister(arg1:flash.display.DisplayObject):void
        {
            arg1.removeEventListener(flash.events.MouseEvent.MOUSE_OVER, this.showTooltip);
            arg1.removeEventListener(flash.events.MouseEvent.MOUSE_OUT, this.hideTooltip);
            delete this.tooltipDataDict[arg1];
            return;
        }

        public function fini():void
        {
            var loc2:*=null;
            this.hideTimer.stop();
            this.hideTimer.removeEventListener(flash.events.TimerEvent.TIMER, this.hideTooltip);
            var loc1:*=[];
            var loc3:*=0;
            var loc4:*=this.tooltipDataDict;
            for (loc2 in loc4) 
            {
                loc1.push(loc2);
            }
            loc3 = 0;
            loc4 = loc1;
            for each (loc2 in loc4) 
            {
                this.unregister(loc2 as flash.display.DisplayObject);
            }
            return;
        }

        public function init(arg1:flash.display.MovieClip):void
        {
            var loc2:*=null;
            this.tooltip = arg1;
            this.content = arg1.content;
            this.tooltip.alpha = 0;
            this.tooltip.visible = false;
            var loc1:*=0;
            while (loc1 < this.tooltip.numChildren) 
            {
                loc2 = this.tooltip.getChildAt(loc1);
                if (loc2.name != "size") 
                {
                    loc2.visible = false;
                }
                ++loc1;
            }
            this.hideTimer = new flash.utils.Timer(this.lifetime, 1);
            this.hideTimer.addEventListener(flash.events.TimerEvent.TIMER, this.hideTooltip);
            return;
        }

        internal function showTooltip(arg1:flash.events.MouseEvent):void
        {
            this.dataToShow = this.tooltipDataDict[arg1.target];
            this.setDataToHint();
            this.tooltip.visible = true;
            this.currentTweenId = lesta.managers.TweenManager.addTweenExplicitly(this.tooltip, 0.3, {"alpha":this.tooltip.alpha}, {"alpha":1}, null, 0, 0.5);
            this.hideTimer.reset();
            this.hideTimer.start();
            return;
        }

        internal function hideTooltip(arg1:flash.events.Event):void
        {
            var event:flash.events.Event;

            var loc1:*;
            event = arg1;
            this.hideTimer.stop();
            lesta.managers.TweenManager.removeTween(this.tooltip, this.currentTweenId);
            lesta.managers.TweenManager.addTweenExplicitly(this.tooltip, 0.3, {"alpha":this.tooltip.alpha}, {"alpha":0}, function ():void
            {
                tooltip.visible = false;
                return;
            })
            return;
        }

        internal function setDataToHint():void
        {
            this.showHint(this.dataToShow);
            this.tooltip.size.height = this.content.height + this.GAP;
            this.tooltip.dispatchEvent(new flash.events.Event(flash.events.Event.RESIZE));
            return;
        }

        internal function showHint(arg1:Object):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            if (this.content) 
            {
                this.content.visible = false;
            }
            if (arg1 is lesta.structs.WeaponsInfo) 
            {
                loc1 = arg1 as lesta.structs.WeaponsInfo;
                var loc4:*=loc1.weaponType;
                switch (loc4) 
                {
                    case lesta.constants.WeaponType.ARTILLERY:
                    {
                        this.content = this.tooltip.artillery;
                        this.setCommonDataToContent(arg1);
                        this.showArtilleryHint(loc1);
                        break;
                    }
                    case lesta.constants.WeaponType.TORPEDO:
                    {
                        this.content = this.tooltip.torpedo;
                        this.setCommonDataToContent(arg1);
                        this.showTorpedoHint(loc1);
                        break;
                    }
                    case lesta.constants.WeaponType.AIRPLANES:
                    {
                        loc2 = loc1.params.squadronType;
                        if (loc2 != lesta.constants.PlaneTypes.FIGHTER) 
                        {
                            this.content = this.tooltip.bomberPlanes;
                            this.setCommonDataToContent(arg1);
                            this.showPlaneHint(loc1, "IDS_MAX_DAMAGE", loc1.params.maxDamage, false);
                        }
                        else 
                        {
                            this.content = this.tooltip.fighterPlanes;
                            this.setCommonDataToContent(arg1);
                            this.showPlaneHint(loc1, "IDS_SHIP_PARAM_SQUADRON_MED_DAMAGE", loc1.params.medDamage, true);
                        }
                        break;
                    }
                    default:
                    {
                        this.showGeneralHint(arg1);
                    }
                }
            }
            else if (arg1 is lesta.structs.ConsumablesInfo) 
            {
                loc3 = arg1 as lesta.structs.ConsumablesInfo;
                this.content = this.tooltip.consumes;
                this.setCommonDataToContent(arg1);
                this.showConsumableHint(loc3);
            }
            return;
        }

        internal function showArtilleryHint(arg1:lesta.structs.WeaponsInfo):void
        {
            var loc1:*=arg1.params;
            if (!this.content.inited) 
            {
                this.content.paramsList.paramItem0.ids.text = "IDS_RELOADTIME";
                this.content.paramsList.paramItem1.ids.text = "IDS_MAX_DAMAGE";
                this.content.paramsList.paramItem2.ids.text = "IDS_TOOLTIP_ARTILLERY_SHOOT_DIST";
                lesta.utils.DisplayObjectUtils.placeChildrenAsVerticalStack(this.content.paramsList);
                this.content.inited = true;
            }
            this.content.paramsList.paramItem0.value.text = String(loc1.shotDelay) + " " + lesta.cpp.Translator.getLocalization("IDS_SECOND");
            this.content.paramsList.paramItem1.value.text = String(loc1["damage" + loc1.ammoType]) + " " + lesta.cpp.Translator.getLocalization("IDS_UNITS");
            this.content.paramsList.paramItem2.value.text = String(loc1.maxDist) + " " + lesta.cpp.Translator.getLocalization("IDS_KILOMETER");
            this.content.innerPanel.height = this.content.paramsList.height + 2 * this.GAP;
            return;
        }

        internal function showTorpedoHint(arg1:lesta.structs.WeaponsInfo):void
        {
            var loc1:*=arg1.params;
            if (!this.content.inited) 
            {
                this.content.paramsList.paramItem0.ids.text = "IDS_RELOADTIME";
                this.content.paramsList.paramItem1.ids.text = "IDS_MAX_DAMAGE";
                this.content.paramsList.paramItem2.ids.text = "IDS_TOOLTIP_ARTILLERY_SHOOT_DIST";
                this.content.paramsList.paramItem3.ids.text = "IDS_SHIP_PARAM_TORPEDO_SPEED";
                lesta.utils.DisplayObjectUtils.placeChildrenAsVerticalStack(this.content.paramsList);
                this.content.inited = true;
            }
            this.content.paramsList.paramItem0.value.text = String(loc1.shotDelay) + " " + lesta.cpp.Translator.getLocalization("IDS_SECOND");
            this.content.paramsList.paramItem1.value.text = Math.round(loc1.damage).toString() + " " + lesta.cpp.Translator.getLocalization("IDS_UNITS");
            this.content.paramsList.paramItem2.value.text = String(loc1.maxDist) + " " + lesta.cpp.Translator.getLocalization("IDS_KILOMETER");
            this.content.paramsList.paramItem3.value.text = String(loc1.speed) + " " + lesta.cpp.Translator.translatePlural("IDS_PL_KNOTS", loc1.speed);
            this.content.innerPanel.height = this.content.paramsList.height + 2 * this.GAP;
            return;
        }

        internal function showConsumableHint(arg1:lesta.structs.ConsumablesInfo):void
        {
            var loc1:*=arg1.params[0];
            var loc2:*=arg1.params[1];
            if (!this.content.inited) 
            {
                this.content.paramsList.paramItem0.ids.text = loc1.ids;
                this.content.paramsList.paramItem1.ids.text = loc2.ids;
                this.content.inited = true;
            }
            var loc3:*=lesta.cpp.Translator.getLocalization("IDS_SECOND");
            this.content.paramsList.paramItem0.value.text = loc1.value + " " + loc3;
            this.content.paramsList.paramItem1.value.text = loc2.value + " " + loc3;
            this.content.innerPanel.height = this.content.paramsList.height + 2 * this.GAP;
            return;
        }

        internal function showPlaneHint(arg1:lesta.structs.WeaponsInfo, arg2:String, arg3:Number, arg4:Boolean):void
        {
            var loc1:*=arg1.params;
            if (!this.content.inited) 
            {
                this.content.paramsList.paramItemSpeed.ids.text = "IDS_SHIP_PARAM_SQUADRON_SPEED";
                this.content.paramsList.paramItemDamage.ids.text = arg2;
                if (arg4) 
                {
                    this.content.paramsList.paramItemAmmoAmount.ids.text = "IDS_SHIP_PARAM_SQUADRON_AMMO_AMOUNT";
                }
                this.content.paramsList.paramItemVitality.ids.text = "IDS_SHIP_PARAM_SQUADRON_VITALITY";
                lesta.utils.DisplayObjectUtils.placeChildrenAsVerticalStack(this.content.paramsList);
                this.content.inited = true;
            }
            this.content.paramsList.paramItemSpeed.value.text = String(loc1.speed) + " " + lesta.cpp.Translator.translatePlural("IDS_PL_KNOTS", loc1.speed);
            this.content.paramsList.paramItemDamage.value.text = Math.round(arg3).toString() + " " + lesta.cpp.Translator.getLocalization("IDS_UNITS");
            if (arg4) 
            {
                this.content.paramsList.paramItemAmmoAmount.value.text = String(Math.floor(loc1.ammoAmount)) + " " + lesta.cpp.Translator.getLocalization("IDS_UNITS");
            }
            this.content.paramsList.paramItemVitality.value.text = String(Math.round(loc1.vitality)) + " " + lesta.cpp.Translator.getLocalization("IDS_UNITS");
            this.content.innerPanel.height = this.content.paramsList.height + 2 * this.GAP;
            return;
        }

        internal function setCommonDataToContent(arg1:Object):void
        {
            if (!this.content.inited) 
            {
                this.content.icon.init(true);
                this.content.typeText.autoSize = "left";
                this.content.typeText.wordWrap = true;
                this.content.descriptionText.autoSize = "left";
                this.content.descriptionText.wordWrap = true;
            }
            this.content.icon.src = arg1.iconPath;
            this.content.typeText.text = arg1.title;
            this.content.descriptionText.text = arg1.description;
            this.content.divider_2.y = this.content.descriptionText.y + this.content.descriptionText.height + this.GAP;
            this.content.paramsList.y = this.content.divider_2.y + this.content.divider_2.height + this.GAP;
            this.content.innerPanel.y = this.content.divider_2.y + this.content.divider_2.height;
            this.content.innerPanel.height = 0;
            this.content.visible = true;
            return;
        }

        internal function showGeneralHint(arg1:Object):void
        {
            this.content.visible = true;
            if (arg1["iconPath"]) 
            {
                this.content["icon"].src = arg1["iconPath"];
            }
            if (arg1["iconClass"]) 
            {
                this.content["icon"].className = arg1["iconClass"];
            }
            var loc1:*=arg1["hintIdent"];
            this.content["typeText"].text = ("IDS_BATTLEHINT_TYPE_" + loc1).toUpperCase();
            this.content["descriptionText"].text = ("IDS_BATTLEHINT_DESCRIPTION_" + loc1).toUpperCase();
            this.content["usageText"].text = ("IDS_BATTLEHINT_USAGE_" + loc1).toUpperCase();
            this.content["usageText"].y = this.content["descriptionText"].y + this.content["descriptionText"].textHeight + this.GAP;
            this.tooltip["size"].height = this.content["icon"].height + this.content["usageText"].textHeight + this.content["descriptionText"].textHeight + 2 * this.GAP;
            this.tooltip.dispatchEvent(new flash.events.Event(flash.events.Event.RESIZE));
            return;
        }

        public const GAP:int=9;

        public const PARAMS_LIST_X:int=4;

        public var tooltip:flash.display.MovieClip;

        internal var content:flash.display.MovieClip;

        internal var tooltipDataDict:flash.utils.Dictionary;

        internal var dataToShow:Object;

        internal var hideTimer:flash.utils.Timer;

        internal var showDelay:Number=500;

        internal var lifetime:Number=8500;

        internal var currentTweenId:int;

        internal var paramItemClass:Class;
    }
}
