package lesta.dialogs.custom_hud 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    
    public class CustomHudGrid extends flash.display.MovieClip
    {
        public function CustomHudGrid()
        {
            this.horizLines = [];
            this.vertLines = [];
            this.horizLinesActive = [];
            this.vertLinesActive = [];
            super();
            this.horizLines = [this.horizUp, this.horizUpMiddle, this.horizBottom, this.horizBottomMiddle, this.horizCenter];
            this.vertLines = [this.vertCenter, this.vertLeft, this.vertLeftMiddle, this.vertRight, this.vertRightMiddle];
            this.horizLinesActive = [this.horizCenter];
            this.vertLinesActive = [this.vertCenter];
            return;
        }

        public function updateSize(arg1:int, arg2:int):void
        {
            var loc3:*=null;
            var loc4:*=null;
            if (!parent) 
            {
                return;
            }
            if (!this.detector) 
            {
                this.detector = (parent as lesta.dialogs.custom_hud.CustomHud).crossingDetector;
            }
            this.background.width = arg1;
            this.background.height = arg2;
            var loc1:*=int((arg2 / 2 - BORDER_GAP) * CENTER_BORDER_RELATION);
            var loc2:*=int((arg1 / 2 - BORDER_GAP) * CENTER_BORDER_RELATION);
            this.vertCenter.x = arg1 / 2;
            this.vertLeft.x = BORDER_GAP;
            this.vertLeftMiddle.x = BORDER_GAP + loc2;
            this.vertRight.x = arg1 - BORDER_GAP;
            this.vertRightMiddle.x = arg1 - BORDER_GAP - loc2;
            this.horizCenter.y = arg2 / 2;
            this.horizUp.y = BORDER_GAP;
            this.horizUpMiddle.y = BORDER_GAP + loc1;
            this.horizBottom.y = arg2 - BORDER_GAP;
            this.horizBottomMiddle.y = arg2 - BORDER_GAP - loc1;
            var loc5:*=0;
            var loc6:*=this.horizLines;
            for each (loc3 in loc6) 
            {
                loc3.width = arg1;
            }
            loc5 = 0;
            loc6 = this.vertLines;
            for each (loc4 in loc6) 
            {
                loc4.height = arg2;
            }
            this.detector.addEventListener(lesta.dialogs.custom_hud.CrossingDetector.HORIZONTAL_BLOCK_CHANGED, this.onBlockTypeChanged);
            this.detector.addEventListener(lesta.dialogs.custom_hud.CrossingDetector.VERTICAL_BLOCK_CHANGED, this.onBlockTypeChanged);
            return;
        }

        internal function onBlockTypeChanged(arg1:flash.events.Event):void
        {
            this.updateLineState(this.detector.selectedGlues);
            return;
        }

        public function updateLineState(arg1:Array):void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=this.horizLinesActive.concat(this.vertLinesActive);
            var loc4:*=0;
            var loc5:*=loc1;
            for each (loc2 in loc5) 
            {
                loc3 = lesta.dialogs.custom_hud.CrossingDetector.getGlueByElementId(arg1, loc2.name);
                loc2.alpha = loc3 ? loc3.distanceFromGlue / lesta.dialogs.custom_hud.CrossingDetector.MIN_DISTANCE_TO_GRID : 1;
            }
            return;
        }

        internal function detectorFunction(arg1:flash.display.MovieClip, arg2:int, arg3:Array):Object
        {
            var loc1:*=new flash.geom.Point(arg1.x, arg1.y);
            if (arg3 == this.vertLinesActive) 
            {
                return {"name":arg1.name, "coord":localToGlobal(loc1).x};
            }
            if (arg3 == this.horizLinesActive) 
            {
                return {"name":arg1.name, "coord":localToGlobal(loc1).y};
            }
            return 0;
        }

        public override function toString():String
        {
            return "gridLines vert: " + this.vertLinesActive.map(this.detectorFunction).sort(Array.NUMERIC).join(",") + " horiz: " + this.horizLinesActive.map(this.detectorFunction).sort(Array.NUMERIC).join(",");
        }

        public static function getGridProperties(arg1:Number, arg2:Number):Object
        {
            var loc1:*=new Object();
            var loc2:*=int((arg2 / 2 - BORDER_GAP) * CENTER_BORDER_RELATION);
            var loc3:*=int((arg1 / 2 - BORDER_GAP) * CENTER_BORDER_RELATION);
            loc1["vertCenter"] = new flash.geom.Point(arg1 / 2, 0);
            loc1["vertLeft"] = new flash.geom.Point(BORDER_GAP, 0);
            loc1["vertLeftMiddle"] = new flash.geom.Point(BORDER_GAP + loc3, 0);
            loc1["vertRight"] = new flash.geom.Point(arg1 - BORDER_GAP, 0);
            loc1["vertRightMiddle"] = new flash.geom.Point(arg1 - BORDER_GAP - loc3, 0);
            loc1["horizCenter"] = new flash.geom.Point(0, arg2 / 2);
            loc1["horizUp"] = new flash.geom.Point(0, BORDER_GAP);
            loc1["horizUpMiddle"] = new flash.geom.Point(0, BORDER_GAP + loc2);
            loc1["horizBottom"] = new flash.geom.Point(0, arg2 - BORDER_GAP);
            loc1["horizBottomMiddle"] = new flash.geom.Point(0, arg2 - BORDER_GAP - loc2);
            return loc1;
        }

        public static const BORDER_GAP:int=15;

        public static const CENTER_BORDER_RELATION:Number=0.6;

        public var background:flash.display.MovieClip;

        public var vertCenter:flash.display.MovieClip;

        public var horizCenter:flash.display.MovieClip;

        public var horizUp:flash.display.MovieClip;

        public var horizUpMiddle:flash.display.MovieClip;

        public var horizBottom:flash.display.MovieClip;

        public var horizBottomMiddle:flash.display.MovieClip;

        public var vertLeft:flash.display.MovieClip;

        public var vertLeftMiddle:flash.display.MovieClip;

        public var vertRight:flash.display.MovieClip;

        public var vertRightMiddle:flash.display.MovieClip;

        public var horizLines:Array;

        public var vertLines:Array;

        public var horizLinesActive:Array;

        public var vertLinesActive:Array;

        public var detector:lesta.dialogs.custom_hud.CrossingDetector;
    }
}
