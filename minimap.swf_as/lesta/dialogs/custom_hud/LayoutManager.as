package lesta.dialogs.custom_hud 
{
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.dialogs.battle_window_new.*;
    import lesta.events.*;
    import lesta.managers.windows.*;
    import lesta.structs.*;
    import lesta.unbound.core.*;
    import lesta.utils.*;
    import scaleform.gfx.*;
    
    public class LayoutManager extends flash.events.EventDispatcher
    {
        public function LayoutManager(arg1:lesta.managers.windows.Overlay, arg2:lesta.dialogs.battle_window_new.ConfigLoader)
        {
            this.containers = [];
            this.mapContainers = new flash.utils.Dictionary();
            this.listAllElementsNames = [];
            this.listPerfElementsNames = [];
            this.mapPerfGroupElements = {};
            this.elementsMap = new flash.utils.Dictionary();
            this.containerSize = {"width":0, "height":0};
            super();
            this.loader = arg2;
            this.elementsContainer = arg1;
            this.updateGrid();
            return;
        }

        public function setContainerSize(arg1:Number, arg2:Number):void
        {
            this.containerSize.width = arg1;
            this.containerSize.height = arg2;
            return;
        }

        public function init():void
        {
            return;
        }

        public function fini():void
        {
            this.loader = null;
            this.grid = null;
            this.elementsContainer = null;
            this.elementsMap = null;
            this.containers = null;
            this.mapContainers = null;
            return;
        }

        public function updateGrid():void
        {
            this.grid = lesta.dialogs.custom_hud.CustomHudGrid.getGridProperties(this.containerSize.width, this.containerSize.height);
            return;
        }

        public function updateElements(arg1:flash.events.Event=null):void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=this.sortedElements;
            for each (loc1 in loc3) 
            {
                this.updateElement(loc1);
            }
            return;
        }

        public function updateElementPartially(arg1:flash.events.Event):void
        {
            var loc1:*=arg1.target is flash.display.DisplayObject ? arg1.target.name : "";
            if (loc1 == "ribbonContainer") 
            {
                this.updateElementByName(loc1);
                return;
            }
            this.updateElementByName("damageClip");
            this.updateElementByName("alarmClipCenter");
            this.updateElementByName("gunMarkerPlaceholder");
            return;
        }

        public function updateElementByName(arg1:String):void
        {
            var loc1:*=this.getContainerByName(arg1);
            var loc2:*=this.elementsMap[arg1];
            this.setCoord(lesta.dialogs.custom_hud.CrossingDetector.HORIZONTAL, loc2, loc1);
            this.setCoord(lesta.dialogs.custom_hud.CrossingDetector.VERTICAL, loc2, loc1);
            return;
        }

        public function sortHudElements():void
        {
            var elements:Array;
            var freeElements:Array;
            var gluedElements:Array;
            var e:lesta.structs.CustomHudElement;

            var loc1:*;
            e = null;
            elements = this.loader.getLayoutCollection();
            elements.forEach(function (arg1:lesta.structs.CustomHudElement, arg2:int, arg3:Array):void
            {
                elementsMap[arg1.id] = arg1;
                return;
            })
            freeElements = elements.filter(this.isElementTied(false));
            gluedElements = elements.filter(this.isElementTied(true));
            var loc2:*=0;
            var loc3:*=gluedElements;
            for each (e in loc3) 
            {
                e.tieDepth = this.getTiedElements(e.id, []).length;
            }
            gluedElements.sortOn("tieDepth", Array.NUMERIC);
            this.sortedElements = freeElements.concat(gluedElements);
            return;
        }

        internal function updateElement(arg1:lesta.structs.CustomHudElement):void
        {
            var loc1:*=this.getContainerByName(arg1.id);
            if (!loc1) 
            {
                return;
            }
            if (loc1 is lesta.dialogs.custom_hud.CustomHudContainer) 
            {
                (loc1 as lesta.dialogs.custom_hud.CustomHudContainer).layout = arg1;
            }
            this.setCoord(lesta.dialogs.custom_hud.CrossingDetector.HORIZONTAL, arg1, loc1);
            this.setCoord(lesta.dialogs.custom_hud.CrossingDetector.VERTICAL, arg1, loc1);
            return;
        }

        public function getContainerByName(arg1:String):flash.display.DisplayObjectContainer
        {
            return this.mapContainers[arg1];
        }

        public function getClipByName(arg1:String):flash.display.DisplayObject
        {
            var id:String;
            var namesArray:Array;
            var containerName:String;
            var container:flash.display.DisplayObjectContainer;
            var containerCustomHud:lesta.dialogs.custom_hud.CustomHudContainer;
            var containerElementHud:lesta.dialogs.custom_hud.HudElementContainer;
            var clip:flash.display.DisplayObject;

            var loc1:*;
            id = arg1;
            namesArray = id.split(".");
            namesArray.forEach(function (arg1:String, arg2:int, arg3:Array):void
            {
                arg1.replace(new RegExp("^\\s+|\\s+$", "gs"), "");
                return;
            })
            containerName = namesArray[0];
            container = this.mapContainers[containerName];
            if (container == null) 
            {
                return null;
            }
            namesArray.shift();
            containerCustomHud = container as lesta.dialogs.custom_hud.CustomHudContainer;
            containerElementHud = container as lesta.dialogs.custom_hud.HudElementContainer;
            clip = containerCustomHud == null ? containerElementHud.getElement() : containerCustomHud.content;
            if (namesArray.length > 0) 
            {
                clip = this.getChildRecursive(namesArray, clip as flash.display.DisplayObjectContainer);
            }
            return clip;
        }

        internal function getChildRecursive(arg1:Array, arg2:flash.display.DisplayObjectContainer):flash.display.DisplayObject
        {
            var loc1:*=arg1.shift();
            var loc2:*;
            if (!(loc2 = arg2.getChildByName(loc1) as flash.display.DisplayObject)) 
            {
                loc2 = arg2[loc1] as flash.display.DisplayObject;
            }
            if (!loc2 || arg1.length > 0 && !(loc2 is flash.display.DisplayObjectContainer)) 
            {
                trace("[BATTLE_ELEMENTS]: there is no child with name " + loc1 + " or child is not DisplayObjectContainer");
                return null;
            }
            if (arg1.length == 0) 
            {
                return loc2;
            }
            return this.getChildRecursive(arg1, loc2 as flash.display.DisplayObjectContainer);
        }

        public function addChildRecursive(arg1:XMLList, arg2:flash.display.DisplayObjectContainer, arg3:Class=null):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=false;
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=null;
            var loc8:*=null;
            var loc9:*=null;
            var loc10:*=null;
            var loc11:*=null;
            var loc12:*=null;
            var loc13:*=false;
            var loc14:*=null;
            var loc15:*=null;
            var loc16:*=null;
            var loc17:*=null;
            var loc18:*=null;
            var loc19:*=0;
            var loc20:*=arg1;
            for each (loc1 in loc20) 
            {
                if (loc1.@enabled == "false") 
                {
                    continue;
                }
                loc2 = loc1.attribute("name").toString();
                loc3 = loc1.attribute("autoPerfTestGroup").toString();
                loc4 = !(loc1.attribute("autoPerfTest").toString() == "false");
                if (loc3 == "") 
                {
                    loc3 = null;
                }
                loc6 = loc1.attribute("class").toString();
                loc8 = new (loc7 = flash.utils.getDefinitionByName(loc6) as Class)() as flash.display.DisplayObjectContainer;
                scaleform.gfx.InteractiveObjectEx.setHitTestDisable(loc8 as flash.display.InteractiveObject, true);
                loc8.name = loc2;
                loc8.addEventListener(flash.events.Event.RESIZE, this.updateElements);
                loc8.addEventListener(lesta.events.UIEvent.RESIZE_ELEMENT, this.updateElementPartially);
                if (arg3) 
                {
                    ((loc5 = new arg3()) as lesta.dialogs.custom_hud.CustomHudContainer).content = loc8;
                }
                else 
                {
                    ((loc5 = new lesta.dialogs.custom_hud.HudElementContainer()) as lesta.dialogs.custom_hud.HudElementContainer).setElement(loc8);
                }
                loc5.name = loc8.name;
                arg2.addChild(loc5);
                this.containers.push(loc5);
                this.mapContainers[loc5.name] = loc5;
                if (loc4) 
                {
                    this.listPerfElementsNames.push(loc5.name);
                }
                this.listAllElementsNames.push(loc5.name);
                if (!(loc3 == null) && loc4) 
                {
                    if ((loc11 = this.mapPerfGroupElements[loc3]) == null) 
                    {
                        loc11 = new Array();
                        this.mapPerfGroupElements[loc3] = loc11;
                    }
                    loc11.push(loc5.name);
                }
                var loc21:*=0;
                var loc22:*=loc1.properties.@*;
                for each (loc9 in loc22) 
                {
                    if ((loc12 = loc9.name()) == "visible") 
                    {
                        continue;
                    }
                    if (loc12 == "hitTest") 
                    {
                        loc13 = lesta.utils.XMLUtils.castXML(loc9);
                        scaleform.gfx.InteractiveObjectEx.setHitTestDisable(loc8 as flash.display.InteractiveObject, !loc13);
                        continue;
                    }
                    if (!loc8.hasOwnProperty(loc12)) 
                    {
                        continue;
                    }
                    loc14 = lesta.unbound.core.UbReflector.getAccessors(loc1.attribute("class").toString(), loc8, [lesta.unbound.core.UbReflector.READ_WRITE, lesta.unbound.core.UbReflector.WRITE_ONLY]);
                    loc15 = lesta.unbound.core.UbReflector.getPublicVariables(loc1.attribute("class").toString(), loc8);
                    loc16 = loc14.concat(loc15);
                    var loc23:*=0;
                    var loc24:*=loc16;
                    for each (loc17 in loc24) 
                    {
                        if (loc17.name.toString() != loc9.name().toString()) 
                        {
                            continue;
                        }
                        loc8[loc17.name] = lesta.utils.XMLUtils.castXML(loc9);
                        break;
                    }
                }
                loc21 = 0;
                loc22 = loc1.methods.@*;
                for each (loc10 in loc22) 
                {
                    if (!(loc8.hasOwnProperty(loc10.name()) && loc8[loc10.name()] is Function)) 
                    {
                        continue;
                    }
                    loc18 = String(loc10) == "" ? null : [int(loc10)];
                    loc8[loc10.name()].apply(loc8, loc18);
                }
                if (!(loc1.elementList.length() > 0)) 
                {
                    continue;
                }
                this.addChildRecursive(loc1.elementList.children(), loc8, arg3);
            }
            return;
        }

        protected function setCoord(arg1:int, arg2:lesta.structs.CustomHudElement, arg3:flash.display.DisplayObject):void
        {
            var loc1:*=null;
            var loc3:*=null;
            var loc7:*=NaN;
            var loc8:*=null;
            var loc9:*=null;
            var loc10:*=NaN;
            loc1 = lesta.dialogs.custom_hud.CrossingDetector.CONSTRAINT_SIZE[arg1];
            var loc2:*=lesta.dialogs.custom_hud.CrossingDetector.CONSTRAINT_COORD[arg1];
            loc3 = arg1 != lesta.dialogs.custom_hud.CrossingDetector.HORIZONTAL ? arg2.vglue : arg2.hglue;
            var loc4:*=arg3["background"] || arg3["size"] ? arg3["background"] || arg3["size"] : arg3;
            var loc5:*=lesta.dialogs.custom_hud.CrossingDetector.getOffsetByConstraintType(loc4, loc3.elementConstraintType);
            var loc6:*=arg3["background"] || arg3["size"] ? 0 : arg3.getRect(arg3)[loc2];
            var loc11:*=loc3.type;
            switch (loc11) 
            {
                case lesta.dialogs.custom_hud.CustomHudGlue.BORDER:
                {
                    loc7 = int(loc3.glueElementID) == lesta.dialogs.custom_hud.CrossingDetector.LEFT || int(loc3.glueElementID) == lesta.dialogs.custom_hud.CrossingDetector.TOP ? loc3.distanceFromGlue : this.containerSize[loc1] - loc3.distanceFromGlue;
                    arg3[loc2] = Math.round(loc7 - loc5 - loc6);
                    break;
                }
                case lesta.dialogs.custom_hud.CustomHudGlue.LINE:
                {
                    arg3[loc2] = Math.round(this.grid[loc3.glueElementID][loc2] - loc5 + loc3.distanceFromGlue);
                    break;
                }
                case lesta.dialogs.custom_hud.CustomHudGlue.ELEMENT:
                {
                    if ((loc8 = this.getContainerByName(loc3.glueElementID)) != null) 
                    {
                        loc9 = loc8["pivot"] == null ? loc8["size"] : loc8["pivot"];
                        loc10 = lesta.dialogs.custom_hud.CrossingDetector.getOffsetByConstraintType(loc9, loc3.glueElementCoord, !(loc8["pivot"] == null));
                        arg3[loc2] = Math.round(loc8[loc2] + loc10 + loc3.distanceFromGlue - loc5);
                    }
                    break;
                }
            }
            return;
        }

        internal function isElementTied(arg1:Boolean):Function
        {
            var value:Boolean;

            var loc1:*;
            value = arg1;
            return function (arg1:lesta.structs.CustomHudElement, arg2:int, arg3:Array):Boolean
            {
                var loc1:*=arg1.hglue.type == lesta.dialogs.custom_hud.CustomHudGlue.ELEMENT || arg1.vglue.type == lesta.dialogs.custom_hud.CustomHudGlue.ELEMENT;
                return value ? loc1 : !loc1;
            }
        }

        internal function getTiedElements(arg1:String, arg2:Array):Array
        {
            var loc1:*=this.elementsMap[arg1];
            if (loc1.hglue.type != lesta.dialogs.custom_hud.CustomHudGlue.ELEMENT) 
            {
                if (loc1.vglue.type == lesta.dialogs.custom_hud.CustomHudGlue.ELEMENT) 
                {
                    arg2.push(loc1.vglue.glueElementID);
                    this.getTiedElements(loc1.vglue.glueElementID, arg2);
                }
            }
            else 
            {
                arg2.push(loc1.hglue.glueElementID);
                this.getTiedElements(loc1.hglue.glueElementID, arg2);
            }
            return arg2;
        }

        public var containers:Array;

        public var mapContainers:flash.utils.Dictionary;

        public var listAllElementsNames:Array;

        public var listPerfElementsNames:Array;

        public var mapPerfGroupElements:Object;

        public var elementsMap:flash.utils.Dictionary;

        public var elementsContainer:lesta.managers.windows.Overlay;

        public var grid:Object;

        public var loader:lesta.dialogs.battle_window_new.ConfigLoader;

        internal var containerSize:Object;

        internal var sortedElements:Array;
    }
}
