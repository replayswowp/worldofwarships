package lesta.dialogs.battle_window 
{
    import flash.display.*;
    
    public class TargetDelegator extends Object
    {
        public function TargetDelegator(arg1:flash.display.MovieClip, arg2:flash.display.MovieClip)
        {
            super();
            this._attackIcon = new IconController(arg1);
            this._targetIcon = new IconController(arg2);
            return;
        }

        public function playSource():void
        {
            this._attackIcon.play();
            return;
        }

        public function clearSource():void
        {
            this._attackIcon.clear();
            return;
        }

        public function playTarget():void
        {
            this._targetIcon.play();
            return;
        }

        internal var _targetIcon:IconController;

        internal var _attackIcon:IconController;
    }
}

import flash.display.*;
import flash.utils.*;


class IconController extends Object
{
    public function IconController(arg1:flash.display.MovieClip)
    {
        super();
        this._icon = arg1;
        this._icon.visible = false;
        this._icon.stop();
        return;
    }

    public function play():void
    {
        this._icon.visible = true;
        this._icon.play();
        this.clearTimeoutFunction();
        this._timeoutId = flash.utils.setTimeout(this.clear, TIMEOUT_ATTACK_TARGET);
        return;
    }

    public function clear():void
    {
        this.clearTimeoutFunction();
        this._icon.visible = false;
        this._icon.stop();
        this._timeoutId = -1;
        return;
    }

    internal function clearTimeoutFunction():void
    {
        if (this._timeoutId > -1) 
        {
            flash.utils.clearTimeout(this._timeoutId);
        }
        return;
    }

    internal static const TIMEOUT_ATTACK_TARGET:Number=3000;

    internal var _icon:flash.display.MovieClip;

    internal var _timeoutId:int=-1;
}