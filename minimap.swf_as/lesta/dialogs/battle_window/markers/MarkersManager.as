package lesta.dialogs.battle_window.markers 
{
    import flash.display.*;
    import lesta.cpp.*;
    import lesta.data.*;
    import lesta.dialogs.battle_window.markers.config.*;
    import lesta.dialogs.battle_window.markers.container.*;
    import lesta.dialogs.battle_window.markers.container.views.base.*;
    import lesta.dialogs.battle_window.markers.container.views.ships.*;
    import lesta.dialogs.battle_window.markers.settings.*;
    import lesta.managers.*;
    import lesta.structs.*;
    import lesta.utils.*;
    import lesta.utils.resourceLoading.*;
    import scaleform.clik.constants.*;
    
    public class MarkersManager extends flash.display.Sprite
    {
        public function MarkersManager()
        {
            super();
            this.init();
            return;
        }

        internal function init():void
        {
            this._settings = new lesta.dialogs.battle_window.markers.settings.MarkersSettings();
            this._configs = this.getConfigResource().markersConfig;
            this._markers = new lesta.dialogs.battle_window.markers.container.AllMarkersContainer(this._configs, this._settings);
            addChild(this._markers);
            lesta.data.GameDelegate.addCallBack("battle.setCursorState", this, this.onSetCursorState);
            lesta.data.GameDelegate.addCallBack("battle.onMouseOverNewEntity", this, this.onMouseOverNewEntity);
            lesta.data.GameDelegate.addCallBack("battle.setIconsConfig", this, this.onSetIconsConfig);
            lesta.data.GameDelegate.addCallBack("battle.setPriorTarget", this, this.onSetPriorTarget);
            lesta.data.GameDelegate.addCallBack("battle.UserDead", this, this.onUserDead);
            lesta.data.GameDelegate.addCallBack("battle.quickCommand", this, this.onQuickCommand);
            lesta.dialogs.battle_window.markers.container.views.ships.ShipMarker.kmPostfix = " " + lesta.cpp.Translator.getLocalization("IDS_KILOMETER");
            this.updateCurrentSqrtHitRadius();
            return;
        }

        public function fini():void
        {
            this._markers.fini();
            lesta.data.GameDelegate.removeCallBack(this);
            lesta.managers.HotKeyManager.instance.clearCallback(lesta.managers.HotKeys.CMD_ICONS, scaleform.clik.constants.InputValue.KEY_DOWN);
            lesta.managers.HotKeyManager.instance.clearCallback(lesta.managers.HotKeys.CMD_ICONS, scaleform.clik.constants.InputValue.KEY_UP);
            return;
        }

        public function update():void
        {
            this._markers.update();
            this.updateIconUnderCursor();
            return;
        }

        public function set isTacticalMap(arg1:Boolean):void
        {
            this._settings.isTacticalMap = arg1;
            this.updateCurrentSqrtHitRadius();
            return;
        }

        public function set aircraftMode(arg1:Boolean):void
        {
            this._settings.isWeaponAircraft = arg1;
            return;
        }

        public function set altVision(arg1:Boolean):void
        {
            this._settings.isAlternativeView = arg1;
            return;
        }

        public function get markers():lesta.dialogs.battle_window.markers.container.AllMarkersContainer
        {
            return this._markers;
        }

        internal function onSetCursorState(arg1:Boolean, arg2:Boolean):void
        {
            this._isCursorActive = arg1;
            this._isCursorOnGui = arg2;
            return;
        }

        internal function onMouseOverNewEntity(arg1:int, arg2:int):void
        {
            this._markers.highlightMarker(arg1, arg2);
            return;
        }

        public function onSetIconsConfig(arg1:Number, arg2:Number, arg3:Array):void
        {
            this._configs.generateConfigs(arg1, arg2, arg3);
            this._settings.near = arg1;
            this._settings.far = arg2;
            this._settings.intermediateDistances = arg3;
            this._settings.intermediateDistancesLength = arg3.length;
            if (this._settings.intermediateDistancesLength) 
            {
                this._settings.intermediateDistancesLast = arg3[(this._settings.intermediateDistancesLength - 1)];
            }
            return;
        }

        public function onSetPriorTarget(arg1:int, arg2:int):void
        {
            this._markers.setAsPriorTarget(arg1, arg2);
            return;
        }

        public function onUserDead():void
        {
            this._markers.hideAllPriorities();
            return;
        }

        protected function onQuickCommand(arg1:lesta.structs.QuickCommandInfo):void
        {
            var loc2:*=null;
            var loc1:*=this.markers.ships.getMarker(arg1.sourcePlayerId);
            if (loc1) 
            {
                loc1.playTargetAnimation(arg1.sourceAnimationName);
            }
            if (arg1.isShipTarget) 
            {
                loc2 = this.markers.ships.getMarker(arg1.targetId);
            }
            if (arg1.isCapturePointTarget) 
            {
                loc2 = this.markers.capturePoints.getMarker(arg1.targetId);
            }
            if (loc2) 
            {
                loc2.playTargetAnimation(arg1.targetAnimationName);
            }
            return;
        }

        internal function getConfigResource():lesta.utils.resourceLoading.PreloadBattleWindowJob
        {
            var loc1:*=lesta.utils.GameInfoHolder.instance.battleWindowInfo.getData().markersResourceId;
            return lesta.utils.resourceLoading.ResourceLoadingDelegator.getJob(loc1) as lesta.utils.resourceLoading.PreloadBattleWindowJob;
        }

        internal function updateCurrentSqrtHitRadius():void
        {
            this._currentSqrtHitRadius = this._settings.isTacticalMap ? ICON_SQRT_HIT_RADIUS_TACTICAL_MAP : ICON_SQRT_HIT_RADIUS;
            return;
        }

        internal function updateIconUnderCursor():void
        {
            var loc4:*=null;
            var loc5:*=0;
            var loc6:*=0;
            var loc7:*=null;
            var loc8:*=NaN;
            if (this._isCursorActive && this._isCursorOnGui || stage == null || this._markers == null) 
            {
                return;
            }
            var loc1:*=this._isCursorActive ? stage.mouseX : stage.stageWidth * 0.5;
            var loc2:*=this._isCursorActive ? stage.mouseY : stage.stageHeight * 0.5;
            var loc3:*=[];
            var loc9:*=0;
            while (loc9 < lesta.dialogs.battle_window.markers.container.AllMarkersContainer.TYPES_LENGTH) 
            {
                loc5 = (loc4 = this._markers.types[loc9]).length;
                loc6 = 0;
                while (loc6 < loc5) 
                {
                    if (!(!(loc7 = loc4.markersArray[loc6]).visible || !loc7.isClickAria || !loc7.info.isControllable)) 
                    {
                        if ((loc8 = (loc7.x - loc1) * (loc7.x - loc1) + (loc7.y - loc2) * (loc7.y - loc2)) <= this._currentSqrtHitRadius) 
                        {
                            loc3.push({"type":loc7.type, "id":loc7.id, "dist":loc8});
                        }
                    }
                    ++loc6;
                }
                ++loc9;
            }
            var loc10:*=-1;
            var loc11:*=-1;
            var loc12:*;
            if ((loc12 = loc3.length) > 0) 
            {
                loc10 = loc3[0][0];
                loc11 = loc3[0][1];
                if (loc12 > 1) 
                {
                    loc3.sortOn("dist", Array.NUMERIC);
                }
            }
            if (loc12 > 1 || !(this._markerTypeUnder == loc10) || !(this._markerIdUnder == loc11)) 
            {
                this._markerTypeUnder = loc10;
                this._markerIdUnder = loc11;
                lesta.data.GameDelegate.call("battle.onChangedIconOver", [loc3]);
            }
            return;
        }

        public static const ICON_SQRT_HIT_RADIUS:int=50 * 50;

        public static const ICON_SQRT_HIT_RADIUS_TACTICAL_MAP:int=20 * 20;

        internal var _isCursorActive:Boolean;

        internal var _isCursorOnGui:Boolean;

        internal var _markerTypeUnder:int=-1;

        internal var _markerIdUnder:int=-1;

        internal var _settings:lesta.dialogs.battle_window.markers.settings.MarkersSettings;

        internal var _configs:lesta.dialogs.battle_window.markers.config.MarkersConfig;

        internal var _markers:lesta.dialogs.battle_window.markers.container.AllMarkersContainer;

        internal var _currentSqrtHitRadius:int=2500;
    }
}
