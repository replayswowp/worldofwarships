package lesta.dialogs.battle_window.markers.container.views.planes 
{
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.constants.*;
    import lesta.controls.*;
    import lesta.cpp.*;
    import lesta.dialogs.battle_window.constants.*;
    import lesta.dialogs.battle_window.markers.container.views.base.*;
    import lesta.dialogs.battle_window.markers.utils.*;
    import lesta.structs.*;
    import lesta.utils.*;
    
    public class PlaneMarker extends lesta.dialogs.battle_window.markers.container.views.base.Marker
    {
        public function PlaneMarker()
        {
            this._appearTimer = new flash.utils.Timer(APPEAR_DELAY, 1);
            this._disappearTimer = new flash.utils.Timer(0, 1);
            super();
            isClickAria = true;
            this.distanceItem = (this.distance as flash.display.MovieClip).distance;
            this.iconAsAnimated = icon as lesta.controls.AnimatedStateClip;
            this.hpBarItem = (this.hpBar as flash.display.MovieClip).hpBar;
            this.orderIndicatorItem = this.orderIndicator.orderIndicator;
            _type = lesta.dialogs.battle_window.markers.utils.MarkerTypes.PLANE;
            this.hotkey.toggle = true;
            this._appearTimer.addEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.onAppearTimerComplete);
            this._disappearTimer.addEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.onDisappearTimerComplete);
            this.radioIndicator.radioIndicator.gotoAndStop(1);
            icon.attackAnim.stop();
            this.selectionAnimation.hide();
            this.distance.setState("inactive", false, false);
            this.iconAsAnimated.setState("inactive", false, false);
            this.orderIndicator.setState("inactive", false, false);
            return;
        }

        internal function updateSelection():void
        {
            if (_isSelected == this._plane.isSelected) 
            {
                return;
            }
            _isSelected = this._plane.isSelected;
            if (_isSelected) 
            {
                this.selectionAnimation.show();
            }
            this.hotkey.toggled = _isSelected;
            var loc1:*;
            this.radioIndicator.alpha = loc1 = _isSelected ? 1 : 0.5;
            this.orderIndicator.alpha = loc1;
            icon.attackAnim.selectedState.normalRelation.visible = !_isSelected;
            icon.attackAnim.selectedState.selectedRelation.visible = _isSelected;
            return;
        }

        public override function init():void
        {
            super.init();
            if (_settings.forceVisible) 
            {
                this.radioIndicator.radioIndicator.gotoAndStop(1);
                this.landing.gotoAndStop(10);
                this.launching.gotoAndStop(10);
            }
            return;
        }

        public override function fini():void
        {
            this._appearTimer.removeEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.onAppearTimerComplete);
            this._disappearTimer.removeEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.onDisappearTimerComplete);
            this._appearTimer.stop();
            this._disappearTimer.stop();
            return;
        }

        public override function set info(arg1:lesta.structs.EntityInfo):void
        {
            super.info = arg1;
            this._plane = arg1 as lesta.structs.PlaneInfo;
            this._isSelf = this._plane.relation == lesta.constants.PlayerRelation.SELF;
            this._isEnemy = this._plane.relation == lesta.constants.PlayerRelation.ENEMY;
            this._isOwnBomber = this._isSelf && (this._plane.typeIdent == lesta.constants.PlaneTypes.DIVE_BOMBER || this._plane.typeIdent == lesta.constants.PlaneTypes.TORPEDO_BOMBER);
            this._relationFrame = this._plane.relation + 1;
            icon.attackAnim.selectedState.normalRelation.visible = !_isSelected;
            icon.attackAnim.selectedState.selectedRelation.visible = _isSelected;
            var loc1:*=this.selectionAnimation.getChildByName("iconContainer") as flash.display.MovieClip;
            if (loc1) 
            {
                loc1.gotoAndStop(this._plane.typeId + 1);
                if (loc1.ammoClip) 
                {
                    loc1.ammoClip.gotoAndStop(1);
                }
            }
            this.updateTKStatus();
            this.distanceItem.gotoAndStop(this._relationFrame);
            this.playerName.gotoAndStop(this._relationFrame);
            this.planeTypeName.gotoAndStop(this._relationFrame);
            this.planeModel.gotoAndStop(this._relationFrame);
            this.planesNumber.gotoAndStop(this._relationFrame);
            this.hpBarItem.value.gotoAndStop(this._relationFrame);
            this.hpBarItem.bg.gotoAndStop(this._relationFrame);
            this.hpBarItem.setMaxCount(this._plane.countPlanes);
            getElelentByName("distance").updateTextFieldRef();
            getElelentByName("playerName").updateTextFieldRef();
            getElelentByName("planeTypeName").updateTextFieldRef();
            getElelentByName("planeModel").updateTextFieldRef();
            getElelentByName("planesNumber").updateTextFieldRef();
            this.playerName.label.text = this._plane.playerName;
            this.planeTypeName.label.text = this._plane.typeIdent;
            this.planeModel.label.text = this._plane.modelName;
            this.hotkey.commandId = this._plane.commandId;
            this.hotkey.visible = !this._plane.isConsumable;
            this.distance.visible = !this._plane.isConsumable;
            return;
        }

        public function showRadioFeedback(arg1:Number, arg2:String):void
        {
            this._duration = arg1;
            this.radioIndicator.gotoAndStop(arg2);
            this._appearTimer.reset();
            this._appearTimer.start();
            return;
        }

        public function showUnderAttackFeedback(arg1:Boolean):void
        {
            if (this._underAttack == arg1) 
            {
                return;
            }
            this._underAttack = arg1;
            if (this._underAttack) 
            {
                icon.attackAnim.play();
            }
            else 
            {
                icon.attackAnim.gotoAndStop(1);
            }
            return;
        }

        public function updateTKStatus():void
        {
            var loc1:*=lesta.utils.GameInfoHolder.instance.mapPlayers[this._plane.ownerId];
            var loc2:*=loc1.getUnionText();
            icon.attackAnim.selectedState.normalRelation.gotoAndStop(loc2);
            icon.attackAnim.selectedState.selectedRelation.gotoAndStop(loc2);
            var loc3:*=(this._plane.isConsumable ? "Consumable" : "") + this._plane.typeIdent;
            icon.attackAnim.selectedState.normalRelation.type.gotoAndStop(loc3);
            icon.attackAnim.selectedState.selectedRelation.type.gotoAndStop(loc3);
            this.ammoClipSelected = icon.attackAnim.selectedState.normalRelation.type.ammoClip;
            this.ammoClipNormal = icon.attackAnim.selectedState.selectedRelation.type.ammoClip;
            this.updateBombers(true);
            return;
        }

        internal function onAppearTimerComplete(arg1:flash.events.TimerEvent):void
        {
            this.radioIndicator.radioIndicator.gotoAndPlay("appear");
            this._disappearTimer.reset();
            this._disappearTimer.delay = this._duration * MILLISECONDS_IN_SECOND;
            this._disappearTimer.start();
            return;
        }

        internal function onDisappearTimerComplete(arg1:flash.events.TimerEvent):void
        {
            this.radioIndicator.radioIndicator.gotoAndPlay("disappear");
            return;
        }

        protected override function getVisible():Boolean
        {
            return this._plane.isActive && this._plane.isVisible;
        }

        protected override function localUpdate():void
        {
            this.updateInactive();
            this.updateSelection();
            this.updatePlaneState();
            this.updateDirection();
            this.updatePlanesCount();
            this.updateDistance();
            this.updateBombers();
            this.updateOrder();
            this.updateAmmo();
            this.updateAltVision();
            return;
        }

        internal function updatePlanesCount():void
        {
            if (this._plane.currentCountPlanes == this._currentCountPlanes) 
            {
                return;
            }
            this._currentCountPlanes = this._plane.currentCountPlanes;
            var loc1:*="";
            if (this._isSelf) 
            {
                loc1 = "/" + this._plane.countPlanes;
            }
            var loc2:*=this._currentCountPlanes + loc1;
            this.planesNumber.label.text = loc2;
            this.hpBarItem.setCurrentCount(this._currentCountPlanes);
            return;
        }

        internal function updateDistance():void
        {
            if (this._distanceToShip == this._plane.distanceToShip) 
            {
                return;
            }
            this._distanceToShip = this._plane.distanceToShip;
            var loc1:*=this._plane.distanceToShip.toFixed(1) + " " + lesta.cpp.Translator.getLocalization("IDS_KILOMETER");
            this.distanceItem.label.text = loc1;
            return;
        }

        internal function updateBombers(arg1:Boolean=false):void
        {
            if (!this._isOwnBomber || this._hasBomb == this._plane.hasBomb && !arg1) 
            {
                return;
            }
            this._hasBomb = this._plane.hasBomb;
            var loc1:*=this._plane.hasBomb ? 2 : 1;
            icon.attackAnim.selectedState.normalRelation.type.suspention.gotoAndStop(loc1);
            icon.attackAnim.selectedState.selectedRelation.type.suspention.gotoAndStop(loc1);
            return;
        }

        internal function updateOrder():void
        {
            if (this._order == this._plane.order) 
            {
                return;
            }
            this.orderIndicatorItem.gotoAndStop(1);
            this.orderIndicatorItem.order_in.gotoAndStop(this._plane.order);
            this.orderIndicatorItem.order_out.gotoAndStop(this._order);
            this.orderIndicatorItem.play();
            this._order = this._plane.order;
            return;
        }

        internal function updatePlaneState():void
        {
            if (this._state == this._plane.state) 
            {
                return;
            }
            this._state = this._plane.state;
            var loc1:*=this._plane.state >= lesta.dialogs.battle_window.constants.SquadronStates.LAUNCHING;
            var loc2:*=this._plane.state == lesta.dialogs.battle_window.constants.SquadronStates.FLY || this._plane.state == lesta.dialogs.battle_window.constants.SquadronStates.IN_LAND_QUEUE;
            var loc3:*=this._plane.state == lesta.dialogs.battle_window.constants.SquadronStates.LAUNCHING || this._plane.state == lesta.dialogs.battle_window.constants.SquadronStates.LANDING;
            this.launching.visible = this._plane.state == lesta.dialogs.battle_window.constants.SquadronStates.LAUNCHING || _settings.forceVisible;
            this.landing.visible = this._plane.state == lesta.dialogs.battle_window.constants.SquadronStates.LANDING || _settings.forceVisible;
            icon.direction.visible = !loc3;
            this.orderIndicator.visible = !loc3 && this._isSelf && !this._plane.isConsumable || _settings.forceVisible;
            var loc4:*=this._plane.typeIdent == lesta.constants.PlaneTypes.SCOUNT;
            this.hpBar.visible = this._isSelf && loc1 && !loc3 && !loc4 || !this._isSelf && !loc4 || _settings.forceVisible;
            return;
        }

        internal function updateInactive():void
        {
            var loc1:*=(this._plane.state == lesta.dialogs.battle_window.constants.SquadronStates.LANDING || this._plane.state == lesta.dialogs.battle_window.constants.SquadronStates.LAUNCHING) && this._isSelf;
            if (this._isInactive == loc1 && !_settings.forceVisible) 
            {
                return;
            }
            this._isInactive = loc1;
            var loc2:*=this._isInactive && !_settings.forceVisible ? "inactive" : "normal";
            this.distance.setState(loc2, false, false);
            this.iconAsAnimated.setState(loc2, false, false);
            this.orderIndicator.setState(loc2, false, false);
            return;
        }

        internal function updateDirection():void
        {
            var loc1:*=lesta.utils.GameInfoHolder.instance.cameraInfo;
            icon.direction.rotation = (this._plane.worldYaw - loc1.yaw) * 180 / Math.PI;
            return;
        }

        internal function updateAltVision():void
        {
            if (this._isAltVision == _settings.isAlternativeView) 
            {
                return;
            }
            this._isAltVision = _settings.isAlternativeView;
            return;
        }

        internal function updateAmmo():void
        {
            var loc1:*=0;
            if (this.ammoClipNormal) 
            {
                loc1 = this.ammoClipNormal.totalFrames - Math.ceil((this.ammoClipNormal.totalFrames - 1) * this._plane.ammo / this._plane.maxAmmo);
                if (this.ammoClipNormal.currentFrame != loc1) 
                {
                    this.ammoClipNormal.gotoAndStop(loc1);
                }
                if (this.ammoClipSelected && !(this.ammoClipSelected.currentFrame == loc1)) 
                {
                    this.ammoClipSelected.gotoAndStop(loc1);
                }
            }
            return;
        }

        internal static const APPEAR_DELAY:int=200;

        internal static const MILLISECONDS_IN_SECOND:int=1000;

        internal var _hasBomb:Boolean;

        internal var _order:String="None";

        internal var _relationFrame:int=-1;

        internal var _state:int=-1;

        internal var _isInactive:Boolean=true;

        internal var _tkStatus:Boolean=false;

        public var planesNumber:flash.display.MovieClip;

        internal var _currentCountPlanes:int;

        internal var _underAttack:Boolean;

        internal var _isAltVision:Boolean=false;

        internal var _duration:Number;

        internal var _appearTimer:flash.utils.Timer;

        internal var _disappearTimer:flash.utils.Timer;

        internal var _distanceToShip:Number;

        public var iconAsAnimated:lesta.controls.AnimatedStateClip;

        public var hotkey:lesta.controls.HotKeyContainer;

        public var distance:lesta.controls.AnimatedStateClip;

        public var playerName:flash.display.MovieClip;

        public var planeTypeName:flash.display.MovieClip;

        public var planeModel:flash.display.MovieClip;

        public var radioIndicator:flash.display.MovieClip;

        public var landing:flash.display.MovieClip;

        public var launching:flash.display.MovieClip;

        public var selectionAnimation:lesta.controls.AnimationEffect;

        public var hpBar:lesta.controls.AnimatedStateClip;

        public var hpBarItem:lesta.dialogs.battle_window.markers.container.views.planes.PlaneHealthBar;

        public var orderIndicator:flash.display.MovieClip;

        internal var _isOwnBomber:Boolean;

        public var orderIndicatorItem:flash.display.MovieClip;

        public var distanceItem:flash.display.MovieClip;

        public var ammoClipSelected:flash.display.MovieClip;

        public var ammoClipNormal:flash.display.MovieClip;

        internal var _plane:lesta.structs.PlaneInfo;

        internal var _isSelf:Boolean;

        internal var _isEnemy:Boolean;
    }
}
