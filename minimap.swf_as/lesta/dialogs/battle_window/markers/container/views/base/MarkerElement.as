package lesta.dialogs.battle_window.markers.container.views.base 
{
    import flash.display.*;
    import flash.text.*;
    import lesta.managers.*;
    
    public class MarkerElement extends flash.display.MovieClip
    {
        public function MarkerElement(arg1:flash.display.DisplayObject)
        {
            this.startProperties = new Object();
            this.finishProperties = new Object();
            super();
            this.displayObject = arg1;
            this.displayObject.x = 0;
            this.displayObject.y = 0;
            this.updateTextFieldRef();
            return;
        }

        public function updateTextFieldRef():void
        {
            this._textField = this.getTextField();
            if (this._textField) 
            {
                this._textFormat = this._textField.getTextFormat();
                this._textField.autoSize = this._textFormat.align;
                this._textFormat.color = this._color;
                this._textField.setTextFormat(this._textFormat);
            }
            else 
            {
                this._textFormat = null;
            }
            return;
        }

        public function setFontColor(arg1:uint):void
        {
            if (this._color == arg1) 
            {
                return;
            }
            this._color = arg1;
            if (this._textFormat) 
            {
                this._textFormat.color = arg1;
                this._textField.setTextFormat(this._textFormat);
            }
            return;
        }

        public function startTween(arg1:Number):void
        {
            lesta.managers.TweenManager.addTweenExplicitly(this.displayObject, arg1, this.startProperties, this.finishProperties);
            return;
        }

        internal function getTextField():flash.text.TextField
        {
            var loc4:*=null;
            if (this.displayObject is flash.text.TextField) 
            {
                return this.displayObject as flash.text.TextField;
            }
            var loc1:*=this.displayObject as flash.display.DisplayObjectContainer;
            if (loc1 == null) 
            {
                return null;
            }
            var loc2:*=loc1.numChildren;
            if (!loc2) 
            {
                return null;
            }
            var loc3:*=0;
            while (loc3 < loc2) 
            {
                if ((loc4 = loc1.getChildAt(loc3)) is flash.text.TextField && loc4.name == "label") 
                {
                    return loc4 as flash.text.TextField;
                }
                ++loc3;
            }
            return null;
        }

        public var displayObject:flash.display.DisplayObject;

        internal var _textField:flash.text.TextField=null;

        internal var _textFormat:flash.text.TextFormat=null;

        internal var _color:uint=0;

        public var startProperties:Object;

        public var finishProperties:Object;
    }
}
