package lesta.dialogs.battle_window.markers.container.views.base 
{
    import flash.display.*;
    import lesta.dialogs.battle_window.*;
    import lesta.dialogs.battle_window.markers.config.*;
    import lesta.dialogs.battle_window.markers.settings.*;
    import lesta.structs.*;
    
    public class SimpleMarker extends flash.display.MovieClip
    {
        public function SimpleMarker()
        {
            super();
            return;
        }

        public function init():void
        {
            return;
        }

        public function fini():void
        {
            this._settings = null;
            this._configs = null;
            this._info = null;
            return;
        }

        public function get type():Number
        {
            return this._type;
        }

        public function get id():int
        {
            return this._info.id;
        }

        public function set configs(arg1:lesta.dialogs.battle_window.markers.config.MarkersConfig):void
        {
            this._configs = arg1;
            return;
        }

        public function get hidden():Boolean
        {
            return this._hidden;
        }

        public function update():void
        {
            this.distanceToCamera = this._info.distanceToCamera;
            this.updateHidden();
            visible = this._info.isOnScreen && !this._hidden;
            if (!visible) 
            {
                return;
            }
            x = Math.round(this._info.screenPos.x);
            y = Math.round(this._info.screenPos.y);
            this.localUpdate();
            return;
        }

        public function set info(arg1:lesta.structs.EntityInfo):void
        {
            this._info = arg1;
            return;
        }

        public function get info():lesta.structs.EntityInfo
        {
            return this._info;
        }

        public function set settings(arg1:lesta.dialogs.battle_window.markers.settings.MarkersSettings):void
        {
            this._settings = arg1;
            return;
        }

        protected function getVisible():Boolean
        {
            return true;
        }

        protected function localUpdate():void
        {
            return;
        }

        internal function updateHidden():void
        {
            var loc1:*=!this._settings.forceVisible && !this.getVisible();
            if (this._hidden == loc1) 
            {
                return;
            }
            this._hidden = loc1;
            return;
        }

        protected function initTargetDelegator():void
        {
            return;
        }

        public function playTargetAnimation(arg1:String):void
        {
            this._targetAnimation.playTarget(arg1);
            return;
        }

        public function clearTargetAnimation():void
        {
            this._targetAnimation.clear();
            return;
        }

        protected var _settings:lesta.dialogs.battle_window.markers.settings.MarkersSettings;

        protected var _configs:lesta.dialogs.battle_window.markers.config.MarkersConfig;

        protected var _info:lesta.structs.EntityInfo;

        protected var _type:int;

        protected var _hidden:Boolean=true;

        public var distanceToCamera:Number=0;

        public var forDelete:Boolean=false;

        public var isClickAria:Boolean=false;

        protected var _targetAnimation:lesta.dialogs.battle_window.TargetAnimationController;
    }
}
