package lesta.dialogs.battle_window.markers.container 
{
    import lesta.dialogs.battle_window.markers.config.*;
    import lesta.dialogs.battle_window.markers.container.views.base.*;
    import lesta.dialogs.battle_window.markers.settings.*;
    import lesta.structs.*;
    
    public class TemporaryMarkersContainer extends lesta.dialogs.battle_window.markers.container.MarkersContainer
    {
        public function TemporaryMarkersContainer(arg1:int, arg2:lesta.dialogs.battle_window.markers.config.MarkersConfig, arg3:lesta.dialogs.battle_window.markers.settings.MarkersSettings)
        {
            super(arg1, arg2, arg3);
            return;
        }

        public override function update():void
        {
            this.createMarkersAndSetParameters();
            this.removeDeletedMarkers();
            if (_markersLinkedList != null) 
            {
                updateMarkers(_markersLinkedList);
                sort();
            }
            return;
        }

        protected override function createMarkersAndSetParameters():void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            if (_markersLinkedList != null) 
            {
                loc3 = _markersLinkedList;
                do 
                {
                    loc3.value.forDelete = true;
                    var loc6:*;
                    loc3 = loc6 = loc3.next;
                }
                while (loc6);
            }
            var loc1:*=_dataProvider.length;
            var loc2:*=0;
            while (loc2 < loc1) 
            {
                if ((loc4 = _dataProvider[loc2]).id != -1) 
                {
                    if (!(loc5 = _markersObject[loc4.id])) 
                    {
                        loc5 = this.createMarker(loc4);
                    }
                    loc5.forDelete = false;
                }
                ++loc2;
            }
            return;
        }

        protected function removeDeletedMarkers():void
        {
            var loc2:*=null;
            if (_markersLinkedList == null) 
            {
                return;
            }
            var loc1:*=_markersLinkedList;
            _markersLinkedList = null;
            do 
            {
                if (loc1.value.forDelete) 
                {
                    this.removeMarker(loc1.value);
                    if (loc1.prev) 
                    {
                        loc1.prev.next = null;
                    }
                }
                else 
                {
                    if (loc2 != null) 
                    {
                        loc2.next = loc1;
                    }
                    else 
                    {
                        _markersLinkedList = loc1;
                    }
                    loc1.prev = loc2;
                    loc2 = loc1;
                }
                var loc3:*;
                loc1 = loc3 = loc1.next;
            }
            while (loc3);
            return;
        }

        protected override function createMarker(arg1:lesta.structs.EntityInfo):lesta.dialogs.battle_window.markers.container.views.base.SimpleMarker
        {
            var loc1:*=_markersLinkedList;
            var loc2:*=super.createMarker(arg1);
            if (loc1) 
            {
                loc1.prev = _markersLinkedList;
            }
            return loc2;
        }

        protected function removeMarker(arg1:lesta.dialogs.battle_window.markers.container.views.base.SimpleMarker):void
        {
            markersArray.splice(markersArray.indexOf(arg1), 1);
            delete _markersObject[arg1.id];
            arg1.fini();
            removeChild(arg1);
            var loc1:*;
            length--;
            return;
        }
    }
}
