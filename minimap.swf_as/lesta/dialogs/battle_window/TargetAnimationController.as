package lesta.dialogs.battle_window 
{
    import flash.display.*;
    import flash.utils.*;
    import lesta.controls.*;
    
    public class TargetAnimationController extends Object
    {
        public function TargetAnimationController(arg1:flash.display.DisplayObjectContainer, arg2:String)
        {
            super();
            this._factory = new IconFactory();
            var loc1:*=flash.utils.getDefinitionByName(arg2) as Class;
            this.indicator = new loc1() as lesta.controls.AnimatedStateClip;
            arg1.addChild(this.indicator);
            this.clipContainer = this.indicator["container"];
            return;
        }

        public function playTarget(arg1:String):void
        {
            this.indicator.setState("show");
            var loc1:*=this._factory.getIcon(arg1);
            trace("-->>", "playTarget", "icon:", loc1);
            this.clearClip();
            this.clipContainer.addChild(loc1);
            this.clearTimeOut();
            this._timeoutId = flash.utils.setTimeout(this.hideTarget, TIMEOUT_ATTACK_TARGET);
            return;
        }

        public function clear():void
        {
            this.clearTimeOut();
            this.hideTarget();
            return;
        }

        public function hideTarget():void
        {
            this.indicator.setState("inv");
            return;
        }

        internal function clearTimeOut():void
        {
            if (this._timeoutId > -1) 
            {
                flash.utils.clearTimeout(this._timeoutId);
            }
            this._timeoutId = -1;
            return;
        }

        internal function clearClip():void
        {
            while (this.clipContainer.numChildren) 
            {
                this.clipContainer.removeChildAt(0);
            }
            return;
        }

        internal static const TIMEOUT_ATTACK_TARGET:Number=3000;

        internal var indicator:lesta.controls.AnimatedStateClip;

        internal var clipContainer:flash.display.DisplayObjectContainer;

        internal var _timeoutId:int=-1;

        internal var _factory:IconFactory;
    }
}

import flash.display.*;
import flash.utils.*;


class IconFactory extends Object
{
    public function IconFactory()
    {
        this.classes = new flash.utils.Dictionary();
        super();
        return;
    }

    public function getIcon(arg1:String):flash.display.MovieClip
    {
        trace("-->>", "getIcon", "name:", arg1);
        if (!this.classes[arg1]) 
        {
            this.classes[arg1] = flash.utils.getDefinitionByName(arg1);
        }
        var loc1:*=this.classes[arg1];
        return new loc1();
    }

    internal var classes:flash.utils.Dictionary;
}