package lesta.dialogs.battle_window 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import lesta.utils.*;
    
    public class CacheDottedLineSprite extends flash.display.Sprite
    {
        public function CacheDottedLineSprite()
        {
            this.listSprites = new Vector.<flash.display.Sprite>();
            this.endSprite = new flash.display.Sprite();
            super();
            addChild(this.endSprite);
            return;
        }

        public function drawDottedLine(arg1:int, arg2:Number, arg3:Number, arg4:Number, arg5:Number):void
        {
            var loc6:*=null;
            var loc1:*;
            var loc2:*=(loc1 = arg1 / SPRITE_LENGTH) * SPRITE_LENGTH;
            var loc3:*;
            var loc4:*=loc3 = this.listSprites.length;
            while (loc4 < loc1) 
            {
                loc6 = new flash.display.Sprite();
                this.listSprites.push(loc6);
                addChild(loc6);
                lesta.utils.Drawing.drawHorDottedLine(loc6, SPRITE_LENGTH, arg3, arg4, arg5);
                loc6.x = loc4 * SPRITE_LENGTH;
                loc6.y = 0;
                ++loc3;
                ++loc4;
            }
            this.endSprite.x = loc2;
            this.endSprite.graphics.clear();
            lesta.utils.Drawing.drawHorDottedLine(this.endSprite, arg1 - loc2, arg3, arg4, arg5);
            loc4 = 0;
            var loc5:*=this.listSprites.length;
            while (loc4 < loc5) 
            {
                this.listSprites[loc4].visible = loc4 < loc1;
                ++loc4;
            }
            this.lastVisibleSprites = loc1;
            return;
        }

        public static const SPRITE_LENGTH:int=200;

        internal var listSprites:__AS3__.vec.Vector.<flash.display.Sprite>;

        internal var endSprite:flash.display.Sprite;

        internal var lastVisibleSprites:int=0;
    }
}
