package lesta.dialogs.battle_window.constants 
{
    import flash.utils.*;
    
    public class SquadronStates extends Object
    {
        public function SquadronStates()
        {
            super();
            return;
        }

        
        {
            MAP_IDENTS = new flash.utils.Dictionary();
            MAP_IDENTS[DISBANDED] = "disbanded";
            MAP_IDENTS[PREPARING] = "preparing";
            MAP_IDENTS[READY] = "ready";
            MAP_IDENTS[IN_LAUNCH_QUEUE] = "in_launch_queue";
            MAP_IDENTS[LAUNCHING] = "launching";
            MAP_IDENTS[FLY] = "fly";
            MAP_IDENTS[IN_LAND_QUEUE] = "in_land_queue";
            MAP_IDENTS[LANDING] = "landing";
        }

        public static const DISBANDED:int=0;

        public static const PREPARING:int=1;

        public static const READY:int=2;

        public static const IN_LAUNCH_QUEUE:int=3;

        public static const LAUNCHING:int=4;

        public static const FLY:int=5;

        public static const IN_LAND_QUEUE:int=6;

        public static const LANDING:int=7;

        public static var MAP_IDENTS:flash.utils.Dictionary;
    }
}
