package lesta.dialogs.battle_window.constants 
{
    public class NavpointTypes extends Object
    {
        public function NavpointTypes()
        {
            super();
            return;
        }

        public static const THIS_SHIP:int=-3;

        public static const FAKE:int=-2;

        public static const THIS_PLANE:int=-1;

        public static const LAUNCH:int=0;

        public static const MOVE_TO:int=1;

        public static const ATTACK:int=2;

        public static const FOLLOW:int=3;

        public static const LAND:int=4;

        public static const PATROL:int=5;

        public static const RECON:int=6;

        public static const ATTACK_RADIUS:int=8;

        public static const ATTACK_GROUND:int=9;
    }
}
