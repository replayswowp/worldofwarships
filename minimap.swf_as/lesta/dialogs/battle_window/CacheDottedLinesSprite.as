package lesta.dialogs.battle_window 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.geom.*;
    import lesta.dialogs.battle_window.markers.container.views.planes.*;
    import lesta.structs.*;
    
    public class CacheDottedLinesSprite extends flash.display.Sprite
    {
        public function CacheDottedLinesSprite()
        {
            this.listLines = new Vector.<lesta.dialogs.battle_window.CacheDottedLineSprite>();
            super();
            this.setLinesCount(2);
            return;
        }

        public function setLinesCount(arg1:int):void
        {
            var loc1:*=null;
            var loc2:*=this.listLines.length;
            while (loc2 < arg1) 
            {
                loc1 = new lesta.dialogs.battle_window.CacheDottedLineSprite();
                this.listLines.push(loc1);
                addChild(loc1);
                ++loc2;
            }
            loc2 = arg1;
            while (loc2 < this.listLines.length) 
            {
                this.listLines[loc2].visible = false;
                ++loc2;
            }
            return;
        }

        public function setLineParams(arg1:int, arg2:uint, arg3:Number):void
        {
            var loc2:*=null;
            var loc1:*;
            (loc1 = this.listLines[arg1]).alpha = arg3;
            if (loc1.transform.colorTransform.color != arg2) 
            {
                (loc2 = loc1.transform.colorTransform).color = arg2;
                loc1.transform.colorTransform = loc2;
            }
            return;
        }

        public function drawDottedLine(arg1:int, arg2:int, arg3:int, arg4:int, arg5:int, arg6:uint, arg7:Number, arg8:Number, arg9:Number, arg10:Number):void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=NaN;
            if (arg1 < this.listLines.length) 
            {
                loc1 = this.listLines[arg1];
                loc2 = Math.sqrt((arg4 - arg2) * (arg4 - arg2) + (arg5 - arg3) * (arg5 - arg3));
                loc3 = Math.atan2(arg5 - arg3, arg4 - arg2);
                loc1.rotation = loc3 * RAD_TO_DIG;
                loc1.x = arg2;
                loc1.y = arg3;
                loc1.drawDottedLine(loc2, loc3, arg8, arg9, arg10);
                loc1.visible = true;
            }
            return;
        }

        public function setLineInvisible(arg1:int):void
        {
            var loc1:*=this.listLines[arg1];
            loc1.visible = false;
            return;
        }

        public static const RAD_TO_DIG:Number=180 / Math.PI;

        public var navpointList:lesta.structs.NavpointList=null;

        internal var listLines:__AS3__.vec.Vector.<lesta.dialogs.battle_window.CacheDottedLineSprite>;

        internal var countLines:int=0;

        internal var newCountLines:int=0;

        public var planeIcon:lesta.dialogs.battle_window.markers.container.views.planes.PlaneMarker=null;

        public var planeInfo:lesta.structs.PlaneInfo=null;
    }
}
