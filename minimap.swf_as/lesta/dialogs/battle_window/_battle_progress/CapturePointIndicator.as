package lesta.dialogs.battle_window._battle_progress 
{
    import flash.display.*;
    import lesta.constants.*;
    import lesta.controls.*;
    import lesta.structs.*;
    
    public class CapturePointIndicator extends lesta.controls.AnimatedStateClip
    {
        public function CapturePointIndicator()
        {
            super();
            if (this.size) 
            {
                this.size.visible = false;
            }
            return;
        }

        public function initialize(arg1:int):void
        {
            this.pointId = arg1;
            this.letters.letters.gotoAndStop(arg1 + 1);
            this.captureIndicator = this.indicators.getChildByName("captureIndicator") as lesta.dialogs.battle_window._battle_progress.ColoredIndicator;
            this.lockIndicator = this.indicators.getChildByName("lockIndicator") as lesta.dialogs.battle_window._battle_progress.ColoredIndicator;
            this.captureIndicator.maximum = PROGRESSBAR_MAX_VALUE;
            this.lockIndicator.maximum = PROGRESSBAR_MAX_VALUE;
            this.lockIndicator.visible = false;
            return;
        }

        public function update(arg1:lesta.structs.CapturePointInfo):void
        {
            visible = arg1.active;
            if (!visible) 
            {
                return;
            }
            this.captureIndicator.backgroundColor = arg1.ownerRelation + 1;
            this.indicatorsFX.gotoAndStop(arg1.ownerRelation + 1);
            this.captureIndicator.barColor = arg1.invaderRelation + 1;
            var loc1:*=arg1.ownerRelation != lesta.constants.PlayerRelation.NEUTRAL ? this.pointId + 1 + arg1.ownerRelation * LETTERS_AMOUNT : this.pointId + 1;
            this.letters.letters.gotoAndStop(loc1);
            this.lettersFX.letters.gotoAndStop(loc1);
            this.indicatorLocked.gotoAndStop(arg1.ownerRelation);
            this.captureIndicator.value = arg1.captureProgress * PROGRESSBAR_MAX_VALUE;
            this.lockIndicator.visible = arg1.lockProgress > 0 && arg1.lockProgress < 1;
            if (arg1.invaderRelation == lesta.constants.PlayerRelation.ENEMY && this.captureIndicator.maskClip.scaleX > 0 || arg1.invaderRelation == lesta.constants.PlayerRelation.FRIEND && this.captureIndicator.maskClip.scaleX < 0) 
            {
                this.captureIndicator.maskClip.scaleX = -this.captureIndicator.maskClip.scaleX;
            }
            if (this.lockIndicator.visible) 
            {
                this.lockIndicator.value = arg1.lockProgress * PROGRESSBAR_MAX_VALUE;
            }
            if (arg1.isLocked) 
            {
                setState("locked");
            }
            else if (this.lockIndicator.visible) 
            {
                setState("locking");
            }
            else if (arg1.captureProgress > 0 && arg1.captureProgress < 1) 
            {
                setState("capture");
            }
            else if (arg1.captureProgress != 0) 
            {
                setState("normal");
            }
            else 
            {
                setState("captured");
            }
            return;
        }

        internal static const PROGRESSBAR_MAX_VALUE:int=100;

        internal static const LETTERS_AMOUNT:int=10;

        public var lettersFX:flash.display.MovieClip;

        public var letters:flash.display.MovieClip;

        public var indicators:flash.display.MovieClip;

        public var indicatorsFX:flash.display.MovieClip;

        public var indicatorLocked:flash.display.MovieClip;

        public var size:flash.display.Sprite;

        internal var captureIndicator:lesta.dialogs.battle_window._battle_progress.ColoredIndicator;

        internal var lockIndicator:lesta.dialogs.battle_window._battle_progress.ColoredIndicator;

        internal var pointId:int;

        public var alertIndicatorContainer:flash.display.MovieClip;
    }
}
