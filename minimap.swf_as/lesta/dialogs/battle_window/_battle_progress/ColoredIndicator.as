package lesta.dialogs.battle_window._battle_progress 
{
    import flash.display.*;
    import scaleform.clik.constants.*;
    import scaleform.clik.controls.*;
    
    public class ColoredIndicator extends scaleform.clik.controls.StatusIndicator
    {
        public function ColoredIndicator()
        {
            super();
            this.bar.stop();
            this.bar.mask = this.maskClip;
            this.bar.cacheAsBitmap = true;
            if (this.background) 
            {
                this.background.stop();
            }
            if (this.size) 
            {
                this.size.visible = false;
            }
            return;
        }

        public function set barColor(arg1:int):void
        {
            if (!(this._barColor == arg1) && arg1 > 0) 
            {
                this._barColor = arg1;
                invalidateState();
            }
            return;
        }

        public function get barColor():int
        {
            return this._barColor;
        }

        public function set backgroundColor(arg1:int):void
        {
            if (!(this._backgroundColor == arg1) && arg1 > 0) 
            {
                this._backgroundColor = arg1;
                invalidateState();
            }
            return;
        }

        public function get backgroundColor():int
        {
            return this._backgroundColor;
        }

        protected override function draw():void
        {
            super.draw();
            if (isInvalid(scaleform.clik.constants.InvalidationType.STATE)) 
            {
                this.updateColors();
            }
            return;
        }

        protected override function updatePosition():void
        {
            if (!enabled) 
            {
                return;
            }
            var loc1:*=(_value - _minimum) / (_maximum - _minimum);
            var loc2:*=Math.max(1, Math.round(loc1 * this.maskClip.totalFrames));
            this.bar.visible = loc2 > 1;
            this.maskClip.gotoAndStop(loc2);
            return;
        }

        protected function updateColors():void
        {
            if (!enabled) 
            {
                return;
            }
            this.bar.gotoAndStop(this._barColor);
            if (this.background) 
            {
                this.background.gotoAndStop(this._backgroundColor);
            }
            return;
        }

        public var maskClip:flash.display.MovieClip;

        public var bar:flash.display.MovieClip;

        public var background:flash.display.MovieClip;

        public var size:flash.display.Sprite;

        internal var _barColor:int=0;

        internal var _backgroundColor:int=0;
    }
}
