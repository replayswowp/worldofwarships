package lesta.dialogs.battle_window._minimap.constants 
{
    public class MapItemTypes extends Object
    {
        public function MapItemTypes()
        {
            super();
            return;
        }

        
        {
            COUNT = 0;
            var loc1:*;
        }

        public static const SHIP_ITEM:int=COUNT++;

        public static const SQUADRON_ITEM:int=COUNT++;

        public static const NAVPOINTS_ITEM:int=COUNT++;

        public static const CAPTURE_POINT_ITEM:int=COUNT++;

        public static const OBSERVE_VECTOR_ITEM:int=COUNT++;

        public static const SHOOT_RANGE_ITEM:int=COUNT++;

        public static const VISIBILITY_RANGE_ITEM:int=COUNT++;

        public static var COUNT:int=0;
    }
}
