package lesta.dialogs.battle_window._minimap.constants 
{
    public class MapItemGroups extends Object
    {
        public function MapItemGroups()
        {
            super();
            return;
        }

        public static const COUNT:int=5;

        public static const DEFAULT:int=0;

        public static const SQUADRON:int=1;

        public static const DEAD:int=2;

        public static const OWN_SQUADRON:int=3;

        public static const RANGES:int=4;
    }
}
