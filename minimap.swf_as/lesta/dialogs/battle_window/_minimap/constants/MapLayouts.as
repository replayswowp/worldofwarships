package lesta.dialogs.battle_window._minimap.constants 
{
    public class MapLayouts extends Object
    {
        public function MapLayouts()
        {
            super();
            return;
        }

        
        {
            COUNT = 0;
            var loc1:*;
        }

        public static const NAMES:Array=["ZONE", "DEAD", "OBSERVE", "SHIPS", "PLANE_NAVPOINTS", "PLANES"];

        public static const ZONE:int=COUNT++;

        public static const ZONE_INDICATOR:int=COUNT++;

        public static const DEAD:int=COUNT++;

        public static const OBSERVE:int=COUNT++;

        public static const SHIPS:int=COUNT++;

        public static const PLANE_NAVPOINTS:int=COUNT++;

        public static const PLANES:int=COUNT++;

        public static var COUNT:int=0;
    }
}
