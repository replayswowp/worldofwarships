package lesta.dialogs.battle_window._minimap 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import lesta.cpp.*;
    import lesta.dialogs.battle_window._minimap.components.*;
    import lesta.dialogs.battle_window._minimap.constants.*;
    import scaleform.gfx.*;
    
    public class Minimap extends flash.display.Sprite
    {
        public function Minimap()
        {
            this.mapContentOffset = new flash.geom.Point(0, 0);
            super();
            var loc1:*;
            this.size.mouseChildren = loc1 = false;
            this.size.mouseEnabled = loc1;
            this.size.visible = false;
            this.horizLetters.mouseChildren = loc1 = false;
            this.horizLetters.mouseEnabled = loc1;
            this.vertLetters.mouseChildren = loc1 = false;
            this.vertLetters.mouseEnabled = loc1;
            scaleform.gfx.InteractiveObjectEx.setHitTestDisable(this.size, true);
            scaleform.gfx.InteractiveObjectEx.setHitTestDisable(this.horizLetters, true);
            scaleform.gfx.InteractiveObjectEx.setHitTestDisable(this.vertLetters, true);
            this.vertLetters.type = 1;
            this.vertLetters.cacheAsBitmap = true;
            this.horizLetters.type = 0;
            this.horizLetters.cacheAsBitmap = true;
            this.mapWidget = this.mapContent["mapWidget"];
            this.mapContentOffset.x = (this.size.width - this.mapWidget.settings.defaultSize.x) / 2;
            this.mapContentOffset.y = (this.size.height - this.mapWidget.settings.defaultSize.y) / 2;
            return;
        }

        public function setRotation(arg1:Number, arg2:Boolean=false):void
        {
            var loc3:*=NaN;
            var loc4:*=0;
            var loc5:*=0;
            var loc6:*=null;
            var loc1:*=(-Math.round(arg1 / 90)) * 90;
            var loc2:*=Math.abs(arg1 + loc1);
            if (arg2 || this.canRotate && !this.isTweenBlocked && !(this.mapContent.rotation == loc1) && !(Math.abs(loc1) == Math.abs(this.mapContent.rotation) && Math.abs(loc1) == 180) && (loc2 < this.toleranceAngle || loc2 > 90 - this.toleranceAngle)) 
            {
                this.isTweenBlocked = true;
                loc3 = this.mapContent.rotation;
                if (Math.abs(loc3) == 180 && loc3 * loc1 < 0) 
                {
                    loc3 = loc3 * -1;
                }
                lesta.cpp.Tween.to(this.mapContent, this.tweenDuration, {"rotation":loc3}, {"rotation":loc1}, this.onTweenEnd, 1);
                this.vertLetters.visible = false;
                this.horizLetters.visible = false;
                this.vertLetters.rotation = loc1;
                this.horizLetters.rotation = loc1;
                this.mapWidget.settings.mapRotation = loc1;
                loc4 = this.mapWidget.capturePoints.listItems.length;
                loc5 = 0;
                while (loc5 < loc4) 
                {
                    (loc6 = this.mapWidget.capturePoints.listItems[loc5] as lesta.dialogs.battle_window._minimap.components.MapCapturePointItem).updateVerticalRotation();
                    ++loc5;
                }
            }
            return;
        }

        public function updateScale(arg1:Number):void
        {
            if (this.scale == arg1) 
            {
                return;
            }
            this.scale = arg1;
            this.size.width = this.mapWidget.settings.defaultSize.x * this.scale + this.mapContentOffset.x * 2;
            this.size.height = this.mapWidget.settings.defaultSize.y * this.scale + this.mapContentOffset.y * 2;
            this.mapContent.x = this.mapWidget.settings.defaultSize.x * this.scale / 2 + this.mapContentOffset.x;
            this.mapContent.y = this.mapWidget.settings.defaultSize.y * this.scale / 2 + this.mapContentOffset.y;
            this.scaleButtons.x = this.size.width - this.scaleButtons["size"].width;
            this.scaleButtons.y = this.size.height - this.scaleButtons["size"].height;
            this.vertLetters.x = this.size.width - this.mapContentOffset.x;
            this.horizLetters.y = this.size.height - this.mapContentOffset.y;
            var loc1:*;
            this.horizLetters.x = loc1 = this.mapContentOffset.x;
            this.vertLetters.y = loc1;
            this.updateLetters();
            dispatchEvent(new flash.events.Event(flash.events.Event.RESIZE));
            return;
        }

        internal function onTweenEnd():void
        {
            this.isTweenBlocked = false;
            this.vertLetters.visible = true;
            this.horizLetters.visible = true;
            this.mapContent["enemyBase"].rotation = -this.mapContent.rotation;
            this.mapContent["alliedBase"].rotation = -this.mapContent.rotation;
            dispatchEvent(new flash.events.Event(flash.events.Event.RESIZE));
            return;
        }

        internal function updateLetters():void
        {
            var loc5:*=null;
            var loc1:*=this.mapWidget.settings.defaultSize.x / 10 * this.scale;
            var loc2:*=(loc1 - this.horizLetters.getChildAt(0).width) / 2;
            var loc3:*=(loc1 - this.vertLetters.getChildAt(0).height) / 2;
            var loc4:*=0;
            while (loc4 < 10) 
            {
                (loc5 = this.vertLetters.getChildAt(loc4)).y = loc3 + loc4 * loc1;
                (loc5 = this.horizLetters.getChildAt(loc4)).x = loc2 + loc4 * loc1;
                ++loc4;
            }
            return;
        }

        public function playCommandSource(arg1:int):void
        {
            var loc1:*=this.mapWidget.ships.mapItems[arg1] as lesta.dialogs.battle_window._minimap.components.MapShipItem;
            if (loc1) 
            {
                loc1.targetDelegator.playSource();
            }
            return;
        }

        public function playCommandTarget(arg1:int):void
        {
            var loc1:*=this.mapWidget.ships.mapItems[arg1] as lesta.dialogs.battle_window._minimap.components.MapShipItem;
            if (loc1) 
            {
                loc1.targetDelegator.playTarget();
            }
            return;
        }

        public function clearCommandSource(arg1:int):void
        {
            var loc1:*=this.mapWidget.ships.mapItems[arg1] as lesta.dialogs.battle_window._minimap.components.MapShipItem;
            if (loc1) 
            {
                loc1.targetDelegator.clearSource();
            }
            return;
        }

        public function setUserDead():void
        {
            this.mapWidget.mouseChildren = false;
            this.mapWidget.mouseEnabled = false;
            this.mapWidget.setGroupVisibility(lesta.dialogs.battle_window._minimap.constants.MapItemGroups.OWN_SQUADRON, false);
            this.mapWidget.setGroupVisibility(lesta.dialogs.battle_window._minimap.constants.MapItemGroups.RANGES, false);
            this.mapWidget.observe.item.mask = null;
            return;
        }

        internal static const SHIP_ITEM:String="ship";

        public var mapContent:flash.display.Sprite;

        public var vertLetters:lesta.dialogs.battle_window._minimap.components.MapLabelsClip;

        public var horizLetters:lesta.dialogs.battle_window._minimap.components.MapLabelsClip;

        public var size:flash.display.Sprite;

        public var scaleButtons:flash.display.Sprite;

        public var toleranceAngle:Number=15;

        public var tweenDuration:Number=2;

        public var canRotate:Boolean=false;

        internal var isTweenBlocked:Boolean=false;

        internal var mapWidget:lesta.dialogs.battle_window._minimap.WoWsMapWidget;

        internal var scale:Number=1;

        internal var mapContentOffset:flash.geom.Point;
    }
}
