package lesta.dialogs.battle_window._minimap.components 
{
    import flash.display.*;
    import lesta.constants.*;
    import lesta.structs.*;
    import lesta.utils.*;
    
    public class MapSquadronItem extends lesta.dialogs.battle_window._minimap.components.MapEntityItem
    {
        public function MapSquadronItem()
        {
            super();
            return;
        }

        public override function setData(arg1:Object):void
        {
            super.setData(arg1);
            lesta.utils.DisplayObjectUtils.stopAndInvisibleRecursive(this.typeIcon);
            this.typeIcon.visible = true;
            this.planeInfo = arg1 as lesta.structs.PlaneInfo;
            this.curStateTeamkiller = this.planeInfo.tkStatus;
            var loc1:*=MAP_TYPE_INDEX[this.planeInfo.typeIdent][this.planeInfo.isConsumable ? 1 : 0];
            var loc2:*=this.typeIcon.getChildAt(loc1) as flash.display.DisplayObjectContainer;
            loc2.visible = true;
            var loc3:*;
            (loc3 = loc2.getChildAt(this.planeInfo.relation == lesta.constants.PlayerRelation.FRIEND && this.planeInfo.tkStatus ? TEAMKILLER_INDEX : this.planeInfo.relation)).visible = true;
            return;
        }

        public override function refresh():void
        {
            if (this.curStateTeamkiller != this.planeInfo.tkStatus) 
            {
                this.setData(this.planeInfo);
            }
            this.mSelectedClip.visible = this.planeInfo.isSelected;
            return;
        }

        internal static function createTypeIndexMap():Object
        {
            var loc1:*={};
            loc1[lesta.constants.PlaneTypes.SCOUNT] = [0, 0];
            loc1[lesta.constants.PlaneTypes.DIVE_BOMBER] = [1, 0];
            loc1[lesta.constants.PlaneTypes.TORPEDO_BOMBER] = [2, 0];
            loc1[lesta.constants.PlaneTypes.FIGHTER] = [3, 4];
            return loc1;
        }

        
        {
            MAP_TYPE_INDEX = createTypeIndexMap();
        }

        internal static const TEAMKILLER_INDEX:uint=3;

        public var typeIcon:flash.display.Sprite;

        public var mSelectedClip:flash.display.MovieClip;

        internal var planeInfo:lesta.structs.PlaneInfo;

        internal var curStateTeamkiller:Boolean;

        internal static var MAP_TYPE_INDEX:Object;
    }
}
