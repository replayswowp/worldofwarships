package lesta.dialogs.battle_window._minimap.components 
{
    import lesta.controls.minimap.*;
    import lesta.structs.*;
    
    public class MapEntityItem extends lesta.controls.minimap.MapItem
    {
        public function MapEntityItem()
        {
            super();
            return;
        }

        public override function setData(arg1:Object):void
        {
            super.setData(arg1);
            this.entity = arg1 as lesta.structs.EntityInfo;
            return;
        }

        public override function update():void
        {
            refresh();
            this.visible = this.entity.isVisible && settingsCommon.mapGroupsVisibility[settingsType.group] && settingsCommon.mapGroupsVisibility[itemData.group];
            if (this.visible) 
            {
                if (curLayoutId != itemData.layoutId) 
                {
                    curLayoutId = itemData.layoutId;
                    settingsCommon.listLayouts[curLayoutId].addChild(this);
                }
                this.x = this.entity.worldPos.x * settingsCommon.defaultSize.x;
                this.y = this.entity.worldPos.z * settingsCommon.defaultSize.y;
                this.scaleX = settingsCommon.itemsScaleX / settingsCommon.mapScaleX;
                this.scaleY = settingsCommon.itemsScaleY / settingsCommon.mapScaleY;
            }
            return;
        }

        public override function updateVisible():void
        {
            this.visible = this.entity.isVisible && settingsCommon.mapGroupsVisibility[settingsType.group] && settingsCommon.mapGroupsVisibility[itemData.group];
            return;
        }

        public override function updateScale():void
        {
            this.scaleX = settingsCommon.itemsScaleX / settingsCommon.mapScaleX;
            this.scaleY = settingsCommon.itemsScaleY / settingsCommon.mapScaleY;
            return;
        }

        internal var entity:lesta.structs.EntityInfo;
    }
}
