package lesta.dialogs.battle_window._minimap.components 
{
    import flash.display.*;
    
    public class MapLabelsClip extends flash.display.MovieClip
    {
        public function MapLabelsClip()
        {
            super();
            this.reverseLetters = letters.slice().reverse();
            this.reverseDigits = digits.slice().reverse();
            return;
        }

        public override function set rotation(arg1:Number):void
        {
            var loc2:*=null;
            this.setCurrentArray(arg1);
            var loc1:*=0;
            while (loc1 < numChildren) 
            {
                loc2 = getChildAt(loc1);
                loc2["text"] = this.currentArray[loc1];
                ++loc1;
            }
            return;
        }

        internal function setCurrentArray(arg1:Number):void
        {
            var loc1:*=arg1;
            switch (loc1) 
            {
                case 0:
                {
                    this.currentArray = this.type != 0 ? letters : digits;
                    break;
                }
                case 90:
                {
                    this.currentArray = this.type != 0 ? digits : this.reverseLetters;
                    break;
                }
                case 180:
                case -180:
                {
                    this.currentArray = this.type != 0 ? this.reverseLetters : this.reverseDigits;
                    break;
                }
                case -90:
                case 270:
                {
                    this.currentArray = this.type != 0 ? this.reverseDigits : letters;
                    break;
                }
            }
            return;
        }

        public static const letters:Array=["A", "B", "C", "D", "E", "F", "G", "H", "I", "J"];

        public static const digits:Array=["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];

        public var type:int;

        internal var currentArray:Array;

        internal var reverseLetters:Array;

        internal var reverseDigits:Array;
    }
}
