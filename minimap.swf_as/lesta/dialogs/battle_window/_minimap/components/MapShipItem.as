﻿package lesta.dialogs.battle_window._minimap.components 
{
    import flash.display.*;
    import lesta.constants.*;
    import lesta.dialogs.battle_window.*;
    import lesta.dialogs.battle_window._minimap.constants.*;
    import lesta.structs.*;
    import lesta.utils.*;
    
    public class MapShipItem extends lesta.dialogs.battle_window._minimap.components.MapEntityItem
    {
        public function MapShipItem()
        {
            super();
            this.targetDelegator = new lesta.dialogs.battle_window.TargetDelegator(this.mSignalPlayerAttack, this.mSignalPlayerTarget);
            return;
        }

        public override function setData(arg1:Object):void
        {
            super.setData(arg1);
            lesta.utils.DisplayObjectUtils.stopAndInvisibleRecursive(this.typeIcon);
            this.typeIcon.visible = true;
            this.playerInfo = arg1 as lesta.structs.Player;
            var loc1:*=this.playerInfo.relation != lesta.constants.PlayerRelation.SELF ? this.playerInfo.shipTypeIndex + 1 : 0;
            this.icon = this.typeIcon.getChildAt(loc1) as flash.display.DisplayObjectContainer;
            this.icon.visible = true;
            this.directionLine.visible = this.playerInfo.relation == lesta.constants.PlayerRelation.SELF && this.playerInfo.isAlive;
            this.curStateTeamkiller = this.playerInfo.tkStatus;
            this.curStateIcon = this.icon.getChildAt(this.getFrameIndex());
            this.curStateIcon.visible = true;
            return;
        }

        public override function refresh():void
        {
            if (!(this.curStateAlive == this.playerInfo.isAlive) || !(this.curStateVisible == this.playerInfo.vehicleVisible)) 
            {
                this.curStateAlive = this.playerInfo.isAlive;
                this.curStateVisible = this.playerInfo.vehicleVisible;
                this.curStateIcon.visible = false;
                this.curStateIcon = this.icon.getChildAt(this.getFrameIndex());
                this.curStateIcon.visible = true;
                this.directionLine.visible = this.playerInfo.relation == lesta.constants.PlayerRelation.SELF && this.playerInfo.isAlive;
                if (!this.curStateAlive) 
                {
                    itemData.layoutId = lesta.dialogs.battle_window._minimap.constants.MapLayouts.DEAD;
                    itemData.group = lesta.dialogs.battle_window._minimap.constants.MapItemGroups.DEAD;
                }
            }
            if (this.curStateTeamkiller != this.playerInfo.tkStatus) 
            {
                this.setData(this.playerInfo);
            }
            this.rotation = this.playerInfo.worldYaw * lesta.utils.Geometry.DEG_TO_RAD_COEF;
            return;
        }

        protected function getFrameIndex():int
        {
            var loc2:*=0;
            if (this.playerInfo.relation == lesta.constants.PlayerRelation.SELF) 
            {
                return this.playerInfo.isAlive ? INDEX_SELF : INDEX_SELF_DEAD;
            }
            if (!this.playerInfo.isAlive) 
            {
                return INDEX_DEAD;
            }
            var loc1:*=this.playerInfo.vehicleVisible ? 0 : 1;
            if (this.playerInfo.relation != lesta.constants.PlayerRelation.ENEMY) 
            {
                if (this.playerInfo.isInSameDivision) 
                {
                    loc2 = INDEX_DIVISION;
                }
                else 
                {
                    loc2 = this.playerInfo.tkStatus ? INDEX_TEAMKILLER : INDEX_ALLY;
                }
            }
            else 
            {
                loc2 = INDEX_ENEMY;
            }
            return loc2 + loc1;
        }

        internal static const INDEX_SELF:int=1;

        internal static const INDEX_SELF_DEAD:int=0;

        internal static const INDEX_TEAMKILLER_INVIS:int=8;

        internal static const INDEX_TEAMKILLER:int=7;

        internal static const INDEX_ENEMY_INVIS:int=6;

        internal static const INDEX_ENEMY:int=5;

        internal static const INDEX_ALLY_INVIS:int=4;

        internal static const INDEX_ALLY:int=3;

        internal static const INDEX_DIVISION_INVIS:int=2;

        internal static const INDEX_DIVISION:int=1;

        internal static const INDEX_DEAD:int=0;

        public var typeIcon:flash.display.Sprite;

        public var icon:flash.display.DisplayObjectContainer;

        public var curStateIcon:flash.display.DisplayObject;

        public var mSignalPlayerAttack:flash.display.MovieClip;

        public var mSignalPlayerTarget:flash.display.MovieClip;

        public var directionLine:flash.display.MovieClip;

        public var targetDelegator:lesta.dialogs.battle_window.TargetDelegator;

        internal var curStateAlive:Boolean=true;

        internal var curStateVisible:Boolean=false;

        internal var curStateTeamkiller:Boolean;

        internal var playerInfo:lesta.structs.Player;
    }
}
