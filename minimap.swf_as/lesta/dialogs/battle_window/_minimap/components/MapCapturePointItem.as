package lesta.dialogs.battle_window._minimap.components 
{
    import flash.display.*;
    import flash.utils.*;
    import lesta.controls.minimap.*;
    import lesta.dialogs.battle_window._battle_progress.*;
    import lesta.structs.*;
    
    public class MapCapturePointItem extends lesta.controls.minimap.MapItem
    {
        public function MapCapturePointItem()
        {
            super();
            return;
        }

        public override function setData(arg1:Object):void
        {
            super.setData(arg1);
            this.info = arg1 as lesta.structs.CapturePointInfo;
            if (!this.indicator) 
            {
                this.indicator = this.createIndicator();
                this.indicator.initialize(this.info.id);
                addChild(this.indicator);
            }
            x = this.info.posX * settingsCommon.defaultSize.x;
            y = this.info.posY * settingsCommon.defaultSize.y;
            var loc1:*=flash.utils.getDefinitionByName(ZONE_CLIP_CLASS) as Class;
            this.zone = new loc1() as flash.display.MovieClip;
            this.zone.gotoAndStop(this.info.ownerRelation + 1);
            var loc2:*;
            this.zone.scaleY = loc2 = this.info.radius * 3.0625;
            this.zone.scaleX = loc2;
            addChildAt(this.zone, 0);
            return;
        }

        public override function update():void
        {
            if (curLayoutId != itemData.layoutId) 
            {
                curLayoutId = itemData.layoutId;
                settingsCommon.listLayouts[curLayoutId].addChild(this);
            }
            this.indicator.scaleX = settingsCommon.itemsScaleX / settingsCommon.mapScaleX * MINIMAP_CAPTURE_POINT_INDICATOR_SCALE;
            this.indicator.scaleY = settingsCommon.itemsScaleY / settingsCommon.mapScaleY * MINIMAP_CAPTURE_POINT_INDICATOR_SCALE;
            this.indicator.update(this.info);
            this.zone.visible = this.info.isVisible;
            var loc1:*=this.info.ownerRelation + 1;
            if (this.zone.currentFrame != loc1) 
            {
                this.zone.gotoAndStop(loc1);
            }
            return;
        }

        public override function updateScale():void
        {
            this.indicator.scaleX = settingsCommon.itemsScaleX / settingsCommon.mapScaleX * MINIMAP_CAPTURE_POINT_INDICATOR_SCALE;
            this.indicator.scaleY = settingsCommon.itemsScaleY / settingsCommon.mapScaleY * MINIMAP_CAPTURE_POINT_INDICATOR_SCALE;
            return;
        }

        public function updateVerticalRotation():void
        {
            this.indicator.letters.rotation = -settingsCommon.mapRotation;
            return;
        }

        protected function createIndicator():lesta.dialogs.battle_window._battle_progress.CapturePointIndicator
        {
            var loc1:*=flash.utils.getDefinitionByName("CapturePointIndicatorClip") as Class;
            return new loc1() as lesta.dialogs.battle_window._battle_progress.CapturePointIndicator;
        }

        internal static const MINIMAP_CAPTURE_POINT_INDICATOR_SCALE:Number=0.6;

        internal static const ZONE_CLIP_CLASS:String="CaptureZoneClip";

        internal var zone:flash.display.MovieClip;

        internal var indicator:lesta.dialogs.battle_window._battle_progress.CapturePointIndicator;

        internal var info:lesta.structs.CapturePointInfo;
    }
}
