package lesta.dialogs.battle_window._minimap 
{
    import flash.display.*;
    import lesta.controls.minimap.*;
    import lesta.dialogs.battle_window._minimap.constants.*;
    
    public class WoWsMapWidget extends lesta.controls.minimap.MapWidget
    {
        public function WoWsMapWidget()
        {
            this.rectAttentions = new flash.display.Sprite();
            super();
            setItemTypesCount(lesta.dialogs.battle_window._minimap.constants.MapItemTypes.COUNT);
            setCountGroups(lesta.dialogs.battle_window._minimap.constants.MapItemGroups.COUNT);
            addChild(this.rectAttentions);
            this.ships = new lesta.controls.minimap.ItemsContainer(lesta.dialogs.battle_window._minimap.constants.MapItemTypes.SHIP_ITEM, lesta.dialogs.battle_window._minimap.constants.MapLayouts.SHIPS, lesta.dialogs.battle_window._minimap.constants.MapItemGroups.DEFAULT);
            this.squadrons = new lesta.controls.minimap.ItemsContainer(lesta.dialogs.battle_window._minimap.constants.MapItemTypes.SQUADRON_ITEM, lesta.dialogs.battle_window._minimap.constants.MapLayouts.PLANES, lesta.dialogs.battle_window._minimap.constants.MapItemGroups.SQUADRON);
            this.navpoints = new lesta.controls.minimap.ItemsContainer(lesta.dialogs.battle_window._minimap.constants.MapItemTypes.NAVPOINTS_ITEM, lesta.dialogs.battle_window._minimap.constants.MapLayouts.PLANE_NAVPOINTS, lesta.dialogs.battle_window._minimap.constants.MapItemGroups.OWN_SQUADRON);
            this.capturePoints = new lesta.controls.minimap.ItemsContainer(lesta.dialogs.battle_window._minimap.constants.MapItemTypes.CAPTURE_POINT_ITEM, lesta.dialogs.battle_window._minimap.constants.MapLayouts.ZONE_INDICATOR, lesta.dialogs.battle_window._minimap.constants.MapItemGroups.DEFAULT);
            this.observe = new lesta.controls.minimap.SingleItemContainer(lesta.dialogs.battle_window._minimap.constants.MapItemTypes.OBSERVE_VECTOR_ITEM, lesta.dialogs.battle_window._minimap.constants.MapLayouts.OBSERVE, lesta.dialogs.battle_window._minimap.constants.MapItemGroups.DEFAULT);
            this.shootRange = new lesta.controls.minimap.SingleItemContainer(lesta.dialogs.battle_window._minimap.constants.MapItemTypes.SHOOT_RANGE_ITEM, lesta.dialogs.battle_window._minimap.constants.MapLayouts.OBSERVE, lesta.dialogs.battle_window._minimap.constants.MapItemGroups.RANGES);
            this.visibilityRange = new lesta.controls.minimap.SingleItemContainer(lesta.dialogs.battle_window._minimap.constants.MapItemTypes.VISIBILITY_RANGE_ITEM, lesta.dialogs.battle_window._minimap.constants.MapLayouts.OBSERVE, lesta.dialogs.battle_window._minimap.constants.MapItemGroups.RANGES);
            setItemsContainer(this.ships);
            setItemsContainer(this.squadrons);
            setItemsContainer(this.navpoints);
            setItemsContainer(this.capturePoints);
            setItemsContainer(this.observe);
            setItemsContainer(this.shootRange);
            setItemsContainer(this.visibilityRange);
            return;
        }

        public var ships:lesta.controls.minimap.ItemsContainer;

        public var squadrons:lesta.controls.minimap.ItemsContainer;

        public var navpoints:lesta.controls.minimap.ItemsContainer;

        public var capturePoints:lesta.controls.minimap.ItemsContainer;

        public var observe:lesta.controls.minimap.SingleItemContainer;

        public var shootRange:lesta.controls.minimap.SingleItemContainer;

        public var visibilityRange:lesta.controls.minimap.SingleItemContainer;

        public var rectAttentions:flash.display.Sprite;
    }
}
