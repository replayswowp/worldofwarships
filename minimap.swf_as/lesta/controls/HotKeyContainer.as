package lesta.controls 
{
    import flash.events.*;
    import lesta.managers.*;
    
    public class HotKeyContainer extends lesta.controls.HotKeyAnimation
    {
        public function HotKeyContainer()
        {
            super();
            addEventListener(flash.events.Event.ADDED_TO_STAGE, this.setIndicator);
            addEventListener(flash.events.Event.REMOVED_FROM_STAGE, this.onRemovedFromStage);
            return;
        }

        public override function fini():void
        {
            lesta.managers.HotKeyManager.instance.clearHotkeyIndicator(_commandId);
            super.fini();
            return;
        }

        internal function setIndicator(arg1:flash.events.Event=null):void
        {
            if (_commandId == -1) 
            {
                return;
            }
            updateCommandData();
            lesta.managers.HotKeyManager.instance.setHotkeyIndicator(this, _commandId);
            return;
        }

        internal function onRemovedFromStage(arg1:flash.events.Event):void
        {
            lesta.managers.HotKeyManager.instance.clearHotkeyIndicator(_commandId);
            return;
        }

        public override function setState(arg1:String, arg2:Boolean=false, arg3:Boolean=true, arg4:String=null, arg5:Boolean=false):void
        {
            if (this._isInited) 
            {
                super.setState(arg1, arg2, arg3, arg4, arg5);
            }
            else 
            {
                this.cacheState = arg1;
            }
            return;
        }

        public override function set commandId(arg1:int):void
        {
            _commandId = arg1;
            if (stage) 
            {
                this.setIndicator();
            }
            return;
        }

        public function set isInited(arg1:Boolean):void
        {
            this._isInited = arg1;
            if (arg1) 
            {
                this.setState(this.cacheState);
            }
            return;
        }

        public static const INVISIBLE:String="inv";

        public static const DEFAULT:String="def";

        public static const DOWN:String="down";

        public static const TOGGLE:String="toggle";

        internal var _isInited:Boolean=true;

        internal var cacheState:String="";
    }
}
