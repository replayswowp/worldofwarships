package lesta.controls.minimap 
{
    public class ItemTypeSettings extends Object
    {
        public function ItemTypeSettings()
        {
            super();
            return;
        }

        public var group:int=0;

        public var typeId:int=0;

        public var visible:Boolean=true;

        public var posX:Number=0;

        public var posY:Number=0;

        public var scaleX:Number=0;

        public var scaleY:Number=0;
    }
}
