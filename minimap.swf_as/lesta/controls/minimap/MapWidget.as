package lesta.controls.minimap 
{
    import __AS3__.vec.*;
    import flash.display.*;
    
    public class MapWidget extends flash.display.Sprite
    {
        public function MapWidget()
        {
            this.listLayouts = new Array();
            this.settings = new lesta.controls.minimap.MapSettings();
            super();
            this.settings.defaultSize.x = this.width / this.scaleX;
            this.settings.defaultSize.y = this.height / this.scaleY;
            return;
        }

        public function setItemTypesCount(arg1:int):void
        {
            this.listItemTypes = new Vector.<lesta.controls.minimap.Container>(arg1, true);
            this.countItemsTypes = arg1;
            return;
        }

        public function setCountLayouts(arg1:int, arg2:Array):void
        {
            var loc2:*=null;
            this.settings.listLayouts = new Vector.<flash.display.Sprite>(arg1, true);
            var loc1:*=0;
            while (loc1 < arg1) 
            {
                (loc2 = new flash.display.Sprite()).name = "layout_" + arg2[loc1 + 1];
                var loc3:*;
                loc2.mouseEnabled = loc3 = false;
                loc2.mouseChildren = loc3;
                addChild(loc2);
                this.settings.listLayouts[loc1] = loc2;
                ++loc1;
            }
            return;
        }

        public function setCountGroups(arg1:int):void
        {
            this.settings.mapGroupsVisibility = new Vector.<Boolean>(arg1, true);
            var loc1:*=0;
            while (loc1 < arg1) 
            {
                this.settings.mapGroupsVisibility[loc1] = true;
                ++loc1;
            }
            return;
        }

        public function setItemsContainer(arg1:lesta.controls.minimap.Container):void
        {
            arg1.mapSettings = this.settings;
            this.listItemTypes[arg1.ident] = arg1;
            return;
        }

        public function setGroupVisibility(arg1:int, arg2:Boolean):void
        {
            this.settings.mapGroupsVisibility[arg1] = arg2;
            this.refreshAllItemsVisibility();
            return;
        }

        public function setScale(arg1:Number, arg2:Number):void
        {
            this.scaleX = arg1;
            this.scaleY = arg2;
            this.settings.mapScaleX = arg1;
            this.settings.mapScaleY = arg2;
            return;
        }

        public function setAllItemsScale(arg1:Number, arg2:Number):void
        {
            this.settings.itemsScaleX = arg1;
            this.settings.itemsScaleY = arg2;
            return;
        }

        public function updateAllItems():void
        {
            var loc1:*=0;
            while (loc1 < this.countItemsTypes) 
            {
                this.listItemTypes[loc1].updateAllItems();
                ++loc1;
            }
            return;
        }

        public function updateAllItemsScale():void
        {
            this.refreshAllItemsScale();
            return;
        }

        protected function refreshAllItemsScale():void
        {
            var loc1:*=0;
            while (loc1 < this.countItemsTypes) 
            {
                this.listItemTypes[loc1].updateAllItemsScale();
                ++loc1;
            }
            return;
        }

        internal function refreshAllItemsVisibility():void
        {
            var loc1:*=0;
            while (loc1 < this.countItemsTypes) 
            {
                this.listItemTypes[loc1].updateAllItemsVisibility();
                ++loc1;
            }
            return;
        }

        internal var listLayouts:Array;

        internal var listItemTypes:__AS3__.vec.Vector.<lesta.controls.minimap.Container>=null;

        internal var countItemsTypes:int=0;

        public var settings:lesta.controls.minimap.MapSettings;
    }
}
