﻿package lesta.controls.minimap 
{
    import __AS3__.vec.*;
    import flash.geom.*;
    
    public class MapSettings extends Object
    {
        public function MapSettings()
        {
            this.defaultSize = new flash.geom.Point();
            super();
            return;
        }

        public var defaultSize:flash.geom.Point;

        public var itemsScaleX:Number=1;

        public var itemsScaleY:Number=1;

        public var mapScaleX:Number=1;

        public var mapScaleY:Number=1;

        public var mapRotation:Number=0;

        public var mapGroupsVisibility:__AS3__.vec.Vector.<Boolean>=null;

        public var listLayouts:*=null;
    }
}
