package lesta.controls.minimap 
{
    import lesta.utils.*;
    
    public class DataItemUpdater extends Object implements lesta.utils.IUpdater
    {
        public function DataItemUpdater(arg1:Function, arg2:Object)
        {
            super();
            this.func = arg1;
            this.obj = arg2;
            return;
        }

        public function update(arg1:Object):void
        {
            this.func.call(this.obj, arg1);
            return;
        }

        internal var func:Function;

        internal var obj:Object;
    }
}
