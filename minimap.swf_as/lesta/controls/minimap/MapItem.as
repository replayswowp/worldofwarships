package lesta.controls.minimap 
{
    import flash.display.*;
    import lesta.utils.*;
    
    public class MapItem extends flash.display.Sprite
    {
        public function MapItem()
        {
            this.itemData = new lesta.controls.minimap.MapItemData();
            super();
            return;
        }

        public function setClip(arg1:flash.display.DisplayObject):void
        {
            addChild(arg1);
            arg1.x = 0;
            arg1.y = 0;
            this.clip = arg1;
            return;
        }

        public function setData(arg1:Object):void
        {
            this.inited = true;
            this.data = arg1;
            return;
        }

        public function update():void
        {
            this.refresh();
            this.visible = this.itemData.visible && this.settingsCommon.mapGroupsVisibility[this.settingsType.group] && this.settingsCommon.mapGroupsVisibility[this.itemData.group];
            if (visible) 
            {
                if (this.curLayoutId != this.itemData.layoutId) 
                {
                    this.curLayoutId = this.itemData.layoutId;
                    this.settingsCommon.listLayouts[this.curLayoutId].addChild(this);
                }
                this.x = this.itemData.posX * this.settingsCommon.defaultSize.x;
                this.y = this.itemData.posY * this.settingsCommon.defaultSize.y;
                this.rotation = this.itemData.yaw * lesta.utils.Geometry.DEG_TO_RAD_COEF;
                this.scaleX = this.itemData.scaleX;
                this.scaleY = this.itemData.scaleY;
            }
            return;
        }

        public function updateScale():void
        {
            return;
        }

        public function updateVisible():void
        {
            this.visible = this.itemData.visible && this.settingsCommon.mapGroupsVisibility[this.settingsType.group] && this.settingsCommon.mapGroupsVisibility[this.itemData.group];
            return;
        }

        public function refresh():void
        {
            return;
        }

        public var clip:flash.display.DisplayObject=null;

        public var data:Object=null;

        public var itemData:lesta.controls.minimap.MapItemData;

        public var settingsCommon:lesta.controls.minimap.MapSettings=null;

        public var settingsType:lesta.controls.minimap.ItemTypeSettings=null;

        protected var curLayoutId:int=-1;

        public var inited:Boolean=false;
    }
}
