package lesta.controls.minimap 
{
    import flash.display.*;
    
    public class SingleItemContainer extends lesta.controls.minimap.Container
    {
        public function SingleItemContainer(arg1:int, arg2:int, arg3:int)
        {
            super(arg1, arg2, arg3);
            return;
        }

        public function setClip(arg1:flash.display.DisplayObject, arg2:Object=null):void
        {
            this.item = new lesta.controls.minimap.MapItem();
            this.item.setClip(arg1);
            this.item.setData(arg2);
            this.item.itemData.layoutId = layoutId;
            this.item.settingsCommon = mapSettings;
            this.item.settingsType = settings;
            return;
        }

        public function update():void
        {
            this.item.update();
            return;
        }

        public override function updateAllItems():void
        {
            this.item.update();
            return;
        }

        public override function updateAllItemsScale():void
        {
            this.item.updateScale();
            return;
        }

        public override function updateAllItemsVisibility():void
        {
            this.item.updateVisible();
            return;
        }

        public function toString():String
        {
            var loc1:*="[SingleItemInfo ident : %]";
            return loc1.replace("%", ident);
        }

        public var item:lesta.controls.minimap.MapItem=null;
    }
}
