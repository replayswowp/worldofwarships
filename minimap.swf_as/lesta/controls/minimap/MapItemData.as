package lesta.controls.minimap 
{
    public class MapItemData extends Object
    {
        public function MapItemData()
        {
            super();
            return;
        }

        internal var curLayoutId:int=-1;

        public var group:int=0;

        public var layoutId:int=0;

        public var posX:Number=0;

        public var posY:Number=0;

        public var yaw:Number=0;

        public var scaleX:Number=1;

        public var scaleY:Number=1;

        public var visible:Boolean=true;
    }
}
