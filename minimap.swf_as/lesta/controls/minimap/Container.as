package lesta.controls.minimap 
{
    public class Container extends Object
    {
        public function Container(arg1:int, arg2:int, arg3:int)
        {
            this.settings = new lesta.controls.minimap.ItemTypeSettings();
            super();
            this.ident = arg1;
            this.layoutId = arg2;
            this.settings.typeId = arg1;
            this.settings.group = arg3;
            return;
        }

        public function updateAllItems():void
        {
            return;
        }

        public function updateAllItemsScale():void
        {
            return;
        }

        public function updateAllItemsVisibility():void
        {
            return;
        }

        public var ident:int=-1;

        public var layoutId:int=0;

        public var mapSettings:lesta.controls.minimap.MapSettings=null;

        public var settings:lesta.controls.minimap.ItemTypeSettings;
    }
}
