package lesta.controls.minimap 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.utils.*;
    import lesta.utils.*;
    
    public class ItemsContainer extends lesta.controls.minimap.Container
    {
        public function ItemsContainer(arg1:int, arg2:int, arg3:int)
        {
            this.dataProvider = [];
            this.fixedListItems = new Vector.<lesta.controls.minimap.MapItem>();
            this.listItems = new Vector.<lesta.controls.minimap.MapItem>();
            this.mapItems = new flash.utils.Dictionary();
            super(arg1, arg2, arg3);
            return;
        }

        public function init(arg1:Class, arg2:Array, arg3:String, arg4:String):void
        {
            this.ItemClassClip = arg1;
            this.dataProvider = arg2 == null ? [] : arg2;
            this.labelId = arg3;
            this.mapLabelId = arg4;
            this.spreadUpdater = new lesta.utils.FixedSpreadUpdate(10, new lesta.controls.minimap.DataItemUpdater(this.updateItemByData, this));
            this.spreadUpdater.init(this.dataProvider);
            return;
        }

        public function createItemsWithIncrementId(arg1:int):void
        {
            this.fixedListItems = new Vector.<lesta.controls.minimap.MapItem>(arg1, true);
            this.listItems = new Vector.<lesta.controls.minimap.MapItem>(arg1, true);
            var loc1:*=0;
            while (loc1 < arg1) 
            {
                this.fixedListItems[loc1] = this.createClearItem();
                ++loc1;
            }
            return;
        }

        public function createClearItem():lesta.controls.minimap.MapItem
        {
            var loc1:*=new this.ItemClassClip();
            var loc2:*=loc1 as lesta.controls.minimap.MapItem;
            loc2.itemData.layoutId = layoutId;
            loc2.itemData.group = settings.group;
            loc2.settingsCommon = mapSettings;
            loc2.settingsType = settings;
            return loc2;
        }

        public override function updateAllItems():void
        {
            var loc1:*=this.dataProvider.length;
            var loc2:*=0;
            while (loc2 < loc1) 
            {
                this.updateItemByData(this.dataProvider[loc2]);
                ++loc2;
            }
            return;
        }

        public function updateItemByData(arg1:Object):void
        {
            var loc1:*=arg1[this.labelId];
            var loc2:*=this.fixedListItems[loc1];
            if (!loc2.inited) 
            {
                loc2.setData(arg1);
                this.mapItems[arg1[this.mapLabelId]] = loc2;
                this.listItems[this.countItems] = loc2;
                var loc3:*;
                var loc4:*=((loc3 = this).countItems + 1);
                loc3.countItems = loc4;
            }
            loc2.update();
            return;
        }

        public override function updateAllItemsVisibility():void
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < this.countItems) 
            {
                loc2 = this.listItems[loc1];
                loc2.updateVisible();
                ++loc1;
            }
            return;
        }

        public override function updateAllItemsScale():void
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < this.countItems) 
            {
                loc2 = this.listItems[loc1];
                loc2.updateScale();
                ++loc1;
            }
            return;
        }

        public var ItemClassClip:Class=null;

        public var dataProvider:Array;

        public var labelId:String="id";

        public var mapLabelId:String="id";

        public var fixedListItems:__AS3__.vec.Vector.<lesta.controls.minimap.MapItem>;

        public var listItems:__AS3__.vec.Vector.<lesta.controls.minimap.MapItem>;

        public var mapItems:flash.utils.Dictionary;

        public var countItems:int=0;

        public var spreadUpdater:lesta.utils.FixedSpreadUpdate;
    }
}
