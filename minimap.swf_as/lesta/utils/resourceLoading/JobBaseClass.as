package lesta.utils.resourceLoading 
{
    import flash.events.*;
    
    public class JobBaseClass extends flash.events.EventDispatcher
    {
        public function JobBaseClass(arg1:int)
        {
            super();
            this._id = arg1;
            return;
        }

        public function get id():uint
        {
            return this._id;
        }

        public function release():void
        {
            lesta.utils.resourceLoading.ResourceLoadingDelegator.release(this.id);
            return;
        }

        internal var _id:int;
    }
}
