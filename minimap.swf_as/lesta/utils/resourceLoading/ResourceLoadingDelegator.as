package lesta.utils.resourceLoading 
{
    import flash.utils.*;
    import lesta.data.*;
    
    public class ResourceLoadingDelegator extends Object
    {
        public function ResourceLoadingDelegator()
        {
            super();
            return;
        }

        public static function init():void
        {
            jobs = new flash.utils.Dictionary();
            lesta.data.GameDelegate.addCallBack("resourceLoadingDelegator.doJob", lesta.utils.resourceLoading.ResourceLoadingDelegator, doJob);
            lesta.data.GameDelegate.addCallBack("resourceLoadingDelegator.releaseJob", lesta.utils.resourceLoading.ResourceLoadingDelegator, release);
            return;
        }

        public static function fini():void
        {
            var loc1:*=undefined;
            lesta.data.GameDelegate.removeCallBack(lesta.utils.resourceLoading.ResourceLoadingDelegator);
            var loc2:*=0;
            var loc3:*=jobs;
            for (loc1 in loc3) 
            {
                release(loc1);
            }
            return;
        }

        internal static function createFlashJobId():int
        {
            maxFlashJobID = maxFlashJobID + 2;
            return maxFlashJobID;
        }

        public static function createJob(arg1:String, arg2:String):uint
        {
            var loc1:*=createFlashJobId();
            doJob(arg1, loc1, arg2);
            return loc1;
        }

        internal static function doJob(arg1:String, arg2:int, arg3:String):void
        {
            var loc1:*;
            var loc2:*;
            (loc2 = new (loc1 = lesta.utils.resourceLoading.JobTypes.getJobClass(arg1, arg3))(arg2, arg3)).doJob();
            jobs[arg2] = loc2;
            return;
        }

        public static function getJob(arg1:int):lesta.utils.resourceLoading.IJob
        {
            if (!(arg1 in jobs)) 
            {
                trace("ResourceLoadingDelegator: Error. no such job. ID: " + arg1);
            }
            return jobs[arg1];
        }

        static function release(arg1:int):void
        {
            if (!(arg1 in jobs)) 
            {
                return;
            }
            jobs[arg1].fini();
            delete jobs[arg1];
            return;
        }

        
        {
            maxFlashJobID = 0;
        }

        internal static var jobs:flash.utils.Dictionary;

        internal static var maxFlashJobID:int=0;
    }
}
