package lesta.utils 
{
    import __AS3__.vec.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.constants.*;
    import lesta.data.*;
    import lesta.structs.*;
    import scaleform.clik.data.*;
    
    public class GameInfoHolder extends lesta.data.DataHub
    {
        public function GameInfoHolder()
        {
            this.listEnemyPlayersDeadFirstNotConvoy = new scaleform.clik.data.DataProvider();
            this.listAliedPlayersDeadFirstNotConvoy = new scaleform.clik.data.DataProvider();
            this.listEnemyPlayersDeadFirstConvoy = new scaleform.clik.data.DataProvider();
            this.listAliedPlayersDeadFirstConvoy = new scaleform.clik.data.DataProvider();
            this.alreadyKilledShips = new flash.utils.Dictionary();
            this.listSelectedPlanes = [];
            this.listTorpedoes = [];
            super();
            return;
        }

        public override function init():void
        {
            var battleTypesCollection:lesta.data.EntityCollection;
            var mapsCollection:lesta.data.EntityCollection;
            var maxPlayersCollection:lesta.data.EntityCollection;
            var collectionCapturePoints:lesta.data.EntityCollection;
            var collectionGuns:lesta.data.EntityCollection;

            var loc1:*;
            super.init();
            lesta.data.GameDelegate.addCallBack("GameInfoHolder.initServerTime", this, this.initServerTime);
            this.shipListCollection = createSharedCollection("GameInfoHolder.initShipsList", SHIPS, lesta.structs.ShipInfo);
            createSharedCollection("GameInfoHolder.initTimeList", TIME, null, function (arg1:Object):Object
            {
                return arg1.data;
            })
            createSharedSingleton("GameInfoHolder.updateOptionData", OPTION_DATA);
            this.battleTasksCollection = createSharedCollection("battle.updateTasks", BATTLE_TASKS);
            this.accountLevelInfo = createSharedSingleton("GameInfoHolder.updateAccountLevel", ACCOUNT_LEVEL_INFO, lesta.structs.AccountLevelInfo).getData() as lesta.structs.AccountLevelInfo;
            battleTypesCollection = createSharedCollection("GameInfoHolder.initBattleModeList", TYPE, null, function (arg1:Object):Object
            {
                return arg1.label;
            })
            mapsCollection = createSharedCollection("GameInfoHolder.initMapsList", MAPS, lesta.structs.MapInfo);
            maxPlayersCollection = createSharedCollection("GameInfoHolder.initPlayersNumList", MAX_PLAYERS, Object);
            maxPlayersCollection.clearOnUpdate = true;
            mapsCollection.clearOnUpdate = true;
            battleTypesCollection.clearOnUpdate = true;
            createSharedSingleton("GameInfoHolder.initBalancerInfo", BALANCER_STATS, lesta.structs.BalancerInfo);
            this.cameraInfo = createSharedSingleton(["GameInfoHolder.updateCameraInfo", "GameInfoHolder.setRanges"], CAMERA, lesta.structs.CameraInfo).getData() as lesta.structs.CameraInfo;
            this.gameInfo = createSharedSingleton([new lesta.data.DataChannel("GameInfoHolder.initGameInfo", INIT_GAME_INFO), new lesta.data.DataChannel("GameInfoHolder.updateFragsCount", GAME_INFO_UPDATE_FRAGS), new lesta.data.DataChannel("GameInfoHolder.initGameVersion", "GameVersion")], GAME_INFO, lesta.structs.GameInfo).getData() as lesta.structs.GameInfo;
            createSharedSingleton("GameInfoHolder.setBattleResultsStats", BATTLE_STATS, Object);
            this.battleModeInfo = createSharedSingleton([new lesta.data.DataChannel("GameInfoHolder.setBattleModeInfo", BATTLE_MODE_INFO), new lesta.data.DataChannel("GameInfoHolder.updateBattleModeInfo", UPDATE_BATTLE_MODE_INFO), new lesta.data.DataChannel("GameInfoHolder.updateBattleTime", UPDATE_BATTLE_TIME)], BATTLE_MODE_INFO, lesta.structs.BattleModeInfo).getData() as lesta.structs.BattleModeInfo;
            createSharedSingleton("playerProfile.setPlayerProfileData", PLAYER_PROFILE, Object);
            createSharedSingleton([new lesta.data.DataChannel("dock.initData", DOCK_INIT_DATA), new lesta.data.DataChannel("dock.updateData", DOCK_UPDATE_DATA)], DOCK_DEFAULT_VALUES, lesta.structs.DockData);
            createSharedSingleton("rewards.updateData", REWARD_DATA, lesta.structs.RewardData);
            createSharedSingleton([new lesta.data.DataChannel("DivisionEntrance.initData", DIVISION_ENTRANCE_INIT_DATA), new lesta.data.DataChannel("DivisionEntrance.receiveDivisionSeekers", DIVISION_ENTRANCE_RECEIVE_SEEKERS), new lesta.data.DataChannel("DivisionEntrance.updateDivisionSeekerStatus", DIVISION_ENTRANCE_UPDATE_SEEKER_STATUS)], DIVISION_ENTRANCE_DATA, lesta.structs.DivisionEntranceData);
            createSharedCollection(new lesta.data.DataChannel("DivisionEntrance.invitationToDivision", INVITATIONS_TO_DIVISION), INVITATIONS_TO_DIVISION, lesta.structs.InvitationToDivision).clearOnUpdate = true;
            createSharedCollection("DivisionEntrance.receivePlayerSearchResult", DIVISION_ENTRANCE_RECEIVE_SEARCH_RESULT, Object).clearOnUpdate = true;
            createSharedCollection("Division.players", PLAYERS_IN_DIVISION, lesta.structs.PlayerInPrebattleInfo).clearOnUpdate = true;
            createSharedCollection(new lesta.data.DataChannel("Division.invitedPlayers", PLAYERS_INVITED_TO_DIVISION), PLAYERS_INVITED_TO_DIVISION, lesta.structs.PlayerInvitedToPrebattleInfo).clearOnUpdate = true;
            createSharedSingleton("Division.data", DIVISION_DATA, lesta.structs.DivisionData);
            createSharedSingleton("ModalWindowConvertExp.initData", CONVERT_EXP_DATA, Object);
            this.collectionBotShips = createSharedCollection(new lesta.data.DataChannel("GameInfoHolder.initBotsShipsList", BOTS_SHIPS, this.postUpdateBotShips), BOTS_SHIPS, lesta.structs.PlayerShipInfo);
            this.collectionBotShips.clearOnUpdate = true;
            this.collectionPlayersShips = createSharedCollection([new lesta.data.DataChannel("GameInfoHolder.initPlayerShipsList", PLAYER_SHIPS, this.postUpdatePlayerShips)], PLAYER_SHIPS, lesta.structs.PlayerShipInfo);
            this.collectionPlayersShips.clearOnUpdate = true;
            this.collectionAllCrews = createSharedCollection(new lesta.data.DataChannel("GameInfoHolder.onCrewsRestored", CREWS_ALL), CREWS_ALL, lesta.structs.CrewInfo);
            this.collectionAllCrewSkills = createSharedCollection(new lesta.data.DataChannel("GameInfoHolder.initSkills", CREW_SKILLS_ALL), CREW_SKILLS_ALL, lesta.structs.CrewSkillInfo);
            createSharedSingleton(new lesta.data.DataChannel("GameInfoHolder.updateCrewsSingletone", CREWS_SINGLETON), CREWS_SINGLETON, lesta.structs.CrewsSingleton);
            createSharedSingleton(new lesta.data.DataChannel("GameInfoHolder.updateCrewToHireSingletone", CREW_TO_HIRE_SINGLETON), CREW_TO_HIRE_SINGLETON, lesta.structs.CrewToHireSingleton);
            createSharedSingleton(new lesta.data.DataChannel("GameInfoHolder.setExteriorConfigSingleton", EXTERIOR_CONFIG_SINGLETON), EXTERIOR_CONFIG_SINGLETON, lesta.structs.ExteriorConfigSingleton);
            createSharedCollection(new lesta.data.DataChannel("GameInfoHolder.setExteriorStorage", EXTERIOR_STORAGE), EXTERIOR_STORAGE, lesta.structs.ExteriorStorageItem);
            collectionCapturePoints = createSharedCollection([new lesta.data.DataChannel("GameInfoHolder.initCapturePoints", INIT_CAPTURE_POINTS), new lesta.data.DataChannel("GameInfoHolder.updateCapturePoints", UPDATE_CAPTURE_POINTS), new lesta.data.DataChannel("GameInfoHolder.updateCapturePointsScreenInfo", CAPTURE_POINTS_SCREEN_INFO)], CAPTURE_POINTS, lesta.structs.CapturePointInfo);
            this.listCapturePoints = collectionCapturePoints.getList();
            this.collectionModernization = createSharedCollection("GameInfoHolder.initModernization", MODERNIZATION_INFO, lesta.structs.ModernizationInfo, function (arg1:Object):Object
            {
                return arg1.modId;
            })
            this.collectionWeapons = createSharedCollection("GameInfoHolder.initWeapons", WEAPONS_INFO, lesta.structs.WeaponsInfo);
            this.weaponsByCommandId = new lesta.data.EntityCollectionMapView(null, function (arg1:lesta.structs.WeaponsInfo):Object
            {
                return arg1.commandId;
            }, this.collectionWeapons).getMap()
            this.collectionConsumables = createSharedCollection("GameInfoHolder.initConsumes", CONSUME_INFO, lesta.structs.ConsumablesInfo);
            this.collectionModernization.clearOnUpdate = true;
            this.collectionWeapons.clearOnUpdate = true;
            this.collectionConsumables.clearOnUpdate = true;
            collectionGuns = createSharedCollection("GameInfoHolder.updateGunState", GUN_STATES_INFO, lesta.structs.GunStateInfo, function (arg1:Object):Object
            {
                return arg1.weaponType * 1000 + arg1.gunID;
            })
            this.collectionActiveQuests = new scaleform.clik.data.DataProvider();
            this.collectionDailyQuests = createSharedCollection([new lesta.data.DataChannel("GameInfoHolder.initDailyQuestsInfo", DAILY_QUESTS_INFO, this.postUpdateQuestInfo), new lesta.data.DataChannel("GameInfoHolder.updateDailyQuestsTime", DAILY_QUESTS_TIME), new lesta.data.DataChannel("GameInfoHolder.clearDailyQuestsInfo", DAILY_QUESTS_CLEAR, this.clearQuestInfo)], DAILY_QUESTS, lesta.structs.DailyQuestInfo);
            this.serverState = createSharedSingleton([new lesta.data.DataChannel("GameInfoHolder.updateHasFreePreBattles", HAS_FREE_PREBATTLES)], SERVER_STATE, lesta.structs.ServerState).getData() as lesta.structs.ServerState;
            this.accountResourceInfo = createSharedSingleton([new lesta.data.DataChannel("dock.updateCreditsBalance", CREDITS_UPDATE), new lesta.data.DataChannel("dock.updateGold", GOLD_UPDATE), new lesta.data.DataChannel("dock.updateFreeExp", FREE_EXP_UPDATE), new lesta.data.DataChannel("dock.updateSlotsNum", SLOTS_NUM_UPDATE), new lesta.data.DataChannel("GameInfoHolder.updatePremiumTime", PREMIUM_TIME_UPDATE)], ACCOUNT_RESOURCE_INFO, lesta.structs.AccountResourceInfo) as lesta.structs.AccountResourceInfo;
            this.collectionPremiumAccounts = createSharedCollection("GameInfoHolder.updateAccountPremiums", PREMIUM_ACCOUNTS, Object);
            createSharedCollection("GameInfoHolder.pricesUpdate", PRICES_COLLECTION, lesta.structs.PriceInfoSet);
            this.modulesCollection = createSharedCollection("ResearchShipsTreeController.updateModulesData", MODULES_COLLECTION, lesta.structs.UpgradableShipsModuleInfo);
            this.modulesCollection.clearOnUpdate = true;
            createSharedCollection("ResearchShipsTreeController.updateShipsData", UPGRADABLE_SHIPS_COLLECTION, lesta.structs.UpgradableShipInfo);
            createSharedSingleton("ResearchShipsTreeController.sellShipDataUpdate", SELL_SHIP_DATA, lesta.structs.SellShipData);
            this.battleWindowInfo = createSharedSingleton("MarkersController.updateMarkersSharedData", MARKERS_SHARED_DATA, Object);
            lesta.data.GameDelegate.addCallBack("GameInfoHolder.clearBattleInfo", this, this.clearBattleInfo);
            this.collectionAchievements = createSharedCollection(new lesta.data.DataChannel("GameInfoHolder.initAchievements", ACHIEVEMENTS_COLLECTION, this.postUpdateAchievements), ACHIEVEMENTS_COLLECTION, lesta.structs.AchievementInfo);
            this.collectionRewards = createSharedCollection([new lesta.data.DataChannel("GameInfoHolder.initRewards", REWARDS_INIT), new lesta.data.DataChannel("GameInfoHolder.updateRewards", REWARDS_UPDATE)], REWARDS_COLLECTION, lesta.structs.RewardInfo);
            this.evaluationLimits = createSharedSingleton("GameInfoHolder.updateEvaluationLimits", EVALUATION_LIMITS, lesta.structs.EvaluationLimitsInfo).getData() as lesta.structs.EvaluationLimitsInfo;
            createSharedSingleton("GameInfoHolder.updateSysMessageIndex", SYSTEM_MESSAGES_INDEX, Object);
            createSharedCollection([new lesta.data.DataChannel("GameInfoHolder.updateSystemMessagesList", SYSTEM_MESSAGES)], SYSTEM_MESSAGES, lesta.structs.SystemLogMessageInfo);
            lesta.data.GameDelegate.addCallBack("GameInfoHolder.clearSession", this, this.clearAccount);
            createSharedSingleton("GameInfoHolder.initBuildInfo", BUILD_INFO, lesta.structs.BuildInfo);
            this.initPlanesDataExchange();
            this.initPlayersDataExchange();
            this.initNavpointsDataExchange();
            this.initTorpedoesDataExchange();
            return;
        }

        internal function initServerTime(arg1:Number):void
        {
            this.initialServerTime = arg1;
            this.serverTimeDelta = arg1 - new Date().time / 1000;
            return;
        }

        internal function clearPlayerShips(arg1:Array):void
        {
            this.collectionPlayersShips.clear();
            return;
        }

        internal function postUpdatePlayerShips(arg1:Array, arg2:*):void
        {
            var loc1:*=this.collectionPlayersShips.getList();
            var loc2:*=0;
            while (loc2 < loc1.length) 
            {
                loc1[loc2].updateShipInfoReference();
                ++loc2;
            }
            loc1.sortOn(SHIP_LIST_SORT_FIELDS, SHIP_LIST_SORT_PROPS);
            loc1.invalidate();
            return;
        }

        internal function postUpdateBotShips(arg1:Array, arg2:*):void
        {
            var loc1:*=this.collectionBotShips.getList();
            var loc2:*=0;
            while (loc2 < loc1.length) 
            {
                loc1[loc2].updateShipInfoReference();
                ++loc2;
            }
            loc1.invalidate();
            return;
        }

        internal function postUpdateQuestInfo(arg1:Array):void
        {
            var loc2:*=null;
            this.collectionActiveQuests.cleanUp();
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                loc2 = arg1[loc1];
                if (loc2.isActive) 
                {
                    this.collectionActiveQuests.push(loc2);
                }
                ++loc1;
            }
            return;
        }

        internal function clearQuestInfo(arg1:Array):void
        {
            this.collectionDailyQuests.clear();
            return;
        }

        internal function postUpdateAchievements(arg1:Array):void
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                loc2 = arg1[loc1];
                loc2.update();
                ++loc1;
            }
            return;
        }

        internal function fillDataProviders(arg1:lesta.structs.Player):void
        {
            if (arg1.relation != lesta.constants.PlayerRelation.ENEMY) 
            {
                if (arg1.shipTypeIdent != lesta.constants.ShipTypes.Auxiliary) 
                {
                    this.listAliedPlayersDeadFirstNotConvoy.push(arg1);
                }
                else 
                {
                    this.listAliedPlayersDeadFirstConvoy.push(arg1);
                }
                if (this.selfPlayer.division > 0 && this.selfPlayer.division == arg1.division) 
                {
                    arg1.isInSameDivision = true;
                }
            }
            else if (arg1.shipTypeIdent != lesta.constants.ShipTypes.Auxiliary) 
            {
                this.listEnemyPlayersDeadFirstNotConvoy.push(arg1);
            }
            else 
            {
                this.listEnemyPlayersDeadFirstConvoy.push(arg1);
            }
            return;
        }

        internal function postUpdatePlayersInfoAction(arg1:Array):void
        {
            var loc3:*=null;
            this.setOnlyUpdatedObjectsToBeVisible(this.listPlayers, arg1);
            this.mapTargets = [];
            var loc1:*=0;
            var loc2:*=arg1.length;
            while (loc1 < loc2) 
            {
                if ((loc3 = arg1[loc1]).isMainGunFocusTarget) 
                {
                    this.mapTargets[lesta.constants.WeaponType.ARTILLERY] = loc3;
                }
                if (loc3.isATBAPriorTarget) 
                {
                    this.mapTargets[lesta.constants.WeaponType.ATBA] = loc3;
                }
                ++loc1;
            }
            this.sortPlayersList();
            this.listAliedPlayers.invalidate();
            this.listEnemyPlayers.invalidate();
            return;
        }

        internal function postUpdatePlayersStatesAction(arg1:Array):void
        {
            var dirtyObjects:Array;
            var listKilled:__AS3__.vec.Vector.<lesta.structs.Player>;
            var i:int;
            var l:int;
            var player:lesta.structs.Player;
            var listPlayersLostTarget:__AS3__.vec.Vector.<lesta.structs.Player>;
            var j:int;

            var loc1:*;
            player = null;
            listPlayersLostTarget = null;
            j = 0;
            dirtyObjects = arg1;
            this.observerdPlayer = collections[PLAYERS_IN_BATTLE].getFirstMemberSuitableTo(function (arg1:lesta.structs.Player):Boolean
            {
                return arg1.isObserved;
            }) as lesta.structs.Player
            listKilled = new Vector.<lesta.structs.Player>();
            i = 0;
            l = dirtyObjects.length;
            while (i < l) 
            {
                player = dirtyObjects[i];
                if (!player.isAlive && !(player.id in this.alreadyKilledShips)) 
                {
                    this.alreadyKilledShips[player.id] = true;
                    listKilled.push(player);
                }
                ++i;
            }
            if (listKilled.length > 0) 
            {
                this.sortPlayersList();
                listPlayersLostTarget = new Vector.<lesta.structs.Player>();
                j = 0;
                while (j < this.listPlayers.length) 
                {
                    player = this.listPlayers[j];
                    if (listKilled.indexOf(player.currentAttackedTarget) != -1) 
                    {
                        listPlayersLostTarget.push(player);
                    }
                    ++j;
                }
                if (listPlayersLostTarget.length > 0) 
                {
                    invokeCallback(PLAYERS_LOST_TARGET, [listPlayersLostTarget]);
                }
                this.listAliedPlayers.invalidate();
                this.listEnemyPlayers.invalidate();
                this.listAliedPlayersDeadFirstNotConvoy.invalidate();
                this.listEnemyPlayersDeadFirstNotConvoy.invalidate();
                this.listAliedPlayersDeadFirstConvoy.invalidate();
                this.listEnemyPlayersDeadFirstConvoy.invalidate();
            }
            this.listAliedPlayers.invalidate();
            return;
        }

        internal function postUpdatePlayersTeamkillStatusAction(arg1:Array):void
        {
            this.listAliedPlayers.invalidate();
            this.listAliedPlayersDeadFirstNotConvoy.invalidate();
            this.listAliedPlayersDeadFirstConvoy.invalidate();
            return;
        }

        internal function sortPlayersList():void
        {
            this.listEnemyPlayers.sortOn(PLAYERS_LIST_SORT_FIELDS, PLAYERS_LIST_SORT_PROPS);
            this.listAliedPlayers.sortOn(PLAYERS_LIST_SORT_FIELDS, PLAYERS_LIST_SORT_PROPS);
            this.listAliedPlayersDeadFirstNotConvoy.sortOn(PLAYERS_LIST_SORT_FIELDS, PLAYERS_LIST_SORT_PROPS_DEAD_FIRST);
            this.listAliedPlayersDeadFirstConvoy.sortOn(PLAYERS_LIST_SORT_FIELDS, PLAYERS_LIST_SORT_PROPS);
            this.listEnemyPlayersDeadFirstNotConvoy.sortOn(PLAYERS_LIST_SORT_FIELDS, PLAYERS_LIST_SORT_PROPS_DEAD_FIRST);
            this.listEnemyPlayersDeadFirstConvoy.sortOn(PLAYERS_LIST_SORT_FIELDS, PLAYERS_LIST_SORT_PROPS_DEAD_FIRST);
            return;
        }

        internal function initPlanesDataExchange():void
        {
            var planesCollection:lesta.data.EntityCollection;
            var ourPlanesFilter:Function;
            var keyFunctionForOurPlanesMap:Function;

            var loc1:*;
            lesta.data.GameDelegate.addCallBack("GameInfoHolder.playerShipSelectionChanged", this, this.onCarrierSelectionChanged);
            planesCollection = createSharedCollection([new lesta.data.DataChannel("GameInfoHolder.initPlanesInfo", PLANES_INIT_INFO), new lesta.data.DataChannel("GameInfoHolder.updatePlanesInfo", PLANES_INFO, this.postUpdatePlaneInfoAction), new lesta.data.DataChannel("GameInfoHolder.updatePlanesMapInfo", PLANES_MAP_INFO), new lesta.data.DataChannel("GameInfoHolder.updatePlanesIconInfo", PLANES_ICON_INFO), new lesta.data.DataChannel("GameInfoHolder.updatePlanesTeamkillInfo", PLANES_TEAMKILL_INFO), new lesta.data.DataChannel("GameInfoHolder.updateCanLandEscortInfo", PLANES_CAN_LAND_ESCORT_INFO)], PLANES_IN_BATTLE, lesta.structs.PlaneInfo, function (arg1:Object):Object
            {
                return arg1.relation != lesta.constants.PlayerRelation.SELF ? arg1.id : ":" + arg1.ownPlaneId;
            })
            this.listPlanes = planesCollection.getList();
            this.mapPlanes = planesCollection.getMap();
            ourPlanesFilter = function (arg1:lesta.structs.PlaneInfo):Boolean
            {
                return arg1.relation == lesta.constants.PlayerRelation.SELF;
            }
            keyFunctionForOurPlanesMap = function (arg1:lesta.structs.PlaneInfo):Object
            {
                return arg1.ownPlaneId;
            }
            this.mapOwnPlanes = new lesta.data.EntityCollectionMapView(ourPlanesFilter, keyFunctionForOurPlanesMap, planesCollection).getMap();
            this.listOwnPlanes = new lesta.data.EntityCollectionListView(ourPlanesFilter, planesCollection).getList();
            return;
        }

        internal function postUpdatePlaneInfoAction(arg1:Array):void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc1:*=0;
            var loc2:*=arg1.length;
            while (loc1 < loc2) 
            {
                loc3 = new flash.events.Event(flash.events.Event.CHANGE);
                (loc4 = arg1[loc1]).dispatchEvent(loc3);
                ++loc1;
            }
            return;
        }

        internal function onCarrierSelectionChanged(arg1:Boolean):void
        {
            this.playerShipSelected = arg1;
            this.selfPlayer.isSelected = arg1;
            invokeCallback(PLAYER_SHIP_SELECTION, [arg1]);
            return;
        }

        public function setSelectedPlanes(arg1:Array):void
        {
            this.listSelectedPlanes.length = 0;
            this.listSelectedPlanes.push.apply(null, arg1);
            invokeCallback(SELECTED_PLANES);
            return;
        }

        public function clearAccount():void
        {
            objects[BATTLE_STATS].clear();
            objects[PLAYER_PROFILE].clear();
            objects[DOCK_DEFAULT_VALUES].clear();
            objects[ACCOUNT_RESOURCE_INFO].clear();
            objects[SYSTEM_MESSAGES_INDEX].clear();
            collections[PLAYER_SHIPS].clear();
            collections[PRICES_COLLECTION].clear();
            collections[MODULES_COLLECTION].clear();
            collections[UPGRADABLE_SHIPS_COLLECTION].clear();
            collections[EXTERIOR_STORAGE].clear();
            collections[SYSTEM_MESSAGES].clear();
            return;
        }

        internal function initTorpedoesDataExchange():void
        {
            var loc1:*=new lesta.data.DataChannel("GameInfoHolder.setMyTorpedoesInfo", TORPEDOES_INFO, this.postSetMyTorpedoesInfoAction);
            createSharedCollection(loc1, TORPEDOES_IN_BATTLE, lesta.structs.EntityInfo);
            return;
        }

        internal function postSetMyTorpedoesInfoAction(arg1:Array):void
        {
            this.listTorpedoes.length = 0;
            this.listTorpedoes.push.apply(null, arg1);
            return;
        }

        
        {
            instance = new GameInfoHolder();
            SHIP_LIST_SORT_FIELDS = ["country", "level", "stypeIdent"];
            SHIP_LIST_SORT_PROPS = [Array.DESCENDING, Array.NUMERIC, Array.DESCENDING];
            PLAYERS_LIST_SORT_FIELDS = [lesta.structs.EntityInfo.IS_ALIVE, lesta.structs.Player.SHIP_TYPE_SORT_INDEX, lesta.structs.Player.SHIP_LEVEL, lesta.structs.Player.SHIP_NAME, lesta.structs.Player.NAME];
            PLAYERS_LIST_SORT_PROPS = [Array.NUMERIC | Array.DESCENDING, Array.NUMERIC, Array.NUMERIC | Array.DESCENDING, 0, 0];
            PLAYERS_LIST_SORT_PROPS_DEAD_FIRST = [Array.NUMERIC, Array.NUMERIC, Array.NUMERIC | Array.DESCENDING, 0, 0];
        }

        internal function setOnlyUpdatedObjectsToBeVisible(arg1:Array, arg2:Array):void
        {
            var loc1:*=0;
            var loc2:*=arg1.length;
            while (loc1 < loc2) 
            {
                arg1[loc1].isVisible = false;
                ++loc1;
            }
            loc1 = 0;
            loc2 = arg2.length;
            while (loc1 < loc2) 
            {
                arg2[loc1].isVisible = true;
                ++loc1;
            }
            return;
        }

        public function clearBattleInfo():void
        {
            collections[PLAYERS_IN_BATTLE].clear();
            lesta.structs.Player.LAST_PLAYER_ENTITY_ID = 0;
            collections[PLANES_IN_BATTLE].clear();
            lesta.structs.PlaneInfo.LAST_PLANE_ENTITY_ID = 0;
            collections[SQUADRON_ORDER_LISTS].clear();
            collections[SQUADRON_ORDERS_INFO].clear();
            collections[GUN_STATES_INFO].clear();
            collections[CAPTURE_POINTS].clear();
            this.listSelectedPlanes.length = 0;
            this.listEnemyPlayersDeadFirstConvoy.length = 0;
            this.listEnemyPlayersDeadFirstNotConvoy.length = 0;
            this.listAliedPlayersDeadFirstConvoy.length = 0;
            this.listAliedPlayersDeadFirstNotConvoy.length = 0;
            this.selfPlayer = null;
            var loc1:*=getObject(lesta.utils.GameInfoHolder.GAME_INFO) as lesta.structs.GameInfo;
            loc1.clear();
            var loc2:*=getObject(lesta.utils.GameInfoHolder.BATTLE_MODE_INFO) as lesta.structs.BattleModeInfo;
            loc2.clear();
            return;
        }

        internal function initNavpointsDataExchange():void
        {
            this.navpointsSettings = createSharedSingleton("GameInfoHolder.initNavpointsSettings", NAVPOINT_SETTINGS, lesta.structs.NavpointsSettings).getData() as lesta.structs.NavpointsSettings;
            lesta.data.GameDelegate.addCallBack("GameInfoHolder.setSelectedPlanes", this, this.setSelectedPlanes);
            var loc1:*=createSharedCollection([new lesta.data.DataChannel("GameInfoHolder.updateSquadronOrderLists", null, this.postReceiveUpdateSquadronOrderLists), new lesta.data.DataChannel("GameInfoHolder.updateAutopilotOrderLists", UPDATE_AUTOPILOT_WAYPOINTS, this.postReceiveUpdateAutopilotOrderLists)], SQUADRON_ORDER_LISTS, lesta.structs.NavpointList);
            var loc2:*=createSharedCollection([new lesta.data.DataChannel("GameInfoHolder.updateSquadronOrdersInfo", SQUADRON_ORDERS_INFO, this.postUpdateSquadronOrdersInfo), new lesta.data.DataChannel("GameInfoHolder.updateSquadronDuplicates", SQUADRON_DUPLICATE_INFO, null), new lesta.data.DataChannel("GameInfoHolder.updateSquadronOrdersPositions", UPDATE_SQUADRON_ORDER_POSITIONS, null), new lesta.data.DataChannel("GameInfoHolder.updateAutopilotWaypoints", UPDATE_AUTOPILOT_WAYPOINTS, this.postUpdateWayPointsOrdersInfo), new lesta.data.DataChannel("GameInfoHolder.updateAutopilotWaypointsPositions", UPDATE_AUTOPILOT_WAYPOINTS_POSITIONS, null)], SQUADRON_ORDERS_INFO, lesta.structs.NavpointInfo);
            this.mapNavpointListsBySquadron = loc1.getMap();
            this.listNavpointLists = loc1.getList();
            this.mapNavpoints = loc2.getMap();
            this.mapNavpointsByFullId = new flash.utils.Dictionary();
            this.mapWaypointsByFullId = new flash.utils.Dictionary();
            return;
        }

        internal function postReceiveUpdateSquadronOrderLists(arg1:Array):void
        {
            var loc3:*=null;
            var loc1:*=0;
            var loc2:*=arg1.length;
            while (loc1 < loc2) 
            {
                (loc3 = arg1[loc1]).updateNavpointReferences();
                invokeCallback(UPDATE_SQUADRON_ORDER_LISTS, [loc3.id]);
                ++loc1;
            }
            return;
        }

        internal function postUpdateSquadronOrdersInfo(arg1:Array):void
        {
            var loc2:*=null;
            this.mapNavpointsByFullId = new flash.utils.Dictionary();
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                loc2 = arg1[loc1];
                loc2.updateAlphaColor();
                this.mapNavpointsByFullId[loc2.fullId] = loc2;
                ++loc1;
            }
            return;
        }

        internal function postUpdateWayPointsOrdersInfo(arg1:Array):void
        {
            var loc2:*=null;
            this.mapWaypointsByFullId = new flash.utils.Dictionary();
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                loc2 = arg1[loc1];
                loc2.updateAlphaColor();
                this.mapWaypointsByFullId[loc2.fullId] = loc2;
                ++loc1;
            }
            return;
        }

        internal function postReceiveUpdateAutopilotOrderLists(arg1:Array):void
        {
            arg1[0].updateNavpointReferences();
            return;
        }

        internal function initPlayersDataExchange():void
        {
            var playersInBattleCollection:lesta.data.EntityCollection;
            var isEnemy:Function;
            var isNotEnemy:Function;

            var loc1:*;
            playersInBattleCollection = createSharedCollection([new lesta.data.DataChannel("GameInfoHolder.initPlayersList", LIST_PLAYERS, this.postInitPlayersActions), new lesta.data.DataChannel("GameInfoHolder.updatePlayersInfo", PLAYERS_INFO, this.postUpdatePlayersInfoAction), new lesta.data.DataChannel("GameInfoHolder.updatePlayersMaxHealth", PLAYERS_DRAWER), new lesta.data.DataChannel("GameInfoHolder.updatePlayersDrawer", PLAYERS_DRAWER), new lesta.data.DataChannel("GameInfoHolder.updatePlayersStates", PLAYERS_STATE, this.postUpdatePlayersStatesAction), new lesta.data.DataChannel("GameInfoHolder.updatePlayersTeamkillStatus", PLAYERS_STATE, this.postUpdatePlayersTeamkillStatusAction), new lesta.data.DataChannel("GameInfoHolder.updateSelfPlayerInfo", SELF_PLAYER_INFO)], PLAYERS_IN_BATTLE, lesta.structs.Player);
            this.listPlayers = playersInBattleCollection.getList();
            this.mapPlayers = playersInBattleCollection.getMap();
            isEnemy = function (arg1:lesta.structs.Player):Boolean
            {
                return arg1.relation == lesta.constants.PlayerRelation.ENEMY;
            }
            isNotEnemy = function (arg1:lesta.structs.Player):Boolean
            {
                return !(arg1.relation == lesta.constants.PlayerRelation.ENEMY);
            }
            this.listEnemyPlayers = new lesta.data.EntityCollectionListView(isEnemy, playersInBattleCollection).getList();
            this.listAliedPlayers = new lesta.data.EntityCollectionListView(isNotEnemy, playersInBattleCollection).getList();
            return;
        }

        internal function postInitPlayersActions(arg1:Array):void
        {
            var dirtyObjects:Array;
            var i:int;
            var player:lesta.structs.Player;

            var loc1:*;
            player = null;
            dirtyObjects = arg1;
            this.selfPlayer = collections[PLAYERS_IN_BATTLE].getFirstMemberSuitableTo(function (arg1:lesta.structs.Player):Boolean
            {
                return arg1.relation == lesta.constants.PlayerRelation.SELF;
            }) as lesta.structs.Player
            this.listEnemyPlayersDeadFirstConvoy.cleanUp();
            this.listEnemyPlayersDeadFirstNotConvoy.cleanUp();
            this.listAliedPlayersDeadFirstConvoy.cleanUp();
            this.listAliedPlayersDeadFirstNotConvoy.cleanUp();
            i = 0;
            while (i < dirtyObjects.length) 
            {
                player = dirtyObjects[i];
                player.updateShipInfoData();
                this.fillDataProviders(player);
                ++i;
            }
            this.sortPlayersList();
            this.listEnemyPlayers.invalidate();
            this.listAliedPlayers.invalidate();
            this.listAliedPlayersDeadFirstNotConvoy.invalidate();
            this.listEnemyPlayersDeadFirstNotConvoy.invalidate();
            this.listAliedPlayersDeadFirstConvoy.invalidate();
            this.listEnemyPlayersDeadFirstConvoy.invalidate();
            invokeCallback(CAMERA);
            return;
        }

        public function fini():void
        {
            this.shipListCollection.clear();
            lesta.data.GameDelegate.removeCallBack(this);
            return;
        }

        public static const PLANES_INIT_INFO:String="PlanesInitInfo";

        public static const PLANES_INFO:String="PlanesInfo";

        public static const PLANES_MAP_INFO:String="PlanesMapInfo";

        public static const PLANES_ICON_INFO:String="PlanesIconInfo";

        public static const PLANES_TEAMKILL_INFO:String="PlanesTeamkillInfo";

        public static const PLANES_CAN_LAND_ESCORT_INFO:String="CanLandEscortInfo";

        public static const SELECTED_PLANES:String="selectedPlanes";

        public static const TORPEDOES_INFO:String="torpedoesInfo";

        public static const PLAYER_SHIP_SELECTION:String="playerShipSelected";

        public static const MODULES_COLLECTION:String="modulesCollection";

        public static const UPGRADABLE_SHIPS_COLLECTION:String="upgradableShipsCollection";

        public static const PREMIUM_ACCOUNTS:String="premiumAccounts";

        public static const ACCOUNT_RESOURCE_INFO:String="accountResourceInfo";

        public static const GOLD_UPDATE:String="goldUpdate";

        public static const CREDITS_UPDATE:String="creditsUpdate";

        public static const SLOTS_NUM_UPDATE:String="slotsNumUpdate";

        public static const PREMIUM_TIME_UPDATE:String="premiumUpdate";

        public static const UPDATE_SQUADRON_ORDER_LISTS:String="updatedSquadronOrders";

        public static const UPDATE_SQUADRON_ORDER_POSITIONS:String="updatedSquadronOrdersPosition";

        public static const UPDATE_AUTOPILOT_WAYPOINTS:String="updateAutopilotWaypoints";

        public static const UPDATE_AUTOPILOT_WAYPOINTS_POSITIONS:String="updateAutopilotWaypointsPositions";

        public static const CAMERA:String="camera";

        public static const GAME_INFO:String="gameInfo";

        public static const MAX_PLAYERS:String="maxPlayers";

        public static const BATTLE_MODE_INFO:String="battleModeInfo";

        public static const UPDATE_BATTLE_MODE_INFO:String="updateBattleModeInfo";

        public static const UPDATE_BATTLE_TIME:String="updateBattleTime";

        public static const PLAYERS_LOST_TARGET:String="playersLostTarget";

        public static const DAILY_QUESTS:String="dailyc";

        public static const DAILY_QUESTS_INFO:String="dailycInfo";

        public static const DAILY_QUESTS_TIME:String="dailycTime";

        public static const DAILY_QUESTS_CLEAR:String="dailycClear";

        public static const BATTLE_TASKS:String="battleTasks";

        public static const CREWS_ALL:String="crewsAll";

        public static const SERVER_STATE:String="serverState";

        public static const HAS_FREE_PREBATTLES:String="hasFreeRooms";

        public static const CREW_SKILLS_ALL:String="crewsSkillsAll";

        public static const CREW_TO_HIRE_SINGLETON:String="crewToHireSingleton";

        public static const EXTERIOR_CONFIG_SINGLETON:String="exteriorConfigSingleton";

        public static const EXTERIOR_STORAGE:String="exteriorStorage";

        public static const PRICES_COLLECTION:String="pricesAll";

        public static const SELL_SHIP_DATA:String="sellShipData";

        public static const ACHIEVEMENTS_COLLECTION:String="achievements";

        public static const REWARDS_COLLECTION:String="rewards";

        public static const REWARDS_INIT:String="rewardsInit";

        public static const REWARDS_UPDATE:String="rewardsUpdate";

        public static const REWARD_DATA:String="rewardData";

        public static const REWARD_INIT_DATA:String="rewardInitData";

        public static const REWARD_UPDATE_DATA:String="rewardUpdateData";

        public static const EVALUATION_LIMITS:String="evaluationLimits";

        public static const MARKERS_SHARED_DATA:String="markersSharedData";

        public static const SYSTEM_MESSAGES:String="systemLogMessages";

        public static const SYSTEM_MESSAGES_INDEX:String="systemMessageIndex";

        public static const BUILD_INFO:String="buildInfo";

        public static const CREWS_SINGLETON:String="crewsSingleton";

        public static const PLAYER_PROFILE:String="playerProfile";

        public static const DOCK_DEFAULT_VALUES:String="dockDefaultValues";

        public static const DOCK_INIT_DATA:String="dockInitData";

        public static const DOCK_UPDATE_DATA:String="dockUpdateData";

        public static const CONVERT_EXP_DATA:String="ModalWindowConvertExp.initData";

        public static const MAPS:String="maps";

        public static const SHIPS:String="ships";

        public static const BATTLE_STATS:String="battle_stats";

        public static const PLAYER_SHIPS:String="playerShips";

        public static const BOTS_SHIPS:String="botsShips";

        public static const TYPE:String="type";

        public static const FREE_EXP_UPDATE:String="freeExpUpdate";

        public static const TIME:String="time";

        public static const NAVPOINT_SETTINGS:String="navpoint settings";

        public static const PLAYERS_IN_BATTLE:String="players in battle";

        public static const PLANES_IN_BATTLE:String="planes in battle";

        public static const TORPEDOES_IN_BATTLE:String="torpedoes in battle";

        public static const BALANCER_STATS:String="balancer_stats";

        public static const BALANCER_COMMON:String="balancer_common";

        public static const SQUADRON_ORDER_LISTS:String="squadronOrders";

        public static const SQUADRON_ORDERS_INFO:String="squadronOrdersInfo";

        public static const SQUADRON_DUPLICATE_INFO:String="squadronDuplicateInfo";

        public static const INIT_CAPTURE_POINTS:String="initCapturePoints";

        public static const UPDATE_CAPTURE_POINTS:String="updateCapturePoints";

        public static const CAPTURE_POINTS:String="capturePoints";

        public static const CAPTURE_POINTS_SCREEN_INFO:String="capturePointsScreenInfo";

        public static const ACCOUNT_LEVEL_INFO:String="accountLevelInfo";

        public static const MODERNIZATION_INFO:String="modernizationInfo";

        public static const WEAPONS_INFO:String="weaponsInfo";

        public static const CONSUME_INFO:String="consumeInfo";

        public static const DIVISION_ENTRANCE_DATA:String="divisionEntranceData";

        public static const DIVISION_ENTRANCE_INIT_DATA:String="divisionEntranceInitData";

        public static const DIVISION_ENTRANCE_RECEIVE_SEEKERS:String="divisionEntranceReceiveSeekers";

        public static const DIVISION_ENTRANCE_RECEIVE_SEARCH_RESULT:String="DivisionEntrance.ReceiveSearchResult";

        public static const DIVISION_ENTRANCE_UPDATE_SEEKER_STATUS:String="divisionEntranceUpdateSeekerStatus";

        public static const INVITATIONS_TO_DIVISION:String="invitationsToDivision";

        public static const PLAYERS_IN_DIVISION:String="playersInDivision";

        public static const PLAYERS_INVITED_TO_DIVISION:String="playersInvitedToDivision";

        public static const DIVISION_DATA:String="divisionData";

        public static const OPTION_DATA:String="optionData";

        public static const INIT_GAME_INFO:String="initGameInfo";

        public static const GAME_INFO_UPDATE_FRAGS:String="GameInfoUpdateFragsCount";

        public static const LIST_PLAYERS:String="listPlayers";

        public static const GUN_STATES_INFO:String="gunStatesInfo";

        public static const PLAYERS_INFO:String="PlayersInfo";

        public static const SELF_PLAYER_INFO:String="SelfPlayersInfo";

        public static const PLAYERS_DRAWER:String="PlayersDrawer";

        public static const PLAYERS_STATE:String="PlayersState";

        public var initialServerTime:Number;

        public var serverTimeDelta:Number;

        public var collectionAllCrews:lesta.data.EntityCollection;

        public var collectionAllCrewSkills:lesta.data.EntityCollection;

        public var collectionPlayersShips:lesta.data.EntityCollection;

        public var collectionBotShips:lesta.data.EntityCollection;

        public var collectionActiveQuests:scaleform.clik.data.DataProvider;

        public var collectionModernization:lesta.data.EntityCollection;

        public var collectionWeapons:lesta.data.EntityCollection;

        public var weaponsByCommandId:flash.utils.Dictionary;

        public var collectionConsumables:lesta.data.EntityCollection;

        public var collectionPremiumAccounts:lesta.data.EntityCollection;

        public var collectionAchievements:lesta.data.EntityCollection;

        public var collectionRewards:lesta.data.EntityCollection;

        public var gameInfo:lesta.structs.GameInfo;

        public var cameraInfo:lesta.structs.CameraInfo;

        public var serverState:lesta.structs.ServerState;

        public var battleModeInfo:lesta.structs.BattleModeInfo;

        public var accountLevelInfo:lesta.structs.AccountLevelInfo;

        public var crewsSingleton:lesta.structs.CrewsSingleton;

        public var crewToHireSingleton:lesta.structs.CrewToHireSingleton;

        public var accountResourceInfo:lesta.structs.AccountResourceInfo;

        public var collectionDailyQuests:lesta.data.EntityCollection;

        public var stockModules:flash.utils.Dictionary;

        public var installedModules:flash.utils.Dictionary;

        public var battleWindowInfo:lesta.data.EntitySingleton;

        public var evaluationLimits:lesta.structs.EvaluationLimitsInfo;

        public var battleTasksCollection:lesta.data.EntityCollection;

        public var modulesCollection:lesta.data.EntityCollection;

        public var mapOwnPlanes:flash.utils.Dictionary;

        public var listOwnPlanes:scaleform.clik.data.DataProvider;

        public var listPlanes:scaleform.clik.data.DataProvider;

        public var mapPlanes:flash.utils.Dictionary;

        public var playerShipSelected:Boolean=false;

        public var listPlayers:scaleform.clik.data.DataProvider;

        public var mapPlayers:flash.utils.Dictionary;

        public var listEnemyPlayers:scaleform.clik.data.DataProvider;

        public var listAliedPlayers:scaleform.clik.data.DataProvider;

        public var listEnemyPlayersDeadFirstNotConvoy:scaleform.clik.data.DataProvider;

        public var listAliedPlayersDeadFirstNotConvoy:scaleform.clik.data.DataProvider;

        public var listEnemyPlayersDeadFirstConvoy:scaleform.clik.data.DataProvider;

        public var listAliedPlayersDeadFirstConvoy:scaleform.clik.data.DataProvider;

        public var selfPlayer:lesta.structs.Player;

        public var observerdPlayer:lesta.structs.Player;

        public var mapTargets:Array;

        internal var alreadyKilledShips:flash.utils.Dictionary;

        public var listSelectedPlanes:Array;

        public var navpointsSettings:lesta.structs.NavpointsSettings;

        public var mapNavpointListsBySquadron:flash.utils.Dictionary;

        protected var listNavpointLists:Array;

        public var mapNavpoints:flash.utils.Dictionary;

        public var mapNavpointsByFullId:flash.utils.Dictionary;

        public var mapWaypointsByFullId:flash.utils.Dictionary;

        public var listTorpedoes:Array;

        public var listCapturePoints:scaleform.clik.data.DataProvider;

        public static var instance:lesta.utils.GameInfoHolder;

        public static var SHIP_LIST_SORT_FIELDS:Array;

        public static var SHIP_LIST_SORT_PROPS:Array;

        public static var PLAYERS_LIST_SORT_FIELDS:Array;

        public static var PLAYERS_LIST_SORT_PROPS:Array;

        public static var PLAYERS_LIST_SORT_PROPS_DEAD_FIRST:Array;

        public var shipListCollection:lesta.data.EntityCollection;
    }
}
