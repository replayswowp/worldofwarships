package lesta.utils 
{
    import flash.geom.*;
    
    public class Geometry extends Object
    {
        public function Geometry()
        {
            super();
            return;
        }

        public static function normAngle(arg1:Number, arg2:Number):Number
        {
            if (arg1 > arg2 + Math.PI * 2) 
            {
                arg1 = arg1 - Math.PI * 2;
            }
            if (arg1 < arg2) 
            {
                arg1 = arg1 + Math.PI * 2;
            }
            return arg1;
        }

        public static function clamp(arg1:Number, arg2:Number, arg3:Number):Number
        {
            if (arg1 < arg2) 
            {
                arg1 = arg2;
            }
            if (arg1 > arg3) 
            {
                arg1 = arg3;
            }
            return arg1;
        }

        public static function getLinesIntersection(arg1:flash.geom.Point, arg2:flash.geom.Point, arg3:flash.geom.Point, arg4:flash.geom.Point, arg5:flash.geom.Point):Boolean
        {
            var loc1:*=arg2.y - arg1.y;
            var loc2:*=arg1.x - arg2.x;
            var loc3:*=(-loc1) * arg1.x - loc2 * arg1.y;
            var loc4:*=arg4.y - arg3.y;
            var loc5:*=arg3.x - arg4.x;
            var loc6:*=(-loc4) * arg3.x - loc5 * arg3.y;
            var loc7:*=loc4 * arg1.x + loc5 * arg1.y + loc6;
            var loc8:*=loc4 * arg2.x + loc5 * arg2.y + loc6;
            var loc9:*=loc1 * arg3.x + loc2 * arg3.y + loc3;
            var loc10:*=loc1 * arg4.x + loc2 * arg4.y + loc3;
            if (loc7 * loc8 >= 0 || loc9 * loc10 >= 0) 
            {
                return false;
            }
            var loc11:*=loc7 / (loc7 - loc8);
            arg5.x = arg1.x + (arg2.x - arg1.x) * loc11;
            arg5.y = arg1.y + (arg2.y - arg1.y) * loc11;
            return true;
        }

        
        {
            DEG_TO_RAD_COEF = 180 / Math.PI;
        }

        public static var DEG_TO_RAD_COEF:Number=57.2957795131;
    }
}
