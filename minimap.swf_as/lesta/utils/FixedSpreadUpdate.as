package lesta.utils 
{
    import __AS3__.vec.*;
    
    public class FixedSpreadUpdate extends Object
    {
        public function FixedSpreadUpdate(arg1:int, arg2:lesta.utils.IUpdater)
        {
            super();
            this.frameRate = arg1;
            this.updater = arg2;
            this.listHandlers = new Vector.<SpreadHandler>(this.maxHandlerDeath + 1, true);
            this.listHandlers[0] = new SpreadHandler();
            this.listHandlers[1] = new SpreadHandler1();
            this.listHandlers[2] = new SpreadHandler2();
            this.listHandlers[3] = new SpreadHandler3();
            this.listHandlers[4] = new SpreadHandler4();
            this.listHandlers[5] = new SpreadHandler5();
            this.handler = this.listHandlers[this.curHandlerDepth];
            return;
        }

        public function init(arg1:Array):void
        {
            this.listObjects = arg1;
            var loc1:*=0;
            var loc2:*=this.listHandlers.length;
            while (loc1 < loc2) 
            {
                this.listHandlers[loc1].init(arg1, this.updater);
                ++loc1;
            }
            this.updateDepth();
            return;
        }

        public function updateDepth():void
        {
            this.countObjects = this.listObjects.length;
            var loc1:*=this.countObjects > 0 ? int(this.countObjects / this.frameRate) + 1 : 0;
            this.setDepth(loc1);
            return;
        }

        public function setDepth(arg1:int):void
        {
            if (arg1 > this.maxHandlerDeath) 
            {
                arg1 = this.maxHandlerDeath;
            }
            if (this.curHandlerDepth == arg1) 
            {
                return;
            }
            this.curHandlerDepth = arg1;
            this.handler = this.listHandlers[this.curHandlerDepth];
            return;
        }

        public function update():void
        {
            this.counter = (this.counter + this.curHandlerDepth) % this.countObjects;
            this.handler.handle(this.counter, this.countObjects);
            this.countObjects = this.listObjects.length;
            return;
        }

        protected var frameRate:int;

        protected var listObjects:Array=null;

        protected var func:Function=null;

        protected var countObjects:int=0;

        protected var counter:int=-1;

        protected var handler:SpreadHandler=null;

        protected var listHandlers:__AS3__.vec.Vector.<SpreadHandler>;

        protected var maxHandlerDeath:int=5;

        protected var curHandlerDepth:int=0;

        protected var updater:lesta.utils.IUpdater;
    }
}


class SpreadHandler extends Object
{
    public function SpreadHandler()
    {
        super();
        return;
    }

    public function init(arg1:Array, arg2:lesta.utils.IUpdater):void
    {
        this.objects = arg1;
        this.updater = arg2;
        return;
    }

    public function handle(arg1:int, arg2:int):void
    {
        return;
    }

    protected var updater:lesta.utils.IUpdater;

    protected var objects:Array=null;
}

class SpreadHandler1 extends SpreadHandler
{
    public function SpreadHandler1()
    {
        super();
        return;
    }

    public override function handle(arg1:int, arg2:int):void
    {
        updater.update(objects[arg1]);
        return;
    }
}

class SpreadHandler2 extends SpreadHandler
{
    public function SpreadHandler2()
    {
        super();
        return;
    }

    public override function handle(arg1:int, arg2:int):void
    {
        updater.update(objects[arg1]);
        updater.update(objects[(arg1 + 1) % arg2]);
        return;
    }
}

class SpreadHandler3 extends SpreadHandler
{
    public function SpreadHandler3()
    {
        super();
        return;
    }

    public override function handle(arg1:int, arg2:int):void
    {
        updater.update(objects[arg1]);
        updater.update(objects[(arg1 + 1) % arg2]);
        updater.update(objects[(arg1 + 2) % arg2]);
        return;
    }
}

class SpreadHandler4 extends SpreadHandler
{
    public function SpreadHandler4()
    {
        super();
        return;
    }

    public override function handle(arg1:int, arg2:int):void
    {
        updater.update(objects[arg1]);
        updater.update(objects[(arg1 + 1) % arg2]);
        updater.update(objects[(arg1 + 2) % arg2]);
        updater.update(objects[(arg1 + 3) % arg2]);
        return;
    }
}

class SpreadHandler5 extends SpreadHandler
{
    public function SpreadHandler5()
    {
        super();
        return;
    }

    public override function handle(arg1:int, arg2:int):void
    {
        updater.update(objects[arg1]);
        updater.update(objects[(arg1 + 1) % arg2]);
        updater.update(objects[(arg1 + 2) % arg2]);
        updater.update(objects[(arg1 + 3) % arg2]);
        updater.update(objects[(arg1 + 4) % arg2]);
        return;
    }
}