package lesta.utils 
{
    public interface IUpdater
    {
        function update(arg1:Object):void;
    }
}
