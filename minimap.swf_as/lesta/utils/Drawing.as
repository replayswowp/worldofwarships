package lesta.utils 
{
    import flash.display.*;
    
    public class Drawing extends Object
    {
        public function Drawing()
        {
            super();
            return;
        }

        public static function drawDottedLine(arg1:flash.display.Sprite, arg2:Number, arg3:Number, arg4:Number, arg5:Number, arg6:uint, arg7:Number, arg8:Number, arg9:Number, arg10:Number):void
        {
            arg1.graphics.lineStyle(arg8, arg6, arg7);
            arg1.graphics.moveTo(arg2, arg3);
            var loc1:*=Math.atan2(arg5 - arg3, arg4 - arg2);
            var loc2:*=Math.sin(loc1);
            var loc3:*=Math.cos(loc1);
            var loc4:*=Math.sqrt((arg4 - arg2) * (arg4 - arg2) + (arg5 - arg3) * (arg5 - arg3));
            var loc5:*=0;
            do 
            {
                loc5 = loc5 + arg9;
                arg1.graphics.lineTo(arg2 + loc3 * loc5, arg3 + loc2 * loc5);
                loc5 = loc5 + arg10;
                arg1.graphics.moveTo(arg2 + loc3 * loc5, arg3 + loc2 * loc5);
            }
            while (loc5 < loc4);
            return;
        }

        public static function drawHorDottedLine(arg1:flash.display.Sprite, arg2:int, arg3:int, arg4:int, arg5:int):void
        {
            arg1.graphics.lineStyle(arg3, 0);
            arg1.graphics.moveTo(0, 0);
            var loc1:*=0;
            while (loc1 < arg2) 
            {
                loc1 = loc1 + arg4;
                arg1.graphics.lineTo(loc1, 0);
                loc1 = loc1 + arg5;
                arg1.graphics.moveTo(loc1, 0);
            }
            return;
        }
    }
}
