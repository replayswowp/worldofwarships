package lesta.data 
{
    import __AS3__.vec.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.utils.*;
    import scaleform.clik.data.*;
    
    public class EntityCollection extends lesta.data.EntityStorage
    {
        public function EntityCollection(arg1:String, arg2:Class=null, arg3:Function=null)
        {
            super(arg1);
            this.entityClass = arg2 || Object;
            this.entityIDFetcher = arg3;
            this.content = new flash.utils.Dictionary();
            this.slaveViews = new Vector.<lesta.data.EntityCollectionView>();
            this.defaultListView = new lesta.data.EntityCollectionListView(null, this);
            this.defaultMapView = new lesta.data.EntityCollectionMapView(null, null, this);
            return;
        }

        public function getEntityClassName():String
        {
            return flash.utils.getQualifiedClassName(this.entityClass).replace("::", ".");
        }

        public function getContent():flash.utils.Dictionary
        {
            return this.content;
        }

        public function getList():scaleform.clik.data.DataProvider
        {
            return this.defaultListView.getList();
        }

        public function getMap():flash.utils.Dictionary
        {
            return this.defaultMapView.getMap();
        }

        public function getDataByID(arg1:Object):Object
        {
            return this.content[arg1];
        }

        public function attachView(arg1:lesta.data.EntityCollectionView):void
        {
            this.slaveViews.push(arg1);
            return;
        }

        public override function receiveIncomingData(arg1:Object):Array
        {
            var loc6:*=null;
            var loc7:*=null;
            var loc8:*=null;
            var loc9:*=null;
            var loc10:*=false;
            var loc11:*=0;
            if (arg1 is Array) 
            {
                loc6 = arg1 as Array;
            }
            else 
            {
                loc6 = [arg1];
            }
            var loc1:*=[];
            var loc2:*=0;
            var loc3:*=this.slaveViews.length;
            if (this.clearOnUpdate) 
            {
                this.clear();
            }
            var loc4:*=0;
            var loc5:*=loc6.length;
            while (loc4 < loc5) 
            {
                loc7 = loc6[loc4];
                loc8 = this.entityIDFetcher != null ? this.entityIDFetcher(loc7) : loc7.id;
                if ((loc9 = this.content[loc8]) == null) 
                {
                    loc9 = new this.entityClass();
                    this.content[loc8] = loc9;
                    loc10 = false;
                }
                else 
                {
                    loc10 = true;
                }
                injectDataInto(loc9, loc7);
                if (!loc10) 
                {
                    loc11 = 0;
                    while (loc11 < loc3) 
                    {
                        this.slaveViews[loc11].addMember(loc9, loc8);
                        ++loc11;
                    }
                }
                var loc12:*;
                loc1[loc12 = loc2++] = loc9;
                ++loc4;
            }
            if (this.clearOnUpdate) 
            {
                this.defaultListView.getList().dispatchEvent(new flash.events.Event(flash.events.Event.CHANGE));
            }
            return loc1;
        }

        public function collectionMemberWasAddedExternally(arg1:Object):void
        {
            var loc1:*=this.entityIDFetcher != null ? this.entityIDFetcher(arg1) : arg1.id;
            var loc2:*=0;
            var loc3:*=this.slaveViews.length;
            while (loc2 < loc3) 
            {
                this.slaveViews[loc2].addMember(arg1, loc1);
                ++loc2;
            }
            return;
        }

        public function getFirstMemberSuitableTo(arg1:Function):Object
        {
            var loc4:*=null;
            var loc1:*=this.getList();
            var loc2:*=0;
            var loc3:*=loc1.length;
            while (loc2 < loc3) 
            {
                loc4 = loc1[loc2];
                if (arg1(loc4)) 
                {
                    return loc4;
                }
                ++loc2;
            }
            return null;
        }

        public function deleteMemberByID(arg1:*):Boolean
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=0;
            if (arg1 in this.content) 
            {
                loc1 = this.content[arg1];
                delete this.content[arg1];
                loc2 = 0;
                loc3 = this.slaveViews.length;
                while (loc2 < loc3) 
                {
                    this.slaveViews[loc2].deleteMember(loc1, arg1);
                    ++loc2;
                }
                return true;
            }
            return false;
        }

        public function clear():void
        {
            var loc1:*=0;
            var loc2:*=this.slaveViews.length;
            while (loc1 < loc2) 
            {
                this.slaveViews[loc1].clear();
                ++loc1;
            }
            lesta.utils.Algorithm.clearDictionary(this.content);
            return;
        }

        public var clearOnUpdate:Boolean=false;

        internal var content:flash.utils.Dictionary;

        internal var defaultListView:lesta.data.EntityCollectionListView;

        internal var defaultMapView:lesta.data.EntityCollectionMapView;

        internal var slaveViews:__AS3__.vec.Vector.<lesta.data.EntityCollectionView>;

        internal var entityClass:Class;

        internal var entityIDFetcher:Function;
    }
}
