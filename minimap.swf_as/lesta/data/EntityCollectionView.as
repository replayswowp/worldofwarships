package lesta.data 
{
    public class EntityCollectionView extends Object
    {
        public function EntityCollectionView(arg1:Function=null, arg2:lesta.data.EntityCollection=null)
        {
            super();
            this.filterFunction = arg1;
            if (arg2 != null) 
            {
                arg2.attachView(this);
            }
            return;
        }

        public function addMember(arg1:Object, arg2:Object):void
        {
            if (this.filterFunction == null || this.filterFunction(arg1)) 
            {
                this.doAddMember(arg1, arg2);
            }
            return;
        }

        public function deleteMember(arg1:Object, arg2:Object):void
        {
            return;
        }

        protected function doAddMember(arg1:Object, arg2:Object):void
        {
            return;
        }

        public function clear():void
        {
            return;
        }

        protected var filterFunction:Function;
    }
}
