package lesta.data 
{
    import flash.utils.*;
    import lesta.utils.*;
    
    public class EntityCollectionMapView extends lesta.data.EntityCollectionView
    {
        public function EntityCollectionMapView(arg1:Function=null, arg2:Function=null, arg3:lesta.data.EntityCollection=null)
        {
            super(arg1, arg3);
            this.mapKeyFunction = arg2;
            this.data = new flash.utils.Dictionary();
            return;
        }

        public function getMap():flash.utils.Dictionary
        {
            return this.data;
        }

        protected override function doAddMember(arg1:Object, arg2:Object):void
        {
            var loc1:*=this.mapKeyFunction != null ? this.mapKeyFunction(arg1) : arg2;
            this.data[loc1] = arg1;
            return;
        }

        public override function deleteMember(arg1:Object, arg2:Object):void
        {
            var loc1:*=this.mapKeyFunction != null ? this.mapKeyFunction(arg1) : arg2;
            delete this.data[loc1];
            return;
        }

        public override function clear():void
        {
            lesta.utils.Algorithm.clearDictionary(this.data);
            return;
        }

        internal var mapKeyFunction:Function;

        internal var data:flash.utils.Dictionary;
    }
}
