package 
{
    import lesta.dialogs.battle_window._battle_progress.*;
    
    public dynamic class CapturePointIndicatorMinimapClip extends lesta.dialogs.battle_window._battle_progress.CapturePointIndicator
    {
        public function CapturePointIndicatorMinimapClip()
        {
            super();
            addFrameScript(0, this.frame1, 14, this.frame15, 21, this.frame22, 38, this.frame39);
            return;
        }

        internal function frame1():*
        {
            stop();
            return;
        }

        internal function frame15():*
        {
            stop();
            return;
        }

        internal function frame22():*
        {
            stop();
            return;
        }

        internal function frame39():*
        {
            stop();
            return;
        }
    }
}
