package battle_stats_fla 
{
    import flash.display.*;
    
    public dynamic class BasePointLettersClip_14 extends flash.display.MovieClip
    {
        public function BasePointLettersClip_14()
        {
            super();
            addFrameScript(0, this.frame1, 19, this.frame20);
            return;
        }

        function frame1():*
        {
            stop();
            return;
        }

        function frame20():*
        {
            gotoAndPlay("locking");
            return;
        }

        public var symbol:flash.display.MovieClip;
    }
}
