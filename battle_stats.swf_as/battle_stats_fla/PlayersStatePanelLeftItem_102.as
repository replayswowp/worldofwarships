package battle_stats_fla 
{
    import flash.display.*;
    import flash.text.*;
    
    public dynamic class PlayersStatePanelLeftItem_102 extends flash.display.MovieClip
    {
        public function PlayersStatePanelLeftItem_102()
        {
            super();
            addFrameScript(0, this.frame1);
            return;
        }

        function frame1():*
        {
            stop();
            return;
        }

        public var txt_user_name:flash.text.TextField;

        public var mvc_ship_position:ShipPositionPH;

        public var txt_frags:flash.text.TextField;

        public var cameraClip:flash.display.MovieClip;
    }
}
