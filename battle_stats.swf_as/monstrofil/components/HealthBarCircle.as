﻿package monstrofil.components {
	import flash.display.Sprite;
	import monstrofil.components.Arc;
	
	public class HealthBarCircle extends Arc {
		public function HealthBarCircle(x:Number, y:Number, radius:Number, w:Number) {
			super(x, y, radius, w);
		}
		
		public function draw_bar(health, maxHealth, color=0xccFFcc) {
			if (health * maxHealth != 0){
				var angle_from = -90;
				var angle_to = 360.0 * health / maxHealth;
			
				super.draw(angle_from, angle_to, color);
			}			
			
		}

	}
	
}
