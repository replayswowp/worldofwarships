package 
{
    import adobe.utils.*;
    import flash.accessibility.*;
    import flash.desktop.*;
    import flash.display.*;
    import flash.errors.*;
    import flash.events.*;
    import flash.external.*;
    import flash.filters.*;
    import flash.geom.*;
    import flash.globalization.*;
    import flash.media.*;
    import flash.net.*;
    import flash.net.drm.*;
    import flash.printing.*;
    import flash.profiler.*;
    import flash.sampler.*;
    import flash.sensors.*;
    import flash.system.*;
    import flash.text.*;
    import flash.text.engine.*;
    import flash.text.ime.*;
    import flash.ui.*;
    import flash.utils.*;
    import flash.xml.*;
    import lesta.dialogs.battle_window.*;
    
    public dynamic class BattleProgressClip extends lesta.dialogs.battle_window.BattleProgress
    {
        public function BattleProgressClip()
        {
            super();
            this.__setProp_allyProgressBar_BattleProgressClip_Layer2_0();
            this.__setProp_enemyProgressBar_BattleProgressClip_Layer2_0();
            return;
        }

        internal function __setProp_allyProgressBar_BattleProgressClip_Layer2_0():*
        {
            try 
            {
                allyProgressBar["componentInspectorSetting"] = true;
            }
            catch (e:Error)
            {
            };
            allyProgressBar.enabled = true;
            allyProgressBar.enableInitCallback = false;
            allyProgressBar.maximum = 100;
            allyProgressBar.minimum = 0;
            allyProgressBar.soundSet = "";
            allyProgressBar.value = 0;
            allyProgressBar.visible = true;
            try 
            {
                allyProgressBar["componentInspectorSetting"] = false;
            }
            catch (e:Error)
            {
            };
            return;
        }

        internal function __setProp_enemyProgressBar_BattleProgressClip_Layer2_0():*
        {
            try 
            {
                enemyProgressBar["componentInspectorSetting"] = true;
            }
            catch (e:Error)
            {
            };
            enemyProgressBar.enabled = true;
            enemyProgressBar.enableInitCallback = false;
            enemyProgressBar.maximum = 100;
            enemyProgressBar.minimum = 0;
            enemyProgressBar.soundSet = "";
            enemyProgressBar.value = 0;
            enemyProgressBar.visible = true;
            try 
            {
                enemyProgressBar["componentInspectorSetting"] = false;
            }
            catch (e:Error)
            {
            };
            return;
        }
    }
}
