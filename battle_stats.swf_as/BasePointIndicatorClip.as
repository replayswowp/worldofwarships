package 
{
    import lesta.dialogs.battle_window._battle_progress.*;
    
    public dynamic class BasePointIndicatorClip extends lesta.dialogs.battle_window._battle_progress.CapturePointIndicator
    {
        public function BasePointIndicatorClip()
        {
            super();
            addFrameScript(12, this.frame13, 13, this.frame14, 25, this.frame26, 26, this.frame27, 39, this.frame40, 55, this.frame56, 79, this.frame80, 96, this.frame97);
            return;
        }

        internal function frame13():*
        {
            stop();
            return;
        }

        internal function frame14():*
        {
            stop();
            return;
        }

        internal function frame26():*
        {
            stop();
            return;
        }

        internal function frame27():*
        {
            stop();
            return;
        }

        internal function frame40():*
        {
            stop();
            return;
        }

        internal function frame56():*
        {
            stop();
            return;
        }

        internal function frame80():*
        {
            stop();
            return;
        }

        internal function frame97():*
        {
            stop();
            return;
        }
    }
}
