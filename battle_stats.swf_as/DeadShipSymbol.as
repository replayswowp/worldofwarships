package 
{
    import flash.display.*;
    
    public dynamic class DeadShipSymbol extends flash.display.MovieClip
    {
        public function DeadShipSymbol()
        {
            super();
            addFrameScript(0, this.frame1);
            return;
        }

        internal function frame1():*
        {
            stop();
            return;
        }
    }
}
