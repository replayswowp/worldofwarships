package 
{
    import flash.display.*;
    import flash.text.*;
    
    public dynamic class PrebattleText extends flash.display.Sprite
    {
        public function PrebattleText()
        {
            super();
            return;
        }

        public var prebattle_timer:flash.text.TextField;

        public var prebattle_text:flash.text.TextField;

        public var labelGameModeDescription:flash.text.TextField;
    }
}
