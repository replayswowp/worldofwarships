package lesta.datahub 
{
    import flash.utils.*;
    
    public class Component extends Object
    {
        public function Component()
        {
            super();
            return;
        }

        public function fini():void
        {
            this.entity = null;
            return;
        }

        public function get classID():uint
        {
            return 0;
        }

        public function get className():String
        {
            return "NoName";
        }

        public function toString():String
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc1:*="Component " + this.className + "{";
            var loc2:*=flash.utils.describeType(this);
            var loc3:*=[];
            var loc7:*=0;
            var loc8:*=loc2.variable;
            for each (loc4 in loc8) 
            {
                loc5 = String(loc4.attribute("name"));
                loc6 = String(loc4.attribute("type"));
                loc1 = loc1 + ("\n\t" + loc5 + ":" + loc6 + " = " + this[loc5]);
            }
            loc1 = loc1 + "\n}";
            return loc1;
        }

        public static const ADD:uint=0;

        public static const REMOVE:uint=65536;

        public var entity:lesta.datahub.Entity;
    }
}
