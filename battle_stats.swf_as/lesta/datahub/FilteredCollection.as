package lesta.datahub 
{
    public class FilteredCollection extends lesta.datahub.Collection
    {
        public function FilteredCollection()
        {
            super();
            return;
        }

        public function refill(arg1:Array):void
        {
            items.length = 0;
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                items.push(arg1[loc1]);
                ++loc1;
            }
            update();
            return;
        }
    }
}
