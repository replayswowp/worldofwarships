package lesta.datahub 
{
    import flash.events.*;
    
    public class EntityRef extends flash.events.EventDispatcher
    {
        public function EntityRef()
        {
            super();
            return;
        }

        public function setEntity(arg1:lesta.datahub.Entity):void
        {
            this.ref = arg1;
            dispatchEvent(new flash.events.Event(flash.events.Event.CHANGE));
            return;
        }

        public var ref:lesta.datahub.Entity=null;
    }
}
