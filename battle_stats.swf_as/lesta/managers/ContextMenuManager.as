package lesta.managers 
{
    import flash.display.*;
    import flash.utils.*;
    import lesta.data.*;
    import lesta.dialogs.battle_window.context_menu.*;
    import lesta.managers.tooltipBehaviour.*;
    
    public class ContextMenuManager extends Object
    {
        public function ContextMenuManager()
        {
            this.mapMenus = new flash.utils.Dictionary();
            this.mapData = new flash.utils.Dictionary();
            this.behaviours = [];
            super();
            this.behaviours.push(new lesta.managers.tooltipBehaviour.HorizontalInteractiveTooltipBehaviour());
            this.behaviours.push(new lesta.managers.tooltipBehaviour.InfotipBehaviour());
            this.behaviours.push(new lesta.managers.tooltipBehaviour.VerticalInfotipBehaviour());
            this.behaviours.push(new lesta.managers.tooltipBehaviour.HorizontalInfoWidgetBehaviour());
            this.behaviours.push(new lesta.managers.tooltipBehaviour.VerticalInfoWidgetBehaviour());
            this.behaviours.push(new lesta.managers.tooltipBehaviour.ContextSubmenuBehaviour());
            this.behaviours.push(new lesta.managers.tooltipBehaviour.ContextSubmenuVerticalBehavior());
            this.behaviours.push(new lesta.managers.tooltipBehaviour.VerticalInfotipBehaviourSwitch());
            this.behaviours.push(new lesta.managers.tooltipBehaviour.WindowTooltipBehaviour());
            lesta.data.GameDelegate.addCallBack("battle.setCursorState", this, this.onSetCursorState);
            return;
        }

        public function fini():void
        {
            var loc1:*=null;
            lesta.data.GameDelegate.removeCallBack(this);
            var loc2:*=0;
            var loc3:*=this.mapMenus;
            for each (loc1 in loc3) 
            {
                loc1.fini();
            }
            this.manager.free();
            this.currentMenuData = null;
            this.mapData = null;
            this.mapMenus = null;
            this.manager = null;
            return;
        }

        public function get scope():Object
        {
            return this.currentMenuData.scope;
        }

        public function get tooltipManager():lesta.managers.TooltipManager
        {
            return this.manager;
        }

        public function initialize(arg1:flash.display.DisplayObjectContainer):void
        {
            var loc2:*=null;
            var loc3:*=null;
            this.manager = new lesta.managers.TooltipManager(arg1, this.behaviours);
            this.container = arg1;
            var loc1:*=0;
            while (loc1 < arg1.numChildren) 
            {
                loc2 = arg1.getChildAt(loc1);
                (loc3 = loc2 as lesta.dialogs.battle_window.context_menu.ContextMenu).init(this);
                loc3.visible = false;
                this.mapMenus[loc3.name] = loc3;
                ++loc1;
            }
            return;
        }

        public function openMenu(arg1:ContextData):void
        {
            this.currentMenuData = arg1;
            this.initializeMenu(arg1.menuId);
            return;
        }

        public function initializeMenu(arg1:String):void
        {
            this.mapMenus[arg1].open();
            return;
        }

        public function getMenu(arg1:String):lesta.dialogs.battle_window.context_menu.ContextMenu
        {
            var loc1:*=this.mapMenus[arg1];
            return loc1;
        }

        public function updateScope(arg1:flash.display.DisplayObjectContainer, arg2:Object):void
        {
            var loc1:*=this.mapData[arg1];
            if (loc1) 
            {
                loc1.scope = arg2;
            }
            return;
        }

        public function attachMenu(arg1:flash.display.DisplayObjectContainer, arg2:String, arg3:Function, arg4:Object, arg5:Function=null, arg6:Function=null):void
        {
            var loc1:*;
            (loc1 = new ContextData(this, arg2, arg3, arg4)).onShow = arg5;
            loc1.onHide = arg6;
            loc1.id = this.manager.registerObject(arg1, loc1.getMenu, loc1.hideHandler, this.dummyHandler, loc1.showHandler, this.dummyHandler, this.dummyHandler, this.dummyHandler, 11, this.dummyHandler);
            this.mapData[arg1] = loc1;
            return;
        }

        public function detachMenu(arg1:flash.display.DisplayObjectContainer):void
        {
            var loc1:*=this.mapData[arg1];
            if (loc1) 
            {
                this.manager.unregisterTooltip(loc1.id);
                delete this.mapData[arg1];
            }
            return;
        }

        public function invokeCallback(arg1:String):void
        {
            this.manager.hideAllTooltips();
            this.currentMenuData.callback(arg1, this.scope);
            return;
        }

        internal function onSetCursorState(arg1:Boolean, arg2:Boolean):void
        {
            if (!arg1) 
            {
                this.manager.hideAllTooltips();
            }
            return;
        }

        internal function dummyHandler():Boolean
        {
            return false;
        }

        internal var container:flash.display.DisplayObjectContainer;

        internal var manager:lesta.managers.TooltipManager;

        internal var mapMenus:flash.utils.Dictionary;

        internal var mapData:flash.utils.Dictionary;

        internal var currentMenuData:ContextData;

        internal var behaviours:Array;
    }
}

import lesta.dialogs.battle_window.context_menu.*;


class ContextData extends Object
{
    public function ContextData(arg1:lesta.managers.ContextMenuManager, arg2:String, arg3:Function, arg4:Object)
    {
        super();
        this.manager = arg1;
        this.menuId = arg2;
        this.callback = arg3;
        this.scope = arg4;
        return;
    }

    public function getMenu():lesta.dialogs.battle_window.context_menu.ContextMenu
    {
        return this.manager.getMenu(this.menuId);
    }

    public function showHandler():void
    {
        this.manager.openMenu(this);
        if (this.onShow != null) 
        {
            this.onShow();
        }
        return;
    }

    public function hideHandler():void
    {
        if (this.onHide != null) 
        {
            this.onHide();
        }
        return;
    }

    public var id:int;

    public var menuId:String;

    public var callback:Function;

    public var scope:Object;

    public var manager:lesta.managers.ContextMenuManager;

    public var onShow:Function;

    public var onHide:Function;
}