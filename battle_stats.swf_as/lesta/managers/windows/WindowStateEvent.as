package lesta.managers.windows 
{
    import flash.events.*;
    
    public class WindowStateEvent extends flash.events.Event
    {
        public function WindowStateEvent(arg1:String, arg2:String)
        {
            super(arg1, false, false);
            this._state = arg2;
            return;
        }

        public function get state():String
        {
            return this._state;
        }

        public function get overlay():lesta.managers.windows.Overlay
        {
            return target as lesta.managers.windows.Overlay;
        }

        public function get windowId():int
        {
            return this.overlay ? this.overlay.windowId : -1;
        }

        public static const STATE_CHANGED:String="windowStateChanged";

        internal var _state:String;
    }
}
