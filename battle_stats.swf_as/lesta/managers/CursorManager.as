package lesta.managers 
{
    import flash.events.*;
    import lesta.data.*;
    
    public class CursorManager extends flash.events.EventDispatcher
    {
        public function CursorManager()
        {
            super();
            return;
        }

        public static function setCursorFlag(arg1:String, arg2:Boolean):void
        {
            lesta.data.GameDelegate.call("setCursorFlag", [arg1, arg2]);
            return;
        }
    }
}
