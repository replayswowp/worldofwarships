package lesta.managers 
{
    public class HotKeys extends Object
    {
        public function HotKeys()
        {
            super();
            return;
        }

        public static const CMD_MOVE_FORWARD:int=0;

        public static const CMD_MOVE_BACK:int=1;

        public static const CMD_MOVE_LEFT:int=2;

        public static const CMD_MOVE_RIGHT:int=3;

        public static const CRUISE_SPEED_UP:int=10;

        public static const CRUISE_SPEED_DOWN:int=11;

        public static const CRUISE_ANGLE_RIGHT:int=12;

        public static const CRUISE_ANGLE_LEFT:int=13;

        public static const CRUISE_ANGLE_0:int=14;

        public static const CMD_MAIN_CALIBER:int=20;

        public static const CMD_ANTI_TORPEDO_CALIBER:int=21;

        public static const CMD_TORPEDO:int=22;

        public static const CMD_ANTI_AIRCRAFT:int=23;

        public static const CMD_AIRCRAFT:int=24;

        public static const MAP_CAMERA_MOVE:int=33;

        public static const CMD_SHOOT:int=40;

        public static const CMD_ACTION:int=41;

        public static const CMD_TORPEDOES_ANGLE:int=50;

        public static const CMD_ARTILLERY_AMMO_0:int=51;

        public static const CMD_ARTILLERY_AMMO_1:int=52;

        public static const CMD_SELECT_AIRPLANES:int=60;

        public static const CMD_DESELECT_AIRPLANES:int=61;

        public static const CMD_ORDER_AIRPLANES:int=62;

        public static const MAP_PLANE_ORDER_MOVE:int=63;

        public static const CMD_SELECT_NEXT_AIRPLANE:int=64;

        public static const CMD_SELECT_ALL_AIRPLANES:int=65;

        public static const CMD_CARRIER_LOCK:int=66;

        public static const CMD_CARRIER_SELECT:int=67;

        public static const MAP_SCALE_PLUS:int=70;

        public static const MAP_SCALE_MINUS:int=71;

        public static const MAP_SHOW_PLANES:int=72;

        public static const MAP_SHOW_DEAD:int=73;

        public static const MAP_RECT_ATTENTION:int=74;

        public static const CMD_FREE_CURSOR:int=80;

        public static const CMD_STATS:int=82;

        public static const CMD_GAME_STATS:int=83;

        public static const CMD_HELP:int=84;

        public static const CMD_ICONS:int=85;

        public static const CMD_SQUADRON_0:int=90;

        public static const CMD_SQUADRON_1:int=91;

        public static const CMD_SQUADRON_2:int=92;

        public static const CMD_SQUADRON_3:int=93;

        public static const CMD_SQUADRON_4:int=94;

        public static const CMD_SQUADRON_5:int=95;

        public static const CMD_SQUADRON_6:int=96;

        public static const CMD_SQUADRON_7:int=97;

        public static const CMD_SQUADRON_8:int=98;

        public static const CMD_SQUADRON_9:int=99;

        public static const CMD_CONSUMABLE_0:int=100;

        public static const CMD_CONSUMABLE_1:int=101;

        public static const CMD_CONSUMABLE_2:int=102;

        public static const CMD_CONSUMABLE_3:int=103;

        public static const CMD_CONSUMABLE_4:int=104;
    }
}
