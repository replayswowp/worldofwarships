package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    
    public class VerticalInfotipBehaviourSwitch extends lesta.managers.tooltipBehaviour.VerticalInfotipBehaviour
    {
        public function VerticalInfotipBehaviourSwitch()
        {
            super();
            return;
        }

        public override function getName():String
        {
            return "verticalInfotipSwitch";
        }

        public override function onReasonClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            if (arg2.visible) 
            {
                hideTooltip(arg2);
            }
            else 
            {
                showTooltip(arg1, arg2, arg3);
            }
            arg3.stopPropagation();
            return arg2.visible;
        }
    }
}
