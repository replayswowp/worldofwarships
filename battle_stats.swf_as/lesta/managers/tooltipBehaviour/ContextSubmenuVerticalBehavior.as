package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import lesta.managers.*;
    
    public class ContextSubmenuVerticalBehavior extends lesta.managers.tooltipBehaviour.ContextSubmenuBehaviour
    {
        public function ContextSubmenuVerticalBehavior()
        {
            super();
            return;
        }

        public override function getName():String
        {
            return "submenuVertical";
        }

        protected override function appearTooltipAnimation(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):void
        {
            visualize(arg1, arg2, arg3);
            var loc1:*=appearDuration;
            var loc2:*=appearDelay;
            var loc3:*={"alpha":arg2.tipInstance.alpha, "y":arg2.tipInstance.y - 10};
            var loc4:*={"alpha":1, "y":arg2.tipInstance.y};
            arg2.tipInstance.alpha = 0;
            arg2.tweenId = lesta.managers.TweenManager.addTweenExplicitly(arg2.tipInstance, loc1, loc3, loc4, null, 0, loc2);
            return;
        }

        protected override function disappearTooltipAnimation(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            var tooltip:lesta.managers.tooltipBehaviour.TooltipInfo;
            var finishAnimationCallback:Function;
            var duration:Number;
            var delay:Number;
            var startProps:Object;
            var finishProps:Object;

            var loc1:*;
            finishAnimationCallback = null;
            tooltip = arg1;
            finishAnimationCallback = function ():void
            {
                tooltip.tipInstance.visible = false;
                return;
            }
            duration = disappearDuration;
            delay = disappearDelay;
            startProps = {"alpha":tooltip.tipInstance.alpha, "y":tooltip.tipInstance.y};
            finishProps = {"alpha":0, "y":tooltip.tipInstance.y - 10};
            tooltip.tweenId = lesta.managers.TweenManager.addTweenExplicitly(tooltip.tipInstance, duration, startProps, finishProps, finishAnimationCallback, 0, delay);
            return;
        }

        protected override function placeTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):void
        {
            var loc1:*=null;
            var loc2:*=false;
            var loc3:*=NaN;
            var loc4:*=NaN;
            var loc5:*=NaN;
            if (isValidTooltip(arg2)) 
            {
                loc2 = (loc1 = getTargetRect(arg2)).bottom + arg2.tipInstance.height + tinyOffset > arg1.stage.stageHeight;
                loc3 = arg2.getStyleProperty("tinyOffset", tinyOffset);
                loc4 = arg2.getStyleProperty("rightOffset", rightOffset);
                loc5 = arg2.getStyleProperty("bottomOffset", bottomOffset);
                arg2.tipInstance.x = loc4 + loc1.left + (loc1.width - arg2.tipInstance.width) / 2;
                arg2.tipInstance.x = Math.max(loc3, Math.min(arg2.tipInstance.x, arg1.stage.stageWidth - loc3 - arg2.tipInstance.width));
                arg2.tipInstance.y = loc5 + (loc2 ? loc1.top - arg2.tipInstance.height : loc1.bottom);
                placeTooltipPin(arg1, arg2, arg3, DIRECTION_VERTICAL, loc2);
            }
            return;
        }

        public override function onReasonClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            if (arg2.visible) 
            {
                hideTooltip(arg2);
            }
            else 
            {
                showTooltip(arg1, arg2, arg3);
            }
            arg3.stopPropagation();
            return arg2.visible;
        }
    }
}
