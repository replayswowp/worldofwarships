package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import lesta.managers.*;
    
    public class SimpleTooltipBehaviour extends Object implements lesta.managers.tooltipBehaviour.ITooltipBehaviour
    {
        public function SimpleTooltipBehaviour()
        {
            super();
            return;
        }

        public function getName():String
        {
            return "simple";
        }

        public function onTooltipMouseDown(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public function onTooltipResize(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            if (arg2.visible && !this.resizeListeningDisabled) 
            {
                this.placeTooltip(arg1, arg2, null);
            }
            this.resizeListeningDisabled = false;
            return arg2.visible;
        }

        public function onOutsideMouseDown(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        protected function placeTooltipPin(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent, arg4:int=0, arg5:Boolean=false):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc4:*=null;
            var loc5:*=NaN;
            var loc3:*=arg2.getStyleProperty("isTipCenter", false);
            if (arg4 != DIRECTION_HORIZONTAL) 
            {
                if (arg5) 
                {
                    loc1 = arg2.getPin(lesta.managers.tooltipBehaviour.TooltipInfo.PIN_BOTTOM);
                }
                else 
                {
                    loc1 = arg2.getPin(lesta.managers.tooltipBehaviour.TooltipInfo.PIN_TOP);
                }
                if (loc1 == null) 
                {
                    return;
                }
                loc4 = arg2.tipInstance.getBounds(loc1.parent);
                if (loc3) 
                {
                    loc2 = arg2.tooltipReason.getBounds(loc1.parent);
                    loc1.x = Math.max(loc4.left, loc2.right - loc1.width / 2 - loc2.width / 2);
                }
                else 
                {
                    loc5 = (arg3 == null ? loc1.parent.width / 2 : loc1.parent.mouseX) - loc1.width / 2;
                    loc1.x = Math.max(loc4.left, Math.min(loc4.right - loc1.width, loc5));
                }
            }
            else 
            {
                if (arg5) 
                {
                    loc1 = arg2.getPin(lesta.managers.tooltipBehaviour.TooltipInfo.PIN_RIGHT);
                }
                else 
                {
                    loc1 = arg2.getPin(lesta.managers.tooltipBehaviour.TooltipInfo.PIN_LEFT);
                }
                if (loc1 == null) 
                {
                    return;
                }
                loc4 = arg2.tipInstance.getBounds(loc1.parent);
                if (loc3) 
                {
                    loc2 = arg2.tooltipReason.getBounds(loc1.parent);
                    loc1.y = Math.max(loc4.top, loc2.top + loc2.height / 2 - loc4.top - loc1.height / 2);
                }
                else 
                {
                    loc5 = (arg3 == null ? loc4.height / 2 : loc1.parent.mouseY) - loc1.height / 2;
                    loc1.y = Math.max(loc4.top, Math.min(loc4.bottom - loc1.height, loc5));
                }
            }
            return;
        }

        public function onOutsideMouseClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public function onTooltipMouseMove(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public function onTooltipClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public function onTooltipClose(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            this.hideTooltip(arg2);
            return true;
        }

        public function onApplicationLooseFocus(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            this.hideTooltip(arg2);
            return true;
        }

        public function onTooltipStopDragging(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            return false;
        }

        protected static function getTargetRect(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):flash.geom.Rectangle
        {
            var loc1:*=new flash.geom.Point(0, 0);
            loc1 = arg1.tooltipReason.localToGlobal(loc1);
            loc1 = arg1.tipInstance.parent.globalToLocal(loc1);
            return new flash.geom.Rectangle(loc1.x, loc1.y, arg1.tooltipReason.width, arg1.tooltipReason.height);
        }

        public function onRegisterTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            arg2.tipInstance.alpha = 0;
            arg2.tipInstance.visible = false;
            return;
        }

        public function onUnregisterTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            lesta.managers.TweenManager.clearObject(arg2.tipInstance);
            return;
        }

        public function onApplicationSizeChanged(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            this.placeTooltip(arg1, arg2, null);
            return;
        }

        public function showTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent, arg4:Boolean=false):void
        {
            var scene:flash.display.DisplayObjectContainer;
            var tooltip:lesta.managers.tooltipBehaviour.TooltipInfo;
            var event:flash.events.MouseEvent;
            var now:Boolean=false;
            var delay:Number;

            var loc1:*;
            scene = arg1;
            tooltip = arg2;
            event = arg3;
            now = arg4;
            if (tooltip.visible) 
            {
                return;
            }
            tooltip.beforeShowCallback();
            tooltip.visible = true;
            delay = Number(tooltip.getStyleProperty("appearDelay;", this.appearDelay));
            if (delay > 0) 
            {
                tooltip.tweenId = lesta.managers.TweenManager.addTweenExplicitly(tooltip.tipInstance, delay, {"alpha":0}, {"alpha":0}, function ():void
                {
                    appearTooltipAnimation(scene, tooltip, event);
                    return;
                }, delay * 1000)
            }
            else 
            {
                this.appearTooltipAnimation(scene, tooltip, event);
            }
            tooltip.showCallback();
            return;
        }

        protected function visualize(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):void
        {
            this.resizeListeningDisabled = true;
            arg2.tipInstance.visible = true;
            arg2.addTooltipToStage();
            arg2.validateTooltipSize();
            this.placeTooltip(arg1, arg2, arg3);
            return;
        }

        protected function appearTooltipAnimation(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):void
        {
            this.visualize(arg1, arg2, arg3);
            var loc1:*=arg2.getStyleProperty("appearDuration", this.appearDuration);
            var loc2:*=arg2.getStyleProperty("appearOffset", this.appearOffset);
            var loc3:*={"alpha":arg2.tipInstance.alpha, "y":arg2.tipInstance.y + loc2};
            var loc4:*={"alpha":1, "y":arg2.tipInstance.y};
            arg2.tweenId = lesta.managers.TweenManager.addTweenExplicitly(arg2.tipInstance, loc1, loc3, loc4);
            return;
        }

        public function hideTooltip(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            if (lesta.managers.TweenManager.hasTween(arg1.tipInstance)) 
            {
                lesta.managers.TweenManager.removeTween(arg1.tipInstance, arg1.tweenId);
                arg1.tipInstance.visible = false;
            }
            if (arg1.visible) 
            {
                arg1.visible = false;
                this.disappearTooltipAnimation(arg1);
            }
            return;
        }

        protected function disappearTooltipAnimation(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):void
        {
            var tooltip:lesta.managers.tooltipBehaviour.TooltipInfo;
            var finishAnimationCallback:Function;
            var duration:Number;
            var delay:Number;
            var offset:Number;
            var startProps:Object;
            var finishProps:Object;

            var loc1:*;
            finishAnimationCallback = null;
            tooltip = arg1;
            finishAnimationCallback = function ():void
            {
                if (!tooltip.tipInstance) 
                {
                    trace("[Error]", "[WWSD-46508]", "TooltipInfo was finalized without killing all tweens");
                }
                tooltip.tipInstance.visible = false;
                return;
            }
            duration = tooltip.getStyleProperty("disappearDuration", this.disappearDuration);
            delay = tooltip.getStyleProperty("disappearDelay", this.disappearDelay);
            offset = tooltip.getStyleProperty("disappearOffset", this.disappearOffset);
            startProps = {"alpha":tooltip.tipInstance.alpha, "y":tooltip.tipInstance.y};
            finishProps = {"alpha":0, "y":tooltip.tipInstance.y + offset};
            lesta.managers.TweenManager.addTweenExplicitly(tooltip.tipInstance, duration, startProps, finishProps, finishAnimationCallback, 0, delay);
            return;
        }

        protected function isValidTooltip(arg1:lesta.managers.tooltipBehaviour.TooltipInfo):Boolean
        {
            if (arg1 == null || arg1.tipInstance.parent == null || arg1.tooltipReason == null) 
            {
                if (arg1) 
                {
                    trace("[Warning] Orphan tooltip: ", arg1, arg1.tipInstance.parent, arg1.tooltipReason);
                }
                else 
                {
                    trace("[Warning] Orphan tooltip: ", arg1);
                }
                return false;
            }
            return true;
        }

        protected function placeTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):void
        {
            var loc1:*=NaN;
            var loc2:*=NaN;
            var loc3:*=NaN;
            var loc4:*=NaN;
            var loc5:*=false;
            var loc6:*=false;
            var loc7:*=NaN;
            var loc8:*=NaN;
            if (this.isValidTooltip(arg2)) 
            {
                loc1 = arg2.getStyleProperty("screenTopOffset", this.screenTopOffset);
                loc2 = arg2.getStyleProperty("tinyOffset", this.tinyOffset);
                loc3 = arg2.getStyleProperty("rightOffset", this.rightOffset);
                loc4 = arg2.getStyleProperty("bottomOffset", this.bottomOffset);
                loc5 = arg1.stage.mouseX + arg2.tipInstance.width + loc3 + loc2 > arg1.stage.stageWidth;
                loc6 = arg1.stage.mouseY + arg2.tipInstance.height + loc4 + loc2 > arg1.stage.stageHeight;
                loc7 = loc5 ? arg1.stage.mouseX - arg2.tipInstance.width : arg1.stage.mouseX;
                loc8 = loc6 ? arg1.stage.mouseY - arg2.tipInstance.height : arg1.stage.mouseY;
                if (loc6) 
                {
                    loc8 = loc8 - loc2;
                    if (loc5) 
                    {
                        loc7 = loc7 - loc2;
                    }
                    else 
                    {
                        loc7 = loc7 + loc3;
                    }
                }
                else 
                {
                    loc8 = loc8 + loc4;
                    if (loc5) 
                    {
                        loc7 = loc7 - loc2;
                    }
                    else 
                    {
                        loc7 = loc7 + loc3;
                    }
                }
                if (loc8 < loc1) 
                {
                    loc8 = loc1;
                }
                arg2.tipInstance.x = loc7;
                arg2.tipInstance.y = loc8;
                this.placeTooltipPin(arg1, arg2, arg3, DIRECTION_HORIZONTAL, loc5);
            }
            return;
        }

        public function onTooltipStartDragging(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            return false;
        }

        public function onReasonMouseOver(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public function onReasonMouseOut(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public function onReasonRollOver(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public function onReasonRollOut(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            this.hideTooltip(arg2);
            return true;
        }

        public function onReasonMouseMove(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            var loc1:*;
            var loc2:*=(loc1 = arg2.tooltipReason.getBounds(arg1)).contains(arg1.mouseX, arg1.mouseY);
            var loc3:*=false;
            if (loc2) 
            {
                if (arg2.visible) 
                {
                    this.placeTooltip(arg1, arg2, arg3);
                    loc3 = true;
                }
                else if (!lesta.managers.TweenManager.hasTween(arg2.tipInstance) || arg2.tipInstance.alpha == 0) 
                {
                    this.showTooltip(arg1, arg2, arg3);
                    loc3 = true;
                }
            }
            else 
            {
                this.hideTooltip(arg2);
                loc3 = true;
            }
            return loc3;
        }

        public function onReasonMouseDown(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return this.onReasonMouseMove(arg1, arg2, arg3);
        }

        public function onReasonClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public function onTooltipRollOver(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public function onTooltipRollOut(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return false;
        }

        public static const DIRECTION_HORIZONTAL:int=0;

        public static const DIRECTION_VERTICAL:int=1;

        public static const DIRECTION_DIAGONAL:int=2;

        protected var appearDelay:Number=0.5;

        protected var appearDuration:Number=0.1;

        protected var appearOffset:Number=5;

        protected var disappearDuration:Number=0.1;

        protected var disappearOffset:Number=-5;

        protected var screenTopOffset:int=10;

        protected var tinyOffset:int=2;

        protected var rightOffset:int=6;

        protected var bottomOffset:int=26;

        protected var resizeListeningDisabled:Boolean=false;

        protected var disappearDelay:Number=0;
    }
}
