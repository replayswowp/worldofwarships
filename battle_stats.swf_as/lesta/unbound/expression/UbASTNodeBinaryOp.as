package lesta.unbound.expression 
{
    public class UbASTNodeBinaryOp extends Object implements lesta.unbound.expression.IUbASTNode
    {
        public function UbASTNodeBinaryOp(arg1:lesta.unbound.expression.IUbASTNode, arg2:String, arg3:lesta.unbound.expression.IUbASTNode)
        {
            super();
            this.operator = arg2;
            this.firstOperand = arg1;
            this.secondOperand = arg3;
            return;
        }

        public function get astType():int
        {
            return lesta.unbound.expression.UbASTNodeType.BINARY_OPERATION;
        }

        public var operator:String;

        public var firstOperand:lesta.unbound.expression.IUbASTNode;

        public var secondOperand:lesta.unbound.expression.IUbASTNode;
    }
}
