package lesta.unbound.expression 
{
    public class UbASTNodeList extends Object implements lesta.unbound.expression.IUbASTNode
    {
        public function UbASTNodeList(arg1:Array)
        {
            super();
            this.items = arg1;
            return;
        }

        public function get astType():int
        {
            return lesta.unbound.expression.UbASTNodeType.LIST;
        }

        public var items:Array;
    }
}
