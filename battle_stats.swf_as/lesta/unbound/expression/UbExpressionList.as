package lesta.unbound.expression 
{
    import __AS3__.vec.*;
    
    public class UbExpressionList extends lesta.unbound.expression.UbExpressionBase implements lesta.unbound.expression.IUbExpression
    {
        public function UbExpressionList(arg1:lesta.unbound.expression.UbASTNodeList)
        {
            var loc4:*=null;
            this.compiledItems = new Vector.<lesta.unbound.expression.IUbExpression>();
            super(arg1);
            var loc1:*=arg1.items;
            var loc2:*=0;
            var loc3:*=loc1.length;
            while (loc2 < loc3) 
            {
                loc4 = lesta.unbound.expression.UbExpressionCompiler.createExpression(loc1[loc2]);
                this.compiledItems.push(loc4);
                addRequestedPropertiesOfExpression(loc4);
                ++loc2;
            }
            return;
        }

        public override function eval(arg1:Object):*
        {
            var loc1:*=[];
            var loc2:*=0;
            var loc3:*=this.compiledItems.length;
            while (loc2 < loc3) 
            {
                loc1.push(this.compiledItems[loc2].eval(arg1));
                ++loc2;
            }
            return loc1;
        }

        internal var compiledItems:__AS3__.vec.Vector.<lesta.unbound.expression.IUbExpression>;
    }
}
