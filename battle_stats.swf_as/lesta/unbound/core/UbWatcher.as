package lesta.unbound.core 
{
    import lesta.unbound.expression.*;
    
    public class UbWatcher extends Object implements lesta.unbound.core.IWatcher
    {
        public function UbWatcher(arg1:lesta.unbound.expression.IUbExpression, arg2:lesta.unbound.core.UbScope, arg3:Function, arg4:*=undefined)
        {
            super();
            this.expression = arg1;
            this.scope = arg2;
            this.onChange = arg3;
            this.oldValue = arg4;
            arg2.addWatcher(arg1, this);
            return;
        }

        public function onChanged(... rest):void
        {
            if (!this.dirty) 
            {
                this.scope.dirtyWatchers.push(this);
                this.dirty = true;
            }
            return;
        }

        public function update():void
        {
            if (!this.dirty) 
            {
                return;
            }
            this.dirty = false;
            var loc1:*=this.expression.eval(this.scope);
            var loc2:*=this.oldValue;
            this.oldValue = loc1;
            if (loc1 == loc2) 
            {
                if (typeof loc1 == "object") 
                {
                    this.onChange(loc2, loc1);
                }
            }
            else 
            {
                this.onChange(loc2, loc1);
            }
            return;
        }

        public function currentValue():*
        {
            return this.expression.eval(this.scope);
        }

        public function free():void
        {
            this.scope.removeWatcher(this, this.expression);
            this.expression = null;
            this.scope = null;
            this.onChange = null;
            this.dirty = false;
            return;
        }

        internal var expression:lesta.unbound.expression.IUbExpression;

        internal var scope:lesta.unbound.core.UbScope;

        internal var oldValue:*;

        internal var onChange:Function;

        internal var dirty:Boolean=true;
    }
}
