﻿package lesta.unbound.core 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.utils.*;
    import lesta.structs.*;
    import lesta.unbound.layout.*;
    
    public class UbController extends Object
    {
        public function UbController()
        {
            super();
            return;
        }

        public function init(arg1:__AS3__.vec.Vector):void
        {
            return;
        }

        public function onParamsChanged(arg1:Array, arg2:Array):void
        {
            return;
        }

        public function initialize(arg1:lesta.unbound.core.UbCentral, arg2:lesta.unbound.core.UbScope, arg3:flash.display.DisplayObject):void
        {
            this.scope = arg2;
            arg2.incrementRefcount();
            this.exportPublicMethodsToScope(arg2);
            this.element = arg3;
            this.element.addEventListener(lesta.unbound.layout.NativeToUbEvent.TRANSFER, this.transferEvent, true, 0, true);
            return;
        }

        public function free():void
        {
            if (this.freeStarted) 
            {
                return;
            }
            this.freeStarted = true;
            this.scope.decrementRefcount();
            this.scope = null;
            this.element.removeEventListener(lesta.unbound.layout.NativeToUbEvent.TRANSFER, this.transferEvent, true);
            this.element = null;
            return;
        }

        protected function dataToScope(arg1:Object):void
        {
            this.scope.copyDataFrom(arg1);
            return;
        }

        internal function exportPublicMethodsToScope(arg1:lesta.unbound.core.UbScope):void
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc1:*=lesta.unbound.core.UbReflector.getPublicMethods(flash.utils.getQualifiedClassName(this), this);
            var loc2:*=0;
            var loc3:*=loc1.length;
            while (loc2 < loc3) 
            {
                if (!((loc5 = (loc4 = loc1[loc2]).name) in this.nonExportableMethods)) 
                {
                    arg1[loc5] = this[loc5];
                }
                ++loc2;
            }
            return;
        }

        protected function transferEvent(arg1:lesta.unbound.layout.NativeToUbEvent):void
        {
            arg1.stopImmediatePropagation();
            var loc1:*=arg1.eventData;
            this.scope.onEvent(loc1.type, loc1.ev, loc1.down);
            return;
        }

        protected function updateInScope(arg1:String, arg2:Boolean=false):void
        {
            this.scope.onEvent(arg1 + lesta.unbound.core.UbScope.CHANGED, null, arg2 ? lesta.unbound.core.UbScope.EVENT_DIRECTION_DOWN : lesta.unbound.core.UbScope.EVENT_DIRECTION_NONE);
            return;
        }

        protected function getPriceFromScope(arg1:String):lesta.structs.PriceInfo
        {
            var loc1:*=0;
            return this.scope[arg1].prices[loc1] as lesta.structs.PriceInfo;
        }

        internal const nonExportableMethods:Object={"initialize":true, "addBindingForDestructionNotification":true, "free":true, "init":true, "addBinding":true};

        protected var freeStarted:Boolean=false;

        protected var scope:lesta.unbound.core.UbScope;

        protected var element:flash.display.DisplayObject;
    }
}
