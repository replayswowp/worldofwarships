package lesta.unbound.core 
{
    import __AS3__.vec.*;
    import flash.utils.*;
    import lesta.unbound.expression.*;
    
    use namespace flash_proxy;
    
    public dynamic class UbScope extends flash.utils.Proxy
    {
        public function UbScope(arg1:lesta.unbound.core.UbGlobalDefinitions)
        {
            this.$dataRef = new lesta.unbound.core.UbDataRef();
            this.dirtyWatchers = new Vector.<lesta.unbound.core.IWatcher>();
            this.$children = new Vector.<lesta.unbound.core.UbScope>();
            this.$listeners = new flash.utils.Dictionary();
            this.storage = new flash.utils.Dictionary();
            this.props = new Vector.<String>();
            super();
            this.$importedFields = arg1;
            this.copyDataFrom(this.$importedFields);
            var loc1:*;
            this.$sid = scopeIdCounter++;
            return;
        }

        public function addWatcher(arg1:lesta.unbound.expression.IUbExpression, arg2:lesta.unbound.core.IWatcher):void
        {
            this.dirtyWatchers.push(arg2);
            this.$importedFields.subscriptionHelper.subscribeMe(this, arg1, arg2.onChanged);
            return;
        }

        public function removeWatcher(arg1:lesta.unbound.core.IWatcher, arg2:lesta.unbound.expression.IUbExpression):void
        {
            var loc1:*=0;
            if (!this.finalized) 
            {
                loc1 = this.dirtyWatchers.indexOf(arg1);
                this.dirtyWatchers.splice(loc1, 1);
                this.$importedFields.subscriptionHelper.unSubscribeMe(this, arg2, arg1.onChanged);
            }
            return;
        }

        public function on(arg1:String, arg2:Function):void
        {
            if (!(arg1 in this.$listeners)) 
            {
                this.$listeners[arg1] = new flash.utils.Dictionary(true);
            }
            this.$listeners[arg1][arg2] = 1;
            return;
        }

        public function off(arg1:String, arg2:Function):void
        {
            var loc1:*=null;
            if (this.$listeners && arg1 in this.$listeners) 
            {
                loc1 = this.$listeners[arg1];
                delete loc1[arg2];
            }
            return;
        }

        flash_proxy override function nextNameIndex(arg1:int):int
        {
            if (arg1 < this.props.length) 
            {
                return arg1 + 1;
            }
            return 0;
        }

        public function broadcastEvent(arg1:String, arg2:Object):void
        {
            var loc1:*=0;
            var loc2:*=this.$children.length;
            while (loc1 < loc2) 
            {
                this.$children[loc1].onEvent(arg1, arg2, lesta.unbound.core.UbScope.EVENT_DIRECTION_DOWN);
                ++loc1;
            }
            return;
        }

        public function bubbleEvent(arg1:String, arg2:Object):void
        {
            this.$parent.onEvent(arg1, arg2, lesta.unbound.core.UbScope.EVENT_DIRECTION_UP);
            return;
        }

        public function onEvent(arg1:String, arg2:Object=null, arg3:int=2):void
        {
            var loc2:*=undefined;
            var loc1:*;
            if (loc1 = this.$listeners[arg1]) 
            {
                var loc3:*=0;
                var loc4:*=loc1;
                for (loc2 in loc4) 
                {
                    loc2(arg2);
                }
            }
            loc3 = arg3;
            switch (loc3) 
            {
                case EVENT_DIRECTION_UP:
                {
                    this.bubbleEvent(arg1, arg2);
                    break;
                }
                case EVENT_DIRECTION_DOWN:
                {
                    this.broadcastEvent(arg1, arg2);
                    break;
                }
            }
            return;
        }

        public function newChild():lesta.unbound.core.UbScope
        {
            var loc1:*=new lesta.unbound.core.UbScope(this.$importedFields);
            loc1.$parent = this;
            loc1.$root = this.$root;
            this.$children.push(loc1);
            return loc1;
        }

        public function childDestroyed(arg1:lesta.unbound.core.UbScope):void
        {
            var loc1:*=this.$children.indexOf(arg1);
            this.$children[loc1] = this.$children[(this.$children.length - 1)];
            this.$children.pop();
            return;
        }

        public function incrementRefcount():void
        {
            var loc1:*;
            var loc2:*=((loc1 = this).$refCount + 1);
            loc1.$refCount = loc2;
            return;
        }

        public function decrementRefcount():void
        {
            var loc1:*;
            var loc2:*=((loc1 = this).$refCount - 1);
            loc1.$refCount = loc2;
            if (this.$refCount == 0) 
            {
                this.free();
            }
            return;
        }

        public function copyDataFrom(arg1:Object):void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=0;
            var loc5:*=0;
            var loc6:*=null;
            var loc1:*=flash.utils.getQualifiedClassName(arg1);
            if (loc1 == "Object" || loc1 == "Dictionary") 
            {
                var loc7:*=0;
                var loc8:*=arg1;
                for (loc2 in loc8) 
                {
                    this.flash_proxy::setProperty(loc2, arg1[loc2]);
                }
            }
            else if (loc1 != "Array") 
            {
                if (loc1 == "int" || loc1 == "String" || loc1 == "Number" || loc1 == "Boolean") 
                {
                    this.flash_proxy::setProperty("$value", arg1);
                }
                else 
                {
                    loc3 = lesta.unbound.core.UbReflector.getPublicVariables(loc1, arg1);
                    loc4 = 0;
                    loc5 = loc3.length;
                    while (loc4 < loc5) 
                    {
                        loc6 = loc3[loc4].name;
                        this.flash_proxy::setProperty(loc6, arg1[loc6]);
                        ++loc4;
                    }
                }
            }
            else 
            {
                this.flash_proxy::setProperty("dataArray", arg1);
            }
            return;
        }

        public function findInScope(arg1:String):*
        {
            var loc1:*=this;
            var loc2:*=loc1[arg1];
            while (loc1.$parent && loc2 == null) 
            {
                loc2 = loc1.$parent[arg1];
                loc1 = loc1.$parent;
            }
            return loc2;
        }

        protected function free(arg1:Boolean=true):void
        {
            var loc3:*=undefined;
            this.dirtyWatchers = null;
            this.$refCount = 0;
            var loc1:*=0;
            var loc2:*=this.$children.length;
            while (loc1 < loc2) 
            {
                this.$children[loc1].free(false);
                ++loc1;
            }
            this.$children = null;
            if (arg1) 
            {
                this.$parent.childDestroyed(this);
            }
            this.$importedFields = null;
            var loc4:*=0;
            var loc5:*=this.$listeners;
            for (loc3 in loc5) 
            {
                delete this.$listeners[loc3];
            }
            this.$listeners = null;
            this.$parent = null;
            this.$root = null;
            loc4 = 0;
            loc5 = this.storage;
            for (loc3 in loc5) 
            {
                delete this.storage[loc3];
            }
            this.storage = null;
            this.finalized = true;
            return;
        }

        public function update():void
        {
            var loc1:*=0;
            var loc2:*=0;
            var loc3:*=null;
            if (this.disabled) 
            {
                return;
            }
            while (this.dirtyWatchers.length > 0) 
            {
                loc3 = this.dirtyWatchers;
                this.dirtyWatchers = new Vector.<lesta.unbound.core.IWatcher>();
                loc1 = 0;
                loc2 = loc3.length;
                while (loc1 < loc2) 
                {
                    loc3[loc1].update();
                    ++loc1;
                }
            }
            loc1 = 0;
            loc2 = this.$children.length;
            while (loc1 < loc2) 
            {
                this.$children[loc1].update();
                ++loc1;
            }
            return;
        }

        flash_proxy override function getProperty(arg1:*):*
        {
            return this.storage[arg1];
        }

        flash_proxy override function setProperty(arg1:*, arg2:*):void
        {
            if (arg2 == this.storage[arg1]) 
            {
                if (typeof arg2 == "object") 
                {
                    this.onEvent(arg1 + CHANGED, arg2);
                }
            }
            else 
            {
                this.storage[arg1] = arg2;
                if (this.props.indexOf(arg1) == -1) 
                {
                    this.props.push(arg1);
                }
                this.onEvent(arg1 + CHANGED, arg2);
            }
            return;
        }

        flash_proxy override function hasProperty(arg1:*):Boolean
        {
            return String(arg1) in this.storage;
        }

        flash_proxy override function deleteProperty(arg1:*):Boolean
        {
            this.nameStr = String(arg1);
            this.deletionResult = delete this.storage[this.nameStr];
            if (this.deletionResult) 
            {
                this.onEvent(this.nameStr + CHANGED);
            }
            return this.deletionResult;
        }

        flash_proxy override function callProperty(arg1:*, ... rest):*
        {
            var loc1:*=String(arg1);
            var loc2:*=loc1;
            switch (loc2) 
            {
                case "valueOf":
                {
                    return this;
                }
                case "toString":
                {
                    return "[UbScope] #" + this.$sid;
                }
            }
            return 0;
        }

        
        {
            scopeIdCounter = 0;
        }

        flash_proxy override function nextName(arg1:int):String
        {
            return this.props[(arg1 - 1)];
        }

        flash_proxy override function nextValue(arg1:int):*
        {
            return this.storage[this.props[arg1]];
        }

        public static const EVENT_DIRECTION_DOWN:int=1;

        public static const EVENT_DIRECTION_NONE:int=2;

        public static const CHANGED:String="_$UbScope$_Changed_";

        public static const EVENT_DIRECTION_UP:int=0;

        public var $parent:lesta.unbound.core.UbScope;

        public var $root:lesta.unbound.core.UbScope;

        public var $importedFields:lesta.unbound.core.UbGlobalDefinitions;

        public var $sid:Number;

        public var $dataRef:lesta.unbound.core.UbDataRef;

        public var dirtyWatchers:__AS3__.vec.Vector.<lesta.unbound.core.IWatcher>;

        protected var $children:__AS3__.vec.Vector.<lesta.unbound.core.UbScope>;

        protected var $listeners:flash.utils.Dictionary;

        protected var $refCount:int=0;

        protected var storage:flash.utils.Dictionary;

        protected var props:__AS3__.vec.Vector.<String>;

        protected var nameStr:String;

        protected var deletionResult:Boolean;

        public var disabled:Boolean=false;

        public var finalized:Boolean=false;

        protected static var scopeIdCounter:Number=0;
    }
}
