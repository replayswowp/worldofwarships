package lesta.unbound.core 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.unbound.bindings.*;
    import lesta.unbound.expression.*;
    import lesta.unbound.layout.*;
    
    public class UbCentral extends Object
    {
        public function UbCentral(arg1:flash.display.Stage, arg2:lesta.unbound.core.UbInjector, arg3:lesta.unbound.core.UbGlobalDefinitions=null)
        {
            super();
            var loc1:*;
            this._id = idCounter++;
            this.stage = arg1;
            this.expressionCompiler = new lesta.unbound.expression.UbExpressionCompiler();
            this.globalDefinitions = arg3 || new lesta.unbound.core.UbGlobalDefinitions();
            this.globalDefinitions.compile = this.expressionCompiler.compile;
            this.globalDefinitions.subscriptionHelper = new lesta.unbound.core.UbScopeSubscriptionHelper();
            this.globalDefinitions.$stage = arg1;
            this.injector = arg2 != null ? arg2 : new lesta.unbound.core.UbInjector();
            this.injector.addPreconstructed(this);
            this.injector.addPreconstructed(arg1);
            this.rootScope = new lesta.unbound.core.UbRootScope(this.globalDefinitions);
            this.rootScope.incrementRefcount();
            this.rootScope.$root = this.rootScope;
            this.rootScope.$importedFields = this.globalDefinitions;
            this.registeredBindingsTypes = {"controller":lesta.unbound.bindings.UbControllerBinding, "controllerWithNewScope":lesta.unbound.bindings.UbControllerNewScopeBinding};
            this.timerUpdate = new flash.utils.Timer(30);
            this.timerUpdate.addEventListener(flash.events.TimerEvent.TIMER, this.onTimerUpdate);
            return;
        }

        public function get id():int
        {
            return this._id;
        }

        public function start():void
        {
            this.timerUpdate.start();
            return;
        }

        public function stop():void
        {
            this.timerUpdate.stop();
            return;
        }

        public function setGlobalDefinition(arg1:String, arg2:Object):void
        {
            this.globalDefinitions[arg1] = arg2;
            return;
        }

        public function getGlobalDefinition(arg1:String):Object
        {
            return this.globalDefinitions[arg1];
        }

        public function free():void
        {
            if (this.stage == null) 
            {
                return;
            }
            this.timerUpdate.stop();
            this.timerUpdate.removeEventListener(flash.events.TimerEvent.TIMER, this.onTimerUpdate);
            this.timerUpdate = null;
            this.rootScope.decrementRefcount();
            this.rootScope = null;
            this.injector = null;
            this.registeredBindingsTypes = null;
            this.expressionCompiler.fini();
            this.expressionCompiler = null;
            this.globalDefinitions.free();
            this.globalDefinitions = null;
            this.stage = null;
            return;
        }

        public function forceUpdate():void
        {
            this.rootScope.update();
            return;
        }

        public function onEvent(arg1:String, arg2:Object, arg3:int):void
        {
            this.rootScope.onEvent(arg1, arg2, arg3);
            return;
        }

        public function on(arg1:String, arg2:Function):void
        {
            this.rootScope.on(arg1, arg2);
            return;
        }

        public function off(arg1:String, arg2:Function):void
        {
            this.rootScope.off(arg1, arg2);
            return;
        }

        public function createBrother():lesta.unbound.core.UbCentral
        {
            var loc2:*=null;
            var loc1:*=new lesta.unbound.core.UbCentral(this.stage, this.injector, this.globalDefinitions.getCopy());
            var loc3:*=0;
            var loc4:*=this.registeredBindingsTypes;
            for (loc2 in loc4) 
            {
                loc1.registerBindingType(loc2, this.registeredBindingsTypes[loc2]);
            }
            return loc1;
        }

        public function registerBindingType(arg1:String, arg2:Class):void
        {
            this.registeredBindingsTypes[arg1] = arg2;
            return;
        }

        public function setInitialData(arg1:Object):void
        {
            this.initData = arg1;
            this.rootScope.copyDataFrom(arg1);
            return;
        }

        public function bindBlock(arg1:lesta.unbound.layout.UbBlock, arg2:lesta.unbound.core.UbScope=null):void
        {
            var loc1:*=arg2 || this.rootScope.newChild();
            this.bindBlockRecursively(arg1, loc1);
            return;
        }

        internal function bindBlockRecursively(arg1:lesta.unbound.layout.UbBlock, arg2:lesta.unbound.core.UbScope):void
        {
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=null;
            var loc1:*=0;
            var loc2:*=arg1.numChildren;
            while (loc1 < loc2) 
            {
                if ((loc5 = arg1.getChildAt(loc1) as lesta.unbound.layout.UbBlock) != null) 
                {
                    this.bindBlockRecursively(loc5, arg2);
                }
                ++loc1;
            }
            var loc3:*=arg1.bindingsSettings;
            var loc4:*=new Vector.<lesta.unbound.bindings.UbBinding>();
            loc1 = 0;
            loc2 = loc3.length;
            while (loc1 < loc2) 
            {
                loc6 = loc3[loc1];
                loc7 = this.createBinding(loc6.name, loc6.compiledExpressions, loc6.expressionsList, arg1.getBindingsTarget(loc6.elementName), arg2);
                loc4.push(loc7);
                ++loc1;
            }
            arg1.bindings = loc4;
            return;
        }

        internal function createBinding(arg1:String, arg2:__AS3__.vec.Vector.<lesta.unbound.expression.IUbExpression>, arg3:Array, arg4:flash.display.DisplayObject, arg5:lesta.unbound.core.UbScope):lesta.unbound.bindings.UbBinding
        {
            var loc1:*=null;
            if (arg1 in this.registeredBindingsTypes) 
            {
                loc1 = new this.registeredBindingsTypes[arg1]();
            }
            else 
            {
                loc1 = new lesta.unbound.bindings.UbPropertyBinding(arg1);
            }
            this.injector.inject(loc1);
            loc1.init(arg2, arg3, arg4, arg5, this);
            return loc1;
        }

        internal function onTimerUpdate(arg1:flash.events.Event):void
        {
            this.rootScope.update();
            return;
        }

        
        {
            idCounter = 0;
        }

        public var initData:Object;

        protected var expressionCompiler:lesta.unbound.expression.UbExpressionCompiler;

        protected var globalDefinitions:lesta.unbound.core.UbGlobalDefinitions;

        protected var injector:lesta.unbound.core.UbInjector;

        protected var registeredBindingsTypes:Object;

        protected var rootScope:lesta.unbound.core.UbScope;

        protected var stage:flash.display.Stage;

        protected var _id:int=0;

        internal var timerUpdate:flash.utils.Timer;

        internal static var idCounter:int=0;
    }
}
