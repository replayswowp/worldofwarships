package lesta.unbound.core 
{
    import flash.display.*;
    import lesta.datahub.*;
    import lesta.unbound.layout.*;
    
    public class UbGlobalDefinitions extends Object
    {
        public function UbGlobalDefinitions()
        {
            super();
            return;
        }

        public function getCopy():lesta.unbound.core.UbGlobalDefinitions
        {
            var loc1:*=new lesta.unbound.core.UbGlobalDefinitions();
            loc1.compile = this.compile;
            loc1.tr = this.tr;
            loc1.toUpperCase = this.toUpperCase;
            loc1.toLowerCase = this.toLowerCase;
            loc1.$stage = this.$stage;
            loc1.format = this.format;
            loc1.timestampToHourMinSec = this.timestampToHourMinSec;
            loc1.subscriptionHelper = this.subscriptionHelper;
            loc1.dataHub = this.dataHub;
            loc1.TooltipBehaviour = this.TooltipBehaviour;
            loc1.ChannelGroup = this.ChannelGroup;
            loc1.InviteType = this.InviteType;
            loc1.WindowTooltipState = this.WindowTooltipState;
            loc1.PriceInfoSet = this.PriceInfoSet;
            loc1.LobbyConstants = this.LobbyConstants;
            loc1.BattleTypes = this.BattleTypes;
            loc1.EvaluationType = this.EvaluationType;
            loc1.EvaluationTopic = this.EvaluationTopic;
            loc1.URL = this.URL;
            loc1.GameMode = this.GameMode;
            loc1.UIStrings = this.UIStrings;
            loc1.RankedBattleStatus = this.RankedBattleStatus;
            loc1.RBDeny = this.RBDeny;
            loc1.Path = this.Path;
            loc1.ShipTypes = this.ShipTypes;
            loc1.Country = this.Country;
            loc1.RewardReason = this.RewardReason;
            loc1.RankBattlesStages = this.RankBattlesStages;
            loc1.ObjectTypes = this.ObjectTypes;
            loc1.ExteriorTypes = this.ExteriorTypes;
            loc1.SSETypes = this.SSETypes;
            loc1.CapturePointType = this.CapturePointType;
            loc1.ShipTypes = this.ShipTypes;
            loc1.Country = this.Country;
            loc1.ShipLevels = this.ShipLevels;
            return loc1;
        }

        public function toString():String
        {
            return "[UbGlobalDefinitions] {compile=" + String(this.compile) + ",tr=" + String(this.tr) + ",toUpperCase=" + String(this.toUpperCase) + ",toLowerCase=" + String(this.toLowerCase) + ",format=" + String(this.format) + ",timestampToHourMinSec=" + String(this.timestampToHourMinSec) + ",subscriptionHelper=" + String(this.subscriptionHelper) + ",$stage=" + String(this.$stage) + "}";
        }

        public function free():void
        {
            this.compile = null;
            this.subscriptionHelper = null;
            this.$stage = null;
            return;
        }

        public var compile:Function;

        public var tr:Function;

        public var toUpperCase:Function;

        public var toLowerCase:Function;

        public var $stage:flash.display.Stage;

        public var format:Function;

        public var timestampToHourMinSec:Function;

        public var subscriptionHelper:lesta.unbound.core.UbScopeSubscriptionHelper;

        public var dataHub:lesta.datahub.DataHub;

        public var TooltipBehaviour:Object;

        public var PriceInfoSet:Class;

        public var ChannelGroup:Object;

        public var InviteType:Object;

        public var WindowTooltipState:Object;

        public var layoutTooltips:lesta.unbound.layout.UbBlock;

        public var layoutWindows:lesta.unbound.layout.UbBlock;

        public var LobbyConstants:Class;

        public var BattleTypes:Class;

        public var URL:Class;

        public var AccountLevelConstants:Object;

        public var EvaluationType:Class;

        public var EvaluationTopic:Class;

        public var UIStrings:Class;

        public var RankedBattleStatus:Class;

        public var RBDeny:Class;

        public var RewardReason:Class;

        public var RankBattlesStages:Class;

        public var ObjectTypes:Class;

        public var ExteriorTypes:Class;

        public var SSETypes:Class;

        public var CapturePointType:Class;

        public var ShipTypes:Class;

        public var Country:Class;

        public var ShipLevels:Class;

        public var Path:Class;

        public var ExteriorConfig:Object;

        public var GameMode:Class;
    }
}
