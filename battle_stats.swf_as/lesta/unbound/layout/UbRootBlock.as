package lesta.unbound.layout 
{
    public class UbRootBlock extends lesta.unbound.layout.UbBlock
    {
        public function UbRootBlock()
        {
            super();
            return;
        }

        public override function free():void
        {
            super.free();
            if (parent) 
            {
                parent.removeChild(this);
            }
            return;
        }

        protected override function get localUpdatesEnabled():Boolean
        {
            return true;
        }
    }
}
