﻿package lesta.unbound.layout 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.unbound.style.*;
    import scaleform.gfx.*;
    
    public class UbBlock extends lesta.unbound.layout.UbStyleable
    {
        public function UbBlock()
        {
            super();
            styleFragments = {};
            styleClasses = {};
            computedStyle = new lesta.unbound.style.UbStyle();
            this.backgroundPainter = new lesta.unbound.style.UbBackgroundPainter(this);
            var loc1:*;
            this.id = cid++;
            this.addEventListener(UPDATE_ROOT, this.interceptUpdate, false, 0, true);
            return;
        }

        protected function doReflow():void
        {
            var loc1:*=null;
            var loc2:*=computedStyle.flow;
            switch (loc2) 
            {
                case lesta.unbound.style.UbStyle.FLOW_HORIZONTAL:
                {
                    this.doReflowHorizontal();
                    break;
                }
                case lesta.unbound.style.UbStyle.FLOW_VERTICAL:
                {
                    this.doReflowVertical();
                    break;
                }
                case lesta.unbound.style.UbStyle.FLOW_TILE_HORIZONTAL:
                {
                    this.doReflowTileLTR();
                    break;
                }
                case lesta.unbound.style.UbStyle.FLOW_TILE_VERTICAL:
                {
                    this.doReflowTileTTB();
                    break;
                }
            }
            if (computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) 
            {
                loc1 = parent as lesta.unbound.layout.UbBlock;
                loc1.doReflowAbsoluteChild(this);
            }
            return;
        }

        public function doReflowAbsoluteChild(arg1:lesta.unbound.layout.UbBlock):void
        {
            var loc1:*=computedStyle.scrollTop;
            var loc2:*=computedStyle.scrollLeft;
            arg1.placeAt(arg1.getMarginLeft() + loc2, arg1.getMarginTop() + loc1, _sourceWidth, _sourceHeight);
            return;
        }

        protected function doReflowHorizontal():void
        {
            var loc6:*=null;
            var loc1:*=computedStyle.scrollTop;
            var loc2:*=computedStyle.scrollLeft;
            var loc3:*=0;
            var loc4:*=0;
            var loc5:*=numChildren;
            while (loc4 < loc5) 
            {
                if ((loc6 = getChildAt(loc4) as lesta.unbound.layout.UbBlock) != null) 
                {
                    if (loc6.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) 
                    {
                        if (loc6.visible) 
                        {
                            loc3 = loc3 + loc6.getMarginLeft();
                            loc6.placeAt(loc3 + loc2, loc6.getMarginTop() + loc1, _sourceWidth, _sourceHeight);
                            loc3 = loc3 + (loc6.measuredWidth + loc6.getMarginRight());
                        }
                    }
                    else 
                    {
                        loc6.placeAt(loc6.getMarginLeft() + loc2, loc6.getMarginTop() + loc1, _sourceWidth, _sourceHeight);
                    }
                }
                ++loc4;
            }
            _contentWidth = loc3;
            return;
        }

        protected function doReflowVertical():void
        {
            var loc6:*=null;
            var loc1:*=computedStyle.scrollTop;
            var loc2:*=computedStyle.scrollLeft;
            var loc3:*=0;
            var loc4:*=0;
            var loc5:*=numChildren;
            while (loc4 < loc5) 
            {
                if ((loc6 = getChildAt(loc4) as lesta.unbound.layout.UbBlock) != null) 
                {
                    if (loc6.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) 
                    {
                        if (loc6.visible) 
                        {
                            loc3 = loc3 + loc6.getMarginTop();
                            loc6.placeAt(loc6.getMarginLeft() + loc2, loc3 + loc1, _sourceWidth, _sourceHeight);
                            loc3 = loc3 + (loc6.measuredHeight + loc6.getMarginBottom());
                        }
                    }
                    else 
                    {
                        loc6.placeAt(loc6.getMarginLeft() + loc2, loc6.getMarginTop() + loc1, _sourceWidth, _sourceHeight);
                    }
                }
                ++loc4;
            }
            _contentHeight = loc3;
            return;
        }

        protected function doReflowTileLTR():void
        {
            var loc11:*=null;
            var loc12:*=NaN;
            var loc13:*=NaN;
            var loc14:*=0;
            var loc1:*=computedStyle.scrollTop;
            var loc2:*=computedStyle.scrollLeft;
            var loc3:*=0;
            var loc4:*=0;
            var loc5:*=0;
            var loc6:*=new Array();
            var loc7:*=new Array();
            var loc8:*=computedStyle.align;
            var loc9:*=0;
            var loc10:*=numChildren;
            while (loc9 < loc10) 
            {
                if ((loc11 = getChildAt(loc9) as lesta.unbound.layout.UbBlock) != null) 
                {
                    if (loc11.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) 
                    {
                        if (loc11.visible) 
                        {
                            loc5 = Math.max(loc5, getChildsVerticalSize(loc11));
                            loc3 = loc3 + loc11.getMarginLeft();
                            if (loc8 == lesta.unbound.style.UbStyle.ALIGN_LEFT) 
                            {
                                loc11.placeAt(loc3 + loc2, loc1 + loc4 + loc11.getMarginTop(), _sourceWidth, _sourceHeight);
                            }
                            else 
                            {
                                loc7.push([loc3 + loc2, loc1 + loc4 + loc11.getMarginTop()]);
                                loc6.push(loc11);
                            }
                            loc3 = loc3 + (loc11.measuredWidth + loc11.getMarginRight());
                            if ((loc12 = loc3 + getChildsHorizontalSize(loc11)) > measuredWidth || loc9 == (loc10 - 1)) 
                            {
                                loc13 = 0;
                                if (loc8 != lesta.unbound.style.UbStyle.ALIGN_LEFT) 
                                {
                                    if (loc8 != lesta.unbound.style.UbStyle.ALIGN_CENTER) 
                                    {
                                        if (loc8 == lesta.unbound.style.UbStyle.ALIGN_RIGHT) 
                                        {
                                            loc13 = measuredWidth - loc3;
                                        }
                                    }
                                    else 
                                    {
                                        loc13 = (measuredWidth - loc3) / 2;
                                    }
                                    loc14 = 0;
                                    while (loc14 < loc6.length) 
                                    {
                                        loc6[loc14].placeAt(loc7[loc14][0] + loc13, loc7[loc14][1], _sourceWidth, _sourceHeight);
                                        ++loc14;
                                    }
                                    loc6 = new Array();
                                    loc7 = new Array();
                                }
                                loc3 = 0;
                                loc4 = loc4 + loc5;
                                loc5 = 0;
                            }
                        }
                    }
                    else 
                    {
                        loc11.placeAt(loc11.getMarginLeft() + loc2, loc11.getMarginTop() + loc1, _sourceWidth, _sourceHeight);
                    }
                }
                ++loc9;
            }
            _contentHeight = loc4 + loc5;
            return;
        }

        protected function doReflowTileTTB():void
        {
            var loc8:*=null;
            var loc9:*=NaN;
            var loc1:*=computedStyle.scrollTop;
            var loc2:*=computedStyle.scrollLeft;
            var loc3:*=loc2;
            var loc4:*=0;
            var loc5:*=0;
            var loc6:*=0;
            var loc7:*=numChildren;
            while (loc6 < loc7) 
            {
                if ((loc8 = getChildAt(loc6) as lesta.unbound.layout.UbBlock) != null) 
                {
                    if (loc8.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) 
                    {
                        if (loc8.visible) 
                        {
                            if ((loc9 = loc4 + getChildsVerticalSize(loc8)) > measuredHeight) 
                            {
                                loc4 = 0;
                                loc3 = loc3 + loc5;
                                loc5 = 0;
                            }
                            loc5 = Math.max(loc5, getChildsHorizontalSize(loc8));
                            loc4 = loc4 + loc8.getMarginTop();
                            loc8.placeAt(loc3 + loc8.getMarginLeft(), loc4 + loc1, _sourceWidth, _sourceHeight);
                            loc4 = loc4 + (loc8.measuredHeight + loc8.getMarginBottom());
                        }
                    }
                    else 
                    {
                        loc8.placeAt(loc8.getMarginLeft() + loc2, loc8.getMarginTop() + loc1, _sourceWidth, _sourceHeight);
                    }
                }
                ++loc6;
            }
            _contentWidth = loc3;
            return;
        }

        protected function placeAt(arg1:Number, arg2:Number, arg3:Number, arg4:Number):void
        {
            var loc1:*=NaN;
            var loc2:*=NaN;
            var loc3:*=NaN;
            var loc4:*=NaN;
            if (isNaN(computedStyle.right)) 
            {
                if (isNaN(computedStyle.left)) 
                {
                    this.lastInternallySetX = 0;
                }
                else 
                {
                    loc2 = computeDimensionValue(computedStyle.left, computedStyle.leftMode, arg3);
                    this.lastInternallySetX = arg1 + loc2;
                }
            }
            else 
            {
                loc1 = computeDimensionValue(computedStyle.right, computedStyle.rightMode, arg3);
                this.lastInternallySetX = arg1 + arg3 - _sourceWidth - loc1;
            }
            if (isNaN(computedStyle.bottom)) 
            {
                if (isNaN(computedStyle.top)) 
                {
                    this.lastInternallySetY = 0;
                }
                else 
                {
                    loc4 = computeDimensionValue(computedStyle.top, computedStyle.topMode, arg4);
                    this.lastInternallySetY = arg2 + loc4;
                }
            }
            else 
            {
                loc3 = computeDimensionValue(computedStyle.bottom, computedStyle.bottomMode, arg4);
                this.lastInternallySetY = arg2 + arg4 - _sourceHeight - loc3;
            }
            super.x = this.lastExternallySetX + this.lastInternallySetX;
            super.y = this.lastExternallySetY + this.lastInternallySetY;
            return;
        }

        protected function dispatchUpdate():void
        {
            if (parent == null) 
            {
                return;
            }
            if (stage) 
            {
                stage.invalidate();
            }
            if (this.localUpdatesEnabled) 
            {
                this.update();
            }
            else 
            {
                parent.dispatchEvent(new flash.events.Event(UPDATE_ROOT, true));
            }
            return;
        }

        public override function get width():Number
        {
            return super.measuredWidth;
        }

        protected function updateMouse(arg1:Boolean, arg2:Boolean, arg3:Boolean):void
        {
            mouseEnabled = arg1;
            mouseChildren = arg2;
            scaleform.gfx.InteractiveObjectEx.setHitTestDisable(this, !arg3);
            return;
        }

        protected function redraw():void
        {
            if (!visible || parent == null) 
            {
                return;
            }
            if (computedStyle && computedStyle.dirty > 0) 
            {
                this.recomputeStyle();
                propagateDirtyProperties();
                measureX();
                measureY();
                postMeasureHook();
                this.reflowAndRepaint(measureEnabled);
                updateMeasured();
                dispatchEvent(new flash.events.Event(flash.events.Event.RESIZE));
            }
            return;
        }

        protected function checkDirtyOnNextFrame(arg1:flash.events.Event):void
        {
            removeEventListener(flash.events.Event.RENDER, this.checkDirtyOnNextFrame);
            removeEventListener(flash.events.Event.ENTER_FRAME, this.checkDirtyOnNextFrame);
            this.redraw();
            return;
        }

        internal function interceptUpdate(arg1:flash.events.Event):void
        {
            if (this.localUpdatesEnabled) 
            {
                arg1.stopImmediatePropagation();
                if (computedStyle) 
                {
                    computedStyle.dirty = computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_REFLOW | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y | lesta.unbound.style.UbStyle.DIRTY_PAINT;
                }
                this.update();
            }
            return;
        }

        public override function get x():Number
        {
            return this.lastExternallySetX;
        }

        public override function set x(arg1:Number):void
        {
            this.lastExternallySetX = arg1;
            super.x = this.lastExternallySetX + this.lastInternallySetX;
            return;
        }

        public override function get y():Number
        {
            return this.lastExternallySetY;
        }

        public override function set y(arg1:Number):void
        {
            this.lastExternallySetY = arg1;
            super.y = this.lastExternallySetY + this.lastInternallySetY;
            return;
        }

        
        {
            cid = 0;
        }

        public override function set width(arg1:Number):void
        {
            if (arg1 != styleFragments.width) 
            {
                computedStyle.dirty = computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X;
                styleFragments.width = arg1;
                this.invalidateStyleRecompute();
            }
            return;
        }

        public override function set visible(arg1:Boolean):void
        {
            if (arg1 != super.visible) 
            {
                this.invalidateChildren();
                super.visible = arg1;
                if (this.callbackOnVisibleChanged != null) 
                {
                    this.callbackOnVisibleChanged.call(null, arg1);
                }
            }
            return;
        }

        public override function get height():Number
        {
            return super.measuredHeight;
        }

        public override function set height(arg1:Number):void
        {
            if (arg1 != styleFragments.height) 
            {
                computedStyle.dirty = computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
                styleFragments.height = arg1;
                this.invalidateStyleRecompute();
            }
            return;
        }

        public function set ubScaleX(arg1:Number):void
        {
            if (arg1 != styleFragments.scaleX) 
            {
                styleFragments.scaleX = arg1;
                this.invalidateStyleRecompute();
            }
            return;
        }

        public function get ubScaleX():Number
        {
            return computedStyle.scaleX;
        }

        public function set ubScaleY(arg1:Number):void
        {
            if (arg1 != styleFragments.scaleY) 
            {
                styleFragments.scaleY = arg1;
                this.invalidateStyleRecompute();
            }
            return;
        }

        public function get ubScaleY():Number
        {
            return computedStyle.scaleY;
        }

        public function get nativeWidth():Number
        {
            return super.width;
        }

        public function get nativeHeight():Number
        {
            return super.height;
        }

        public function get nativeX():Number
        {
            return this.x;
        }

        public function get nativeY():Number
        {
            return this.y;
        }

        public function get left():Number
        {
            return this.lastExternallySetX;
        }

        public function set left(arg1:Number):void
        {
            this.x = arg1;
            return;
        }

        public function get top():Number
        {
            return this.lastExternallySetY;
        }

        public function set top(arg1:Number):void
        {
            this.y = arg1;
            return;
        }

        public function get style():Object
        {
            this.invalidateStyleRecompute();
            return styleFragments;
        }

        protected function get localUpdatesEnabled():Boolean
        {
            return _isolatedPosition || this.disableParentUpdate;
        }

        public override function free():void
        {
            var loc4:*=null;
            this.backgroundPainter.fini();
            removeEventListener(flash.events.Event.RENDER, this.checkDirtyOnNextFrame);
            removeEventListener(flash.events.Event.ENTER_FRAME, this.checkDirtyOnNextFrame);
            removeEventListener(UPDATE_ROOT, this.interceptUpdate);
            var loc1:*=[];
            var loc2:*=this.numChildren;
            var loc3:*=0;
            while (loc3 < loc2) 
            {
                if ((loc4 = this.getChildAt(loc3) as lesta.unbound.layout.UbBlock) != null) 
                {
                    loc1.push(loc4);
                }
                ++loc3;
            }
            while (loc1.length) 
            {
                loc1.pop().free();
            }
            if (this.scrollinglController) 
            {
                this.scrollinglController.free();
            }
            this.scrollinglController = null;
            this.prevScrollingController = null;
            this.factory = null;
            if (this.bindings) 
            {
                while (this.bindings.length > 0) 
                {
                    this.bindings.pop().free();
                }
            }
            this.bindings = null;
            this.bindingsSettings = null;
            super.free();
            return;
        }

        protected override function recomputeStyle():void
        {
            if (!visible || parent == null) 
            {
                return;
            }
            super.recomputeStyle();
            if (this.prevScrollingController != computedStyle.scrollbarController) 
            {
                if (this.scrollinglController) 
                {
                    this.scrollinglController.free();
                }
                this.scrollinglController = new computedStyle.scrollbarController() as lesta.unbound.style.IScrollingController;
                this.scrollinglController.init(this, this.doScroll, this.beginScroll, this.endScroll);
                this.prevScrollingController = computedStyle.scrollbarController;
            }
            return;
        }

        public override function toString():String
        {
            return "[" + flash.utils.getQualifiedClassName(this) + " " + name + "_" + this.id + "]";
        }

        protected override function invalidateStyleRecompute():void
        {
            if (!styleNeedsToBeRecomputed) 
            {
                super.invalidateStyleRecompute();
                this.dispatchUpdate();
            }
            return;
        }

        public function getBindingsTarget(arg1:String):flash.display.DisplayObject
        {
            return this;
        }

        public function getFiltersTarget():flash.display.DisplayObject
        {
            return this;
        }

        public function setFactory(arg1:lesta.unbound.layout.UbBlockFactory):void
        {
            this.factory = arg1;
            return;
        }

        public function getFactory():lesta.unbound.layout.UbBlockFactory
        {
            return this.factory;
        }

        public function addStyleClass(arg1:String):void
        {
            var loc1:*=null;
            if (!(arg1 in styleClasses) && !(arg1 in styleClassesBase)) 
            {
                loc1 = this.factory.getStyleClassById(arg1);
                if (loc1 != null) 
                {
                    styleClasses[arg1] = loc1;
                }
                this.invalidateStyleRecompute();
            }
            return;
        }

        public function removeStyleClass(arg1:String):void
        {
            if (arg1 in styleClasses) 
            {
                delete styleClasses[arg1];
                this.invalidateStyleRecompute();
            }
            return;
        }

        public function addChildProperly(arg1:lesta.unbound.layout.UbBlock):void
        {
            addChild(arg1);
            this.invalidateChildren();
            return;
        }

        public function invalidateChildren():void
        {
            if (computedStyle) 
            {
                computedStyle.dirty = computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_REFLOW | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y | lesta.unbound.style.UbStyle.DIRTY_PAINT;
            }
            this.dispatchUpdate();
            return;
        }

        public function invalidateFlow(arg1:Boolean=false):void
        {
            if (arg1) 
            {
                _measuredOnce = false;
            }
            if (computedStyle) 
            {
                computedStyle.dirty = computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_REFLOW;
            }
            this.dispatchUpdate();
            return;
        }

        public function removeChildProperly(arg1:lesta.unbound.layout.UbBlock):void
        {
            removeChild(arg1);
            this.invalidateChildren();
            return;
        }

        public function scrollTo(arg1:Number, arg2:Function=null):void
        {
            if (this.scrollinglController == null) 
            {
                computedStyle.scrollPosition = arg1;
            }
            else 
            {
                this.scrollinglController.scrollToFixed(arg1, arg2);
            }
            return;
        }

        public function update(arg1:Boolean=false):void
        {
            if (arg1) 
            {
                this.redraw();
            }
            else 
            {
                addEventListener(flash.events.Event.RENDER, this.checkDirtyOnNextFrame, false, 0, true);
                addEventListener(flash.events.Event.ENTER_FRAME, this.checkDirtyOnNextFrame, false, 0, true);
                if (stage) 
                {
                    stage.invalidate();
                }
            }
            return;
        }

        protected function reflowAndRepaint(arg1:Boolean=true):void
        {
            var loc3:*=null;
            if (!visible || parent == null) 
            {
                return;
            }
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_REFLOW) 
            {
                if (arg1) 
                {
                    this.doReflow();
                }
            }
            var loc1:*=0;
            var loc2:*=numChildren;
            while (loc1 < loc2) 
            {
                if ((loc3 = getChildAt(loc1) as lesta.unbound.layout.UbBlock) != null) 
                {
                    loc3.reflowAndRepaint(arg1);
                }
                ++loc1;
            }
            this.commonStyleDirtyChecks();
            if (this.scrollinglController) 
            {
                this.scrollinglController.update(_contentWidth, _contentHeight);
            }
            computedStyle.dirty = 0;
            dispatchEvent(new flash.events.Event(BLOCK_REFLOW));
            return;
        }

        protected function commonStyleDirtyChecks():void
        {
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_PAINT) 
            {
                this.backgroundPainter.paintBackground();
            }
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_FILTERS) 
            {
                this.backgroundPainter.paintFilters();
            }
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_ALPHA && !isNaN(computedStyle.alpha)) 
            {
                alpha = computedStyle.alpha;
            }
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MOUSE) 
            {
                this.updateMouse(computedStyle.mouseEnabled, computedStyle.mouseChildren, computedStyle.hitTest);
            }
            return;
        }

        protected function doScroll():void
        {
            alpha = alpha;
            this.doReflow();
            return;
        }

        protected function beginScroll():void
        {
            mouseChildren = false;
            return;
        }

        protected function endScroll():void
        {
            mouseChildren = computedStyle.mouseChildren;
            dispatchEvent(new flash.events.Event(END_SCROLL));
            return;
        }

        public static const END_SCROLL:String="UbBlock.END_SCROLL";

        public static const BLOCK_REFLOW:String="UbBlock.BLOCK_REFLOW";

        public static const UPDATE_ROOT:String="UbBlock.UPDATE_ROOT";

        protected var backgroundPainter:lesta.unbound.style.UbBackgroundPainter;

        public var bindingsSettings:__AS3__.vec.Vector.<lesta.unbound.layout.UbBindingData>;

        public var bindings:__AS3__.vec.Vector;

        public var id:int=0;

        public var callbackOnVisibleChanged:Function=null;

        public var disableParentUpdate:Boolean=false;

        protected var lastExternallySetX:int=0;

        protected var lastExternallySetY:int=0;

        protected var lastInternallySetY:int=0;

        protected var factory:lesta.unbound.layout.UbBlockFactory;

        protected var scrollinglController:lesta.unbound.style.IScrollingController;

        protected var prevScrollingController:Class;

        protected var lastInternallySetX:int=0;

        protected static var cid:int=0;
    }
}
