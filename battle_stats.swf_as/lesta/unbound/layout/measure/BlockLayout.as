package lesta.unbound.layout.measure 
{
    import flash.events.*;
    import lesta.unbound.layout.*;
    import lesta.unbound.style.*;
    
    public class BlockLayout extends lesta.unbound.layout.measure.ScrollableLayout
    {
        public function BlockLayout(arg1:lesta.unbound.layout.UbBlock)
        {
            super(arg1);
            return;
        }

        internal static function getChildsHorizontalSize(arg1:lesta.unbound.layout.UbStyleable):Number
        {
            return arg1.visible ? arg1.layout.dimensions.fullWidth : 0;
        }

        internal static function getChildsVerticalSize(arg1:lesta.unbound.layout.UbStyleable):Number
        {
            return arg1.visible ? arg1.layout.dimensions.fullHeight : 0;
        }

        public override function apply():void
        {
            if (!measureEnabled) 
            {
                return;
            }
            this.propagateDirtyProperties();
            prepare();
            this.measureX();
            this.measureY();
            this.postMeasure();
            this.repaint();
            return;
        }

        protected function get numChildren():int
        {
            return _block.numChildren;
        }

        protected function getChildAt(arg1:int):lesta.unbound.layout.UbBlock
        {
            return _block.getChildAt(arg1) as lesta.unbound.layout.UbBlock;
        }

        public override function fini():void
        {
            if (scrollinglController) 
            {
                scrollinglController.free();
            }
            scrollinglController = null;
            prevScrollingController = null;
            super.fini();
            return;
        }

        protected function get parent():lesta.unbound.layout.UbStyleable
        {
            return _block.parent as lesta.unbound.layout.UbStyleable;
        }

        protected function fixContentSize():void
        {
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                this.fixChildrenWidths();
            }
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
            {
                this.fixChildrenHeights();
            }
            return;
        }

        public override function postMeasure():void
        {
            var loc1:*=null;
            if (!measureEnabled) 
            {
                return;
            }
            this.fixContentSize();
            var loc2:*=0;
            var loc3:*=this.numChildren;
            while (loc2 < loc3) 
            {
                loc1 = this.getChildAt(loc2) as lesta.unbound.layout.UbStyleable;
                if (loc1 != null) 
                {
                    loc1.layout.postMeasure();
                }
                ++loc2;
            }
            return;
        }

        public override function measureX():void
        {
            if (!measureEnabled) 
            {
                return;
            }
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                if (_block.computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
                {
                    if (_block.computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_FILL) 
                    {
                        this.doMeasureX();
                        this.recursiveMeasureX();
                    }
                    else 
                    {
                        this.recursiveMeasureX();
                        this.doMeasureX();
                    }
                }
                else 
                {
                    this.recursiveMeasureX();
                    this.doMeasureX();
                }
            }
            else 
            {
                this.recursiveMeasureX();
            }
            return;
        }

        public override function measureY():void
        {
            if (!measureEnabled) 
            {
                return;
            }
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
            {
                if (_block.computedStyle.heightMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
                {
                    if (_block.computedStyle.heightMode != lesta.unbound.style.UbStyle.DIMENSION_FILL) 
                    {
                        this.doMeasureY();
                        this.recursiveMeasureY();
                    }
                    else 
                    {
                        this.recursiveMeasureY();
                        this.doMeasureY();
                    }
                }
                else 
                {
                    this.recursiveMeasureY();
                    this.doMeasureY();
                }
            }
            else 
            {
                this.recursiveMeasureY();
            }
            return;
        }

        protected function recursiveMeasureX():void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc4:*=NaN;
            var loc5:*=NaN;
            var loc6:*=NaN;
            var loc7:*=NaN;
            _contentWidth = 0;
            var loc3:*=0;
            if (_block.computedStyle.flow == lesta.unbound.style.UbStyle.FLOW_TILE_VERTICAL) 
            {
                loc4 = 0;
                loc5 = 0;
                loc6 = 0;
                loc2 = 0;
                loc3 = this.numChildren;
                while (loc2 < loc3) 
                {
                    loc1 = this.getChildAt(loc2) as lesta.unbound.layout.UbStyleable;
                    if (!(loc1 == null) && !(loc1.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc1.visible) 
                    {
                        loc1.layout.measureX();
                        if ((loc7 = loc5 + getChildsVerticalSize(loc1)) > dimensions.measuredHeight) 
                        {
                            loc5 = 0;
                            loc4 = loc4 + loc6;
                            loc6 = 0;
                        }
                        loc6 = Math.max(loc6, getChildsHorizontalSize(loc1));
                        loc5 = loc5 + (loc1.layout.dimensions.measuredHeight + loc1.layout.dimensions.marginBottom + loc1.layout.dimensions.marginTop);
                    }
                    ++loc2;
                }
                _contentWidth = loc4 + loc6;
            }
            else 
            {
                loc2 = 0;
                loc3 = this.numChildren;
                while (loc2 < loc3) 
                {
                    loc1 = this.getChildAt(loc2) as lesta.unbound.layout.UbStyleable;
                    if (loc1 != null) 
                    {
                        loc1.layout.measureX();
                        if (!(loc1.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc1.visible) 
                        {
                            _contentWidth = _contentWidth + getChildsHorizontalSize(loc1);
                        }
                    }
                    ++loc2;
                }
            }
            return;
        }

        protected function recursiveMeasureY():void
        {
            var loc3:*=null;
            _contentHeight = 0;
            var loc1:*=0;
            var loc2:*=this.numChildren;
            while (loc1 < loc2) 
            {
                loc3 = this.getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                if (loc3 != null) 
                {
                    loc3.layout.measureY();
                    if (!(loc3.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc3.visible) 
                    {
                        _contentHeight = _contentHeight + getChildsVerticalSize(loc3);
                    }
                }
                ++loc1;
            }
            return;
        }

        protected function doMeasureX(arg1:Number=NaN):void
        {
            var loc1:*=_block.computedStyle.widthMode;
            var loc2:*=_block.computedStyle.width;
            var loc3:*=dimensions.marginLeft + dimensions.marginRight;
            if (isNaN(arg1)) 
            {
                if (_block.computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_FILL) 
                {
                    arg1 = this.parent ? this.parent.layout.dimensions.width : 0;
                }
                else 
                {
                    arg1 = this.parent ? (this.parent.layout as lesta.unbound.layout.measure.BlockLayout).measuredAvailableWidth : 0;
                }
            }
            var loc4:*=lesta.unbound.style.UbStyle.FLOW_HORIZONTAL;
            var loc5:*=dimensions.width;
            dimensions.width = this.clampWidth(this.doMeasureSize(loc1, loc2, arg1, loc3, loc4, getChildsHorizontalSize));
            if (dimensions.width != loc5) 
            {
                _block.computedStyle.dirty = _block.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_PAINT | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X;
            }
            return;
        }

        protected function clampWidth(arg1:Number):Number
        {
            if (arg1 > _block.computedStyle.maxWidth) 
            {
                return _block.computedStyle.maxWidth;
            }
            if (arg1 < _block.computedStyle.minWidth) 
            {
                return _block.computedStyle.minWidth;
            }
            return arg1;
        }

        protected function doMeasureY(arg1:Number=NaN):void
        {
            var loc1:*=_block.computedStyle.heightMode;
            var loc2:*=_block.computedStyle.height;
            var loc3:*=dimensions.marginTop + dimensions.marginBottom;
            if (isNaN(arg1)) 
            {
                if (_block.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_FILL && this.parent.layout is lesta.unbound.layout.measure.BlockLayout) 
                {
                    arg1 = this.parent ? (this.parent.layout as lesta.unbound.layout.measure.BlockLayout).measuredAvailableHeight : 0;
                }
                else 
                {
                    arg1 = this.parent ? this.parent.layout.dimensions.height : 0;
                }
            }
            var loc4:*=lesta.unbound.style.UbStyle.FLOW_VERTICAL;
            var loc5:*=getChildsVerticalSize;
            var loc6:*=dimensions.height;
            dimensions.height = this.clampHeight(this.doMeasureSize(loc1, loc2, arg1, loc3, loc4, loc5));
            if (dimensions.height != loc6) 
            {
                _block.computedStyle.dirty = _block.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_PAINT | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
            }
            return;
        }

        protected function clampHeight(arg1:Number):Number
        {
            if (arg1 > _block.computedStyle.maxHeight) 
            {
                return _block.computedStyle.maxHeight;
            }
            if (arg1 < _block.computedStyle.minHeight) 
            {
                return _block.computedStyle.minHeight;
            }
            return arg1;
        }

        protected function fixChildrenWidths():void
        {
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=NaN;
            var loc1:*=this.numChildren;
            if (_block.computedStyle.flow == lesta.unbound.style.UbStyle.FLOW_HORIZONTAL || _block.computedStyle.flow == lesta.unbound.style.UbStyle.FLOW_TILE_VERTICAL) 
            {
                this._totalAbsoluteContentWidth = 0;
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = this.getChildAt(loc3) as lesta.unbound.layout.UbStyleable;
                    if (!(loc2 == null) && loc2.visible && !(loc2.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && (loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE || loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT)) 
                    {
                        this._totalAbsoluteContentWidth = this._totalAbsoluteContentWidth + getChildsHorizontalSize(loc2);
                    }
                    ++loc3;
                }
                loc4 = Math.max(0, dimensions.width - this._totalAbsoluteContentWidth);
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = this.getChildAt(loc3);
                    if (!(loc2 == null) && loc2.layout is lesta.unbound.layout.measure.BlockLayout && (loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_FILL)) 
                    {
                        (loc2.layout as lesta.unbound.layout.measure.BlockLayout).doMeasureX(loc2.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE ? loc4 : dimensions.width);
                        (loc2.layout as lesta.unbound.layout.measure.BlockLayout).fixChildrenWidths();
                    }
                    ++loc3;
                }
            }
            else 
            {
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = this.getChildAt(loc3);
                    if (!(loc2 == null) && loc2.layout is lesta.unbound.layout.measure.BlockLayout && (loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_FILL)) 
                    {
                        (loc2.layout as lesta.unbound.layout.measure.BlockLayout).doMeasureX(dimensions.width);
                        (loc2.layout as lesta.unbound.layout.measure.BlockLayout).fixChildrenWidths();
                    }
                    ++loc3;
                }
            }
            return;
        }

        protected function fixChildrenHeights():void
        {
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=NaN;
            var loc1:*=this.numChildren;
            if (_block.computedStyle.flow != lesta.unbound.style.UbStyle.FLOW_VERTICAL) 
            {
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = this.getChildAt(loc3) as lesta.unbound.layout.UbStyleable;
                    if (!(loc2 == null) && loc2.layout is lesta.unbound.layout.measure.BlockLayout && (loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_FILL)) 
                    {
                        (loc2.layout as lesta.unbound.layout.measure.BlockLayout).doMeasureY(dimensions.height);
                        (loc2.layout as lesta.unbound.layout.measure.BlockLayout).fixChildrenHeights();
                    }
                    ++loc3;
                }
            }
            else 
            {
                this._totalAbsoluteContentHeight = 0;
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = this.getChildAt(loc3) as lesta.unbound.layout.UbStyleable;
                    if (!(loc2 == null) && loc2.visible && !(loc2.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && (loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE || loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT)) 
                    {
                        this._totalAbsoluteContentHeight = this._totalAbsoluteContentHeight + getChildsVerticalSize(loc2);
                    }
                    ++loc3;
                }
                loc4 = Math.max(0, dimensions.height - this._totalAbsoluteContentHeight);
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = this.getChildAt(loc3) as lesta.unbound.layout.UbStyleable;
                    if (!(loc2 == null) && loc2.layout is lesta.unbound.layout.measure.BlockLayout && (loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_FILL)) 
                    {
                        (loc2.layout as lesta.unbound.layout.measure.BlockLayout).doMeasureY(loc2.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE ? loc4 : dimensions.height);
                        (loc2.layout as lesta.unbound.layout.measure.BlockLayout).fixChildrenHeights();
                    }
                    ++loc3;
                }
            }
            return;
        }

        protected function doMeasureSize(arg1:int, arg2:Number, arg3:Number, arg4:Number, arg5:int, arg6:Function):Number
        {
            var loc1:*=NaN;
            var loc2:*=NaN;
            var loc3:*=arg1;
            switch (loc3) 
            {
                case lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE:
                case lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH:
                case lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT:
                {
                    loc1 = dimensions.computeDimensionValue(arg2, arg1, arg3);
                    break;
                }
                case lesta.unbound.style.UbStyle.DIMENSION_RELATIVE:
                {
                    loc1 = dimensions.computeDimensionValue(arg2, arg1, arg3);
                    loc1 = Math.max(0, loc1 - arg4);
                    break;
                }
                case lesta.unbound.style.UbStyle.DIMENSION_FILL:
                {
                    loc1 = dimensions.computeDimensionValue(arg2, arg1, arg3);
                    loc1 = Math.max(0, loc1 - arg4);
                    loc2 = this.doMeasureAuto(arg5, arg6);
                    loc1 = Math.min(loc1, loc2);
                    break;
                }
                default:
                {
                    loc1 = this.doMeasureAuto(arg5, arg6);
                    break;
                }
            }
            return loc1;
        }

        protected function doMeasureAuto(arg1:int, arg2:Function):Number
        {
            var measureDirection:int;
            var getChildsSize:Function;
            var flowDirection:int;
            var measureAsStack:Function;
            var measureHeightTileLTR:Function;
            var measureWidthTileTTB:Function;
            var measureMax:Function;

            var loc1:*;
            measureDirection = arg1;
            getChildsSize = arg2;
            measureAsStack = function ():Number
            {
                var loc4:*=null;
                var loc1:*=0;
                var loc2:*=0;
                var loc3:*=numChildren;
                while (loc2 < loc3) 
                {
                    if (!((loc4 = getChildAt(loc2) as lesta.unbound.layout.UbStyleable) == null) && !(loc4.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE)) 
                    {
                        loc1 = loc1 + getChildsSize(loc4);
                    }
                    ++loc2;
                }
                return loc1;
            }
            measureHeightTileLTR = function ():Number
            {
                var loc6:*=null;
                var loc7:*=NaN;
                var loc8:*=NaN;
                var loc1:*=0;
                var loc2:*=0;
                var loc3:*=0;
                var loc4:*=0;
                var loc5:*=numChildren;
                while (loc4 < loc5) 
                {
                    if (!((loc6 = getChildAt(loc4) as lesta.unbound.layout.UbStyleable) == null) && !(loc6.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc6.visible) 
                    {
                        loc7 = getChildsHorizontalSize(loc6);
                        if ((loc8 = loc1 + loc7) > dimensions.measuredWidth) 
                        {
                            loc1 = 0;
                            loc2 = loc2 + loc3;
                            loc3 = 0;
                        }
                        loc3 = Math.max(loc3, getChildsVerticalSize(loc6));
                        loc1 = loc1 + loc7;
                    }
                    ++loc4;
                }
                loc2 = loc2 + loc3;
                return loc2;
            }
            measureWidthTileTTB = function ():Number
            {
                var loc6:*=null;
                var loc7:*=NaN;
                var loc1:*=0;
                var loc2:*=0;
                var loc3:*=0;
                var loc4:*=0;
                var loc5:*=numChildren;
                while (loc4 < loc5) 
                {
                    if (!((loc6 = getChildAt(loc4)) == null) && !(loc6.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc6.visible) 
                    {
                        if ((loc7 = loc2 + getChildsVerticalSize(loc6)) > dimensions.measuredHeight) 
                        {
                            loc2 = 0;
                            loc1 = loc1 + loc3;
                            loc3 = 0;
                        }
                        loc3 = Math.max(loc3, getChildsHorizontalSize(loc6));
                        loc2 = loc2 + loc6.layout.dimensions.fullHeight;
                    }
                    ++loc4;
                }
                loc1 = loc1 + loc3;
                return loc1;
            }
            measureMax = function ():Number
            {
                var loc4:*=null;
                var loc5:*=NaN;
                var loc1:*=0;
                var loc2:*=0;
                var loc3:*=numChildren;
                while (loc2 < loc3) 
                {
                    if (!((loc4 = getChildAt(loc2) as lesta.unbound.layout.UbStyleable) == null) && !(loc4.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE)) 
                    {
                        loc1 = (loc5 = getChildsSize(loc4)) > loc1 ? loc5 : loc1;
                    }
                    ++loc2;
                }
                return loc1;
            }
            flowDirection = _block.computedStyle.flow > 1 ? _block.computedStyle.flow - 2 : _block.computedStyle.flow;
            if (measureDirection == flowDirection) 
            {
                return measureAsStack();
            }
            if (_block.computedStyle.flow == lesta.unbound.style.UbStyle.FLOW_TILE_VERTICAL) 
            {
                return measureWidthTileTTB();
            }
            if (_block.computedStyle.flow == lesta.unbound.style.UbStyle.FLOW_TILE_HORIZONTAL) 
            {
                return measureHeightTileLTR();
            }
            return measureMax();
        }

        protected function get measuredAvailableWidth():Number
        {
            var loc1:*=NaN;
            if (this.parent.layout is lesta.unbound.layout.measure.BlockLayout && (_block.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || _block.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_FILL)) 
            {
                loc1 = _block.computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_FILL ? 1 : _block.computedStyle.width / 100;
                return (this.parent.layout as lesta.unbound.layout.measure.BlockLayout).measuredAvailableWidth * loc1 - this._totalAbsoluteContentWidth;
            }
            return dimensions.width - this._totalAbsoluteContentWidth;
        }

        protected function get measuredAvailableHeight():Number
        {
            var loc1:*=NaN;
            if (this.parent.layout is lesta.unbound.layout.measure.BlockLayout && (_block.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || _block.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_FILL)) 
            {
                loc1 = _block.computedStyle.heightMode != lesta.unbound.style.UbStyle.DIMENSION_FILL ? 1 : _block.computedStyle.height / 100;
                return (this.parent.layout as lesta.unbound.layout.measure.BlockLayout).measuredAvailableHeight * loc1 - this._totalAbsoluteContentHeight;
            }
            return dimensions.height - this._totalAbsoluteContentHeight;
        }

        public override function reflow():void
        {
            if (!measureEnabled) 
            {
                return;
            }
            var loc1:*=_block.computedStyle.flow;
            switch (loc1) 
            {
                case lesta.unbound.style.UbStyle.FLOW_HORIZONTAL:
                {
                    this.doReflowHorizontal();
                    break;
                }
                case lesta.unbound.style.UbStyle.FLOW_VERTICAL:
                {
                    this.doReflowVertical();
                    break;
                }
                case lesta.unbound.style.UbStyle.FLOW_TILE_HORIZONTAL:
                {
                    this.doReflowTileLTR();
                    break;
                }
                case lesta.unbound.style.UbStyle.FLOW_TILE_VERTICAL:
                {
                    this.doReflowTileTTB();
                    break;
                }
            }
            if (_block.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE && this.parent.layout is lesta.unbound.layout.measure.BlockLayout) 
            {
                (this.parent.layout as lesta.unbound.layout.measure.BlockLayout).doReflowAbsoluteChild(_block);
            }
            return;
        }

        internal function doReflowAbsoluteChild(arg1:lesta.unbound.layout.UbStyleable):void
        {
            var loc1:*=_block.computedStyle.scrollTop;
            var loc2:*=_block.computedStyle.scrollLeft;
            arg1.layout.dimensions.updatePosition(arg1.layout.dimensions.marginLeft + loc2, arg1.layout.dimensions.marginTop + loc1, dimensions.width, dimensions.height);
            return;
        }

        protected function doReflowHorizontal():void
        {
            var loc6:*=null;
            var loc1:*=_block.computedStyle.scrollTop;
            var loc2:*=_block.computedStyle.scrollLeft;
            var loc3:*=0;
            var loc4:*=0;
            var loc5:*=this.numChildren;
            while (loc4 < loc5) 
            {
                if ((loc6 = this.getChildAt(loc4)) != null) 
                {
                    if (loc6.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) 
                    {
                        if (loc6.visible) 
                        {
                            loc3 = loc3 + loc6.layout.dimensions.marginLeft;
                            loc6.layout.dimensions.updatePosition(loc3 + loc2, loc6.layout.dimensions.marginTop + loc1, dimensions.width, dimensions.height);
                            loc3 = loc3 + (loc6.layout.dimensions.measuredWidth + loc6.layout.dimensions.marginRight);
                        }
                    }
                    else 
                    {
                        loc6.layout.dimensions.updatePosition(loc6.layout.dimensions.marginLeft + loc2, loc6.layout.dimensions.marginTop + loc1, dimensions.width, dimensions.height);
                    }
                }
                ++loc4;
            }
            _contentWidth = loc3;
            return;
        }

        protected function doReflowVertical():void
        {
            var loc6:*=null;
            var loc1:*=_block.computedStyle.scrollTop;
            var loc2:*=_block.computedStyle.scrollLeft;
            var loc3:*=0;
            var loc4:*=0;
            var loc5:*=this.numChildren;
            while (loc4 < loc5) 
            {
                if ((loc6 = this.getChildAt(loc4)) != null) 
                {
                    if (loc6.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) 
                    {
                        if (loc6.visible) 
                        {
                            loc3 = loc3 + loc6.layout.dimensions.marginTop;
                            loc6.layout.dimensions.updatePosition(loc6.layout.dimensions.marginLeft + loc2, loc3 + loc1, dimensions.width, dimensions.height);
                            loc3 = loc3 + (loc6.layout.dimensions.measuredHeight + loc6.layout.dimensions.marginBottom);
                        }
                    }
                    else 
                    {
                        loc6.layout.dimensions.updatePosition(loc6.layout.dimensions.marginLeft + loc2, loc6.layout.dimensions.marginTop + loc1, dimensions.width, dimensions.height);
                    }
                }
                ++loc4;
            }
            _contentHeight = loc3;
            return;
        }

        protected function doReflowTileLTR():void
        {
            var loc11:*=null;
            var loc12:*=NaN;
            var loc13:*=NaN;
            var loc14:*=0;
            var loc1:*=_block.computedStyle.scrollTop;
            var loc2:*=_block.computedStyle.scrollLeft;
            var loc3:*=0;
            var loc4:*=0;
            var loc5:*=0;
            var loc6:*=[];
            var loc7:*=[];
            var loc8:*=_block.computedStyle.align;
            var loc9:*=0;
            var loc10:*=this.numChildren;
            while (loc9 < loc10) 
            {
                if ((loc11 = this.getChildAt(loc9)) != null) 
                {
                    if (loc11.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) 
                    {
                        if (loc11.visible) 
                        {
                            loc5 = Math.max(loc5, getChildsVerticalSize(loc11));
                            loc3 = loc3 + loc11.layout.dimensions.marginLeft;
                            if (loc8 == lesta.unbound.style.UbStyle.ALIGN_LEFT) 
                            {
                                loc11.layout.dimensions.updatePosition(loc3 + loc2, loc1 + loc4 + loc11.layout.dimensions.marginTop, dimensions.width, dimensions.height);
                            }
                            else 
                            {
                                loc7.push([loc3 + loc2, loc1 + loc4 + loc11.layout.dimensions.marginTop]);
                                loc6.push(loc11);
                            }
                            loc3 = loc3 + (loc11.layout.dimensions.measuredWidth + loc11.layout.dimensions.marginRight);
                            if ((loc12 = loc3 + getChildsHorizontalSize(loc11)) > dimensions.measuredWidth || loc9 == (loc10 - 1)) 
                            {
                                loc13 = 0;
                                if (loc8 != lesta.unbound.style.UbStyle.ALIGN_LEFT) 
                                {
                                    if (loc8 != lesta.unbound.style.UbStyle.ALIGN_CENTER) 
                                    {
                                        if (loc8 == lesta.unbound.style.UbStyle.ALIGN_RIGHT) 
                                        {
                                            loc13 = dimensions.measuredWidth - loc3;
                                        }
                                    }
                                    else 
                                    {
                                        loc13 = (dimensions.measuredWidth - loc3) / 2;
                                    }
                                    loc14 = 0;
                                    while (loc14 < loc6.length) 
                                    {
                                        loc6[loc14].layout.dimensions.updatePosition(loc7[loc14][0] + loc13, loc7[loc14][1], dimensions.width, dimensions.height);
                                        ++loc14;
                                    }
                                    loc6 = [];
                                    loc7 = [];
                                }
                                loc3 = 0;
                                loc4 = loc4 + loc5;
                                loc5 = 0;
                            }
                        }
                    }
                    else 
                    {
                        loc11.layout.dimensions.updatePosition(loc11.layout.dimensions.marginLeft + loc2, loc11.layout.dimensions.marginTop + loc1, dimensions.width, dimensions.height);
                    }
                }
                ++loc9;
            }
            _contentHeight = loc4 + loc5;
            return;
        }

        protected function doReflowTileTTB():void
        {
            var loc8:*=null;
            var loc9:*=NaN;
            var loc1:*=_block.computedStyle.scrollTop;
            var loc2:*=_block.computedStyle.scrollLeft;
            var loc3:*=loc2;
            var loc4:*=0;
            var loc5:*=0;
            var loc6:*=0;
            var loc7:*=this.numChildren;
            while (loc6 < loc7) 
            {
                if ((loc8 = this.getChildAt(loc6)) != null) 
                {
                    if (loc8.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) 
                    {
                        if (loc8.visible) 
                        {
                            if ((loc9 = loc4 + getChildsVerticalSize(loc8)) > dimensions.measuredHeight) 
                            {
                                loc4 = 0;
                                loc3 = loc3 + loc5;
                                loc5 = 0;
                            }
                            loc5 = Math.max(loc5, getChildsHorizontalSize(loc8));
                            loc4 = loc4 + loc8.layout.dimensions.marginTop;
                            loc8.layout.dimensions.updatePosition(loc3 + loc8.layout.dimensions.marginLeft, loc4 + loc1, dimensions.width, dimensions.height);
                            loc4 = loc4 + (loc8.layout.dimensions.measuredHeight + loc8.layout.dimensions.marginBottom);
                        }
                    }
                    else 
                    {
                        loc8.layout.dimensions.updatePosition(loc8.layout.dimensions.marginLeft + loc2, loc8.layout.dimensions.marginTop + loc1, dimensions.width, dimensions.height);
                    }
                }
                ++loc6;
            }
            _contentWidth = loc3;
            return;
        }

        public override function propagateDirtyProperties():void
        {
            var loc3:*=null;
            if (!measureEnabled) 
            {
                return;
            }
            this.pushDirtyDownwards();
            var loc1:*=0;
            var loc2:*=this.numChildren;
            while (loc1 < loc2) 
            {
                loc3 = this.getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                if (loc3 != null) 
                {
                    loc3.layout.propagateDirtyProperties();
                }
                ++loc1;
            }
            this.pullDirtyFromBelow();
            return;
        }

        protected function pushDirtyDownwards():void
        {
            var loc1:*=0;
            var loc2:*=0;
            var loc3:*=null;
            var loc4:*=null;
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                loc1 = 0;
                loc2 = this.numChildren;
                while (loc1 < loc2) 
                {
                    loc3 = this.getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                    if (loc3 != null) 
                    {
                        if ((loc4 = loc3.computedStyle).widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc4.widthMode == lesta.unbound.style.UbStyle.DIMENSION_FILL) 
                        {
                            loc4.dirty = loc4.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X;
                        }
                        if (!loc3.isolatedPosition) 
                        {
                            loc4.dirty = loc4.dirty | lesta.unbound.style.UbStyle.DIRTY_POSITION;
                        }
                    }
                    ++loc1;
                }
            }
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
            {
                loc1 = 0;
                loc2 = this.numChildren;
                while (loc1 < loc2) 
                {
                    loc3 = this.getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                    if (loc3 != null) 
                    {
                        if ((loc4 = loc3.computedStyle).heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc4.heightMode == lesta.unbound.style.UbStyle.DIMENSION_FILL) 
                        {
                            loc4.dirty = loc4.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
                        }
                        if (!loc3.isolatedPosition) 
                        {
                            loc4.dirty = loc4.dirty | lesta.unbound.style.UbStyle.DIRTY_POSITION;
                        }
                    }
                    ++loc1;
                }
            }
            return;
        }

        protected function pullDirtyFromBelow():void
        {
            var loc3:*=null;
            var loc1:*=0;
            var loc2:*=this.numChildren;
            while (loc1 < loc2) 
            {
                loc3 = this.getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                if (!(loc3 == null) && !loc3.isolatedPosition && loc3.computedStyle.dirty & (lesta.unbound.style.UbStyle.DIRTY_MEASURE_X | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y | lesta.unbound.style.UbStyle.DIRTY_POSITION)) 
                {
                    _block.computedStyle.dirty = _block.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_REFLOW;
                    break;
                }
                ++loc1;
            }
            if (_block.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || _block.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_FILL) 
            {
                loc1 = 0;
                loc2 = this.numChildren;
                while (loc1 < loc2) 
                {
                    loc3 = this.getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                    if (!(loc3 == null) && !(loc3.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc3.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
                    {
                        _block.computedStyle.dirty = _block.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X | lesta.unbound.style.UbStyle.DIRTY_REFLOW;
                    }
                    ++loc1;
                }
            }
            if (_block.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || _block.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_FILL) 
            {
                loc1 = 0;
                loc2 = this.numChildren;
                while (loc1 < loc2) 
                {
                    loc3 = this.getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                    if (!(loc3 == null) && !(loc3.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc3.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
                    {
                        _block.computedStyle.dirty = _block.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y | lesta.unbound.style.UbStyle.DIRTY_REFLOW;
                    }
                    ++loc1;
                }
            }
            return;
        }

        public override function repaint():void
        {
            var loc3:*=null;
            if (!measureEnabled) 
            {
                return;
            }
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_REFLOW) 
            {
                this.reflow();
            }
            var loc1:*=0;
            var loc2:*=this.numChildren;
            while (loc1 < loc2) 
            {
                loc3 = this.getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                if (loc3 != null) 
                {
                    loc3.layout.repaint();
                }
                ++loc1;
            }
            _block.commonStyleDirtyChecks();
            updateScroller();
            _block.computedStyle.dirty = 0;
            _block.dispatchEvent(new flash.events.Event(lesta.unbound.layout.UbEvent.BLOCK_REFLOW));
            return;
        }

        protected var _totalAbsoluteContentWidth:Number=0;

        protected var _totalAbsoluteContentHeight:Number=0;
    }
}
