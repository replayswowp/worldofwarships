package lesta.unbound.layout.measure 
{
    import flash.text.*;
    import lesta.unbound.layout.*;
    import lesta.unbound.style.*;
    import lesta.utils.*;
    
    public class TextBlockLayout extends lesta.unbound.layout.measure.BlockLayout
    {
        public function TextBlockLayout(arg1:lesta.unbound.layout.UbBlock)
        {
            this._htmlPattern = new RegExp("<[^<]+>");
            super(arg1);
            return;
        }

        internal function get textBlock():lesta.unbound.layout.UbTextBlock
        {
            return _block as lesta.unbound.layout.UbTextBlock;
        }

        internal function get textField():flash.text.TextField
        {
            return this.textBlock.textField;
        }

        internal function get text():String
        {
            return this.textBlock.text;
        }

        internal function get textFormat():flash.text.TextFormat
        {
            return this.textBlock.textFormat;
        }

        protected function preMeasure():void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=!(_block.computedStyle.styleSheet == null);
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_STYLESHEET) 
            {
                if (loc1) 
                {
                    loc2 = {};
                    loc2.fontFamily = _block.computedStyle.fontFamily;
                    loc2.color = "#" + _block.computedStyle.textColor.toString(16);
                    loc2.fontSize = dimensions.computeDimensionValue(_block.computedStyle.fontSize, _block.computedStyle.fontSizeMode, 0);
                    loc2.textAlign = _block.computedStyle.textAlign;
                    loc2.leading = _block.computedStyle.leading;
                    loc2.letterSpacing = _block.computedStyle.letterSpacing;
                    loc3 = new flash.text.StyleSheet();
                    loc3.parseCSS(_block.computedStyle.styleSheet);
                    loc3.setStyle("body", loc2);
                    this.textField.styleSheet = loc3;
                }
                else 
                {
                    this.textField.styleSheet = null;
                }
            }
            if (_block.computedStyle.dirty & (lesta.unbound.style.UbStyle.DIRTY_TEXT | lesta.unbound.style.UbStyle.DIRTY_STYLESHEET)) 
            {
                if (loc1) 
                {
                    this.textField.htmlText = this.text;
                }
                else 
                {
                    this.textField.text = this.text;
                    if (this.textField.text.search(this._htmlPattern) > -1) 
                    {
                        this.textField.text = null;
                        this.textField.htmlText = this.text;
                    }
                    this.textFormat.font = _block.computedStyle.fontFamily;
                    this.textFormat.color = _block.computedStyle.textColor;
                    this.textFormat.size = dimensions.computeDimensionValue(_block.computedStyle.fontSize, _block.computedStyle.fontSizeMode, 0);
                    this.textFormat.align = _block.computedStyle.textAlign;
                    this.textFormat.leading = _block.computedStyle.leading;
                    this.textFormat.letterSpacing = _block.computedStyle.letterSpacing;
                    this.textField.setTextFormat(this.textFormat);
                }
                _block.computedStyle.dirty = _block.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
            }
            return;
        }

        public override function measureX():void
        {
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                this.doMeasureX();
            }
            return;
        }

        public override function measureY():void
        {
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
            {
                this.doMeasureY();
            }
            return;
        }

        protected override function doMeasureX(arg1:Number=NaN):void
        {
            var loc1:*=NaN;
            if (_block.computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
            {
                super.doMeasureX(arg1);
            }
            else 
            {
                var loc2:*;
                this.textField.multiline = loc2 = false;
                this.textField.wordWrap = loc2;
                loc1 = this.textField.textWidth + TEXTFIELD_GUTTER;
                if (loc1 > _block.computedStyle.maxWidth) 
                {
                    this.textField.multiline = loc2 = true;
                    this.textField.wordWrap = loc2;
                    dimensions.width = _block.computedStyle.maxWidth;
                }
                else if (this.text.length > 0) 
                {
                    dimensions.width = clampWidth(loc1);
                }
                else 
                {
                    dimensions.width = clampWidth(0);
                }
            }
            if (this.textField.width != dimensions.width) 
            {
                this.textField.width = dimensions.width;
            }
            lesta.utils.DisplayObjectUtils.trimSingleLineText(this.textField);
            return;
        }

        protected override function doMeasureY(arg1:Number=NaN):void
        {
            if (_block.computedStyle.heightMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
            {
                super.doMeasureY(arg1);
            }
            else if (this.text.length > 0) 
            {
                dimensions.height = clampHeight(this.textField.textHeight + TEXTFIELD_GUTTER);
            }
            else 
            {
                dimensions.height = clampHeight(0);
            }
            if (this.textField.height != dimensions.height) 
            {
                this.textField.height = dimensions.height;
            }
            return;
        }

        public override function repaint():void
        {
            _block.commonStyleDirtyChecks();
            _block.computedStyle.dirty = 0;
            return;
        }

        public override function propagateDirtyProperties():void
        {
            if (!_block.visible) 
            {
                return;
            }
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                _block.computedStyle.dirty = _block.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
            }
            this.preMeasure();
            return;
        }

        internal static const TEXTFIELD_GUTTER:int=4;

        internal var _htmlPattern:RegExp;
    }
}
