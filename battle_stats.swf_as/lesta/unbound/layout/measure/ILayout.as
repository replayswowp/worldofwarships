package lesta.unbound.layout.measure 
{
    import lesta.unbound.interfaces.*;
    
    public interface ILayout extends lesta.unbound.layout.measure.ILayoutDimensions, lesta.unbound.layout.measure.ILayoutScroll, lesta.unbound.interfaces.IDestructable
    {
        function apply():void;

        function prepare():void;

        function measureX():void;

        function measureY():void;

        function postMeasure():void;

        function reflow():void;

        function repaint():void;

        function updateVirtual():void;

        function propagateDirtyProperties():void;
    }
}
