﻿package lesta.unbound.bindings 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import lesta.unbound.core.*;
    
    public class UbControllerNewScopeBinding extends lesta.unbound.bindings.UbControllerBinding
    {
        public function UbControllerNewScopeBinding()
        {
            super();
            return;
        }

        public override function init(arg1:__AS3__.vec.Vector, arg2:Array, arg3:flash.display.DisplayObject, arg4:lesta.unbound.core.UbScope, arg5:lesta.unbound.core.UbCentral):void
        {
            super.init(arg1, arg2, arg3, arg4.newChild(), arg5);
            return;
        }
    }
}
