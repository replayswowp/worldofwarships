package lesta.utils 
{
    import flash.display.*;
    import flash.events.*;
    
    public class EnterFrameTimer extends flash.events.EventDispatcher
    {
        public function EnterFrameTimer(arg1:uint=0, arg2:uint=0)
        {
            super();
            this.delayFrames = arg1;
            this.repeatCount = arg2;
            return;
        }

        public function get repeatCount():uint
        {
            return this._repeatCount;
        }

        public function set repeatCount(arg1:uint):void
        {
            if (arg1 < 0) 
            {
                throw new Error("Repeat count less than zero");
            }
            if (arg1 == 0) 
            {
                arg1 = uint.MAX_VALUE;
            }
            this._repeatCount = arg1;
            return;
        }

        public function get delayFrames():uint
        {
            return this._delayFrames;
        }

        public function set delayFrames(arg1:uint):void
        {
            if (arg1 < 0) 
            {
                throw new Error("Delay frames less than zero");
            }
            this._delayFrames = arg1;
            this._currentFrame = 0;
            return;
        }

        public function get running():Boolean
        {
            return this._running;
        }

        public function get currentCount():uint
        {
            return this._currentCount;
        }

        public function get currentFrame():uint
        {
            return this._currentFrame;
        }

        public function get progress():Number
        {
            var loc1:*=(this._currentFrame + 1) * 1 / (this._delayFrames + 1);
            return this._repeatCount > 0 ? (this._currentCount * 1 + loc1) / this._repeatCount : 1;
        }

        public function start():void
        {
            if (!this._running) 
            {
                this._running = true;
                enterFrameReceiver.addEventListener(flash.events.Event.ENTER_FRAME, this.onEnterFrame);
            }
            return;
        }

        public function stop():void
        {
            if (this._running) 
            {
                this._running = false;
                enterFrameReceiver.removeEventListener(flash.events.Event.ENTER_FRAME, this.onEnterFrame);
            }
            return;
        }

        public function reset():void
        {
            this.stop();
            this._currentCount = 0;
            this._currentFrame = 0;
            return;
        }

        public function restart():void
        {
            this._currentCount = 0;
            this._currentFrame = 0;
            this.start();
            return;
        }

        internal function onEnterFrame(arg1:flash.events.Event):void
        {
            var loc1:*;
            var loc2:*=((loc1 = this)._currentFrame + 1);
            loc1._currentFrame = loc2;
            if (this._currentFrame > this._delayFrames) 
            {
                this._currentFrame = 0;
                loc2 = ((loc1 = this)._currentCount + 1);
                loc1._currentCount = loc2;
                if (this._currentCount < this._repeatCount) 
                {
                    dispatchEvent(new flash.events.TimerEvent(flash.events.TimerEvent.TIMER));
                }
                else 
                {
                    dispatchEvent(new flash.events.TimerEvent(flash.events.TimerEvent.TIMER_COMPLETE));
                    this.reset();
                }
            }
            return;
        }

        public override function toString():String
        {
            return super.toString() + "{repeatCount=" + String(this._repeatCount) + ",currentCount=" + String(this._currentCount) + ",delayFrames=" + String(this._delayFrames) + ",currentFrame=" + String(this._currentFrame) + ",running=" + String(this._running) + "}";
        }

        
        {
            enterFrameReceiver = new flash.display.Shape();
        }

        internal var _repeatCount:uint=0;

        internal var _delayFrames:uint=0;

        internal var _currentFrame:uint;

        internal var _currentCount:uint;

        internal var _running:Boolean;

        internal static var enterFrameReceiver:flash.display.Shape;
    }
}
