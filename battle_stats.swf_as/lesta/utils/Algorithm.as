package lesta.utils 
{
    import flash.utils.*;
    
    public class Algorithm extends Object
    {
        public function Algorithm()
        {
            super();
            return;
        }

        public static function roundNumber(arg1:Number, arg2:int):Number
        {
            var loc1:*=Math.pow(10, arg2);
            return Math.round(arg1 * loc1) / loc1;
        }

        public static function findFirstByProperty(arg1:Array, arg2:String, arg3:String, arg4:Boolean=false):Object
        {
            var loc1:*=0;
            var loc2:*=arg1.length;
            while (loc1 < loc2) 
            {
                if (arg1[loc1][arg2] == arg3) 
                {
                    return arg4 ? [arg1[loc1], loc1] : arg1[loc1];
                }
                ++loc1;
            }
            return null;
        }

        public static function clearDictionary(arg1:flash.utils.Dictionary):void
        {
            var loc2:*=undefined;
            var loc3:*=0;
            var loc4:*=0;
            var loc1:*=[];
            var loc5:*=0;
            var loc6:*=arg1;
            for (loc2 in loc6) 
            {
                loc1.push(loc2);
            }
            loc3 = 0;
            loc4 = loc1.length;
            while (loc3 < loc4) 
            {
                delete arg1[loc1[loc3]];
                ++loc3;
            }
            return;
        }

        public static function getPropertyByPath(arg1:*, arg2:String):*
        {
            var loc1:*=arg2.split(".");
            var loc2:*=0;
            var loc3:*=loc1.length;
            while (loc2 < loc3 && !(arg1 == null)) 
            {
                arg1 = arg1[loc1[loc2]];
                ++loc2;
            }
            return arg1;
        }
    }
}
