package lesta.utils 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.geom.*;
    
    public class Align extends Object
    {
        public function Align()
        {
            super();
            return;
        }

        public static function distributeWidth(arg1:__AS3__.vec.Vector.<flash.display.DisplayObject>, arg2:Number, arg3:Number, arg4:Boolean=true):void
        {
            var loc2:*=null;
            if (!(arg1 && arg1.length)) 
            {
                return;
            }
            var loc1:*=0;
            var loc3:*=[];
            var loc4:*=0;
            while (loc4 < arg1.length) 
            {
                loc2 = arg4 ? getRectFromSize(arg1[loc4]) : arg1[loc4].getRect(arg1[loc4]);
                loc3[loc4] = loc2;
                loc1 = loc1 + loc2.width;
                ++loc4;
            }
            var loc5:*=(arg3 - arg2 - loc1) / (arg1.length + 1);
            var loc6:*=arg2 + loc5;
            loc4 = 0;
            while (loc4 < arg1.length) 
            {
                arg1[loc4].x = loc6 - loc3[loc4].x;
                loc6 = loc6 + (loc3[loc4].width + loc5);
                ++loc4;
            }
            return;
        }

        internal static function getRectFromSize(arg1:flash.display.DisplayObject):flash.geom.Rectangle
        {
            var loc2:*=null;
            var loc1:*=arg1 as flash.display.DisplayObjectContainer;
            if (loc1) 
            {
                loc2 = loc1.getChildByName("size") as flash.display.Sprite;
                if (loc2) 
                {
                    return loc2.getRect(loc1);
                }
            }
            return arg1.getRect(arg1);
        }
    }
}
