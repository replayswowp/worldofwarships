package lesta.utils.resourceLoading 
{
    import flash.display.*;
    
    public class DisplayResource extends lesta.utils.resourceLoading.LoadResourceJob
    {
        public function DisplayResource(arg1:int, arg2:String)
        {
            super(arg1, arg2);
            loader = new lesta.utils.resourceLoading.DisplayLoader();
            return;
        }

        public function get content():flash.display.DisplayObject
        {
            return (loader as flash.display.Loader).content;
        }
    }
}
