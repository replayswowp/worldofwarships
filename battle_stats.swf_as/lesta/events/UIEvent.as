package lesta.events 
{
    import flash.events.*;
    
    public class UIEvent extends flash.events.Event
    {
        public function UIEvent(arg1:String, arg2:Boolean=false, arg3:Boolean=false)
        {
            super(arg1, arg2, arg3);
            return;
        }

        public static const RESIZE_ELEMENT:String="resizeElement";
    }
}
