package lesta.libs.unbound 
{
    import lesta.datahub.*;
    import lesta.unbound.core.*;
    
    public class Utils extends Object
    {
        public function Utils()
        {
            super();
            return;
        }

        public static function getDataHubWatcher(arg1:Array, arg2:lesta.datahub.DataHub, arg3:lesta.unbound.core.UbScope, arg4:Function, arg5:Function=null, arg6:Function=null):lesta.libs.unbound.DataHubWatcher
        {
            var loc1:*=arg3;
            var loc2:*;
            if ((loc2 = arg1[0]) != "dataHub") 
            {
                if (loc2 != "$dataRef") 
                {
                    arg1.shift();
                    arg1.unshift("ref");
                    arg1.unshift(loc2);
                    arg1.unshift(DATA_HUB_REFS);
                }
            }
            else 
            {
                loc1 = arg2._global;
                arg1.shift();
            }
            var loc3:*;
            return loc3 = new lesta.libs.unbound.DataHubWatcher(loc1, arg1, arg4, arg5, arg6);
        }

        public static const DATA_HUB_REFS:String="dataHubRefs";
    }
}
