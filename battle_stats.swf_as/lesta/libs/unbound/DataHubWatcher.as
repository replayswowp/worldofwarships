package lesta.libs.unbound 
{
    import flash.events.*;
    
    public class DataHubWatcher extends Object
    {
        public function DataHubWatcher(arg1:Object, arg2:Array, arg3:Function, arg4:Function, arg5:Function=null)
        {
            var loc3:*=null;
            this.listDHObjects = new Array();
            super();
            this.path = arg2;
            this.onChanged = arg3;
            this.onEvent = arg4;
            var loc1:*;
            (loc1 = new DHObject("root", null, null, null)).value = arg1;
            loc1.updateDispatcher(arg1 as flash.events.EventDispatcher);
            var loc2:*=0;
            while (loc2 < arg2.length) 
            {
                loc3 = new DHObject(arg2[loc2], loc1, this.onValueChanged, this.onEventInvoked);
                this.listDHObjects.push(loc3);
                loc1 = loc3;
                ++loc2;
            }
            this.firstObject = this.listDHObjects[0];
            this.lastObject = this.listDHObjects[(this.listDHObjects.length - 1)];
            this.lastObject.onBuilt = arg5;
            this.firstObject.update();
            return;
        }

        public function getValue():*
        {
            return this.lastObject.value;
        }

        public function fini():void
        {
            var loc3:*=null;
            var loc1:*=0;
            var loc2:*=this.listDHObjects.length;
            while (loc1 < loc2) 
            {
                loc3 = this.listDHObjects[loc1];
                loc3.fini();
                ++loc1;
            }
            return;
        }

        internal function onValueChanged():void
        {
            this.onChanged(this.lastObject.value);
            return;
        }

        internal function onEventInvoked(arg1:Array):void
        {
            this.onChanged();
            if (this.onEvent != null) 
            {
                this.onEvent(arg1);
            }
            return;
        }

        public var path:Array;

        internal var global:DHObject;

        internal var firstObject:DHObject;

        internal var lastObject:DHObject;

        internal var listDHObjects:Array;

        internal var onChanged:Function=null;

        internal var onEvent:Function=null;
    }
}

import flash.events.*;
import lesta.utils.*;


class DHObject extends Object
{
    public function DHObject(arg1:String, arg2:DHObject, arg3:Function, arg4:Function)
    {
        super();
        this.name = arg1;
        this.prev = arg2;
        this.onValueChanged = arg3;
        this.onEvent = arg4;
        if (this.prev != null) 
        {
            this.prev.next = this;
        }
        return;
    }

    public function fini():void
    {
        this.prev = null;
        this.next = null;
        this.onValueChanged = null;
        this.onEvent = null;
        this.onBuilt = null;
        if (this.dispatcher) 
        {
            this.dispatcher.removeEventListener(flash.events.Event.CHANGE, this.onChanged);
            this.dispatcher = null;
        }
        if (this.invoker) 
        {
            this.invoker.removeEventListener(lesta.utils.InvokerEvent.ENVOKED, this.onInvoked);
            this.invoker = null;
        }
        return;
    }

    public function update():void
    {
        if (this.prev == null || this.prev.value == null) 
        {
            return;
        }
        var loc1:*=this.prev.value[this.name];
        var loc2:*=this.value == null && !(loc1 == null);
        this.value = loc1;
        var loc3:*=loc1 as lesta.utils.Invoker;
        var loc4:*=loc3 != null ? null : loc1 as flash.events.EventDispatcher;
        this.updateInvoker(loc3);
        this.updateDispatcher(loc4);
        if (this.next) 
        {
            this.next.update();
        }
        if (loc2 && !(this.onBuilt == null)) 
        {
            this.onBuilt(this.value);
        }
        return;
    }

    public function updateDispatcher(arg1:flash.events.EventDispatcher):void
    {
        if (this.dispatcher != arg1) 
        {
            if (this.dispatcher) 
            {
                this.dispatcher.removeEventListener(flash.events.Event.CHANGE, this.onChanged);
            }
            this.dispatcher = arg1;
            if (this.dispatcher) 
            {
                this.dispatcher.addEventListener(flash.events.Event.CHANGE, this.onChanged);
            }
        }
        return;
    }

    public function updateInvoker(arg1:lesta.utils.Invoker):void
    {
        if (this.invoker != arg1) 
        {
            if (this.invoker) 
            {
                this.invoker.removeEventListener(lesta.utils.InvokerEvent.ENVOKED, this.onInvoked);
            }
            this.invoker = arg1;
            if (this.invoker) 
            {
                this.invoker.addEventListener(lesta.utils.InvokerEvent.ENVOKED, this.onInvoked);
            }
        }
        return;
    }

    internal function onChanged(arg1:flash.events.Event):void
    {
        if (this.next) 
        {
            this.next.update();
        }
        this.onValueChanged();
        return;
    }

    internal function onInvoked(arg1:lesta.utils.InvokerEvent):void
    {
        this.onEvent(arg1.args);
        return;
    }

    public var name:String;

    public var onValueChanged:Function=null;

    public var onEvent:Function=null;

    public var onBuilt:Function=null;

    public var prev:DHObject=null;

    public var next:DHObject=null;

    public var dispatcher:flash.events.EventDispatcher=null;

    public var invoker:lesta.utils.Invoker=null;

    public var value:*=null;
}