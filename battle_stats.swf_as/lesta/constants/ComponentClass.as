package lesta.constants 
{
    public class ComponentClass extends Object
    {
        public function ComponentClass()
        {
            super();
            return;
        }

        public static const _Global:uint=1;

        public static const account:uint=2;

        public static const accountLevel:uint=3;

        public static const accountLevelView:uint=4;

        public static const contactsGroup:uint=5;

        public static const groupViewElements:uint=6;

        public static const groupOfElements:uint=7;

        public static const elementInGroups:uint=8;

        public static const contact:uint=9;

        public static const channelParticipant:uint=10;

        public static const channel:uint=11;

        public static const channelsGroup:uint=12;

        public static const chat:uint=13;

        public static const searchResult:uint=14;

        public static const searchResultItem:uint=15;

        public static const crew:uint=16;

        public static const retraining:uint=17;

        public static const port:uint=18;

        public static const show:uint=19;

        public static const dataComponent:uint=20;

        public static const sysMessage:uint=21;

        public static const worldPosition:uint=22;

        public static const screenPosition:uint=23;

        public static const timer:uint=24;

        public static const language:uint=25;

        public static const squadron:uint=26;

        public static const bigWorldEntity:uint=27;
    }
}
