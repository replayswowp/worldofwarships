package lesta.constants 
{
    public class Common extends Object
    {
        public function Common()
        {
            super();
            return;
        }

        public static const TEAMKILLER_COLOR:uint=16750282;

        public static const TORPEDO_VISIBILITY_DIST:Number=0.5;

        public static const BW_TO_BALLISTIC:Number=30;
    }
}
