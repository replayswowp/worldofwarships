package lesta.constants 
{
    public class GameMode extends Object
    {
        public function GameMode()
        {
            super();
            return;
        }

        public static const TEST:int=0;

        public static const STANDART:int=1;

        public static const SINGLEBASE:int=2;

        public static const AWESOME:int=7;

        public static const TUTORIAL:int=8;

        public static const MEGABASE:int=9;

        public static const FORTS:int=10;

        public static const STANDARD_DOMINATION:int=11;
    }
}
