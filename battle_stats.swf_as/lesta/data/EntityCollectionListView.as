package lesta.data 
{
    import scaleform.clik.data.*;
    
    public class EntityCollectionListView extends lesta.data.EntityCollectionView
    {
        public function EntityCollectionListView(arg1:Function=null, arg2:lesta.data.EntityCollection=null)
        {
            super(arg1, arg2);
            this.data = new scaleform.clik.data.DataProvider() as Array;
            return;
        }

        public function getList():scaleform.clik.data.DataProvider
        {
            return this.data as scaleform.clik.data.DataProvider;
        }

        protected override function doAddMember(arg1:Object, arg2:Object):void
        {
            this.data[this.data.length] = arg1;
            return;
        }

        public override function deleteMember(arg1:Object, arg2:Object):void
        {
            var loc1:*=this.data.indexOf(arg1);
            this.data.splice(loc1, 1);
            return;
        }

        public override function clear():void
        {
            this.data.length = 0;
            return;
        }

        internal var data:Array;
    }
}
