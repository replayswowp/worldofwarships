package lesta.structs 
{
    public class SystemLogMessageInfo extends Object
    {
        public function SystemLogMessageInfo()
        {
            super();
            return;
        }

        public var id:int;

        public var data:Object;

        public var extendedData:Object;

        public var hasExtendedData:Boolean;

        public var extendedDataAvailable:Boolean;

        public var groupId:int;

        public var idInGroup:int;

        public var typeId:int;

        public var sourceId:int;

        public var show:Boolean;

        public var ttl:Number;
    }
}
