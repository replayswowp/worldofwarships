package lesta.structs 
{
    public class SSEInfo extends Object
    {
        public function SSEInfo()
        {
            this.rewards = [];
            this.rewardsSpecial = [];
            this.conditions = [];
            this.steps = [];
            this.actions = [];
            this.conditionSet = [];
            super();
            return;
        }

        public function toString():String
        {
            return "SSEInfo{id=" + String(this.id) + "\ntype=" + this.type + "\ntitleText=" + String(this.titleText) + "\ntypeText=" + String(this.typeText) + "\nsseId=" + String(this.sseId) + "\nisActive=" + String(this.isActive) + "\nactiveStep=" + String(this.activeStep) + "\nprogress=" + String(this.progress) + "\nbackgroundURL=" + String(this.backgroundURL) + "}";
        }

        public var id:int;

        public var type:String="";

        public var titleText:String="";

        public var descriptionText:String="";

        public var rewards:Array;

        public var rewardsSpecial:Array;

        public var conditions:Array;

        public var steps:Array;

        public var actions:Array;

        public var typeText:String="";

        public var sseId:String="";

        public var isActive:Boolean=true;

        public var readyToBattle:Boolean=true;

        public var inOneBattle:Boolean=false;

        public var activeStep:int=0;

        public var currentStep:int=-1;

        public var progress:Number=0;

        public var technoIds:String="";

        public var addcondIds:String="";

        public var precondIds:String="";

        public var conditionSet:Array;

        public var iconURL:String="";

        public var backgroundURL:String="";

        public var startDate:String="";

        public var stopDate:String="";

        public var timeToChange:Number=0;

        public var battleType:String="";

        public var isInDivision:Boolean=false;
    }
}
