package lesta.structs 
{
    public class RankedBattlesStats extends Object
    {
        public function RankedBattlesStats()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "RankedBattlesStats{seasonId=" + this.seasonId + ",rank=" + String(this.rank) + ",rankBest=" + String(this.rankBest) + ",stars=" + String(this.stars) + ",league=" + String(this.league) + "}";
        }

        public var seasonId:int;

        public var rank:int;

        public var rankBest:int;

        public var stars:int;

        public var totalStars:int;

        public var league:int;

        public var participated:Boolean;

        public var messageStartWithStars:String;

        public var messageStartFromRank:String;

        public var stage:int;

        public var rankInSeasonMessage:String;
    }
}
