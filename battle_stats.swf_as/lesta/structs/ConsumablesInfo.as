package lesta.structs 
{
    import lesta.utils.*;
    
    public class ConsumablesInfo extends Object
    {
        public function ConsumablesInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return lesta.utils.Debug.formatString("[ConsumablesInfo id: %, slotId : %, title : %, installed : %, description : %]", [this.id, this.slotId, this.title, this.installed, this.description]);
        }

        public var id:Number;

        public var slotId:int;

        public var name:String;

        public var commandId:Number;

        public var params:Array;

        public var freeOfCharge:Boolean;

        public var canBuy:Boolean;

        public var useGold:Boolean;

        public var installed:Boolean;

        public var numInStorage:int;

        public var slotsNum:int;

        public var iconPath:String;

        public var title:String;

        public var description:String;
    }
}
