package lesta.structs 
{
    public class GunStateInfo extends Object
    {
        public function GunStateInfo()
        {
            super();
            return;
        }

        public var weaponType:int;

        public var gunID:int;

        public var positionID:int;

        public var reloadProgress:Number;

        public var timeRemain:Number;

        public var critTimeRemain:Number;

        public var critTimeFull:Number;

        public var aimYaw:Number;

        public var gunYaw:Number;

        public var aimState:int;

        public var aimDiff:Number;

        public var healthState:int;

        public var burnState:int;

        public var screenX:Number;

        public var isSelected:Boolean;

        public var shootNodes:int;
    }
}
