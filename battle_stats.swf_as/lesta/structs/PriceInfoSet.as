package lesta.structs 
{
    public class PriceInfoSet extends Object
    {
        public function PriceInfoSet()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "PriceInfoSet{id=" + String(this.id) + ",prices=" + String(this.prices) + "}";
        }

        public static const OP_EXPLORE:String="0_";

        public static const OP_BUY:String="1_";

        public static const OP_INFO:String="2_";

        public static const CU_GOLD:String="gold";

        public static const CU_CR:String="credits";

        public static const CU_XP:String="exp";

        public static const CU_VALUE:String="value";

        public var id:String;

        public var prices:Array;
    }
}
