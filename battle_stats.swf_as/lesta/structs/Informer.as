package lesta.structs 
{
    public class Informer extends Object
    {
        public function Informer()
        {
            super();
            return;
        }

        public var visible:Boolean=false;

        public var type:String="";

        public var data:Object=null;
    }
}
