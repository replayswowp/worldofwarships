﻿package lesta.structs 
{
    import scaleform.clik.controls.*;
    
    public class ToogleHintInfo extends lesta.structs.SimpleHintInfo
    {
        public function ToogleHintInfo(arg1:scaleform.clik.controls.Button, arg2:String, arg3:String)
        {
            super("");
            this.button = arg1;
            this.label = arg2;
            this.selectedLabel = arg3;
            return;
        }

        public override function getLabel():String
        {
            return this.button.selected ? this.selectedLabel : this.label;
        }

        internal var button:scaleform.clik.controls.Button;

       // internal var label:String;

        internal var selectedLabel:String;
    }
}
