package lesta.structs 
{
    public class SimpleHintInfo extends Object
    {
        public function SimpleHintInfo(arg1:String)
        {
            super();
            this.label = arg1;
            return;
        }

        public function getLabel():String
        {
            return this.label;
        }

        public var attachToCursor:Boolean=true;

        internal var label:String;
    }
}
