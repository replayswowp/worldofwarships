package lesta.structs 
{
    public class PriceInfo extends Object
    {
        public function PriceInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "PriceInfo{id=" + String(this.id) + ",currency=" + String(this.currency) + ",basePrice=" + String(this.basePrice) + ",discountPercent=" + String(this.discountPercent) + ",finalPrice=" + String(this.finalPrice) + ",till=" + String(this.till) + ",discountId=" + String(this.discountId) + "}";
        }

        public var id:String;

        public var currency:String;

        public var currencyID:int;

        public var basePrice:Number;

        public var discountPercent:int;

        public var finalPrice:Number;

        public var till:int;

        public var timeLeft:String;

        public var discountId:int;
    }
}
