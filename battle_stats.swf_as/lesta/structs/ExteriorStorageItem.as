package lesta.structs 
{
    public class ExteriorStorageItem extends Object
    {
        public function ExteriorStorageItem()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "[ExteriorStorageItem] " + "id: " + this.id + ",count: " + this.count;
        }

        public var id:String;

        public var count:int;
    }
}
