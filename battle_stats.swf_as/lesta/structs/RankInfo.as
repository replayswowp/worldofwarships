package lesta.structs 
{
    public class RankInfo extends Object
    {
        public function RankInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "RankInfo{id=" + this.id + ",starsToNext=" + String(this.starsToNext) + ",starEarnPlace=" + String(this.starEarnPlace) + ",starLossPlace=" + String(this.starLossPlace) + ",isSavePoint=" + String(this.isSavePoint) + "}";
        }

        internal static const IMAGE_PATH_BIG:String="../ranks/icons/icon_rank_big_";

        internal static const IMAGE_PATH_SMALL:String="../ranks/icons/icon_rank_small_";

        internal static const IMAGE_EXTENSION:String=".png";

        public var id:int;

        public var leagueId:int;

        public var starsToNext:int;

        public var starEarnPlace:int;

        public var starLossPlace:int;

        public var isSavePoint:Boolean;

        public var rewards:Array;

        public var rewardsSpecial:Array;

        public var outerClipFrame:int;

        public var earnCondition:String;

        public var lossCondition:String;

        public var shipLevelMin:int;

        public var shipLevelMax:int;

        public var shipLevelMinRome:String;

        public var shipLevelMaxRome:String;

        public var allowedTheirsShipsDescription:String;
    }
}
