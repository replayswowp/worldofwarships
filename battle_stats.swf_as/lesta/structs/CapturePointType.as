package lesta.structs 
{
    public class CapturePointType extends Object
    {
        public function CapturePointType()
        {
            super();
            return;
        }

        public static const CONTROL:int=1;

        public static const BASE:int=2;

        public static const MEGABASE:int=3;

        public static const BUILDING_CP:int=4;

        public static const BASE_WITH_POINTS:int=5;
    }
}
