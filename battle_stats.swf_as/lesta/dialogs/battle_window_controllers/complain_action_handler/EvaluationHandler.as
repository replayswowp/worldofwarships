package lesta.dialogs.battle_window_controllers.complain_action_handler 
{
    import lesta.data.*;
    
    public class EvaluationHandler extends Object implements lesta.dialogs.battle_window_controllers.complain_action_handler.ActionHandler
    {
        public function EvaluationHandler(arg1:int, arg2:int)
        {
            super();
            this.type = arg1;
            this.topic = arg2;
            return;
        }

        public function handleAction(arg1:Object):void
        {
            lesta.data.GameDelegate.call("battle.onComplaint", [arg1.nickname, this.type, this.topic]);
            return;
        }

        internal var type:int;

        internal var topic:int;
    }
}
