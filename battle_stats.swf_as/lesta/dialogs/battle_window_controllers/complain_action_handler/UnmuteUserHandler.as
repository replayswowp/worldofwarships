package lesta.dialogs.battle_window_controllers.complain_action_handler 
{
    import lesta.data.*;
    
    public class UnmuteUserHandler extends Object implements lesta.dialogs.battle_window_controllers.complain_action_handler.ActionHandler
    {
        public function UnmuteUserHandler()
        {
            super();
            return;
        }

        public function handleAction(arg1:Object):void
        {
            lesta.data.GameDelegate.call("battleChat.unmuteUser", [arg1.avatarId]);
            return;
        }
    }
}
