package lesta.dialogs.battle_window_controllers.complain_action_handler 
{
    public interface ActionHandler
    {
        function handleAction(arg1:Object):void;
    }
}
