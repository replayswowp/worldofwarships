package lesta.dialogs.battle_loading 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.text.*;
    import flash.utils.*;
    import lesta.structs.*;
    import lesta.utils.*;
    
    public class BattleReward extends flash.display.Sprite
    {
        public function BattleReward()
        {
            super();
            this.size.visible = false;
            return;
        }

        public function update(arg1:lesta.structs.BattleModeInfo):void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            if (!arg1) 
            {
                return;
            }
            var loc1:*=new Vector.<flash.display.DisplayObject>(0);
            var loc6:*;
            this.capturePointsClip.visible = loc6 = arg1.rewardForCaptureMsg || arg1.rewardForHoldMsg;
            if (loc6) 
            {
                loc2 = new Vector.<flash.display.DisplayObject>(0);
                loc3 = this.capturePointsClip.getChildByName("size") as flash.display.Sprite;
                (loc4 = this.capturePointsClip.getChildByName("clipHolding") as flash.display.MovieClip).visible = loc6 = !(arg1.rewardForHoldMsg == "");
                if (loc6) 
                {
                    loc4.rewardText.text = arg1.rewardForHoldMsg;
                    loc2.push(loc4);
                }
                (loc5 = this.capturePointsClip.getChildByName("clipCapture") as flash.display.MovieClip).visible = loc6 = !(arg1.rewardForCaptureMsg == "");
                if (loc6) 
                {
                    loc5.rewardText.text = arg1.rewardForCaptureMsg;
                    loc2.push(loc5);
                }
                lesta.utils.Align.distributeWidth(loc2, loc3.x, loc3.x + loc3.width);
                loc1.push(this.capturePointsClip);
            }
            this.enemiesClip.visible = loc6 = !(arg1.shipRewardMap == null);
            if (loc6) 
            {
                this.drawShipIcons(arg1.shipRewardMap, this.enemiesClip, flash.utils.getDefinitionByName("ShipReward") as Class);
                loc1.push(this.enemiesClip);
            }
            this.alliesClip.visible = loc6 = !(arg1.shipPenaltyMap == null);
            if (loc6) 
            {
                this.drawShipIcons(arg1.shipPenaltyMap, this.alliesClip, flash.utils.getDefinitionByName("ShipPenalty") as Class);
                loc1.unshift(this.alliesClip);
            }
            lesta.utils.Align.distributeWidth(loc1, this.size.x, this.size.x + this.size.width);
            return;
        }

        internal function drawShipIcons(arg1:Object, arg2:flash.display.Sprite, arg3:Class):void
        {
            var loc2:*=null;
            var loc3:*=NaN;
            var loc4:*=0;
            var loc6:*=null;
            while (arg2.numChildren > 1) 
            {
                arg2.removeChildAt(1);
            }
            var loc1:*=["Auxiliary", "AirCarrier", "Battleship", "Cruiser", "Destroyer"];
            var loc7:*=0;
            var loc8:*=arg1;
            for (loc2 in loc8) 
            {
                if (arg1[loc2] != "") 
                {
                    continue;
                }
                loc1.splice(loc1.indexOf(loc2), 1);
            }
            loc3 = this.REWARD_NODE_DRAW_X;
            loc4 = 0;
            while (loc4 < loc1.length) 
            {
                loc6 = loc1[loc4];
                loc3 = loc3 + (this.drawShipReward(arg3, arg2, loc3, this.REWARD_NODE_DRAW_Y, this.SHIP_TYPE_FRAMES[loc6], arg1[loc6]) + this.REWARD_NODE_INTERVAL);
                ++loc4;
            }
            var loc5:*;
            if (loc5 = arg2.getChildByName("textField") as flash.text.TextField) 
            {
                loc5.width = loc3 - this.REWARD_NODE_INTERVAL;
            }
            return;
        }

        internal function drawShipReward(arg1:Class, arg2:flash.display.Sprite, arg3:Number, arg4:Number, arg5:int, arg6:int):Number
        {
            var loc1:*;
            (loc1 = new arg1()).x = arg3;
            loc1.y = arg4;
            loc1.gotoAndStop(arg5);
            loc1.textField.text = arg6 > 0 ? "+" + arg6 : String(arg6);
            arg2.addChild(loc1);
            return loc1.width;
        }

        internal const REWARD_NODE_DRAW_X:int=32;

        internal const REWARD_NODE_DRAW_Y:int=40;

        internal const REWARD_NODE_INTERVAL:int=6;

        internal const INTERVAL_BETWEEN_TEXT_FIELDS:int=-7;

        internal const REWARD_NODES_ORDER:Object={"Auxiliary":0, "AirCarrier":1, "Battleship":2, "Cruiser":3, "Destroyer":4};

        internal const SHIP_TYPE_FRAMES:Object={"Auxiliary":5, "AirCarrier":2, "Battleship":4, "Cruiser":3, "Destroyer":1};

        public var capturePointsClip:flash.display.Sprite;

        public var alliesClip:flash.display.Sprite;

        public var enemiesClip:flash.display.Sprite;

        public var size:flash.display.Sprite;
    }
}
