package lesta.dialogs.battle_window.constants 
{
    public class PlaneOrders extends Object
    {
        public function PlaneOrders()
        {
            super();
            return;
        }

        public static const NONE:String="None";

        public static const LAUNCH:String="Launch";

        public static const LAND:String="Land";

        public static const LANDING:String="LandInProgress";

        public static const MOVE_TO:String="MoveTo";

        public static const RECON:String="Recon";

        public static const PATROL:String="Patrol";

        public static const ATTACK_TORPEDO:String="AttackTorpedo";

        public static const ATTACK_BOMBER:String="AttackBomber";

        public static const ATTACK_PLANE:String="AttackPlane";

        public static const AG_BOMBER:String="AttackGroundBomber";

        public static const AG_TORPEDO:String="AttackGroundTorpedo";

        public static const FOLLOW_SHIP:String="FollowShip";

        public static const ESCORT_SHIP:String="EscortShip";

        public static const FOLLOW_PLANE:String="FollowPlane";

        public static const ESCORT_PLANE:String="EscortPlane";

        public static const EXPECTS_ORDER:String="ExpectsOrder";
    }
}
