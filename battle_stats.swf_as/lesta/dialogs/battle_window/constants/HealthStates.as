package lesta.dialogs.battle_window.constants 
{
    public class HealthStates extends Object
    {
        public function HealthStates()
        {
            super();
            return;
        }

        public static const NORMAL:int=0;

        public static const DAMAGED:int=1;

        public static const CRIT:int=2;

        public static const DEAD:int=3;

        public static const DETONATED:int=4;

        public static const GUI_STATES:Array=[NORMAL, DAMAGED, CRIT, DEAD];
    }
}
