package lesta.dialogs.battle_window._battle_progress 
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import lesta.controls.*;
    import lesta.structs.*;
    
    public class CapturePointTimerController extends Object
    {
        public function CapturePointTimerController(arg1:lesta.structs.ServerTimer, arg2:lesta.controls.AnimatedStateClip)
        {
            super();
            this._serverTimer = arg1;
            this._timer = arg2;
            this.init();
            return;
        }

        internal function init():void
        {
            this._timer.addEventListener(flash.events.Event.ENTER_FRAME, this.onFrame);
            return;
        }

        internal function onFrame(arg1:flash.events.Event):void
        {
            var loc1:*=this._serverTimer.getTimeLeft();
            this.updateState(loc1);
            this.tf.text = getTimeString(loc1);
            if (loc1 < 0) 
            {
                this._timer.removeEventListener(flash.events.Event.ENTER_FRAME, this.onFrame);
            }
            return;
        }

        internal function updateState(arg1:int):void
        {
            var loc1:*="stage1";
            if (arg1 < 10000) 
            {
                loc1 = "stage3";
            }
            else if (arg1 < 60000) 
            {
                loc1 = "stage2";
            }
            this._timer.setState(loc1);
            return;
        }

        internal function get tf():flash.text.TextField
        {
            return (this._timer as flash.display.MovieClip).textClip.textField;
        }

        internal static function getTimeString(arg1:Number):String
        {
            var loc1:*="";
            var loc2:*=arg1 / 1000;
            var loc3:*=Math.floor(loc2 / 60);
            var loc4:*=loc2 - loc3 * 60;
            loc1 = loc1 + (loc3 > 0 ? loc3 + ":" : "");
            loc1 = loc1 + ((int(loc4) < 10 && loc3 > 0 ? "0" : "") + (loc3 > 0 ? int(loc4) : loc4.toFixed(1)));
            return loc1;
        }

        internal var _serverTimer:lesta.structs.ServerTimer;

        internal var _timer:lesta.controls.AnimatedStateClip;
    }
}
