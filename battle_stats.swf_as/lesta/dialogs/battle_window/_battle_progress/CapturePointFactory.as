package lesta.dialogs.battle_window._battle_progress 
{
    import flash.utils.*;
    import lesta.structs.*;
    
    public class CapturePointFactory extends Object
    {
        public function CapturePointFactory()
        {
            super();
            return;
        }

        public static function getIndicatorByType(arg1:int):CapturePointContainer
        {
            var loc1:*=null;
            var loc4:*=arg1;
            switch (loc4) 
            {
                case lesta.structs.CapturePointType.BASE:
                {
                    loc1 = "BasePointContainer";
                    break;
                }
                case lesta.structs.CapturePointType.BASE_WITH_POINTS:
                {
                    loc1 = "CapturePointContainer";
                    break;
                }
                case lesta.structs.CapturePointType.CONTROL:
                {
                    loc1 = "CapturePointContainer";
                    break;
                }
                case lesta.structs.CapturePointType.MEGABASE:
                {
                    loc1 = "MegaBasePointContainer";
                    break;
                }
                default:
                {
                    loc1 = "CapturePointContainer";
                }
            }
            var loc2:*=flash.utils.getDefinitionByName(loc1) as Class;
            var loc3:*;
            return loc3 = new loc2() as CapturePointContainer;
        }
    }
}
