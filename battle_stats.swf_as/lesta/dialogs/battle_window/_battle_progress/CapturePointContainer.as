package lesta.dialogs.battle_window._battle_progress 
{
    import flash.display.*;
    
    public class CapturePointContainer extends flash.display.MovieClip
    {
        public function CapturePointContainer()
        {
            super();
            return;
        }

        public function updatePosition(arg1:int, arg2:int):int
        {
            x = arg1 + int(this.size.width / 2);
            y = arg2;
            return arg1 + this.size.width;
        }

        public var size:flash.display.Sprite;

        public var content:lesta.dialogs.battle_window._battle_progress.CapturePointIndicator;
    }
}
