package lesta.dialogs.battle_window._battle_progress 
{
    import flash.display.*;
    import flash.events.*;
    import flash.text.*;
    import flash.utils.*;
    import lesta.cpp.*;
    import lesta.utils.*;
    
    public class ScoreCounter extends flash.display.MovieClip
    {
        public function ScoreCounter()
        {
            super();
            stop();
            this.mainText.stop();
            this.animText.stop();
            this.mainTextField = this.mainText.inner.getChildByName("textField") as flash.text.TextField;
            this.animatedTextField = this.animText.inner.getChildByName("textField") as flash.text.TextField;
            this.tweener = new ScoreTweener([this.mainTextField, this.animatedTextField]);
            this.initializeLabels();
            this.initialize();
            return;
        }

        public function set settings(arg1:uint):void
        {
            this._settings = arg1;
            checkCreateSettingsArrays();
            var loc1:*=this._settings;
            switch (loc1) 
            {
                case SETTINGS_SCORE:
                {
                    this.raiseSegments = SCORE_RAISE_SEGMENTS;
                    this.settingsAdd = scoreSettingAddSub[0];
                    this.settingsSub = scoreSettingAddSub[1];
                    break;
                }
                case SETTINGS_RIBBON_APPEAR:
                {
                    this.raiseSegments = RIBBON_APPEAR_RAISE_SEGMENTS;
                    this.settingsAdd = ribbonAppearSettingAddSub[0];
                    this.settingsSub = ribbonAppearSettingAddSub[1];
                    break;
                }
                case SETTINGS_RIBBON_ICON:
                {
                    this.raiseSegments = RIBBON_ICON_RAISE_SEGMENTS;
                    this.settingsAdd = ribbonIconSettingAddSub[0];
                    this.settingsSub = ribbonIconSettingAddSub[1];
                    break;
                }
            }
            return;
        }

        public function get score():int
        {
            return this.currentScore + this.addingScore;
        }

        public function updateScore(arg1:int):void
        {
            if (this.initialized) 
            {
                this.addScore(arg1 - this.currentScore - this.addingScore);
            }
            else 
            {
                this.setInitialScore(arg1);
            }
            return;
        }

        public function setInitialScore(arg1:int):void
        {
            this.scaleClip(this.mainText.inner, 1);
            this.scaleClip(this.animText.inner, 1);
            this.currentScore = arg1;
            this.tweener.writeScore(arg1);
            this.initialized = true;
            return;
        }

        public function appear(arg1:int=0, arg2:int=1):void
        {
            this.setInitialScore(arg1);
            this.currentPhase = PHASE_APPEAR;
            this.addScore(arg2);
            return;
        }

        public function addScore(arg1:int):void
        {
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=null;
            if (!arg1) 
            {
                return;
            }
            this.addingScore = this.addingScore + arg1;
            if (this.ally) 
            {
                this.animText.gotoAndStop(this.addingScore > 0 ? FRAME_POSITIVE : FRAME_NEGATIVE);
            }
            else 
            {
                this.animText.gotoAndStop(FRAME_NEUTRAL);
            }
            var loc1:*=this.getAnimationSetting(this.score);
            this.scaleClip(this.mainText.inner, loc1.startOverScale, loc1.scaleChangeTime);
            this.scaleClip(this.animText.inner, loc1.startOverScale, loc1.scaleChangeTime);
            this.currentAnimationSetting = this.getAnimationSetting(this.addingScore);
            var loc5:*=this.currentPhase;
            switch (loc5) 
            {
                case PHASE_STATIC:
                {
                    this.startRaise("raise");
                    break;
                }
                case PHASE_RAISE:
                {
                    this.tweener.adjustTween(this.scoreTo, this.currentAnimationSetting.end - currentFrame);
                    break;
                }
                case PHASE_EXPLODE:
                {
                    loc2 = currentLabel;
                    loc3 = 0;
                    if (loc2) 
                    {
                        if (loc4 = this.labelDescr[loc2]) 
                        {
                            loc3 = loc4.len - (currentFrame - loc4.frame);
                        }
                    }
                    if (loc3 > 0) 
                    {
                        this.tweener.adjustTween(this.scoreTo, loc3);
                    }
                    else 
                    {
                        this.tweener.stopTween();
                        this.tweener.writeScore(this.scoreTo);
                    }
                    break;
                }
                case PHASE_WAITING:
                {
                    this.waitTimer.stop();
                    this.startRaise("raise");
                    break;
                }
                case PHASE_APPEAR:
                {
                    this.startRaise("appear");
                    break;
                }
            }
            return;
        }

        public function onRaise(arg1:int):void
        {
            var loc1:*=this.addingScore > 0 ? this.settingsAdd : this.settingsSub;
            var loc2:*=loc1[(arg1 - 1)];
            if (loc2.label == this.currentAnimationSetting.label) 
            {
                var loc3:*;
                var loc4:*=((loc3 = this).currentPhase + 1);
                loc3.currentPhase = loc4;
                gotoAndPlay(this.currentAnimationSetting.label);
            }
            return;
        }

        internal function onTweenDelayTimerComplete(arg1:flash.events.TimerEvent):void
        {
            this.tweenDelayTimer.reset();
            this.currentPhase = PHASE_RAISE;
            if (this.currentAnimationSetting.count > 0) 
            {
                this.tweener.makeTween(this.scoreFrom, this.scoreTo, this.currentAnimationSetting.raiseLength);
            }
            else 
            {
                this.tweener.writeScore(this.scoreTo);
            }
            return;
        }

        public function onEndAnim():void
        {
            this.currentAnimationSetting = null;
            this.currentScore = this.currentScore + this.addingScore;
            this.currentPhase = PHASE_STATIC;
            this.addingScore = 0;
            this.tweener.stopTween();
            gotoAndStop("normal");
            return;
        }

        public function wait(arg1:Number):void
        {
            if (arg1 <= 0) 
            {
                return;
            }
            if (this.waitTimer) 
            {
                this.waitTimer.stop();
                this.waitTimer.reset();
            }
            else 
            {
                this.waitTimer = new flash.utils.Timer(1000, 1);
                this.waitTimer.addEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.onWaitTimerComplete);
            }
            this.waitTimer.delay = arg1 * 1000;
            this.waitTimer.start();
            stop();
            this.currentPhase = PHASE_WAITING;
            this.currentScore = this.currentScore + this.addingScore;
            this.addingScore = 0;
            return;
        }

        internal function onWaitTimerComplete(arg1:flash.events.TimerEvent):void
        {
            play();
            return;
        }

        internal static function createSettingsFor(arg1:Array, arg2:Array):Array
        {
            var loc4:*=null;
            var loc5:*=0;
            var loc6:*=null;
            var loc7:*=0;
            var loc8:*=null;
            var loc9:*=NaN;
            var loc10:*=NaN;
            var loc1:*=[arg1, arg2];
            var loc2:*=[];
            var loc3:*=0;
            while (loc3 < loc1.length) 
            {
                loc5 = (loc4 = loc1[loc3]).length;
                loc6 = [];
                loc7 = 0;
                while (loc7 < loc5) 
                {
                    loc9 = (loc8 = loc4[loc7])["startOverScale"] == null ? 1 : loc8["startOverScale"];
                    loc10 = loc8["scaleChangeTime"] == null ? 0 : loc8["scaleChangeTime"];
                    loc6.push(new AnimationSetting(loc8["score"], loc8["count"], loc8["label"], loc8["raiseLength"], loc9, loc10));
                    ++loc7;
                }
                loc2.push(loc6);
                ++loc3;
            }
            return loc2;
        }

        internal static function checkCreateSettingsArrays():void
        {
            if (!scoreSettingAddSub) 
            {
                scoreSettingAddSub = createSettingsFor(SCORE_ANIMATION_SETTINGS_ADD, SCORE_ANIMATION_SETTINGS_SUB);
            }
            if (!ribbonAppearSettingAddSub) 
            {
                ribbonAppearSettingAddSub = createSettingsFor(RIBBON_APPEAR_ANIMATION_SETTINGS_ADD, RIBBON_APPEAR_ANIMATION_SETTINGS_SUB);
            }
            if (!ribbonIconSettingAddSub) 
            {
                ribbonIconSettingAddSub = createSettingsFor(RIBBON_ICON_ANIMATION_SETTINGS_ADD, RIBBON_ICON_ANIMATION_SETTINGS_SUB);
            }
            return;
        }

        
        {
            classFramesDescriptors = new flash.utils.Dictionary();
        }

        internal function startRaise(arg1:String):void
        {
            gotoAndPlay(arg1);
            if (this.currentAnimationSetting.counterWaitFrames > 0) 
            {
                this.currentPhase = PHASE_DELAY;
                this.tweener.writeScore(this.scoreFrom);
                this.tweenDelayTimer.delayFrames = this.currentAnimationSetting.counterWaitFrames;
                this.tweenDelayTimer.start();
            }
            else 
            {
                this.currentPhase = PHASE_RAISE;
                this.onTweenDelayTimerComplete(null);
            }
            return;
        }

        internal function scaleClip(arg1:flash.display.Sprite, arg2:Number, arg3:Number=0):void
        {
            if (arg3) 
            {
                lesta.cpp.Tween.to(arg1, arg3, {"scaleX":arg1.scaleX, "scaleY":arg1.scaleY}, {"scaleX":arg2, "scaleY":arg2}, null, 1);
            }
            else 
            {
                var loc1:*;
                arg1.scaleY = loc1 = arg2;
                arg1.scaleX = loc1;
            }
            return;
        }

        internal function getAnimationSetting(arg1:int):AnimationSetting
        {
            var loc1:*=arg1 > 0 ? this.settingsAdd : this.settingsSub;
            var loc2:*=Math.abs(arg1);
            var loc3:*=(loc1.length - 1);
            while (loc3 >= 0) 
            {
                if (loc2 >= loc1[loc3].score) 
                {
                    loc1[loc3].start = this.raiseSegments[0];
                    loc1[loc3].end = this.raiseSegments[loc3 + 1];
                    return loc1[loc3];
                }
                --loc3;
            }
            return null;
        }

        internal function get scoreFrom():int
        {
            return this.addingScore > 0 ? Math.max(this.currentScore, this.scoreTo - this.currentAnimationSetting.count) : Math.min(this.currentScore, this.scoreTo + this.currentAnimationSetting.count);
        }

        internal function get scoreTo():int
        {
            return this.currentScore + this.addingScore;
        }

        internal function initializeLabels():void
        {
            var loc2:*=0;
            var loc3:*=0;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=0;
            var loc1:*=flash.utils.getQualifiedClassName(this);
            if (loc1 in lesta.dialogs.battle_window._battle_progress.ScoreCounter.classFramesDescriptors) 
            {
                this.labelDescr = lesta.dialogs.battle_window._battle_progress.ScoreCounter.classFramesDescriptors[loc1];
            }
            else 
            {
                this.labelDescr = new flash.utils.Dictionary();
                loc2 = currentLabels.length;
                loc3 = 0;
                while (loc3 < loc2) 
                {
                    loc4 = new LabelDescriptor();
                    loc5 = currentLabels[loc3].name;
                    loc4.name = loc5;
                    loc4.frame = currentLabels[loc3].frame;
                    loc6 = 0;
                    if (loc3 != (loc2 - 1)) 
                    {
                        loc6 = currentLabels[loc3 + 1].frame;
                    }
                    else 
                    {
                        loc6 = totalFrames + 1;
                    }
                    --loc6;
                    loc4.len = loc6 - currentLabels[loc3].frame;
                    this.labelDescr[loc5] = loc4;
                    ++loc3;
                }
                lesta.dialogs.battle_window._battle_progress.ScoreCounter.classFramesDescriptors[loc1] = this.labelDescr;
            }
            return;
        }

        protected function initialize():void
        {
            this.tweenDelayTimer = new lesta.utils.EnterFrameTimer();
            this.tweenDelayTimer.repeatCount = 1;
            this.tweenDelayTimer.addEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.onTweenDelayTimerComplete);
            return;
        }

        public function fini():void
        {
            stop();
            if (this.waitTimer) 
            {
                this.waitTimer.removeEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.onWaitTimerComplete);
                this.waitTimer.stop();
                this.waitTimer = null;
            }
            this.tweenDelayTimer.removeEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.onTweenDelayTimerComplete);
            this.tweenDelayTimer.stop();
            this.tweenDelayTimer = null;
            return;
        }

        public function get settings():uint
        {
            return this._settings;
        }

        public static const SETTINGS_RIBBON_APPEAR:uint=2;

        internal static const SCORE_RAISE_SEGMENTS:Array=[11, 21, 26, 30];

        internal static const SCORE_ANIMATION_SETTINGS_ADD:Array=[{"score":0, "count":11, "label":"0+", "raiseLength":10}, {"score":100, "count":13, "label":"100+", "raiseLength":15}, {"score":300, "count":15, "label":"300+", "raiseLength":20}];

        internal static const SCORE_ANIMATION_SETTINGS_SUB:Array=[{"score":0, "count":11, "label":"0+", "raiseLength":20}, {"score":100, "count":13, "label":"100+", "raiseLength":25}, {"score":300, "count":15, "label":"300+", "raiseLength":30}];

        internal static const RIBBON_APPEAR_RAISE_SEGMENTS:Array=[23, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37];

        internal static const RIBBON_APPEAR_ANIMATION_SETTINGS_ADD:Array=[{"score":0, "count":1, "label":"0+", "raiseLength":5, "startOverScale":1, "scaleChangeTime":0.15}, {"score":1, "count":2, "label":"0+", "raiseLength":5, "startOverScale":1.1, "scaleChangeTime":0.15}, {"score":2, "count":3, "label":"0+", "raiseLength":5, "startOverScale":1.2, "scaleChangeTime":0.15}, {"score":3, "count":4, "label":"0+", "raiseLength":5, "startOverScale":1.3, "scaleChangeTime":0.15}, {"score":4, "count":5, "label":"0+", "raiseLength":10, "startOverScale":1.4, "scaleChangeTime":0.15}, {"score":5, "count":5, "label":"0+", "raiseLength":10, "startOverScale":1.5, "scaleChangeTime":0.15}, {"score":6, "count":5, "label":"0+", "raiseLength":10, "startOverScale":1.6, "scaleChangeTime":0.15}, {"score":7, "count":5, "label":"0+", "raiseLength":10, "startOverScale":1.7, "scaleChangeTime":0.15}, {"score":8, "count":5, "label":"0+", "raiseLength":10, "startOverScale":1.8, "scaleChangeTime":0.15}, {"score":9, "count":5, "label":"0+", "raiseLength":10, "startOverScale":1.9, "scaleChangeTime":0.15}, {"score":10, "count":5, "label":"0+", "raiseLength":10, "startOverScale":2, "scaleChangeTime":0.15}];

        internal static const RIBBON_APPEAR_ANIMATION_SETTINGS_SUB:Array=[{"score":0, "count":1, "label":"0+", "raiseLength":5}, {"score":2, "count":1, "label":"2+", "raiseLength":5}, {"score":5, "count":5, "label":"5+", "raiseLength":5}];

        internal static const RIBBON_ICON_RAISE_SEGMENTS:Array=[24, 33, 34, 35];

        internal static const RIBBON_ICON_ANIMATION_SETTINGS_ADD:Array=[{"score":0, "count":2, "label":"0+", "raiseLength":5}, {"score":2, "count":5, "label":"2+", "raiseLength":5}, {"score":5, "count":7, "label":"5+", "raiseLength":5}];

        internal static const RIBBON_ICON_ANIMATION_SETTINGS_SUB:Array=[{"score":0, "count":1, "label":"0+", "raiseLength":5}, {"score":2, "count":1, "label":"2+", "raiseLength":5}, {"score":5, "count":5, "label":"5+", "raiseLength":5}];

        public static const SETTINGS_RIBBON_ICON:uint=3;

        internal static const FRAME_NEUTRAL:int=1;

        internal static const FRAME_POSITIVE:int=2;

        internal static const FRAME_NEGATIVE:int=3;

        internal static const PHASE_STATIC:int=0;

        internal static const PHASE_DELAY:int=1;

        internal static const PHASE_RAISE:int=2;

        internal static const PHASE_EXPLODE:int=3;

        internal static const PHASE_WAITING:int=4;

        internal static const PHASE_APPEAR:int=5;

        public static const SETTINGS_SCORE:uint=1;

        public var mainText:flash.display.MovieClip;

        public var animText:flash.display.MovieClip;

        public var ally:Boolean;

        internal var mainTextField:flash.text.TextField;

        internal var animatedTextField:flash.text.TextField;

        internal var currentScore:int;

        internal var addingScore:int=0;

        internal var currentAnimationSetting:AnimationSetting;

        internal var initialized:Boolean=false;

        protected var tweener:ScoreTweener;

        internal static var scoreSettingAddSub:Array;

        internal var waitTimer:flash.utils.Timer;

        internal var settingsSub:Array;

        internal var lastScoreShown:lesta.dialogs.battle_window._battle_progress.IncomingScore;

        internal var currentPhase:int=0;

        internal var settingsAdd:Array;

        internal var raiseSegments:Array;

        internal var _settings:uint=0;

        internal static var ribbonIconSettingAddSub:Array;

        protected static var classFramesDescriptors:flash.utils.Dictionary;

        internal var tweenDelayTimer:lesta.utils.EnterFrameTimer;

        internal var labelDescr:flash.utils.Dictionary;

        internal static var ribbonAppearSettingAddSub:Array;
    }
}

import flash.events.*;
import lesta.utils.*;


class AnimationSetting extends Object
{
    public function AnimationSetting(arg1:int, arg2:int, arg3:String, arg4:int, arg5:Number=1, arg6:Number=0)
    {
        super();
        this.score = arg1;
        this.count = arg2;
        this.label = arg3;
        this.raiseLength = arg4;
        this.startOverScale = arg5;
        this.scaleChangeTime = arg6;
        return;
    }

    public function get length():int
    {
        return this.end - this.start;
    }

    public function get counterWaitFrames():int
    {
        return this.end - this.start - this.raiseLength;
    }

    public function toString():String
    {
        return "[AnimationSetting: score = " + this.score + ", count = " + this.count + ", label = " + this.label + ", raiseLength = " + this.raiseLength + ", startOverScale = " + this.startOverScale + ", scaleChangeTime = " + this.scaleChangeTime + "]";
    }

    public var score:int=0;

    public var count:int=10;

    public var label:String=null;

    public var raiseLength:int=0;

    public var start:int=0;

    public var end:int=0;

    public var startOverScale:Number;

    public var scaleChangeTime:Number;
}

class ScoreTweener extends Object
{
    public function ScoreTweener(arg1:Array)
    {
        super();
        this.textFields = arg1;
        this.timer = new lesta.utils.EnterFrameTimer();
        this.timer.repeatCount = 1;
        this.timer.addEventListener(flash.events.TimerEvent.TIMER, this.onTimer);
        this.timer.addEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.onTimerComplete);
        return;
    }

    public function adjustTween(arg1:int, arg2:int=-1):void
    {
        if (this.timer.running) 
        {
            this.scoreFrom = this.scoreFrom + (arg1 - this.scoreFrom) * this.timer.progress;
            this.scoreTo = arg1;
            if (arg2 >= 0) 
            {
                this.timer.repeatCount = arg2;
            }
            else 
            {
                this.timer.repeatCount = this.timer.repeatCount - this.timer.currentCount;
            }
        }
        else 
        {
            this.scoreFrom = this.scoreTo;
            this.scoreTo = arg1;
            this.timer.repeatCount = arg2;
        }
        if (this.timer.repeatCount > 0) 
        {
            this.timer.restart();
        }
        else 
        {
            this.stopTween();
        }
        return;
    }

    public function makeTween(arg1:int, arg2:int, arg3:int):void
    {
        this.scoreFrom = arg1;
        this.scoreTo = arg2;
        this.timer.repeatCount = arg3;
        if (!this.timer.running) 
        {
            this.writeScore(arg1);
            this.timer.restart();
        }
        return;
    }

    public function stopTween():void
    {
        this.timer.stop();
        this.writeScore(this.scoreTo);
        return;
    }

    public function writeScore(arg1:int):void
    {
        var loc1:*=0;
        while (loc1 < this.textFields.length) 
        {
            this.textFields[loc1].text = this.preffix + String(arg1);
            ++loc1;
        }
        return;
    }

    internal function onTimerComplete(arg1:flash.events.TimerEvent):void
    {
        this.writeScore(this.scoreTo);
        return;
    }

    internal function onTimer(arg1:flash.events.TimerEvent):void
    {
        this.writeScore(int(this.scoreFrom + (this.scoreTo - this.scoreFrom) * this.timer.progress));
        return;
    }

    public var preffix:String="";

    internal var scoreFrom:int=0;

    internal var scoreTo:int=0;

    internal var textFields:Array;

    internal var timer:lesta.utils.EnterFrameTimer;
}

class LabelDescriptor extends Object
{
    public function LabelDescriptor()
    {
        super();
        return;
    }

    public function toString():String
    {
        return "LabelDescriptor{name=" + String(this.name) + ",frame=" + String(this.frame) + ",len=" + String(this.len) + "}";
    }

    public var name:String;

    public var frame:int;

    public var len:int;
}