package lesta.dialogs.battle_window._battle_progress 
{
    import flash.display.*;
    import flash.utils.*;
    import lesta.constants.*;
    import lesta.controls.*;
    import lesta.structs.*;
    import lesta.utils.*;
    
    public class CapturePointIndicator extends lesta.controls.AnimatedStateClip
    {
        public function CapturePointIndicator()
        {
            super();
            return;
        }

        public function initialize(arg1:lesta.structs.CapturePointInfo):void
        {
            var loc2:*=null;
            var loc3:*=null;
            this.pointId = arg1.id;
            this.isLargeBase = arg1.type == lesta.structs.CapturePointType.MEGABASE || arg1.type == lesta.structs.CapturePointType.BASE_WITH_POINTS;
            this.updateSymbol(arg1);
            this.indicatorsFX.stop();
            this.setupIndicators(arg1);
            var loc1:*=lesta.utils.GameInfoHolder.instance.getCollectionAsDictionary(lesta.utils.GameInfoHolder.SERVER_TIMERS);
            if (arg1.timerName in loc1) 
            {
                loc2 = loc1[arg1.timerName];
                loc3 = new lesta.dialogs.battle_window._battle_progress.CapturePointTimerController(loc2, this.timer);
            }
            return;
        }

        internal function setupIndicators(arg1:lesta.structs.CapturePointInfo):void
        {
            var loc1:*=this.indicators.getChildByName("captureIndicator") as lesta.dialogs.battle_window._battle_progress.ColoredIndicator;
            var loc2:*=this.indicators.getChildByName("captureIndicatorLarge") as lesta.dialogs.battle_window._battle_progress.ColoredIndicator;
            if (loc1) 
            {
                loc1.visible = !this.isLargeBase;
            }
            if (loc2) 
            {
                loc2.visible = this.isLargeBase;
            }
            this.captureIndicator = this.isLargeBase ? loc2 : loc1;
            this.captureIndicator.maximum = PROGRESSBAR_MAX_VALUE;
            this.lockIndicator = this.indicators.getChildByName("lockIndicator") as lesta.dialogs.battle_window._battle_progress.ColoredIndicator;
            this.lockIndicator.maximum = PROGRESSBAR_MAX_VALUE;
            return;
        }

        public function update(arg1:lesta.structs.CapturePointInfo):void
        {
            visible = arg1.visible;
            if (!visible) 
            {
                return;
            }
            this.captureIndicator.backgroundColor = arg1.ownerRelation + 1;
            var loc1:*=arg1.ownerRelation + 1;
            if (this.indicatorsFX.currentFrame == loc1) 
            {
                this.indicatorsFX.stop();
            }
            else 
            {
                this.indicatorsFX.gotoAndStop(arg1.ownerRelation + 1);
            }
            this.captureIndicator.barColor = arg1.invaderRelation + 1;
            this.updateSymbol(arg1);
            if (arg1.ownerRelation == this.indicatorLocked.currentFrame) 
            {
                this.indicatorLocked.stop();
            }
            else 
            {
                this.indicatorLocked.gotoAndStop(arg1.ownerRelation);
            }
            this.captureIndicator.value = arg1.captureProgress * PROGRESSBAR_MAX_VALUE;
            if (arg1.invaderRelation == lesta.constants.PlayerRelation.ENEMY && this.captureIndicator.maskClip.scaleX > 0 || arg1.invaderRelation == lesta.constants.PlayerRelation.FRIEND && this.captureIndicator.maskClip.scaleX < 0) 
            {
                this.captureIndicator.maskClip.scaleX = -this.captureIndicator.maskClip.scaleX;
            }
            var loc2:*=this.isLargeBase ? 3 : 0;
            loc2 = loc2 + int(arg1.ownerRelation);
            this.indicatorLocked.gotoAndStop(loc2);
            this.lockIndicator.stop();
            if (arg1.active) 
            {
                if (arg1.isLocked) 
                {
                    setState("locked");
                }
                else if (arg1.lockProgress > 0 && arg1.lockProgress < 1) 
                {
                    setState("locking");
                }
                else if (arg1.captureProgress > 0 && arg1.captureProgress < 1) 
                {
                    setState("capture");
                }
                else if (arg1.captureProgress != 0) 
                {
                    setState("normal");
                }
                else 
                {
                    setState("captured");
                }
            }
            else 
            {
                setState("not_active");
            }
            return;
        }

        internal function updateSymbol(arg1:lesta.structs.CapturePointInfo):void
        {
            var loc2:*=null;
            var loc1:*=this.getSymbolFrame(arg1);
            if (this.lastSymbolFrame != loc1) 
            {
                this.symbolContainer.symbol.gotoAndStop(loc1);
                this.symbolContainerFX.symbol.gotoAndStop(loc1);
                this.lastSymbolFrame = loc1;
                if (!this.isFlagPoint(arg1.type)) 
                {
                    loc2 = arg1.type != lesta.structs.CapturePointType.CONTROL ? (this.pointId + 1).toString() : LETTERS[this.pointId];
                    this.symbolContainer.symbol.textField.text = loc2;
                    this.symbolContainerFX.symbol.textField.text = loc2;
                }
            }
            return;
        }

        internal function getSymbolFrame(arg1:lesta.structs.CapturePointInfo):int
        {
            var loc1:*=arg1.ownerRelation != lesta.constants.PlayerRelation.NEUTRAL ? arg1.ownerRelation + 1 : 1;
            var loc2:*=this.isFlagPoint(arg1.type) ? 3 : 0;
            return loc1 + loc2;
        }

        internal function isFlagPoint(arg1:int):Boolean
        {
            var loc1:*=arg1 == lesta.structs.CapturePointType.BASE_WITH_POINTS || arg1 == lesta.structs.CapturePointType.MEGABASE || arg1 == lesta.structs.CapturePointType.BASE;
            return loc1;
        }

        internal static const PROGRESSBAR_MAX_VALUE:int=100;

        internal static const LETTERS:Array=["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K"];

        public var symbolContainerFX:flash.display.MovieClip;

        public var symbolContainer:flash.display.MovieClip;

        public var indicators:flash.display.MovieClip;

        public var indicatorsFX:flash.display.MovieClip;

        public var indicatorLocked:flash.display.MovieClip;

        public var timer:lesta.controls.AnimatedStateClip;

        public var alertIndicatorContainer:flash.display.MovieClip;

        public var alertIndicatorContainerMinimap:flash.display.MovieClip;

        internal var captureIndicator:lesta.dialogs.battle_window._battle_progress.ColoredIndicator;

        internal var lockIndicator:lesta.dialogs.battle_window._battle_progress.ColoredIndicator;

        internal var pointId:int;

        internal var lastSymbolFrame:int=-1;

        internal var isLargeBase:Boolean=false;
    }
}
