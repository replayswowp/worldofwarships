package lesta.dialogs.battle_window._battle_progress 
{
    import flash.display.*;
    import flash.text.*;
    
    public class IncomingScore extends flash.display.MovieClip
    {
        public function IncomingScore()
        {
            var loc1:*=0;
            super();
            this.textField = this.textClip.getChildByName("textField") as flash.text.TextField;
            if (!labelMap) 
            {
                labelMap = new Object();
                loc1 = 0;
                while (loc1 < currentLabels.length) 
                {
                    labelMap[currentLabels[loc1].name] = currentLabels[loc1].frame;
                    ++loc1;
                }
            }
            return;
        }

        public function initialize(arg1:int, arg2:lesta.dialogs.battle_window._battle_progress.ScoreController):void
        {
            this.owner = arg2;
            this._score = arg1;
            this.textField.text = this._score > 0 ? "+" + this._score : String(this._score);
            this.initialized = true;
            gotoAndPlay(arg1 > 0 ? "add" : "sub");
            return;
        }

        public function add(arg1:int):Boolean
        {
            if (arg1 > 0 != this._score > 0) 
            {
                return false;
            }
            var loc1:*=this._score > 0 ? "add" : "sub";
            var loc2:*=labelMap[loc1];
            if (currentFrame - loc2 > this.ADD_NEW_SCORE_BEFORE) 
            {
                return false;
            }
            this._score = this._score + arg1;
            this.textField.text = this._score > 0 ? "+" + this._score : String(this._score);
            return true;
        }

        public function get score():int
        {
            return this._score;
        }

        public function free():void
        {
            stop();
            this._score = 0;
            this.owner = null;
            this.initialized = false;
            return;
        }

        protected function onEndAnim():void
        {
            if (!this.initialized) 
            {
                return;
            }
            this.owner.removeScoreClip(this);
            return;
        }

        protected const ADD_NEW_SCORE_BEFORE:int=10;

        public var textClip:flash.display.MovieClip;

        internal var initialized:Boolean=false;

        internal var owner:lesta.dialogs.battle_window._battle_progress.ScoreController;

        internal var textField:flash.text.TextField;

        internal var _score:int=0;

        internal static var labelMap:Object;
    }
}
