package lesta.dialogs.battle_window._battle_progress 
{
    import flash.utils.*;
    import lesta.utils.*;
    
    public class ScoreController extends Object
    {
        public function ScoreController(arg1:lesta.dialogs.battle_window._battle_progress.ScoreCounter, arg2:Boolean)
        {
            super();
            this.pool = new lesta.utils.ObjectPool(flash.utils.getDefinitionByName("IncomingScoreClip") as Class);
            this.scoreCounter = arg1;
            this.scoreCounter.settings = lesta.dialogs.battle_window._battle_progress.ScoreCounter.SETTINGS_SCORE;
            var loc1:*;
            this.ally = loc1 = arg2;
            this.scoreCounter.ally = loc1;
            return;
        }

        public function updateScore(arg1:int):void
        {
            if (arg1 == this.scoreCounter.score) 
            {
                return;
            }
            var loc1:*=arg1 - this.scoreCounter.score;
            if (!(this.lastScoreShown && this.lastScoreShown.add(loc1))) 
            {
                this.lastScoreShown = this.pool.alloc() as lesta.dialogs.battle_window._battle_progress.IncomingScore;
                this.lastScoreShown.initialize(loc1, this);
                this.lastScoreShown.x = this.ally ? this.scoreCounter.mainText.x - this.scoreCounter.mainText.inner.textField.textWidth - this.lastScoreShown.textClip.textField.textWidth / 2 - this.ADDED_SCORE_OFFSET : this.scoreCounter.mainText.x + this.scoreCounter.mainText.inner.textField.textWidth + this.lastScoreShown.textClip.textField.textWidth / 2 + this.ADDED_SCORE_OFFSET;
                this.scoreCounter.addChild(this.lastScoreShown);
            }
            this.scoreCounter.updateScore(arg1);
            return;
        }

        public function removeScoreClip(arg1:lesta.dialogs.battle_window._battle_progress.IncomingScore):void
        {
            if (this.lastScoreShown == arg1) 
            {
                this.lastScoreShown = null;
            }
            this.scoreCounter.removeChild(arg1);
            arg1.free();
            this.pool.free(arg1);
            return;
        }

        protected const ADDED_SCORE_OFFSET:int=18;

        internal var pool:lesta.utils.ObjectPool;

        internal var scoreCounter:lesta.dialogs.battle_window._battle_progress.ScoreCounter;

        internal var lastScoreShown:lesta.dialogs.battle_window._battle_progress.IncomingScore;

        internal var ally:Boolean;

        internal var currentScore:int=0;
    }
}
