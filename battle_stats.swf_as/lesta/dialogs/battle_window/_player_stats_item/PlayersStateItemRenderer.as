package lesta.dialogs.battle_window._player_stats_item 
{
    import flash.display.*;
    import lesta.constants.*;
    import lesta.controls.*;
    import lesta.structs.*;
    
    public class PlayersStateItemRenderer extends flash.display.Sprite
    {
        public function PlayersStateItemRenderer()
        {
            this.contextMenuScope = {};
            super();
            this.shipIcon = new lesta.controls.Image();
            this.shipIcon.init();
            this.item.mvc_ship_position.addChild(this.shipIcon);
            this.priorityIcon = this.item.priorityMarkers as lesta.dialogs.battle_window._player_stats_item.PriorityIcon;
            this.division.visible = false;
            return;
        }

        public function set data(arg1:lesta.structs.Player):void
        {
            if (!this._data) 
            {
                this.setConstantData(arg1);
            }
            this._data = arg1;
            this.setMutableData(arg1);
            return;
        }

        public function get data():lesta.structs.Player
        {
            return this._data;
        }

        public function setConstantData(arg1:lesta.structs.Player):void
        {
            if (arg1.name) 
            {
                if (arg1.name.length > 6) 
                {
                    this.item.txt_user_name.text = arg1.name.substr(0, 6) + "...";
                }
                else 
                {
                    this.item.txt_user_name.text = arg1.name;
                }
            }
            if (arg1.pathShipTinyIcon && arg1.pathOwnShipTinyIcon) 
            {
                this.shipIcon.src = arg1.relation != lesta.constants.PlayerRelation.SELF ? arg1.pathShipTinyIcon : arg1.pathOwnShipTinyIcon;
            }
            this.shipIcon.scaleX = arg1.relation != lesta.constants.PlayerRelation.ENEMY ? ICON_SCALE : -ICON_SCALE;
            this.shipIcon.scaleY = ICON_SCALE;
            if (arg1.division > 0) 
            {
                this.division.gotoAndStop(arg1.isInSameDivision ? arg1.division : arg1.division + 10);
                this.division.visible = true;
            }
            else 
            {
                this.division.visible = false;
            }
            return;
        }

        public function setMutableData(arg1:lesta.structs.Player):void
        {
            var loc1:*=arg1.getFrameLabel();
            if (this.item.currentFrameLabel == loc1) 
            {
                if (arg1.fragsCount != this.lastFragsCount) 
                {
                    this.lastFragsCount = arg1.fragsCount;
                    this.item.txt_frags.text = String(this.lastFragsCount);
                }
            }
            else 
            {
                this.item.gotoAndStop(loc1);
                this.setConstantData(arg1);
                this.lastFragsCount = arg1.fragsCount;
                this.item.txt_frags.text = arg1.fragsCount;
            }
            this.item.cameraClip.visible = arg1.isObserved;
            this.contextMenuScope.nickname = arg1.name;
            this.contextMenuScope.muteStatus = arg1.mute;
            return;
        }

        internal static const ICON_SCALE:Number=0.7;

        public var item:flash.display.MovieClip;

        public var priorityIcon:lesta.dialogs.battle_window._player_stats_item.PriorityIcon;

        public var division:flash.display.MovieClip;

        internal var shipIcon:lesta.controls.Image;

        internal var lastFragsCount:Number=NaN;

        internal var _data:lesta.structs.Player=null;

        internal var crosshairFlag:uint=0;

        public var contextMenuScope:Object;
    }
}
