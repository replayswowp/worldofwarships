package lesta.dialogs.battle_window.context_menu 
{
    import flash.display.*;
    import lesta.managers.*;
    
    public class ContextMenuItem extends flash.display.Sprite
    {
        public function ContextMenuItem()
        {
            super();
            return;
        }

        public function fini():void
        {
            this.manager = null;
            return;
        }

        public function init(arg1:lesta.managers.ContextMenuManager):void
        {
            this.manager = arg1;
            return;
        }

        public function open():void
        {
            return;
        }

        public function get nextItemOffset():Number
        {
            return height;
        }

        public var label:String;

        protected var manager:lesta.managers.ContextMenuManager;
    }
}
