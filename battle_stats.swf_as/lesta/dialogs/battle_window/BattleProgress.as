package lesta.dialogs.battle_window 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.text.*;
    import lesta.constants.*;
    import lesta.dialogs.battle_window._battle_progress.*;
    import lesta.structs.*;
    import lesta.utils.*;
    import scaleform.clik.controls.*;
    import scaleform.clik.data.*;
    
    public class BattleProgress extends flash.display.Sprite
    {
        public function BattleProgress()
        {
            super();
            this.gameInfoHolder = lesta.utils.GameInfoHolder.instance;
            this.gameInfoHolder.addCallback(this, lesta.utils.GameInfoHolder.INIT_CAPTURE_POINTS, this.createCapturePoints);
            var loc1:*=this.gameInfoHolder.gameInfo.gameModeId;
            var loc2:*;
            mouseEnabled = loc2 = false;
            mouseChildren = loc2;
            this.tutorialMode = loc1 == lesta.constants.GameMode.TUTORIAL;
            if (loc1 == lesta.constants.GameMode.AWESOME || loc1 == lesta.constants.GameMode.STANDARD_DOMINATION) 
            {
                this.allyProgressBar.enabled = true;
                this.enemyProgressBar.enabled = true;
                this.allyScoreController = new lesta.dialogs.battle_window._battle_progress.ScoreController(this.allyScore, true);
                this.enemyScoreController = new lesta.dialogs.battle_window._battle_progress.ScoreController(this.enemyScore, false);
            }
            else 
            {
                removeChild(this.allyProgressBar);
                removeChild(this.enemyProgressBar);
                this.allyScore.visible = false;
                this.enemyScore.visible = false;
                this.dividerText.visible = false;
            }
            return;
        }

        public function fini():void
        {
            this.gameInfoHolder.removeCallback(this);
            return;
        }

        public function createCapturePoints():void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=FilterUtil.getSortedItems(this.gameInfoHolder.listCapturePoints as Array);
            this.cpContainers = new Vector.<CapturePointContainer>(loc1.length);
            var loc4:*=0;
            var loc5:*=0;
            var loc6:*=0;
            while (loc6 < loc1.length) 
            {
                loc3 = loc1[loc6];
                loc2 = lesta.dialogs.battle_window._battle_progress.CapturePointFactory.getIndicatorByType(loc3.type);
                addChild(loc2);
                loc2.content.initialize(loc3);
                loc2.content.update(loc3);
                this.cpContainers[loc3.id] = loc2;
                loc4 = loc4 + loc2.size.width;
                loc5 = Math.max(loc5, loc2.size.height);
                ++loc6;
            }
            var loc7:*=-int(loc4 / 2);
            updateIndicatorsLayout(this.cpContainers, loc7, int(loc5 / 2));
            this.gameInfoHolder.addCallback(this, lesta.utils.GameInfoHolder.UPDATE_CAPTURE_POINTS, this.updateCaptureProgress);
            return;
        }

        public function updateScore(arg1:int, arg2:int, arg3:int, arg4:int):void
        {
            if (this.tutorialMode) 
            {
                return;
            }
            this.allyProgressBar.maximum = arg2;
            this.allyProgressBar.value = arg1;
            this.enemyProgressBar.maximum = arg4;
            this.enemyProgressBar.value = arg3;
            this.allyScoreController.updateScore(arg1);
            this.enemyScoreController.updateScore(arg3);
            return;
        }

        public function updateCaptureProgress():void
        {
            var loc2:*=null;
            var loc1:*=this.gameInfoHolder.listCapturePoints;
            var loc3:*=0;
            while (loc3 < loc1.length) 
            {
                loc2 = loc1[loc3];
                this.cpContainers[loc2.id].content.update(loc2);
                ++loc3;
            }
            if (this.tutorialMode) 
            {
                this.updateTutorialPoints(loc1);
            }
            return;
        }

        internal function updateTutorialPoints(arg1:scaleform.clik.data.DataProvider):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=new Vector.<CapturePointContainer>(0);
            var loc4:*=0;
            var loc5:*=0;
            var loc6:*=0;
            while (loc6 < arg1.length) 
            {
                loc1 = arg1[loc6];
                if (loc1.active) 
                {
                    loc2 = this.cpContainers[loc1.id];
                    loc3.push(loc2);
                    loc4 = loc4 + loc2.size.width;
                    loc5 = Math.max(loc5, loc2.size.height);
                }
                ++loc6;
            }
            loc3.fixed = true;
            updateIndicatorsLayout(loc3, -int(loc4 / 2), int(loc5 / 2));
            return;
        }

        internal static function updateIndicatorsLayout(arg1:__AS3__.vec.Vector.<CapturePointContainer>, arg2:int, arg3:int):void
        {
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                arg2 = arg1[loc1].updatePosition(arg2, arg3);
                ++loc1;
            }
            return;
        }

        internal static const TARGETS_LIST_Y_OFFSET:Number=17;

        public var allyProgressBar:scaleform.clik.controls.StatusIndicator;

        public var enemyProgressBar:scaleform.clik.controls.StatusIndicator;

        public var allyScore:lesta.dialogs.battle_window._battle_progress.ScoreCounter;

        public var enemyScore:lesta.dialogs.battle_window._battle_progress.ScoreCounter;

        public var dividerText:flash.text.TextField;

        public var gameInfoHolder:lesta.utils.GameInfoHolder;

        internal var cpContainers:__AS3__.vec.Vector.<CapturePointContainer>;

        internal var allyScoreController:lesta.dialogs.battle_window._battle_progress.ScoreController;

        internal var enemyScoreController:lesta.dialogs.battle_window._battle_progress.ScoreController;

        internal var tutorialMode:Boolean=false;
    }
}

import flash.utils.*;
import lesta.constants.*;
import lesta.structs.*;


class FilterUtil extends Object
{
    public function FilterUtil()
    {
        super();
        return;
    }

    public static function getSortedItems(arg1:Array):Array
    {
        var loc5:*=null;
        var loc1:*=getTypeDict(arg1);
        var loc2:*=[];
        var loc3:*=getCenterItems(loc1);
        var loc4:*=0;
        while (loc4 < ORDER.length) 
        {
            if (loc5 = loc1[ORDER[loc4]]) 
            {
                loc5.sortOn("id");
                loc2 = insertIn(loc2, loc5);
            }
            ++loc4;
        }
        loc2 = insertIn(loc2, loc3);
        return loc2;
    }

    internal static function insertIn(arg1:Array, arg2:Array):Array
    {
        var loc1:*=Math.floor(arg1.length / 2);
        var loc2:*=arg1.slice(0, loc1);
        var loc3:*=arg1.slice(loc1, arg1.length);
        return loc2.concat(arg2).concat(loc3);
    }

    internal static function getCenterItems(arg1:flash.utils.Dictionary):Array
    {
        var loc2:*=0;
        var loc3:*=null;
        var loc4:*=null;
        var loc1:*=[];
        var loc5:*=0;
        var loc6:*=[lesta.structs.CapturePointType.MEGABASE, lesta.structs.CapturePointType.BASE, lesta.structs.CapturePointType.BASE_WITH_POINTS];
        for each (loc2 in loc6) 
        {
            loc3 = [];
            var loc7:*=0;
            var loc8:*=arg1[loc2];
            for each (loc4 in loc8) 
            {
                if (loc4.ownerRelation == lesta.constants.PlayerRelation.NEUTRAL) 
                {
                    loc1.push(loc4);
                    continue;
                }
                loc3.push(loc4);
            }
            arg1[loc2] = loc3;
            if (!loc1.length) 
            {
                continue;
            }
            break;
        }
        return loc1;
    }

    internal static function getTypeDict(arg1:Array):flash.utils.Dictionary
    {
        var loc2:*=null;
        var loc1:*=new flash.utils.Dictionary();
        var loc3:*=0;
        var loc4:*=arg1;
        for each (loc2 in loc4) 
        {
            if (!loc1[loc2.type]) 
            {
                loc1[loc2.type] = [];
            }
            (loc1[loc2.type] as Array).push(loc2);
        }
        return loc1;
    }

    internal static const ORDER:Array=[lesta.structs.CapturePointType.MEGABASE, lesta.structs.CapturePointType.BUILDING_CP, lesta.structs.CapturePointType.BASE, lesta.structs.CapturePointType.BASE_WITH_POINTS, lesta.structs.CapturePointType.CONTROL];
}