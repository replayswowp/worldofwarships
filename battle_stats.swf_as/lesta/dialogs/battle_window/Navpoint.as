package lesta.dialogs.battle_window 
{
    import flash.display.*;
    import flash.events.*;
    import lesta.controls.*;
    import lesta.data.*;
    import lesta.dialogs.battle_window.constants.*;
    import lesta.structs.*;
    import scaleform.gfx.*;
    
    public class Navpoint extends Object
    {
        public function Navpoint(arg1:flash.display.MovieClip)
        {
            super();
            this.clip = arg1 as lesta.controls.AnimatedStateClip;
            this.size = arg1.size;
            this.clipTypeNavpoint = arg1.navpoints;
            this.clipAsMovieClip = arg1;
            this.size.mouseChildren = false;
            this.size.mouseEnabled = false;
            arg1.hitArea = this.size;
            return;
        }

        public function fini():void
        {
            this.clip.removeEventListener(flash.events.MouseEvent.ROLL_OVER, this.onNavpointMouseOver);
            this.clip.removeEventListener(flash.events.MouseEvent.ROLL_OUT, this.onNavpointMouseOut);
            this.clip.removeEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.onNavpointMouseDown);
            if (this.navpointInfo.objectMarker && !this.hidden) 
            {
                var loc1:*;
                var loc2:*=((loc1 = this.navpointInfo.objectMarker).navpointsAmount - 1);
                loc1.navpointsAmount = loc2;
            }
            this.draggingEnabled = false;
            this.isOver = false;
            this.callbackOver(this);
            this.navpointInfo = null;
            this.clip = null;
            this.size = null;
            this.clipTypeNavpoint = null;
            this.callbackOver = null;
            return;
        }

        public function setNavpointInfo(arg1:int, arg2:lesta.structs.NavpointInfo):void
        {
            this.squadronId = arg1;
            this.navpointInfo = arg2;
            var loc1:*=Math.max(2, arg2.type + 2);
            this.clipTypeNavpoint.gotoAndStop(loc1);
            this.clipTypeNavpoint.dirAttack.visible = arg2.type == lesta.dialogs.battle_window.constants.NavpointTypes.ATTACK_RADIUS;
            var loc2:*=arg2.type;
            switch (loc2) 
            {
                case lesta.dialogs.battle_window.constants.NavpointTypes.ATTACK:
                case lesta.dialogs.battle_window.constants.NavpointTypes.ATTACK_RADIUS:
                case lesta.dialogs.battle_window.constants.NavpointTypes.ATTACK_GROUND:
                {
                    this.clipTypeNavpoint.clip.planeType.gotoAndStop(arg2.planeType);
                }
            }
            this.clip.setState("show", false, true, "create");
            this.draggingEnabled = arg2.isDraggable;
            this.setClipHit();
            return;
        }

        public function update():void
        {
            this.clip.alpha = this.navpointInfo.alpha * Math.max(0.4, 1.6 - 0.6 * this.navpointInfo.index);
            this.draggingEnabled = this.navpointInfo.isDraggable;
            return;
        }

        public function stopDragging():void
        {
            if (!this.hidden) 
            {
                this.clip.setState(this.isOver ? "over" : "show");
            }
            return;
        }

        public function showViaSelection():void
        {
            if (this.hidden) 
            {
                this.hidden = false;
                this.clip.visible = true;
                this.clip.setState("show", false, true, "select");
                if (this._draggingEnabled) 
                {
                    this.clip.addEventListener(flash.events.MouseEvent.ROLL_OVER, this.onNavpointMouseOver);
                    this.clip.addEventListener(flash.events.MouseEvent.ROLL_OUT, this.onNavpointMouseOut);
                    this.clip.addEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.onNavpointMouseDown);
                }
            }
            return;
        }

        public function hide():void
        {
            if (!this.hidden) 
            {
                this.hidden = true;
                this.clip.setState("inv", false, true, null, true);
                if (this.navpointInfo.objectMarker) 
                {
                    this.navpointInfo.objectMarker.navpointsAmount = this.navpointInfo.countDuplicated;
                }
                if (this._draggingEnabled) 
                {
                    this.clip.removeEventListener(flash.events.MouseEvent.ROLL_OVER, this.onNavpointMouseOver);
                    this.clip.removeEventListener(flash.events.MouseEvent.ROLL_OUT, this.onNavpointMouseOut);
                    this.clip.removeEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.onNavpointMouseDown);
                    if (this.callbackOver != null) 
                    {
                        this.callbackOver(this, true);
                    }
                }
            }
            return;
        }

        internal function onNavpointMouseOver(arg1:flash.events.MouseEvent):void
        {
            this.isOver = true;
            if (this.clip.state != "dragging") 
            {
                this.clip.setState("over");
            }
            if (this.callbackOver != null) 
            {
                this.callbackOver(this);
            }
            return;
        }

        internal function onNavpointMouseOut(arg1:flash.events.MouseEvent):void
        {
            this.isOver = false;
            if (this.clip.state != "dragging") 
            {
                this.clip.setState("show");
            }
            if (this.callbackOver != null) 
            {
                this.callbackOver(this);
            }
            return;
        }

        internal function onNavpointMouseDown(arg1:flash.events.MouseEvent):void
        {
            var loc1:*=arg1 as scaleform.gfx.MouseEventEx;
            var loc2:*=!(loc1 == null) && loc1.buttonIdx == scaleform.gfx.MouseEventEx.RIGHT_BUTTON;
            this.clip.setState("dragging");
            if (loc2) 
            {
                lesta.data.GameDelegate.call("navpoints.onNavpointRemove", [this.navpointInfo.id]);
            }
            else 
            {
                lesta.data.GameDelegate.call("navpoints.onNavpointMouseDown", [this.navpointInfo.id]);
            }
            return;
        }

        internal function get draggingEnabled():Boolean
        {
            return this._draggingEnabled;
        }

        internal function set draggingEnabled(arg1:Boolean):void
        {
            if (this._draggingEnabled != arg1) 
            {
                this._draggingEnabled = arg1;
                this.setClipHit();
                if (this._draggingEnabled) 
                {
                    this.clip.addEventListener(flash.events.MouseEvent.ROLL_OVER, this.onNavpointMouseOver);
                    this.clip.addEventListener(flash.events.MouseEvent.ROLL_OUT, this.onNavpointMouseOut);
                    this.clip.addEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.onNavpointMouseDown);
                }
                else 
                {
                    this.clip.removeEventListener(flash.events.MouseEvent.ROLL_OVER, this.onNavpointMouseOver);
                    this.clip.removeEventListener(flash.events.MouseEvent.ROLL_OUT, this.onNavpointMouseOut);
                    this.clip.removeEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.onNavpointMouseDown);
                }
            }
            return;
        }

        internal function setClipHit():void
        {
            scaleform.gfx.InteractiveObjectEx.setHitTestDisable(this.clip, !this._draggingEnabled);
            this.clip.mouseChildren = this._draggingEnabled;
            this.clip.mouseEnabled = this._draggingEnabled;
            return;
        }

        public var clip:lesta.controls.AnimatedStateClip;

        public var clipAsMovieClip:flash.display.MovieClip;

        public var size:flash.display.MovieClip;

        public var clipTypeNavpoint:flash.display.MovieClip;

        public var squadronId:int=0;

        public var navpointInfo:lesta.structs.NavpointInfo;

        public var toDelete:Boolean=false;

        public var isOver:Boolean=false;

        public var hidden:Boolean=false;

        public var callbackOver:Function=null;

        internal var _draggingEnabled:Boolean=false;
    }
}
