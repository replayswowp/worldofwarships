package lesta.dialogs.battle_window 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.constants.*;
    import lesta.data.*;
    import lesta.structs.*;
    import lesta.utils.*;
    import scaleform.clik.data.*;
    
    public class HeaderShipsList extends flash.display.Sprite
    {
        public function HeaderShipsList()
        {
            this.alliedItems = [];
            this.enemyItems = [];
            this.itemMap = new flash.utils.Dictionary();
            this.alliedContainer = new flash.display.MovieClip();
            this.enemyContainer = new flash.display.MovieClip();
            new Vector.<HeaderItemData>(2)[0] = null;
            new Vector.<HeaderItemData>(2)[1] = null;
            this.displayingTargetByWeaponType = new Vector.<HeaderItemData>(2);
            super();
            addChild(this.alliedContainer);
            addChild(this.enemyContainer);
            this.alliedData = lesta.utils.GameInfoHolder.instance.listAlliedPlayersDeadFirst;
            this.enemyData = lesta.utils.GameInfoHolder.instance.listEnemyPlayersDeadFirst;
            this.alliedData.addEventListener(flash.events.Event.CHANGE, this.allyHandler, false, 0, true);
            this.enemyData.addEventListener(flash.events.Event.CHANGE, this.enemyHandler, false, 0, true);
            lesta.utils.GameInfoHolder.instance.addCallback(this, lesta.utils.GameInfoHolder.INIT_GAME_INFO, this.onInitGameInfo);
            lesta.data.GameDelegate.addCallBack("battle.weaponLockChanged", this, this.onWeaponLockChanged);
            this.allyHandler();
            this.enemyHandler();
            this.updateDistanceFromCenter();
            this.cacheAsBitmap = true;
            return;
        }

        public function fini():void
        {
            this.alliedData.removeEventListener(flash.events.Event.CHANGE, this.allyHandler);
            this.enemyData.removeEventListener(flash.events.Event.CHANGE, this.enemyHandler);
            lesta.utils.GameInfoHolder.instance.removeCallback(this);
            lesta.data.GameDelegate.removeCallBack(this);
            while (numChildren) 
            {
                removeChildAt(0);
            }
            this.alliedItems = null;
            this.enemyItems = null;
            this.alliedData = null;
            this.enemyData = null;
            this.displayingTargetByWeaponType = null;
            this.itemMap = null;
            return;
        }

        internal function onInitGameInfo():void
        {
            this.updateDistanceFromCenter();
            return;
        }

        internal function updateDistanceFromCenter():void
        {
            var loc1:*=lesta.utils.GameInfoHolder.instance.gameInfo.gameModeId == lesta.constants.GameMode.AWESOME;
            this.distanceFromCenter = loc1 ? DISTANCE_FROM_WITHOUT_SCORE : DISTANCE_FROM_CENTER;
            this.updateLayout();
            return;
        }

        internal function allyHandler(arg1:flash.events.Event=null):void
        {
            this.changeHandler(this.alliedData, this.alliedItems, this.alliedContainer);
            return;
        }

        internal function enemyHandler(arg1:flash.events.Event=null):void
        {
            this.changeHandler(this.enemyData, this.enemyItems, this.enemyContainer);
            return;
        }

        internal function changeHandler(arg1:scaleform.clik.data.DataProvider, arg2:Array, arg3:flash.display.MovieClip):void
        {
            if (arg2.length == arg1.length) 
            {
                this.updateData(arg2, arg1, arg3);
            }
            else 
            {
                this.initData(arg2, arg1, arg3);
            }
            return;
        }

        internal function initData(arg1:Array, arg2:scaleform.clik.data.DataProvider, arg3:flash.display.MovieClip):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=0;
            var loc5:*=arg2;
            for each (loc1 in loc5) 
            {
                loc3 = new HeaderItemData(loc1);
                this.itemMap[loc1.id] = loc3;
                arg1.push(loc3);
            }
            loc4 = 0;
            loc5 = arg1;
            for each (loc2 in loc5) 
            {
                arg3.addChild(loc2.movie);
            }
            lesta.utils.DisplayObjectUtils.placeChildrenAsHorizontalStack(arg3, this.gap);
            if (arg3 == this.enemyContainer) 
            {
                arg3.scaleX = -1;
            }
            this.updateLayout();
            return;
        }

        internal function updateData(arg1:Array, arg2:Array, arg3:flash.display.MovieClip):void
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < arg2.length) 
            {
                (loc2 = this.itemMap[(arg2[loc1] as lesta.structs.Player).id]).updateData();
                arg3.setChildIndex(loc2.movie, loc1);
                ++loc1;
            }
            lesta.utils.DisplayObjectUtils.placeChildrenAsHorizontalStack(arg3, this.gap);
            this.updateLayout();
            return;
        }

        internal function updateLayout():void
        {
            this.alliedContainer.x = -this.distanceFromCenter - this.alliedContainer.width;
            this.enemyContainer.x = this.distanceFromCenter + this.enemyContainer.width;
            return;
        }

        internal function onWeaponLockChanged(arg1:int):void
        {
            if (arg1 > lesta.constants.WeaponType.ATBA) 
            {
                return;
            }
            var loc1:*=this.displayingTargetByWeaponType[arg1];
            if (loc1) 
            {
                loc1.priorityIcon.showCrosshair(arg1, false);
            }
            var loc2:*=lesta.utils.GameInfoHolder.instance.mapTargets[arg1];
            loc1 = loc2 ? this.itemMap[loc2.id] : null;
            this.displayingTargetByWeaponType[arg1] = loc1;
            if (loc1) 
            {
                loc1.priorityIcon.showCrosshair(arg1, true);
            }
            return;
        }

        public static const DISTANCE_FROM_CENTER:int=50;

        public static const DISTANCE_FROM_WITHOUT_SCORE:int=50;

        internal var alliedData:scaleform.clik.data.DataProvider;

        internal var enemyData:scaleform.clik.data.DataProvider;

        internal var alliedItems:Array;

        internal var enemyItems:Array;

        internal var itemMap:flash.utils.Dictionary;

        internal var alliedContainer:flash.display.MovieClip;

        internal var enemyContainer:flash.display.MovieClip;

        internal var displayingTargetByWeaponType:__AS3__.vec.Vector.<HeaderItemData>;

        internal var distanceFromCenter:int=50;

        internal var gap:int=6;
    }
}

import flash.display.*;
import flash.utils.*;
import lesta.dialogs.battle_window._player_stats_item.*;
import lesta.structs.*;


class HeaderItemData extends Object
{
    public function HeaderItemData(arg1:lesta.structs.Player)
    {
        super();
        var loc1:*=flash.utils.getDefinitionByName("HeaderItem") as Class;
        this.player = arg1;
        this.movie = new loc1();
        this.movie.size.visible = false;
        this.shipTypeIndex = arg1.shipTypeIndex;
        this.priorityIcon = this.movie.priorityMarkers as lesta.dialogs.battle_window._player_stats_item.PriorityIcon;
        this.updateData(true);
        return;
    }

    public function updateData(arg1:Boolean=false):void
    {
        var loc1:*=this.player.getFrameLabel();
        if (!(this.movie.currentFrameLabel == loc1) || arg1) 
        {
            this.movie.gotoAndStop(loc1);
        }
        var loc2:*=this.player.shipTypeIndex + 1;
        var loc3:*;
        if (!((loc3 = this.movie.getChildAt(0) as flash.display.MovieClip).currentFrame == loc2) || arg1) 
        {
            loc3.gotoAndStop(loc2);
        }
        return;
    }

    public var player:lesta.structs.Player;

    public var movie:flash.display.MovieClip;

    public var priorityIcon:lesta.dialogs.battle_window._player_stats_item.PriorityIcon;

    public var shipTypeIndex:int;

    internal var crosshairFlag:uint=0;
}