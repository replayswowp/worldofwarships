package lesta.dialogs.battle_window._ingame_chat 
{
    import flash.events.*;
    import flash.text.*;
    import flash.utils.*;
    import lesta.controls.*;
    import lesta.cpp.*;
    import lesta.data.*;
    import lesta.dialogs.battle_window_controllers.*;
    import lesta.managers.*;
    import lesta.structs.*;
    import scaleform.clik.controls.*;
    import scaleform.gfx.*;
    
    public class QuickChat extends lesta.controls.ChatModel
    {
        public function QuickChat()
        {
            this.numberOfFrames = this.deathTime / this.milisecondInFrame;
            super();
            return;
        }

        protected override function init(arg1:flash.events.Event):void
        {
            super.init(arg1);
            txa_output.alpha = 0;
            this.txtChatHelp.alpha = 0;
            var loc1:*;
            txaHeader.alpha = loc1 = 0.5;
            txa_input.alpha = loc1;
            this.txtChatHelp.text = "IDS_TAB_CHANGE_CHANNEL";
            this.chatDisabledLabel.visible = false;
            this.lifeTimer = new flash.utils.Timer(this.lifeTime, 1);
            this.lifeTimer.addEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.startDeathTimer);
            scaleform.gfx.InteractiveObjectEx.setHitTestDisable(this.txtChatHelp, true);
            this.mute_button.toggle = true;
            this.mute_button.addEventListener(flash.events.Event.SELECT, this.onMuteButtonSelected);
            this.mute_button.visible = false;
            return;
        }

        public function setHintManager(arg1:lesta.managers.BattleHintManager):void
        {
            this.hintManager = arg1;
            var loc1:*=new lesta.structs.ToogleHintInfo(this.mute_button, "IDS_CHAT_DISABLE_HINT", "IDS_CHAT_ENABLE_HINT");
            arg1.register(this.mute_button, loc1);
            return;
        }

        public override function free():void
        {
            this.lifeTimer.removeEventListener(flash.events.TimerEvent.TIMER, this.startDeathTimer);
            this.stopTimer();
            this.lifeTimer = null;
            this.controller = null;
            this.mute_button.removeEventListener(flash.events.Event.SELECT, this.onMuteButtonSelected);
            super.free();
            return;
        }

        public override function incomingMessage(arg1:String, arg2:int, arg3:String):void
        {
            super.incomingMessage(arg1, arg2, arg3);
            this.stopTimer();
            txa_output.alpha = 1;
            if (!inFocus()) 
            {
                this.startLifeTimer();
            }
            return;
        }

        protected override function changeFocusIn(arg1:flash.events.FocusEvent):void
        {
            super.changeFocusIn(arg1);
            this.stopTimer();
            this.txtChatHelp.alpha = 1;
            txa_output.alpha = 1;
            var loc1:*;
            txaHeader.alpha = loc1 = 1;
            txa_input.alpha = loc1;
            this.mute_button.visible = true;
            return;
        }

        protected override function changeFocusOut(arg1:flash.events.FocusEvent):void
        {
            super.changeFocusOut(arg1);
            this.txtChatHelp.alpha = 0;
            var loc1:*;
            txaHeader.alpha = loc1 = 0.5;
            txa_input.alpha = loc1;
            this.startLifeTimer();
            if (arg1.relatedObject != this.mute_button) 
            {
                this.updateMuteButtonVisibility();
            }
            return;
        }

        protected override function resetFocus(arg1:Boolean):void
        {
            super.resetFocus(arg1);
            this.updateMuteButtonVisibility();
            return;
        }

        internal function startLifeTimer():void
        {
            this.stopTimer();
            this.lifeTimer.reset();
            this.lifeTimer.start();
            return;
        }

        internal function startDeathTimer():void
        {
            this.lifeTimer.stop();
            this.deathTxaOutputTweenId = lesta.cpp.Tween.to(txa_output, this.deathTime, {"alpha":txa_output.alpha}, {"alpha":this.HID_ALPHA}, this.stopTimer);
            return;
        }

        internal function stopTimer():void
        {
            this.lifeTimer.stop();
            lesta.cpp.Tween.kill(this.deathTxaOutputTweenId);
            return;
        }

        protected override function setText(arg1:Object):void
        {
            arg1.controller = this.controller;
            super.setText(arg1);
            return;
        }

        internal function onMuteButtonSelected(arg1:flash.events.Event):void
        {
            if (this.mute_button.selected) 
            {
                lesta.data.GameDelegate.call("battleChat.muteWhole", []);
                this.onMuted();
            }
            else 
            {
                lesta.data.GameDelegate.call("battleChat.unmuteWhole", []);
                this.onUnmuted();
            }
            this.resetFocus(true);
            return;
        }

        internal function onMuted():void
        {
            txaHeader.visible = false;
            txa_input.alpha = 0.5;
            var loc1:*;
            txa_input.mouseChildren = loc1 = false;
            txa_input.mouseEnabled = loc1;
            this.chatDisabledLabel.visible = true;
            return;
        }

        internal function onUnmuted():void
        {
            txaHeader.visible = true;
            txa_input.alpha = 1;
            var loc1:*;
            txa_input.mouseChildren = loc1 = true;
            txa_input.mouseEnabled = loc1;
            this.chatDisabledLabel.visible = false;
            return;
        }

        internal function updateMuteButtonVisibility():void
        {
            this.mute_button.visible = this.mute_button.selected;
            return;
        }

        public const HID_ALPHA:Number=0;

        public var chatDisabledLabel:flash.text.TextField;

        public var txtChatHelp:scaleform.clik.controls.TextInput;

        public var mute_button:scaleform.clik.controls.Button;

        public var controller:lesta.dialogs.battle_window_controllers.ComplainController;

        internal var lifeTime:int=25000;

        internal var deathTime:int=3;

        internal var milisecondInFrame:int=41;

        internal var deathTxaOutputTweenId:int=-1;

        internal var numberOfFrames:int;

        internal var lifeTimer:flash.utils.Timer;

        internal var hintManager:lesta.managers.BattleHintManager;
    }
}
