package lesta.dialogs.battle_window.markers.container 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.utils.*;
    import lesta.data.*;
    import lesta.dialogs.battle_window.markers.config.*;
    import lesta.dialogs.battle_window.markers.container.views.base.*;
    import lesta.dialogs.battle_window.markers.settings.*;
    import lesta.dialogs.battle_window.markers.utils.*;
    
    public class AllMarkersContainer extends flash.display.Sprite
    {
        public function AllMarkersContainer(arg1:lesta.dialogs.battle_window.markers.config.MarkersConfig, arg2:lesta.dialogs.battle_window.markers.settings.MarkersSettings)
        {
            super();
            this._configs = arg1;
            this._settings = arg2;
            this.init();
            return;
        }

        internal function init():void
        {
            this.types = new Vector.<lesta.dialogs.battle_window.markers.container.MarkersContainer>(lesta.dialogs.battle_window.markers.container.AllMarkersContainer.TYPES_LENGTH, true);
            this._ships = new lesta.dialogs.battle_window.markers.container.MarkersContainer(lesta.dialogs.battle_window.markers.utils.MarkerTypes.SHIP, this._configs, this._settings);
            this._planes = new lesta.dialogs.battle_window.markers.container.MarkersContainer(lesta.dialogs.battle_window.markers.utils.MarkerTypes.PLANE, this._configs, this._settings);
            this._torpedoes = new lesta.dialogs.battle_window.markers.container.TemporaryMarkersContainer(lesta.dialogs.battle_window.markers.utils.MarkerTypes.TORPEDO, this._configs, this._settings);
            this._capturePoints = new lesta.dialogs.battle_window.markers.container.MarkersContainer(lesta.dialogs.battle_window.markers.utils.MarkerTypes.CAPTURE_POINTS, this._configs, this._settings);
            addChild(this._capturePoints);
            addChild(this._ships);
            addChild(this._torpedoes);
            addChild(this._planes);
            this.types[lesta.dialogs.battle_window.markers.utils.MarkerTypes.PLANE] = this._planes;
            this.types[lesta.dialogs.battle_window.markers.utils.MarkerTypes.SHIP] = this._ships;
            this.types[lesta.dialogs.battle_window.markers.utils.MarkerTypes.TORPEDO] = this._torpedoes;
            this.types[lesta.dialogs.battle_window.markers.utils.MarkerTypes.CAPTURE_POINTS] = this._capturePoints;
            this._priorTargetMarkers = [];
            this.lastLastStateUpdateTime = flash.utils.getTimer();
            return;
        }

        public function fini():void
        {
            var loc2:*=null;
            lesta.data.GameDelegate.removeCallBack(this);
            this._priorTargetMarkers = null;
            var loc1:*=0;
            while (loc1 < lesta.dialogs.battle_window.markers.container.AllMarkersContainer.TYPES_LENGTH) 
            {
                loc2 = this.types[loc1];
                loc2.fini();
                ++loc1;
            }
            return;
        }

        public function highlightMarker(arg1:int, arg2:int):void
        {
            if (this._highlightedMarker) 
            {
                this._highlightedMarker.highlighted = false;
            }
            if (arg1 == -1 || arg2 == -1) 
            {
                return;
            }
            var loc1:*=this.types[arg1];
            var loc2:*=loc1.getMarker(arg2) as lesta.dialogs.battle_window.markers.container.views.base.Marker;
            this._highlightedMarker = loc2;
            if (this._highlightedMarker) 
            {
                this._highlightedMarker.highlighted = true;
            }
            return;
        }

        public function setAsPriorTarget(arg1:int, arg2:int):void
        {
            if (this._priorTargetMarkers[arg1]) 
            {
                this._priorTargetMarkers[arg1].priority = false;
            }
            if (arg1 == -1 || arg2 == -1) 
            {
                return;
            }
            var loc1:*=this.types[arg1];
            var loc2:*=loc1.getMarker(arg2) as lesta.dialogs.battle_window.markers.container.views.base.Marker;
            this._priorTargetMarkers[arg1] = loc2;
            if (loc2) 
            {
                loc2.priority = true;
            }
            return;
        }

        public function hideAllPriorities():void
        {
            var loc1:*=0;
            while (loc1 < this._priorTargetMarkers.length) 
            {
                if (this._priorTargetMarkers[loc1]) 
                {
                    this._priorTargetMarkers[loc1].priority = false;
                    this._priorTargetMarkers[loc1] = null;
                }
                ++loc1;
            }
            return;
        }

        public function update(arg1:Array, arg2:int, arg3:int, arg4:Number, arg5:Boolean=false):void
        {
            this._ships.update(arg1, arg2, arg3, arg4, arg5);
            this._planes.update(arg1, arg2, arg3, arg4, arg5);
            this._torpedoes.update(arg1, arg2, arg3, arg4, arg5);
            this._capturePoints.update(arg1, arg2, arg3, arg4, arg5);
            if (flash.utils.getTimer() - this.lastLastStateUpdateTime > STATES_UPDATE_TICK_PERIOD) 
            {
                this._ships.updateMarkersStatesTick();
                this._planes.updateMarkersStatesTick();
                this._torpedoes.updateMarkersStatesTick();
                this._capturePoints.updateMarkersStatesTick();
                this.lastLastStateUpdateTime = flash.utils.getTimer();
            }
            return;
        }

        public function get ships():lesta.dialogs.battle_window.markers.container.MarkersContainer
        {
            return this._ships;
        }

        public function get planes():lesta.dialogs.battle_window.markers.container.MarkersContainer
        {
            return this._planes;
        }

        public function get torpedoes():lesta.dialogs.battle_window.markers.container.MarkersContainer
        {
            return this._torpedoes;
        }

        public function get capturePoints():lesta.dialogs.battle_window.markers.container.MarkersContainer
        {
            return this._capturePoints;
        }

        public function getMarker(arg1:int, arg2:int):lesta.dialogs.battle_window.markers.container.views.base.SimpleMarker
        {
            if (arg1 < 0 || arg1 >= TYPES_LENGTH) 
            {
                return null;
            }
            return this.types[arg1].getMarker(arg2);
        }

        internal static const STATES_UPDATE_TICK_PERIOD:int=10;

        public static const TYPES_LENGTH:int=4;

        internal var _ships:lesta.dialogs.battle_window.markers.container.MarkersContainer;

        internal var _planes:lesta.dialogs.battle_window.markers.container.MarkersContainer;

        internal var _torpedoes:lesta.dialogs.battle_window.markers.container.TemporaryMarkersContainer;

        internal var _capturePoints:lesta.dialogs.battle_window.markers.container.MarkersContainer;

        public var types:__AS3__.vec.Vector.<lesta.dialogs.battle_window.markers.container.MarkersContainer>;

        internal var _highlightedMarker:lesta.dialogs.battle_window.markers.container.views.base.Marker;

        internal var _priorTargetMarkers:Array;

        internal var _configs:lesta.dialogs.battle_window.markers.config.MarkersConfig;

        internal var _settings:lesta.dialogs.battle_window.markers.settings.MarkersSettings;

        internal var lastLastStateUpdateTime:int=0;

        internal var drawerStarted:Boolean=false;
    }
}
