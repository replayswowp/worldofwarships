package lesta.dialogs.battle_window.markers.container.views.ships 
{
    import flash.display.*;
    import lesta.cpp.*;
    import lesta.utils.*;
    
    public class ShipMarkerPercentBar extends flash.display.MovieClip
    {
        public function ShipMarkerPercentBar()
        {
            super();
            this.init();
            return;
        }

        internal function init():void
        {
            lesta.utils.DisplayObjectUtils.stopRecursive(this);
            return;
        }

        public function fini():void
        {
            lesta.cpp.Tween.kill(this.tweenIdBar);
            lesta.cpp.Tween.kill(this.tweenIdBarDamage);
            lesta.cpp.Tween.kill(this.tweenIdBarSelfDamage);
            this.isDirty = false;
            return;
        }

        public function set maximum(arg1:Number):void
        {
            this._maximum = arg1;
            return;
        }

        public function setValue(arg1:Number, arg2:Number, arg3:Number):void
        {
            this.health = arg1;
            this.damage = this.damage + arg2;
            this.selfDamage = this.selfDamage + arg3;
            this.isDirty = true;
            this.updateData();
            return;
        }

        internal function applyTween():void
        {
            var loc1:*=this.health / this._maximum;
            var loc2:*=this.damage + this.selfDamage;
            var loc3:*=loc2 != 0 ? this.selfDamage / loc2 : 0;
            this.barSelfDamage.track.scaleX = loc1 + loc3 * (this.bar.track.scaleX - loc1);
            this.tweenIdBar = lesta.cpp.Tween.to(this.bar.track, HEALTH_BAR_TWEEN_DURATION, {"scaleX":this.bar.track.scaleX}, {"scaleX":loc1}, this.onBarTweenEnd);
            return;
        }

        internal function onBarTweenEnd():void
        {
            var loc1:*=this.health / this._maximum;
            this.tweenIdBarDamage = lesta.cpp.Tween.to(this.barDamage.track, DAMAGE_BAR_TWEEN_DURATION, {"scaleX":this.barDamage.track.scaleX}, {"scaleX":loc1}, this.onDamageTweenEnd, 3, 0.5);
            this.tweenIdBarSelfDamage = lesta.cpp.Tween.to(this.barSelfDamage.track, DAMAGE_BAR_TWEEN_DURATION, {"scaleX":this.barSelfDamage.track.scaleX}, {"scaleX":loc1}, this.onSelfDamageTweenEnd, 3, 0.5);
            return;
        }

        internal function updateData():void
        {
            if (this.isDirty && !(this._isDamageShows || this._isSelfDamageShows)) 
            {
                this._isSelfDamageShows = true;
                this._isDamageShows = true;
                this.applyTween();
                this.damage = 0;
                this.selfDamage = 0;
                this.isDirty = false;
            }
            return;
        }

        internal function onDamageTweenEnd():void
        {
            this._isDamageShows = false;
            this.updateData();
            return;
        }

        internal function onSelfDamageTweenEnd():void
        {
            this._isSelfDamageShows = false;
            this.updateData();
            return;
        }

        internal static const HEALTH_BAR_TWEEN_DURATION:Number=0.35;

        internal static const DAMAGE_BAR_TWEEN_DURATION:Number=0.75;

        public var bar:flash.display.MovieClip;

        public var barDamage:flash.display.MovieClip;

        public var barSelfDamage:flash.display.MovieClip;

        public var background:flash.display.MovieClip;

        internal var _maximum:Number;

        internal var _isDamageShows:Boolean=false;

        internal var _isSelfDamageShows:Boolean=false;

        internal var isDirty:Boolean=false;

        internal var health:Number=0;

        internal var damage:Number=0;

        internal var selfDamage:Number=0;

        internal var tweenIdBar:int;

        internal var tweenIdBarDamage:int;

        internal var tweenIdBarSelfDamage:int;
    }
}
