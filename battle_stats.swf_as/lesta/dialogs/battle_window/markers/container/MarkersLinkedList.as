package lesta.dialogs.battle_window.markers.container 
{
    import lesta.dialogs.battle_window.markers.container.views.base.*;
    
    public class MarkersLinkedList extends Object
    {
        public function MarkersLinkedList(arg1:lesta.dialogs.battle_window.markers.container.views.base.SimpleMarker, arg2:lesta.dialogs.battle_window.markers.container.MarkersLinkedList, arg3:lesta.dialogs.battle_window.markers.container.MarkersLinkedList=null)
        {
            super();
            this.value = arg1;
            this.next = arg2;
            this.prev = arg3;
            return;
        }

        public function fini():void
        {
            if (this.next != null) 
            {
                this.next.fini();
            }
            this.value = null;
            this.next = null;
            this.prev = null;
            return;
        }

        public var value:lesta.dialogs.battle_window.markers.container.views.base.SimpleMarker;

        public var next:lesta.dialogs.battle_window.markers.container.MarkersLinkedList;

        public var prev:lesta.dialogs.battle_window.markers.container.MarkersLinkedList;
    }
}
