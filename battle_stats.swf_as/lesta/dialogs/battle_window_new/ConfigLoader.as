package lesta.dialogs.battle_window_new 
{
    import flash.display.*;
    import flash.events.*;
    import flash.net.*;
    import flash.utils.*;
    import lesta.data.*;
    import lesta.dialogs.custom_hud.*;
    import lesta.structs.*;
    import lesta.utils.*;
    
    public class ConfigLoader extends flash.events.EventDispatcher
    {
        public function ConfigLoader()
        {
            this.layoutNames = [];
            this.swfsLoader = new SwfsLoader();
            this.mapControllers = {};
            super();
            this.loadConfigs();
            return;
        }

        public function init():void
        {
            return;
        }

        public function fini():void
        {
            this.layoutCollection = null;
            this.controllersCollection = null;
            this.elementsCollection = null;
            this.animationCollection = null;
            this.optionsCollection = null;
            this.layoutNames = null;
            this.mapControllers = null;
            this.swfsLoader.unload();
            this.swfsLoader = null;
            return;
        }

        public function getControllerByName(arg1:String):lesta.dialogs.battle_window_new.HudElementController
        {
            return this.mapControllers[arg1];
        }

        public function saveLayout(arg1:lesta.dialogs.custom_hud.LayoutManager, arg2:Boolean=false):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=arg1.containers;
            for each (loc1 in loc4) 
            {
                var loc5:*=0;
                var loc6:*=this.layoutCollection;
                for each (loc2 in loc6) 
                {
                    if (!(loc2.id == loc1.name && loc1.layout)) 
                    {
                        continue;
                    }
                    loc2.vglue = loc1.layout.vglue.toXML("<vglue></vglue>");
                    loc2.hglue = loc1.layout.hglue.toXML("<hglue></hglue>");
                    break;
                }
            }
            if (arg2) 
            {
                lesta.data.GameDelegate.call("customHud.saveLayout", [this.layoutCollection.toXMLString()]);
            }
            return;
        }

        public function getControllersCollection(arg1:lesta.dialogs.custom_hud.LayoutManager):Array
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=null;
            var loc8:*=null;
            var loc9:*=null;
            var loc10:*=null;
            var loc1:*=[];
            var loc11:*=0;
            var loc12:*=this.controllersCollection;
            for each (loc2 in loc12) 
            {
                loc3 = loc2.attribute("class");
                loc4 = loc2.attribute("name");
                loc5 = flash.utils.getDefinitionByName(loc3) as Class;
                loc6 = [];
                if (!((loc7 = new loc5()) is lesta.dialogs.battle_window_new.HudElementController)) 
                {
                    trace("Controller must be HudElementController: ", loc3);
                    continue;
                }
                loc8 = loc7 as lesta.dialogs.battle_window_new.HudElementController;
                var loc13:*=0;
                var loc14:*=String(loc2.attribute("clips")).split(",");
                for each (loc9 in loc14) 
                {
                    loc10 = null;
                    if (loc9.charAt(0) != "!") 
                    {
                        loc10 = arg1.getClipByName(loc9);
                    }
                    else 
                    {
                        loc9 = loc9.substr(1);
                        loc10 = arg1.getContainerByName(loc9);
                    }
                    if (loc10 == null) 
                    {
                        continue;
                    }
                    loc6.push(loc10);
                }
                loc8.setClips(loc6);
                if (!(loc4 == null) && !(loc4 == "")) 
                {
                    this.mapControllers[loc4] = loc8;
                }
                loc1.push(loc8);
            }
            return loc1;
        }

        public function getLayoutCollection():Array
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=[];
            var loc4:*=0;
            var loc5:*=this.layoutCollection;
            for each (loc2 in loc5) 
            {
                loc3 = lesta.utils.Reflection.toObject(loc2, new lesta.structs.CustomHudElement()) as lesta.structs.CustomHudElement;
                loc1.push(loc3);
            }
            return loc1;
        }

        public function getAnimationCollection(arg1:lesta.dialogs.custom_hud.LayoutManager):Array
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc1:*=[];
            var loc6:*=0;
            var loc7:*=this.animationCollection;
            for each (loc2 in loc7) 
            {
                (loc3 = lesta.utils.Reflection.toObject(loc2, new Object())).delay = int(loc2.@delay.toString());
                loc4 = [];
                var loc8:*=0;
                var loc9:*=String(loc2.attribute("clips")).split(",");
                for each (loc5 in loc9) 
                {
                    loc4.push(arg1.getClipByName(loc5));
                }
                loc3.clips = loc4;
                loc1.push(loc3);
            }
            return loc1;
        }

        public function reloadConfig():void
        {
            var loc1:*=new flash.net.URLLoader();
            loc1.addEventListener(flash.events.Event.COMPLETE, this.itemsReloaded);
            loc1.addEventListener(flash.events.IOErrorEvent.IO_ERROR, this.errorHandler);
            loc1.load(new flash.net.URLRequest(itemUrl));
            return;
        }

        public function reloadLayout():void
        {
            return;
        }

        internal function loadConfigs():void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=0;
            var loc5:*=[itemUrl, layoutUrl];
            for each (loc1 in loc5) 
            {
                loc2 = new flash.net.URLLoader();
                loc3 = loc1 != itemUrl ? this.layoutLoaded : this.itemsLoadedInit;
                loc2.addEventListener(flash.events.Event.COMPLETE, loc3);
                loc2.addEventListener(flash.events.IOErrorEvent.IO_ERROR, this.errorHandler);
                loc2.load(new flash.net.URLRequest(loc1));
            }
            return;
        }

        internal function errorHandler(arg1:flash.events.IOErrorEvent):void
        {
            trace("error in loading layout XML!");
            return;
        }

        internal function itemsReloaded(arg1:flash.events.Event):void
        {
            var loc1:*=XML(arg1.target.data);
            this.elementsCollection = loc1.elementList.children();
            this.controllersCollection = loc1.controllers.children();
            this.animationCollection = loc1.animation.children();
            this.optionsCollection = loc1.rotateMinimap;
            dispatchEvent(new flash.events.Event("reloaded"));
            return;
        }

        internal function itemsLoadedInit(arg1:flash.events.Event):void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc1:*=XML(arg1.target.data);
            this.elementsCollection = loc1.elementList.children();
            this.controllersCollection = loc1.controllers.children();
            this.animationCollection = loc1.animation.children();
            this.optionsCollection = loc1.rotateMinimap;
            var loc2:*=[];
            var loc5:*=0;
            var loc6:*=loc1;
            for each (loc3 in loc6) 
            {
                if (!(!((loc4 = String(loc3.@url)) == "") && loc2.indexOf(loc4) == -1)) 
                {
                    continue;
                }
                loc2.push(loc4);
            }
            this.swfsLoader.load(loc2, this.swfsLoaded);
            return;
        }

        internal function layoutLoaded(arg1:flash.events.Event):void
        {
            var loc2:*=null;
            var loc1:*=XML(arg1.target.data);
            this.layoutCollection = loc1.element;
            var loc3:*=0;
            var loc4:*=this.layoutCollection;
            for each (loc2 in loc4) 
            {
                this.layoutNames.push(loc2.id.toString());
            }
            this.isLayoutLoaded = true;
            if (this.isItemsLoaded && this.isLayoutLoaded) 
            {
                dispatchEvent(new flash.events.Event(flash.events.Event.COMPLETE));
            }
            return;
        }

        internal function swfsLoaded():void
        {
            this.isItemsLoaded = true;
            if (this.isItemsLoaded && this.isLayoutLoaded) 
            {
                dispatchEvent(new flash.events.Event(flash.events.Event.COMPLETE));
            }
            return;
        }

        public static const itemUrl:String="../battle_elements.xml";

        public static const layoutUrl:String="../battle_layout.xml";

        public var layoutCollection:XMLList;

        public var controllersCollection:XMLList;

        public var elementsCollection:XMLList;

        public var animationCollection:XMLList;

        public var optionsCollection:XMLList;

        public var layoutNames:Array;

        internal var swfsLoader:SwfsLoader;

        internal var isItemsLoaded:Boolean=false;

        internal var isLayoutLoaded:Boolean=false;

        internal var mapControllers:Object;
    }
}

import flash.display.*;
import flash.events.*;
import flash.net.*;
import flash.system.*;
import flash.utils.*;


class SwfsLoader extends Object
{
    public function SwfsLoader()
    {
        this.loadersMap = new flash.utils.Dictionary();
        super();
        return;
    }

    public function load(arg1:Array, arg2:Function):void
    {
        var loc1:*=null;
        var loc2:*=null;
        this.onComplete = arg2;
        this.loadersNum = arg1.length;
        var loc3:*=0;
        var loc4:*=arg1;
        for each (loc1 in loc4) 
        {
            (loc2 = new flash.display.Loader()).contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, this.onLoadComplete, false, 0, true);
            loc2.contentLoaderInfo.addEventListener(flash.events.IOErrorEvent.IO_ERROR, this.onError);
            loc2.load(new flash.net.URLRequest(loc1), new flash.system.LoaderContext(false, flash.system.ApplicationDomain.currentDomain));
        }
        return;
    }

    public function unload():void
    {
        var loc1:*=null;
        var loc2:*=null;
        var loc3:*=0;
        var loc4:*=this.loadersMap;
        for (loc1 in loc4) 
        {
            loc2 = this.loadersMap[loc1];
            if (loc2 is flash.display.Loader) 
            {
                loc2.unload();
            }
            loc2 = null;
            delete this.loadersMap[loc1];
        }
        return;
    }

    internal function onLoadComplete(arg1:flash.events.Event):void
    {
        (arg1.target as flash.display.LoaderInfo).removeEventListener(flash.events.Event.COMPLETE, this.onLoadComplete);
        this.loadersMap[(arg1.target as flash.display.LoaderInfo).url] = (arg1.target as flash.display.LoaderInfo).loader.content;
        var loc1:*;
        var loc2:*=((loc1 = this).loadedNum + 1);
        loc1.loadedNum = loc2;
        if (this.loadedNum == this.loadersNum) 
        {
            this.onComplete();
        }
        return;
    }

    internal function onError(arg1:flash.events.IOErrorEvent):void
    {
        trace(["ERROR_IN_SWF"], (arg1.target as flash.display.LoaderInfo).url);
        return;
    }

    public var loadersMap:flash.utils.Dictionary;

    public var onComplete:Function=null;

    internal var loadersNum:int;

    internal var loadedNum:int=0;
}