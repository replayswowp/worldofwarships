package lesta.components 
{
    import lesta.constants.*;
    import lesta.datahub.*;
    
    public class _Global extends lesta.datahub.Component
    {
        public function _Global()
        {
            this.chat = new lesta.datahub.EntityRef();
            this.player = new lesta.datahub.EntityRef();
            this.userPrefs = {};
            super();
            return;
        }

        public override function get classID():uint
        {
            return lesta.constants.ComponentClass._Global;
        }

        public override function get className():String
        {
            return "_Global";
        }

        public var chat:lesta.datahub.EntityRef;

        public var playerId:int=0;

        public var player:lesta.datahub.EntityRef;

        public var userPrefs:Object;
    }
}
