package lesta.controls 
{
    import flash.display.*;
    
    public class AnimationEffect extends flash.display.MovieClip
    {
        public function AnimationEffect()
        {
            super();
            return;
        }

        public function show():void
        {
            visible = true;
            gotoAndPlay(1);
            return;
        }

        public function hide():void
        {
            visible = false;
            stop();
            return;
        }
    }
}
