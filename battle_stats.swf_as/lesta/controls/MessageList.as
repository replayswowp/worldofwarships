package lesta.controls 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.constants.*;
    import lesta.cpp.*;
    import lesta.dialogs.battle_window.constants.*;
    import lesta.interfaces.*;
    import scaleform.clik.constants.*;
    import scaleform.clik.controls.*;
    import scaleform.clik.core.*;
    import scaleform.clik.data.*;
    import scaleform.gfx.*;
    
    public class MessageList extends scaleform.clik.core.UIComponent
    {
        public function MessageList()
        {
            this._dataProvider = new scaleform.clik.data.DataProvider();
            this._messageListItemRendererController = new lesta.controls.MessageListItemRendererContoller();
            this.listElements = new Vector.<MessageListElement>();
            this.substituteArray = [];
            super();
            if (this.size != null) 
            {
                this.size.visible = false;
            }
            else 
            {
                this.size = this;
            }
            this.setSize(this.size.width, this.size.height);
            if (this.maskerClip) 
            {
                this.mask = this.maskerClip;
            }
            scaleform.gfx.InteractiveObjectEx.setHitTestDisable(this.itemContainer, true);
            this.wheelEventsDispatcher = this.backClip;
            addEventListener(flash.events.Event.ADDED_TO_STAGE, this.init);
            return;
        }

        public function set messageBufferOverflowDeathTime(arg1:Number):void
        {
            this._messageBufferOverflowDeathTime = arg1 as Number;
            return;
        }

        public function get actualScrollBarPosition():Number
        {
            return this._scrollBarTypeLocation != "right" ? 0 : _width - this.scrollBarObj.width;
        }

        public function set antiSpam(arg1:Boolean):void
        {
            this._antiSpamValue = arg1;
            return;
        }

        public function set tweenTime(arg1:Number):void
        {
            this._timeTween = arg1;
            return;
        }

        public function set alphaModifier(arg1:Number):void
        {
            this._alphaModifaer = arg1;
            return;
        }

        public function set hasSubstitution(arg1:Boolean):void
        {
            this._hasSubstitution = arg1;
            return;
        }

        public function get availableWidth():Number
        {
            var loc1:*=0;
            if (!this._isScrollBarAboveItemRenderers) 
            {
                loc1 = this.scrollBarObj ? this.scrollBarObj.width : 0;
            }
            return width - loc1;
        }

        protected override function draw():void
        {
            super.draw();
            if (isInvalid(MessageListInvalidationType.ITEMRENDERER)) 
            {
                this.reDraw();
            }
            if (isInvalid(scaleform.clik.constants.InvalidationType.SCROLL_BAR) || isInvalid(MessageListInvalidationType.SCROLLBAR_POSITION) || isInvalid(MessageListInvalidationType.SCROLLBAR_STEP)) 
            {
                this.initializNewScrollBar();
            }
            if (isInvalid(scaleform.clik.constants.InvalidationType.SIZE)) 
            {
                if (this.maskerClip) 
                {
                    this.maskerClip.width = _width;
                    this.maskerClip.height = _height;
                }
                if (this.backClip) 
                {
                    this.backClip.width = _width;
                    this.backClip.height = _height;
                }
                this.reDraw();
                this.updateActualListHeight();
                this.itemContainer.y = Math.max(0, this._startContainerY - this._actualListHieght);
                if (this.scrollBarObj != null) 
                {
                    this.scrollBarObj.x = this.actualScrollBarPosition;
                    this.scrollBarObj.height = _height;
                    this.updateScrollBar();
                    this.enabledScrollBarThumb();
                }
            }
            if (isInvalid(scaleform.clik.constants.InvalidationType.DATA)) 
            {
                this.updateList();
            }
            if (isInvalid(MessageListInvalidationType.TIME)) 
            {
                this.initializNewTimer();
            }
            if (isInvalid(MessageListInvalidationType.FLOW) || isInvalid(scaleform.clik.constants.InvalidationType.SIZE)) 
            {
                this.reDraw();
            }
            if (isInvalid(MessageListInvalidationType.SCROLLBAR_ABOVE_ITEMRENDERER)) 
            {
                this.updateItemContainerPositionX();
            }
            if (isInvalid(MessageListInvalidationType.BLINK_ARROW_BUTTON_ELEMENT)) 
            {
                this.addBlink();
            }
            return;
        }

        public override function setSize(arg1:Number, arg2:Number):void
        {
            var loc3:*=null;
            var loc4:*=null;
            super.setSize(arg1, arg2);
            this._startContainerY = this._textDisplayFlow != TOP_DOWN ? this._height : 0;
            var loc1:*=this.listElements.length;
            var loc2:*=0;
            while (loc2 < loc1) 
            {
                loc3 = this.listElements[loc2];
                (loc4 = this._messageListItemRendererController.getTestInstance(this._dataProvider[loc2].id)).init(this._dataProvider[loc2], this.availableWidth);
                loc4.validateNow();
                loc3.height = loc4.height;
                ++loc2;
            }
            this.updateList();
            return;
        }

        internal function initializNewScrollBar():void
        {
            if (this.scrollBarObj) 
            {
                this.removeScrollBar();
            }
            if (this._scrollBarClass != null) 
            {
                this.createScrollBar();
                invalidate(MessageListInvalidationType.FLOW);
            }
            return;
        }

        public function set scrollBar(arg1:Class):void
        {
            this._scrollBarClass = arg1;
            invalidate(scaleform.clik.constants.InvalidationType.SCROLL_BAR);
            return;
        }

        internal function removeScrollBar():void
        {
            this.removeChild(this.scrollBarObj);
            this.scrollBarObj.removeEventListener(flash.events.Event.SCROLL, this.handleScroll);
            if (this.wheelEventsDispatcher) 
            {
                this.wheelEventsDispatcher.removeEventListener(flash.events.MouseEvent.MOUSE_WHEEL, this.handleMouseWheel);
            }
            else 
            {
                parent.removeEventListener(flash.events.MouseEvent.MOUSE_WHEEL, this.handleMouseWheel);
            }
            return;
        }

        internal function createScrollBar():void
        {
            this.scrollBarObj = new this._scrollBarClass() as scaleform.clik.controls.ScrollBar;
            this.scrollBarObj.addEventListener(flash.events.Event.SCROLL, this.handleScroll);
            this.scrollBarObj.focusTarget = this;
            this.scrollBarObj.tabEnabled = false;
            this.scrollBarObj.x = this.actualScrollBarPosition;
            this.scrollBarObj.height = _height;
            this.addChild(this.scrollBarObj);
            if (this.wheelEventsDispatcher) 
            {
                this.wheelEventsDispatcher.addEventListener(flash.events.MouseEvent.MOUSE_WHEEL, this.handleMouseWheel);
            }
            else 
            {
                parent.addEventListener(flash.events.MouseEvent.MOUSE_WHEEL, this.handleMouseWheel);
            }
            this.addBlink();
            if (this._blinkArrowButtonElement) 
            {
                this._blinkArrowButtonElement.visible = false;
            }
            this.updateItemContainerPositionX();
            this.updateScrollBar();
            return;
        }

        internal function handleScroll(arg1:flash.events.Event):void
        {
            this.enabledScrollBarThumb();
            invalidate(MessageListInvalidationType.FLOW);
            return;
        }

        protected function handleMouseWheel(arg1:flash.events.MouseEvent):void
        {
            this.scrollBarObj.position = this.scrollBarObj.position - (arg1.delta > 0 ? 1 : -1) * this._scrollBarStep;
            return;
        }

        internal function updateItemContainerPositionX():void
        {
            var loc1:*=0;
            if (!this._isScrollBarAboveItemRenderers && !(this.scrollBarObj == null)) 
            {
                loc1 = this._scrollBarTypeLocation != "right" ? this.scrollBarObj.width : 0;
            }
            this.itemContainer.x = loc1;
            return;
        }

        internal function enabledScrollBarThumb():void
        {
            if (Math.round(this.scrollBarObj.position) >= Math.round(this._actualListHieght - this._height)) 
            {
                this.autoScrollToLastMessage = true;
                if (this._blinkArrowButtonElement) 
                {
                    this._blinkArrowButtonElement.visible = false;
                }
            }
            else 
            {
                this.autoScrollToLastMessage = false;
            }
            return;
        }

        internal function updateScrollBar():void
        {
            var loc1:*=NaN;
            if (this.scrollBarObj != null) 
            {
                loc1 = this._actualListHieght - this._height;
                this.scrollBarObj.setScrollProperties(this._height, 0, loc1, this._scrollBarStep);
                if (this.autoScrollToLastMessage || this.scrollBarObj.position > loc1) 
                {
                    this.scrollBarObj.position = loc1;
                }
                if (this.scrollBarObj.position < 0) 
                {
                    this.scrollBarObj.position = 0;
                }
                if (loc1 < 0) 
                {
                    this.autoScrollToLastMessage = true;
                    this.stopBlink();
                }
            }
            return;
        }

        internal function updateBlinkTimer():void
        {
            if (!(this._timeBlinking == 0) && this._blinkArrowButtonElement && !this.autoScrollToLastMessage) 
            {
                this._blinkArrowButtonElement.visible = true;
                this.startBlinkTimer();
            }
            return;
        }

        internal function updateList():void
        {
            var loc2:*=0;
            var loc3:*=null;
            var loc1:*=this.listElements.length;
            while (loc1 < this._dataProvider.length) 
            {
                loc2 = this._dataProvider[loc1].type;
                this._hasNewMessage = true;
                loc3 = this._messageListItemRendererController.getTestInstance(this._dataProvider[loc1].id);
                loc3.init(this._dataProvider[loc1], this.availableWidth);
                loc3.validateNow();
                this.listElements.push(new MessageListElement(loc3.height, this._listItemRendererLifeTime, this._listItemRendererDethTime, this._messageBufferOverflowDeathTime, loc3.alpha, this.freeElement, loc2, this._timeTween));
                ++loc1;
            }
            this.removeBufferOverflowElements();
            this.updateActualListHeight();
            this.updateScrollBar();
            this.updateBlinkTimer();
            invalidate(MessageListInvalidationType.FLOW);
            return;
        }

        internal function updateActualListHeight():void
        {
            var loc2:*=null;
            var loc1:*=0;
            var loc3:*=0;
            while (loc3 < this.listElements.length) 
            {
                loc2 = this.listElements[loc3];
                loc1 = loc1 + (loc2.height + this.lineSpacing);
                ++loc3;
            }
            this._actualListHieght = loc1 - this.lineSpacing;
            return;
        }

        internal function reDraw():void
        {
            var loc5:*=0;
            var loc6:*=0;
            var loc7:*=0;
            var loc8:*=null;
            var loc9:*=null;
            var loc10:*=null;
            var loc11:*=null;
            var loc12:*=0;
            this.tweenIsEnd();
            var loc1:*=0;
            var loc2:*=null;
            var loc3:*=this.getElementByPosition();
            var loc4:*=0;
            this._messageListItemRendererController.start();
            if (loc3 != null) 
            {
                loc1 = loc3["id"];
                loc5 = loc3["offset"];
                loc6 = Math.max(0, this._startContainerY - this._actualListHieght);
                this.itemContainer.y = loc6;
                loc7 = 0;
                loc8 = this.dataProvider[loc1];
                while (loc1 < this.listElements.length && loc7 < this.listElements.length && loc5 < (this._height - 1) && !(loc8 == null)) 
                {
                    loc2 = this._messageListItemRendererController.setData(loc8);
                    loc9 = this.listElements[loc1];
                    if (loc2 && loc9) 
                    {
                        if (this._hasSubstitution && loc2["textField"]) 
                        {
                            scaleform.gfx.TextFieldEx.setImageSubstitutions(loc2["textField"], this.substituteArray);
                        }
                        loc2.init(loc8, this.width);
                        if (!(this._alphaModifaer == 0) && !(this.listElements.length == 0) && this._hasNewMessage) 
                        {
                            loc4 = 1 - ((this.listElements.length - 1) - loc7) * this._alphaModifaer;
                            (loc10 = this.listElements[loc1]).messageListAlpha = loc4 != 0 ? loc4 : this._alphaModifaer;
                        }
                        loc9.visualObject = loc2;
                        loc2.width = this.availableWidth;
                        loc2.validateNow();
                        loc2.y = loc5;
                        loc2.visible = true;
                        loc9.height = loc2.height;
                        loc5 = loc5 + (loc2.height + this.lineSpacing);
                        this.itemContainer.addChild(loc2 as flash.display.DisplayObject);
                    }
                    ++loc1;
                    ++loc7;
                    loc8 = this.dataProvider[loc1];
                }
                if (this._textDisplayFlow == BOTTOM_UP && this._isTweenItemRenderer && this.autoScrollToLastMessage && this._hasNewMessage && this.listElements.length > 0) 
                {
                    loc11 = this.listElements[(this.listElements.length - 1)];
                    loc12 = this.itemContainer.y;
                    this.itemContainer.y = this.itemContainer.y + (loc11.height + this.lineSpacing);
                    this.playItemRendererTween(loc12);
                }
            }
            this._hasNewMessage = false;
            this.cleanVisibleElementList();
            return;
        }

        internal function playItemRendererTween(arg1:int):void
        {
            lesta.cpp.Tween.kill(this._tweenItemRendererId);
            this._tweenItemRendererId = lesta.cpp.Tween.to(this.itemContainer, this._timeTween, {"y":this.itemContainer.y}, {"y":arg1}, this.tweenIsEnd);
            return;
        }

        internal function tweenIsEnd():void
        {
            lesta.cpp.Tween.kill(this._tweenItemRendererId);
            this._tweenItemRendererId = -1;
            return;
        }

        internal function getElementByPosition():Object
        {
            var loc1:*=0;
            var loc2:*=null;
            var loc3:*=this.scrollBarObj == null ? this._actualListHieght - this.height : this.scrollBarObj.position;
            if (loc3 < 0) 
            {
                loc3 = 0;
            }
            var loc4:*=0;
            while (loc4 < this.listElements.length) 
            {
                loc1 = loc1 + (this.listElements[loc4].height + this.lineSpacing);
                if (loc1 >= loc3) 
                {
                    loc2 = {"id":loc4, "offset":loc1 - loc3 - this.listElements[loc4].height};
                    break;
                }
                ++loc4;
            }
            return loc2;
        }

        internal function cleanVisibleElementList():void
        {
            var loc1:*=this._messageListItemRendererController.end();
            var loc2:*=0;
            while (loc2 < loc1.length) 
            {
                loc1[loc2].fini();
                loc1[loc2].visible = false;
                ++loc2;
            }
            return;
        }

        internal function addBlink():void
        {
            if (this.scrollBarObj && this._blinkArrowButtonElement) 
            {
                this._blinkArrowButtonElement.mouseChildren = false;
                this._blinkArrowButtonElement.mouseEnabled = false;
                this._blinkArrowButtonElement.x = this.scrollBarObj.x;
                this._blinkArrowButtonElement.y = this.scrollBarObj.downArrow.y - this._blinkArrowButtonElement.height + 2;
                this._blinkArrowButtonElement.visible = false;
                this.addChild(this._blinkArrowButtonElement);
                this.initializNewTimer();
            }
            return;
        }

        internal function initializNewTimer():void
        {
            if (this._blinkArrowButtonElement && !this.timerBlink) 
            {
                this.timerBlink = null;
                this.timerBlink = new flash.utils.Timer(this._timeBlinking * 1000, 1);
                this.timerBlink.addEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.blinkTimerIsOver, false, 0, true);
            }
            return;
        }

        internal function startBlinkTimer():void
        {
            if (this.timerBlink) 
            {
                this.timerBlink.reset();
                this.timerBlink.start();
            }
            return;
        }

        internal function blinkTimerIsOver(arg1:flash.events.TimerEvent):void
        {
            this.stopBlink();
            return;
        }

        internal function stopBlink():void
        {
            if (this._blinkArrowButtonElement) 
            {
                this._blinkArrowButtonElement.visible = false;
                this.timerBlink.stop();
            }
            return;
        }

        internal function freeElement():void
        {
            this.listElements.shift();
            this._dataProvider.shift();
            invalidateData();
            return;
        }

        internal function init_substitute():void
        {
            var loc1:*=0;
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=null;
            var loc5:*=0;
            var loc6:*=lesta.constants.ShipModules.LIST_FRAMES_ORDER;
            for each (loc1 in loc6) 
            {
                var loc7:*=0;
                var loc8:*=lesta.dialogs.battle_window.constants.HealthStates.GUI_STATES;
                for each (loc3 in loc8) 
                {
                    loc4 = "icon_module_" + loc1 + "_" + loc3;
                    this.addSubstituteElement(loc4);
                }
            }
            loc2 = "icon_fire_small";
            this.addSubstituteElement(loc2);
            return;
        }

        internal function addSubstituteElement(arg1:String):void
        {
            var loc2:*=null;
            var loc1:*=flash.utils.getDefinitionByName(arg1) as Class;
            if (loc1) 
            {
                loc2 = new loc1();
                this.substituteArray.push({"subString":arg1, "image":loc2, "baseLineY":32, "width":26, "height":26});
            }
            return;
        }

        internal function displayingAlready(arg1:Object):Boolean
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=(this.listElements.length - 1);
            while (loc1 >= 0) 
            {
                loc2 = this._dataProvider[loc1];
                if (arg1.type == loc2.type) 
                {
                    if ((loc3 = this.listElements[loc1].visualObject) != null) 
                    {
                        loc3.play();
                    }
                    return true;
                }
                --loc1;
            }
            return false;
        }

        internal function init():void
        {
            removeEventListener(flash.events.Event.ADDED_TO_STAGE, this.init);
            if (this._hasSubstitution) 
            {
                this.init_substitute();
            }
            return;
        }

        public function set iconContainerHitTestDisable(arg1:Boolean):void
        {
            scaleform.gfx.InteractiveObjectEx.setHitTestDisable(this.itemContainer, arg1);
            if (arg1) 
            {
                if (this.backClip != null) 
                {
                    this.wheelEventsDispatcher = this.backClip;
                }
            }
            else 
            {
                this.wheelEventsDispatcher = this.itemContainer;
            }
            return;
        }

        public function set itemRendererName(arg1:Array):void
        {
            if (arg1.length == 0) 
            {
                trace("Error: must specify at least one element");
                return;
            }
            this._messageListItemRendererController.initItemRendererCollection(arg1);
            invalidate(MessageListInvalidationType.ITEMRENDERER);
            return;
        }

        public function get dataProvider():scaleform.clik.data.DataProvider
        {
            return this._dataProvider;
        }

        public function set dataProvider(arg1:scaleform.clik.data.DataProvider):void
        {
            this._dataProvider = arg1;
            invalidateData();
            return;
        }

        public function addMessage(arg1:Object):void
        {
            if (!(arg1.type == null) && this._antiSpamValue && this.displayingAlready(arg1)) 
            {
                return;
            }
            this._dataProvider.push(arg1);
            invalidateData();
            return;
        }

        public function set scrollBarStep(arg1:Number):void
        {
            this._scrollBarStep = arg1;
            invalidate(MessageListInvalidationType.SCROLLBAR_STEP);
            return;
        }

        public function set scrollBarAboveItemRenderer(arg1:Boolean):void
        {
            this._isScrollBarAboveItemRenderers = arg1;
            invalidate(MessageListInvalidationType.SCROLLBAR_ABOVE_ITEMRENDERER);
            return;
        }

        public function set tweenListItemRenderer(arg1:Boolean):void
        {
            this._isTweenItemRenderer = arg1;
            return;
        }

        public function get scrollBarName():String
        {
            return this._scrollBarClassName;
        }

        public function set scrollBarName(arg1:String):void
        {
            if (arg1 == "") 
            {
                return;
            }
            var loc1:*=flash.utils.getDefinitionByName(arg1) as Class;
            if (loc1 == null) 
            {
                trace("Error: " + this + ", The class " + arg1 + " cannot be found in your library. Please ensure it is there.");
            }
            else 
            {
                this.scrollBar = loc1;
            }
            return;
        }

        internal function removeBufferOverflowElements():void
        {
            var loc1:*=this.listElements.length - this._messageBufferLength;
            var loc2:*=0;
            while (loc2 < loc1) 
            {
                this.listElements[loc2].bufferOverflow();
                ++loc2;
            }
            return;
        }

        public function get blinkArrowButtonElementName():String
        {
            return this._blinkArrowButtonElementClassName;
        }

        public function set blinkArrowButtonElementName(arg1:String):void
        {
            if (arg1 == "") 
            {
                return;
            }
            var loc1:*=flash.utils.getDefinitionByName(arg1) as Class;
            if (loc1 == null) 
            {
                trace("Error: " + this + ", The object " + arg1 + " cannot be found in your library. Please ensure it is there.");
            }
            else 
            {
                this.blinkArrowButtonElement = loc1;
            }
            return;
        }

        public function set blinkArrowButtonElement(arg1:Class):void
        {
            this._blinkArrowButtonElement = new arg1() as flash.display.MovieClip;
            invalidate(MessageListInvalidationType.BLINK_ARROW_BUTTON_ELEMENT);
            return;
        }

        public function set messageBufferLength(arg1:Number):void
        {
            this._messageBufferLength = arg1;
            return;
        }

        public function set scrollPosition(arg1:String):void
        {
            this._scrollBarTypeLocation = arg1;
            invalidate(MessageListInvalidationType.SCROLLBAR_POSITION);
            return;
        }

        public function set textDisplayFlow(arg1:String):void
        {
            this._textDisplayFlow = arg1;
            this._startContainerY = this._textDisplayFlow != TOP_DOWN ? this._height : 0;
            invalidate(MessageListInvalidationType.FLOW);
            return;
        }

        public function set blinkTimer(arg1:Number):void
        {
            this._timeBlinking = arg1;
            invalidate(MessageListInvalidationType.TIME);
            return;
        }

        public function set listItemRendererLifeTime(arg1:Number):void
        {
            this._listItemRendererLifeTime = arg1 as Number;
            return;
        }

        public function set listItemRendererDethTime(arg1:Number):void
        {
            this._listItemRendererDethTime = arg1 as Number;
            return;
        }

        internal static const BOTTOM_UP:String="bottom-up";

        internal static const TOP_DOWN:String="top-down";

        public var lineSpacing:int=0;

        protected var _isScrollBarAboveItemRenderers:Boolean=false;

        protected var _antiSpamValue:Boolean=false;

        protected var _isTweenItemRenderer:Boolean=false;

        protected var _scrollBarClass:Class=null;

        protected var _blinkArrowButtonElement:flash.display.MovieClip=null;

        protected var _dataProvider:scaleform.clik.data.DataProvider;

        protected var _messageBufferLength:int=50;

        protected var _timeBlinking:Number=0;

        protected var _scrollBarClassName:String="";

        protected var _blinkArrowButtonElementClassName:String="";

        protected var _scrollBarTypeLocation:String="left";

        protected var _textDisplayFlow:String="top-down";

        protected var _startContainerY:int;

        protected var _listItemRendererLifeTime:Number=0;

        protected var _listItemRendererDethTime:Number=0;

        public var maskerClip:flash.display.MovieClip;

        protected var _tweenItemRendererId:int=-1;

        protected var _timeTween:Number=0;

        protected var _hasNewMessage:Boolean=false;

        protected var _alphaModifaer:Number=0;

        protected var _hasSubstitution:Boolean=false;

        public var size:flash.display.MovieClip;

        protected var _messageListItemRendererController:lesta.controls.MessageListItemRendererContoller;

        protected var autoScrollToLastMessage:Boolean=true;

        protected var timerBlink:flash.utils.Timer;

        protected var listElements:__AS3__.vec.Vector.<MessageListElement>;

        public var itemContainer:flash.display.MovieClip;

        internal var scrollBarObj:scaleform.clik.controls.ScrollBar;

        internal var substituteArray:Array;

        internal var _actualListHieght:int;

        internal var wheelEventsDispatcher:flash.display.DisplayObject=null;

        protected var _scrollBarStep:int=10;

        public var backClip:flash.display.MovieClip;

        protected var _messageBufferOverflowDeathTime:Number=0;
    }
}

import flash.events.*;
import flash.utils.*;
import lesta.cpp.*;
import lesta.interfaces.*;


class MessageListInvalidationType extends Object
{
    public function MessageListInvalidationType()
    {
        super();
        return;
    }

    public static const SCROLLBAR_POSITION:String="scrollBarPosition";

    public static const TIME:String="time";

    public static const FLOW:String="newFlow";

    public static const ITEMRENDERER:String="newItemRenderer";

    public static const SCROLLBAR_ABOVE_ITEMRENDERER:String="changeScrollBarAboveItemRenderer";

    public static const SCROLLBAR_STEP:String="changeScrollBarStep";

    public static const BLINK_ARROW_BUTTON_ELEMENT:String="changeScrollBarStep";
}

class MessageListElement extends Object
{
    public function MessageListElement(arg1:Number, arg2:Number, arg3:Number, arg4:Number, arg5:Number, arg6:Function, arg7:uint=0, arg8:Number=0)
    {
        super();
        this.height = arg1;
        this.lifeTime = arg2;
        this.deathTime = arg3;
        this.timeBufferOverflow = arg4;
        this.callback = arg6;
        this.messageType = arg7;
        if (this.lifeTime != 0) 
        {
            this.timerLife = new flash.utils.Timer(arg2);
            this.timerLife.addEventListener(flash.events.TimerEvent.TIMER, this.startDeath);
            this.timerLife.start();
        }
        this.tweenId = lesta.cpp.Tween.to(this, arg8, {"messageListAlpha":0}, {"messageListAlpha":arg5}, null, 0, 0, this.updateAlphaValue);
        return;
    }

    public function fini():void
    {
        lesta.cpp.Tween.kill(this.tweenId);
        this._visualObject = null;
        this.timerLife.removeEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.startDeath);
        this.timerLife.stop();
        this.timerLife = null;
        this.height = 0;
        this.callback();
        this.callback = null;
        return;
    }

    public function startDeath(arg1:flash.events.TimerEvent):void
    {
        this.startHidingAnimation(this.deathTime);
        return;
    }

    public function bufferOverflow():void
    {
        this.startHidingAnimation(this.timeBufferOverflow);
        return;
    }

    protected function startHidingAnimation(arg1:Number):void
    {
        if (!this._hiding) 
        {
            this._hiding = true;
            if (this.timerLife) 
            {
                this.timerLife.removeEventListener(flash.events.TimerEvent.TIMER_COMPLETE, this.startDeath);
                this.timerLife.stop();
            }
            lesta.cpp.Tween.kill(this.tweenId);
            this.tweenId = lesta.cpp.Tween.to(this, arg1 * this.messageListAlpha, {"messageListAlpha":this.messageListAlpha}, {"messageListAlpha":0}, this.fini, 0, 0, this.updateAlphaValue);
        }
        return;
    }

    public function set visualObject(arg1:lesta.interfaces.IMessageListItemRenderer):void
    {
        this._visualObject = arg1;
        this.updateAlphaValue();
        return;
    }

    public function get visualObject():lesta.interfaces.IMessageListItemRenderer
    {
        return this._visualObject;
    }

    internal function updateAlphaValue():void
    {
        if (this._visualObject != null) 
        {
            this._visualObject.alpha = this.messageListAlpha;
        }
        return;
    }

    public var height:int=0;

    public var lifeTime:Number=0;

    public var deathTime:Number=0;

    public var messageListAlpha:Number=0;

    public var tweenId:int=-1;

    public var timeBufferOverflow:Number=0;

    public var messageType:uint=0;

    internal var _visualObject:lesta.interfaces.IMessageListItemRenderer=null;

    internal var timerLife:flash.utils.Timer=null;

    internal var callback:Function=null;

    internal var _hiding:Boolean;
}