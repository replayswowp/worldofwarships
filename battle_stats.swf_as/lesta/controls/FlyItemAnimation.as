package lesta.controls 
{
    public class FlyItemAnimation extends lesta.controls.AnimatedStateClip
    {
        public function FlyItemAnimation()
        {
            super();
            setState(STATE_INVISIBLE, false, true, null, true);
            return;
        }

        public function show():void
        {
            setState(STATE_SHOW);
            this.isShown = true;
            return;
        }

        public function hide():void
        {
            setState(STATE_INVISIBLE, false, true, null, true);
            this.isShown = false;
            return;
        }

        public static const STATE_INVISIBLE:String="inv";

        public static const STATE_SHOW:String="show";

        public var isShown:Boolean=false;
    }
}
