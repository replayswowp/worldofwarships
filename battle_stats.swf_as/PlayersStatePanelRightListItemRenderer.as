﻿package 
{
    import lesta.dialogs.battle_window._player_stats_item.*;
	import flash.display.*;
	import lesta.constants.*;
	import flash.text.*;
	import flash.events.*;
	import flash.net.*;
	import lesta.structs.*;
	import lesta.utils.Debug;
	import lesta.cpp.Translator;
	
	import lesta.controls.Image;
	import flash.utils.Dictionary;
	import monstrofil.global.EarsConfig;
	import monstrofil.components.HealthBarLine;
	
    
    public dynamic class PlayersStatePanelRightListItemRenderer extends lesta.dialogs.battle_window._player_stats_item.PlayersStateItemRenderer
    {
		public var frames_normalised: Vector.<String> = new Vector.<String>();		
		public var health: HealthBarLine = new HealthBarLine( );
		public var right:Boolean = true;
		public var spottedIcon:lesta.controls.Image;
		public var wasVisible:Boolean = false;
		public var configReloaded = false;
		
		
		public var textFields:Dictionary = new Dictionary();
		
		public function doMacros(player: Player, pattern: String)
		{
			return EarsConfig.doMacros(player, pattern);
		}
		
		
        public function PlayersStatePanelRightListItemRenderer()
        {
            super();			
			health.right = true;
			health.alpha_ = EarsConfig.health_alpha;
			
			this.spottedIcon = new lesta.controls.Image();
            this.spottedIcon.init();
			this.spottedIcon.x = EarsConfig.lamp_x;
			this.spottedIcon.y = EarsConfig.lamp_y;
			this.addChild(this.spottedIcon);
			
			this.addChild(health);
			this.setChildIndex(health, EarsConfig.health_index);
            return;
        }
			
		
		
		
		public function make_normal(){
			var default_format:TextFormat = this.item.txt_user_name.getTextFormat();
			default_format.bold = true;
			
			health.x = EarsConfig.right_health_x;
			health.y = EarsConfig.right_health_y;
			
			this.shipIcon.alpha = this.item.txt_user_name.alpha;
			
			this.item.txt_user_name.visible = EarsConfig.right_txt_user_name_visible;
			this.item.txt_user_name.width = EarsConfig.right_txt_user_name_width;
			this.item.txt_user_name.x = EarsConfig.right_txt_user_name_x;
			
			this.item.txt_frags.x = EarsConfig.right_txt_frags_x;
			this.item.txt_frags.width = EarsConfig.right_txt_frags_width;
			this.item.txt_frags.visible = EarsConfig.right_txt_frags_visible;
			
			this.item.priorityMarkers.x = EarsConfig.right_priorityMarkers_x;
			
			this.item.mvc_ship_position.x = EarsConfig.right_mvc_ship_position_x;
			if(EarsConfig.SCALE_RIGHT > 0)
				this.item.mvc_ship_position.x = EarsConfig.right_mvc_ship_position_x - EarsConfig.SCALE_RIGHT * 122;
				
			this.item.mvc_ship_position.y = EarsConfig.right_mvc_ship_position_y;
		}
		
		override public function setMutableData(arg1:Player):void{
			super.setMutableData(arg1);
			if (configReloaded){
				for each(var i in EarsConfig.xml.right_ear.textfields.textfield){
					EarsConfig.updateField(this, i, arg1);
				}
			}
			
			
            if (frames_normalised.indexOf(this.item.currentFrameLabel) == -1 || EarsConfig.needReload && !configReloaded) {
				frames_normalised.push(this.item.currentFrameLabel);
				this.make_normal();
	
				if(this.item.currentFrameLabel.indexOf("dead") != -1){
					this.shipIcon.src = EarsConfig.doMacros(arg1, EarsConfig.deadIcon);
				}
				else if (this.item.currentFrameLabel.indexOf("player") != -1){
					this.shipIcon.src = EarsConfig.doMacros(arg1, EarsConfig.ownIcon);
				}
				else {
					this.shipIcon.src = EarsConfig.doMacros(arg1, EarsConfig.aliveIcon);
				}
			}
			
			
			
			if(EarsConfig.lamp_active){
				if(arg1.isVisible){
					this.spottedIcon.src = EarsConfig.visibleIcon;
					this.wasVisible = true;
				}
				else if(this.wasVisible == true){
					this.spottedIcon.src = EarsConfig.unvisibleIcon;
				}
				this.spottedIcon.visible = arg1.isAlive;
			}
			
			
			if(EarsConfig.needReload && !configReloaded){
				this.make_normal();
				this.shipIcon.scaleX = EarsConfig.SCALE_RIGHT;
				this.shipIcon.scaleY = EarsConfig.SCALE_RIGHT > 0? EarsConfig.SCALE_RIGHT: -EarsConfig.SCALE_RIGHT;
				
				this.spottedIcon.x = EarsConfig.lamp_x;
				this.spottedIcon.y = EarsConfig.lamp_y;
				
				health.alpha_ = EarsConfig.health_alpha;
				
				for each(var i in EarsConfig.xml.right_ear.textfields.textfield){
					EarsConfig.createField(this, i, arg1);
				}
				
				this.setChildIndex(health, EarsConfig.health_index);
				configReloaded = true;
			}
			
			if(!(arg1.health == 0 && arg1.isAlive)){
				health.draw(arg1.health, arg1.maxHealth, EarsConfig.right_health_width, EarsConfig.right_health_height);
			}
		}
		
		override public function setConstantData(arg1:Player):void{
			super.setConstantData(arg1);
			this.shipIcon.scaleX = EarsConfig.SCALE_RIGHT;
			this.shipIcon.scaleY = EarsConfig.SCALE_RIGHT > 0? EarsConfig.SCALE_RIGHT: -EarsConfig.SCALE_RIGHT;
		}
    }
}
