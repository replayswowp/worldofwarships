package 
{
    import lesta.dialogs.battle_window._battle_progress.*;
    
    public dynamic class AllyScoreCounterClip extends lesta.dialogs.battle_window._battle_progress.ScoreCounter
    {
        public function AllyScoreCounterClip()
        {
            super();
            addFrameScript(20, this.frame21, 25, this.frame26, 29, this.frame30, 31, this.frame32, 42, this.frame43, 54, this.frame55);
            return;
        }

        internal function frame21():*
        {
            onRaise(1);
            return;
        }

        internal function frame26():*
        {
            onRaise(2);
            return;
        }

        internal function frame30():*
        {
            onRaise(3);
            return;
        }

        internal function frame32():*
        {
            onEndAnim();
            return;
        }

        internal function frame43():*
        {
            onEndAnim();
            return;
        }

        internal function frame55():*
        {
            onEndAnim();
            return;
        }
    }
}
