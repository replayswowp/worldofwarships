package scaleform.clik.interfaces 
{
    public interface IItemRendererFactory
    {
        function create():scaleform.clik.interfaces.IListItemRenderer;

        function destroy(arg1:scaleform.clik.interfaces.IListItemRenderer):void;
    }
}
