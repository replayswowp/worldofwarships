package scaleform.clik.interfaces 
{
    public interface IScrollBar extends scaleform.clik.interfaces.IUIComponent
    {
        function get position():Number;

        function set position(arg1:Number):void;
    }
}
