package scaleform.clik.constants 
{
    public class WrappingMode extends Object
    {
        public function WrappingMode()
        {
            super();
            return;
        }

        public static const NORMAL:String="normal";

        public static const STICK:String="stick";

        public static const WRAP:String="wrap";
    }
}
