package scaleform.clik.constants 
{
    public class NavigationCode extends Object
    {
        public function NavigationCode()
        {
            super();
            return;
        }

        
        {
            UP = "up";
            DOWN = "down";
            LEFT = "left";
            RIGHT = "right";
            START = "start";
            BACK = "back";
            GAMEPAD_A = "enter-gamepad_A";
            GAMEPAD_B = "escape-gamepad_B";
            GAMEPAD_X = "gamepad_X";
            GAMEPAD_Y = "gamepad_Y";
            GAMEPAD_L1 = "gamepad_L1";
            GAMEPAD_L2 = "gamepad_L2";
            GAMEPAD_L3 = "gamepad_L3";
            GAMEPAD_R1 = "gamepad_R1";
            GAMEPAD_R2 = "gamepad_R2";
            GAMEPAD_R3 = "gamepad_R3";
            GAMEPAD_START = "start";
            GAMEPAD_BACK = "back";
            ENTER = "enter";
            ESCAPE = "escape-gamepad_B";
            END = "end";
            HOME = "home";
            PAGE_DOWN = "pageDown";
            PAGE_UP = "pageUp";
            TAB = "tab";
            SHIFT_TAB = "shifttab";
        }

        public static var UP:String="up";

        public static var DOWN:String="down";

        public static var LEFT:String="left";

        public static var RIGHT:String="right";

        public static var START:String="start";

        public static var BACK:String="back";

        public static var GAMEPAD_A:String="enter-gamepad_A";

        public static var GAMEPAD_B:String="escape-gamepad_B";

        public static var GAMEPAD_X:String="gamepad_X";

        public static var GAMEPAD_Y:String="gamepad_Y";

        public static var GAMEPAD_L1:String="gamepad_L1";

        public static var GAMEPAD_L2:String="gamepad_L2";

        public static var GAMEPAD_L3:String="gamepad_L3";

        public static var GAMEPAD_R1:String="gamepad_R1";

        public static var GAMEPAD_R2:String="gamepad_R2";

        public static var GAMEPAD_R3:String="gamepad_R3";

        public static var GAMEPAD_START:String="start";

        public static var GAMEPAD_BACK:String="back";

        public static var ENTER:String="enter";

        public static var ESCAPE:String="escape-gamepad_B";

        public static var END:String="end";

        public static var HOME:String="home";

        public static var PAGE_DOWN:String="pageDown";

        public static var PAGE_UP:String="pageUp";

        public static var TAB:String="tab";

        public static var SHIFT_TAB:String="shifttab";
    }
}
