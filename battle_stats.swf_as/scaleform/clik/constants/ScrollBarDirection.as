package scaleform.clik.constants 
{
    public class ScrollBarDirection extends Object
    {
        public function ScrollBarDirection()
        {
            super();
            return;
        }

        public static const HORIZONTAL:String="horizontal";

        public static const VERTICAL:String="vertical";
    }
}
