package scaleform.clik.utils 
{
    import flash.utils.*;
    
    public class WeakRefDictionary extends Object
    {
        public function WeakRefDictionary()
        {
            super();
            this._dictionary = new flash.utils.Dictionary(true);
            return;
        }

        public function setValue(arg1:Object, arg2:Object):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=this._dictionary;
            for (loc1 in loc4) 
            {
                loc2 = this._dictionary[loc1];
                if (arg1 != loc2) 
                {
                    continue;
                }
                delete this._dictionary[loc1];
            }
            this._dictionary[arg2] = arg1;
            return;
        }

        public function getValue(arg1:Object):Object
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=this._dictionary;
            for (loc1 in loc4) 
            {
                loc2 = this._dictionary[loc1];
                if (arg1 != loc2) 
                {
                    continue;
                }
                return loc1;
            }
            return null;
        }

        protected var _dictionary:flash.utils.Dictionary;
    }
}
