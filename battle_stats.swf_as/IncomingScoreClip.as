package 
{
    import lesta.dialogs.battle_window._battle_progress.*;
    
    public dynamic class IncomingScoreClip extends lesta.dialogs.battle_window._battle_progress.IncomingScore
    {
        public function IncomingScoreClip()
        {
            super();
            addFrameScript(37, this.frame38, 74, this.frame75);
            return;
        }

        internal function frame38():*
        {
            onEndAnim();
            return;
        }

        internal function frame75():*
        {
            onEndAnim();
            return;
        }
    }
}
