﻿package 
{
    import lesta.dialogs.battle_window._player_stats_item.*;
	import flash.events.*;
	import flash.net.*;
	import lesta.structs.*;
    import scaleform.clik.data.*;
	import flash.net.*;
	import big_ears.Config;
	import monstrofil.global.EarsConfig;
	

    
    public dynamic class EarList extends lesta.dialogs.battle_window._player_stats_item.EarList
    {
        public function EarList()
        {
            super();
            return;
        }
		
		override public function init(arg1:scaleform.clik.data.DataProvider, arg2:Boolean=false):void{
			super.init(arg1, arg2);
			var myTextLoader:URLLoader = new URLLoader();
			myTextLoader.addEventListener(Event.COMPLETE, this.onLoaded);
			myTextLoader.load(new URLRequest(EarsConfig.xmlFileName));
		}
		
		
		public function onLoaded(e:Event):void {
			EarsConfig.xml = XML(e.target.data);
			
			EarsConfig.aliveIcon = EarsConfig.xml.global.aliveIcon;
			EarsConfig.deadIcon = EarsConfig.xml.global.deadIcon;
			EarsConfig.ownIcon = EarsConfig.xml.global.ownIcon;
			
			EarsConfig.health_alpha = EarsConfig.xml.global.health_alpha;
			EarsConfig.health_index = EarsConfig.xml.global.health_index;
			
			/* spotted lamps */
			EarsConfig.lamp_active = EarsConfig.xml.lamps.active == "true";
			
			EarsConfig.lamp_x = EarsConfig.xml.lamps.lamp_x;
			
			EarsConfig.lamp_y = EarsConfig.xml.lamps.lamp_y;
			
			EarsConfig.unvisibleIcon = EarsConfig.xml.lamps.unvisibleIcon;
			
			EarsConfig.visibleIcon = EarsConfig.xml.lamps.visibleIcon;
			
			/* left */
			
			EarsConfig.left_txt_ship_visible = EarsConfig.xml.left_ear.txt_ship_visible == "true";
			EarsConfig.left_txt_ship_width = EarsConfig.xml.left_ear.txt_ship_width;
			EarsConfig.left_txt_ship_x = EarsConfig.xml.left_ear.txt_ship_x;
			
			EarsConfig.left_txt_user_name_visible = EarsConfig.xml.left_ear.txt_user_name_visible == "true";
			EarsConfig.left_txt_user_name_width = EarsConfig.xml.left_ear.txt_user_name_width;
			EarsConfig.left_txt_user_name_x = EarsConfig.xml.left_ear.txt_user_name_x;
			
			EarsConfig.left_txt_frags_visible = EarsConfig.xml.left_ear.txt_frags_visible == "true";
			EarsConfig.left_txt_frags_width = EarsConfig.xml.left_ear.txt_frags_width;
			EarsConfig.left_txt_frags_x = EarsConfig.xml.left_ear.txt_frags_x;
			
			EarsConfig.left_mvc_ship_position_x = EarsConfig.xml.left_ear.mvc_ship_position_x;
			EarsConfig.left_mvc_ship_position_y = EarsConfig.xml.left_ear.mvc_ship_position_y;
			
			EarsConfig.left_health_x = EarsConfig.xml.left_ear.health_x;
			EarsConfig.left_health_y = EarsConfig.xml.left_ear.health_y;
			
			EarsConfig.left_health_width = EarsConfig.xml.left_ear.health_width;
			EarsConfig.left_health_height = EarsConfig.xml.left_ear.health_height;
			
			EarsConfig.SCALE_LEFT =  EarsConfig.xml.left_ear.ship_scale;
			EarsConfig.PATTERN_LEFT = EarsConfig.xml.left_ear.ship_pattern;
			
			
			/*RIGHT*/
			EarsConfig.right_txt_ship_visible = EarsConfig.xml.right_ear.txt_ship_visible == "true";
			EarsConfig.right_txt_ship_width = EarsConfig.xml.right_ear.txt_ship_width;
			EarsConfig.right_txt_ship_x = EarsConfig.xml.right_ear.txt_ship_x;
			
			EarsConfig.right_txt_user_name_visible = EarsConfig.xml.right_ear.txt_user_name_visible == "true";
			EarsConfig.right_txt_user_name_width = EarsConfig.xml.right_ear.txt_user_name_width;
			EarsConfig.right_txt_user_name_x = EarsConfig.xml.right_ear.txt_user_name_x;
			
			EarsConfig.right_txt_frags_visible = EarsConfig.xml.right_ear.txt_frags_visible == "true";
			EarsConfig.right_txt_frags_width = EarsConfig.xml.right_ear.txt_frags_width;
			EarsConfig.right_txt_frags_x = EarsConfig.xml.right_ear.txt_frags_x;
			
			EarsConfig.right_mvc_ship_position_x = EarsConfig.xml.right_ear.mvc_ship_position_x;
			EarsConfig.right_mvc_ship_position_y = EarsConfig.xml.right_ear.mvc_ship_position_y;
			
			EarsConfig.right_health_x = EarsConfig.xml.right_ear.health_x;
			EarsConfig.right_health_y = EarsConfig.xml.right_ear.health_y;
			
			EarsConfig.right_health_width = EarsConfig.xml.right_ear.health_width;
			EarsConfig.right_health_height = EarsConfig.xml.right_ear.health_height;
			
			EarsConfig.right_priorityMarkers_x = EarsConfig.xml.right_ear.priorityMarkers_x;
			
			EarsConfig.SCALE_RIGHT =  EarsConfig.xml.right_ear.ship_scale;
			EarsConfig.PATTERN_RIGHT = EarsConfig.xml.right_ear.ship_pattern;
			
			EarsConfig.needReload = true;
			trace("Config is good!");
		}
    }
}
