﻿package 
{
    import lesta.dialogs.battle_window._player_stats_item.*;
	import flash.display.*;
	import lesta.constants.*;
	import flash.text.*;
	import flash.events.*;
	import flash.net.*;
	import lesta.structs.*;
	import lesta.utils.Debug;
	import lesta.cpp.Translator;
	import flash.utils.Dictionary;
	import lesta.controls.Image;
	import flash.external.ExternalInterface;
	import monstrofil.components.HealthBarLine;
	import monstrofil.global.EarsConfig;
    
    public dynamic class PlayersStatePanelLeftListItemRenderer extends lesta.dialogs.battle_window._player_stats_item.PlayersStateItemRenderer
    {
		public var frames_normalised: Vector.<String> = new Vector.<String>();
		
		public var health: HealthBarLine = new HealthBarLine( );
		public var right:Boolean = false;
		public var configReloaded = false;
		
		
		public var textFields:Dictionary = new Dictionary();
	
		
		public function doMacros(player: Player, pattern: String)
		{
			
			return EarsConfig.doMacros(player, pattern);
		}
		
		
        public function PlayersStatePanelLeftListItemRenderer()
        {
            super();
			health.alpha_ = EarsConfig.health_alpha;

			this.addChild(health);			
			this.setChildIndex(health, EarsConfig.health_index);
            return;
        }
		
		public function make_normal(){
			var default_format:TextFormat = this.item.txt_user_name.getTextFormat();
			default_format.bold = true;
			
			health.x = EarsConfig.left_health_x;
			health.y = EarsConfig.left_health_y;
			
			this.shipIcon.alpha = this.item.txt_user_name.alpha;
			
			this.item.txt_user_name.visible = EarsConfig.left_txt_user_name_visible;
			this.item.txt_user_name.width = EarsConfig.left_txt_user_name_width;
			this.item.txt_user_name.x = EarsConfig.left_txt_user_name_x;
			
			this.item.txt_frags.x = EarsConfig.left_txt_frags_x;
			this.item.txt_frags.width = EarsConfig.left_txt_frags_width;
			this.item.txt_frags.visible = EarsConfig.left_txt_frags_visible;
			
			this.item.mvc_ship_position.x = EarsConfig.left_mvc_ship_position_x;
			if(EarsConfig.SCALE_LEFT < 0)
				this.item.mvc_ship_position.x = EarsConfig.left_mvc_ship_position_x - EarsConfig.SCALE_LEFT * 122;
				
			this.item.mvc_ship_position.y = EarsConfig.left_mvc_ship_position_y;
			
					
				
		}
		
		override public function setMutableData(arg1:Player):void{
			super.setMutableData(arg1);
			if (configReloaded){
				for each(var i in EarsConfig.xml.left_ear.textfields.textfield){
					EarsConfig.updateField(this, i, arg1);
				}
			}
			
            if (frames_normalised.indexOf(this.item.currentFrameLabel) == -1 || EarsConfig.needReload && !configReloaded) {
				frames_normalised.push(this.item.currentFrameLabel);
				this.make_normal();
				
				if(this.item.currentFrameLabel.indexOf("dead") != -1){
					this.shipIcon.src = EarsConfig.doMacros(arg1, EarsConfig.deadIcon);
				}
				else if (this.item.currentFrameLabel.indexOf("player") != -1){
					this.shipIcon.src = EarsConfig.doMacros(arg1, EarsConfig.ownIcon);
				}
				else {
					this.shipIcon.src = EarsConfig.doMacros(arg1, EarsConfig.aliveIcon);
				}
			}
			
			if(EarsConfig.needReload && !configReloaded){
				this.make_normal();
				this.shipIcon.scaleX = EarsConfig.SCALE_LEFT;
				this.shipIcon.scaleY = EarsConfig.SCALE_LEFT > 0? EarsConfig.SCALE_LEFT: -EarsConfig.SCALE_LEFT;
				
				for each(var i in EarsConfig.xml.left_ear.textfields.textfield){
					EarsConfig.createField(this, i, arg1);
				}
				
				health.alpha_ = EarsConfig.health_alpha;
				configReloaded = true;
				this.setChildIndex(health, EarsConfig.health_index);
			}
			
			if(!(arg1.health == 0 && arg1.isAlive)){
				health.draw(arg1.health, arg1.maxHealth, EarsConfig.left_health_width, EarsConfig.left_health_height);
			}
		}
		
		
		
		override public function setConstantData(arg1:Player):void{
			super.setConstantData(arg1);			
		}
    }
}
