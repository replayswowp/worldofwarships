package 
{
    import lesta.controls.*;
    
    public dynamic class MegabaseTimerClip extends lesta.controls.AnimatedStateClip
    {
        public function MegabaseTimerClip()
        {
            super();
            addFrameScript(0, this.frame1, 14, this.frame15, 29, this.frame30);
            return;
        }

        internal function frame1():*
        {
            stop();
            return;
        }

        internal function frame15():*
        {
            stop();
            return;
        }

        internal function frame30():*
        {
            stop();
            return;
        }
    }
}
