﻿package 
{
    import flash.display.*;
    import flash.text.*;
    import monstrofil.global.GlobalData;
    
    public dynamic class Pingometer extends flash.display.Sprite
    {
        public function Pingometer()
        {
            super();
			var textFormat:TextFormat = GlobalData.instance.textFormatDefault;
			textFormat.size = 12;
			XVMinfo.defaultTextFormat = textFormat;
			XVMinfo.filters = [GlobalData.instance.shadowDefault];
			XVMinfo.y = this.text.y;
			XVMinfo.x = this.text.x + this.text.width - 20;
			XVMinfo.height = this.text.height;
			XVMinfo.text = "© Monstrofil, 2016";
			this.addChild(XVMinfo);
            return;
        }

        public var text:flash.text.TextField;
		
		public var XVMinfo:TextField = new TextField();
    }
}
