package 
{
    import lesta.dialogs.battle_window._battle_progress.*;
    
    public dynamic class CapturePointIndicatorClip extends lesta.dialogs.battle_window._battle_progress.CapturePointIndicator
    {
        public function CapturePointIndicatorClip()
        {
            super();
            addFrameScript(0, this.frame1, 13, this.frame14, 29, this.frame30, 53, this.frame54, 67, this.frame68);
            return;
        }

        internal function frame1():*
        {
            stop();
            return;
        }

        internal function frame14():*
        {
            stop();
            return;
        }

        internal function frame30():*
        {
            stop();
            return;
        }

        internal function frame54():*
        {
            stop();
            return;
        }

        internal function frame68():*
        {
            stop();
            return;
        }
    }
}
