package 
{
    import flash.display.*;
    
    public dynamic class CapturePointSymbolClip extends flash.display.MovieClip
    {
        public function CapturePointSymbolClip()
        {
            super();
            addFrameScript(0, this.frame1, 19, this.frame20);
            return;
        }

        internal function frame1():*
        {
            stop();
            return;
        }

        internal function frame20():*
        {
            gotoAndPlay("locking");
            return;
        }

        public var symbol:flash.display.MovieClip;
    }
}
