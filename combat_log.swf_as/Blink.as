package 
{
    import flash.display.*;
    
    public dynamic class Blink extends flash.display.MovieClip
    {
        public function Blink()
        {
            super();
            addFrameScript(0, this.frame1, 29, this.frame30, 30, this.frame31);
            return;
        }

        internal function frame1():*
        {
            play();
            return;
        }

        internal function frame30():*
        {
            gotoAndStop(1);
            return;
        }

        internal function frame31():*
        {
            gotoAndStop(1);
            return;
        }
    }
}
