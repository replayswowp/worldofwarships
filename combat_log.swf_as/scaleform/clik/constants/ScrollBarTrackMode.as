package scaleform.clik.constants 
{
    public class ScrollBarTrackMode extends Object
    {
        public function ScrollBarTrackMode()
        {
            super();
            return;
        }

        public static const SCROLL_PAGE:String="scrollPage";

        public static const SCROLL_TO_CURSOR:String="scrollToCursor";
    }
}
