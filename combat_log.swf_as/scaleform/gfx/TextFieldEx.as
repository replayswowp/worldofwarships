package scaleform.gfx 
{
    import flash.display.*;
    import flash.text.*;
    
    public final class TextFieldEx extends scaleform.gfx.InteractiveObjectEx
    {
        public function TextFieldEx()
        {
            super();
            return;
        }

        public static function appendHtml(arg1:flash.text.TextField, arg2:String):void
        {
            return;
        }

        public static function setIMEEnabled(arg1:flash.text.TextField, arg2:Boolean):void
        {
            return;
        }

        public static function setVerticalAlign(arg1:flash.text.TextField, arg2:String):void
        {
            return;
        }

        public static function getVerticalAlign(arg1:flash.text.TextField):String
        {
            return "none";
        }

        public static function setTextAutoSize(arg1:flash.text.TextField, arg2:String):void
        {
            return;
        }

        public static function getTextAutoSize(arg1:flash.text.TextField):String
        {
            return "none";
        }

        public static function setImageSubstitutions(arg1:flash.text.TextField, arg2:Object):void
        {
            return;
        }

        public static function updateImageSubstitution(arg1:flash.text.TextField, arg2:String, arg3:flash.display.BitmapData):void
        {
            return;
        }

        public static const VALIGN_NONE:String="none";

        public static const VALIGN_TOP:String="top";

        public static const VALIGN_CENTER:String="center";

        public static const VALIGN_BOTTOM:String="bottom";

        public static const TEXTAUTOSZ_NONE:String="none";

        public static const TEXTAUTOSZ_SHRINK:String="shrink";

        public static const TEXTAUTOSZ_FIT:String="fit";
    }
}
