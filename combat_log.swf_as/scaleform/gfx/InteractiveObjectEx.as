package scaleform.gfx 
{
    import flash.display.*;
    
    public class InteractiveObjectEx extends scaleform.gfx.DisplayObjectEx
    {
        public function InteractiveObjectEx()
        {
            super();
            return;
        }

        public static function setHitTestDisable(arg1:flash.display.InteractiveObject, arg2:Boolean):void
        {
            return;
        }

        public static function getHitTestDisable(arg1:flash.display.InteractiveObject):Boolean
        {
            return false;
        }

        public static function setTopmostLevel(arg1:flash.display.InteractiveObject, arg2:Boolean):void
        {
            return;
        }

        public static function getTopmostLevel(arg1:flash.display.InteractiveObject):Boolean
        {
            return false;
        }
    }
}
