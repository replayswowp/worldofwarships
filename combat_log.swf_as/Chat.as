﻿package 
{
    import adobe.utils.*;
    import flash.accessibility.*;
    import flash.desktop.*;
    import flash.display.*;
    import flash.errors.*;
    import flash.events.*;
    import flash.external.*;
    import flash.filters.*;
    import flash.geom.*;
    import flash.globalization.*;
    import flash.media.*;
    import flash.net.*;
    import flash.net.drm.*;
    import flash.printing.*;
    import flash.profiler.*;
    import flash.sampler.*;
    import flash.sensors.*;
    import flash.system.*;
    import flash.text.*;
    import flash.text.engine.*;
    import flash.text.ime.*;
    import flash.ui.*;
    import flash.utils.*;
    import flash.xml.*;
    import lesta.dialogs.battle_window._ingame_chat.*;
    import lesta.structs.Player;
    import lesta.cpp.Translator;
    
    public dynamic class Chat extends lesta.dialogs.battle_window._ingame_chat.QuickChat
    {
        public function Chat()
        {
            super();
            this.__setProp_txa_output_Chat_txa_output_0();
            this.__setProp_txaHeader_Chat_txaHeader_0();
            return;
        }
		
		 protected override function setText(arg1:Object):void
        {
            var loc2:lesta.structs.Player=null;
            var loc3:*=null;
		
            var loc1:*={"nickname":arg1.nickname, "avatarId":arg1.avatarId};
            if (controller) 
            {
                controller.handleAddChatMessage(null, loc1);
                loc2 = loc1.player as lesta.structs.Player;
                if (loc2) 
                {
                    loc3 = arg1.message;
                    arg1.message = loc3.replace(arg1.nickname, arg1.nickname + " [" + lesta.cpp.Translator.translate(loc2.shipIDS) + "]");
                }
			}
			
			super.setText(arg1);
            return;
        }

        internal function __setProp_txa_output_Chat_txa_output_0():*
        {
            try 
            {
                txa_output["componentInspectorSetting"] = true;
            }
            catch (e:Error)
            {
            };
            txa_output.alphaModifier = 0;
            txa_output.antiSpam = false;
            txa_output.blinkArrowButtonElementName = "";
            txa_output.blinkTimer = 0;
            txa_output.enabled = true;
            txa_output.enableInitCallback = false;
            txa_output.itemRendererName = ["ChatListItemRenderer"];
            txa_output.listItemRendererDethTime = 2;
            txa_output.listItemRendererLifeTime = 180000;
            txa_output.messageBufferLength = 50;
            txa_output.messageBufferOverflowDeathTime = 1;
            txa_output.scrollBarName = "CombatChatScrollBar";
            txa_output.scrollBarAboveItemRenderer = false;
            txa_output.scrollBarStep = 30;
            txa_output.scrollPosition = "left";
            txa_output.soundSet = "";
            txa_output.textDisplayFlow = "bottom-up";
            txa_output.tweenListItemRenderer = true;
            txa_output.tweenTime = 0.2;
            txa_output.visible = true;
            try 
            {
                txa_output["componentInspectorSetting"] = false;
            }
            catch (e:Error)
            {
            };
            return;
        }

        internal function __setProp_txaHeader_Chat_txaHeader_0():*
        {
            try 
            {
                txaHeader["componentInspectorSetting"] = true;
            }
            catch (e:Error)
            {
            };
            txaHeader.actAsButton = false;
            txaHeader.defaultText = "";
            txaHeader.displayAsPassword = false;
            txaHeader.editable = false;
            txaHeader.enabled = true;
            txaHeader.enableInitCallback = false;
            txaHeader.focusable = false;
            txaHeader.maxChars = 0;
            txaHeader.minThumbSize = 1;
            txaHeader.scrollBar = "";
            txaHeader.text = "";
            txaHeader.thumbOffset = {"top":0, "bottom":0};
            txaHeader.visible = true;
            try 
            {
                txaHeader["componentInspectorSetting"] = false;
            }
            catch (e:Error)
            {
            };
            return;
        }
    }
}
