package 
{
    import adobe.utils.*;
    import flash.accessibility.*;
    import flash.desktop.*;
    import flash.display.*;
    import flash.errors.*;
    import flash.events.*;
    import flash.external.*;
    import flash.filters.*;
    import flash.geom.*;
    import flash.globalization.*;
    import flash.media.*;
    import flash.net.*;
    import flash.net.drm.*;
    import flash.printing.*;
    import flash.profiler.*;
    import flash.sampler.*;
    import flash.sensors.*;
    import flash.system.*;
    import flash.text.*;
    import flash.text.engine.*;
    import flash.text.ime.*;
    import flash.ui.*;
    import flash.utils.*;
    import flash.xml.*;
    import lesta.dialogs.battle_window.*;
    
    public dynamic class CombatLog extends lesta.dialogs.battle_window.BattleLog
    {
        public function CombatLog()
        {
            super();
            this.__setProp_chat_CombatLog_Chat_0();
            return;
        }

        internal function __setProp_chat_CombatLog_Chat_0():*
        {
            try 
            {
                chat["componentInspectorSetting"] = true;
            }
            catch (e:Error)
            {
            };
            chat.contentPadding = {"top":0, "right":0, "bottom":0, "left":6};
            chat.enabled = true;
            chat.enableInitCallback = false;
            chat.maxHeight = 500;
            chat.maxWidth = 500;
            chat.minHeight = 150;
            chat.minWidth = 150;
            chat.soundSet = "";
            chat.source = "";
            chat.title = "My Window";
            chat.visible = true;
            try 
            {
                chat["componentInspectorSetting"] = false;
            }
            catch (e:Error)
            {
            };
            return;
        }
    }
}
