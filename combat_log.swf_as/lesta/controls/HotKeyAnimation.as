package lesta.controls 
{
    import flash.text.*;
    import lesta.managers.*;
    
    public class HotKeyAnimation extends lesta.controls.AnimatedStateClip
    {
        public function HotKeyAnimation()
        {
            super();
            this.textField = item.textField;
            setState(lesta.controls.HotKeyContainer.DEFAULT, true);
            this.startWidth = this.width;
            this.startBackWidth = this.item.back.width;
            this.textStartScale = this.textField.scaleX;
            return;
        }

        public function set commandId(arg1:int):void
        {
            this._commandId = arg1;
            this.updateCommandData();
            return;
        }

        public function get commandId():int
        {
            return this._commandId;
        }

        public function updateCommandData():void
        {
            if (this._commandId == -1) 
            {
                return;
            }
            this.text = lesta.managers.HotKeyManager.instance.getKeyStrByCommandId(this._commandId);
            return;
        }

        public override function set text(arg1:String):void
        {
            super.text = arg1;
            this.updateBackgroundSize();
            return;
        }

        public function set toggle(arg1:Boolean):void
        {
            this._toggle = arg1;
            return;
        }

        public function get toggle():Boolean
        {
            return this._toggle;
        }

        public function set pressed(arg1:Boolean):void
        {
            if (this.toggle) 
            {
                return;
            }
            var loc1:*=arg1 ? lesta.controls.HotKeyContainer.DOWN : lesta.controls.HotKeyContainer.DEFAULT;
            setState(loc1, false, true);
            return;
        }

        public function set toggled(arg1:Boolean):void
        {
            if (!this.toggle) 
            {
                return;
            }
            var loc1:*=arg1 ? lesta.controls.HotKeyContainer.TOGGLE : lesta.controls.HotKeyContainer.DEFAULT;
            setState(loc1);
            setChildState(loc1);
            return;
        }

        private function updateBackgroundSize():void
        {
            var loc1:*=(this.startWidth + this.textField.textWidth - this.startBackWidth + PADDING * 2) / this.startWidth;
            this.scaleX = Math.max(1, loc1);
            this.textField.scaleX = this.textStartScale / this.scaleX;
            this.textField.x = (-this.textField.width) / 2;
            return;
        }

        public static const PADDING:Number=4;

        public var textField:flash.text.TextField;

        protected var _commandId:int=-1;

        private var _toggle:Boolean;

        private var textStartScale:Number=0;

        private var startWidth:Number=0;

        private var startBackWidth:Number=0;
    }
}
