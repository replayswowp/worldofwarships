package lesta.controls 
{
    import flash.display.*;
    import flash.text.*;
    import flash.utils.*;
    
    public class AnimationContainer extends flash.display.MovieClip
    {
        public function AnimationContainer()
        {
            super();
            this.clip = this;
            return;
        }

        public function set clipClassName(arg1:String):void
        {
            this._clipClassName = arg1;
            if (this._clipClassName == "") 
            {
                return;
            }
            this.clipClass = flash.utils.getDefinitionByName(this._clipClassName) as Class;
            this.clip = new this.clipClass();
            this.addClipToDeepest(this.clip);
            return;
        }

        public function get clipClassName():String
        {
            return this._clipClassName;
        }

        public function set text(arg1:String):void
        {
            this._text = arg1;
            this.setTextToAllChildren();
            return;
        }

        public function get text():String
        {
            return this._text;
        }

        private function addClipToDeepest(arg1:flash.display.MovieClip):void
        {
            var loc1:*=this;
            while (loc1.item) 
            {
                loc1 = loc1.item;
            }
            while (loc1.numChildren > 0) 
            {
                loc1.removeChildAt(0);
            }
            loc1.addChild(arg1);
            return;
        }

        protected function setTextToAllChildren():void
        {
            if (this._text != null) 
            {
                this.setTextToClipChildren(this);
            }
            return;
        }

        protected function setTextToClipChildren(arg1:flash.display.DisplayObjectContainer, arg2:String=""):void
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            if (arg1 == null) 
            {
                return;
            }
            var loc1:*=arg2 != "" ? arg2 : this._text;
            var loc2:*=0;
            var loc3:*=arg1.numChildren;
            while (loc2 < loc3) 
            {
                loc4 = arg1.getChildAt(loc2);
                loc5 = loc4 as flash.display.DisplayObjectContainer;
                if (loc5) 
                {
                    this.setTextToClipChildren(loc5);
                }
                else 
                {
                    loc6 = loc4 as flash.text.TextField;
                    if (loc6) 
                    {
                        loc6.autoSize = flash.text.TextFieldAutoSize.CENTER;
                        loc6.text = loc1;
                    }
                }
                ++loc2;
            }
            return;
        }

        public var item:flash.display.MovieClip;

        protected var clip:flash.display.MovieClip;

        private var _clipClassName:String="";

        private var clipClass:Class=null;

        private var _text:String=null;
    }
}
