package lesta.controls 
{
    import __AS3__.vec.*;
    import flash.utils.*;
    import lesta.interfaces.*;
    import lesta.utils.*;
    
    public class MessageListItemRendererContoller extends Object
    {
        public function MessageListItemRendererContoller()
        {
            this.listItemRenderersStruct = [];
            this.listObjectPool = [];
            this.listOfListsVisibleElements = [];
            this.listTestInstances = [];
            super();
            return;
        }

        public function initItemRendererCollection(arg1:Array):Boolean
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc1:*=true;
            var loc2:*=0;
            while (loc2 < arg1.length) 
            {
                loc3 = flash.utils.getDefinitionByName(arg1[loc2]) as Class;
                if (loc3 == null) 
                {
                    trace("[Error] MessageListItemRendererContoller: The class " + arg1[loc2] + " cannot be found in your library. Please ensure it is there.");
                    loc1 = false;
                    break;
                }
                else 
                {
                    loc4 = new lesta.controls.MessageListItemRendererStruct(arg1[loc2], loc3);
                    this.listItemRenderersStruct.push(loc4);
                    this.listObjectPool.push(new lesta.utils.ObjectPool(loc3));
                    this.listOfListsVisibleElements[loc2] = new Array();
                }
                ++loc2;
            }
            return loc1;
        }

        public function getTestInstance(arg1:uint):lesta.interfaces.IMessageListItemRenderer
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=this.listTestInstances[arg1];
            if (loc1 == null) 
            {
                loc2 = this.listItemRenderersStruct[arg1];
                if (loc2 == null) 
                {
                    trace("[Error] MessageListItemRendererContoller: invalid ItemRenderer type " + arg1);
                }
                else 
                {
                    loc3 = this.listItemRenderersStruct[arg1].objectClass;
                    loc1 = new loc3();
                }
                this.listTestInstances[arg1] = loc1;
            }
            return loc1;
        }

        public function start():void
        {
            var loc1:*=0;
            while (loc1 < this.listItemRenderersStruct.length) 
            {
                this.listItemRenderersStruct[loc1].lastCount = this.listItemRenderersStruct[loc1].countUseAtMoment;
                this.listItemRenderersStruct[loc1].countUseAtMoment = 0;
                ++loc1;
            }
            return;
        }

        public function setData(arg1:Object):lesta.interfaces.IMessageListItemRenderer
        {
            var loc1:*=0;
            if (arg1.id) 
            {
                loc1 = arg1.id;
            }
            var loc2:*=this.getElement(loc1);
            var loc3:*;
            var loc4:*=((loc3 = this.listItemRenderersStruct[loc1]).countUseAtMoment + 1);
            loc3.countUseAtMoment = loc4;
            return loc2;
        }

        public function end():__AS3__.vec.Vector.<lesta.interfaces.IMessageListItemRenderer>
        {
            var loc3:*=0;
            var loc4:*=0;
            var loc1:*=new Vector.<lesta.interfaces.IMessageListItemRenderer>();
            var loc2:*=0;
            while (loc2 < this.listObjectPool.length) 
            {
                if (this.listItemRenderersStruct[loc2].countUseAtMoment < this.listItemRenderersStruct[loc2].lastCount) 
                {
                    loc3 = this.listItemRenderersStruct[loc2].countUseAtMoment;
                    loc4 = loc3;
                    while (loc4 < this.listItemRenderersStruct[loc2].lastCount) 
                    {
                        loc1.push(this.listOfListsVisibleElements[loc2][loc3]);
                        this.listObjectPool[loc2].free(this.listOfListsVisibleElements[loc2][loc3]);
                        this.listOfListsVisibleElements[loc2].splice(loc3, 1);
                        ++loc4;
                    }
                }
                ++loc2;
            }
            return loc1;
        }

        private function getElement(arg1:uint):lesta.interfaces.IMessageListItemRenderer
        {
            var loc1:*=null;
            if (this.listItemRenderersStruct[arg1].countUseAtMoment < this.listOfListsVisibleElements[arg1].length) 
            {
                loc1 = this.listOfListsVisibleElements[arg1][this.listItemRenderersStruct[arg1].countUseAtMoment] as lesta.interfaces.IMessageListItemRenderer;
            }
            else 
            {
                loc1 = this.listObjectPool[arg1].alloc() as lesta.interfaces.IMessageListItemRenderer;
                this.listOfListsVisibleElements[arg1].push(loc1);
            }
            return loc1;
        }

        public var listItemRenderersStruct:Array;

        public var listObjectPool:Array;

        public var listOfListsVisibleElements:Array;

        public var listTestInstances:Array;
    }
}
