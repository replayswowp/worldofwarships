package lesta.controls 
{
    import flash.display.*;
    import flash.events.*;
    import flash.ui.*;
    import lesta.data.*;
    import scaleform.clik.controls.*;
    import scaleform.clik.data.*;
    import scaleform.gfx.*;
    
    public class ChatModel extends scaleform.clik.controls.Window
    {
        public function ChatModel()
        {
            this.messagesList = [];
            super();
            this.txa_input.tabEnabled = false;
            this.addEventListener(flash.events.Event.ADDED_TO_STAGE, this.init, false, 0, true);
            lesta.data.GameDelegate.addCallBack("ChatController.incomingMessage", this, this.incomingMessage);
            lesta.data.GameDelegate.addCallBack("ChatController.channelChanged", this, this.changeChannel);
            lesta.data.GameDelegate.addCallBack("GameInfoHolder.updateMuteStatus", this, this.updateMuteStatus);
            return;
        }

        protected function init(arg1:flash.events.Event):void
        {
            this.removeEventListener(flash.events.Event.ADDED_TO_STAGE, this.init);
            this.addEventListener(flash.events.Event.REMOVED_FROM_STAGE, this.free, false, 0, true);
            this.tabEnabled = false;
            this.tabChildren = false;
            this.addEventListener(flash.events.KeyboardEvent.KEY_DOWN, this.onKeyboardDown, false, 0, true);
            this.txa_input.addEventListener(flash.events.FocusEvent.FOCUS_IN, this.changeFocusIn, false, 0, true);
            this.txa_input.addEventListener(flash.events.FocusEvent.FOCUS_OUT, this.changeFocusOut, false, 0, true);
            lesta.data.GameDelegate.addCallBack("ChatController.setFocus", this, this.setFocus);
            lesta.data.GameDelegate.addCallBack("ChatController.resetFocus", this, this.resetFocus);
            return;
        }

        public function inFocus():Boolean
        {
            return scaleform.gfx.FocusManager.getFocus() == this.txa_input.textField;
        }

        protected function setFocus():void
        {
            var loc1:*=scaleform.gfx.FocusManager.getFocus();
            if (this.visible && (loc1 == stage || loc1 == null)) 
            {
                scaleform.gfx.FocusManager.setFocus(this.txa_input.textField);
            }
            return;
        }

        private function onKeyboardDown(arg1:flash.events.KeyboardEvent):void
        {
            if (arg1.keyCode == flash.ui.Keyboard.ESCAPE && this.inFocus()) 
            {
                this.resetFocus(true);
                arg1.stopPropagation();
            }
            else if (arg1.keyCode == flash.ui.Keyboard.ENTER || arg1.keyCode == flash.ui.Keyboard.NUMPAD_ENTER) 
            {
                if (this.inFocus()) 
                {
                    this.outgoingMessage();
                    arg1.stopPropagation();
                }
            }
            return;
        }

        protected function resetFocus(arg1:Boolean):void
        {
            if (arg1) 
            {
                this.txa_input.text = "";
            }
            scaleform.gfx.FocusManager.setFocus(stage);
            return;
        }

        protected function changeFocusIn(arg1:flash.events.FocusEvent):void
        {
            lesta.data.GameDelegate.call("ChatModel.changeFocus", [true]);
            return;
        }

        protected function changeFocusOut(arg1:flash.events.FocusEvent):void
        {
            lesta.data.GameDelegate.call("ChatModel.changeFocus", [false]);
            return;
        }

        public function incomingMessage(arg1:String, arg2:int, arg3:String):void
        {
            this.setText({"message":arg1, "avatarId":arg2, "nickname":arg3});
            return;
        }

        public function changeChannel(arg1:String):void
        {
            this.txaHeader.htmlText = arg1;
            return;
        }

        protected function outgoingMessage():void
        {
            lesta.data.GameDelegate.call("ChatModel.outgoingMessage", [this.txa_input.text.concat()]);
            this.resetFocus(true);
            return;
        }

        protected override function close():void
        {
            this.visible = false;
            return;
        }

        public function free():void
        {
            lesta.data.GameDelegate.removeCallBack(this);
            this.removeEventListener(flash.events.KeyboardEvent.KEY_DOWN, this.onKeyboardDown);
            this.removeEventListener(flash.events.Event.REMOVED_FROM_STAGE, this.free);
            this.txa_input.removeEventListener(flash.events.FocusEvent.FOCUS_IN, this.changeFocusIn);
            this.txa_input.removeEventListener(flash.events.FocusEvent.FOCUS_OUT, this.changeFocusOut);
            addEventListener(flash.events.Event.ADDED_TO_STAGE, this.init, false, 0, true);
            return;
        }

        protected override function draw():void
        {
            super.draw();
            return;
        }

        protected function setText(arg1:Object):void
        {
            var loc1:*=this.txa_output.dataProvider;
            loc1.push(arg1);
            this.txa_output.dataProvider = loc1;
            return;
        }

        private function updateMuteStatus(arg1:Object):void
        {
            var loc3:*=null;
            var loc1:*=arg1["id"];
            var loc2:*=arg1["mute"];
            var loc4:*=0;
            var loc5:*=this.txa_output.dataProvider;
            for each (loc3 in loc5) 
            {
                if (loc3.avatarId != loc1) 
                {
                    continue;
                }
                loc3.muteStatus = loc2;
            }
            return;
        }

        public var txaHeader:scaleform.clik.controls.TextArea;

        public var txa_output:MessageList;

        public var txa_input:scaleform.clik.controls.TextArea;

        protected var txtOutgoing:String="";

        protected var chanalID:int=0;

        protected var messagesList:Array;
    }
}
