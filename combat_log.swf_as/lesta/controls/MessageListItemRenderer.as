package lesta.controls 
{
    import flash.display.*;
    import flash.text.*;
    import lesta.dialogs.battle_window_controllers.*;
    import scaleform.clik.constants.*;
    import scaleform.clik.utils.*;
    
    public class MessageListItemRenderer extends lesta.controls.MessageListItemRendererBase
    {
        public function MessageListItemRenderer()
        {
            super();
            return;
        }

        public override function init(arg1:Object, arg2:Number):void
        {
            var loc1:*=getData() == null;
            super.init(arg1, arg2);
            if (arg1.img) 
            {
                this.event_img.gotoAndStop(arg1.img);
            }
            if (arg1.message) 
            {
                this.textField.htmlText = arg1.message;
            }
            if (arg1.controller) 
            {
                this.controller = arg1.controller;
                if (loc1) 
                {
                    this.controller.attachMessageContextMenu(this, arg1);
                }
                this.controller.handleAddChatMessage(this, arg1);
            }
            this.width = arg2;
            invalidateData();
            return;
        }

        public override function fini():void
        {
            if (this.controller) 
            {
                this.controller.detachMessageContextMenu(this);
                this.controller = null;
            }
            return;
        }

        public function select():void
        {
            this.originalHTML = this.textField.htmlText;
            this.textField.htmlText = "<FONT color=\"#FFFFFF\">" + this.textField.text + "</FONT>";
            return;
        }

        public function deselect():void
        {
            this.textField.htmlText = this.originalHTML;
            return;
        }

        public override function set width(arg1:Number):void
        {
            super.width = arg1;
            invalidate(this.INVALIDATION_TYPE_WIDTH);
            return;
        }

        protected override function configUI():void
        {
            super.configUI();
            constraints = new scaleform.clik.utils.Constraints(this, scaleform.clik.constants.ConstrainMode.REFLOW);
            constraints.addElement("background", this.background, scaleform.clik.utils.Constraints.ALL);
            this.textField.autoSize = flash.text.TextFieldAutoSize.LEFT;
            this.textField.wordWrap = true;
            return;
        }

        protected override function draw():void
        {
            var loc1:*=0;
            if (isInvalid(this.INVALIDATION_TYPE_WIDTH)) 
            {
                this.validateTextFieldWidth();
            }
            if (isInvalid(this.INVALIDATION_TYPE_WIDTH) || isInvalid(scaleform.clik.constants.InvalidationType.DATA)) 
            {
                _height = this.calculateHeight();
                invalidateSize();
            }
            if (isInvalid(scaleform.clik.constants.InvalidationType.SIZE)) 
            {
                loc1 = _height;
                constraints.update(_width, loc1);
            }
            return;
        }

        protected function validateTextFieldWidth():void
        {
            this.textField.width = _width - this.textField.x * 2;
            return;
        }

        private function calculateHeight():Number
        {
            return this.textField.y * 2 + this.textField.height;
        }

        private const INVALIDATION_TYPE_WIDTH:String="width";

        private const MIN_HEIGHT:int=48;

        public var textField:flash.text.TextField;

        public var background:flash.display.MovieClip;

        public var event_img:flash.display.MovieClip;

        protected var _img:String;

        protected var _type:String;

        protected var originalHTML:String;

        protected var controller:lesta.dialogs.battle_window_controllers.ComplainController;
    }
}
