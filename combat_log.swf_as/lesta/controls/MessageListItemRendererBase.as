package lesta.controls 
{
    import lesta.interfaces.*;
    import scaleform.clik.core.*;
    
    public class MessageListItemRendererBase extends scaleform.clik.core.UIComponent implements lesta.interfaces.IMessageListItemRenderer
    {
        public function MessageListItemRendererBase()
        {
            super();
            return;
        }

        public function init(arg1:Object, arg2:Number):void
        {
            this.itemData = arg1;
            return;
        }

        public function fini():void
        {
            return;
        }

        public function getData():Object
        {
            return this.itemData;
        }

        private var itemData:Object;
    }
}
