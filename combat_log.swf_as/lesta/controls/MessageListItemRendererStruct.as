package lesta.controls 
{
    public class MessageListItemRendererStruct extends Object
    {
        public function MessageListItemRendererStruct(arg1:String, arg2:Class)
        {
            super();
            this.name = arg1;
            this.objectClass = arg2;
            return;
        }

        public var name:String="";

        public var objectClass:Class=null;

        public var lastCount:uint=0;

        public var countUseAtMoment:uint=0;
    }
}
