package lesta.controls 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    
    public class AnimatedStateClip extends lesta.controls.AnimationContainer
    {
        public function AnimatedStateClip()
        {
            var loc2:*=null;
            this.mapDefaultStartStates = new flash.utils.Dictionary();
            this.listActions = new Vector.<Action>();
            this.currentLabelsCache = new flash.utils.Dictionary();
            super();
            super.stop();
            var loc1:*=0;
            while (loc1 < currentLabels.length) 
            {
                loc2 = currentLabels[loc1];
                this.currentLabelsCache[loc2.name] = loc2.frame;
                ++loc1;
            }
            return;
        }

        public function get state():String
        {
            return this.curState;
        }

        public function get childState():String
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < numChildren) 
            {
                loc2 = getChildAt(loc1) as lesta.controls.AnimatedStateClip;
                if (loc2 != null) 
                {
                    return loc2.state;
                }
                ++loc1;
            }
            return null;
        }

        private function updateVisible():void
        {
            var loc1:*=this.externalVisible && this.internalVisible;
            if (loc1 != visible) 
            {
                super.visible = loc1;
            }
            return;
        }

        private function isLabelExists(arg1:String):Boolean
        {
            return arg1 in this.currentLabelsCache;
        }

        protected function processNextActions():void
        {
            var loc1:*=null;
            if (this.isAnimPlaying || this.listActions.length == 0) 
            {
                return;
            }
            this.curAction = this.listActions.shift();
            var loc2:*=this.curAction.type;
            switch (loc2) 
            {
                case ACTION_ANIM_STATE:
                {
                    this.isAnimPlaying = true;
                    if (this.mask != null) 
                    {
                        this.mask.visible = false;
                    }
                    gotoAndPlay(this.curAction.label);
                    break;
                }
                case ACTION_STATE:
                {
                    gotoAndStop(this.curAction.label);
                    break;
                }
                case ACTION_ANIM:
                {
                    if (this.listActions.length > 0) 
                    {
                        loc1 = this.listActions[0];
                        if (loc1.type == ACTION_ANIM_STATE) 
                        {
                            loc1.type = ACTION_STATE;
                        }
                    }
                    this.isAnimPlaying = true;
                    if (this.mask != null) 
                    {
                        this.mask.visible = false;
                    }
                    gotoAndPlay(this.curAction.label);
                    break;
                }
                case ACTION_ANIM_TO_FRAME:
                {
                    this.isAnimPlaying = true;
                    if (this.mask != null) 
                    {
                        this.mask.visible = false;
                    }
                    gotoAndStop(this.curAction.fromFrame);
                    addEventListener(flash.events.Event.ENTER_FRAME, this.onEnterFrame);
                    break;
                }
            }
            return;
        }

        private function onEnterFrame(arg1:flash.events.Event):void
        {
            if (currentFrame < this.curAction.toFrame) 
            {
                gotoAndStop(currentFrame + 1);
            }
            else if (currentFrame > this.curAction.toFrame) 
            {
                gotoAndStop((currentFrame - 1));
            }
            else 
            {
                removeEventListener(flash.events.Event.ENTER_FRAME, this.onEnterFrame);
                this.isAnimPlaying = false;
                if (this.mask != null) 
                {
                    this.mask.visible = true;
                }
                this.processNextActions();
            }
            return;
        }

        public static function setStateToAll(arg1:Array, arg2:String, arg3:Boolean=false):void
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                loc2 = arg1[loc1];
                loc2.setState(arg2, arg3);
                ++loc1;
            }
            return;
        }

        public static function setStateToChildren(arg1:Array, arg2:String, arg3:Boolean=false):void
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                loc2 = arg1[loc1];
                loc2.setChildState(arg2, arg3);
                ++loc1;
            }
            return;
        }

        public static function setStateToAllAndChildren(arg1:Array, arg2:String, arg3:String, arg4:Boolean=false, arg5:Boolean=false):void
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < arg1.length) 
            {
                loc2 = arg1[loc1];
                loc2.setState(arg2, arg4);
                loc2.setChildState(arg3, arg5);
                ++loc1;
            }
            return;
        }

        public function fini():void
        {
            super.stop();
            this.listActions.length = 0;
            this.listActions = null;
            this.mapDefaultStartStates = null;
            this.currentLabelsCache = null;
            removeEventListener(flash.events.Event.ENTER_FRAME, this.onEnterFrame);
            return;
        }

        public override function set visible(arg1:Boolean):void
        {
            this.externalVisible = arg1;
            this.updateVisible();
            return;
        }

        public function setDefaultStartState(arg1:String, arg2:String):void
        {
            this.mapDefaultStartStates[arg2] = arg1;
            return;
        }

        public function setFinishState(arg1:String):void
        {
            this.finishState = arg1;
            return;
        }

        public function setState(arg1:String, arg2:Boolean=false, arg3:Boolean=true, arg4:String=null, arg5:Boolean=false):void
        {
            var loc5:*=null;
            var loc6:*=null;
            if (arg1 == this.curState || arg1 == null) 
            {
                return;
            }
            this.internalVisible = !(arg2 && arg1 == "inv");
            this.updateVisible();
            if (this.curState == null) 
            {
                arg2 = true;
            }
            var loc1:*=null;
            if (!arg2) 
            {
                loc5 = arg4 ? this.curState + "-(" + arg4 + ")-" + arg1 : this.curState + "-" + arg1;
                if (this.isLabelExists(loc5)) 
                {
                    loc1 = loc5;
                }
                else 
                {
                    loc6 = this.mapDefaultStartStates[arg1];
                    loc5 = arg4 ? loc6 + "-(" + arg4 + ")-" + arg1 : loc6 + "-" + arg1;
                    if (this.isLabelExists(loc5)) 
                    {
                        loc1 = loc5;
                    }
                }
            }
            var loc2:*=loc1 == null ? ACTION_STATE : ACTION_ANIM_STATE;
            var loc3:*=loc1 == null ? arg1 : loc1;
            if (arg3) 
            {
                this.listActions.length = 0;
            }
            var loc4:*=new Action(loc2);
            loc4.label = loc3;
            loc4.setInvisibleOnEnd = arg5 || arg1 == "inv";
            this.listActions.push(loc4);
            this.curState = arg1;
            this.processNextActions();
            return;
        }

        public function setChildState(arg1:String, arg2:Boolean=false):void
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < numChildren) 
            {
                loc2 = getChildAt(loc1) as lesta.controls.AnimatedStateClip;
                if (loc2 != null) 
                {
                    loc2.setState(arg1, arg2);
                }
                ++loc1;
            }
            return;
        }

        public function playAnim(arg1:String):void
        {
            var loc1:*=new Action(ACTION_ANIM);
            loc1.label = arg1;
            this.listActions.push(loc1);
            this.processNextActions();
            return;
        }

        public function playToFrame(arg1:int, arg2:int):void
        {
            var loc1:*=new Action(ACTION_ANIM_TO_FRAME);
            loc1.fromFrame = arg1;
            loc1.toFrame = arg2;
            this.listActions.push(loc1);
            this.processNextActions();
            return;
        }

        protected function onSetStateFrame(arg1:String):void
        {
            return;
        }

        public override function stop():void
        {
            super.stop();
            if (!(currentLabel == null) && currentLabel.indexOf("finish") == 0) 
            {
                dispatchEvent(new flash.events.Event(flash.events.Event.COMPLETE));
            }
            else 
            {
                dispatchEvent(new flash.events.Event(currentLabel));
            }
            this.isAnimPlaying = false;
            if (this.mask != null) 
            {
                this.mask.visible = true;
            }
            if (!(this.curState == null) && this.listActions.length == 0) 
            {
                if (currentLabel != this.curState) 
                {
                    gotoAndStop(this.curState);
                }
                this.internalVisible = !this.curAction.setInvisibleOnEnd;
                this.updateVisible();
                this.onSetStateFrame(this.curState);
            }
            setTextToAllChildren();
            this.processNextActions();
            return;
        }

        public static const ACTION_ANIM_STATE:int=0;

        public static const ACTION_ANIM:int=1;

        public static const ACTION_STATE:int=2;

        public static const ACTION_ANIM_TO_FRAME:int=3;

        private var mapDefaultStartStates:flash.utils.Dictionary;

        private var finishState:String=null;

        private var listActions:__AS3__.vec.Vector.<Action>;

        private var curState:String=null;

        private var externalVisible:Boolean=true;

        private var internalVisible:Boolean=true;

        public var isAnimPlaying:Boolean=false;

        protected var curAction:Action=null;

        private var currentLabelsCache:flash.utils.Dictionary;
    }
}


class Action extends Object
{
    public function Action(arg1:int)
    {
        super();
        this.type = arg1;
        return;
    }

    public var type:int;

    public var label:Object;

    public var fromFrame:int;

    public var toFrame:int;

    public var setInvisibleOnEnd:Boolean=false;
}