package lesta.unbound.expression 
{
    import __AS3__.vec.*;
    
    public class UbASTNodeHash extends Object implements lesta.unbound.expression.IUbASTNode
    {
        public function UbASTNodeHash(arg1:__AS3__.vec.Vector.<lesta.unbound.expression.UbASTHashPair>)
        {
            super();
            this.items = arg1;
            return;
        }

        public function get astType():int
        {
            return lesta.unbound.expression.UbASTNodeType.HASH;
        }

        public var items:__AS3__.vec.Vector.<lesta.unbound.expression.UbASTHashPair>;
    }
}
