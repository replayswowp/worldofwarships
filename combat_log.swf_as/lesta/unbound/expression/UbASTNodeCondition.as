package lesta.unbound.expression 
{
    public class UbASTNodeCondition extends Object implements lesta.unbound.expression.IUbASTNode
    {
        public function UbASTNodeCondition(arg1:lesta.unbound.expression.IUbASTNode, arg2:lesta.unbound.expression.IUbASTNode, arg3:lesta.unbound.expression.IUbASTNode)
        {
            super();
            this.condition = arg1;
            this.trueChoice = arg2;
            this.falseChoice = arg3;
            return;
        }

        public function get astType():int
        {
            return lesta.unbound.expression.UbASTNodeType.CONDITION;
        }

        public var condition:lesta.unbound.expression.IUbASTNode;

        public var trueChoice:lesta.unbound.expression.IUbASTNode;

        public var falseChoice:lesta.unbound.expression.IUbASTNode;
    }
}
