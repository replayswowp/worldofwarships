package lesta.unbound.expression 
{
    public class UbASTNodeGetProperty extends Object implements lesta.unbound.expression.IUbASTNode
    {
        public function UbASTNodeGetProperty(arg1:lesta.unbound.expression.IUbASTNode, arg2:lesta.unbound.expression.IUbASTNode)
        {
            super();
            this.source = arg1;
            this.property = arg2;
            return;
        }

        public function get astType():int
        {
            return lesta.unbound.expression.UbASTNodeType.GET_PROPERTY;
        }

        public var source:lesta.unbound.expression.IUbASTNode;

        public var property:lesta.unbound.expression.IUbASTNode;
    }
}
