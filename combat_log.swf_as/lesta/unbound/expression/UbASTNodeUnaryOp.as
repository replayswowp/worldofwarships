package lesta.unbound.expression 
{
    public class UbASTNodeUnaryOp extends Object implements lesta.unbound.expression.IUbASTNode
    {
        public function UbASTNodeUnaryOp(arg1:String, arg2:lesta.unbound.expression.IUbASTNode)
        {
            super();
            this.operator = arg1;
            this.operand = arg2;
            return;
        }

        public function get astType():int
        {
            return lesta.unbound.expression.UbASTNodeType.UNARY_OPERATION;
        }

        public var operator:String;

        public var operand:lesta.unbound.expression.IUbASTNode;
    }
}
