package lesta.unbound.expression 
{
    import lesta.utils.*;
    
    public class UbASTUtils extends Object
    {
        public function UbASTUtils()
        {
            super();
            return;
        }

        public static function dump(arg1:int, ... rest):void
        {
            var loc1:*="";
            var loc2:*=0;
            while (loc2 < arg1) 
            {
                loc1 = loc1.concat("    ");
                ++loc2;
            }
            rest.unshift(loc1);
            lesta.utils.Debug.trace(rest.join());
            return;
        }
    }
}
