package lesta.unbound.style 
{
    import fl.motion.*;
    import flash.filters.*;
    import flash.utils.*;
    import lesta.unbound.expression.*;
    
    public class UbStyleParser extends Object
    {
        public function UbStyleParser()
        {
            super();
            var loc1:*=this.numberStyleProcessor;
            var loc2:*=this.constructor(flash.filters.DropShadowFilter, this.spaceSeparatedProcessor(loc1, loc1, this.hexStyleProcessor, loc1, loc1, loc1, loc1, loc1));
            var loc3:*=this.constructor(flash.filters.GlowFilter, this.spaceSeparatedProcessor(this.hexStyleProcessor, loc1, loc1, loc1, loc1, loc1));
            var loc4:*=this.constructor(flash.filters.BlurFilter, this.spaceSeparatedProcessor(loc1, loc1, loc1));
            var loc5:*=this.constructor(flash.filters.ColorMatrixFilter, this.adjustColorProcessor);
            this.styleFragmentProcessors = {"width":this.dimensionStyleProcessor, "height":this.dimensionStyleProcessor, "backgroundImage":this.stringStyleProcessor, "backgroundStretchX":this.booleanStyleProcessor, "backgroundStretchY":this.booleanStyleProcessor, "textColor":this.hexStyleProcessor, "backgroundColor":this.hexStyleProcessor, "flow":this.enumStyleProcessor({"vertical":lesta.unbound.style.UbStyle.FLOW_VERTICAL, "horizontal":lesta.unbound.style.UbStyle.FLOW_HORIZONTAL, "htile":lesta.unbound.style.UbStyle.FLOW_TILE_HORIZONTAL, "vtile":lesta.unbound.style.UbStyle.FLOW_TILE_VERTICAL}), "top":this.dimensionStyleProcessor, "left":this.dimensionStyleProcessor, "bottom":this.dimensionStyleProcessor, "right":this.dimensionStyleProcessor, "marginTop":this.dimensionStyleProcessor, "marginLeft":this.dimensionStyleProcessor, "marginBottom":this.dimensionStyleProcessor, "marginRight":this.dimensionStyleProcessor, "position":this.enumStyleProcessor({"absolute":lesta.unbound.style.UbStyle.POSITION_ABSOLUTE, "relative":lesta.unbound.style.UbStyle.POSITION_FLOW_HORIZONTAL}), "text":this.stringStyleProcessor, "align":this.stringStyleProcessor, "backgroundRepeatX":this.booleanStyleProcessor, "backgroundRepeatY":this.booleanStyleProcessor, "fontFamily":this.stringStyleProcessor, "fontSize":this.fontSizeStyleProcessor, "userData":this.objectProcessor, "styleSheet":this.stringStyleProcessor, "textAlign":this.enumStyleProcessor({"left":lesta.unbound.style.UbStyle.ALIGN_LEFT, "right":lesta.unbound.style.UbStyle.ALIGN_RIGHT, "center":lesta.unbound.style.UbStyle.ALIGN_CENTER, "justify":lesta.unbound.style.UbStyle.ALIGN_JUSTIFY}), "alpha":this.numberStyleProcessor, "overflow":this.enumStyleProcessor({"visible":lesta.unbound.style.UbStyle.OVERFLOW_VISIBLE, "hidden":lesta.unbound.style.UbStyle.OVERFLOW_HIDDEN, "scroll":lesta.unbound.style.UbStyle.OVERFLOW_SCROLL}), "scrollbar":this.classFromStringProcessor, "scrollbarController":this.classFromStringProcessor, "scrollArea":this.classFromStringProcessor, "scrollbarTrackMode":this.stringStyleProcessor, "wheelScrollSpeed":this.numberStyleProcessor, "dropShadowFilter":loc2, "glowFilter":loc3, "blurFilter":loc4, "colorMatrixFilter":loc5, "mouseChildren":this.booleanStyleProcessor, "mouseEnabled":this.booleanStyleProcessor, "selectable":this.booleanStyleProcessor, "backgroundSize":this.enumStyleProcessor({"cover":lesta.unbound.style.UbStyle.BACKGROUND_SIZE_COVER}), "widthByChildren":this.booleanStyleProcessor, "heightByChildren":this.booleanStyleProcessor, "maxWidth":this.numberStyleProcessor, "minWidth":this.numberStyleProcessor, "maxHeight":this.numberStyleProcessor, "minHeight":this.numberStyleProcessor, "hitTest":this.booleanStyleProcessor, "leading":this.numberStyleProcessor, "letterSpacing":this.numberStyleProcessor, "measureEnabled":this.booleanStyleProcessor};
            return;
        }

        public function fini():void
        {
            if (this._compiler) 
            {
                this._compiler.fini();
                this._compiler = null;
            }
            this.styleFragmentProcessors = null;
            return;
        }

        public function parseXml(arg1:XML):Object
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*={};
            var loc4:*=0;
            var loc5:*=arg1.children();
            for each (loc2 in loc5) 
            {
                if ((loc3 = loc2.name()) in this.styleFragmentProcessors) 
                {
                    loc1[loc3] = this.process(loc3, loc2.attribute("value"), loc1);
                    continue;
                }
                trace("Unrecognized style property -- " + loc3 + " \n at: \n" + arg1.toString());
            }
            return loc1;
        }

        public function process(arg1:String, arg2:*, arg3:*):*
        {
            var loc1:*;
            return (loc1 = this.styleFragmentProcessors)[arg1](arg1, arg2, arg3);
        }

        internal function booleanStyleProcessor(arg1:String, arg2:String, arg3:Object):Boolean
        {
            return arg2 == "true";
        }

        internal function numberStyleProcessor(arg1:String, arg2:String, arg3:Object):Number
        {
            return Number(arg2);
        }

        internal function dimensionStyleProcessor(arg1:String, arg2:String, arg3:Object):Number
        {
            var loc1:*=this.extractNumberFromText(arg2);
            var loc2:*=this.computeDimensionMode(arg2);
            arg3[arg1 + "Mode"] = loc2;
            if (loc2 == lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
            {
                return 0;
            }
            if (loc2 == lesta.unbound.style.UbStyle.DIMENSION_FILL && isNaN(loc1)) 
            {
                return 100;
            }
            return loc1;
        }

        internal function fontSizeStyleProcessor(arg1:String, arg2:String, arg3:Object):Number
        {
            var loc1:*=this.computeDimensionMode(arg2, [lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH, lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT, lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE]);
            arg3[arg1 + "Mode"] = loc1;
            return this.extractNumberFromText(arg2);
        }

        internal function computeDimensionMode(arg1:String, arg2:Array=null):int
        {
            var loc1:*=0;
            if (arg1.indexOf("auto") >= 0) 
            {
                loc1 = lesta.unbound.style.UbStyle.DIMENSION_AUTO;
            }
            else if (arg1.indexOf("f") >= 0) 
            {
                loc1 = lesta.unbound.style.UbStyle.DIMENSION_FILL;
            }
            else if (arg1.indexOf("%") >= 0) 
            {
                loc1 = lesta.unbound.style.UbStyle.DIMENSION_RELATIVE;
            }
            else if (arg1.indexOf("sw") >= 0) 
            {
                loc1 = lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH;
            }
            else if (arg1.indexOf("sh") >= 0) 
            {
                loc1 = lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT;
            }
            else 
            {
                loc1 = lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE;
            }
            if (arg2 && arg2.indexOf(loc1) == -1) 
            {
                throw new Error("Forbidden dimension mode for value: " + arg1);
            }
            return loc1;
        }

        internal function extractNumberFromText(arg1:String):Number
        {
            var loc4:*=null;
            var loc1:*=[];
            var loc2:*=0;
            var loc3:*=arg1.length;
            while (loc2 < loc3) 
            {
                if ((loc4 = arg1.charAt(loc2)) == "." || loc4 == "-" || loc4 >= "0" && loc4 <= "9") 
                {
                    loc1.push(loc4);
                }
                ++loc2;
            }
            if (loc1.length == 0) 
            {
                return undefined;
            }
            return Number(loc1.join(""));
        }

        internal function stringStyleProcessor(arg1:String, arg2:String, arg3:Object):String
        {
            return arg2;
        }

        internal function classFromStringProcessor(arg1:String, arg2:String, arg3:Object):Class
        {
            return flash.utils.getDefinitionByName(arg2) as Class;
        }

        internal function hexStyleProcessor(arg1:String, arg2:String, arg3:Object):uint
        {
            return uint(arg2);
        }

        internal function enumStyleProcessor(arg1:Object):Function
        {
            var enum:Object;

            var loc1:*;
            enum = arg1;
            return function (arg1:String, arg2:String, arg3:Object):Object
            {
                if (arg2 in enum) 
                {
                    return enum[arg2];
                }
                throw new Error("unknown value " + arg2 + " for property " + arg1);
            }
        }

        internal function spaceSeparatedProcessor(... rest):Function
        {
            var processors:Array;

            var loc1:*;
            processors = rest;
            return function (arg1:String, arg2:String, arg3:Object):Array
            {
                var loc1:*;
                if ((loc1 = arg2.split(" ")).length > processors.length) 
                {
                    throw new Error("wrong arguments count for style property " + arg1);
                }
                var loc2:*=[];
                var loc3:*=0;
                var loc4:*=loc1.length;
                while (loc3 < loc4) 
                {
                    var loc5:*;
                    loc2.push((loc5 = processors)[loc3](arg1, loc1[loc3], arg3));
                    ++loc3;
                }
                return loc2;
            }
        }

        internal function adjustColorProcessor(arg1:String, arg2:String, arg3:Object):Array
        {
            var loc1:*=arg2.split(" ");
            var loc2:*;
            (loc2 = new fl.motion.AdjustColor()).contrast = Number(loc1.pop());
            loc2.brightness = Number(loc1.pop());
            loc2.saturation = Number(loc1.pop());
            loc2.hue = Number(loc1.pop());
            return [loc2.CalculateFinalFlatArray()];
        }

        internal function constructor(arg1:Class, arg2:Function):Function
        {
            var cls:Class;
            var processor:Function;

            var loc1:*;
            cls = arg1;
            processor = arg2;
            return function (arg1:String, arg2:String, arg3:Object):Object
            {
                var loc1:*=processor(arg1, arg2, arg3);
                var loc2:*=loc1.length;
                switch (loc2) 
                {
                    case 1:
                    {
                        return new cls(loc1[0]);
                    }
                    case 2:
                    {
                        return new cls(loc1[0], loc1[1]);
                    }
                    case 3:
                    {
                        return new cls(loc1[0], loc1[1], loc1[2]);
                    }
                    case 4:
                    {
                        return new cls(loc1[0], loc1[1], loc1[2], loc1[3]);
                    }
                    case 5:
                    {
                        return new cls(loc1[0], loc1[1], loc1[2], loc1[3], loc1[4]);
                    }
                    case 6:
                    {
                        return new cls(loc1[0], loc1[1], loc1[2], loc1[3], loc1[4], loc1[5]);
                    }
                    case 7:
                    {
                        return new cls(loc1[0], loc1[1], loc1[2], loc1[3], loc1[4], loc1[5], loc1[6]);
                    }
                    case 8:
                    {
                        return new cls(loc1[0], loc1[1], loc1[2], loc1[3], loc1[4], loc1[5], loc1[6], loc1[7]);
                    }
                    case 9:
                    {
                        return new cls(loc1[0], loc1[1], loc1[2], loc1[3], loc1[4], loc1[5], loc1[6], loc1[7], loc1[8]);
                    }
                    case 10:
                    {
                        return new cls(loc1[0], loc1[1], loc1[2], loc1[3], loc1[4], loc1[5], loc1[6], loc1[7], loc1[8], loc1[9]);
                    }
                    case 11:
                    {
                        return new cls(loc1[0], loc1[1], loc1[2], loc1[3], loc1[4], loc1[5], loc1[6], loc1[7], loc1[8], loc1[9], loc1[10]);
                    }
                    case 12:
                    {
                        return new cls(loc1[0], loc1[1], loc1[2], loc1[3], loc1[4], loc1[5], loc1[6], loc1[7], loc1[8], loc1[9], loc1[10], loc1[11]);
                    }
                    case 13:
                    {
                        return new cls(loc1[0], loc1[1], loc1[2], loc1[3], loc1[4], loc1[5], loc1[6], loc1[7], loc1[8], loc1[9], loc1[10], loc1[11], loc1[12]);
                    }
                    default:
                    {
                        return new cls();
                    }
                }
            }
        }

        internal function objectProcessor(arg1:String, arg2:String, arg3:Object):Object
        {
            return this.compiler.compile(arg2).eval({});
        }

        internal function get compiler():lesta.unbound.expression.UbExpressionCompiler
        {
            if (this._compiler == null) 
            {
                this._compiler = new lesta.unbound.expression.UbExpressionCompiler();
            }
            return this._compiler;
        }

        internal var _compiler:lesta.unbound.expression.UbExpressionCompiler;

        internal var styleFragmentProcessors:Object;
    }
}
