package lesta.unbound.style 
{
    import flash.filters.*;
    
    public class UbStyle extends Object
    {
        public function UbStyle()
        {
            this.dirty = DIRTY_MEASURE_X | DIRTY_MEASURE_Y | DIRTY_PAINT | DIRTY_REFLOW | DIRTY_POSITION | DIRTY_MOUSE | DIRTY_ALPHA | DIRTY_FILTERS | DIRTY_TEXT;
            super();
            return;
        }

        public function set colorMatrixFilter(arg1:flash.filters.ColorMatrixFilter):void
        {
            if (this._colorMatrixFilter == arg1) 
            {
                return;
            }
            this._colorMatrixFilter = arg1;
            this.dirty = this.dirty | DIRTY_FILTERS;
            return;
        }

        public function get fontFamily():String
        {
            return this._fontFamily;
        }

        public function set fontFamily(arg1:String):void
        {
            if (this._fontFamily == arg1) 
            {
                return;
            }
            this._fontFamily = arg1;
            this.dirty = this.dirty | DIRTY_TEXT;
            return;
        }

        public function get fontSize():Number
        {
            return this._fontSize;
        }

        public function set fontSize(arg1:Number):void
        {
            if (this._fontSize == arg1) 
            {
                return;
            }
            this._fontSize = arg1;
            this.dirty = this.dirty | DIRTY_TEXT;
            return;
        }

        public function get width():Number
        {
            return this._width;
        }

        public function set width(arg1:Number):void
        {
            if (this._width == arg1) 
            {
                return;
            }
            this._width = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_X;
            return;
        }

        public function get height():Number
        {
            return this._height;
        }

        public function set height(arg1:Number):void
        {
            if (this._height == arg1) 
            {
                return;
            }
            this._height = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_Y;
            return;
        }

        public function get widthMode():int
        {
            return this._widthMode;
        }

        public function set widthMode(arg1:int):void
        {
            if (this._widthMode == arg1) 
            {
                return;
            }
            this._widthMode = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_X;
            return;
        }

        public function get heightMode():int
        {
            return this._heightMode;
        }

        public function set heightMode(arg1:int):void
        {
            if (this._heightMode == arg1) 
            {
                return;
            }
            this._heightMode = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_Y;
            return;
        }

        public function get fontSizeMode():int
        {
            return this._fontSizeMode;
        }

        public function set fontSizeMode(arg1:int):void
        {
            if (this._fontSizeMode == arg1) 
            {
                return;
            }
            this._fontSizeMode = arg1;
            this.dirty = this.dirty | DIRTY_TEXT;
            return;
        }

        public function get textColor():uint
        {
            return this._textColor;
        }

        public function set textColor(arg1:uint):void
        {
            if (this._textColor == arg1) 
            {
                return;
            }
            this._textColor = arg1;
            this.dirty = this.dirty | DIRTY_TEXT;
            return;
        }

        public function get minWidth():Number
        {
            return this._minWidth;
        }

        public function set minWidth(arg1:Number):void
        {
            if (this._minWidth == arg1) 
            {
                return;
            }
            this._minWidth = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_X;
            return;
        }

        public function get maxWidth():Number
        {
            return this._maxWidth;
        }

        public function set maxWidth(arg1:Number):void
        {
            if (this._maxWidth == arg1) 
            {
                return;
            }
            this._maxWidth = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_X;
            return;
        }

        public function get minHeight():Number
        {
            return this._minHeight;
        }

        public function set minHeight(arg1:Number):void
        {
            if (this._minHeight == arg1) 
            {
                return;
            }
            this._minHeight = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_Y;
            return;
        }

        public function get maxHeight():Number
        {
            return this._maxHeight;
        }

        public function set maxHeight(arg1:Number):void
        {
            if (this._maxHeight == arg1) 
            {
                return;
            }
            this._maxHeight = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_Y;
            return;
        }

        public function get textAlign():String
        {
            return this._textAlign;
        }

        public function set textAlign(arg1:String):void
        {
            if (this._textAlign == arg1) 
            {
                return;
            }
            this._textAlign = arg1;
            this.dirty = this.dirty | DIRTY_TEXT;
            return;
        }

        public function get widthByChildren():Boolean
        {
            return this._widthByChildren;
        }

        public function set widthByChildren(arg1:Boolean):void
        {
            if (this._widthByChildren == arg1) 
            {
                return;
            }
            this._widthByChildren = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_X;
            return;
        }

        public function get heightByChildren():Boolean
        {
            return this._heightByChildren;
        }

        public function set heightByChildren(arg1:Boolean):void
        {
            if (this._heightByChildren == arg1) 
            {
                return;
            }
            this._heightByChildren = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_Y;
            return;
        }

        public function get leading():Number
        {
            return this._leading;
        }

        public function get align():String
        {
            return this._align;
        }

        public function set align(arg1:String):void
        {
            this._align = arg1;
            this.dirty = this.dirty | DIRTY_REFLOW | DIRTY_MEASURE_X | DIRTY_MEASURE_Y;
            return;
        }

        public function set leading(arg1:Number):void
        {
            if (this._leading == arg1) 
            {
                return;
            }
            this._leading = arg1;
            this.dirty = this.dirty | DIRTY_TEXT;
            return;
        }

        public function get styleSheet():String
        {
            return this._styleSheet;
        }

        public function get scaleX():Number
        {
            return this._scaleX;
        }

        public function set scaleX(arg1:Number):void
        {
            if (this._scaleX == arg1) 
            {
                return;
            }
            this._scaleX = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_X;
            return;
        }

        public function get scaleY():Number
        {
            return this._scaleY;
        }

        public function set scaleY(arg1:Number):void
        {
            if (this._scaleY == arg1) 
            {
                return;
            }
            this._scaleY = arg1;
            this.dirty = this.dirty | DIRTY_MEASURE_Y;
            return;
        }

        public function set styleSheet(arg1:String):void
        {
            if (this._styleSheet == arg1) 
            {
                return;
            }
            this._styleSheet = arg1;
            this.dirty = this.dirty | DIRTY_STYLESHEET;
            return;
        }

        public function get letterSpacing():Number
        {
            return this._letterSpacing;
        }

        public function set letterSpacing(arg1:Number):void
        {
            if (this._letterSpacing == arg1) 
            {
                return;
            }
            this._letterSpacing = arg1;
            this.dirty = this.dirty | DIRTY_TEXT;
            return;
        }

        public function get mouseEnabled():Boolean
        {
            return this._mouseEnabled;
        }

        public function set mouseEnabled(arg1:Boolean):void
        {
            if (this._mouseEnabled == arg1) 
            {
                return;
            }
            this._mouseEnabled = arg1;
            this.dirty = this.dirty | DIRTY_MOUSE;
            return;
        }

        public function get mouseChildren():Boolean
        {
            return this._mouseChildren;
        }

        public function set mouseChildren(arg1:Boolean):void
        {
            if (this._mouseChildren == arg1) 
            {
                return;
            }
            this._mouseChildren = arg1;
            this.dirty = this.dirty | DIRTY_MOUSE;
            return;
        }

        public function get selectable():Boolean
        {
            return this._selectable;
        }

        public function get top():Number
        {
            return this._top;
        }

        public function set top(arg1:Number):void
        {
            if (this._top == arg1) 
            {
                return;
            }
            this._top = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get bottom():Number
        {
            return this._bottom;
        }

        public function set bottom(arg1:Number):void
        {
            if (this._bottom == arg1) 
            {
                return;
            }
            this._bottom = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get left():Number
        {
            return this._left;
        }

        public function set left(arg1:Number):void
        {
            if (this._left == arg1) 
            {
                return;
            }
            this._left = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get right():Number
        {
            return this._right;
        }

        public function set right(arg1:Number):void
        {
            if (this._right == arg1) 
            {
                return;
            }
            this._right = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get topMode():int
        {
            return this._topMode;
        }

        public function set topMode(arg1:int):void
        {
            if (this._topMode == arg1) 
            {
                return;
            }
            this._topMode = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get bottomMode():int
        {
            return this._bottomMode;
        }

        public function set bottomMode(arg1:int):void
        {
            if (this._bottomMode == arg1) 
            {
                return;
            }
            this._bottomMode = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get leftMode():int
        {
            return this._leftMode;
        }

        public function set leftMode(arg1:int):void
        {
            if (this._leftMode == arg1) 
            {
                return;
            }
            this._leftMode = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get rightMode():int
        {
            return this._rightMode;
        }

        public function set rightMode(arg1:int):void
        {
            if (this._rightMode == arg1) 
            {
                return;
            }
            this._rightMode = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function set selectable(arg1:Boolean):void
        {
            if (this._selectable == arg1) 
            {
                return;
            }
            this._selectable = arg1;
            this.dirty = this.dirty | DIRTY_MOUSE;
            return;
        }

        public function get hitTest():Boolean
        {
            return this._hitTest;
        }

        public function set hitTest(arg1:Boolean):void
        {
            if (this._hitTest == arg1) 
            {
                return;
            }
            this._hitTest = arg1;
            this.dirty = this.dirty | DIRTY_MOUSE;
            return;
        }

        public function get scrollTop():Number
        {
            return this._scrollTop;
        }

        public function set scrollTop(arg1:Number):void
        {
            this._scrollTop = arg1;
            return;
        }

        public function get scrollLeft():Number
        {
            return this._scrollLeft;
        }

        public function set scrollLeft(arg1:Number):void
        {
            this._scrollLeft = arg1;
            return;
        }

        public function get scrollPosition():Number
        {
            if (this._flow == FLOW_HORIZONTAL || this.flow == FLOW_TILE_VERTICAL) 
            {
                return this._scrollLeft;
            }
            return this._scrollTop;
        }

        public function get marginTop():Number
        {
            return this._marginTop;
        }

        public function set marginTop(arg1:Number):void
        {
            if (this._marginTop == arg1) 
            {
                return;
            }
            this._marginTop = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get marginBottom():Number
        {
            return this._marginBottom;
        }

        public function set marginBottom(arg1:Number):void
        {
            if (this._marginBottom == arg1) 
            {
                return;
            }
            this._marginBottom = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get marginLeft():Number
        {
            return this._marginLeft;
        }

        public function set marginLeft(arg1:Number):void
        {
            if (this._marginLeft == arg1) 
            {
                return;
            }
            this._marginLeft = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get marginRight():Number
        {
            return this._marginRight;
        }

        public function set marginRight(arg1:Number):void
        {
            if (this._marginRight == arg1) 
            {
                return;
            }
            this._marginRight = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get marginTopMode():int
        {
            return this._marginTopMode;
        }

        public function set marginTopMode(arg1:int):void
        {
            if (this._marginTopMode == arg1) 
            {
                return;
            }
            this._marginTopMode = arg1;
            this.dirty = this.dirty | DIRTY_POSITION | DIRTY_MEASURE_Y;
            return;
        }

        public function get marginBottomMode():int
        {
            return this._marginBottomMode;
        }

        public function set marginBottomMode(arg1:int):void
        {
            if (this._marginBottomMode == arg1) 
            {
                return;
            }
            this._marginBottomMode = arg1;
            this.dirty = this.dirty | DIRTY_POSITION | DIRTY_MEASURE_Y;
            return;
        }

        public function get marginLeftMode():int
        {
            return this._marginLeftMode;
        }

        public function set marginLeftMode(arg1:int):void
        {
            if (this._marginLeftMode == arg1) 
            {
                return;
            }
            this._marginLeftMode = arg1;
            this.dirty = this.dirty | DIRTY_POSITION | DIRTY_MEASURE_X;
            return;
        }

        public function get marginRightMode():int
        {
            return this._marginRightMode;
        }

        public function set marginRightMode(arg1:int):void
        {
            if (this._marginRightMode == arg1) 
            {
                return;
            }
            this._marginRightMode = arg1;
            this.dirty = this.dirty | DIRTY_POSITION | DIRTY_MEASURE_X;
            return;
        }

        public function set scrollPosition(arg1:Number):void
        {
            if (this._flow == FLOW_HORIZONTAL || this.flow == FLOW_TILE_VERTICAL) 
            {
                this._scrollLeft = arg1;
            }
            else 
            {
                this._scrollTop = arg1;
            }
            return;
        }

        public function get position():int
        {
            return this._position;
        }

        public function set position(arg1:int):void
        {
            if (this._position == arg1) 
            {
                return;
            }
            this._position = arg1;
            this.dirty = this.dirty | DIRTY_POSITION;
            return;
        }

        public function get scrollbar():Class
        {
            return this._scrollbarClass;
        }

        public function get flow():int
        {
            return this._flow;
        }

        public function set flow(arg1:int):void
        {
            if (this._flow == arg1) 
            {
                return;
            }
            this._flow = arg1;
            this.dirty = this.dirty | DIRTY_REFLOW | DIRTY_MEASURE_X | DIRTY_MEASURE_Y;
            return;
        }

        public function set scrollbar(arg1:Class):void
        {
            this._scrollbarClass = arg1;
            return;
        }

        public function get scrollbarController():Class
        {
            return this._scrollbarController;
        }

        public function set scrollbarController(arg1:Class):void
        {
            this._scrollbarController = arg1;
            return;
        }

        public function set backgroundImage(arg1:String):void
        {
            if (this._backgroundImageValueCache == arg1) 
            {
                return;
            }
            this._backgroundImageValueCache = arg1;
            var loc1:*=arg1.split(":");
            this._backgroundImageOriginatesFromLibrary = loc1[0] == "symbol";
            this._backgroundImageId = loc1[1];
            this.dirty = this.dirty | DIRTY_PAINT;
            return;
        }

        public function get backgroundImage():String
        {
            return this._backgroundImageId;
        }

        public function get backgroundImageOriginatesFromLibrary():Boolean
        {
            return this._backgroundImageOriginatesFromLibrary;
        }

        public function get scrollArea():Class
        {
            return this._scrollArea;
        }

        public function set backgroundColor(arg1:uint):void
        {
            if (this._backgroundColor == arg1) 
            {
                return;
            }
            this._backgroundColor = arg1;
            this.dirty = this.dirty | DIRTY_PAINT;
            return;
        }

        public function get backgroundColor():uint
        {
            return this._backgroundColor;
        }

        public function set scrollArea(arg1:Class):void
        {
            this._scrollArea = arg1;
            return;
        }

        public function get scrollbarTrackMode():String
        {
            return this._scrollbarTrackMode;
        }

        public function set scrollbarTrackMode(arg1:String):void
        {
            this._scrollbarTrackMode = arg1;
            return;
        }

        public function get wheelScrollSpeed():Number
        {
            return this._wheelScrollSpeed;
        }

        public function set wheelScrollSpeed(arg1:Number):void
        {
            this._wheelScrollSpeed = arg1;
            return;
        }

        public function set backgroundStretchX(arg1:Boolean):void
        {
            if (this._backgroundStretchX == arg1) 
            {
                return;
            }
            this._backgroundStretchX = arg1;
            this.dirty = this.dirty | DIRTY_PAINT;
            return;
        }

        public function get backgroundStretchX():Boolean
        {
            return this._backgroundStretchX;
        }

        public function set backgroundStretchY(arg1:Boolean):void
        {
            if (this._backgroundStretchY == arg1) 
            {
                return;
            }
            this._backgroundStretchY = arg1;
            this.dirty = this.dirty | DIRTY_PAINT;
            return;
        }

        public function get backgroundStretchY():Boolean
        {
            return this._backgroundStretchY;
        }

        public function set backgroundRepeatX(arg1:Boolean):void
        {
            if (this._backgroundRepeatX == arg1) 
            {
                return;
            }
            this._backgroundRepeatX = arg1;
            this.dirty = this.dirty | DIRTY_PAINT;
            return;
        }

        public function get backgroundRepeatX():Boolean
        {
            return this._backgroundRepeatX;
        }

        public function set backgroundRepeatY(arg1:Boolean):void
        {
            if (this._backgroundRepeatY == arg1) 
            {
                return;
            }
            this._backgroundRepeatY = arg1;
            this.dirty = this.dirty | DIRTY_PAINT;
            return;
        }

        public function get backgroundRepeatY():Boolean
        {
            return this._backgroundRepeatY;
        }

        public function set backgroundSize(arg1:int):void
        {
            if (this._backgroundSize == arg1) 
            {
                return;
            }
            this._backgroundSize = arg1;
            this.dirty = this.dirty | DIRTY_PAINT;
            return;
        }

        public function get backgroundSize():int
        {
            return this._backgroundSize;
        }

        public function get alpha():Number
        {
            return this._alpha;
        }

        public function set alpha(arg1:Number):void
        {
            if (this._alpha == arg1) 
            {
                return;
            }
            this._alpha = arg1;
            this.dirty = this.dirty | DIRTY_ALPHA;
            return;
        }

        public function get overflow():int
        {
            return this._overflow;
        }

        public function set overflow(arg1:int):void
        {
            if (this._overflow == arg1) 
            {
                return;
            }
            this._overflow = arg1;
            this.dirty = this.dirty | DIRTY_PAINT;
            return;
        }

        public function get dropShadowFilter():flash.filters.DropShadowFilter
        {
            return this._dropShadowFilter;
        }

        public function set dropShadowFilter(arg1:flash.filters.DropShadowFilter):void
        {
            if (this._dropShadowFilter == arg1) 
            {
                return;
            }
            this._dropShadowFilter = arg1;
            this.dirty = this.dirty | DIRTY_FILTERS;
            return;
        }

        public function get blurFilter():flash.filters.BlurFilter
        {
            return this._blurFilter;
        }

        public function set blurFilter(arg1:flash.filters.BlurFilter):void
        {
            if (this._blurFilter == arg1) 
            {
                return;
            }
            this._blurFilter = arg1;
            this.dirty = this.dirty | DIRTY_FILTERS;
            return;
        }

        public function get glowFilter():flash.filters.GlowFilter
        {
            return this._glowFilter;
        }

        public function set glowFilter(arg1:flash.filters.GlowFilter):void
        {
            if (this._glowFilter == arg1) 
            {
                return;
            }
            this._glowFilter = arg1;
            this.dirty = this.dirty | DIRTY_FILTERS;
            return;
        }

        public function get colorMatrixFilter():flash.filters.ColorMatrixFilter
        {
            return this._colorMatrixFilter;
        }

        public static const DIRTY_PAINT:uint=4;

        public static const DIMENSION_AUTO:int=0;

        public static const ALIGN_LEFT:String="left";

        public static const ALIGN_RIGHT:String="right";

        public static const ALIGN_CENTER:String="center";

        public static const ALIGN_JUSTIFY:String="justify";

        public static const BACKGROUND_SIZE_COVER:int=1;

        public static const DIMENSION_RELATIVE:int=1;

        public static const DIMENSION_ABSOLUTE:int=2;

        public static const DIMENSION_FILL:int=3;

        public static const DIMENSION_RELATIVE_TO_STAGE_WIDTH:int=4;

        public static const DIMENSION_RELATIVE_TO_STAGE_HEIGHT:int=5;

        public static const ISOLATED_DIMENSIONS:Array=[DIMENSION_AUTO, DIMENSION_ABSOLUTE];

        public static const POSITION_ABSOLUTE:int=0;

        public static const POSITION_FLOW_VERTICAL:int=1;

        public static const POSITION_FLOW_HORIZONTAL:int=2;

        public static const FLOW_HORIZONTAL:int=0;

        public static const FLOW_VERTICAL:int=1;

        public static const FLOW_TILE_HORIZONTAL:int=2;

        public static const FLOW_TILE_VERTICAL:int=3;

        public static const DIRTY_MEASURE_X:uint=1;

        public static const DIRTY_REFLOW:uint=2;

        public static const DIRTY_POSITION:uint=8;

        public static const DIRTY_MEASURE_Y:uint=16;

        public static const DIRTY_MOUSE:uint=32;

        public static const DIRTY_ALPHA:uint=64;

        public static const DIRTY_FILTERS:uint=128;

        public static const DIRTY_TEXT:uint=256;

        public static const DIRTY_STYLESHEET:uint=512;

        public static const OVERFLOW_VISIBLE:int=0;

        public static const OVERFLOW_SCROLL:int=1;

        public static const OVERFLOW_HIDDEN:int=2;

        public static const BACKGROUND_SIZE_NOT_SET:int=0;

        internal var _backgroundImageId:String="";

        internal var _backgroundImageValueCache:String="";

        internal var _backgroundImageOriginatesFromLibrary:Boolean=true;

        internal var _backgroundColor:uint=0;

        internal var _backgroundStretchX:Boolean=false;

        internal var _backgroundStretchY:Boolean=false;

        internal var _backgroundRepeatX:Boolean=false;

        internal var _backgroundRepeatY:Boolean=false;

        internal var _backgroundSize:int=0;

        internal var _fontSizeMode:int=2;

        internal var _textColor:uint=0;

        internal var _scrollTop:Number=0;

        internal var _wheelScrollSpeed:Number=0;

        internal var _scrollbarClass:Class;

        internal var _scrollbarController:Class;

        internal var _scrollArea:Class;

        internal var _scrollbarTrackMode:String;

        internal var _scaleX:Number=1;

        internal var _leading:Number=0;

        internal var _styleSheet:String;

        internal var _letterSpacing:Number=0;

        internal var _glowFilter:flash.filters.GlowFilter;

        internal var _overflow:int=0;

        internal var _mouseEnabled:Boolean=true;

        internal var _mouseChildren:Boolean=true;

        internal var _selectable:Boolean=false;

        internal var _hitTest:Boolean=true;

        internal var _blurFilter:flash.filters.BlurFilter;

        internal var _colorMatrixFilter:flash.filters.ColorMatrixFilter;

        internal var _dropShadowFilter:flash.filters.DropShadowFilter;

        internal var _alpha:Number=NaN;

        internal var _fontFamily:String="";

        internal var _fontSize:Number=16;

        public var userData:Object;

        public var measureEnabled:Boolean=true;

        public var dirty:uint;

        internal var _width:Number=0;

        internal var _height:Number=0;

        internal var _widthMode:int=0;

        internal var _heightMode:int=0;

        internal var _minWidth:Number=0;

        internal var _maxWidth:Number=Infinity;

        internal var _minHeight:Number=0;

        internal var _maxHeight:Number=Infinity;

        internal var _widthByChildren:Boolean=false;

        internal var _heightByChildren:Boolean=false;

        internal var _align:String="left";

        internal var _scaleY:Number=1;

        internal var _top:Number=0;

        internal var _bottom:Number=NaN;

        internal var _left:Number=0;

        internal var _right:Number=NaN;

        internal var _topMode:int=2;

        internal var _bottomMode:int=2;

        internal var _leftMode:int=2;

        internal var _textAlign:String="left";

        internal var _rightMode:int=2;

        internal var _marginTop:Number=0;

        internal var _marginBottom:Number=NaN;

        internal var _marginLeft:Number=0;

        internal var _marginRight:Number=NaN;

        internal var _marginTopMode:int=2;

        internal var _marginBottomMode:int=2;

        internal var _marginLeftMode:int=2;

        internal var _marginRightMode:int=2;

        internal var _position:int=1;

        internal var _flow:int=1;

        internal var _scrollLeft:Number=0;
    }
}
