package lesta.unbound.core 
{
    import flash.utils.*;
    
    public class UbInjector extends Object
    {
        public function UbInjector(arg1:Array=null)
        {
            var loc1:*=0;
            var loc2:*=0;
            super();
            this.instantiatedDependencies = new flash.utils.Dictionary();
            this.dependenciesCache = new flash.utils.Dictionary();
            this.preconstructedCache = new flash.utils.Dictionary();
            if (arg1 != null) 
            {
                loc1 = 0;
                loc2 = arg1.length;
                while (loc1 < loc2) 
                {
                    this.addPreconstructed(arg1[loc1]);
                    ++loc1;
                }
            }
            return;
        }

        public function addPreconstructed(arg1:Object):void
        {
            var loc1:*=flash.utils.getDefinitionByName(flash.utils.getQualifiedClassName(arg1)) as Class;
            var loc2:*;
            this.instantiatedDependencies[loc1] = loc2 = arg1;
            this.preconstructedCache[loc1] = loc2;
            return;
        }

        public function inject(arg1:Object):void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=0;
            var loc5:*=0;
            var loc6:*=null;
            var loc7:*=null;
            var loc8:*=null;
            var loc9:*=null;
            var loc10:*=null;
            var loc1:*=flash.utils.getQualifiedClassName(arg1);
            if (!(loc1 in this.dependenciesCache)) 
            {
                loc2 = lesta.unbound.core.UbReflector.getPublicVariables(loc1, arg1);
                loc3 = [];
                loc4 = 0;
                loc5 = loc2.length;
                while (loc4 < loc5) 
                {
                    loc6 = loc2[loc4];
                    if (!((loc7 = flash.utils.getDefinitionByName(loc6.type) as Class) in this.instantiatedDependencies)) 
                    {
                        this.instantiatedDependencies[loc7] = new loc7();
                        if (!(loc7 in this.preconstructedCache)) 
                        {
                            this.inject(this.instantiatedDependencies[loc7]);
                        }
                    }
                    loc3.push([loc6.name, this.instantiatedDependencies[loc7]]);
                    ++loc4;
                }
                this.dependenciesCache[loc1] = loc3;
            }
            loc3 = this.dependenciesCache[loc1];
            loc4 = 0;
            loc5 = loc3.length;
            while (loc4 < loc5) 
            {
                loc9 = (loc8 = loc3[loc4])[0];
                loc10 = loc8[1];
                arg1[loc9] = loc10;
                ++loc4;
            }
            return;
        }

        internal var instantiatedDependencies:flash.utils.Dictionary;

        internal var dependenciesCache:flash.utils.Dictionary;

        internal var preconstructedCache:flash.utils.Dictionary;
    }
}
