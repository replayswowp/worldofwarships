package lesta.unbound.core 
{
    import flash.utils.*;
    import lesta.unbound.expression.*;
    
    public class UbScopeSubscriptionHelper extends Object
    {
        public function UbScopeSubscriptionHelper()
        {
            super();
            return;
        }

        public function subscribeMe(arg1:lesta.unbound.core.UbScope, arg2:lesta.unbound.expression.IUbExpression, arg3:Function):void
        {
            var loc3:*=null;
            var loc1:*=new flash.utils.Dictionary(true);
            var loc2:*=arg2.requestedProps;
            var loc4:*=0;
            var loc5:*=loc2;
            for (loc3 in loc5) 
            {
                loc1[loc3] = loc2[loc3];
            }
            this.doSubscribe(arg1, loc1, arg3);
            return;
        }

        internal function doSubscribe(arg1:lesta.unbound.core.UbScope, arg2:flash.utils.Dictionary, arg3:Function):void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=0;
            var loc4:*=arg2;
            for (loc1 in loc4) 
            {
                if (lesta.unbound.expression.UbExpressionBase.specialContainerProps.indexOf(loc1) == -1) 
                {
                    arg1.on(loc1 + lesta.unbound.core.UbScope.CHANGED, arg3);
                    continue;
                }
                if (!((loc2 = arg2[loc1]) > 0)) 
                {
                    continue;
                }
                arg2[loc1] = (loc2 - 1);
                this.doSubscribe(arg1[loc1], arg2, arg3);
            }
            return;
        }

        public function unSubscribeMe(arg1:lesta.unbound.core.UbScope, arg2:lesta.unbound.expression.IUbExpression, arg3:Function):void
        {
            var loc3:*=null;
            var loc1:*=new flash.utils.Dictionary(true);
            var loc2:*=arg2.requestedProps;
            var loc4:*=0;
            var loc5:*=loc2;
            for (loc3 in loc5) 
            {
                loc1[loc3] = loc2[loc3];
            }
            this.doUnSubscribe(arg1, loc1, arg3);
            return;
        }

        internal function doUnSubscribe(arg1:lesta.unbound.core.UbScope, arg2:flash.utils.Dictionary, arg3:Function):void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=0;
            var loc4:*=arg2;
            for (loc1 in loc4) 
            {
                if (lesta.unbound.expression.UbExpressionBase.specialContainerProps.indexOf(loc1) == -1) 
                {
                    arg1.off(loc1 + lesta.unbound.core.UbScope.CHANGED, arg3);
                    continue;
                }
                if (!((loc2 = arg2[loc1]) > 0)) 
                {
                    continue;
                }
                arg2[loc1] = (loc2 - 1);
                this.doUnSubscribe(arg1[loc1], arg2, arg3);
            }
            return;
        }
    }
}
