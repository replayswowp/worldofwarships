package lesta.unbound.core 
{
    public class UbRootScope extends lesta.unbound.core.UbScope
    {
        public function UbRootScope(arg1:lesta.unbound.core.UbGlobalDefinitions)
        {
            super(arg1);
            return;
        }

        public override function bubbleEvent(arg1:String, arg2:Object):void
        {
            return;
        }

        protected override function free(arg1:Boolean=true):void
        {
            super.free(false);
            return;
        }
    }
}
