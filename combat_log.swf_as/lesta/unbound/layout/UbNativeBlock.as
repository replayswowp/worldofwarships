package lesta.unbound.layout 
{
    import flash.display.*;
    import flash.events.*;
    import lesta.unbound.interfaces.*;
    import lesta.unbound.style.*;
    
    public class UbNativeBlock extends lesta.unbound.layout.UbBlock
    {
        public function UbNativeBlock()
        {
            super();
            this.addEventListener(lesta.unbound.layout.UbNativeBlock.INVALIDATE, this.handleInvalidateEvent);
            return;
        }

        public override function free():void
        {
            this.removeEventListener(lesta.unbound.layout.UbNativeBlock.INVALIDATE, this.handleInvalidateEvent);
            super.free();
            if (this._nativeChild == null) 
            {
                return;
            }
            if (this._nativeChild is lesta.unbound.interfaces.IDestructable) 
            {
                (this._nativeChild as lesta.unbound.interfaces.IDestructable).fini();
            }
            this._nativeChild = null;
            return;
        }

        public override function getBindingsTarget(arg1:String):flash.display.DisplayObject
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=0;
            var loc5:*=null;
            if (arg1 == null) 
            {
                return this._nativeChild;
            }
            loc1 = arg1.split(".");
            loc2 = this._nativeChild as flash.display.DisplayObjectContainer;
            loc3 = 0;
            loc4 = loc1.length;
            while (loc3 < loc4) 
            {
                if ((loc5 = loc2.getChildByName(loc1[loc3])) == null) 
                {
                    throw new Error("Cannot find element " + arg1 + " for inner binding");
                }
                else 
                {
                    loc2 = loc5 as flash.display.DisplayObjectContainer;
                }
                ++loc3;
            }
            return loc5;
        }

        public function setNativeChild(arg1:flash.display.DisplayObject):flash.display.DisplayObject
        {
            if (this._nativeChild != null) 
            {
                removeChildAt(0);
            }
            this._nativeChild = addChild(arg1);
            dispatchUpdate();
            return arg1;
        }

        protected function handleInvalidateEvent(arg1:flash.events.Event):void
        {
            invalidateChildren();
            return;
        }

        protected override function measureX():void
        {
            if (!visible) 
            {
                return;
            }
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                if (computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
                {
                    doMeasureX();
                    this._nativeChild.width = _sourceWidth;
                    _sourceWidth = this._nativeChild.width;
                    recursiveMeasureX();
                }
                else if (computedStyle.widthByChildren) 
                {
                    recursiveMeasureX();
                    doMeasureX();
                }
                else 
                {
                    _sourceWidth = clampWidth(this._nativeChild.width);
                    recursiveMeasureX();
                }
            }
            else 
            {
                recursiveMeasureX();
            }
            return;
        }

        protected override function measureY():void
        {
            if (!visible) 
            {
                return;
            }
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
            {
                if (computedStyle.heightMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
                {
                    doMeasureY();
                    this._nativeChild.height = _sourceHeight;
                    _sourceHeight = this._nativeChild.height;
                    recursiveMeasureY();
                }
                else if (computedStyle.heightByChildren) 
                {
                    recursiveMeasureY();
                    doMeasureY();
                }
                else 
                {
                    _sourceHeight = clampHeight(this._nativeChild.height);
                    recursiveMeasureY();
                }
            }
            else 
            {
                recursiveMeasureY();
            }
            return;
        }

        protected override function reflowAndRepaint(arg1:Boolean=false):void
        {
            if (!visible || parent == null || !measureEnabled) 
            {
                return;
            }
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                this._nativeChild.width = _sourceWidth;
            }
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
            {
                this._nativeChild.height = _sourceHeight;
            }
            super.reflowAndRepaint(arg1);
            return;
        }

        protected override function updateMouse(arg1:Boolean, arg2:Boolean, arg3:Boolean):void
        {
            super.updateMouse(arg1, arg2, arg3);
            var loc1:*;
            if ((loc1 = this._nativeChild as flash.display.InteractiveObject) != null) 
            {
                loc1.mouseEnabled = arg1;
            }
            return;
        }

        public static const INVALIDATE:String="UbNativeBlock.INVALIDATE";

        internal var _nativeChild:flash.display.DisplayObject;
    }
}
