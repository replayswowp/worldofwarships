package lesta.unbound.layout 
{
    import flash.display.*;
    import flash.events.*;
    import lesta.unbound.core.*;
    import lesta.unbound.style.*;
    
    public class UbStyleable extends flash.display.Sprite
    {
        public function UbStyleable()
        {
            this.computedStyle = new lesta.unbound.style.UbStyle();
            super();
            addEventListener(ENABLE_MEASURE, this.onMeasureChange);
            addEventListener(DISABLE_MEASURE, this.onMeasureChange);
            return;
        }

        protected function getChildsHorizontalSize(arg1:lesta.unbound.layout.UbStyleable):Number
        {
            return arg1.visible ? arg1._sourceWidth + arg1.getMarginLeft() + arg1.getMarginRight() : 0;
        }

        protected function getChildsVerticalSize(arg1:lesta.unbound.layout.UbStyleable):Number
        {
            return arg1.visible ? arg1.getMarginBottom() + arg1._sourceHeight + arg1.getMarginTop() : 0;
        }

        protected function getMarginTop():Number
        {
            return this.computeDimensionValue(this.computedStyle.marginTop, this.computedStyle.marginTopMode, this._sourceHeight);
        }

        protected function getMarginBottom():Number
        {
            return isNaN(this.computedStyle.marginBottom) ? 0 : this.computeDimensionValue(this.computedStyle.marginBottom, this.computedStyle.marginBottomMode, this._sourceHeight);
        }

        protected function getMarginLeft():Number
        {
            return this.computeDimensionValue(this.computedStyle.marginLeft, this.computedStyle.marginLeftMode, this._sourceWidth);
        }

        protected function getMarginRight():Number
        {
            return isNaN(this.computedStyle.marginRight) ? 0 : this.computeDimensionValue(this.computedStyle.marginRight, this.computedStyle.marginRightMode, this._sourceWidth);
        }

        internal function invalidateStageRelativeDimensions():void
        {
            if (this.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || this.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT) 
            {
                this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X;
            }
            if (this.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || this.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT) 
            {
                this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
            }
            if (this.computedStyle.leftMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || this.computedStyle.leftMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT || this.computedStyle.rightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || this.computedStyle.rightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT) 
            {
                this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X;
            }
            if (this.computedStyle.topMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || this.computedStyle.topMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT || this.computedStyle.bottomMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || this.computedStyle.bottomMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT) 
            {
                this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
            }
            if (this.computedStyle.marginLeftMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || this.computedStyle.marginLeftMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT || this.computedStyle.marginRightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || this.computedStyle.marginRightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT) 
            {
                this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X;
            }
            if (this.computedStyle.marginTopMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || this.computedStyle.marginTopMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT || this.computedStyle.marginBottomMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || this.computedStyle.marginBottomMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT) 
            {
                this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
            }
            if (this.computedStyle.fontSizeMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || this.computedStyle.fontSizeMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT) 
            {
                this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_STYLESHEET;
            }
            return;
        }

        public function get measuredWidth():int
        {
            return this._sourceWidth * this.computedStyle.scaleX;
        }

        public function set measuredWidth(arg1:int):void
        {
            return;
        }

        public function get measuredHeight():int
        {
            return this._sourceHeight * this.computedStyle.scaleY;
        }

        protected function get measureEnabled():Boolean
        {
            return this._forceMasureEnabled || this.computedStyle.measureEnabled || !this._measuredOnce;
        }

        protected function onMeasureChange(arg1:flash.events.Event):void
        {
            this._forceMasureEnabled = arg1.type == ENABLE_MEASURE;
            this._measuredOnce = false;
            return;
        }

        protected function getStageWidth():Number
        {
            return lesta.unbound.core.UbHelpers.getRootBlock(this).style.width;
        }

        protected function getStageHeight():Number
        {
            return lesta.unbound.core.UbHelpers.getRootBlock(this).style.height;
        }

        protected function computeDimensionValue(arg1:Number, arg2:int, arg3:Number):Number
        {
            if (arg2 == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE) 
            {
                return arg1;
            }
            if (arg2 == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH) 
            {
                return arg1 * this.getStageWidth() * 0.01;
            }
            if (arg2 == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT) 
            {
                return arg1 * this.getStageHeight() * 0.01;
            }
            return arg1 * arg3 * 0.01;
        }

        public function get contentWidth():int
        {
            return this._contentWidth;
        }

        public function get contentHeight():int
        {
            return this._contentHeight;
        }

        protected function get isolatedPosition():Boolean
        {
            return this._isolatedPosition;
        }

        protected function get measuredAvailableWidth():Number
        {
            var loc1:*=NaN;
            if (this.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || this.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_FILL) 
            {
                loc1 = this.computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_FILL ? 1 : this.computedStyle.width / 100;
                return (parent as lesta.unbound.layout.UbStyleable).measuredAvailableWidth * loc1 - this._totalAbsoluteContentWidth;
            }
            return this._sourceWidth - this._totalAbsoluteContentWidth;
        }

        protected function get measuredAvailableHeight():Number
        {
            var loc1:*=NaN;
            if (this.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || this.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_FILL) 
            {
                loc1 = this.computedStyle.heightMode != lesta.unbound.style.UbStyle.DIMENSION_FILL ? 1 : this.computedStyle.height / 100;
                return (parent as lesta.unbound.layout.UbStyleable).measuredAvailableHeight * loc1 - this._totalAbsoluteContentHeight;
            }
            return this._sourceHeight - this._totalAbsoluteContentHeight;
        }

        public function setBaseStyle(arg1:Object, arg2:Object):void
        {
            this.styleClassesBase = arg1;
            this.styleFragmentsBase = arg2;
            this.invalidateStyleRecompute();
            return;
        }

        public function free():void
        {
            removeEventListener(ENABLE_MEASURE, this.onMeasureChange);
            removeEventListener(DISABLE_MEASURE, this.onMeasureChange);
            this.styleFragmentsBase = null;
            this.styleClassesBase = null;
            this.styleFragments = null;
            this.styleClasses = null;
            this.computedStyle = null;
            return;
        }

        protected function invalidateStyleRecompute():void
        {
            this.styleNeedsToBeRecomputed = true;
            return;
        }

        protected function recomputeStyle():void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            if (this.styleNeedsToBeRecomputed) 
            {
                this.styleNeedsToBeRecomputed = false;
                var loc7:*=0;
                var loc8:*=this.styleClassesBase;
                for (loc4 in loc8) 
                {
                    loc3 = this.styleClassesBase[loc4];
                    var loc9:*=0;
                    var loc10:*=loc3;
                    for (loc5 in loc10) 
                    {
                        this.computedStyle[loc5] = loc3[loc5];
                    }
                }
                loc7 = 0;
                loc8 = this.styleClasses;
                for (loc4 in loc8) 
                {
                    loc3 = this.styleClasses[loc4];
                    loc9 = 0;
                    loc10 = loc3;
                    for (loc5 in loc10) 
                    {
                        this.computedStyle[loc5] = loc3[loc5];
                    }
                }
                loc7 = 0;
                loc8 = this.styleFragmentsBase;
                for (loc5 in loc8) 
                {
                    this.computedStyle[loc5] = this.styleFragmentsBase[loc5];
                }
                loc7 = 0;
                loc8 = this.styleFragments;
                for (loc5 in loc8) 
                {
                    this.computedStyle[loc5] = this.styleFragments[loc5];
                }
                this._isolatedPosition = this.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE && isNaN(this.computedStyle.right) && isNaN(this.computedStyle.bottom) && this.computedStyle.marginBottomMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE && this.computedStyle.marginLeftMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE && this.computedStyle.marginTopMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE && this.computedStyle.marginRightMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE && this.computedStyle.topMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE && this.computedStyle.rightMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE && this.computedStyle.bottomMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE && this.computedStyle.leftMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE;
            }
            var loc1:*=0;
            var loc2:*=numChildren;
            while (loc1 < loc2) 
            {
                if ((loc6 = getChildAt(loc1) as lesta.unbound.layout.UbStyleable) != null) 
                {
                    loc6.recomputeStyle();
                }
                ++loc1;
            }
            return;
        }

        protected function propagateDirtyProperties():void
        {
            var loc3:*=null;
            if (!visible || parent == null || !this.measureEnabled) 
            {
                return;
            }
            this.pushDirtyDownwards();
            var loc1:*=0;
            var loc2:*=numChildren;
            while (loc1 < loc2) 
            {
                loc3 = getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                if (loc3 != null) 
                {
                    loc3.propagateDirtyProperties();
                }
                ++loc1;
            }
            this.pullDirtyFromBelow();
            return;
        }

        protected function postMeasureHook():void
        {
            var loc1:*=null;
            if (!visible || parent == null || !this.measureEnabled) 
            {
                return;
            }
            this.fixContentSize();
            var loc2:*=0;
            var loc3:*=numChildren;
            while (loc2 < loc3) 
            {
                loc1 = getChildAt(loc2) as lesta.unbound.layout.UbStyleable;
                if (loc1 != null) 
                {
                    loc1.postMeasureHook();
                }
                ++loc2;
            }
            return;
        }

        protected function updateMeasured():void
        {
            this._measuredOnce = true;
            return;
        }

        protected function measureX():void
        {
            if (!visible || parent == null || !this.measureEnabled) 
            {
                return;
            }
            if (this.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                if (this.computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
                {
                    if (this.computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_FILL) 
                    {
                        this.doMeasureX();
                        this.recursiveMeasureX();
                    }
                    else 
                    {
                        this.recursiveMeasureX();
                        this.doMeasureX();
                    }
                }
                else 
                {
                    this.recursiveMeasureX();
                    this.doMeasureX();
                }
            }
            else 
            {
                this.recursiveMeasureX();
            }
            return;
        }

        protected function measureY():void
        {
            if (!visible || parent == null || !this.measureEnabled) 
            {
                return;
            }
            if (this.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
            {
                if (this.computedStyle.heightMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
                {
                    if (this.computedStyle.heightMode != lesta.unbound.style.UbStyle.DIMENSION_FILL) 
                    {
                        this.doMeasureY();
                        this.recursiveMeasureY();
                    }
                    else 
                    {
                        this.recursiveMeasureY();
                        this.doMeasureY();
                    }
                }
                else 
                {
                    this.recursiveMeasureY();
                    this.doMeasureY();
                }
            }
            else 
            {
                this.recursiveMeasureY();
            }
            return;
        }

        public function invalidateStageRelativeChildren():void
        {
            var loc3:*=null;
            this.invalidateStageRelativeDimensions();
            var loc1:*=0;
            var loc2:*=numChildren;
            while (loc1 < loc2) 
            {
                loc3 = getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                if (loc3) 
                {
                    loc3.invalidateStageRelativeChildren();
                }
                ++loc1;
            }
            return;
        }

        public function set measuredHeight(arg1:int):void
        {
            return;
        }

        protected function pushDirtyDownwards():void
        {
            var loc1:*=0;
            var loc2:*=0;
            var loc3:*=null;
            var loc4:*=null;
            if (this.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                loc1 = 0;
                loc2 = numChildren;
                while (loc1 < loc2) 
                {
                    loc3 = getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                    if (loc3 != null) 
                    {
                        if ((loc4 = loc3.computedStyle).widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc4.widthMode == lesta.unbound.style.UbStyle.DIMENSION_FILL) 
                        {
                            loc4.dirty = loc4.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X;
                        }
                        if (!loc3.isolatedPosition) 
                        {
                            loc4.dirty = loc4.dirty | lesta.unbound.style.UbStyle.DIRTY_POSITION;
                        }
                    }
                    ++loc1;
                }
            }
            if (this.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
            {
                loc1 = 0;
                loc2 = numChildren;
                while (loc1 < loc2) 
                {
                    loc3 = getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                    if (loc3 != null) 
                    {
                        if ((loc4 = loc3.computedStyle).heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc4.heightMode == lesta.unbound.style.UbStyle.DIMENSION_FILL) 
                        {
                            loc4.dirty = loc4.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
                        }
                        if (!loc3.isolatedPosition) 
                        {
                            loc4.dirty = loc4.dirty | lesta.unbound.style.UbStyle.DIRTY_POSITION;
                        }
                    }
                    ++loc1;
                }
            }
            return;
        }

        protected function pullDirtyFromBelow():void
        {
            var loc3:*=null;
            var loc1:*=0;
            var loc2:*=numChildren;
            while (loc1 < loc2) 
            {
                loc3 = getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                if (!(loc3 == null) && !loc3.isolatedPosition && loc3.computedStyle.dirty & (lesta.unbound.style.UbStyle.DIRTY_MEASURE_X | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y | lesta.unbound.style.UbStyle.DIRTY_POSITION)) 
                {
                    this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_REFLOW;
                    break;
                }
                ++loc1;
            }
            if (this.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || this.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_FILL) 
            {
                loc1 = 0;
                loc2 = numChildren;
                while (loc1 < loc2) 
                {
                    loc3 = getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                    if (!(loc3 == null) && !(loc3.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc3.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
                    {
                        this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X | lesta.unbound.style.UbStyle.DIRTY_REFLOW;
                    }
                    ++loc1;
                }
            }
            if (this.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || this.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_FILL) 
            {
                loc1 = 0;
                loc2 = numChildren;
                while (loc1 < loc2) 
                {
                    loc3 = getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                    if (!(loc3 == null) && !(loc3.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc3.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
                    {
                        this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y | lesta.unbound.style.UbStyle.DIRTY_REFLOW;
                    }
                    ++loc1;
                }
            }
            return;
        }

        protected function recursiveMeasureX():void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc4:*=NaN;
            var loc5:*=NaN;
            var loc6:*=NaN;
            var loc7:*=NaN;
            this._contentWidth = 0;
            var loc3:*=0;
            if (this.computedStyle.flow == lesta.unbound.style.UbStyle.FLOW_TILE_VERTICAL) 
            {
                loc4 = 0;
                loc5 = 0;
                loc6 = 0;
                loc2 = 0;
                loc3 = numChildren;
                while (loc2 < loc3) 
                {
                    loc1 = getChildAt(loc2) as lesta.unbound.layout.UbStyleable;
                    if (!(loc1 == null) && !(loc1.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc1.visible) 
                    {
                        loc1.measureX();
                        if ((loc7 = loc5 + this.getChildsVerticalSize(loc1)) > this.measuredHeight) 
                        {
                            loc5 = 0;
                            loc4 = loc4 + loc6;
                            loc6 = 0;
                        }
                        loc6 = Math.max(loc6, this.getChildsHorizontalSize(loc1));
                        loc5 = loc5 + (loc1.measuredHeight + loc1.getMarginBottom() + loc1.getMarginTop());
                    }
                    ++loc2;
                }
                this._contentWidth = loc4 + loc6;
            }
            else 
            {
                loc2 = 0;
                loc3 = numChildren;
                while (loc2 < loc3) 
                {
                    loc1 = getChildAt(loc2) as lesta.unbound.layout.UbStyleable;
                    if (loc1 != null) 
                    {
                        loc1.measureX();
                        if (!(loc1.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc1.visible) 
                        {
                            this._contentWidth = this._contentWidth + this.getChildsHorizontalSize(loc1);
                        }
                    }
                    ++loc2;
                }
            }
            return;
        }

        protected function recursiveMeasureY():void
        {
            var loc3:*=null;
            this._contentHeight = 0;
            var loc1:*=0;
            var loc2:*=numChildren;
            while (loc1 < loc2) 
            {
                loc3 = getChildAt(loc1) as lesta.unbound.layout.UbStyleable;
                if (loc3 != null) 
                {
                    loc3.measureY();
                    if (!(loc3.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc3.visible) 
                    {
                        this._contentHeight = this._contentHeight + this.getChildsVerticalSize(loc3);
                    }
                }
                ++loc1;
            }
            return;
        }

        protected function doMeasureX(arg1:Number=NaN):void
        {
            var loc1:*=this.computedStyle.widthMode;
            var loc2:*=this.computedStyle.width;
            var loc3:*=this.getMarginLeft() + this.getMarginRight();
            if (isNaN(arg1)) 
            {
                if (this.computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_FILL) 
                {
                    arg1 = parent as lesta.unbound.layout.UbStyleable ? (parent as lesta.unbound.layout.UbStyleable)._sourceWidth : 0;
                }
                else 
                {
                    arg1 = parent as lesta.unbound.layout.UbStyleable ? (parent as lesta.unbound.layout.UbStyleable).measuredAvailableWidth : 0;
                }
            }
            var loc4:*=lesta.unbound.style.UbStyle.FLOW_HORIZONTAL;
            var loc5:*=this.getChildsHorizontalSize;
            var loc6:*=this._sourceWidth;
            this._sourceWidth = this.clampWidth(this.doMeasureSize(loc1, loc2, arg1, loc3, loc4, loc5));
            if (this._sourceWidth != loc6) 
            {
                this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_PAINT | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X;
            }
            return;
        }

        protected function clampWidth(arg1:Number):Number
        {
            if (arg1 > this.computedStyle.maxWidth) 
            {
                return this.computedStyle.maxWidth;
            }
            if (arg1 < this.computedStyle.minWidth) 
            {
                return this.computedStyle.minWidth;
            }
            return arg1;
        }

        protected function doMeasureY(arg1:Number=NaN):void
        {
            var loc1:*=this.computedStyle.heightMode;
            var loc2:*=this.computedStyle.height;
            var loc3:*=this.getMarginTop() + this.getMarginBottom();
            if (isNaN(arg1)) 
            {
                if (this.computedStyle.heightMode != lesta.unbound.style.UbStyle.DIMENSION_FILL) 
                {
                    arg1 = parent as lesta.unbound.layout.UbStyleable ? (parent as lesta.unbound.layout.UbStyleable)._sourceHeight : 0;
                }
                else 
                {
                    arg1 = parent as lesta.unbound.layout.UbStyleable ? (parent as lesta.unbound.layout.UbStyleable).measuredAvailableHeight : 0;
                }
            }
            var loc4:*=lesta.unbound.style.UbStyle.FLOW_VERTICAL;
            var loc5:*=this.getChildsVerticalSize;
            var loc6:*=this._sourceHeight;
            this._sourceHeight = this.clampHeight(this.doMeasureSize(loc1, loc2, arg1, loc3, loc4, loc5));
            if (this._sourceHeight != loc6) 
            {
                this.computedStyle.dirty = this.computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_PAINT | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
            }
            return;
        }

        protected function clampHeight(arg1:Number):Number
        {
            if (arg1 > this.computedStyle.maxHeight) 
            {
                return this.computedStyle.maxHeight;
            }
            if (arg1 < this.computedStyle.minHeight) 
            {
                return this.computedStyle.minHeight;
            }
            return arg1;
        }

        protected function fixChildrenWidths():void
        {
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=NaN;
            var loc1:*=numChildren;
            if (this.computedStyle.flow == lesta.unbound.style.UbStyle.FLOW_HORIZONTAL || this.computedStyle.flow == lesta.unbound.style.UbStyle.FLOW_TILE_VERTICAL) 
            {
                this._totalAbsoluteContentWidth = 0;
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = getChildAt(loc3) as lesta.unbound.layout.UbStyleable;
                    if (!(loc2 == null) && loc2.visible && !(loc2.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && (loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE || loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT)) 
                    {
                        this._totalAbsoluteContentWidth = this._totalAbsoluteContentWidth + this.getChildsHorizontalSize(loc2);
                    }
                    ++loc3;
                }
                loc4 = Math.max(0, this._sourceWidth - this._totalAbsoluteContentWidth);
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = getChildAt(loc3) as lesta.unbound.layout.UbStyleable;
                    if (!(loc2 == null) && (loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_FILL)) 
                    {
                        loc2.doMeasureX(loc2.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE ? loc4 : this._sourceWidth);
                        loc2.fixChildrenWidths();
                    }
                    ++loc3;
                }
            }
            else 
            {
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = getChildAt(loc3) as lesta.unbound.layout.UbStyleable;
                    if (!(loc2 == null) && (loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc2.computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_FILL)) 
                    {
                        loc2.doMeasureX(this._sourceWidth);
                        loc2.fixChildrenWidths();
                    }
                    ++loc3;
                }
            }
            return;
        }

        protected function fixChildrenHeights():void
        {
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=NaN;
            var loc1:*=numChildren;
            if (this.computedStyle.flow != lesta.unbound.style.UbStyle.FLOW_VERTICAL) 
            {
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = getChildAt(loc3) as lesta.unbound.layout.UbStyleable;
                    if (!(loc2 == null) && (loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_FILL)) 
                    {
                        loc2.doMeasureY(this._sourceHeight);
                        loc2.fixChildrenHeights();
                    }
                    ++loc3;
                }
            }
            else 
            {
                this._totalAbsoluteContentHeight = 0;
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = getChildAt(loc3) as lesta.unbound.layout.UbStyleable;
                    if (!(loc2 == null) && loc2.visible && !(loc2.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && (loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE || loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_AUTO || loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH || loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT)) 
                    {
                        this._totalAbsoluteContentHeight = this._totalAbsoluteContentHeight + this.getChildsVerticalSize(loc2);
                    }
                    ++loc3;
                }
                loc4 = Math.max(0, this._sourceHeight - this._totalAbsoluteContentHeight);
                loc3 = 0;
                while (loc3 < loc1) 
                {
                    loc2 = getChildAt(loc3) as lesta.unbound.layout.UbStyleable;
                    if (!(loc2 == null) && (loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE || loc2.computedStyle.heightMode == lesta.unbound.style.UbStyle.DIMENSION_FILL)) 
                    {
                        loc2.doMeasureY(loc2.computedStyle.position != lesta.unbound.style.UbStyle.POSITION_ABSOLUTE ? loc4 : this._sourceHeight);
                        loc2.fixChildrenHeights();
                    }
                    ++loc3;
                }
            }
            return;
        }

        protected function fixContentSize():void
        {
            if (this.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                this.fixChildrenWidths();
            }
            if (this.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
            {
                this.fixChildrenHeights();
            }
            return;
        }

        protected function doMeasureSize(arg1:int, arg2:Number, arg3:Number, arg4:Number, arg5:int, arg6:Function):Number
        {
            var loc1:*=NaN;
            var loc2:*=NaN;
            var loc3:*=arg1;
            switch (loc3) 
            {
                case lesta.unbound.style.UbStyle.DIMENSION_ABSOLUTE:
                case lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_WIDTH:
                case lesta.unbound.style.UbStyle.DIMENSION_RELATIVE_TO_STAGE_HEIGHT:
                {
                    loc1 = this.computeDimensionValue(arg2, arg1, arg3);
                    break;
                }
                case lesta.unbound.style.UbStyle.DIMENSION_RELATIVE:
                {
                    loc1 = this.computeDimensionValue(arg2, arg1, arg3);
                    loc1 = Math.max(0, loc1 - arg4);
                    break;
                }
                case lesta.unbound.style.UbStyle.DIMENSION_FILL:
                {
                    loc1 = this.computeDimensionValue(arg2, arg1, arg3);
                    loc1 = Math.max(0, loc1 - arg4);
                    loc2 = this.doMeasureAuto(arg5, arg6);
                    loc1 = Math.min(loc1, loc2);
                    break;
                }
                default:
                {
                    loc1 = this.doMeasureAuto(arg5, arg6);
                    break;
                }
            }
            return loc1;
        }

        protected function doMeasureAuto(arg1:int, arg2:Function):Number
        {
            var measureDirection:int;
            var getChildsSize:Function;
            var flowDirection:int;
            var measureAsStack:Function;
            var measureHeightTileLTR:Function;
            var measureWidthTileTTB:Function;
            var measureMax:Function;

            var loc1:*;
            measureDirection = arg1;
            getChildsSize = arg2;
            measureAsStack = function ():Number
            {
                var loc4:*=null;
                var loc1:*=0;
                var loc2:*=0;
                var loc3:*=numChildren;
                while (loc2 < loc3) 
                {
                    if (!((loc4 = getChildAt(loc2) as lesta.unbound.layout.UbStyleable) == null) && !(loc4.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE)) 
                    {
                        loc1 = loc1 + getChildsSize(loc4);
                    }
                    ++loc2;
                }
                return loc1;
            }
            measureHeightTileLTR = function ():Number
            {
                var loc6:*=null;
                var loc7:*=NaN;
                var loc8:*=NaN;
                var loc1:*=0;
                var loc2:*=0;
                var loc3:*=0;
                var loc4:*=0;
                var loc5:*=numChildren;
                while (loc4 < loc5) 
                {
                    if (!((loc6 = getChildAt(loc4) as lesta.unbound.layout.UbStyleable) == null) && !(loc6.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc6.visible) 
                    {
                        loc7 = getChildsHorizontalSize(loc6);
                        if ((loc8 = loc1 + loc7) > measuredWidth) 
                        {
                            loc1 = 0;
                            loc2 = loc2 + loc3;
                            loc3 = 0;
                        }
                        loc3 = Math.max(loc3, getChildsVerticalSize(loc6));
                        loc1 = loc1 + loc7;
                    }
                    ++loc4;
                }
                loc2 = loc2 + loc3;
                return loc2;
            }
            measureWidthTileTTB = function ():Number
            {
                var loc6:*=null;
                var loc7:*=NaN;
                var loc1:*=0;
                var loc2:*=0;
                var loc3:*=0;
                var loc4:*=0;
                var loc5:*=numChildren;
                while (loc4 < loc5) 
                {
                    if (!((loc6 = getChildAt(loc4) as lesta.unbound.layout.UbBlock) == null) && !(loc6.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE) && loc6.visible) 
                    {
                        if ((loc7 = loc2 + getChildsVerticalSize(loc6)) > measuredHeight) 
                        {
                            loc2 = 0;
                            loc1 = loc1 + loc3;
                            loc3 = 0;
                        }
                        loc3 = Math.max(loc3, getChildsHorizontalSize(loc6));
                        if (name == "shipCarouseContaner") 
                        {
                            trace("maxWidthInColumn", loc3, "acumWidth", loc1);
                        }
                        loc2 = loc2 + (loc6.measuredHeight + loc6.getMarginBottom() + loc6.getMarginTop());
                    }
                    ++loc4;
                }
                loc1 = loc1 + loc3;
                return loc1;
            }
            measureMax = function ():Number
            {
                var loc4:*=null;
                var loc5:*=NaN;
                var loc1:*=0;
                var loc2:*=0;
                var loc3:*=numChildren;
                while (loc2 < loc3) 
                {
                    if (!((loc4 = getChildAt(loc2) as lesta.unbound.layout.UbStyleable) == null) && !(loc4.computedStyle.position == lesta.unbound.style.UbStyle.POSITION_ABSOLUTE)) 
                    {
                        loc1 = (loc5 = getChildsSize(loc4)) > loc1 ? loc5 : loc1;
                    }
                    ++loc2;
                }
                return loc1;
            }
            flowDirection = this.computedStyle.flow > 1 ? this.computedStyle.flow - 2 : this.computedStyle.flow;
            if (measureDirection == flowDirection) 
            {
                return measureAsStack();
            }
            if (this.computedStyle.flow == lesta.unbound.style.UbStyle.FLOW_TILE_VERTICAL) 
            {
                return measureWidthTileTTB();
            }
            if (this.computedStyle.flow == lesta.unbound.style.UbStyle.FLOW_TILE_HORIZONTAL) 
            {
                return measureHeightTileLTR();
            }
            return measureMax();
        }

        public static const ENABLE_MEASURE:String="UbStyleable.ENABLE_MEASURE";

        public static const DISABLE_MEASURE:String="UbStyleable.DISABLE_MEASURE";

        public var styleFragments:Object;

        public var styleClasses:Object;

        public var computedStyle:lesta.unbound.style.UbStyle;

        protected var _sourceWidth:int=0;

        protected var _sourceHeight:int=0;

        protected var styleFragmentsBase:Object;

        protected var styleNeedsToBeRecomputed:Boolean;

        protected var _totalAbsoluteContentWidth:Number=0;

        protected var _totalAbsoluteContentHeight:Number=0;

        protected var _contentHeight:int=0;

        protected var _contentWidth:int=0;

        protected var _isolatedPosition:Boolean;

        protected var _measuredOnce:Boolean=false;

        protected var _forceMasureEnabled:Boolean=false;

        protected var styleClassesBase:Object;
    }
}
