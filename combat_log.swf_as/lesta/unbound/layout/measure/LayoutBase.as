package lesta.unbound.layout.measure 
{
    import lesta.unbound.layout.*;
    
    public class LayoutBase extends Object
    {
        public function LayoutBase(arg1:lesta.unbound.layout.UbBlock)
        {
            super();
            this._block = arg1;
            this._dimensions = new lesta.unbound.layout.measure.Dimensions(arg1);
            return;
        }

        public function fini():void
        {
            this._dimensions = null;
            this._block = null;
            return;
        }

        public function get dimensions():lesta.unbound.layout.measure.Dimensions
        {
            return this._dimensions;
        }

        public function get x():Number
        {
            return this._dimensions.extX;
        }

        public function set x(arg1:Number):void
        {
            this._dimensions.extX = arg1;
            this._block.nativeX = this._dimensions.x;
            return;
        }

        public function get y():Number
        {
            return this._dimensions.extY;
        }

        public function set y(arg1:Number):void
        {
            this._dimensions.extY = arg1;
            this._block.nativeY = this._dimensions.y;
            return;
        }

        public function get measureEnabled():Boolean
        {
            return this._block.visible && this._block.parent;
        }

        public function apply():void
        {
            this.propagateDirtyProperties();
            this.prepare();
            this.measureX();
            this.measureY();
            this.postMeasure();
            this.reflow();
            this.repaint();
            return;
        }

        public function propagateDirtyProperties():void
        {
            return;
        }

        public function prepare():void
        {
            var loc2:*=null;
            if (!this.measureEnabled) 
            {
                return;
            }
            var loc1:*=this._block.numChildren;
            var loc3:*=0;
            while (loc3 < loc1) 
            {
                loc2 = this._block.getChildAt(loc3) as lesta.unbound.layout.UbBlock;
                if (loc2) 
                {
                    loc2.layout.prepare();
                }
                ++loc3;
            }
            return;
        }

        public function measureX():void
        {
            return;
        }

        public function measureY():void
        {
            return;
        }

        public function reflow():void
        {
            return;
        }

        public function repaint():void
        {
            return;
        }

        public function postMeasure():void
        {
            return;
        }

        public function updateVirtual():void
        {
            return;
        }

        protected var _block:lesta.unbound.layout.UbBlock;

        private var _dimensions:lesta.unbound.layout.measure.Dimensions;
    }
}
