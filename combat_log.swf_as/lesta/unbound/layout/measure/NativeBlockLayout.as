package lesta.unbound.layout.measure 
{
    import flash.display.*;
    import lesta.unbound.layout.*;
    import lesta.unbound.style.*;
    
    public class NativeBlockLayout extends lesta.unbound.layout.measure.BlockLayout
    {
        public function NativeBlockLayout(arg1:lesta.unbound.layout.UbBlock)
        {
            super(arg1);
            return;
        }

        private function get nativeBlock():lesta.unbound.layout.UbNativeBlock
        {
            return _block as lesta.unbound.layout.UbNativeBlock;
        }

        private function get nativeChild():flash.display.DisplayObject
        {
            return this.nativeBlock.nativeChild;
        }

        public override function measureX():void
        {
            if (!measureEnabled) 
            {
                return;
            }
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                if (_block.computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
                {
                    doMeasureX();
                    recursiveMeasureX();
                }
                else if (_block.computedStyle.widthByChildren) 
                {
                    recursiveMeasureX();
                    doMeasureX();
                }
                else 
                {
                    dimensions.width = clampWidth(this.nativeChild.width);
                    recursiveMeasureX();
                }
            }
            else 
            {
                recursiveMeasureX();
            }
            return;
        }

        public override function measureY():void
        {
            if (!measureEnabled) 
            {
                return;
            }
            if (_block.computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
            {
                if (_block.computedStyle.heightMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
                {
                    doMeasureY();
                    recursiveMeasureY();
                }
                else if (_block.computedStyle.heightByChildren) 
                {
                    recursiveMeasureY();
                    doMeasureY();
                }
                else 
                {
                    dimensions.height = clampHeight(this.nativeChild.height);
                    recursiveMeasureY();
                }
            }
            else 
            {
                recursiveMeasureY();
            }
            return;
        }
    }
}
