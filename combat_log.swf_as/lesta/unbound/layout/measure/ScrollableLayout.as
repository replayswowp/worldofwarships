package lesta.unbound.layout.measure 
{
    import lesta.unbound.layout.*;
    import lesta.unbound.style.*;
    
    public class ScrollableLayout extends lesta.unbound.layout.measure.LayoutBase implements lesta.unbound.layout.measure.ILayout
    {
        public function ScrollableLayout(arg1:lesta.unbound.layout.UbBlock)
        {
            super(arg1);
            return;
        }

        public function updateScroller():void
        {
            this.updateScrollController();
            if (this.scrollinglController) 
            {
                this.scrollinglController.update(this._contentWidth, this._contentHeight);
            }
            this.dispatchScrollUpdate();
            return;
        }

        public function scrollTo(arg1:Number, arg2:Function=null):void
        {
            if (this.scrollinglController == null) 
            {
                _block.computedStyle.scrollPosition = arg1;
            }
            else 
            {
                this.scrollinglController.scrollToFixed(arg1, arg2);
            }
            this.dispatchScrollUpdate();
            return;
        }

        private function dispatchScrollUpdate():void
        {
            var loc1:*=new lesta.unbound.layout.UbEvent(lesta.unbound.layout.UbEvent.SCROLL_UPDATE);
            loc1.data = {"contentHeight":this._contentHeight, "contentWidth":this._contentWidth};
            _block.dispatchEvent(loc1);
            return;
        }

        private function updateScrollController():void
        {
            if (this.prevScrollingController != _block.computedStyle.scrollbarController) 
            {
                if (this.scrollinglController) 
                {
                    this.scrollinglController.free();
                }
                this.scrollinglController = new _block.computedStyle.scrollbarController() as lesta.unbound.style.IScrollingController;
                this.scrollinglController.init(_block, this.doScroll, this.beginScroll, this.endScroll);
                this.prevScrollingController = _block.computedStyle.scrollbarController;
            }
            return;
        }

        protected function doScroll():void
        {
            _block.alpha = _block.alpha;
            reflow();
            return;
        }

        protected function beginScroll():void
        {
            _block.mouseChildren = false;
            return;
        }

        protected function endScroll():void
        {
            _block.mouseChildren = _block.computedStyle.mouseChildren;
            _block.dispatchEvent(new lesta.unbound.layout.UbEvent(lesta.unbound.layout.UbEvent.END_SCROLL));
            this.dispatchScrollUpdate();
            return;
        }

        protected var _contentWidth:Number;

        protected var _contentHeight:Number;

        protected var prevScrollingController:Class;

        protected var scrollinglController:lesta.unbound.style.IScrollingController;
    }
}
