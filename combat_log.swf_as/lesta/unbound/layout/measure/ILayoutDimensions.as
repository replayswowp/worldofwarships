package lesta.unbound.layout.measure 
{
    public interface ILayoutDimensions
    {
        function get x():Number;

        function set x(arg1:Number):void;

        function get y():Number;

        function set y(arg1:Number):void;

        function get dimensions():lesta.unbound.layout.measure.Dimensions;
    }
}
