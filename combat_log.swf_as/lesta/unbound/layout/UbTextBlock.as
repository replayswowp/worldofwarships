package lesta.unbound.layout 
{
    import flash.display.*;
    import flash.text.*;
    import lesta.unbound.style.*;
    import lesta.utils.*;
    
    public class UbTextBlock extends lesta.unbound.layout.UbBlock
    {
        public function UbTextBlock()
        {
            this._htmlPattern = new RegExp("<[^<]+>");
            this._textFormat = new flash.text.TextFormat();
            super();
            this._textField = new flash.text.TextField();
            this._textField.antiAliasType = flash.text.AntiAliasType.NORMAL;
            this._textField.embedFonts = true;
            addChild(this._textField);
            return;
        }

        public function set intValue(arg1:Number):void
        {
            this._intValue = arg1;
            this.pureText = isNaN(this._intValue) ? "" : this._intValue.toFixed();
            return;
        }

        public function get intValue():Number
        {
            return this._intValue;
        }

        public function set pureText(arg1:String):void
        {
            this._text = arg1;
            this._textField.text = this._text ? this._text : "";
            this._textField.setTextFormat(this._textFormat);
            return;
        }

        public function get pureText():String
        {
            return this._text;
        }

        public function set text(arg1:String):void
        {
            if (this._text == arg1) 
            {
                return;
            }
            this._text = arg1;
            computedStyle.dirty = computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_TEXT;
            dispatchUpdate();
            return;
        }

        public override function getFiltersTarget():flash.display.DisplayObject
        {
            return this._textField;
        }

        public override function set visible(arg1:Boolean):void
        {
            if (arg1) 
            {
                computedStyle.dirty = computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_TEXT;
            }
            super.visible = arg1;
            return;
        }

        protected override function recomputeStyle():void
        {
            if (styleNeedsToBeRecomputed) 
            {
                super.recomputeStyle();
                if (computedStyle.widthMode == lesta.unbound.style.UbStyle.DIMENSION_RELATIVE) 
                {
                    var loc1:*;
                    this._textField.wordWrap = loc1 = true;
                    this._textField.multiline = loc1;
                }
            }
            this.preMeasureHook();
            return;
        }

        protected function preMeasureHook():void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=!(computedStyle.styleSheet == null);
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_STYLESHEET) 
            {
                if (loc1) 
                {
                    loc2 = {};
                    loc2.fontFamily = computedStyle.fontFamily;
                    loc2.color = "#" + computedStyle.textColor.toString(16);
                    loc2.fontSize = computeDimensionValue(computedStyle.fontSize, computedStyle.fontSizeMode, 0);
                    loc2.textAlign = computedStyle.textAlign;
                    loc2.leading = computedStyle.leading;
                    loc2.letterSpacing = computedStyle.letterSpacing;
                    loc3 = new flash.text.StyleSheet();
                    loc3.parseCSS(computedStyle.styleSheet);
                    loc3.setStyle("body", loc2);
                    this._textField.styleSheet = loc3;
                }
                else 
                {
                    this._textField.styleSheet = null;
                }
            }
            if (computedStyle.dirty & (lesta.unbound.style.UbStyle.DIRTY_TEXT | lesta.unbound.style.UbStyle.DIRTY_STYLESHEET)) 
            {
                if (loc1) 
                {
                    this._textField.htmlText = this._text;
                }
                else 
                {
                    this._textField.text = this._text;
                    if (this._textField.text.search(this._htmlPattern) > -1) 
                    {
                        this._textField.text = null;
                        this._textField.htmlText = this._text;
                    }
                    this._textFormat.font = computedStyle.fontFamily;
                    this._textFormat.color = computedStyle.textColor;
                    this._textFormat.size = computeDimensionValue(computedStyle.fontSize, computedStyle.fontSizeMode, 0);
                    this._textFormat.align = computedStyle.textAlign;
                    this._textFormat.leading = computedStyle.leading;
                    this._textFormat.letterSpacing = computedStyle.letterSpacing;
                    this._textField.setTextFormat(this._textFormat);
                }
                computedStyle.dirty = computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_X | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
            }
            return;
        }

        protected override function measureX():void
        {
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                this.doMeasureX();
            }
            return;
        }

        protected override function measureY():void
        {
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y) 
            {
                this.doMeasureY();
            }
            return;
        }

        protected override function doMeasureX(arg1:Number=NaN):void
        {
            var loc1:*=NaN;
            if (computedStyle.widthMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
            {
                super.doMeasureX(arg1);
            }
            else 
            {
                var loc2:*;
                this._textField.multiline = loc2 = false;
                this._textField.wordWrap = loc2;
                loc1 = this._textField.textWidth + TEXTFIELD_GUTTER;
                if (loc1 > computedStyle.maxWidth) 
                {
                    this._textField.multiline = loc2 = true;
                    this._textField.wordWrap = loc2;
                    _sourceWidth = computedStyle.maxWidth;
                }
                else if (this._text.length > 0) 
                {
                    _sourceWidth = clampWidth(loc1);
                }
                else 
                {
                    _sourceWidth = clampWidth(0);
                }
            }
            if (this._textField.width != _sourceWidth) 
            {
                this._textField.width = _sourceWidth;
            }
            lesta.utils.DisplayObjectUtils.trimSingleLineText(this._textField);
            return;
        }

        protected override function doMeasureY(arg1:Number=NaN):void
        {
            if (computedStyle.heightMode != lesta.unbound.style.UbStyle.DIMENSION_AUTO) 
            {
                super.doMeasureY(arg1);
            }
            else if (this._text.length > 0) 
            {
                _sourceHeight = clampHeight(this._textField.textHeight + TEXTFIELD_GUTTER);
            }
            else 
            {
                _sourceHeight = clampHeight(0);
            }
            if (this._textField.height != _sourceHeight) 
            {
                this._textField.height = _sourceHeight;
            }
            return;
        }

        protected override function reflowAndRepaint(arg1:Boolean=false):void
        {
            commonStyleDirtyChecks();
            computedStyle.dirty = 0;
            return;
        }

        protected override function updateMouse(arg1:Boolean, arg2:Boolean, arg3:Boolean):void
        {
            super.updateMouse(arg1, arg2, arg3);
            this._textField.mouseEnabled = arg1;
            this._textField.selectable = computedStyle.selectable;
            return;
        }

        protected override function propagateDirtyProperties():void
        {
            if (!visible) 
            {
                return;
            }
            if (computedStyle.dirty & lesta.unbound.style.UbStyle.DIRTY_MEASURE_X) 
            {
                computedStyle.dirty = computedStyle.dirty | lesta.unbound.style.UbStyle.DIRTY_MEASURE_Y;
            }
            return;
        }

        internal static const TEXTFIELD_GUTTER:int=4;

        internal var _textField:flash.text.TextField;

        internal var _text:String;

        internal var _htmlPattern:RegExp;

        internal var _textFormat:flash.text.TextFormat;

        internal var _intValue:Number=NaN;
    }
}
