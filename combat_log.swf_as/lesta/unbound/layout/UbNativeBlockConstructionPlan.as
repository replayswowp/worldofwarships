package lesta.unbound.layout 
{
    import flash.display.*;
    import flash.utils.*;
    
    public class UbNativeBlockConstructionPlan extends lesta.unbound.layout.UbBlockConstructionPlan
    {
        public function UbNativeBlockConstructionPlan()
        {
            super();
            return;
        }

        public override function createFromXml(arg1:XML, arg2:lesta.unbound.layout.UbBlockFactory):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            super.createFromXml(arg1, arg2);
            this.nativeObjectClass = flash.utils.getDefinitionByName(arg1.attribute("className")) as Class;
            var loc6:*=0;
            var loc7:*=arg1.innerBind;
            for each (loc1 in loc7) 
            {
                loc2 = loc1.attribute("name");
                loc3 = loc1.attribute("elementName");
                loc4 = loc1.attribute("value");
                (loc5 = bindingsFromString(loc2, loc4, arg2, loc1.toXMLString())).elementName = loc3;
                bindings.push(loc5);
            }
            return;
        }

        public override function construct(arg1:lesta.unbound.layout.UbBlockFactory):lesta.unbound.layout.UbBlock
        {
            var loc3:*=null;
            var loc1:*=new lesta.unbound.layout.UbNativeBlock();
            var loc2:*=flash.display.DisplayObject(new this.nativeObjectClass());
            var loc4:*=0;
            var loc5:*=settingsDict;
            for (loc3 in loc5) 
            {
                loc2[loc3] = settingsDict[loc3];
                if (!(loc3 in PROPERTIES_ALSO_SET_FOR_SELF)) 
                {
                    continue;
                }
                loc1[loc3] = settingsDict[loc3];
            }
            loc1.setNativeChild(loc2);
            principalConstruct(loc1, arg1);
            return loc1;
        }

        internal static const PROPERTIES_ALSO_SET_FOR_SELF:Object={"mouseChildren":true, "mouseEnabled":true};

        internal var nativeObjectClass:Class;
    }
}
