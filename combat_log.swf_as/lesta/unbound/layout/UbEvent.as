package lesta.unbound.layout 
{
    import flash.events.*;
    
    public class UbEvent extends flash.events.Event
    {
        public function UbEvent(arg1:String, arg2:Boolean=false, arg3:Boolean=false)
        {
            super(arg1, arg2, arg3);
            return;
        }

        public static const UPDATE_ROOT:String="UbBlock.UPDATE_ROOT";

        public static const END_SCROLL:String="UbBlock.END_SCROLL";

        public static const BLOCK_REFLOW:String="UbBlock.BLOCK_REFLOW";

        public static const SCROLL_UPDATE:String="UbBlock.SCROLL_UPDATE";

        public var data:Object;
    }
}
