package lesta.datahub 
{
    import flash.utils.*;
    import lesta.components.*;
    import lesta.utils.*;
    
    public class DataHub extends Object
    {
        public function DataHub()
        {
            this.entities = new flash.utils.Dictionary();
            this.invokers = new flash.utils.Dictionary(true);
            this.collections = new flash.utils.Dictionary();
            this._global = new lesta.components._Global();
            super();
            return;
        }

        public function fini():void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=this.invokers;
            for each (loc1 in loc3) 
            {
                loc1.fini();
            }
            this._global = null;
            this.collections = null;
            this.entities = null;
            return;
        }

        public function createEntity():lesta.datahub.Entity
        {
            var loc3:*;
            var loc4:*;
            var loc1:*=loc3.topEntityId = loc4 = ((loc3 = this).topEntityId + 1);
            var loc2:*=new lesta.datahub.Entity(loc1);
            this.entities[loc1] = loc2;
            return loc2;
        }

        public function getEntity(arg1:uint):lesta.datahub.Entity
        {
            return this.entities[arg1];
        }

        public function removeEntity(arg1:uint):void
        {
            delete this.entities[arg1];
            return;
        }

        public function createComponent(arg1:String):lesta.datahub.Component
        {
            var loc1:*=flash.utils.getDefinitionByName(COMPONENT_PACKAGE + arg1) as Class;
            var loc2:*=new loc1() as lesta.datahub.Component;
            return loc2;
        }

        public function addComponent(arg1:lesta.datahub.Entity, arg2:lesta.datahub.Component):lesta.datahub.Component
        {
            arg1.addComponent(arg2);
            this.invokeComponentCallback(arg2.classID | lesta.datahub.Component.ADD, [arg2]);
            return arg2;
        }

        public function removeComponent(arg1:lesta.datahub.Component):void
        {
            var loc1:*=arg1.entity;
            this.invokeComponentCallback(arg1.classID | lesta.datahub.Component.REMOVE, [arg1]);
            loc1.removeComponent(arg1.classID);
            return;
        }

        public function getComponent(arg1:uint, arg2:int=0):lesta.datahub.Component
        {
            var loc1:*=this.collections[arg1];
            if (!loc1) 
            {
                return null;
            }
            if (arg2 < 0) 
            {
                arg2 = arg2 + loc1.items.length;
            }
            return loc1.items.length > arg2 ? loc1.items[arg2].getComponent(arg1) : null;
        }

        public function getCollection(arg1:uint):lesta.datahub.Collection
        {
            var loc1:*=this.collections[arg1];
            if (!loc1) 
            {
                loc1 = new lesta.datahub.Collection();
                this.collections[arg1] = loc1;
            }
            return loc1;
        }

        public function addComponentCallback(arg1:uint, arg2:uint, arg3:Function):void
        {
            this.getInvoker(arg1 | arg2).addCallback(arg3);
            return;
        }

        public function removeComponentCallback(arg1:uint, arg2:uint, arg3:Function):void
        {
            var loc1:*=this.getInvoker(arg1 | arg2, false);
            if (loc1) 
            {
                loc1.removeCallback(arg3);
            }
            return;
        }

        public function invokeComponentCallback(arg1:uint, arg2:Array):void
        {
            var loc1:*=this.getInvoker(arg1, false);
            if (loc1) 
            {
                loc1.invoke(arg2);
            }
            return;
        }

        private function getInvoker(arg1:uint, arg2:Boolean=true):lesta.utils.Invoker
        {
            var loc1:*=this.invokers[arg1];
            if (!loc1 && arg2) 
            {
                loc1 = new lesta.utils.Invoker();
                this.invokers[arg1] = loc1;
            }
            return loc1;
        }

        private static const COMPONENT_PACKAGE:String="lesta.components.";

        private var entities:flash.utils.Dictionary;

        private var invokers:flash.utils.Dictionary;

        private var collections:flash.utils.Dictionary;

        private var topEntityId:uint=0;

        public var _global:lesta.components._Global;
    }
}
