package lesta.datahub 
{
    import __AS3__.vec.*;
    import lesta.utils.*;
    
    public class SortedCollection extends lesta.datahub.Collection
    {
        public function SortedCollection()
        {
            this.sorted = new Vector.<Node>();
            super();
            return;
        }

        public override function fini():void
        {
            this.sorted = null;
            super.fini();
            return;
        }

        public override function add(arg1:lesta.datahub.Entity):void
        {
            return;
        }

        public override function remove(arg1:lesta.datahub.Entity):void
        {
            return;
        }

        public function updateSorting():void
        {
            var loc3:*=null;
            var loc1:*=new Vector.<Node>(this.sorted.length);
            var loc2:*=0;
            while (loc2 < this.sorted.length) 
            {
                loc3 = this.sorted[loc2];
                lesta.utils.Debug.assert(loc1[loc3.index] == null, "[Fl] SortedCollection: two nodes at the same index (" + loc3.index + "): " + loc1[loc3.index] + " and " + loc3);
                loc1[loc3.index] = loc3;
                items[loc3.index] = loc3.entity;
                if (loc2 != loc3.index) 
                {
                    evMoved.invoke([loc3.entity, loc2, loc3.index]);
                }
                ++loc2;
            }
            this.sorted = loc1;
            update();
            return;
        }

        public function createNode(arg1:lesta.datahub.Entity, arg2:int):Node
        {
            var loc1:*=new Node(arg1, arg2);
            this.sorted.splice(arg2, 0, loc1);
            items.splice(arg2, 0, arg1);
            var loc2:*=arg2 + 1;
            while (loc2 < this.sorted.length) 
            {
                var loc3:*;
                var loc4:*=((loc3 = this.sorted[loc2]).index + 1);
                loc3.index = loc4;
                ++loc2;
            }
            evAdded.invoke([arg1, arg2]);
            update();
            return loc1;
        }

        public function removeNodeAt(arg1:int):void
        {
            var loc1:*=items[arg1];
            this.sorted.splice(arg1, 1);
            items.splice(arg1, 1);
            var loc2:*=arg1;
            while (loc2 < this.sorted.length) 
            {
                var loc3:*;
                var loc4:*=((loc3 = this.sorted[loc2]).index - 1);
                loc3.index = loc4;
                ++loc2;
            }
            evRemoved.invoke([loc1, arg1]);
            update();
            return;
        }

        public var sorted:__AS3__.vec.Vector.<Node>;
    }
}


class Node extends Object
{
    public function Node(arg1:lesta.datahub.Entity, arg2:int)
    {
        super();
        this.entity = arg1;
        this.index = arg2;
        return;
    }

    public function toString():String
    {
        return "(Node #" + this.index + " " + this.entity + ")";
    }

    public var entity:lesta.datahub.Entity;

    public var index:int;
}