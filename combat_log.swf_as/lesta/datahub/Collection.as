package lesta.datahub 
{
    import flash.events.*;
    import flash.utils.*;
    import lesta.utils.*;
    
    public class Collection extends flash.events.EventDispatcher
    {
        public function Collection()
        {
            this.items = new Array();
            this.evAdded = new lesta.utils.Invoker();
            this.evRemoved = new lesta.utils.Invoker();
            this.evMoved = new lesta.utils.Invoker();
            this.children = new flash.utils.Dictionary(true);
            super();
            return;
        }

        public function fini():void
        {
            this.evAdded.fini();
            this.evRemoved.fini();
            this.evMoved.fini();
            this.children = null;
            this.items = null;
            return;
        }

        public function add(arg1:lesta.datahub.Entity):void
        {
            this.items.push(arg1);
            this.evAdded.invoke([arg1, (this.items.length - 1)]);
            this.update();
            return;
        }

        public function insertFront(arg1:lesta.datahub.Entity):void
        {
            this.items.unshift(arg1);
            this.update();
            return;
        }

        public function remove(arg1:lesta.datahub.Entity):void
        {
            var loc1:*=this.items.indexOf(arg1);
            if (loc1 >= 0) 
            {
                this.items.splice(loc1, 1);
                this.evRemoved.invoke([arg1, loc1]);
                this.update();
            }
            return;
        }

        public function child(arg1:*):lesta.datahub.Collection
        {
            return this.children[arg1];
        }

        public function getChildByPath(arg1:String):lesta.datahub.Collection
        {
            var loc2:*=null;
            var loc3:*=0;
            var loc1:*=this;
            if (arg1) 
            {
                loc2 = arg1.split(".");
                loc3 = 0;
                while (loc3 < loc2.length) 
                {
                    loc1 = loc1.child(loc2[loc3]);
                    lesta.utils.Debug.assert(!(loc1 == null), "[Fl] Collection: failed to get collection by path : " + arg1 + " No child: " + loc2[loc3]);
                    ++loc3;
                }
            }
            return loc1;
        }

        public function createChild(arg1:*):lesta.datahub.Collection
        {
            lesta.utils.Debug.assert(!this.children[arg1], "[Fl] Attempt to create child collection id=" + arg1 + ", but it is already created!");
            var loc1:*=new lesta.datahub.Collection();
            this.children[arg1] = loc1;
            return loc1;
        }

        public function createSorting(arg1:*):lesta.datahub.SortedCollection
        {
            lesta.utils.Debug.assert(!this.children[arg1], "[Fl] Attempt to create sorting of collection id=" + arg1 + ", but it is already created!");
            var loc1:*=new lesta.datahub.SortedCollection();
            this.children[arg1] = loc1;
            return loc1;
        }

        public function createFiltering(arg1:*):lesta.datahub.FilteredCollection
        {
            lesta.utils.Debug.assert(!this.children[arg1], "[Fl] Attempt to create filtering of collection id=" + arg1 + ", but it is already created!");
            var loc1:*=new lesta.datahub.FilteredCollection();
            this.children[arg1] = loc1;
            return loc1;
        }

        public function removeChild(arg1:*):void
        {
            lesta.utils.Debug.assert(this.children[arg1], "[Fl] Attempt to remove child collection id=" + arg1 + ", which does not exist!");
            delete this.children[arg1];
            return;
        }

        protected function update():void
        {
            dispatchEvent(new flash.events.Event(flash.events.Event.CHANGE));
            return;
        }

        public var items:Array;

        public var evAdded:lesta.utils.Invoker;

        public var evRemoved:lesta.utils.Invoker;

        public var evMoved:lesta.utils.Invoker;

        private var children:flash.utils.Dictionary;
    }
}
