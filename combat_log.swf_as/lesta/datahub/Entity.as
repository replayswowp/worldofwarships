package lesta.datahub 
{
    import flash.events.*;
    import flash.utils.*;
    import lesta.utils.*;
    
    public final dynamic class Entity extends flash.events.EventDispatcher
    {
        public function Entity(arg1:uint)
        {
            this.components = new flash.utils.Dictionary(true);
            super();
            this.id = arg1;
            return;
        }

        public function fini():void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=this.components;
            for each (loc1 in loc3) 
            {
                loc1.fini();
            }
            this.components = null;
            return;
        }

        public function addComponent(arg1:lesta.datahub.Component):void
        {
            lesta.utils.Debug.assert(!this.components[arg1.classID], "[Fl] Attempt to add component #" + arg1.classID + ", to entity #" + this.id + ", but it already has such component!");
            arg1.entity = this;
            this.components[arg1.classID] = arg1;
            this[arg1.className] = arg1;
            dispatchEvent(new flash.events.Event(flash.events.Event.CHANGE));
            return;
        }

        public function getComponent(arg1:uint):lesta.datahub.Component
        {
            return this.components[arg1];
        }

        public function removeComponent(arg1:uint):void
        {
            lesta.utils.Debug.assert(this.components[arg1], "[Fl] Attempt to remove component #" + arg1 + ", from entity #" + this.id + ", but it has no such component!");
            var loc1:*=this.components[arg1];
            delete this.components[arg1];
            delete this[loc1.className];
            loc1.fini();
            dispatchEvent(new flash.events.Event(flash.events.Event.CHANGE));
            return;
        }

        public override function toString():String
        {
            var loc2:*=null;
            var loc1:*=[];
            var loc3:*=0;
            var loc4:*=this.components;
            for (loc2 in loc4) 
            {
                loc1.push(this.components[loc2].className);
            }
            return lesta.utils.Debug.formatString("[Entity %: %]", [this.id, loc1.toString()]);
        }

        private var components:flash.utils.Dictionary;

        public var id:uint=0;
    }
}
