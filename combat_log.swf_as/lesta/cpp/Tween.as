package lesta.cpp 
{
    import flash.external.*;
    
    public class Tween extends Object
    {
        public function Tween()
        {
            super();
            return;
        }

        public static function to(arg1:Object, arg2:Number, arg3:Object, arg4:Object, arg5:Function=null, arg6:int=0, arg7:Number=0, arg8:Function=null):int
        {
            return flash.external.ExternalInterface.call(lesta.cpp.Definitions.TWEEN_EXTENSION_CALL, lesta.cpp.Definitions.TWEEN_METHOD_TO, arg1, arg5 == null ? null : new CallbackWrapper(arg5).execute, arg2, arg7, arg6, arg3, arg4, arg8 == null ? null : new CallbackWrapper(arg8).execute);
        }

        public static function kill(arg1:int):void
        {
            flash.external.ExternalInterface.call(lesta.cpp.Definitions.TWEEN_EXTENSION_CALL, lesta.cpp.Definitions.TWEEN_METHOD_KILL, arg1);
            return;
        }

        
        {
            listTimer = [];
            tweenInInc = 0;
            mapDebugTweens = [];
        }

        public static const EASE_LINEAR:int=0;

        public static const EASE_OUT_QUAD:int=1;

        public static const EASE_IN_QUAD:int=2;

        public static const EASE_IN_OUT_QUAD:int=3;

        private static var listTimer:Array;

        private static var tweenInInc:int=0;

        private static var mapDebugTweens:Array;
    }
}


class CallbackWrapper extends Object
{
    public function CallbackWrapper(arg1:Function)
    {
        super();
        this.callback = arg1;
        return;
    }

    public function execute():void
    {
        this.callback();
        return;
    }

    private var callback:Function;
}