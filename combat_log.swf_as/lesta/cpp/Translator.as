package lesta.cpp 
{
    import flash.external.*;
    
    public class Translator extends Object
    {
        public function Translator()
        {
            super();
            return;
        }

        public static function set localizations(arg1:Object):void
        {
            _localizations = arg1;
            return;
        }

        public static function getLocalization(arg1:String):String
        {
            if (!_localizations) 
            {
                return arg1;
            }
            if (!_localizations.hasOwnProperty(arg1)) 
            {
                return arg1;
            }
            return _localizations[arg1];
        }

        public static function translate(arg1:String):String
        {
            return flash.external.ExternalInterface.call(lesta.cpp.Definitions.TRANSLATOR_EXTENSION_CALL, lesta.cpp.Definitions.TRANSLATOR_METHOD_TRANSLATE, arg1);
        }

        public static function translatePlural(arg1:String, arg2:int):String
        {
            return flash.external.ExternalInterface.call(lesta.cpp.Definitions.TRANSLATOR_EXTENSION_CALL, lesta.cpp.Definitions.TRANSLATOR_METHOD_TRANSLATE_PLURAL, arg1, arg2);
        }

        private static var _localizations:Object;
    }
}
