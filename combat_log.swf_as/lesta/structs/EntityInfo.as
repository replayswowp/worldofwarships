package lesta.structs 
{
    import flash.events.*;
    import flash.geom.*;
    
    public class EntityInfo extends flash.events.EventDispatcher
    {
        public function EntityInfo()
        {
            this.worldPos = new flash.geom.Vector3D();
            this.screenPos = new flash.geom.Point();
            super();
            return;
        }

        public static const IS_ALIVE:String="isAlive";

        public var entityId:int=0;

        public var id:int=0;

        public var relation:uint=1;

        public var isAlive:Boolean=true;

        public var worldPos:flash.geom.Vector3D;

        public var worldYaw:Number=0;

        public var screenPos:flash.geom.Point;

        public var isBehindCamera:Boolean=false;

        public var isSelected:Boolean=false;

        public var isVisible:Boolean=false;

        public var scaleIcon:Number=1;

        public var distanceToShip:Number=0;

        public var distanceToCamera:Number=0;

        public var isActive:Boolean=true;

        public var isOnScreen:Boolean=false;

        public var isControllable:Boolean=true;

        public var isConsumable:Boolean=false;
    }
}
