package lesta.structs 
{
    public class AchievementInfo extends Object
    {
        public function AchievementInfo()
        {
            super();
            return;
        }

        public function update():void
        {
            this.condition = this.stages[0].conditions[0];
            this.description = this.condition.description;
            this.imageURL = IMAGE_PATH + this.name + IMAGE_EXTENSION;
            this.imageDesURL = IMAGE_PATH + this.name + IMAGE_DES_EXTENSION;
            return;
        }

        public function toString():String
        {
            return "AchievementInfo{name=" + this.name + ",id=" + String(this.id) + ",onePerBattle=" + String(this.onePerBattle) + ",subtype=" + String(this.subtype) + ",amount=" + String(this.amount) + ",condition=" + String(this.condition.description) + "}";
        }

        private static const IMAGE_PATH:String="../achievements/icons/icon_achievement_";

        private static const IMAGE_EXTENSION:String=".png";

        private static const IMAGE_DES_EXTENSION:String="_des.png";

        public var id:Number=0;

        public var subtype:int=0;

        public var enabled:Boolean=false;

        public var name:String;

        public var nameIDS:String;

        public var nameLocalized:String;

        public var subtypeIDS:String;

        public var group:Boolean=false;

        public var multiple:Boolean=false;

        public var onePerBattle:Boolean=false;

        public var hasReward:Boolean=false;

        public var rewardEnabled:Boolean=false;

        public var rewardConditionIDS:String="";

        public var multipleIDS:String="";

        public var stages:Array;

        public var amount:int=0;

        public var timestamp:int=0;

        public var achieveTime:String="";

        public var achieveShip:Number=0;

        public var isDock:Boolean=false;

        public var showProgress:Boolean=false;

        public var condition:Object;

        public var description:String="";

        public var imageURL:String="";

        public var imageDesURL:String="";
    }
}
