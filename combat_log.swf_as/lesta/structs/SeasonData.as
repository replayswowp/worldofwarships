package lesta.structs 
{
    public class SeasonData extends Object
    {
        public function SeasonData()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "SeasonData{id=" + this.id + ",startRank=" + String(this.startRank) + ",shipLevelMin=" + String(this.shipLevelMin) + ",shipLevelMax=" + String(this.shipLevelMax) + ",Start=" + String(this.startTimeString) + ",Finish=" + String(this.finishTimeString) + "}";
        }

        public var id:int=0;

        public var title:String;

        public var accountLevel:int;

        public var shipLevelMin:int;

        public var shipLevelMax:int;

        public var shipLevelMinRome:String;

        public var shipLevelMaxRome:String;

        public var startTime:int;

        public var finishTime:int;

        public var startTimeString:String;

        public var finishTimeString:String;

        public var startRank:int;

        public var active:Boolean;

        public var denyReason:uint;

        public var allowedMyShipsDescription:String;

        public var allowedMyLevelDescription:String;

        public var allowedTheirsShipsDescription:String;

        public var allowedTheirsLevelDescription:String;

        public var messageEndSeason:String;

        public var stage:int;

        public var rankToLeagueDict:Object;
    }
}
