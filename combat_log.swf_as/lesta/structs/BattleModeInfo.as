package lesta.structs 
{
    public class BattleModeInfo extends Object
    {
        public function BattleModeInfo()
        {
            super();
            return;
        }

        public function clear():void
        {
            this.helpText = "";
            this.loadingProgress = 0;
            this.battleTimeLeft = 0;
            this.battleTimeLeftText = "";
            this.battleState = 0;
            this.readyToBattle = false;
            return;
        }

        public var modeId:String;

        public var mapName:String;

        public var minimapPath:String;

        public var bitmapId:int;

        public var initialScore:int;

        public var teamWinScore:int;

        public var teamLoseScore:int;

        public var shipRewardMap:Object;

        public var shipPenaltyMap:Object;

        public var shipRewardList:Array;

        public var shipPenaltyList:Array;

        public var rewardForCaptureMsg:String;

        public var rewardForHoldMsg:String;

        public var victoryCondition:String;

        public var defeatCondition:String;

        public var helpText:String="";

        public var loadingProgress:Number=0;

        public var tabIndex:Number=0;

        public var battleTimeLeft:Number=0;

        public var battleTimeLeftText:String="";

        public var battleState:Number=0;

        public var readyToBattle:Boolean=false;
    }
}
