package lesta.structs 
{
    import lesta.utils.*;
    
    public class UpgradableShipsModuleInfo extends Object
    {
        public function UpgradableShipsModuleInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return lesta.utils.Debug.formatString("[UpgradableShipsModuleInfo name: % shipId: %]", [this.name, this.shipId]);
        }

        public static function getTypeByName(arg1:String):String
        {
            arg1 = arg1.toUpperCase();
            var loc1:*=arg1.charAt(arg1.indexOf("U") + 1);
            return MODULE_TYPES[loc1];
        }

        public static function getInvalidModuleInfo():lesta.structs.UpgradableShipsModuleInfo
        {
            var loc1:*=new UpgradableShipsModuleInfo();
            loc1.name = "INVALID";
            loc1.nameIDS = "IDS_INVALID";
            loc1.type = "INVALID";
            loc1.typeIDS = "IDS_INVALID";
            return loc1;
        }

        public static const MODULE_TYPES:Object={"H":"hull", "A":"artillery", "S":"suo", "T":"torpedoes", "E":"engine", "P":"planes", "F":"flightControl", "I":"fighter", "B":"torpedoBomber", "D":"diveBomber"};

        public var id:Number;

        public var name:String;

        public var type:String;

        public var typeIDS:String;

        public var prevName:String;

        public var dependentModules:Array;

        public var nextShipNames:Array;

        public var depth:Number;

        public var costXP:int;

        public var costCR:int;

        public var totalCostCR:int;

        public var costFreeXP:int;

        public var totalCostXP:int;

        public var isInstalled:Boolean;

        public var unableToInstall:Boolean;

        public var unableToInstallReason:int;

        public var count:Object;

        public var isStock:Boolean;

        public var toExplore:Boolean;

        public var toPurchase:Boolean;

        public var toInstall:Boolean;

        public var nameIDS:String;

        public var dependencyResearched:Boolean;

        public var expDeficit:int;

        public var creditsDeficit:int;

        public var params:Object;

        public var shipParamsWithThisModule:Object;

        public var neededModules:Array;

        public var toBuyList:Array;

        public var shipId:Number;

        public var isExplored:Boolean;

        public var unableToInstallIncompatibles:Array;
    }
}
