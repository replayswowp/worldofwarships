package lesta.structs 
{
    import lesta.utils.*;
    
    public class NavpointsSettings extends Object
    {
        public function NavpointsSettings()
        {
            this.mapColors = {};
            super();
            return;
        }

        public function toString():String
        {
            var loc1:*="[NavpointsSettings : countActive = %, alphaUnactive = %, alphaActive = %, lineThickness = %, dotLength = %, spaceLength = %]";
            loc1 = lesta.utils.Debug.formatString(loc1, [this.countActiveNavpoints, this.alphaUnactiveNavpoint, this.alphaActiveNavpoint, this.lineThickness, this.dotLength, this.spaceLength]);
            return loc1;
        }

        public var mapColors:Object;

        public var countActiveNavpoints:int=0;

        public var alphaUnactiveNavpoint:Number=0;

        public var alphaActiveNavpoint:Number=0;

        public var lineThickness:Number=1;

        public var dotLength:Number=1;

        public var spaceLength:Number=4;
    }
}
