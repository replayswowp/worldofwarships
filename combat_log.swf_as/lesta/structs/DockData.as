package lesta.structs 
{
    public class DockData extends Object
    {
        public function DockData()
        {
            super();
            return;
        }

        public var loaded:Boolean;

        public var shipId:Number;

        public var shipIdForModuleInset:Number;

        public var serverName:String;

        public var playerName:String;

        public var karma:int;

        public var is3DMode:Boolean;

        public var battleType:String;

        public var offerSurvey:Boolean=false;

        public var paramsConfig:Object;

        public var mapListParams:Object;

        public var enteredTypeBalancer:int=-1;

        public var goToBattleState:Boolean=false;

        public var tkStatus:Boolean=false;

        public var carouselVisibility:Boolean=true;

        public var introMissionEnabled:Boolean=false;

        public var introMissionDescription:String;
    }
}
