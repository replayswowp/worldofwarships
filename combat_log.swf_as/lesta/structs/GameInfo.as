package lesta.structs 
{
    public class GameInfo extends Object
    {
        public function GameInfo()
        {
            super();
            return;
        }

        public function clear():void
        {
            this.alliedFrags = 0;
            this.enemyFrags = 0;
            this.gameMode = "";
            this.gameModeDescription = "";
            this.duration = 0;
            return;
        }

        public var duration:Number=0;

        public var gameModeId:int=0;

        public var gameMode:String="";

        public var gameVersion:String="";

        public var battleType:String;

        public var gameModeDescription:String="";

        public var alliedFrags:int=0;

        public var enemyFrags:int=0;
    }
}
