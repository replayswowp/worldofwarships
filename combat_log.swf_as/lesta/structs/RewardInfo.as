package lesta.structs 
{
    public class RewardInfo extends Object
    {
        public function RewardInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "RewardInfo{name=" + this.name + ",id=" + String(this.id) + ",amount=" + String(this.amount) + ",reasons=" + this.reasons + "}";
        }

        private static const IMAGE_PATH:String="/gui/achievements/icons/icon_achievement_";

        private static const IMAGE_EXTENSION:String=".png";

        public static const COMMON:uint=0;

        public static const SPECIAL:uint=1;

        public var id:String="";

        public var category:uint=0;

        public var amount:Number=0;

        public var reasons:Array;

        public var name:String;

        public var type:String;

        public var subtype:String;

        public var params:Object;
    }
}
