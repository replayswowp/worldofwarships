package lesta.structs 
{
    public class UIRealmSettings extends Object
    {
        public function UIRealmSettings()
        {
            super();
            return;
        }

        public var loginRestrict:String;

        public var nicknameRestrict:String;

        public var passRestrict:String;

        public var showEulaText:Boolean;

        public var eulaText:String;

        public var sendTimeSpentMessage:Boolean;

        public var wowsLogo:String;

        public var showShipTreeBg:Boolean;

        public var showKongzhong:Boolean;

        public var showPlayersCountOnServer:Boolean;

        public var currentRealm:String;

        public var licenseLogoUrls:Array;
    }
}
