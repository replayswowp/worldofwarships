package lesta.structs 
{
    import lesta.utils.*;
    
    public class WeaponsInfo extends Object
    {
        public function WeaponsInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return lesta.utils.Debug.formatString("[WeaponsInfo slotId : %, title : %, weaponType : %, description : %, paramsDescriptor : %]", [this.slotId, this.title, this.weaponType, this.description, this.paramsDescriptor]);
        }

        public var id:Number;

        public var slotId:int;

        public var weaponType:int;

        public var paramsDescriptor:String;

        public var iconPath:String;

        public var title:String;

        public var description:String;

        public var commandId:int;

        public var installed:Boolean=true;

        public var params:Object;
    }
}
