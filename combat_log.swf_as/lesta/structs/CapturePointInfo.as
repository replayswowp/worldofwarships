package lesta.structs 
{
    import lesta.utils.*;
    
    public class CapturePointInfo extends lesta.structs.EntityInfo
    {
        public function CapturePointInfo(arg1:int=-1, arg2:int=-1, arg3:int=-1, arg4:Number=0, arg5:Number=0, arg6:Boolean=false, arg7:Number=0, arg8:Number=0, arg9:Number=0, arg10:int=0, arg11:int=0, arg12:Boolean=true, arg13:Boolean=false)
        {
            super();
            this.id = arg1;
            this.ownerRelation = arg2;
            this.invaderRelation = arg3;
            this.captureProgress = arg4;
            this.lockProgress = arg5;
            this.isLocked = arg6;
            this.posX = arg7;
            this.posY = arg8;
            this.radius = arg9;
            this.active = arg12;
            this.bothInside = arg13;
            return;
        }

        public override function toString():String
        {
            return lesta.utils.Debug.formatString("[CapturePointInfo id: %, ownerRelation: %, invaderRelation: % captureProgress: % lockProgress: % isLocked: % active: %]", [id, this.ownerRelation, this.invaderRelation, this.captureProgress, this.lockProgress, this.isLocked, this.active]);
        }

        public var type:int=-1;

        public var ownerRelation:int=-1;

        public var invaderRelation:int=-1;

        public var captureProgress:Number=0;

        public var lockProgress:Number=0;

        public var isLocked:Boolean=false;

        public var posX:Number=0;

        public var posY:Number=0;

        public var radius:Number=0;

        public var active:Boolean=true;

        public var visible:Boolean=true;

        public var timerName:String="";

        public var bothInside:Boolean=false;

        public var scale:Number=1;
    }
}
