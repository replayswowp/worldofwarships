package lesta.structs 
{
    public class DailyQuestInfo extends Object
    {
        public function DailyQuestInfo()
        {
            super();
            return;
        }

        public function toString():String
        {
            return "DailyQuestInfo{id=" + String(this.id) + ",type=" + String(this.type) + ",typeText=" + String(this.typeText) + ",backgroundURL=" + String(this.backgroundURL) + ",isSeen=" + String(this.isSeen) + ",isActive=" + String(this.isActive) + ",emptyType=" + String(this.emptyType) + ",timeLeft=" + String(this.timeLeft) + "}";
        }

        public var id:int=0;

        public var questId:int=0;

        public var type:String="quest";

        public var titleText:String="";

        public var descriptionText:String="";

        public var rewardType:int=0;

        public var rewardAddType:int=0;

        public var rewardNum:Number=0;

        public var progressMax:Number=0;

        public var progressCurrent:Number=0;

        public var backgroundURL:String="";

        public var isSeen:Boolean=false;

        public var isActive:Boolean=true;

        public var day:int=1;

        public var month:int=1;

        public var monthIDS:String="IDS_MONTH_1";

        public var timeLeftFormatted:String="";

        public var timeLeft:Number=0;

        public var timeToChange:Number=0;

        public var inOneBattle:Boolean=false;

        public var typeText:String="IDS_DAILY_QUESTS";

        public var emptyType:int=0;

        public var iconId:int=0;
    }
}
