package lesta.structs 
{
    public class ServerTimer extends Object
    {
        public function ServerTimer()
        {
            super();
            return;
        }

        public function get remainingTime():Number
        {
            return this._remainingTime;
        }

        public function set remainingTime(arg1:Number):void
        {
            this.resetTimer();
            this._remainingTime = arg1;
            return;
        }

        public function get enabled():Boolean
        {
            return this._enabled;
        }

        public function set enabled(arg1:Boolean):void
        {
            if (this._enabled != arg1) 
            {
                if (arg1) 
                {
                    this._pauseDuration = this._pauseDuration + (new Date().getTime() - this._pauseTime);
                    this._pauseTime = 0;
                }
                else 
                {
                    this._pauseTime = new Date().getTime();
                }
            }
            this._enabled = arg1;
            return;
        }

        public function getTimeLeft():Number
        {
            var loc1:*=this._remainingTime * 1000 + this.pauseTime - new Date().getTime() + this._creationTime;
            return loc1 > 0 ? loc1 : 0;
        }

        private function get pauseTime():Number
        {
            return this._pauseDuration + (this._enabled ? 0 : new Date().getTime() - this._pauseTime);
        }

        private function resetTimer():void
        {
            this._creationTime = new Date().getTime();
            this._pauseDuration = 0;
            if (!this._enabled) 
            {
                this._pauseTime = this._creationTime;
            }
            return;
        }

        public function toString():String
        {
            return "[ServerTimer] -" + " name: " + this.id + " T: " + this.time + " rT: " + this._remainingTime + " pT: " + this.pauseTime + " getTimeLeft: " + this.getTimeLeft();
        }

        public var id:String="";

        public var time:Number=0;

        private var _remainingTime:Number=0;

        private var _enabled:Boolean=true;

        private var _pauseTime:Number=0;

        private var _pauseDuration:Number=0;

        private var _creationTime:Number=0;
    }
}
