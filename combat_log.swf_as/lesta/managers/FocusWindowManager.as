package lesta.managers 
{
    import __AS3__.vec.*;
    import flash.utils.*;
    import scaleform.clik.core.*;
    
    public class FocusWindowManager extends Object
    {
        public function FocusWindowManager()
        {
            this.mapComponents = new flash.utils.Dictionary();
            this.listComponents = new Vector.<ComponentInfo>();
            super();
            return;
        }

        public function register(arg1:scaleform.clik.core.UIComponent, arg2:int, arg3:Boolean):void
        {
            var loc1:*=new ComponentInfo();
            loc1.clikObj = arg1;
            loc1.tabIndex = arg2;
            loc1.defaultFocused = arg3;
            loc1.init();
            this.mapComponents[arg1] = loc1;
            this.listComponents.push(loc1);
            var loc2:*;
            var loc3:*=((loc2 = this).countComponents + 1);
            loc2.countComponents = loc3;
            return;
        }

        public function update(arg1:scaleform.clik.core.UIComponent, arg2:int, arg3:Boolean):void
        {
            var loc1:*=this.mapComponents[arg1];
            loc1.clikObj = arg1;
            loc1.tabIndex = arg2;
            loc1.defaultFocused = arg3;
            loc1.init();
            return;
        }

        public function unregister(arg1:scaleform.clik.core.UIComponent):void
        {
            var loc1:*=this.mapComponents[arg1];
            var loc2:*=this.listComponents.indexOf(arg1);
            delete this.mapComponents[arg1];
            this.listComponents.splice(loc2, 1);
            var loc3:*;
            var loc4:*=((loc3 = this).countComponents - 1);
            loc3.countComponents = loc4;
            loc1.fini();
            return;
        }

        public function fini():void
        {
            var loc1:*=0;
            while (loc1 < this.countComponents) 
            {
                this.listComponents[loc1].fini();
                ++loc1;
            }
            this.listComponents.length = 0;
            this.countComponents = 0;
            return;
        }

        public function handleWindowOnStartShow():void
        {
            var loc1:*=0;
            while (loc1 < this.countComponents) 
            {
                this.listComponents[loc1].handleWindowOnStartShow();
                ++loc1;
            }
            return;
        }

        public function handleWindowOnStartHide():void
        {
            var loc1:*=0;
            while (loc1 < this.countComponents) 
            {
                this.listComponents[loc1].handleWindowOnStartHide();
                ++loc1;
            }
            return;
        }

        public function handleWindowOnBecomeTop():void
        {
            var loc1:*=0;
            while (loc1 < this.countComponents) 
            {
                this.listComponents[loc1].handleWindowOnBecomeTop();
                ++loc1;
            }
            return;
        }

        public function handleWindowOnLostTop():void
        {
            var loc1:*=0;
            while (loc1 < this.countComponents) 
            {
                this.listComponents[loc1].handleWindowOnLostTop();
                ++loc1;
            }
            return;
        }

        private var mapComponents:flash.utils.Dictionary;

        private var listComponents:__AS3__.vec.Vector.<ComponentInfo>;

        private var countComponents:int=0;
    }
}

import flash.display.*;
import flash.events.*;
import scaleform.clik.controls.*;
import scaleform.clik.core.*;
import scaleform.clik.managers.*;


class ComponentInfo extends Object
{
    public function ComponentInfo()
    {
        super();
        return;
    }

    public function init():void
    {
        this.lastHasFocus = this.defaultFocused;
        this.updateState();
        if (this.defaultFocused) 
        {
            this.clikObj.addEventListener(flash.events.Event.ADDED_TO_STAGE, this.handleObjAddedToStage);
        }
        return;
    }

    public function fini():void
    {
        if (this.clikObj) 
        {
            this.clikObj.removeEventListener(flash.events.Event.ADDED_TO_STAGE, this.handleObjAddedToStage);
            this.clikObj = null;
        }
        return;
    }

    public function handleWindowOnStartShow():void
    {
        this.updateState();
        return;
    }

    public function handleWindowOnStartHide():void
    {
        this.lastHasFocus = this.clikObj.hasFocus;
        return;
    }

    public function handleWindowOnBecomeTop():void
    {
        var loc1:*=null;
        if (this.clikObj) 
        {
            this.clikObj.focusable = true;
            this.clikObj.tabIndex = this.tabIndex;
            loc1 = this.clikObj is scaleform.clik.controls.TextInput ? (this.clikObj as scaleform.clik.controls.TextInput).textField : this.clikObj;
            if (this.lastHasFocus) 
            {
                scaleform.clik.managers.FocusHandler.getInstance().setFocus(loc1);
            }
        }
        return;
    }

    public function handleWindowOnLostTop():void
    {
        if (this.clikObj) 
        {
            this.lastHasFocus = this.clikObj.hasFocus;
            this.clikObj.tabIndex = 0;
            this.clikObj.focusable = false;
            this.clikObj.validateNow();
            scaleform.clik.managers.FocusHandler.getInstance().setFocus(null);
        }
        return;
    }

    private function handleObjAddedToStage(arg1:flash.events.Event):void
    {
        scaleform.clik.managers.FocusHandler.getInstance().setFocus(this.clikObj);
        return;
    }

    private function updateState():void
    {
        this.clikObj.focusable = true;
        this.clikObj.tabIndex = this.tabIndex;
        if (this.lastHasFocus) 
        {
            scaleform.clik.managers.FocusHandler.getInstance().setFocus(this.clikObj);
        }
        return;
    }

    public var clikObj:scaleform.clik.core.UIComponent;

    public var tabIndex:int=0;

    public var defaultFocused:Boolean=false;

    public var lastHasFocus:Boolean=false;
}