package lesta.managers 
{
    import flash.utils.*;
    import lesta.cpp.*;
    
    public class TweenManager extends Object
    {
        public function TweenManager()
        {
            super();
            return;
        }

        public static function addTween(arg1:Object, arg2:Number, arg3:Object, arg4:Object, arg5:Function=null, arg6:int=0, arg7:Number=0, arg8:Function=null):int
        {
            var object:Object;
            var duration:Number;
            var startProps:Object;
            var finishProps:Object;
            var callback:Function=null;
            var easing:int=0;
            var delay:Number=0;
            var onTick:Function=null;
            var id:int;
            var proxyCallback:Function;

            var loc1:*;
            id = 0;
            proxyCallback = null;
            object = arg1;
            duration = arg2;
            startProps = arg3;
            finishProps = arg4;
            callback = arg5;
            easing = arg6;
            delay = arg7;
            onTick = arg8;
            proxyCallback = function (... rest):void
            {
                removeTween(object, id);
                if (callback != null) 
                {
                    callback.apply(object, rest);
                }
                return;
            }
            if (dicTweens[object] == undefined) 
            {
                dicTweens[object] = [];
            }
            id = lesta.cpp.Tween.to(object, duration, startProps, finishProps, proxyCallback, easing, delay, onTick);
            dicTweens[object].push(id);
            return id;
        }

        public static function removeTween(arg1:Object, arg2:int):void
        {
            var loc1:*=0;
            if (dicTweens[arg1] != undefined) 
            {
                loc1 = dicTweens[arg1].indexOf(arg2);
                if (loc1 > -1) 
                {
                    dicTweens[arg1].splice(loc1, 1);
                }
            }
            lesta.cpp.Tween.kill(arg2);
            return;
        }

        public static function removeAllTweens(arg1:Object):void
        {
            var loc1:*=null;
            if (dicTweens[arg1] != undefined) 
            {
                loc1 = dicTweens[arg1];
                while (loc1.length) 
                {
                    lesta.cpp.Tween.kill(loc1.pop());
                }
            }
            return;
        }

        public static function clearObject(arg1:Object):void
        {
            removeAllTweens(arg1);
            delete dicTweens[arg1];
            return;
        }

        public static function addTweenExplicitly(arg1:Object, arg2:Number, arg3:Object, arg4:Object, arg5:Function=null, arg6:int=0, arg7:Number=0, arg8:Function=null):int
        {
            removeAllTweens(arg1);
            return addTween(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8);
        }

        public static function hasTween(arg1:Object):Boolean
        {
            if (!(dicTweens[arg1] == undefined) && dicTweens[arg1].length > 0) 
            {
                return true;
            }
            return false;
        }

        
        {
            dicTweens = new flash.utils.Dictionary(true);
        }

        private static var dicTweens:flash.utils.Dictionary;
    }
}
