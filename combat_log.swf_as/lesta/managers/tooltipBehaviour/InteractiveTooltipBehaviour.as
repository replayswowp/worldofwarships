package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    
    public class InteractiveTooltipBehaviour extends lesta.managers.tooltipBehaviour.SimpleTooltipBehaviour
    {
        public function InteractiveTooltipBehaviour()
        {
            super();
            bottomOffset = 0;
            rightOffset = 0;
            tinyOffset = 15;
            return;
        }

        public override function getName():String
        {
            return "interactive";
        }

        protected override function placeTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):void
        {
            var loc1:*=NaN;
            var loc2:*=NaN;
            var loc3:*=NaN;
            var loc4:*=null;
            var loc5:*=false;
            if (isValidTooltip(arg2)) 
            {
                loc1 = arg2.getStyleProperty("tinyOffset", tinyOffset);
                loc2 = arg2.getStyleProperty("rightOffset", rightOffset);
                loc3 = arg2.getStyleProperty("bottomOffset", bottomOffset);
                loc4 = getTargetRect(arg2);
                loc5 = loc4.bottom + arg2.tipInstance.height + tinyOffset > arg1.stage.stageHeight;
                loc5 = arg2.getStyleProperty("isTop", loc5);
                arg2.tipInstance.x = loc4.left + (loc4.width - arg2.tipInstance.width) / 2 + loc2;
                arg2.tipInstance.x = Math.max(loc1, Math.min(arg2.tipInstance.x, arg1.stage.stageWidth - loc1 - arg2.tipInstance.width));
                arg2.tipInstance.y = loc3 + (loc5 ? loc4.top - arg2.tipInstance.height : loc4.bottom);
                placeTooltipPin(arg1, arg2, arg3, DIRECTION_VERTICAL, loc5);
            }
            return;
        }

        public override function onReasonRollOver(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            showTooltip(arg1, arg2, arg3);
            arg2.tipInstance.mouseChildren = false;
            arg2.tipInstance.mouseEnabled = false;
            return true;
        }

        public override function onReasonMouseMove(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return arg2.visible;
        }

        public override function onReasonMouseDown(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            hideTooltip(arg2);
            return false;
        }

        public override function onTooltipResize(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            return arg2.visible;
        }
    }
}
