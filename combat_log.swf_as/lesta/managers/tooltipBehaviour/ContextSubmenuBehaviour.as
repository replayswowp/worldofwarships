package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import lesta.managers.*;
    
    public class ContextSubmenuBehaviour extends lesta.managers.tooltipBehaviour.SimpleTooltipBehaviour
    {
        public function ContextSubmenuBehaviour()
        {
            super();
            screenTopOffset = 0;
            tinyOffset = 0;
            rightOffset = 0;
            bottomOffset = 0;
            return;
        }

        public override function getName():String
        {
            return "submenu";
        }

        public override function showTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent, arg4:Boolean=false):void
        {
            if (lesta.managers.TweenManager.hasTween(arg2.tipInstance)) 
            {
                lesta.managers.TweenManager.removeAllTweens(arg2.tipInstance);
                arg2.beforeShowCallback();
                arg2.visible = true;
                arg2.addTooltipToStage();
                arg2.tipInstance.visible = true;
                arg2.validateTooltipSize();
                arg2.tipInstance.alpha = 1;
                this.placeTooltip(arg1, arg2, arg3);
                arg2.showCallback();
            }
            else 
            {
                super.showTooltip(arg1, arg2, arg3, arg4);
            }
            return;
        }

        protected override function placeTooltip(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):void
        {
            var loc1:*=null;
            var loc2:*=false;
            var loc3:*=NaN;
            var loc4:*=NaN;
            var loc5:*=NaN;
            if (isValidTooltip(arg2)) 
            {
                loc1 = getTargetRect(arg2);
                loc2 = loc1.right + arg2.tipInstance.width + tinyOffset > arg1.stage.stageWidth;
                loc3 = arg2.getStyleProperty("tinyOffset", tinyOffset);
                loc4 = arg2.getStyleProperty("rightOffset", rightOffset);
                loc5 = arg2.getStyleProperty("bottomOffset", bottomOffset);
                arg2.tipInstance.y = loc5 + loc1.top;
                arg2.tipInstance.y = Math.max(loc3, Math.min(arg2.tipInstance.y, arg1.stage.stageHeight - loc3 - arg2.tipInstance.height));
                arg2.tipInstance.x = loc2 ? loc1.left - arg2.tipInstance.width - loc4 : loc1.right + loc4;
                placeTooltipPin(arg1, arg2, arg3, DIRECTION_HORIZONTAL, loc2);
            }
            return;
        }

        public override function onTooltipRollOver(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            this.showTooltip(arg1, arg2, arg3);
            return true;
        }

        public override function onTooltipRollOut(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            hideTooltip(arg2);
            return false;
        }

        public override function onReasonRollOut(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            var loc1:*=arg2.tipInstance.getBounds(arg2.tipInstance).contains(arg2.tipInstance.mouseX, arg2.tipInstance.mouseY);
            if (loc1) 
            {
                return arg2.visible;
            }
            hideTooltip(arg2);
            return false;
        }

        public override function onReasonRollOver(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            this.showTooltip(arg1, arg2, arg3);
            return true;
        }

        public override function onOutsideMouseClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            hideTooltip(arg2);
            return false;
        }

        public override function onReasonClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            this.showTooltip(arg1, arg2, arg3);
            arg3.stopPropagation();
            return super.onReasonClick(arg1, arg2, arg3);
        }

        public override function onTooltipClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            arg3.stopPropagation();
            if (arg2.parentTooltip) 
            {
                arg2.parentTooltip.tipInstance.dispatchEvent(new flash.events.Event(flash.events.Event.CLOSE));
            }
            hideTooltip(arg2);
            return false;
        }

        public override function onTooltipResize(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            return arg2.visible;
        }

        public override function onReasonMouseMove(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return arg2.visible;
        }
    }
}
