package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    import lesta.interfaces.*;
    import lesta.utils.*;
    import scaleform.gfx.*;
    
    public class ContextMenuBehaviour extends lesta.managers.tooltipBehaviour.SimpleTooltipBehaviour
    {
        public function ContextMenuBehaviour()
        {
            super();
            appearDelay = 0;
            tinyOffset = 0;
            rightOffset = 0;
            bottomOffset = 0;
            return;
        }

        public override function getName():String
        {
            return "contextMenu";
        }

        public override function onOutsideMouseDown(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            hideTooltip(arg2);
            return false;
        }

        public override function onReasonMouseMove(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return arg2.visible;
        }

        public override function onReasonRollOut(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return arg2.visible;
        }

        public override function onTooltipClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            hideTooltip(arg2);
            return false;
        }

        public override function onTooltipResize(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.Event):Boolean
        {
            return arg2.visible;
        }

        public override function onReasonClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            var loc3:*=null;
            var loc1:*=true;
            if (this.isChatMessage(arg2.tooltipReason)) 
            {
                loc3 = this.castToChatMessage(arg2.tooltipReason).getData();
                loc1 = loc3.nickname && !(loc3.nickname == lesta.utils.GameInfoHolder.instance.selfPlayer.name);
            }
            var loc2:*=arg3 as scaleform.gfx.MouseEventEx;
            if (loc2.buttonIdx == scaleform.gfx.MouseEventEx.RIGHT_BUTTON && loc1) 
            {
                if (!arg2.visible) 
                {
                    showTooltip(arg1, arg2, arg3, true);
                }
                placeTooltip(arg1, arg2, arg3);
                arg3.stopPropagation();
                return true;
            }
            hideTooltip(arg2);
            return false;
        }

        private function isChatMessage(arg1:flash.display.DisplayObjectContainer):Boolean
        {
            return arg1 is lesta.interfaces.IMessageListItemRenderer;
        }

        private function castToChatMessage(arg1:flash.display.DisplayObjectContainer):lesta.interfaces.IMessageListItemRenderer
        {
            return arg1 as lesta.interfaces.IMessageListItemRenderer;
        }
    }
}
