package lesta.managers.tooltipBehaviour 
{
    import flash.display.*;
    import flash.events.*;
    
    public class HorizontalInfoWidgetBehaviour extends lesta.managers.tooltipBehaviour.InfotipBehaviour implements lesta.managers.tooltipBehaviour.ITooltipBehaviour
    {
        public function HorizontalInfoWidgetBehaviour()
        {
            super();
            return;
        }

        public override function getName():String
        {
            return "horizontalInfoWidget";
        }

        public override function onOutsideMouseClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            return arg2.visible;
        }

        public override function onTooltipClick(arg1:flash.display.DisplayObjectContainer, arg2:lesta.managers.tooltipBehaviour.TooltipInfo, arg3:flash.events.MouseEvent):Boolean
        {
            hideTooltip(arg2);
            return false;
        }
    }
}
