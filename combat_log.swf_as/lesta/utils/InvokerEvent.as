package lesta.utils 
{
    import flash.events.*;
    
    public class InvokerEvent extends flash.events.Event
    {
        public function InvokerEvent(arg1:String, arg2:Array)
        {
            super(arg1);
            this.args = arg2;
            return;
        }

        public static const ENVOKED:String="Invoked";

        public var args:Array;
    }
}
