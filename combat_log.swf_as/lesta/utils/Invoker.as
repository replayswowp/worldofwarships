package lesta.utils 
{
    import __AS3__.vec.*;
    import flash.events.*;
    
    public class Invoker extends flash.events.EventDispatcher
    {
        public function Invoker()
        {
            super();
            this.callbacks = new Vector.<Function>();
            return;
        }

        public function fini():void
        {
            this.callbacks = null;
            return;
        }

        public function invoke(arg1:Array):void
        {
            var loc1:*=(this.callbacks.length - 1);
            while (loc1 >= 0) 
            {
                this.callbacks[loc1].apply(null, arg1);
                --loc1;
            }
            dispatchEvent(new lesta.utils.InvokerEvent(lesta.utils.InvokerEvent.ENVOKED, arg1));
            return;
        }

        public function addCallback(arg1:Function):void
        {
            if (this.callbacks.indexOf(arg1) < 0) 
            {
                this.callbacks.push(arg1);
            }
            return;
        }

        public function removeCallback(arg1:Function):void
        {
            var loc1:*=this.callbacks.indexOf(arg1);
            if (loc1 >= 0) 
            {
                this.callbacks.splice(loc1, 1);
            }
            return;
        }

        public var parent:Object=null;

        private var callbacks:__AS3__.vec.Vector.<Function>;
    }
}
