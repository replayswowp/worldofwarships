package lesta.utils.resourceLoading 
{
    import flash.media.*;
    import flash.net.*;
    
    public class VideoResource extends lesta.utils.resourceLoading.LoadResourceJob
    {
        public function VideoResource(arg1:int, arg2:String)
        {
            super(arg1, arg2);
            loader = new lesta.utils.resourceLoading.VideoLoader();
            return;
        }

        public function get video():flash.media.Video
        {
            return loader.video;
        }

        public function get netStream():flash.net.NetStream
        {
            return loader.netStream;
        }
    }
}
