package lesta.utils.resourceLoading 
{
    import flash.events.*;
    import flash.media.*;
    import flash.net.*;
    import flash.system.*;
    
    public class VideoLoader extends Object
    {
        public function VideoLoader()
        {
            super();
            this._contentLoaderInfo = new flash.events.EventDispatcher();
            return;
        }

        public function get contentLoaderInfo():flash.events.EventDispatcher
        {
            return this._contentLoaderInfo;
        }

        public function close():void
        {
            this._netStream.close();
            return;
        }

        public function load(arg1:flash.net.URLRequest):void
        {
            this._video = new flash.media.Video();
            this.netConnection = new flash.net.NetConnection();
            this.netConnection.connect(null);
            this._netStream = new flash.net.NetStream(this.netConnection);
            this._netStream.client = {};
            this._netStream.client.onMetaData = this.onMetaData;
            this._netStream.addEventListener(flash.events.NetStatusEvent.NET_STATUS, this.onStatus);
            this._netStream.addEventListener(flash.events.IOErrorEvent.IO_ERROR, this.onIOError);
            this._video.attachNetStream(this._netStream);
            this._netStream.play(arg1.url);
            return;
        }

        public function unload():void
        {
            this.close();
            this.netConnection = null;
            this._netStream = null;
            this._video = null;
            return;
        }

        public function unloadAndStop(arg1:Boolean):void
        {
            this.unload();
            if (arg1) 
            {
                flash.system.System.gc();
            }
            return;
        }

        public function get netStream():flash.net.NetStream
        {
            return this._netStream;
        }

        public function get video():flash.media.Video
        {
            return this._video;
        }

        private function onStatus(arg1:flash.events.NetStatusEvent):void
        {
            if (arg1.info.code == "NetStream.Play.Start") 
            {
                this._netStream.removeEventListener(flash.events.NetStatusEvent.NET_STATUS, this.onStatus);
                this._netStream.removeEventListener(flash.events.IOErrorEvent.IO_ERROR, this.onIOError);
                this._contentLoaderInfo.dispatchEvent(new flash.events.Event(flash.events.Event.COMPLETE));
            }
            if (arg1.info.code == "NetStream.Play.StreamNotFound") 
            {
                this._contentLoaderInfo.dispatchEvent(new flash.events.IOErrorEvent(flash.events.IOErrorEvent.IO_ERROR));
            }
            return;
        }

        private function onIOError(arg1:flash.events.IOErrorEvent):void
        {
            this._contentLoaderInfo.dispatchEvent(new flash.events.IOErrorEvent(flash.events.IOErrorEvent.IO_ERROR));
            return;
        }

        private function onMetaData(arg1:*):void
        {
            return;
        }

        private var _netStream:flash.net.NetStream;

        private var netConnection:flash.net.NetConnection;

        private var _contentLoaderInfo:flash.events.EventDispatcher;

        private var _video:flash.media.Video;
    }
}
