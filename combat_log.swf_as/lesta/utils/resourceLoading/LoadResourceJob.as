package lesta.utils.resourceLoading 
{
    import flash.events.*;
    import flash.net.*;
    import lesta.data.*;
    import lesta.utils.*;
    
    public class LoadResourceJob extends lesta.utils.resourceLoading.JobBaseClass implements lesta.utils.resourceLoading.IJob
    {
        public function LoadResourceJob(arg1:int, arg2:String)
        {
            super(arg1);
            this.url = arg2;
            return;
        }

        function fini():void
        {
            this.loader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, this.onLoadComplete);
            this.loader.contentLoaderInfo.removeEventListener(flash.events.IOErrorEvent.IO_ERROR, this.onIOError);
            this.loader.unload();
            this.loader = null;
            return;
        }

        public function doJob():void
        {
            this.loadResource();
            return;
        }

        public function get result():*
        {
            return this.loader;
        }

        public function get isDone():Boolean
        {
            return this._loaded;
        }

        private function loadResource():void
        {
            this._loaded = false;
            this.loader.contentLoaderInfo.addEventListener(flash.events.Event.COMPLETE, this.onLoadComplete);
            this.loader.contentLoaderInfo.addEventListener(flash.events.IOErrorEvent.IO_ERROR, this.onIOError);
            this.loader.load(new flash.net.URLRequest(this.url));
            return;
        }

        private function onLoadComplete(arg1:flash.events.Event):void
        {
            this.loader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, this.onLoadComplete);
            this.loader.contentLoaderInfo.removeEventListener(flash.events.IOErrorEvent.IO_ERROR, this.onIOError);
            lesta.data.GameDelegate.call("resourceLoadingDelegator.jobDone", [id]);
            this._loaded = true;
            dispatchEvent(new flash.events.Event(flash.events.Event.COMPLETE));
            return;
        }

        private function onIOError(arg1:flash.events.Event):void
        {
            lesta.utils.Debug.assert(false, "[ERROR] Can not load resource " + this.url);
            this.loader.contentLoaderInfo.removeEventListener(flash.events.Event.COMPLETE, this.onLoadComplete);
            this.loader.contentLoaderInfo.removeEventListener(flash.events.IOErrorEvent.IO_ERROR, this.onIOError);
            dispatchEvent(new flash.events.IOErrorEvent(flash.events.IOErrorEvent.IO_ERROR));
            lesta.data.GameDelegate.call("resourceLoadingDelegator.jobFailed", [id]);
            return;
        }

        protected var url:String;

        protected var loader:*;

        protected var _loaded:Boolean;
    }
}
