package lesta.utils.resourceLoading 
{
    import flash.display.*;
    
    public class BitmapDataResource extends lesta.utils.resourceLoading.LoadResourceJob
    {
        public function BitmapDataResource(arg1:int, arg2:String)
        {
            super(arg1, arg2);
            loader = new lesta.utils.resourceLoading.DisplayLoader();
            return;
        }

        public function get bitmapData():flash.display.BitmapData
        {
            return ((loader as flash.display.Loader).content as flash.display.Bitmap).bitmapData;
        }

        override function fini():void
        {
            var loc1:*=loader as flash.display.Loader;
            if (loc1.content) 
            {
                (loc1.content as flash.display.Bitmap).bitmapData.dispose();
            }
            super.fini();
            return;
        }
    }
}
