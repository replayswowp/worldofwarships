package lesta.utils 
{
    public class ObjectPool extends Object
    {
        public function ObjectPool(arg1:Class)
        {
            this.freeObjects = [];
            super();
            this.objectClass = arg1;
            return;
        }

        public function alloc():Object
        {
            if (this.freeObjectsCount == 0) 
            {
                return new this.objectClass();
            }
            var loc1:*;
            var loc2:*;
            return this.freeObjects[loc1.freeObjectsCount = loc2 = ((loc1 = this).freeObjectsCount - 1)];
        }

        public function free(arg1:Object):void
        {
            var loc2:*;
            var loc3:*=((loc2 = this).freeObjectsCount + 1);
            loc2.freeObjectsCount = loc3;
            var loc1:*=(loc2 = this).freeObjectsCount;
            this.freeObjects[loc1] = arg1;
            return;
        }

        private var freeObjects:Array;

        private var freeObjectsCount:int=0;

        private var objectClass:Class;
    }
}
