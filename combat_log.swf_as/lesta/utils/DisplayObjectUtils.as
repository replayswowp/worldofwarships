package lesta.utils 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.text.*;
    import lesta.cpp.*;
    import scaleform.gfx.*;
    
    public class DisplayObjectUtils extends Object
    {
        public function DisplayObjectUtils()
        {
            super();
            return;
        }

        public static function stopAndInvisibleRecursive(arg1:flash.display.DisplayObject):void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=0;
            if (arg1 is flash.display.DisplayObjectContainer) 
            {
                loc1 = flash.display.DisplayObjectContainer(arg1);
                loc2 = 0;
                loc3 = loc1.numChildren;
                while (loc2 < loc3) 
                {
                    stopAndInvisibleRecursive(loc1.getChildAt(loc2));
                    ++loc2;
                }
                if (arg1 is flash.display.MovieClip) 
                {
                    flash.display.MovieClip(arg1).stop();
                }
                if (arg1 is flash.display.DisplayObject) 
                {
                    flash.display.DisplayObject(arg1).visible = false;
                }
            }
            return;
        }

        public static function stopRecursive(arg1:flash.display.DisplayObject):void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=0;
            if (arg1 is flash.display.DisplayObjectContainer) 
            {
                loc1 = flash.display.DisplayObjectContainer(arg1);
                loc2 = 0;
                loc3 = loc1.numChildren;
                while (loc2 < loc3) 
                {
                    stopRecursive(loc1.getChildAt(loc2));
                    ++loc2;
                }
                if (arg1 is flash.display.MovieClip) 
                {
                    flash.display.MovieClip(arg1).stop();
                }
            }
            return;
        }

        public static function placeChildrenAsVerticalStack(arg1:flash.display.DisplayObjectContainer, arg2:Number=0):void
        {
            var loc5:*=null;
            var loc1:*=0;
            var loc2:*=arg1.mask;
            var loc3:*=0;
            var loc4:*=arg1.numChildren;
            while (loc3 < loc4) 
            {
                loc5 = arg1.getChildAt(loc3);
                if (!(loc5 == loc2) && loc5.visible) 
                {
                    loc5.y = loc1;
                    loc1 = loc1 + (loc5.height + arg2);
                }
                ++loc3;
            }
            return;
        }

        public static function placeChildrenAsVerticalStackByTween(arg1:flash.display.DisplayObjectContainer, arg2:Number=0, arg3:Number=0.4, arg4:String=""):void
        {
            var loc6:*=null;
            var loc7:*=NaN;
            var loc8:*=NaN;
            var loc1:*=0;
            var loc2:*=arg1.mask;
            var loc3:*=[];
            var loc4:*=0;
            var loc5:*=arg1.numChildren;
            while (loc4 < loc5) 
            {
                loc6 = arg1.getChildAt(loc4);
                if (!(loc6 == loc2) && loc6.visible) 
                {
                    loc7 = arg4 != "" ? loc6[arg4].height : loc6.height;
                    loc3.push(loc1);
                    loc1 = loc1 + (loc7 + arg2);
                }
                ++loc4;
            }
            loc4 = 0;
            loc5 = arg1.numChildren;
            while (loc4 < loc5) 
            {
                loc6 = arg1.getChildAt(loc4);
                if (!(loc6 == loc2) && loc6.visible) 
                {
                    loc8 = loc6.y;
                    if (loc8 != loc3[loc4]) 
                    {
                        lesta.cpp.Tween.to(loc6, arg3, {"y":loc8}, {"y":loc3[loc4]});
                    }
                }
                ++loc4;
            }
            return;
        }

        public static function placeChildrenAsHorizontalStack(arg1:flash.display.DisplayObjectContainer, arg2:Number=0):void
        {
            var loc5:*=null;
            var loc6:*=NaN;
            var loc1:*=0;
            var loc2:*=arg1.mask;
            var loc3:*=0;
            var loc4:*=arg1.numChildren;
            while (loc3 < loc4) 
            {
                loc5 = arg1.getChildAt(loc3);
                if (!(loc5 == loc2) && loc5.visible) 
                {
                    loc5.x = loc1;
                    loc6 = loc5.size == null ? loc5.width : loc5.size.width;
                    loc1 = loc1 + (loc6 + arg2);
                }
                ++loc3;
            }
            return;
        }

        public static function placeChildrenAsTiles(arg1:flash.display.DisplayObjectContainer, arg2:Number=0, arg3:int=7):void
        {
            var loc9:*=null;
            var loc1:*=0;
            var loc2:*=0;
            var loc3:*=arg1.mask;
            var loc4:*=1;
            var loc5:*=0;
            var loc6:*=0;
            var loc7:*=0;
            var loc8:*=arg1.numChildren;
            while (loc7 < loc8) 
            {
                loc9 = arg1.getChildAt(loc7);
                if (!(loc9 == loc3) && loc9.visible) 
                {
                    loc9.x = loc1;
                    loc9.y = loc2;
                    loc1 = loc1 + (loc9.width + arg2);
                    loc5 = Math.max(loc5, loc9.height);
                    ++loc6;
                    if (loc6 == loc4 * arg3) 
                    {
                        loc1 = 0;
                        loc2 = loc2 + (loc5 + arg2);
                        loc5 = 0;
                        ++loc4;
                    }
                }
                ++loc7;
            }
            return;
        }

        public static function placeAsVerticalStack(arg1:__AS3__.vec.Vector.<flash.display.DisplayObject>, arg2:Number=0, arg3:Number=0):Number
        {
            var loc4:*=null;
            var loc1:*=arg2;
            var loc2:*=0;
            var loc3:*=arg1.length;
            while (loc2 < loc3) 
            {
                loc4 = arg1[loc2];
                loc4.y = loc1;
                loc1 = loc1 + (loc4.height + arg3);
                ++loc2;
            }
            return loc1 - arg3;
        }

        public static function getDescendantByName(arg1:flash.display.DisplayObjectContainer, arg2:String):flash.display.DisplayObject
        {
            var loc2:*=0;
            var loc3:*=0;
            var loc4:*=null;
            var loc5:*=null;
            var loc1:*=arg1.getChildByName(arg2);
            if (loc1) 
            {
                return loc1;
            }
            loc2 = 0;
            loc3 = arg1.numChildren;
            while (loc2 < loc3) 
            {
                loc4 = arg1.getChildAt(loc2) as flash.display.DisplayObjectContainer;
                if (loc4 != null) 
                {
                    loc5 = getDescendantByName(loc4, arg2);
                    if (loc5 != null) 
                    {
                        return loc5;
                    }
                }
                ++loc2;
            }
            return null;
        }

        public static function setHitTestDisableExcept(arg1:flash.display.DisplayObjectContainer, arg2:Array):void
        {
            var loc2:*=null;
            var loc1:*=0;
            while (loc1 < arg1.numChildren) 
            {
                loc2 = arg1.getChildAt(loc1) as flash.display.InteractiveObject;
                if (arg2.indexOf(loc2) == -1) 
                {
                    scaleform.gfx.InteractiveObjectEx.setHitTestDisable(loc2, true);
                }
                ++loc1;
            }
            return;
        }

        public static function coverObjectByAnother(arg1:flash.display.DisplayObject, arg2:Number, arg3:Number):void
        {
            if (arg1.width == 0 || arg1.height == 0) 
            {
                return;
            }
            var loc1:*=arg2 / arg1.width;
            var loc2:*=arg3 / arg1.height;
            var loc3:*=Math.max(loc1, loc2);
            var loc4:*;
            arg1.scaleY = loc4 = loc3;
            arg1.scaleX = loc4;
            arg1.x = (arg2 - arg1.width) * 0.5;
            arg1.y = (arg3 - arg1.height) * 0.5;
            return;
        }

        public static function markAsBlurLayer(arg1:flash.display.DisplayObject):void
        {
            scaleform.gfx.DisplayObjectEx.setRendererString(arg1, "blurLayer");
            return;
        }

        public static function markAsBlurMap(arg1:flash.display.DisplayObject):void
        {
            scaleform.gfx.DisplayObjectEx.setRendererString(arg1, "blurMeBaby");
            return;
        }

        public static function trimSingleLineText(arg1:flash.text.TextField, arg2:String="...", arg3:int=4):void
        {
            var loc1:*=NaN;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=null;
            var loc8:*=null;
            var loc9:*=null;
            if (!arg1.multiline) 
            {
                loc1 = Math.round(arg1.width - arg3);
                loc2 = arg1.htmlText;
                loc3 = getHtmlText(loc2);
                loc4 = loc2.substr(0, loc2.indexOf(loc3));
                loc5 = loc2.substr(loc2.indexOf(loc3) + loc3.length);
                loc6 = arg1.text;
                loc7 = loc3;
                if (loc6 != loc2) 
                {
                    while (loc1 < Math.floor(arg1.textWidth) && loc6.length) 
                    {
                        loc8 = loc6.charAt((loc6.length - 1));
                        loc9 = getXMLString(loc8);
                        loc7 = loc7.substring(0, loc7.lastIndexOf(loc9));
                        loc6 = loc6.substr(0, (loc6.length - 1));
                        arg1.htmlText = loc4 + loc7 + arg2 + loc5;
                    }
                }
                else 
                {
                    while (loc1 < Math.floor(arg1.textWidth) && loc6.length) 
                    {
                        loc6 = loc6.substr(0, (loc6.length - 1));
                        arg1.text = loc6 + arg2;
                    }
                }
            }
            return;
        }

        private static function getHtmlText(arg1:String):String
        {
            var loc2:*=0;
            var loc3:*=0;
            var loc4:*=null;
            var loc1:*=arg1;
            while (loc1.indexOf("<") > -1) 
            {
                loc2 = loc1.indexOf("<");
                loc3 = loc1.indexOf(">");
                loc4 = loc1.substring(loc2, loc3 + 1);
                loc1 = loc1.replace(loc4, "");
            }
            return loc1;
        }

        private static function getXMLString(arg1:String):String
        {
            if (arg1 == "<") 
            {
                return "&lt;";
            }
            var loc1:*=new XML("<root></root>");
            loc1.appendChild(arg1);
            return loc1.children()[0].toXMLString();
        }
    }
}
