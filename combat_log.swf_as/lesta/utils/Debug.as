package lesta.utils 
{
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import scaleform.clik.data.*;
    
    public class Debug extends Object
    {
        public function Debug()
        {
            super();
            return;
        }

        public static function assert(arg1:Boolean, arg2:String, arg3:Function=null, ... rest):void
        {
            if (!arg1) 
            {
                if (arg3 != null) 
                {
                    arg3.apply(null, rest);
                }
                throw new Error("Assertion failed: " + arg2);
            }
            return;
        }

        public static function isUssLoadingDebugMode():Boolean
        {
            var loc1:*=false;
            loc1 = true;
            return loc1;
        }

        public static function setDebugSettings(arg1:Boolean, arg2:Object, arg3:Function):void
        {
            lesta.utils.Debug.debugMode = arg1;
            lesta.utils.Debug.debugSettings = arg2;
            lesta.utils.Debug.out = arg3;
            return;
        }

        public static function getDebugEnabled(arg1:int=-1):Boolean
        {
            if (!debugMode || !(arg1 == -1) && !debugSettings[arg1]) 
            {
                return false;
            }
            return true;
        }

        public static function trace(... rest):void
        {
            debugTrace(lesta.utils.DebugTypes.DEFAULT, rest.join(","));
            return;
        }

        public static function debugTrace(arg1:int, arg2:String):void
        {
            if (!debugMode || !(arg1 == -1) && !debugSettings[arg1]) 
            {
                return;
            }
            lesta.utils.Debug.out(arg2);
            return;
        }

        public static function traceScope(arg1:*, arg2:Array=null, arg3:int=-1):void
        {
            var loc1:*=null;
            debugTrace(arg3, "==================================================");
            debugTrace(arg3, "Debugging scope:" + arg1);
            var loc2:*=0;
            var loc3:*=arg2;
            for each (loc1 in loc3) 
            {
                debugTrace(arg3, "scope." + loc1 + " = " + arg1[loc1]);
            }
            debugTrace(arg3, "==================================================");
            return;
        }

        public static function traceObject(arg1:Object, arg2:int=-1):void
        {
            var loc1:*=null;
            debugTrace(arg2, "==================================================");
            debugTrace(arg2, "Debugging object:" + arg1);
            var loc2:*=0;
            var loc3:*=arg1;
            for (loc1 in loc3) 
            {
                debugTrace(arg2, "scope." + loc1 + " = " + arg1[loc1]);
            }
            debugTrace(arg2, "==================================================");
            return;
        }

        public static function traceChildren(arg1:flash.display.DisplayObjectContainer, arg2:int=-1):void
        {
            var loc2:*=null;
            debugTrace(arg2, "==================================================");
            debugTrace(arg2, "Debugging children:" + arg1);
            var loc1:*=0;
            while (loc1 < arg1.numChildren) 
            {
                loc2 = arg1.getChildAt(loc1);
                debugTrace(arg2, "scope." + loc2.name + " = " + loc2);
                ++loc1;
            }
            debugTrace(arg2, "==================================================");
            return;
        }

        public static function traceChildrenTree(arg1:flash.display.DisplayObjectContainer, arg2:int=-1):void
        {
            debugTrace(arg2, "==================================================");
            debugTrace(arg2, "Debugging children tree:" + arg1);
            traceChildrenTreeR(arg1, "", arg2);
            debugTrace(arg2, "==================================================");
            return;
        }

        private static function traceChildrenTreeR(arg1:flash.display.DisplayObjectContainer, arg2:String, arg3:int=-1):void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=0;
            while (loc1 < arg1.numChildren) 
            {
                loc2 = arg1.getChildAt(loc1);
                loc3 = loc2 as flash.display.DisplayObjectContainer;
                debugTrace(arg3, arg2 + loc2.name + " = " + loc2);
                if (loc3) 
                {
                    traceChildrenTreeR(loc3, arg2 + "\t", arg3);
                }
                ++loc1;
            }
            return;
        }

        public static function traceDataProvider(arg1:scaleform.clik.data.DataProvider, arg2:int=-1):void
        {
            var loc1:*="DataProvider [";
            var loc2:*=0;
            while (loc2 < arg1.length) 
            {
                loc1 = loc1 + (arg1[loc2] + ", ");
                ++loc2;
            }
            loc1 = loc1 + "]";
            debugTrace(arg2, loc1);
            return;
        }

        public static function formatString(arg1:String, arg2:Array):String
        {
            var loc1:*=0;
            while (loc1 < arg2.length) 
            {
                arg1 = arg1.replace("%", arg2[loc1]);
                ++loc1;
            }
            return arg1;
        }

        public static function ftrace(arg1:String, arg2:Array, arg3:int=-1):void
        {
            var loc1:*=formatString(arg1, arg2);
            debugTrace(arg3, loc1);
            return;
        }

        public static function deepTrace(arg1:Object, arg2:int=0, arg3:int=-1):void
        {
            var loc3:*=null;
            var loc1:*="";
            var loc2:*=0;
            while (loc2 < arg2) 
            {
                loc1 = loc1 + "\t";
                ++loc2;
            }
            var loc4:*=0;
            var loc5:*=arg1;
            for (loc3 in loc5) 
            {
                debugTrace(arg3, loc1 + "[" + loc3 + "] -> " + arg1[loc3]);
                deepTrace(arg1[loc3], arg2 + 1);
            }
            return;
        }

        public static function checkChildren(arg1:flash.display.DisplayObjectContainer, arg2:Array):void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc1:*="[ERROR] %type   %obj.%childName";
            var loc2:*=0;
            while (loc2 < arg2.length) 
            {
                loc3 = arg2[loc2];
                loc4 = arg1.getChildByName(loc3) as flash.display.DisplayObject;
                loc5 = null;
                try 
                {
                    loc5 = (arg1 as Object)[loc3] as flash.display.DisplayObject;
                }
                catch (e:Error)
                {
                };
                if (loc4 == null || loc5 == null) 
                {
                    loc6 = loc1;
                    loc6 = loc6.replace("%type", arg1);
                    loc6 = loc6.replace("%obj", arg1.name);
                    loc6 = loc6.replace("%childName", loc3);
                    lesta.utils.Debug.out(loc6);
                }
                ++loc2;
            }
            return;
        }

        public static function incPerFrameCounter(arg1:String, arg2:int=1):void
        {
            if (!isListening) 
            {
                enterFrameListener.addEventListener(flash.events.Event.ENTER_FRAME, onEnterFrameStat);
                isListening = true;
            }
            if (arg1 in perFrameCounter) 
            {
                perFrameCounter[arg1] = perFrameCounter[arg1] + arg2;
            }
            else 
            {
                perFrameCounter[arg1] = arg2;
                var loc1:*;
                numCounters++;
            }
            return;
        }

        private static function onEnterFrameStat(arg1:flash.events.Event):void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc6:*=0;
            var loc7:*=null;
            var loc8:*=null;
            if (numCounters == 0) 
            {
                var loc9:*;
                hysteresis++;
            }
            if (hysteresis > NUM_IDLING_FRAMES) 
            {
                enterFrameListener.removeEventListener(flash.events.Event.ENTER_FRAME, onEnterFrameStat);
                isListening = false;
                hysteresis = 0;
            }
            if (numCounters == 0) 
            {
                return;
            }
            var loc1:*=[];
            var loc2:*=0;
            loc9 = 0;
            loc10 = perFrameCounter;
            for (loc3 in loc10) 
            {
                loc6 = loc3.length;
                loc2 = Math.max(loc2, loc6);
                loc1.push({"name":loc3, "count":perFrameCounter[loc3], "len":loc6});
            }
            loc4 = "";
            loc9 = perFrameCounterSortBy;
            switch (loc9) 
            {
                case "name":
                case "count":
                {
                    loc4 = perFrameCounterSortBy;
                    break;
                }
                default:
                {
                    loc4 = "name";
                }
            }
            loc1.sortOn(loc4, Array.DESCENDING | Array.NUMERIC);
            while (stringWithSpaces.length < loc2) 
            {
                stringWithSpaces = stringWithSpaces + stringWithSpaces;
            }
            trace("===== PerFrameCallCounter::onEnterFrameStat =====");
            var loc5:*=0;
            while (loc5 < loc1.length) 
            {
                loc7 = loc1[loc5];
                loc8 = stringWithSpaces.slice(0, loc2 - loc7["len"]);
                trace(loc7["name"] + ";", loc8, loc7["count"]);
                ++loc5;
            }
            numCounters = 0;
            perFrameCounter = new flash.utils.Dictionary();
            return;
        }

        
        {
            debugMode = false;
            debugSettings = new Object();
            perFrameCounter = new flash.utils.Dictionary();
            enterFrameListener = new flash.display.Shape();
            numCounters = 0;
            hysteresis = 0;
            isListening = false;
            stringWithSpaces = " ";
        }

        private static const perFrameCounterSortBy:String="count";

        private static const NUM_IDLING_FRAMES:int=5;

        private static var debugMode:Boolean=false;

        private static var debugSettings:Object;

        private static var out:Function;

        private static var perFrameCounter:flash.utils.Dictionary;

        private static var enterFrameListener:flash.display.Shape;

        private static var numCounters:int=0;

        private static var hysteresis:int=0;

        private static var isListening:Boolean=false;

        private static var stringWithSpaces:String=" ";
    }
}
