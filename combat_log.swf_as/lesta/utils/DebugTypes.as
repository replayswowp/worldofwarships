package lesta.utils 
{
    public class DebugTypes extends Object
    {
        public function DebugTypes()
        {
            super();
            return;
        }

        public static const DEFAULT:int=0;

        public static const MAIN_SCENE_ERROR:int=1;

        public static const MAIN_SCENE_WARNING:int=2;

        public static const MAIN_SCENE_DEBUG:int=3;

        public static const BATTLE_DRAW_PROFILING:int=12;
    }
}
