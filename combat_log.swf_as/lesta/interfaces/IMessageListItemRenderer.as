package lesta.interfaces 
{
    import flash.events.*;
    
    public interface IMessageListItemRenderer extends lesta.interfaces.IDisplayObject
    {
        function init(arg1:Object, arg2:Number):void;

        function fini():void;

        function play():void;

        function validateNow(arg1:flash.events.Event=null):void;

        function getData():Object;
    }
}
