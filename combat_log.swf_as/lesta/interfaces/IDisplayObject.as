package lesta.interfaces 
{
    import flash.events.*;
    
    public interface IDisplayObject extends flash.events.IEventDispatcher
    {
        function get width():Number;

        function set width(arg1:Number):void;

        function get height():Number;

        function set height(arg1:Number):void;

        function get visible():Boolean;

        function set visible(arg1:Boolean):void;

        function get x():Number;

        function set x(arg1:Number):void;

        function get y():Number;

        function set y(arg1:Number):void;

        function get rotation():Number;

        function set rotation(arg1:Number):void;

        function get mouseChildren():Boolean;

        function set mouseChildren(arg1:Boolean):void;

        function get mouseEnabled():Boolean;

        function set mouseEnabled(arg1:Boolean):void;

        function get tabChildren():Boolean;

        function set tabChildren(arg1:Boolean):void;

        function set alpha(arg1:Number):void;

        function get alpha():Number;
    }
}
