package lesta.interfaces 
{
    public interface IOverlay
    {
        function beforeOpen(arg1:Object=null):void;

        function beforeClose():void;

        function getResult():Object;

        function onBecomeTop(arg1:Object):void;

        function onLostTop():void;

        function setStageSize(arg1:Number, arg2:Number):void;
    }
}
