package lesta.dialogs.battle_window_controllers.complain_action_handler 
{
    import lesta.data.*;
    
    public class MuteUserHandler extends Object implements lesta.dialogs.battle_window_controllers.complain_action_handler.ActionHandler
    {
        public function MuteUserHandler()
        {
            super();
            return;
        }

        public function handleAction(arg1:Object):void
        {
            lesta.data.GameDelegate.call("battleChat.muteUser", [arg1.avatarId]);
            return;
        }
    }
}
