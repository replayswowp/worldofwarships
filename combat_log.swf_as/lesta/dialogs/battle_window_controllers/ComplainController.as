package lesta.dialogs.battle_window_controllers 
{
    import lesta.constants.*;
    import lesta.controls.*;
    import lesta.dialogs.battle_window._ingame_chat.*;
    import lesta.dialogs.battle_window._player_stats_item.*;
    import lesta.dialogs.battle_window_controllers.complain_action_handler.*;
    import lesta.dialogs.battle_window_new.*;
    import lesta.structs.*;
    
    public class ComplainController extends lesta.dialogs.battle_window_new.HudElementController
    {
        public function ComplainController()
        {
            super();
            return;
        }

        protected override function init():void
        {
            this.chat.txa_output.iconContainerHitTestDisable = false;
            this.chat.controller = this;
            this.chat.setHintManager(hintManager);
            this.allyEar.setComplainController(this);
            this.enemyEar.setComplainController(this);
            return;
        }

        public function handleAddChatMessage(arg1:lesta.controls.MessageListItemRenderer, arg2:Object):void
        {
            if (arg2.nickname) 
            {
                arg2.uses = gameInfoHolder.evaluationLimits;
                arg2.player = gameInfoHolder.mapPlayers[arg2["avatarId"]];
            }
            contextMenuManager.updateScope(arg1, arg2);
            return;
        }

        public function handleRemoveChatMessage(arg1:lesta.controls.MessageListItemRenderer):void
        {
            return;
        }

        public function attachMessageContextMenu(arg1:lesta.controls.MessageListItemRenderer, arg2:Object):void
        {
            contextMenuManager.attachMenu(arg1, "playerMenu", this.contextMenuCallback, arg2, arg1.select, arg1.deselect);
            return;
        }

        public function detachMessageContextMenu(arg1:lesta.controls.MessageListItemRenderer):void
        {
            contextMenuManager.detachMenu(arg1);
            return;
        }

        public function attachEarsContextMenu(arg1:lesta.dialogs.battle_window._player_stats_item.PlayersStateItemRenderer, arg2:lesta.structs.Player):void
        {
            if (gameInfoHolder && !(arg2.name == gameInfoHolder.selfPlayer.name)) 
            {
                arg1.contextMenuScope.uses = gameInfoHolder.evaluationLimits;
                arg1.contextMenuScope.avatarId = arg2.id;
                arg1.contextMenuScope.player = arg2;
                contextMenuManager.attachMenu(arg1, "playerMenu", this.contextMenuCallback, arg1.contextMenuScope);
            }
            return;
        }

        public function detachEarsContextMenu(arg1:lesta.dialogs.battle_window._player_stats_item.PlayersStateItemRenderer):void
        {
            contextMenuManager.detachMenu(arg1);
            return;
        }

        private function contextMenuCallback(arg1:String, arg2:Object):void
        {
            var loc1:*=CONTEXT_MENU_ACTION_HANDLERS[arg1];
            loc1.handleAction(arg2);
            return;
        }

        private static const CONTEXT_MENU_ACTION_HANDLERS:Object={"complain.Bot":new lesta.dialogs.battle_window_controllers.complain_action_handler.EvaluationHandler(lesta.constants.EvaluationType.DENUNCIATION, lesta.constants.EvaluationTopic.BOT), "complain.Play":new lesta.dialogs.battle_window_controllers.complain_action_handler.EvaluationHandler(lesta.constants.EvaluationType.DENUNCIATION, lesta.constants.EvaluationTopic.PLAY), "complain.Chat":new lesta.dialogs.battle_window_controllers.complain_action_handler.EvaluationHandler(lesta.constants.EvaluationType.DENUNCIATION, lesta.constants.EvaluationTopic.CHAT), "praise.Play":new lesta.dialogs.battle_window_controllers.complain_action_handler.EvaluationHandler(lesta.constants.EvaluationType.PRAISE, lesta.constants.EvaluationTopic.PLAY), "praise.Chat":new lesta.dialogs.battle_window_controllers.complain_action_handler.EvaluationHandler(lesta.constants.EvaluationType.PRAISE, lesta.constants.EvaluationTopic.CHAT), "chat.muteUser":new lesta.dialogs.battle_window_controllers.complain_action_handler.MuteUserHandler(), "chat.unmuteUser":new lesta.dialogs.battle_window_controllers.complain_action_handler.UnmuteUserHandler()};

        public var chat:lesta.dialogs.battle_window._ingame_chat.QuickChat;

        public var allyEar:lesta.dialogs.battle_window._player_stats_item.EarList;

        public var enemyEar:lesta.dialogs.battle_window._player_stats_item.EarList;
    }
}
