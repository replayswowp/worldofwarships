package lesta.dialogs.battle_window_new 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import flash.utils.*;
    import lesta.data.*;
    import lesta.managers.*;
    import lesta.unbound.core.*;
    import lesta.utils.*;
    import scaleform.clik.utils.*;
    
    public class HudElementController extends flash.events.EventDispatcher
    {
        public function HudElementController()
        {
            this.listChildControllers = [];
            this.mapChildControllers = {};
            this.stageSize = new flash.geom.Point();
            super();
            return;
        }

        public final function setHotkeyManager(arg1:lesta.managers.HotKeyManager):void
        {
            this.hotkeyManager = arg1;
            return;
        }

        public final function setGameInfoHolder(arg1:lesta.utils.GameInfoHolder):void
        {
            this.gameInfoHolder = arg1;
            return;
        }

        public final function setConstraints(arg1:scaleform.clik.utils.Constraints):void
        {
            this.constraints = arg1;
            return;
        }

        public final function setFocusManager(arg1:lesta.managers.FocusWindowManager):void
        {
            this.focusManager = arg1;
            return;
        }

        public final function setHintManager(arg1:lesta.managers.BattleHintManager):void
        {
            this.hintManager = arg1;
            return;
        }

        public final function setContextMenuManager(arg1:lesta.managers.ContextMenuManager):void
        {
            this.contextMenuManager = arg1;
            return;
        }

        public final function initController():void
        {
            this.init();
            return;
        }

        public final function finiController():void
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc1:*=0;
            while (loc1 < this.listChildControllers.length) 
            {
                loc4 = this.listChildControllers[loc1];
                loc4.finiController();
                ++loc1;
            }
            this.listChildControllers = [];
            this.mapChildControllers = {};
            this.fini();
            this.gameInfoHolder.removeCallback(this);
            lesta.data.GameDelegate.removeCallBack(this);
            var loc2:*=lesta.unbound.core.UbReflector.getPublicVariables(flash.utils.getQualifiedClassName(this), this);
            var loc3:*=loc2.length;
            loc1 = 0;
            while (loc1 < loc3) 
            {
                loc5 = loc2[loc1].name;
                this[loc5] = null;
                ++loc1;
            }
            this.gameInfoHolder = null;
            this.gameDelegate = null;
            this.hotkeyManager = null;
            this.hintManager = null;
            this.contextMenuManager = null;
            return;
        }

        protected function init():void
        {
            return;
        }

        protected function fini():void
        {
            return;
        }

        public final function setClips(arg1:Array):void
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc1:*=lesta.unbound.core.UbReflector.getPublicVariables(flash.utils.getQualifiedClassName(this), this);
            var loc2:*=this;
            var loc3:*=0;
            while (loc3 < loc1.length) 
            {
                loc4 = loc1[loc3];
                loc5 = loc4.name;
                if (loc5 != "clip") 
                {
                    loc6 = this.getChildByName(loc5, arg1);
                    if (loc6 == null) 
                    {
                        if (loc5 != "name") 
                        {
                            trace("BattleHUD construct: there is no child with name ", loc5);
                        }
                    }
                    else 
                    {
                        loc2[loc5] = loc6;
                    }
                }
                else 
                {
                    loc2[loc5] = arg1[0];
                }
                ++loc3;
            }
            return;
        }

        public function updateStageSize(arg1:Number, arg2:Number):void
        {
            this.stageSize.x = arg1;
            this.stageSize.y = arg2;
            return;
        }

        public function updateVisibility():void
        {
            return;
        }

        protected function initChildController(arg1:String, arg2:lesta.dialogs.battle_window_new.HudElementController, arg3:Array):void
        {
            this.finiChildController(arg1);
            arg2.setClips(arg3);
            arg2.setGameInfoHolder(this.gameInfoHolder);
            arg2.setHotkeyManager(this.hotkeyManager);
            arg2.setConstraints(this.constraints);
            arg2.setFocusManager(this.focusManager);
            arg2.initController();
            arg2.name = arg1;
            this.listChildControllers.push(arg2);
            this.mapChildControllers[arg1] = arg2;
            return;
        }

        protected function finiChildController(arg1:String):void
        {
            var loc2:*=0;
            var loc1:*=this.mapChildControllers[arg1];
            if (loc1 != null) 
            {
                loc1.finiController();
                delete this.mapChildControllers[arg1];
                loc2 = this.listChildControllers.indexOf(loc1);
                this.listChildControllers.splice(loc2, 1);
            }
            return;
        }

        private function getChildByName(arg1:String, arg2:Array):flash.display.DisplayObject
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc1:*=null;
            var loc2:*=0;
            while (loc2 < arg2.length) 
            {
                loc3 = arg2[loc2] as flash.display.DisplayObject;
                if (loc3.name == arg1) 
                {
                    loc1 = loc3;
                    break;
                }
                loc4 = loc3 as flash.display.DisplayObjectContainer;
                if (loc4 != null) 
                {
                    loc5 = lesta.utils.DisplayObjectUtils.getDescendantByName(loc4, arg1);
                    if (loc5 != null) 
                    {
                        loc1 = loc5;
                        break;
                    }
                }
                ++loc2;
            }
            return loc1;
        }

        public var name:String=null;

        protected var gameInfoHolder:lesta.utils.GameInfoHolder;

        protected var gameDelegate:lesta.data.GameDelegate;

        protected var hotkeyManager:lesta.managers.HotKeyManager;

        protected var hintManager:lesta.managers.BattleHintManager;

        protected var constraints:scaleform.clik.utils.Constraints;

        protected var focusManager:lesta.managers.FocusWindowManager;

        protected var contextMenuManager:lesta.managers.ContextMenuManager;

        private var listChildControllers:Array;

        private var mapChildControllers:Object;

        protected var stageSize:flash.geom.Point;
    }
}
