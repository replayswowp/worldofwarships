package lesta.dialogs.custom_hud 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import flash.utils.*;
    
    public class CrossingDetector extends flash.events.EventDispatcher
    {
        public function CrossingDetector()
        {
            this.gluesMap = new flash.utils.Dictionary();
            this.selectedGlues = [];
            this.vertLines = new flash.utils.Dictionary();
            this.horizLines = new flash.utils.Dictionary();
            this.elementRects = new flash.utils.Dictionary();
            this.restrictions = [];
            this.elementPosition = ["top", "left"];
            super();
            return;
        }

        public static function getOffsetByConstraintType(arg1:flash.display.DisplayObject, arg2:int, arg3:Boolean=false):Number
        {
            var loc1:*=arg1.getRect(arg1.parent);
            if (VERTICAL_CONSTRAINTS.indexOf(arg2) > -1) 
            {
                return arg3 ? arg1.x : loc1.width * VERTICAL_CONSTRAINTS.indexOf(arg2) / 2;
            }
            if (HORIZONTAL_CONSTRAINTS.indexOf(arg2) > -1) 
            {
                return arg3 ? arg1.y : loc1.height * HORIZONTAL_CONSTRAINTS.indexOf(arg2) / 2;
            }
            return 0;
        }

        
        {
            CONSTRAINT_GRID = [];
        }

        public function updateLines(arg1:Array, arg2:Array, arg3:flash.geom.Point):void
        {
            var loc2:*=0;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc1:*=new flash.geom.Point();
            CONSTRAINT_GRID[HORIZONTAL] = this.horizLines;
            CONSTRAINT_GRID[VERTICAL] = this.vertLines;
            var loc6:*=0;
            var loc7:*=CONSTRAINT_TYPES;
            for each (loc2 in loc7) 
            {
                loc3 = [arg2, arg1][loc2];
                loc4 = CONSTRAINT_COORD[loc2];
                var loc8:*=0;
                var loc9:*=loc3;
                for each (loc5 in loc9) 
                {
                    loc1.x = loc5.x;
                    loc1.y = loc5.y;
                    CONSTRAINT_GRID[loc2][loc5.name] = loc5.parent.localToGlobal(loc1)[loc4];
                }
            }
            this.size = arg3;
            this.updateDetector();
            return;
        }

        public function updateElement(arg1:flash.display.DisplayObject):void
        {
            if (!(arg1 is lesta.dialogs.custom_hud.CustomHudContainer) || !arg1["layout"]) 
            {
                return;
            }
            var loc1:*=arg1 as lesta.dialogs.custom_hud.CustomHudContainer;
            this.currentElementID = loc1.name;
            this.elementRects[this.currentElementID] = loc1.getGlobalRect();
            this.currentElement = this.elementRects[this.currentElementID];
            this.gluesMap[this.currentElementID] = [loc1.layout.hglue, loc1.layout.vglue];
            var loc2:*=this.currentElement.topLeft;
            var loc3:*=this.currentElement.bottomRight;
            this.dots = [loc2, flash.geom.Point.interpolate(loc2, loc3, 0.5), loc3];
            this.updateDetector();
            return;
        }

        public function updateElements(arg1:Array):void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=arg1;
            for each (loc1 in loc3) 
            {
                this.updateElement(loc1);
            }
            return;
        }

        private function updateDetector():void
        {
            var vglues:Array;
            var hglues:Array;
            var vertChanged:Boolean;
            var horizChanged:Boolean;
            var dot:flash.geom.Point;
            var hg:lesta.dialogs.custom_hud.CustomHudGlue;
            var vg:lesta.dialogs.custom_hud.CustomHudGlue;
            var ta:Array;

            var loc1:*;
            vglues = null;
            hglues = null;
            hg = null;
            vg = null;
            ta = null;
            vertChanged = false;
            horizChanged = false;
            dot = null;
            this.glueSet = [];
            if (!this.dots) 
            {
                return;
            }
            var loc2:*=0;
            var loc3:*=this.dots;
            for each (dot in loc3) 
            {
                this.fillLinesGlue(dot);
            }
            this.glueSet.push(new lesta.dialogs.custom_hud.CustomHudGlue(lesta.dialogs.custom_hud.CustomHudGlue.BORDER, "", 0, LEFT, this.currentElement.x));
            this.glueSet.push(new lesta.dialogs.custom_hud.CustomHudGlue(lesta.dialogs.custom_hud.CustomHudGlue.BORDER, "", 0, TOP, this.currentElement.y));
            vglues = this.glueSet.filter(function (arg1:lesta.dialogs.custom_hud.CustomHudGlue, arg2:int, arg3:Array):Boolean
            {
                return VERTICAL_CONSTRAINTS.indexOf(arg1.elementConstraintType) > -1;
            })
            hglues = this.glueSet.filter(function (arg1:lesta.dialogs.custom_hud.CustomHudGlue, arg2:int, arg3:Array):Boolean
            {
                return HORIZONTAL_CONSTRAINTS.indexOf(arg1.elementConstraintType) > -1;
            })
            vglues.sortOn("distanceFromGlue", Array.NUMERIC);
            hglues.sortOn("distanceFromGlue", Array.NUMERIC);
            hg = hglues[0];
            vg = vglues[0];
            horizChanged = (this.gluesMap[this.currentElementID][0] as lesta.dialogs.custom_hud.CustomHudGlue).isChanged(hg);
            vertChanged = (this.gluesMap[this.currentElementID][1] as lesta.dialogs.custom_hud.CustomHudGlue).isChanged(vg);
            this.gluesMap[this.currentElementID][0] = hg;
            this.gluesMap[this.currentElementID][1] = vg;
            this.selectedGlues = this.gluesMap[this.currentElementID];
            if (this.isSectorsCheck) 
            {
                dot = this.dots[0];
                ta = [];
                ta.push(dot.y < this.size.y / 2 ? "top" : "bottom");
                ta.push(dot.x < this.size.x / 2 ? "left" : "right");
                if (String(ta) != String(this.elementPosition)) 
                {
                    this.elementPosition = ta;
                    dispatchEvent(new flash.events.Event(ELEMENT_SECTOR_CHANGED));
                }
            }
            if (vertChanged) 
            {
                dispatchEvent(new flash.events.Event(VERTICAL_BLOCK_CHANGED));
            }
            if (horizChanged) 
            {
                dispatchEvent(new flash.events.Event(HORIZONTAL_BLOCK_CHANGED));
            }
            return;
        }

        private function fillBorderGlue():void
        {
            var loc1:*=flash.geom.Point.interpolate(this.currentElement.topLeft, this.currentElement.bottomRight, 0.5);
            var loc2:*=loc1.x < this.size.x / 2 ? LEFT : RIGHT;
            var loc3:*=loc1.x < this.size.x / 2 ? this.currentElement.x : this.size.x - this.currentElement.right;
            var loc4:*=loc1.y < this.size.y / 2 ? TOP : BOTTOM;
            var loc5:*=loc1.y < this.size.y / 2 ? this.currentElement.y : this.size.y - this.currentElement.bottom;
            this.glueSet.push(new lesta.dialogs.custom_hud.CustomHudGlue(lesta.dialogs.custom_hud.CustomHudGlue.BORDER, String(loc2), 0, loc2, loc3));
            this.glueSet.push(new lesta.dialogs.custom_hud.CustomHudGlue(lesta.dialogs.custom_hud.CustomHudGlue.BORDER, String(loc4), 0, loc4, loc5));
            return;
        }

        private function fillLinesGlue(arg1:flash.geom.Point):void
        {
            var loc1:*=0;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=NaN;
            var loc6:*=0;
            var loc7:*=CONSTRAINT_TYPES;
            for each (loc1 in loc7) 
            {
                loc2 = CONSTRAINT_GRID[loc1];
                loc3 = CONSTRAINT_COORD[loc1];
                var loc8:*=0;
                var loc9:*=loc2;
                for (loc4 in loc9) 
                {
                    loc5 = loc2[loc4];
                    if (!(Math.abs(loc5 - arg1[loc3]) < MIN_DISTANCE_TO_GRID)) 
                    {
                        continue;
                    }
                    this.glueSet.push(new lesta.dialogs.custom_hud.CustomHudGlue(lesta.dialogs.custom_hud.CustomHudGlue.LINE, loc4, loc5, CONSTRAINT_POSITIONS[loc1][this.dots.indexOf(arg1)], Math.abs(loc5 - arg1[loc3])));
                }
            }
            return;
        }

        private function fillElementsGlue(arg1:flash.geom.Point):void
        {
            var loc2:*=0;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=null;
            var loc8:*=null;
            var loc9:*=0;
            var loc10:*=NaN;
            var loc11:*=NaN;
            var loc12:*=null;
            var loc1:*=this.dots.indexOf(arg1);
            var loc13:*=0;
            var loc14:*=CONSTRAINT_TYPES;
            for each (loc2 in loc14) 
            {
                loc3 = CONSTRAINT_POSITIONS[loc2];
                loc4 = CONSTRAINT_SIZE[loc2];
                loc5 = CONSTRAINT_COORD[loc2];
                var loc15:*=0;
                var loc16:*=this.elementRects;
                for (loc6 in loc16) 
                {
                    loc7 = this.elementRects[loc6] as flash.geom.Rectangle;
                    loc8 = this.gluesMap[loc6][loc2];
                    if (this.currentElementID == loc6 || loc8.glueElementID == this.currentElementID) 
                    {
                        continue;
                    }
                    var loc17:*=0;
                    var loc18:*=loc3;
                    for each (loc9 in loc18) 
                    {
                        loc10 = loc7[loc4] * loc3.indexOf(loc9) / (loc3.length - 1);
                        loc11 = Math.abs(arg1[loc5] - loc7[loc5] - loc10);
                        if (!(loc11 < MIN_DISTANCE_TO_GRID)) 
                        {
                            continue;
                        }
                        loc12 = new lesta.dialogs.custom_hud.CustomHudGlue(lesta.dialogs.custom_hud.CustomHudGlue.ELEMENT, loc6, loc7[loc5] + loc10, loc3[this.dots.indexOf(arg1)], loc11);
                        loc12.isGluedToElement = this.currentElement.union(loc7).width < this.currentElement.width + loc7.width + MIN_DISTANCE_TO_GRID && this.currentElement.union(loc7).height < this.currentElement.height + loc7.height + MIN_DISTANCE_TO_GRID;
                        if (!loc12.isGluedToElement) 
                        {
                            continue;
                        }
                        this.glueSet.push(loc12);
                    }
                }
            }
            return;
        }

        public override function toString():String
        {
            var loc1:*="";
            return loc1;
        }

        public static function getGlueByElementId(arg1:Array, arg2:String, arg3:int=0):lesta.dialogs.custom_hud.CustomHudGlue
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=arg1;
            for each (loc1 in loc3) 
            {
                if (!(loc1.glueElementID == arg2 && loc1.type == arg3)) 
                {
                    continue;
                }
                return loc1;
            }
            return null;
        }

        public static const ELEMENT_SECTOR_CHANGED:String="positionChanged";

        public static const MIN_DISTANCE_TO_GRID:int=20;

        public static const H_CENTER:int=2;

        public static const RIGHT:int=3;

        public static const HORIZONTAL:int=0;

        public static const TOP:int=4;

        public static const V_CENTER:int=5;

        public static const VERTICAL_CONSTRAINTS:Array=[LEFT, H_CENTER, RIGHT];

        public static const HORIZONTAL_CONSTRAINTS:Array=[TOP, V_CENTER, BOTTOM];

        public static const BOTTOM:int=6;

        public static const LEFT:int=1;

        public static const VERTICAL:int=1;

        public static const CONSTRAINT_TYPES:Array=[HORIZONTAL, VERTICAL];

        public static const CONSTRAINT_COORD:Array=["y", "x"];

        public static const CONSTRAINT_SIZE:Array=["height", "width"];

        public static const CONSTRAINT_POSITIONS:Array=[[TOP, V_CENTER, BOTTOM], [LEFT, H_CENTER, RIGHT]];

        public static const VERTICAL_BLOCK_CHANGED:String="VBlockChanged";

        public static const HORIZONTAL_BLOCK_CHANGED:String="HBlockChanged";

        public var isSectorsCheck:Boolean=false;

        public var gluesMap:flash.utils.Dictionary;

        public var selectedGlues:Array;

        private var vertLines:flash.utils.Dictionary;

        private var elementRects:flash.utils.Dictionary;

        private var size:flash.geom.Point;

        private var currentElement:flash.geom.Rectangle;

        private var currentElementID:String;

        private var dots:Array;

        public static var CONSTRAINT_GRID:Array;

        public var elementPosition:Array;

        private var restrictions:Array;

        private var glueSet:Array;

        private var horizLines:flash.utils.Dictionary;
    }
}
