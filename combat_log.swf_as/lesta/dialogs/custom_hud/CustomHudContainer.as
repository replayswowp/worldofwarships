package lesta.dialogs.custom_hud 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import flash.utils.*;
    import lesta.cpp.*;
    import lesta.structs.*;
    
    public class CustomHudContainer extends flash.display.MovieClip
    {
        public function CustomHudContainer()
        {
            this._glues = [];
            this.indicatorsMap = new flash.utils.Dictionary();
            super();
            this.initIndicators();
            addEventListener(flash.events.Event.ADDED_TO_STAGE, this.addedToStage);
            return;
        }

        public function get selected():Boolean
        {
            return this._selected;
        }

        public function set selected(arg1:Boolean):void
        {
            var loc1:*=null;
            if (this._selected != arg1) 
            {
                this._selected = arg1;
                this.switchDetectorListeners(arg1);
                this.frame.gotoAndStop(arg1 ? 2 : 1);
                this.menu.visible = arg1;
                var loc2:*=0;
                var loc3:*=this.indicatorsMap;
                for (loc1 in loc3) 
                {
                    (loc1 as flash.display.MovieClip).visible = arg1;
                }
            }
            return;
        }

        public function get layout():lesta.structs.CustomHudElement
        {
            return this._layout;
        }

        public function set layout(arg1:lesta.structs.CustomHudElement):void
        {
            this._layout = arg1;
            return;
        }

        public function get glues():Array
        {
            return this._layout ? [this.layout.hglue, this.layout.vglue] : null;
        }

        public function get content():flash.display.DisplayObject
        {
            return this._content;
        }

        public function set content(arg1:flash.display.DisplayObject):void
        {
            this._content = arg1;
            this.placeholder.addChildAt(this.content, 0);
            var loc1:*=this.content.getBounds(this.placeholder);
            this.placeholder.width = loc1.width;
            this.placeholder.height = loc1.height;
            this.content.x = -loc1.x;
            this.content.y = -loc1.y;
            this.updateSize(this.placeholder.width, this.placeholder.height);
            return;
        }

        public function getGlobalRect():flash.geom.Rectangle
        {
            var loc1:*=this.placeholder.getRect(this);
            loc1.offsetPoint(localToGlobal(loc1.topLeft));
            return loc1;
        }

        private function addedToStage(arg1:flash.events.Event):void
        {
            removeEventListener(flash.events.Event.ADDED_TO_STAGE, this.addedToStage);
            var loc1:*=this;
            while (!(loc1 is lesta.dialogs.custom_hud.CustomHud)) 
            {
                loc1 = loc1.parent;
            }
            this.detector = (loc1 as lesta.dialogs.custom_hud.CustomHud).crossingDetector;
            return;
        }

        private function updateScale():void
        {
            this.updateSize(this.placeholder.width, this.placeholder.height);
            return;
        }

        private function updateSize(arg1:Number, arg2:Number):void
        {
            this.frame.width = arg1;
            this.frame.height = arg2;
            this.updateIndicatorsLayout(arg1, arg2);
            return;
        }

        private function switchDetectorListeners(arg1:Boolean):void
        {
            if (arg1) 
            {
                this.detector.addEventListener(lesta.dialogs.custom_hud.CrossingDetector.HORIZONTAL_BLOCK_CHANGED, this.detectorBlockChanged);
                this.detector.addEventListener(lesta.dialogs.custom_hud.CrossingDetector.VERTICAL_BLOCK_CHANGED, this.detectorBlockChanged);
                this.detector.addEventListener(lesta.dialogs.custom_hud.CrossingDetector.ELEMENT_SECTOR_CHANGED, this.sectorChanged);
            }
            else 
            {
                this.detector.removeEventListener(lesta.dialogs.custom_hud.CrossingDetector.HORIZONTAL_BLOCK_CHANGED, this.detectorBlockChanged);
                this.detector.removeEventListener(lesta.dialogs.custom_hud.CrossingDetector.VERTICAL_BLOCK_CHANGED, this.detectorBlockChanged);
                this.detector.removeEventListener(lesta.dialogs.custom_hud.CrossingDetector.ELEMENT_SECTOR_CHANGED, this.sectorChanged);
            }
            return;
        }

        private function detectorBlockChanged(arg1:flash.events.Event):void
        {
            this.layout.hglue = this.detector.selectedGlues[0];
            this.layout.vglue = this.detector.selectedGlues[1];
            this.updateIndicatorsState();
            return;
        }

        public function sectorChanged(arg1:flash.events.Event):void
        {
            var loc2:*=null;
            var loc1:*=this.placeholder.getChildAt(0) as flash.display.MovieClip;
            if (loc1.currentLabel) 
            {
                loc2 = loc1.currentLabel.replace("_in", "_out");
                loc1.gotoAndPlay(loc2);
                loc1.gotoAndPlay(this.detector.elementPosition.join("_") + "_in");
            }
            return;
        }

        public function startGlueing():void
        {
            var loc1:*=NaN;
            var loc2:*=NaN;
            var loc3:*=NaN;
            var loc4:*=NaN;
            if (this.layout.vglue.type != lesta.dialogs.custom_hud.CustomHudGlue.BORDER) 
            {
                loc1 = x;
                loc2 = this.layout.vglue.glueElementCoord - lesta.dialogs.custom_hud.CrossingDetector.getOffsetByConstraintType(this.placeholder, this.layout.vglue.elementConstraintType);
                lesta.cpp.Tween.to(this, 0.3, {"x":loc1}, {"x":loc2});
                this.layout.vglue.distanceFromGlue = 0;
                this.layout.vglue.active = true;
            }
            if (this.layout.hglue.type != lesta.dialogs.custom_hud.CustomHudGlue.BORDER) 
            {
                loc3 = y;
                loc4 = this.layout.hglue.glueElementCoord - lesta.dialogs.custom_hud.CrossingDetector.getOffsetByConstraintType(this.placeholder, this.layout.hglue.elementConstraintType);
                lesta.cpp.Tween.to(this, 0.3, {"y":loc3}, {"y":loc4});
                this.layout.hglue.distanceFromGlue = 0;
                this.layout.hglue.active = true;
            }
            return;
        }

        private function initIndicators():void
        {
            var loc1:*=0;
            var loc2:*=0;
            var loc3:*=null;
            var loc4:*=null;
            var loc5:*=0;
            var loc6:*=lesta.dialogs.custom_hud.CrossingDetector.HORIZONTAL_CONSTRAINTS;
            for each (loc1 in loc6) 
            {
                var loc7:*=0;
                var loc8:*=lesta.dialogs.custom_hud.CrossingDetector.VERTICAL_CONSTRAINTS;
                for each (loc2 in loc8) 
                {
                    loc3 = flash.utils.getDefinitionByName(this.crossClassName) as Class;
                    loc4 = new loc3();
                    addChild(loc4);
                    this.indicatorsMap[loc4] = [loc1, loc2];
                }
            }
            return;
        }

        private function updateIndicatorsLayout(arg1:Number, arg2:Number):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=this.indicatorsMap;
            for (loc1 in loc4) 
            {
                loc2 = this.indicatorsMap[loc1];
                loc1.y = lesta.dialogs.custom_hud.CrossingDetector.HORIZONTAL_CONSTRAINTS.indexOf(loc2[0]) * (arg2 / 2 - loc1.height / 2);
                loc1.x = lesta.dialogs.custom_hud.CrossingDetector.VERTICAL_CONSTRAINTS.indexOf(loc2[1]) * (arg1 / 2 - loc1.width / 2);
            }
            return;
        }

        private function updateIndicatorsState():void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=0;
            var loc4:*=0;
            var loc5:*=0;
            var loc6:*=this.indicatorsMap;
            for (loc1 in loc6) 
            {
                loc2 = this.indicatorsMap[loc1][0];
                loc3 = this.indicatorsMap[loc1][1];
                loc4 = !(this.layout.hglue.type == lesta.dialogs.custom_hud.CustomHudGlue.BORDER) && this.layout.hglue.elementConstraintType == loc2 || !(this.layout.vglue.type == lesta.dialogs.custom_hud.CustomHudGlue.BORDER) && this.layout.vglue.elementConstraintType == loc3 ? 2 : 1;
                loc1.gotoAndStop(loc4);
            }
            return;
        }

        public var placeholder:flash.display.MovieClip;

        public var frame:flash.display.MovieClip;

        public var menu:flash.display.MovieClip;

        private var _content:flash.display.DisplayObject;

        public var currentScale:Number=1;

        public var currentScaleIndex:Number=1;

        private var _layout:lesta.structs.CustomHudElement;

        private var _selected:Boolean=false;

        private var _glues:Array;

        private var indicatorsMap:flash.utils.Dictionary;

        private var detector:lesta.dialogs.custom_hud.CrossingDetector;

        private var crossClassName:String="CrossIndicator";
    }
}
