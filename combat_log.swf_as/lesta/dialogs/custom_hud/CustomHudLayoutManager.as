package lesta.dialogs.custom_hud 
{
    import flash.events.*;
    import flash.geom.*;
    import flash.utils.*;
    import lesta.dialogs.battle_window_new.*;
    import lesta.managers.windows.*;
    
    public class CustomHudLayoutManager extends lesta.dialogs.custom_hud.LayoutManager
    {
        public function CustomHudLayoutManager(arg1:lesta.managers.windows.Overlay, arg2:lesta.dialogs.battle_window_new.ConfigLoader)
        {
            this.moveGroup = [];
            this.selection = [];
            this.containerDeltaMap = new flash.utils.Dictionary();
            this.mouseCoord = new flash.geom.Point();
            super(arg1, arg2);
            return;
        }

        public override function init():void
        {
            elementsContainer.addEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.stageMouseDown);
            elementsContainer.addEventListener(flash.events.MouseEvent.MOUSE_UP, this.stageMouseUp);
            return;
        }

        public override function fini():void
        {
            super.fini();
            elementsContainer.removeEventListener(flash.events.MouseEvent.MOUSE_DOWN, this.stageMouseDown);
            elementsContainer.removeEventListener(flash.events.MouseEvent.MOUSE_UP, this.stageMouseUp);
            return;
        }

        private function updateSelection(arg1:Array):void
        {
            var loc2:*=null;
            var loc1:*=!(arg1[0] == this.selection[0]);
            this.selection = arg1;
            this.selectedContainer = this.selection[0];
            var loc3:*=0;
            var loc4:*=containers;
            for each (loc2 in loc4) 
            {
                loc2.selected = loc2 == this.selectedContainer;
            }
            if (loc1) 
            {
                dispatchEvent(new flash.events.Event(SELECTION_CHANGED));
            }
            return;
        }

        private function stageMouseDown(arg1:flash.events.MouseEvent):void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc1:*=[];
            var loc4:*=0;
            var loc5:*=containers;
            for each (loc2 in loc5) 
            {
                if (!loc2.hitTestPoint(arg1.stageX, arg1.stageY)) 
                {
                    continue;
                }
                loc1.push(loc2);
            }
            this.updateSelection(loc1);
            if (this.selection.length > 0) 
            {
                this.updateMoveGroup();
                elementsContainer.addEventListener(flash.events.Event.ENTER_FRAME, this.stageMouseMove);
                loc4 = 0;
                loc5 = this.moveGroup;
                for each (loc3 in loc5) 
                {
                    this.containerDeltaMap[loc3] = new flash.geom.Point(loc3.mouseX, loc3.mouseY);
                }
            }
            return;
        }

        private function stageMouseUp(arg1:flash.events.MouseEvent):void
        {
            elementsContainer.removeEventListener(flash.events.Event.ENTER_FRAME, this.stageMouseMove);
            if (this.selectedContainer) 
            {
                this.selectedContainer.startGlueing();
            }
            return;
        }

        private function updateMoveGroup():void
        {
            var loc2:*=null;
            var loc1:*=null;
            this.moveGroup = [];
            if (!this.selectedContainer || !this.selectedContainer.glues || !this.selectedContainer.layout) 
            {
                return;
            }
            this.moveGroup.push(this.selectedContainer);
            var loc3:*=0;
            var loc4:*=this.selectedContainer.glues;
            for each (loc1 in loc4) 
            {
                if (!(loc1.type == lesta.dialogs.custom_hud.CustomHudGlue.ELEMENT && loc1.isGluedToElement && this.moveGroup.indexOf(getContainerByName(loc1.glueElementID)) == -1)) 
                {
                    continue;
                }
                this.moveGroup.push(getContainerByName(loc1.glueElementID));
            }
            loc3 = 0;
            loc4 = containers;
            for each (loc2 in loc4) 
            {
                trace("updateGlue2", loc2, this.selectedContainer, this.selectedContainer["name"], loc2["layout"]);
                if (!loc2.layout || loc2 == this.selectedContainer || this.moveGroup.indexOf(this.selectedContainer) > -1) 
                {
                    continue;
                }
                var loc5:*=0;
                var loc6:*=loc2.glues;
                for each (loc1 in loc6) 
                {
                    if (!(loc1.glueElementID == this.selectedContainer.name && loc1.isGluedToElement && this.moveGroup.indexOf(loc2))) 
                    {
                        continue;
                    }
                    this.moveGroup.push(loc2);
                }
            }
            return;
        }

        private function stageMouseMove(arg1:flash.events.Event):void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc1:*="";
            var loc2:*="";
            var loc5:*=0;
            var loc6:*=this.moveGroup;
            for each (loc3 in loc6) 
            {
                loc4 = this.containerDeltaMap[loc3];
                loc3.x = elementsContainer.mouseX - loc4.x;
                loc3.y = elementsContainer.mouseY - loc4.y;
            }
            elementsContainer["debug_text"].text = "Container x: " + this.selectedContainer.x + " y: " + this.selectedContainer.y + "\n" + "results: ";
            dispatchEvent(new flash.events.Event(ELEMENTS_MOVE));
            return;
        }

        public static const IN_EDITOR:int=0;

        public static const IN_GAME:int=1;

        public static const SELECTION_CHANGED:String="selectionChanged";

        public static const ELEMENTS_MOVE:String="elementMoves";

        public var selectedContainer:lesta.dialogs.custom_hud.CustomHudContainer;

        public var moveGroup:Array;

        public var containerClass:String="ElementsContainer";

        private var selection:Array;

        private var containerDeltaMap:flash.utils.Dictionary;

        private var mouseCoord:flash.geom.Point;
    }
}
