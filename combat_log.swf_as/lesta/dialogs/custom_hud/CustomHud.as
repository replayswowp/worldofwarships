package lesta.dialogs.custom_hud 
{
    import flash.display.*;
    import flash.events.*;
    import flash.geom.*;
    import flash.text.*;
    import flash.utils.*;
    import lesta.dialogs.battle_window_new.*;
    import lesta.managers.windows.*;
    import scaleform.clik.controls.*;
    
    public class CustomHud extends lesta.managers.windows.Overlay
    {
        public function CustomHud()
        {
            this.crossingDetector = new lesta.dialogs.custom_hud.CrossingDetector();
            super();
            this.saveButton.addEventListener(flash.events.MouseEvent.CLICK, this.saveButton_click);
            this.loader = new lesta.dialogs.battle_window_new.ConfigLoader();
            this.loader.addEventListener(flash.events.Event.COMPLETE, this.onConfigsLoaded);
            this.hudLayoutManager = new lesta.dialogs.custom_hud.CustomHudLayoutManager(this, this.loader);
            this.hudLayoutManager.grid = this.grid;
            this.hudLayoutManager.addEventListener(lesta.dialogs.custom_hud.CustomHudLayoutManager.SELECTION_CHANGED, this.onSelectionChange);
            this.hudLayoutManager.addEventListener(lesta.dialogs.custom_hud.CustomHudLayoutManager.ELEMENTS_MOVE, this.onContainerMove);
            this.hudLayoutManager.init();
            return;
        }

        private function saveButton_click(arg1:flash.events.Event):void
        {
            this.loader.saveLayout(this.hudLayoutManager, true);
            return;
        }

        public override function updateStageSize():void
        {
            super.updateStageSize();
            if (!this.isLoaded) 
            {
                return;
            }
            if (this.hudLayoutManager.containers.length > 0) 
            {
                this.loader.saveLayout(this.hudLayoutManager);
            }
            this.grid.updateSize(stageWidth, stageHeight);
            this.crossingDetector.updateLines(this.grid.vertLinesActive, this.grid.horizLinesActive, new flash.geom.Point(stageWidth, stageHeight));
            this.hudLayoutManager.updateElements();
            this.crossingDetector.updateElements(this.hudLayoutManager.containers);
            this.debug_text.y = 0;
            this.debug_text.x = width / 2;
            this.debug_text.width = width / 2;
            this.debug_text.height = height / 2 - 20;
            return;
        }

        private function onConfigsLoaded():void
        {
            var loc1:*=flash.utils.getDefinitionByName(this.hudLayoutManager.containerClass) as Class;
            this.hudLayoutManager.addChildRecursive(this.loader.elementsCollection, this, loc1);
            this.isLoaded = true;
            this.updateStageSize();
            return;
        }

        private function onSelectionChange(arg1:flash.events.Event):void
        {
            if (this.hudLayoutManager.selectedContainer) 
            {
                this.crossingDetector.updateElement(this.hudLayoutManager.selectedContainer);
            }
            return;
        }

        private function onContainerMove(arg1:flash.events.Event):void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=this.hudLayoutManager.moveGroup;
            for each (loc1 in loc3) 
            {
                this.crossingDetector.updateElement(loc1);
                this.grid.updateLineState(this.crossingDetector.selectedGlues);
            }
            this.debug_text.text = this.crossingDetector.toString();
            return;
        }

        public var grid:lesta.dialogs.custom_hud.CustomHudGrid;

        public var selectedContainer:lesta.dialogs.custom_hud.CustomHudContainer;

        public var debug_text:flash.text.TextField;

        public var saveButton:flash.display.MovieClip;

        public var cancelButton:scaleform.clik.controls.Button;

        public var crossingDetector:lesta.dialogs.custom_hud.CrossingDetector;

        public var hudLayoutManager:lesta.dialogs.custom_hud.CustomHudLayoutManager;

        public var loader:lesta.dialogs.battle_window_new.ConfigLoader;

        private var isLoaded:Boolean=false;
    }
}
