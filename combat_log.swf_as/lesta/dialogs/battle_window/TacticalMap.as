package lesta.dialogs.battle_window 
{
    import flash.display.*;
    import flash.text.*;
    import flash.utils.*;
    
    public class TacticalMap extends flash.display.MovieClip
    {
        public function TacticalMap()
        {
            this.textElements = new flash.utils.Dictionary();
            super();
            return;
        }

        public function onPlaceTexts(arg1:Array):void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc1:*=0;
            var loc2:*=arg1.length;
            while (loc1 < loc2) 
            {
                loc3 = arg1[loc1];
                if (!(loc3.id in this.textElements)) 
                {
                    this.textElements[loc3.id] = addChild(this.createTextElement());
                    this.textElements[loc3.id].text = loc3.text;
                }
                loc4 = this.textElements[loc3.id];
                loc4.x = loc3.screenX - loc4.width * 0.5;
                loc4.y = loc3.screenY - loc4.height * 0.5;
                loc4.visible = !loc3.behindCamera;
                ++loc1;
            }
            return;
        }

        private function createTextElement():flash.text.TextField
        {
            var loc1:*=new flash.text.TextField();
            loc1.defaultTextFormat = new flash.text.TextFormat("$WWSDefaultFont", 13, 16777215);
            var loc2:*;
            loc1.mouseEnabled = loc2 = false;
            loc1.selectable = loc2;
            loc1.autoSize = flash.text.TextFieldAutoSize.CENTER;
            loc1.alpha = 0.3;
            return loc1;
        }

        private var textElements:flash.utils.Dictionary;
    }
}
