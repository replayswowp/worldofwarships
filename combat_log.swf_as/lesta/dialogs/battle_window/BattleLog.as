package lesta.dialogs.battle_window 
{
    import flash.display.*;
    import flash.events.*;
    import lesta.dialogs.battle_window._ingame_chat.*;
    import scaleform.gfx.*;
    
    public class BattleLog extends flash.display.MovieClip
    {
        public function BattleLog()
        {
            super();
            return;
        }

        private function init(arg1:flash.events.Event=null):void
        {
            scaleform.gfx.InteractiveObjectEx.setHitTestDisable(this.chat.txa_output["backgroundClip"], true);
            return;
        }

        public function free():void
        {
            this.chat.free();
            return;
        }

        public var chat:lesta.dialogs.battle_window._ingame_chat.QuickChat;
    }
}
