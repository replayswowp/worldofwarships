package lesta.dialogs.battle_window._player_stats_item 
{
    import flash.display.*;
    
    public class PriorityIcon extends flash.display.MovieClip
    {
        public function PriorityIcon()
        {
            super();
            stop();
            visible = false;
            return;
        }

        public function showCrosshair(arg1:int, arg2:Boolean):void
        {
            var loc1:*=arg2 ? this.crosshairFlag | 1 << arg1 : this.crosshairFlag & ~(1 << arg1);
            if (loc1 != this.crosshairFlag) 
            {
                this.crosshairFlag = loc1;
                var loc2:*;
                visible = loc2 = !(this.crosshairFlag == 0);
                if (loc2) 
                {
                    gotoAndStop(this.crosshairFlag);
                }
            }
            return;
        }

        private var crosshairFlag:uint=0;
    }
}
