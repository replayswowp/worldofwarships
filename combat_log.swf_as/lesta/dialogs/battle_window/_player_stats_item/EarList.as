package lesta.dialogs.battle_window._player_stats_item 
{
    import __AS3__.vec.*;
    import flash.display.*;
    import flash.events.*;
    import flash.utils.*;
    import lesta.constants.*;
    import lesta.data.*;
    import lesta.dialogs.battle_window_controllers.*;
    import lesta.structs.*;
    import lesta.utils.*;
    import scaleform.clik.data.*;
    import scaleform.gfx.*;
    
    public class EarList extends flash.display.Sprite
    {
        public function EarList()
        {
            this.items = new flash.utils.Dictionary();
            super();
            this.cacheAsBitmap = true;
            new Vector.<lesta.structs.Player>(2)[0] = null;
            new Vector.<lesta.structs.Player>(2)[1] = null;
            this.displayingTargetByWeaponType = new Vector.<lesta.structs.Player>(2);
            return;
        }

        public function init(arg1:scaleform.clik.data.DataProvider, arg2:Boolean=false):void
        {
            lesta.utils.Debug.assert(!(arg1 == null), "[EarList] init arguments: null dataProvider");
            this.initialized = true;
            this._dataProvider = arg1;
            this.isTrackingTargetPrioritiesEnabled = arg2;
            this._dataProvider.addEventListener(flash.events.Event.CHANGE, this.onPlayerDataUpdated);
            return;
        }

        public function fini():void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=this.items;
            for each (loc1 in loc3) 
            {
                this.complainController.detachEarsContextMenu(loc1);
            }
            this._dataProvider.removeEventListener(flash.events.Event.CHANGE, this.onPlayerDataUpdated);
            this._dataProvider = null;
            this.complainController = null;
            this.displayingTargetByWeaponType = null;
            lesta.data.GameDelegate.removeCallBack(this);
            return;
        }

        public function start():void
        {
            lesta.utils.Debug.assert(this.initialized, "[EarList] initialize EarList before using start");
            this.enabled = true;
            this._dataProvider.addEventListener(flash.events.Event.CHANGE, this.onPlayerDataUpdated);
            this.updateTrackingTargetPrioritesState();
            this.update();
            return;
        }

        public function stop():void
        {
            lesta.utils.Debug.assert(this.initialized, "[EarList] initialize EarList before using stop");
            this.enabled = false;
            this._dataProvider.removeEventListener(flash.events.Event.CHANGE, this.onPlayerDataUpdated);
            this.updateTrackingTargetPrioritesState();
            return;
        }

        public function setComplainController(arg1:lesta.dialogs.battle_window_controllers.ComplainController):void
        {
            this.complainController = arg1;
            return;
        }

        private function onPlayerDataUpdated(arg1:flash.events.Event):void
        {
            this.update();
            return;
        }

        private function onItemClicked(arg1:flash.events.MouseEvent):void
        {
            var loc1:*=arg1 as scaleform.gfx.MouseEventEx;
            if (!(loc1 == null) && !(loc1.buttonIdx == scaleform.gfx.MouseEventEx.LEFT_BUTTON) || loc1 == null) 
            {
                return;
            }
            var loc2:*=arg1.currentTarget as lesta.dialogs.battle_window._player_stats_item.PlayersStateItemRenderer;
            var loc3:*=loc2.data.relation == lesta.constants.PlayerRelation.ENEMY;
            var loc4:*=loc2.data.relation == lesta.constants.PlayerRelation.SELF;
            var loc5:*=loc2.data.relation == lesta.constants.PlayerRelation.FRIEND && loc2.data.isAlive;
            if (loc3 || !loc4 && !loc5) 
            {
                return;
            }
            lesta.data.GameDelegate.call("camera.moveCameraToPlayer", [loc2.data.id]);
            return;
        }

        private function onWeaponLockChanged(arg1:int):void
        {
            if (arg1 > lesta.constants.WeaponType.ATBA) 
            {
                return;
            }
            var loc1:*=this.displayingTargetByWeaponType[arg1];
            if (loc1) 
            {
                this.items[loc1.id].priorityIcon.showCrosshair(arg1, false);
            }
            var loc2:*;
            loc1 = loc2 = lesta.utils.GameInfoHolder.instance.mapTargets[arg1];
            this.displayingTargetByWeaponType[arg1] = loc2;
            if (loc1) 
            {
                this.items[loc1.id].priorityIcon.showCrosshair(arg1, true);
            }
            return;
        }

        private function update():void
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc1:*=0;
            var loc2:*=0;
            var loc3:*=this._dataProvider.length;
            while (loc2 < loc3) 
            {
                loc4 = this._dataProvider[loc2];
                loc5 = this.items[loc4.id];
                if (!loc5) 
                {
                    loc5 = this.addItem(loc4) as lesta.dialogs.battle_window._player_stats_item.PlayersStateItemRenderer;
                }
                loc5.data = loc4;
                loc5.y = loc1;
                loc1 = loc1 + loc5.height;
                ++loc2;
            }
            return;
        }

        private function addItem(arg1:lesta.structs.Player):flash.display.DisplayObject
        {
            var loc1:*=new this.itemRendererClass();
            loc1.name = "player_" + String(arg1.id);
            loc1.addEventListener(flash.events.MouseEvent.CLICK, this.onItemClicked, false, 0, true);
            loc1.mouseChildren = false;
            this.complainController.attachEarsContextMenu(loc1, arg1);
            this.items[arg1.id] = loc1;
            return addChild(loc1);
        }

        private function updateTrackingTargetPrioritesState():void
        {
            if (this.isTrackingTargetPrioritiesEnabled && this.enabled) 
            {
                lesta.data.GameDelegate.addCallBack("battle.weaponLockChanged", this, this.onWeaponLockChanged);
            }
            else 
            {
                lesta.data.GameDelegate.removeCallBack(this, "battle.weaponLockChanged", this.onWeaponLockChanged);
            }
            return;
        }

        public var itemRendererClass:Class;

        private var _dataProvider:scaleform.clik.data.DataProvider;

        private var complainController:lesta.dialogs.battle_window_controllers.ComplainController;

        private var items:flash.utils.Dictionary;

        private var displayingTargetByWeaponType:__AS3__.vec.Vector.<lesta.structs.Player>;

        private var isTrackingTargetPrioritiesEnabled:Boolean=false;

        private var enabled:Boolean=false;

        private var initialized:Boolean=false;
    }
}
