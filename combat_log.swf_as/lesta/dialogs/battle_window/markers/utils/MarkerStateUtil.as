package lesta.dialogs.battle_window.markers.utils 
{
    import __AS3__.vec.*;
    import lesta.constants.*;
    
    public class MarkerStateUtil extends Object
    {
        public function MarkerStateUtil()
        {
            super();
            return;
        }

        public static function getStringsByMask(arg1:int):Array
        {
            var loc4:*=null;
            var loc5:*=0;
            var loc6:*=null;
            var loc1:*=new Array();
            var loc2:*=0;
            var loc3:*=SOURCE.length;
            while (loc2 < loc3) 
            {
                loc4 = SOURCE[loc2];
                loc5 = arg1 & 15;
                loc6 = loc4[loc5];
                loc1.push(loc6);
                arg1 = arg1 >> 4;
                ++loc2;
            }
            return loc1;
        }

        private static function getStates():__AS3__.vec.Vector.<Object>
        {
            var loc4:*=null;
            var loc5:*=0;
            var loc6:*=null;
            var loc7:*=0;
            var loc1:*=SOURCE.length;
            var loc2:*=new Vector.<Object>(loc1, true);
            var loc3:*=0;
            while (loc3 < loc1) 
            {
                loc4 = SOURCE[loc3];
                loc5 = loc4.length;
                loc6 = {};
                loc2[loc3] = loc6;
                loc7 = 0;
                while (loc7 < loc5) 
                {
                    loc6[loc4[loc7]] = loc7;
                    ++loc7;
                }
                ++loc3;
            }
            return loc2;
        }

        private static function getStatesCounters():__AS3__.vec.Vector.<int>
        {
            var loc1:*=SOURCE.length;
            var loc2:*=new Vector.<int>(loc1, true);
            var loc3:*=0;
            while (loc3 < loc1) 
            {
                loc2[loc3] = SOURCE[loc3].length;
                ++loc3;
            }
            return loc2;
        }

        public static function getByteMaskFromString(arg1:String):int
        {
            var loc5:*=null;
            var loc6:*=null;
            var loc1:*=0;
            var loc2:*=arg1.split(",");
            var loc3:*=loc2.length;
            var loc4:*=0;
            while (loc4 < loc3) 
            {
                loc5 = loc2[loc4];
                loc6 = STATES[loc4];
                loc1 = loc1 | loc6[loc5] << BITS_ON_PARAMETER * loc4;
                ++loc4;
            }
            return loc1;
        }

        public static function getByteMasksVectorFromString(arg1:String):__AS3__.vec.Vector.<int>
        {
            new Vector.<int>(1)[0] = 0;
            var loc1:*=new Vector.<int>(1);
            var loc2:*=arg1.split(",");
            var loc3:*=loc2.length;
            var loc4:*=0;
            while (loc4 < loc3) 
            {
                if (loc2[loc4] != "*") 
                {
                    updateCurrentMasks(loc1, loc4, loc2[loc4]);
                }
                else 
                {
                    loc1 = generateByteMasks(loc1, loc4);
                }
                ++loc4;
            }
            return loc1;
        }

        private static function generateByteMasks(arg1:__AS3__.vec.Vector.<int>, arg2:int):__AS3__.vec.Vector.<int>
        {
            var loc5:*=0;
            var loc6:*=0;
            var loc1:*=STATES_COUNTERS[arg2];
            var loc2:*=arg1.length;
            var loc3:*=new Vector.<int>(loc1 * loc2, true);
            var loc4:*=0;
            while (loc4 < loc2) 
            {
                loc5 = arg1[loc4];
                loc6 = 0;
                while (loc6 < loc1) 
                {
                    loc3[loc4 * loc1 + loc6] = loc5 | loc6 << BITS_ON_PARAMETER * arg2;
                    ++loc6;
                }
                ++loc4;
            }
            return loc3;
        }

        private static function updateCurrentMasks(arg1:__AS3__.vec.Vector.<int>, arg2:int, arg3:String):void
        {
            var loc3:*=null;
            var loc1:*=arg1.length;
            var loc2:*=0;
            while (loc2 < loc1) 
            {
                loc3 = STATES[arg2];
                arg1[loc2] = arg1[loc2] | loc3[arg3] << BITS_ON_PARAMETER * arg2;
                ++loc2;
            }
            return;
        }

        public static const BITS_ON_PARAMETER:int=4;

        public static const SOURCE:Array=[["normal", "extended"], ["player", "ally", "enemy"], ["ship", "plane"], ["alive", "dead"], ["near", "far", "farthest", "tactical"], ["up", "over", "selected"]];

        private static const STATES:__AS3__.vec.Vector.<Object>=getStates();

        private static const STATES_COUNTERS:__AS3__.vec.Vector.<int>=getStatesCounters();

        private static const DISTANCES:Array=["near", "far", "farthest", "tactical"];

        public static const DISTANCES_LENGTH:int=DISTANCES.length;

        public static const MAP_MARKER_TYPE:Object={"ship":lesta.dialogs.battle_window.markers.utils.MarkerTypes.SHIP, "plane":lesta.dialogs.battle_window.markers.utils.MarkerTypes.PLANE};

        public static const MAP_RELATION:Object={"player":lesta.constants.PlayerRelation.SELF, "ally":lesta.constants.PlayerRelation.FRIEND, "enemy":lesta.constants.PlayerRelation.ENEMY};

        public static const MAP_ALIVE:Object={"dead":false, "alive":true};

        public static const MAP_ALT_VISION:Object={"normal":false, "extended":true};
    }
}
