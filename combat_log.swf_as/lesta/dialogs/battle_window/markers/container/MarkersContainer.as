package lesta.dialogs.battle_window.markers.container 
{
    import flash.display.*;
    import lesta.dialogs.battle_window.markers.config.*;
    import lesta.dialogs.battle_window.markers.container.views.base.*;
    import lesta.dialogs.battle_window.markers.settings.*;
    import lesta.structs.*;
    
    public class MarkersContainer extends flash.display.Sprite
    {
        public function MarkersContainer(arg1:int, arg2:lesta.dialogs.battle_window.markers.config.MarkersConfig, arg3:lesta.dialogs.battle_window.markers.settings.MarkersSettings)
        {
            super();
            this._type = arg1;
            this._configs = arg2;
            this._settings = arg3;
            this.init();
            return;
        }

        private function init():void
        {
            this._markersObject = {};
            this.markersArray = [];
            return;
        }

        public function fini():void
        {
            var loc1:*=null;
            this._dataProvider = null;
            this._configs = null;
            this._settings = null;
            this.markersArray = null;
            this._markersObject = null;
            if (this._markersLinkedList != null) 
            {
                loc1 = this._markersLinkedList;
                do 
                {
                    loc1.value.fini();
                    var loc2:*;
                    loc1 = loc2 = loc1.next;
                }
                while (loc2);
                this._markersLinkedList.fini();
            }
            this._markersLinkedList = null;
            return;
        }

        public function getMarker(arg1:int):lesta.dialogs.battle_window.markers.container.views.base.SimpleMarker
        {
            return this._markersObject[arg1];
        }

        public function update(arg1:Array, arg2:int, arg3:int, arg4:Number, arg5:Boolean=false):void
        {
            this.createMarkersAndSetParameters();
            if (this.markersArray.length) 
            {
                this.updateMarkers(this._markersLinkedList, arg1, arg2, arg3, arg4, arg5);
                this.sort();
            }
            return;
        }

        public function set MarkerClass(arg1:Class):void
        {
            this._MarkerClass = arg1;
            return;
        }

        public function set dataProvider(arg1:Array):void
        {
            this._dataProvider = arg1;
            return;
        }

        protected function createMarkersAndSetParameters():void
        {
            var loc1:*=null;
            if (this._dataProvider.length == this.length) 
            {
                return;
            }
            var loc2:*=0;
            var loc3:*=this._dataProvider;
            for each (loc1 in loc3) 
            {
                if (loc1.id == -1) 
                {
                    continue;
                }
                if (this._markersObject.hasOwnProperty(loc1.id)) 
                {
                    continue;
                }
                this.createMarker(loc1);
            }
            return;
        }

        protected function createMarker(arg1:lesta.structs.EntityInfo):lesta.dialogs.battle_window.markers.container.views.base.SimpleMarker
        {
            var loc1:*=new this._MarkerClass() as lesta.dialogs.battle_window.markers.container.views.base.SimpleMarker;
            loc1.configs = this._configs;
            loc1.settings = this._settings;
            loc1.init();
            loc1.info = arg1;
            addChild(loc1);
            this._markersObject[arg1.id] = loc1;
            this.markersArray.push(loc1);
            this._markersLinkedList = new lesta.dialogs.battle_window.markers.container.MarkersLinkedList(loc1, this._markersLinkedList);
            var loc2:*;
            var loc3:*=((loc2 = this).length + 1);
            loc2.length = loc3;
            return loc1;
        }

        protected function updateMarkers(arg1:lesta.dialogs.battle_window.markers.container.MarkersLinkedList, arg2:Array, arg3:int, arg4:int, arg5:Number, arg6:Boolean=false):void
        {
            do 
            {
                arg1.value.updatePositions(arg2, arg3, arg4, arg5);
                if (arg1.value.visible && (arg1.value.needInitialUpdate || arg6)) 
                {
                    arg1.value.stateUpdate();
                }
                var loc1:*;
                arg1 = loc1 = arg1.next;
            }
            while (loc1);
            this.needToStartStateUpdates = true;
            return;
        }

        public function updateMarkersStatesTick():void
        {
            if (!this.markersArray.length) 
            {
                return;
            }
            if (this.updateCounter >= this.markersArray.length) 
            {
                this.updateCounter = 0;
            }
            if (this.updateCounter > 0 || this.needToStartStateUpdates) 
            {
                this.needToStartStateUpdates = false;
                this.markersArray[this.updateCounter].stateUpdate();
                var loc1:*;
                var loc2:*=((loc1 = this).updateCounter + 1);
                loc1.updateCounter = loc2;
            }
            return;
        }

        protected function sort():void
        {
            this.markersArray.sortOn("distanceToCamera", Array.NUMERIC | Array.DESCENDING);
            var loc1:*=0;
            while (loc1 < this.length) 
            {
                setChildIndex(this.markersArray[loc1], loc1);
                ++loc1;
            }
            return;
        }

        protected var _dataProvider:Array;

        protected var _configs:lesta.dialogs.battle_window.markers.config.MarkersConfig;

        protected var _settings:lesta.dialogs.battle_window.markers.settings.MarkersSettings;

        protected var _markersObject:Object;

        protected var _markersLinkedList:lesta.dialogs.battle_window.markers.container.MarkersLinkedList;

        public var markersArray:Array;

        public var length:int;

        protected var _MarkerClass:Class;

        protected var _type:int=0;

        private var updateCounter:int=0;

        private var needToStartStateUpdates:Boolean=true;
    }
}
