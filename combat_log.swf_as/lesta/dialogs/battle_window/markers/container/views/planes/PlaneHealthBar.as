package lesta.dialogs.battle_window.markers.container.views.planes 
{
    import flash.display.*;
    import flash.utils.*;
    import lesta.cpp.*;
    
    public class PlaneHealthBar extends flash.display.MovieClip
    {
        public function PlaneHealthBar()
        {
            super();
            return;
        }

        public function setMaxCount(arg1:int):void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc1:*=this.value.x;
            this.scaleSize = (this.bg.width - this.GAP * 2 - this.GAP * (arg1 - 1)) / arg1;
            var loc2:*=1;
            while (loc2 < arg1) 
            {
                loc3 = flash.utils.getDefinitionByName("barDivider") as Class;
                loc4 = new loc3() as flash.display.DisplayObject;
                loc1 = loc1 + this.scaleSize + this.GAP;
                addChild(loc4);
                loc4.y = 1;
                loc4.x = loc1;
                ++loc2;
            }
            return;
        }

        public function setCurrentCount(arg1:int):void
        {
            var loc1:*=(this.scaleSize + this.GAP) * arg1 - this.GAP;
            var loc2:*=this.value.width;
            if (loc1 == loc2) 
            {
                return;
            }
            this.value.width = loc1;
            lesta.cpp.Tween.to(this.mat, MAT_CHANGE_TIME, {"width":loc2}, {"width":loc1});
            return;
        }

        private const GAP:int=1;

        public static const MAT_CHANGE_TIME:Number=0.75;

        public var bg:flash.display.MovieClip;

        public var value:flash.display.MovieClip;

        public var mat:flash.display.MovieClip;

        private var scaleSize:Number;
    }
}
