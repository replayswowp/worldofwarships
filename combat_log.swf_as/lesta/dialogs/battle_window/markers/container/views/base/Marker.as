package lesta.dialogs.battle_window.markers.container.views.base 
{
    import flash.display.*;
    import lesta.dialogs.battle_window.*;
    import lesta.dialogs.battle_window.markers.config.*;
    import lesta.dialogs.battle_window.markers.config.elements.*;
    import lesta.dialogs.battle_window.markers.utils.*;
    import lesta.structs.*;
    
    public class Marker extends lesta.dialogs.battle_window.markers.container.views.base.SimpleMarker
    {
        public function Marker()
        {
            super();
            this.priorityClip.visible = false;
            return;
        }

        public override function init():void
        {
            super.init();
            this.readChildren();
            return;
        }

        private function readChildren():void
        {
            var loc1:*=null;
            var loc3:*=null;
            this._listElements = [];
            this._elements = {};
            var loc2:*=0;
            while (loc2 < numChildren) 
            {
                loc1 = getChildAt(loc2);
                if (loc1.name != "priorityClip") 
                {
                    loc3 = new lesta.dialogs.battle_window.markers.container.views.base.MarkerElement(loc1);
                    loc3.name = loc1.name;
                    loc3.addChild(loc1);
                    addChildAt(loc3, loc2);
                    this._elements[loc1.name] = loc3;
                    this._listElements.push(loc3);
                }
                ++loc2;
            }
            addChild(this.priorityClip);
            return;
        }

        public override function fini():void
        {
            this.currentConfig = null;
            this.navpointsAmount = 0;
            super.fini();
            return;
        }

        public function set highlighted(arg1:Boolean):void
        {
            if (this._highlighted == arg1) 
            {
                return;
            }
            this._highlighted = arg1;
            return;
        }

        public function set priority(arg1:Boolean):void
        {
            if (this._priority == arg1) 
            {
                return;
            }
            this._priority = arg1;
            this.priorityClip.visible = this._priority;
            this.updatePriorityoffset();
            return;
        }

        public function get priority():Boolean
        {
            return this._priority;
        }

        protected override function localUpdate():void
        {
            super.localUpdate();
            this.updateAllElementsConfig();
            return;
        }

        public override function set info(arg1:lesta.structs.EntityInfo):void
        {
            super.info = arg1;
            this._maskPart = _info.relation << lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.BITS_ON_PARAMETER | _type << lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.BITS_ON_PARAMETER * 2;
            return;
        }

        public function getElelentByName(arg1:String):lesta.dialogs.battle_window.markers.container.views.base.MarkerElement
        {
            return this._elements[arg1];
        }

        public function set navpointsAmount(arg1:int):void
        {
            this._navpointsAmount = arg1;
            this.updatePriorityoffset();
            return;
        }

        public function get navpointsAmount():int
        {
            return this._navpointsAmount;
        }

        public function updatePriorityoffset():void
        {
            this.priorityClip.x = this.icon.x + (this._navpointsAmount ? lesta.dialogs.battle_window.PlaneNavpoints.NAVPOINT_WIDTH * 0.5 - (lesta.dialogs.battle_window.PlaneNavpoints.NAVPOINT_WIDTH * (this._navpointsAmount + 1) + lesta.dialogs.battle_window.PlaneNavpoints.NAVPOINT_GAP * this._navpointsAmount) * 0.5 : 0);
            this.priorityClip.y = this.icon.y - lesta.dialogs.battle_window.PlaneNavpointsController.NAVPOINTS_OFFSET;
            return;
        }

        private function updateAllElementsConfig():void
        {
            var loc1:*=this.getDistanceIndex();
            var loc2:*=_info.isSelected ? 2 : this._highlighted ? 1 : 0;
            var loc3:*=int(_settings.isAlternativeView) | this._maskPart | int(!_info.isAlive) << lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.BITS_ON_PARAMETER * 3 | loc1 << lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.BITS_ON_PARAMETER * 4 | loc2 << lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.BITS_ON_PARAMETER * 5;
            if (this._configMask == loc3 && !(this.currentConfig == null) && !this.currentConfig.changed && this.displayingEmptyConfig == this.currentConfig.empty) 
            {
                return;
            }
            var loc4:*=0;
            if (!(loc2 == this._selectionIndex) && !(this._selectionIndex == -1)) 
            {
                loc4 = DURATION_MAP[this._selectionIndex][loc2];
            }
            this._selectionIndex = loc2;
            this._configMask = loc3;
            this.currentConfig = _configs.getConfigByMask(this._configMask);
            if (!this.currentConfig) 
            {
                return;
            }
            this.currentConfig.changed = false;
            this.displayingEmptyConfig = this.currentConfig.empty;
            this.updateElementsConfig(this.currentConfig.elements, loc4);
            this.updatePriorityoffset();
            return;
        }

        private function getDistanceIndex():int
        {
            var loc1:*=0;
            if (_settings.isTacticalMap) 
            {
                return 3;
            }
            if (_info.distanceToCamera <= _settings.near) 
            {
                return 0;
            }
            if (_info.distanceToCamera > _settings.far) 
            {
                return 2;
            }
            if (_info.distanceToCamera > _settings.near && _info.distanceToCamera <= _settings.far) 
            {
                if (_settings.intermediateDistancesLength == 0 || _info.distanceToCamera > _settings.intermediateDistancesLast) 
                {
                    return 1;
                }
                loc1 = 0;
                while (loc1 < _settings.intermediateDistancesLength) 
                {
                    if (_info.distanceToCamera < _settings.intermediateDistances[loc1]) 
                    {
                        return lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.DISTANCES_LENGTH + loc1;
                    }
                    ++loc1;
                }
                return 0;
            }
            return 0;
        }

        private function updateElementsConfig(arg1:lesta.dialogs.battle_window.markers.config.elements.MarkerElementsConfig, arg2:Number=0):void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc1:*=this._listElements.length;
            var loc2:*=0;
            while (loc2 < loc1) 
            {
                loc3 = this._listElements[loc2];
                loc4 = arg1.getElementByName(loc3.name);
                if (loc3) 
                {
                    this.setConfigToElement(loc3, loc4, arg2);
                }
                ++loc2;
            }
            return;
        }

        protected function setConfigToElement(arg1:lesta.dialogs.battle_window.markers.container.views.base.MarkerElement, arg2:lesta.dialogs.battle_window.markers.config.elements.MarkerElementConfig, arg3:Number=0):void
        {
            var loc1:*=NaN;
            var loc2:*=NaN;
            arg1.visible = arg2.visible;
            if (arg1.visible) 
            {
                loc1 = arg2.horizontalOffset + (arg2.pinnedToConfig == null ? 0 : arg2.pinnedToConfig.horizontalOffset);
                loc2 = arg2.verticalOffset + (arg2.pinnedToConfig == null ? 0 : arg2.pinnedToConfig.verticalOffset);
                if (_settings.forceVisible || arg3 == 0) 
                {
                    arg1.displayObject.x = loc1;
                    arg1.displayObject.y = loc2;
                    arg1.displayObject.scaleX = arg2.scaleX;
                    arg1.displayObject.scaleY = arg2.scaleY;
                }
                else 
                {
                    arg1.startProperties.x = arg1.displayObject.x;
                    arg1.startProperties.y = arg1.displayObject.y;
                    arg1.startProperties.scaleX = arg1.displayObject.scaleX;
                    arg1.startProperties.scaleY = arg1.displayObject.scaleY;
                    arg1.finishProperties.x = loc1;
                    arg1.finishProperties.y = loc2;
                    arg1.finishProperties.scaleX = arg2.scaleX;
                    arg1.finishProperties.scaleY = arg2.scaleY;
                    arg1.startTween(arg3);
                }
                arg1.setFontColor(arg2.fontColor);
            }
            return;
        }

        private static const DEFAULT_TWEEN_DURATION:Number=0.1;

        private static const DURATION_MAP:Array=[[0.01, DEFAULT_TWEEN_DURATION, 0.01, DEFAULT_TWEEN_DURATION], [0.01, DEFAULT_TWEEN_DURATION, 0.01, DEFAULT_TWEEN_DURATION], [0.01, DEFAULT_TWEEN_DURATION, 0.01, DEFAULT_TWEEN_DURATION], [DEFAULT_TWEEN_DURATION, DEFAULT_TWEEN_DURATION, DEFAULT_TWEEN_DURATION, DEFAULT_TWEEN_DURATION]];

        public var currentConfig:lesta.dialogs.battle_window.markers.config.MarkerConfig=null;

        public var priorityClip:flash.display.MovieClip;

        public var _navpointsAmount:int;

        private var _elements:Object;

        private var _listElements:Array;

        private var _highlighted:Boolean;

        private var _priority:Boolean;

        private var _configMask:int;

        public var icon:flash.display.MovieClip;

        private var displayingEmptyConfig:Boolean=true;

        private var _maskPart:int;

        protected var _isSelected:Boolean=false;

        private var _selectionIndex:int=-1;
    }
}
