package lesta.dialogs.battle_window.markers.container.views.base 
{
    import flash.display.*;
    import flash.utils.*;
    import lesta.dialogs.battle_window.markers.container.views.*;
    
    public class TargetAnimationMarker extends flash.display.MovieClip
    {
        public function TargetAnimationMarker()
        {
            this.controllers = new flash.utils.Dictionary();
            super();
            return;
        }

        public function fini():void
        {
            this.controllers = null;
            return;
        }

        protected function addAnimationController(arg1:String, arg2:flash.display.DisplayObjectContainer, arg3:String):void
        {
            this.controllers[arg1] = new lesta.dialogs.battle_window.markers.container.views.TargetAnimationController(arg2, arg3);
            return;
        }

        public function playTargetAnimation(arg1:String, arg2:String):void
        {
            this.controllers[arg1].playTarget(arg2);
            return;
        }

        public function clearTargetAnimation():void
        {
            var loc1:*=null;
            var loc2:*=0;
            var loc3:*=this.controllers;
            for each (loc1 in loc3) 
            {
                loc1.clear();
            }
            return;
        }

        private var controllers:flash.utils.Dictionary;
    }
}
