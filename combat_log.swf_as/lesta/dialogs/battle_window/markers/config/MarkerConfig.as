package lesta.dialogs.battle_window.markers.config 
{
    import lesta.dialogs.battle_window.markers.config.elements.*;
    import lesta.dialogs.battle_window.markers.utils.*;
    
    public class MarkerConfig extends Object
    {
        public function MarkerConfig(arg1:int, arg2:String="-")
        {
            super();
            this.init();
            this.mask = arg1;
            this.strMask = arg2;
            return;
        }

        private function init():void
        {
            this._elements = new lesta.dialogs.battle_window.markers.config.elements.MarkerElementsConfig();
            return;
        }

        public function get elements():lesta.dialogs.battle_window.markers.config.elements.MarkerElementsConfig
        {
            return this._elements;
        }

        public function copy(arg1:lesta.dialogs.battle_window.markers.config.MarkerConfig):void
        {
            var loc3:*=null;
            var loc4:*=null;
            var loc1:*=0;
            var loc2:*=arg1.elements.length;
            while (loc1 < loc2) 
            {
                loc3 = arg1.elements.getElementByIndex(loc1);
                loc4 = this._elements.getElementByName(loc3.name);
                loc4.copy(loc3);
                ++loc1;
            }
            this._elements.updatePinnedRefs();
            return;
        }

        public function generate(arg1:lesta.dialogs.battle_window.markers.config.MarkerConfig, arg2:lesta.dialogs.battle_window.markers.config.MarkerConfig, arg3:Number):void
        {
            this.elements.generate(arg1.elements, arg2.elements, arg3);
            return;
        }

        public function getXML():XML
        {
            var loc6:*=null;
            var loc1:*=lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.getStringsByMask(this.mask);
            var loc2:*=loc1.join(",");
            var loc3:*=new XML("<state type=\"" + loc2 + "\">\r\n\t\t\t</state>");
            var loc4:*=0;
            var loc5:*=this._elements.length;
            while (loc4 < loc5) 
            {
                loc6 = this._elements.getElementByIndex(loc4);
                loc3.appendChild(loc6.getXML());
                ++loc4;
            }
            return loc3;
        }

        public var mask:int=0;

        public var strMask:String="-";

        private var _elements:lesta.dialogs.battle_window.markers.config.elements.MarkerElementsConfig;

        public var empty:Boolean=true;

        public var changed:Boolean=false;
    }
}
