package lesta.dialogs.battle_window.markers.config 
{
    import __AS3__.vec.*;
    import lesta.dialogs.battle_window.markers.utils.*;
    
    public class MarkersConfig extends Object
    {
        public function MarkersConfig()
        {
            super();
            this.init();
            return;
        }

        private function init():void
        {
            this._configs = {};
            this._listConfigs = [];
            return;
        }

        public function loadFromXML(arg1:XML):void
        {
            this.loadElementsFromXML(arg1.states[0]);
            return;
        }

        public function getXML():XML
        {
            var loc4:*=null;
            var loc5:*=null;
            var loc1:*=new XML("<data>\r\n\t\t\t\t<states>\r\n\t\t\t\t</states>\r\n\t\t\t</data>");
            var loc2:*=0;
            var loc3:*=this._listConfigs.length;
            while (loc2 < loc3) 
            {
                loc4 = this._listConfigs[loc2];
                loc5 = loc4.getXML();
                loc1.states.appendChild(loc5);
                ++loc2;
            }
            return loc1;
        }

        public function getConfigByMask(arg1:int):lesta.dialogs.battle_window.markers.config.MarkerConfig
        {
            var loc1:*=this._configs[arg1];
            if (loc1 == null) 
            {
                loc1 = new lesta.dialogs.battle_window.markers.config.MarkerConfig(arg1);
                this._configs[arg1] = loc1;
                this._listConfigs.push(loc1);
            }
            return loc1;
        }

        public function generateConfigs(arg1:Number, arg2:Number, arg3:Array):void
        {
            var loc6:*=0;
            var loc7:*=null;
            var loc8:*=null;
            var loc9:*=0;
            var loc10:*=0;
            var loc11:*=0;
            var loc12:*=0;
            var loc13:*=NaN;
            var loc14:*=0;
            var loc15:*=0;
            var loc16:*=null;
            var loc1:*=lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.getByteMasksVectorFromString("*,*,*,*,near");
            var loc2:*=lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.getByteMasksVectorFromString("*,*,*,*,far");
            var loc3:*=arg2 - arg1;
            var loc4:*=loc1.length;
            var loc5:*=0;
            while (loc5 < loc4) 
            {
                loc6 = loc1[loc5];
                loc7 = this._configs[loc1[loc5]];
                loc8 = this._configs[loc2[loc5]];
                if (!(!loc7 || !loc8)) 
                {
                    loc9 = arg3.length;
                    loc10 = 0;
                    while (loc10 < loc9) 
                    {
                        loc11 = arg3[loc10];
                        loc12 = loc11 - arg1;
                        loc13 = loc12 / loc3;
                        loc14 = lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.DISTANCES_LENGTH + loc10;
                        loc15 = loc6 | loc14 << lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.BITS_ON_PARAMETER * 4;
                        loc16 = new lesta.dialogs.battle_window.markers.config.MarkerConfig(loc15);
                        loc16.generate(loc7, loc8, loc13);
                        this._configs[loc15] = loc16;
                        ++loc10;
                    }
                }
                ++loc5;
            }
            return;
        }

        private function loadElementsFromXML(arg1:XML):void
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=0;
            var loc4:*=null;
            var loc5:*=0;
            var loc6:*=arg1.state;
            for each (loc1 in loc6) 
            {
                loc2 = loc1.@type;
                loc3 = lesta.dialogs.battle_window.markers.utils.MarkerStateUtil.getByteMaskFromString(loc2);
                loc4 = this._configs[loc3];
                if (loc4 == null) 
                {
                    loc4 = new lesta.dialogs.battle_window.markers.config.MarkerConfig(loc3, loc2);
                    this._configs[loc3] = loc4;
                    this._listConfigs.push(loc4);
                }
                loc4.elements.loadFromXML(loc1);
                loc4.empty = false;
            }
            return;
        }

        private var _configs:Object;

        private var _listConfigs:Array;
    }
}
