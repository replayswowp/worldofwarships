package lesta.constants 
{
    public class ShipModules extends Object
    {
        public function ShipModules()
        {
            super();
            return;
        }

        
        {
            LIST_FRAMES_ORDER = [ENGINE, MAIN_CALIBER, ATBA_GUN, AVIATION, AIR_DEFENSE, OBSERVATION, TORPEDO_TUBE, PATH_CONTROL];
        }

        public static const ENGINE:int=0;

        public static const MAIN_CALIBER:int=1;

        public static const ATBA_GUN:int=2;

        public static const AVIATION:int=3;

        public static const AIR_DEFENSE:int=4;

        public static const OBSERVATION:int=5;

        public static const TORPEDO_TUBE:int=6;

        public static const PATH_CONTROL:int=7;

        public static var LIST_FRAMES_ORDER:Array;
    }
}
