package lesta.constants 
{
    public class PlayerRelation extends Object
    {
        public function PlayerRelation()
        {
            super();
            return;
        }

        public static const SELF:uint=0;

        public static const FRIEND:uint=1;

        public static const ENEMY:uint=2;

        public static const NEUTRAL:uint=3;
    }
}
