package lesta.data 
{
    import flash.utils.*;
    import lesta.utils.*;
    import scaleform.clik.data.*;
    
    public class DataHub extends Object
    {
        public function DataHub()
        {
            this.collections = new flash.utils.Dictionary(true);
            this.objects = new flash.utils.Dictionary(true);
            this.subscribers = new flash.utils.Dictionary(true);
            this.cachedCallbacksByType = new flash.utils.Dictionary(true);
            this.dataChannelsToCollectionMap = new flash.utils.Dictionary(true);
            super();
            return;
        }

        public function init():void
        {
            lesta.data.GameDelegate.addCallBack("getCollectionByDataChannelID", this, this.getCollectionByDataChannelID);
            return;
        }

        public function getObject(arg1:String):Object
        {
            return this.objects[arg1].getData();
        }

        public function getCollectionAsList(arg1:String):scaleform.clik.data.DataProvider
        {
            return this.collections[arg1].getList();
        }

        public function getCollectionAsDictionary(arg1:String):flash.utils.Dictionary
        {
            return this.collections[arg1].getMap();
        }

        public function getDataByID(arg1:String, arg2:*):Object
        {
            if (arg1 in this.collections) 
            {
                return this.collections[arg1].getMap()[arg2];
            }
            return null;
        }

        public function getDataByIndex(arg1:String, arg2:int):Object
        {
            return this.collections[arg1].getList()[arg2];
        }

        public function addCallback(arg1:Object, arg2:String, arg3:Function):void
        {
            lesta.utils.Debug.assert(!(arg1 == null), "GameInfoHolder: try add callback for null scope");
            if (arg1 == null) 
            {
                return;
            }
            var loc1:*=this.subscribers[arg1];
            if (loc1 == null) 
            {
                loc1 = new flash.utils.Dictionary(true);
                this.subscribers[arg1] = loc1;
            }
            lesta.utils.Debug.assert(!(arg2 in loc1), "DataHub.addCallback: only one callback per scope per type is allowed! " + arg1 + " " + arg2);
            loc1[arg2] = arg3;
            var loc2:*=this.getCachedCallbacks(arg2);
            loc2[arg1] = arg3;
            return;
        }

        public function hasCallback(arg1:Object, arg2:String=null):Boolean
        {
            var loc1:*=null;
            if (arg2 != null) 
            {
                loc1 = this.subscribers[arg1];
                if (loc1 != null) 
                {
                    return !(loc1[arg2] == undefined);
                }
                return false;
            }
            return !(this.subscribers[arg1] == undefined);
        }

        public function removeCallback(arg1:Object, arg2:String=null):void
        {
            var loc1:*=null;
            if (arg2 == null) 
            {
                var loc2:*=0;
                var loc3:*=this.subscribers[arg1];
                for (arg2 in loc3) 
                {
                    delete this.cachedCallbacksByType[arg2][arg1];
                }
                delete this.subscribers[arg1];
            }
            else 
            {
                delete this.cachedCallbacksByType[arg2][arg1];
                loc1 = this.subscribers[arg1];
                if (loc1 != null) 
                {
                    delete loc1[arg2];
                }
            }
            return;
        }

        protected function createSharedCollection(arg1:*, arg2:String, arg3:Class=null, arg4:Function=null):lesta.data.EntityCollection
        {
            var loc2:*;
            this.collections[arg2] = loc2 = new lesta.data.EntityCollection(arg2, arg3, arg4);
            var loc1:*=loc2;
            this.installSharedObject(loc1, arg1, arg2);
            return loc1;
        }

        protected function createSharedSingleton(arg1:*, arg2:String, arg3:Class=null):lesta.data.EntitySingleton
        {
            var loc2:*;
            this.objects[arg2] = loc2 = new lesta.data.EntitySingleton(arg2, arg3);
            var loc1:*=loc2;
            this.installSharedObject(loc1, arg1, arg2);
            return loc1;
        }

        public function invokeCallback(arg1:String, arg2:Array=null):void
        {
            var loc2:*=null;
            var loc1:*=this.getCachedCallbacks(arg1);
            var loc3:*=0;
            var loc4:*=loc1;
            for (loc2 in loc4) 
            {
                (loc1[loc2] as Function).apply(loc2, arg2);
            }
            return;
        }

        private function installSharedObject(arg1:lesta.data.EntityStorage, arg2:*, arg3:String):void
        {
            var loc4:*=undefined;
            var loc5:*=null;
            var loc6:*=null;
            var loc7:*=null;
            var loc8:*=null;
            if (!(arg2 is Array)) 
            {
                arg2 = [arg2];
            }
            var loc1:*=false;
            var loc2:*=0;
            var loc3:*=arg2.length;
            while (loc2 < loc3) 
            {
                loc4 = arg2[loc2];
                if (loc4 is lesta.data.DataChannel) 
                {
                    loc6 = loc4.channelName;
                    loc7 = loc4.invokeName;
                    loc8 = loc4.postAction;
                    if (loc7 == arg3) 
                    {
                        loc1 = true;
                    }
                }
                else 
                {
                    loc6 = loc4;
                    loc7 = arg3;
                    loc8 = null;
                    loc1 = true;
                }
                loc5 = new OnDataReceivedCallback(this, arg1, loc8, loc7);
                lesta.data.GameDelegate.addCallBack(loc6, loc5, loc5.receive);
                lesta.data.GameDelegate.addCallBack(loc6 + "_fast", loc5, loc5.fastReceive);
                this.dataChannelsToCollectionMap[loc6] = arg1;
                this.dataChannelsToCollectionMap[loc6 + "_fast"] = arg1;
                ++loc2;
            }
            arg1.invokeOnAnyUpdate = !loc1;
            return;
        }

        private function getCollectionByDataChannelID(arg1:String):lesta.data.EntityCollection
        {
            return this.dataChannelsToCollectionMap[arg1];
        }

        private function getCachedCallbacks(arg1:String):flash.utils.Dictionary
        {
            var loc1:*=this.cachedCallbacksByType[arg1];
            if (loc1 == null) 
            {
                loc1 = new flash.utils.Dictionary(true);
                this.cachedCallbacksByType[arg1] = loc1;
            }
            return loc1;
        }

        protected var collections:flash.utils.Dictionary;

        protected var objects:flash.utils.Dictionary;

        protected var subscribers:flash.utils.Dictionary;

        protected var cachedCallbacksByType:flash.utils.Dictionary;

        protected var dataChannelsToCollectionMap:flash.utils.Dictionary;
    }
}


class OnDataReceivedCallback extends Object
{
    public function OnDataReceivedCallback(arg1:lesta.data.DataHub, arg2:lesta.data.EntityStorage, arg3:Function, arg4:String)
    {
        super();
        this.entityStorage = arg2;
        this.postReceiveAction = arg3;
        this.invokeName = arg4;
        this.dataHub = arg1;
        return;
    }

    public function receive(arg1:Object):void
    {
        var loc1:*=this.entityStorage.receiveIncomingData(arg1);
        if (this.postReceiveAction != null) 
        {
            this.postReceiveAction(loc1);
        }
        this.dataHub.invokeCallback(this.invokeName);
        if (this.entityStorage.invokeOnAnyUpdate) 
        {
            this.dataHub.invokeCallback(this.entityStorage.id);
        }
        return;
    }

    public function fastReceive(arg1:Array):void
    {
        if (this.postReceiveAction != null) 
        {
            this.postReceiveAction(arg1);
        }
        this.dataHub.invokeCallback(this.invokeName);
        if (this.entityStorage.invokeOnAnyUpdate) 
        {
            this.dataHub.invokeCallback(this.entityStorage.id);
        }
        return;
    }

    private var postReceiveAction:Function;

    private var invokeName:String;

    private var dataHub:lesta.data.DataHub;

    private var entityStorage:lesta.data.EntityStorage;
}