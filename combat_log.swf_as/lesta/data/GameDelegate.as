package lesta.data 
{
    import flash.external.*;
    import flash.utils.*;
    import lesta.utils.*;
    
    public class GameDelegate extends Object
    {
        public function GameDelegate()
        {
            super();
            return;
        }

        public static function call(arg1:String, arg2:Array):void
        {
            arg2.unshift(arg1, 0);
            flash.external.ExternalInterface.call.apply(null, arg2);
            return;
        }

        public static function addCallBack(arg1:String, arg2:Object, arg3:Function):void
        {
            lesta.utils.Debug.assert(!checkScopeForDoubledListeners(arg2, arg1), "GameDelegate Error, " + arg2 + "trying to add second callback for " + arg1);
            var loc1:*=mapMethodCallbacks[arg1];
            if (loc1 == null) 
            {
                loc1 = new MethodCallbacks(arg1);
                mapMethodCallbacks[arg1] = loc1;
            }
            loc1.addCallback(arg2, arg3);
            return;
        }

        public static function removeCallBack(arg1:Object, arg2:String=null, arg3:Function=null):void
        {
            var loc3:*=null;
            var loc1:*=null;
            var loc2:*=false;
            removeFromMapMethodScopes(arg1, arg2);
            if (arg2 == null) 
            {
                var loc4:*=0;
                var loc5:*=mapMethodCallbacks;
                for (loc3 in loc5) 
                {
                    doRemoveCallback(arg1, loc3, arg3);
                }
            }
            else 
            {
                loc1 = mapMethodCallbacks[arg2];
                if (loc1 == null) 
                {
                    return;
                }
                doRemoveCallback(arg1, arg2, arg3);
            }
            return;
        }

        private static function doRemoveCallback(arg1:Object, arg2:String, arg3:Function):void
        {
            if (mapMethodCallbacks[arg2].removeCallback(arg1)) 
            {
                delete mapMethodCallbacks[arg2];
            }
            return;
        }

        private static function checkScopeForDoubledListeners(arg1:Object, arg2:String):Boolean
        {
            if (!(arg1 in mapMethodScopes)) 
            {
                mapMethodScopes[arg1] = new flash.utils.Dictionary();
            }
            if (arg2 in mapMethodScopes[arg1]) 
            {
                return true;
            }
            mapMethodScopes[arg1][arg2] = 0;
            return false;
        }

        private static function removeFromMapMethodScopes(arg1:Object, arg2:String=null):void
        {
            if (!(arg1 in mapMethodScopes)) 
            {
                return;
            }
            if (!arg2) 
            {
                delete mapMethodScopes[arg1];
                return;
            }
            if (!arg2 in mapMethodScopes[arg1]) 
            {
                return;
            }
            delete mapMethodScopes[arg1][arg2];
            var loc1:*=0;
            var loc2:*=mapMethodScopes[arg1];
            for each (arg2 in loc2) 
            {
                return;
            }
            delete mapMethodScopes[arg1];
            return;
        }

        
        {
            mapMethodCallbacks = new flash.utils.Dictionary();
            mapMethodScopes = new flash.utils.Dictionary();
        }

        private static var mapMethodCallbacks:flash.utils.Dictionary;

        private static var mapMethodScopes:flash.utils.Dictionary;
    }
}

import __AS3__.vec.*;
import flash.external.*;
import flash.utils.*;


class Callback extends Object
{
    public function Callback(arg1:Object, arg2:Function)
    {
        super();
        this.scope = arg1;
        this.func = arg2;
        return;
    }

    public var scope:Object;

    public var func:Function;
}

class MethodCallbacks extends Object
{
    public function MethodCallbacks(arg1:String)
    {
        this.listCallbacks = new Vector.<Callback>();
        this.listScopes = new Vector.<Object>();
        this.mapHandlerClasses = new flash.utils.Dictionary();
        super();
        this.methodName = arg1;
        this.mapHandlerClasses[1] = Handler1;
        this.mapHandlerClasses[2] = Handler2;
        this.mapHandlerClasses[3] = Handler3;
        this.mapHandlerClasses[4] = Handler4;
        this.mapHandlerClasses[5] = Handler5;
        this.defaultHandlerClass = HandlerN;
        return;
    }

    public function addCallback(arg1:Object, arg2:Function):void
    {
        if (this.listScopes.indexOf(arg1) != -1) 
        {
            return;
        }
        this.listScopes.push(arg1);
        this.listCallbacks.push(new Callback(arg1, arg2));
        this.updateHandler();
        return;
    }

    public function removeCallback(arg1:Object):Boolean
    {
        var loc1:*=this.listScopes.indexOf(arg1);
        if (loc1 == -1) 
        {
            return false;
        }
        this.listScopes.splice(loc1, 1);
        this.listCallbacks.splice(loc1, 1);
        this.updateHandler();
        return this.listScopes.length == 0;
    }

    private function updateHandler():void
    {
        var loc4:*=null;
        var loc1:*=this.listCallbacks.length;
        var loc2:*=!(this.handler == null) && !(this.handler.count == loc1);
        var loc3:*=!(this.handler == null) && this.handler.count > this.maxCount && loc1 > this.maxCount;
        if (this.handler == null || loc2 && !loc3) 
        {
            loc4 = this.mapHandlerClasses[loc1];
            if (loc4 == null) 
            {
                loc4 = this.defaultHandlerClass;
            }
            this.handler = new loc4();
            this.handler.setReferences(this.listCallbacks);
            flash.external.ExternalInterface.addCallback(this.methodName, this.handler.handle);
        }
        return;
    }

    public var methodName:String=null;

    public var listCallbacks:__AS3__.vec.Vector.<Callback>;

    public var listScopes:__AS3__.vec.Vector.<Object>;

    private var handler:Handler=null;

    private var maxCount:int=5;

    private var mapHandlerClasses:flash.utils.Dictionary;

    private var defaultHandlerClass:Class=null;
}

class Handler extends Object
{
    public function Handler(arg1:int)
    {
        super();
        this.count = arg1;
        return;
    }

    public function setReferences(arg1:__AS3__.vec.Vector.<Callback>):void
    {
        return;
    }

    public function handle():Object
    {
        return null;
    }

    public var count:int=0;
}

class Handler1 extends Handler
{
    public function Handler1()
    {
        super(1);
        return;
    }

    public override function setReferences(arg1:__AS3__.vec.Vector.<Callback>):void
    {
        this.scope0 = arg1[0].scope;
        this.func0 = arg1[0].func;
        return;
    }

    public override function handle():Object
    {
        return this.func0.apply(this.scope0, arguments);
    }

    public var scope0:Object;

    public var func0:Function;
}

class Handler2 extends Handler
{
    public function Handler2()
    {
        super(2);
        return;
    }

    public override function setReferences(arg1:__AS3__.vec.Vector.<Callback>):void
    {
        this.scope0 = arg1[0].scope;
        this.func0 = arg1[0].func;
        this.scope1 = arg1[1].scope;
        this.func1 = arg1[1].func;
        return;
    }

    public override function handle():Object
    {
        this.func0.apply(this.scope0, arguments);
        return this.func1.apply(this.scope1, arguments);
    }

    public var scope0:Object;

    public var func0:Function;

    public var scope1:Object;

    public var func1:Function;
}

class Handler3 extends Handler
{
    public function Handler3()
    {
        super(3);
        return;
    }

    public override function setReferences(arg1:__AS3__.vec.Vector.<Callback>):void
    {
        this.scope0 = arg1[0].scope;
        this.func0 = arg1[0].func;
        this.scope1 = arg1[1].scope;
        this.func1 = arg1[1].func;
        this.scope2 = arg1[2].scope;
        this.func2 = arg1[2].func;
        return;
    }

    public override function handle():Object
    {
        this.func0.apply(this.scope0, arguments);
        this.func1.apply(this.scope1, arguments);
        return this.func2.apply(this.scope2, arguments);
    }

    public var scope0:Object;

    public var func0:Function;

    public var scope1:Object;

    public var func1:Function;

    public var scope2:Object;

    public var func2:Function;
}

class Handler4 extends Handler
{
    public function Handler4()
    {
        super(4);
        return;
    }

    public override function setReferences(arg1:__AS3__.vec.Vector.<Callback>):void
    {
        this.scope0 = arg1[0].scope;
        this.func0 = arg1[0].func;
        this.scope1 = arg1[1].scope;
        this.func1 = arg1[1].func;
        this.scope2 = arg1[2].scope;
        this.func2 = arg1[2].func;
        this.scope3 = arg1[3].scope;
        this.func3 = arg1[3].func;
        return;
    }

    public override function handle():Object
    {
        this.func0.apply(this.scope0, arguments);
        this.func1.apply(this.scope1, arguments);
        this.func2.apply(this.scope2, arguments);
        return this.func3.apply(this.scope3, arguments);
    }

    public var scope0:Object;

    public var func0:Function;

    public var scope1:Object;

    public var func1:Function;

    public var scope2:Object;

    public var func2:Function;

    public var scope3:Object;

    public var func3:Function;
}

class Handler5 extends Handler
{
    public function Handler5()
    {
        super(5);
        return;
    }

    public override function setReferences(arg1:__AS3__.vec.Vector.<Callback>):void
    {
        this.scope0 = arg1[0].scope;
        this.func0 = arg1[0].func;
        this.scope1 = arg1[1].scope;
        this.func1 = arg1[1].func;
        this.scope2 = arg1[2].scope;
        this.func2 = arg1[2].func;
        this.scope3 = arg1[3].scope;
        this.func3 = arg1[3].func;
        this.scope4 = arg1[4].scope;
        this.func4 = arg1[4].func;
        return;
    }

    public override function handle():Object
    {
        this.func0.apply(this.scope0, arguments);
        this.func1.apply(this.scope1, arguments);
        this.func2.apply(this.scope2, arguments);
        this.func3.apply(this.scope3, arguments);
        return this.func4.apply(this.scope4, arguments);
    }

    public var scope0:Object;

    public var func0:Function;

    public var scope1:Object;

    public var func1:Function;

    public var scope2:Object;

    public var func2:Function;

    public var scope3:Object;

    public var func3:Function;

    public var scope4:Object;

    public var func4:Function;
}

class HandlerN extends Handler
{
    public function HandlerN()
    {
        super(int.MAX_VALUE);
        return;
    }

    public override function setReferences(arg1:__AS3__.vec.Vector.<Callback>):void
    {
        this.list = arg1;
        return;
    }

    public override function handle():Object
    {
        var loc3:*=null;
        var loc1:*=null;
        var loc2:*=0;
        while (loc2 < this.list.length) 
        {
            loc3 = this.list[loc2];
            loc1 = loc3.func.apply(loc3.scope, arguments);
            ++loc2;
        }
        return loc1;
    }

    public var list:__AS3__.vec.Vector.<Callback>;
}