package lesta.data 
{
    public class EntitySingleton extends lesta.data.EntityStorage
    {
        public function EntitySingleton(arg1:String, arg2:Class=null)
        {
            super(arg1);
            this.entityClass = arg2;
            this.actualData = arg2 != null ? new arg2() : {};
            return;
        }

        public override function receiveIncomingData(arg1:Object):Array
        {
            injectDataInto(this.actualData, arg1);
            return [this.actualData];
        }

        public function getData():Object
        {
            return this.actualData;
        }

        public function clear():void
        {
            this.actualData = this.entityClass != null ? new this.entityClass() : {};
            return;
        }

        private var actualData:Object;

        private var entityClass:Class;
    }
}
