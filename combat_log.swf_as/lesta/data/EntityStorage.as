package lesta.data 
{
    import flash.utils.*;
    
    public class EntityStorage extends Object
    {
        public function EntityStorage(arg1:String)
        {
            super();
            this._id = arg1;
            return;
        }

        public function receiveIncomingData(arg1:Object):Array
        {
            return null;
        }

        public function injectDataInto(arg1:Object, arg2:Object):void
        {
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=0;
            var loc5:*=0;
            var loc6:*=null;
            var loc1:*=flash.utils.getQualifiedClassName(arg2);
            if (loc1 == OBJECT_CLASS || loc1 == ARRAY_CLASS) 
            {
                var loc7:*=0;
                var loc8:*=arg2;
                for (loc2 in loc8) 
                {
                    arg1[loc2] = arg2[loc2];
                }
            }
            else 
            {
                loc3 = this.introspect(arg2, loc1);
                loc4 = 0;
                loc5 = loc3.length;
                while (loc4 < loc5) 
                {
                    loc6 = loc3[loc4];
                    arg1[loc6] = arg2[loc6];
                    ++loc4;
                }
            }
            return;
        }

        private function introspect(arg1:Object, arg2:String):Array
        {
            var loc1:*=null;
            var loc2:*=null;
            var loc3:*=null;
            var loc4:*=null;
            if (!(arg2 in introspectionCache)) 
            {
                loc1 = flash.utils.describeType(arg1);
                loc2 = [];
                var loc5:*=0;
                var loc6:*=loc1.variable;
                for each (loc3 in loc6) 
                {
                    loc2.push(String(loc3.attribute("name")));
                }
                loc5 = 0;
                loc6 = loc1;
                for each (loc4 in loc6) 
                {
                    loc2.push(String(loc4.attribute("name")));
                }
                introspectionCache[arg2] = loc2;
            }
            return introspectionCache[arg2];
        }

        public function get id():String
        {
            return this._id;
        }

        
        {
            introspectionCache = new flash.utils.Dictionary();
        }

        private static const ARRAY_CLASS:String="Array";

        private static const OBJECT_CLASS:String="Object";

        private var _id:String;

        public var invokeOnAnyUpdate:Boolean;

        private static var introspectionCache:flash.utils.Dictionary;
    }
}
